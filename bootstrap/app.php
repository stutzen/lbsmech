<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
(new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
//
}

/*
  |--------------------------------------------------------------------------
  | Create The Application
  |--------------------------------------------------------------------------
  |
  | Here we will load the environment and create the application instance
  | that serves as the central piece of this framework. We'll use this
  | application as an "IoC" container and router for this framework.
  |
 */

$app = new Laravel\Lumen\Application(
realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();


$app->configure('filesystems');

$app->singleton('filesystem', function ($app) {
    return $app->loadComponent(
        'filesystems',
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        'filesystem'
    );
});


/*
  |--------------------------------------------------------------------------
  | Register Container Bindings
  |--------------------------------------------------------------------------
  |
  | Now we will register a few bindings in the service container. We will
  | register the exception handler and the console kernel. You may add
  | your own bindings here if you like or you can make another file.
  |
 */

$app->singleton(
Illuminate\Contracts\Debug\ExceptionHandler::class,
 App\Exceptions\Handler::class
);

$app->singleton(
Illuminate\Contracts\Console\Kernel::class,
 App\Console\Kernel::class
);

/*
  |--------------------------------------------------------------------------
  | Register Middleware
  |--------------------------------------------------------------------------
  |
  | Next, we will register the middleware with the application. These can
  | be global middleware that run before and after each request into a
  | route or middleware that'll be assigned to some specific routes.
  |
 */

$app->middleware([
App\Http\Middleware\JsonRequestMiddleware::class
]);

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

$app->routeMiddleware([
'auth' => App\Http\Middleware\Authenticate::class,
]);

$app->configure('mail');
$app->configure('services');
$app->configure('app');

//$app->register(Sichikawa\LaravelSendgridDriver\MailServiceProvider::class);

unset($app->availableBindings['mailer']);
/*
  |--------------------------------------------------------------------------
  | Register Service Providers
  |--------------------------------------------------------------------------
  |
  | Here we will register all of the application's service providers which
  | are used to bind services into the container. Service providers are
  | totally optional, so you are not required to uncomment this line.
  |
 */

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

$app->register(\App\Providers\FractalServiceProvider::class);

//$app->register(Laravel\Passport\PassportServiceProvider::class);
//$app->register(Dusterio\LumenPassport\PassportServiceProvider::class);
//
//use Dusterio\LumenPassport\LumenPassport;
//LumenPassport::allowMultipleTokens();
/*
  |--------------------------------------------------------------------------
  | Load The Application Routes
  |--------------------------------------------------------------------------
  |
  | Next we will include the routes file so that they can all be added to
  | the application. This will provide all of the URLs the application
  | can respond to, as well as the controllers that may handle them.
  |
 */
$app->alias(Webpatser\Uuid\Uuid::class, 'Uuid');

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
require __DIR__.'/../app/Http/routes.php';
});



$app->singleton('filesystem', function ($app) {
    return $app->loadComponent('filesystems', 'Illuminate\Filesystem\FilesystemServiceProvider', 'filesystem');
});


$domain = $_SERVER['SERVER_NAME'];
$config = config('app');
$db_path = $config['GET_BU_DB_PATH'];
$json_loc = $db_path . "/" . $domain . '.json';

$filecontent = '';
$filename = $json_loc;
$fh = fopen($filename, "r");
while (!feof($fh)) {
$filecontent .= fgets($fh);
}
fclose($fh);




$json_a = json_decode($filecontent, true);

$retval = array();

$server = $json_a['host'];
$db_name = $json_a['dbname'];
$db_password = $json_a['password'];
$db_username = $json_a['dbuser'];
$time_zone  = "Asia/Kolkata";

try {
$conn = new PDO("mysql:host=$server;dbname=$db_name", $db_username, $db_password);
} catch (Exception $e) {
die("Connection failed: " . $conn->connect_error);
}
$domain = $_SERVER['SERVER_NAME'];
$sql = "SELECT * FROM `tbl_app_config` where setting = 'timezone'";
$res = $conn->query($sql);
foreach ($res as $row) {

$time_zone = $row['value'];
}
date_default_timezone_set($time_zone);

return $app;


/*
  $currentWorkingDir = getcwd();

  $currentDir = substr($currentWorkingDir, 0, strlen($currentWorkingDir) - 6);
  $envArray = parse_ini_file($currentDir . '.env');
  $sso_server = $envArray['DB_HOST'];
  $sso_db_username = $envArray['SSO_DB_USERNAME'];
  $sso_db_password = $envArray['SSO_DB_PASSWORD'];
  $sso_db_name = $envArray['SSO_DB_DATABASE'];
  $app_user_name = '';



  try {
  $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
  } catch (Exception $e) {
  die("Connection failed: " . $conn->connect_error);
  }
  $domain = $_SERVER['SERVER_NAME'];
  $sql = "select * from tbl_bu  where  name  = '" . $domain . "'";
  $res = $conn->query($sql);
  foreach ($res as $row) {

  $time_zone = $row['time_zone'];
  }
  date_default_timezone_set($time_zone);

  return $app;
 */