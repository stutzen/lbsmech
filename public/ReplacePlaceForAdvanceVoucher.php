<?php

class ReplacePlaceHolderForAdvanceVoucher extends ReplacePlaceHolder {

    function getPlaceHolderValue($no, $con, $check_print_pdf, $thous_sep, $from_date, $to_date) {
        $replace_array = array();

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $companyname = $row['value'];
        $replace_array['companyname'] = $companyname;
        $bank = '';
        $addr = '';

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'pay_to_address'");
        while ($row = mysqli_fetch_array($result))
            $addr = $row['value'];
        $array = explode("\n", $addr);
        $pay_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pay_det = $pay_det . "<p> " . $array[$i] . "</p>";
        }

        $replace_array['address'] = $pay_det;
        $configureDateFormat = $this->getDateFormat($con);


        $configureDateFormat = $this->getDateFormat($con);
        $result = mysqli_query($con, "select * from tbl_employee_advance where id = '$no' ");
        while ($row = mysqli_fetch_array($result)) {
            $emp_id = $row['emp_id'];
            $emp_code = $row['emp_code'];
            $date = $this->formatDate($row['date'], $configureDateFormat);
            $adv_amount = $row['adv_amount'];
            
            $word = $this->convertNumberToWord($adv_amount);
            //$total_deduction_amt = $this->convertNumberFormat($total_deduction_amt, $thous_sep);
        }



        $result = mysqli_query($con, "select * from tbl_employee where id = '$emp_id' ");
        while ($row = mysqli_fetch_array($result)) {
            $name = $row['f_name'] . '' . $row['l_name'];
            $emp_code = $row['emp_code'];
        }

        $currency = $this->getCurrencySymbol($con);
        $replace_array['currency'] = $currency;
                $replace_array['no'] = $no;

        $replace_array['emp_code'] = $emp_code;
        $replace_array['date'] = $date;
        $replace_array['name'] = $name;
        $replace_array['adv_amount'] = $adv_amount;
        $replace_array['word'] = $word;



        return $replace_array;
    }

    function getDesign($con, $no, $replace_array, $check_print_pdf, $thous_sep) {

        $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'advance_vocher_template'");
        while ($row = mysqli_fetch_array($result))
            $msg = $row['message'];
        if ($check_print_pdf == 'for_pdf') {
            $new_msg = $msg;
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $new_msg);
        }

        $replace_msg = $this->replacePlaceHolderValue($replace_array, $msg, $thous_sep);
        return $replace_msg;
    }

    function replacePlaceHolderValue($replace_array, $msg, $thous_sep) {




        $new_msg = str_replace("{{companyname}}", $replace_array['companyname'], $msg);
        $new_msg = str_replace("{{address}}", $replace_array['address'], $new_msg);

        $new_msg = str_replace("{{name}}", $replace_array['name'], $new_msg);
        $new_msg = str_replace("{{emp_code}}", $replace_array['emp_code'], $new_msg);

        $new_msg = str_replace("{{word}}", $replace_array['word'], $new_msg);
        $new_msg = str_replace("{{date}}", $replace_array['date'], $new_msg);

        $new_msg = str_replace("{{adv_amount}}", $replace_array['adv_amount'], $new_msg);
                $new_msg = str_replace("{{no}}", $replace_array['no'], $new_msg);

        return $new_msg;
        
    }

}

?>
