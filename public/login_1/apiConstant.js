









app.constant('APP_CONST', {
    USER_INFO_SYNC_API: "/public/user/userInfo",
    LOGIN_PAGE_LOADING:"/public/login",
    LOGOUT_API_URL: "/swf/logout",
    API: {
        LOGIN_PAGE_LOADING:'/lbs/public/auth/login'        
    },
    BLOCKER_MSG: {
        PAGE_NAVIGATE: "You will lose unsaved changes if you leave this page",
        PAGE_RELOAD: "You will lose unsaved changes if you reload this page"

    },
    REDIRECT: {
        STAFF_LOGIN_SUCCESS: "/app/business/#/professioanl/myaccount",
        APP_MANAGER_LOGIN_SUCCESS: "/app/business/#/clinic/myaccount",
        ROOT_ADMIN_LOGIN: "/admin/#/",
        ROOT_ADMIN_LOGIN_SUCCESS: "/admin/#/dashboard/clinic",
        UNMAPPED_ROLE_REDIRECT: "/app/business/#/clinic/myaccount/chooserole"
    },
    ROLE: {
        STAFF: 'ROLE_STAFF',
        GUEST: 'GUEST',
        CONSUMER: 'ROLE_CONSUMER',
        ADMIN: 'ROLE_COMPANY',
        ROOT_ADMIN: 'ROLE_ROOT_ADMIN',
        STAFF_APP_MANAGER: 'ROLE_STAFF_APP_MANAGER',
        CLINIC_APP_MANAGER: 'ROLE_CLINIC_APP_MANAGER',
        COMPANY: 'ROLE_COMPANY'
    },
    CLOUD: {
        PREFIX_URL: 'https://storage.googleapis.com/face2face'

    },
    DEFAULT_GEO_POINT: {
        LAT: 51.5073509,
        LNG: -0.1277583
    },
    COUNTRY_MOBILECODE: [
        {'code': '+33', 'name': 'FRA', 'shortname': 'FRA'},
        {'code': '+91', 'name': 'IND', 'shortname': 'IND'},
        {'code': '+40', 'name': 'ROU', 'shortname': 'ROU'},
        {'code': '+94', 'name': 'LKA', 'shortname': 'LKA'},
        {'code': '+966', 'name': 'SAU', 'shortname': 'SAU'},
        {'code': '+971', 'name': 'UAE', 'shortname': 'UAE'},
        {'code': '+44', 'name': 'UK', 'shortname': 'UK'}
    ]
});