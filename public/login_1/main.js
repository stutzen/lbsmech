'use strict';
/* Controllers */

angular.module('app')
        .controller('AppCtrl', ['$scope', '$rootScope', '$translate', '$localStorage', '$window', '$location', '$state', '$timeout', '$cookies', 'Auth', 'StutzenHttpService', 'APP_CONST', function($scope, $rootScope, $translate, $localStorage, $window, $location, $state, $timeout, $cookies, Auth, StutzenHttpService, APP_CONST) {


                $rootScope.userModel = {
                    userName: '',
                    role: ['']

                };
                 $rootScope.CONTEXT_ROOT = "/lbs";
                /*
                 *  It used to take APP_CONST value in UI
                 *
                 */
                $rootScope.APP_CONST = angular.copy(APP_CONST);

                // config
                $rootScope.app = {
                    roleSelectPopup: false,
                    selectedRoleIndex: -1,
                    selectedRole: '',
                    selectedClinicId: '',
                    selectedClinicName: ''
                }

                $rootScope.getNavigationBlockMsg = null;
                window.onbeforeunload = function(event) {

                    var message = null;
                    if ($rootScope.getNavigationBlockMsg != null)
                    {
                        message = $rootScope.getNavigationBlockMsg(true);
                    }
                    if (message != null && message.length > 0)
                    {
                        return message;
                    }
                }
                window.addEventListener('online', onLineIndicatorHandler);
                window.addEventListener('offline', onLineIndicatorHandler);
                function onLineIndicatorHandler($event, response)
                {
                    if ($event.type !== undefined && $event.type.toLowerCase() === 'offline')
                    {
                        $timeout(function() {
                            $scope.serverDisconnectError = "Disconnected from the server";
                        });
                    }
                    else
                    {
                        $timeout(function() {
                            $scope.serverDisconnectError = "";
                        });
                    }
                }
                ;

                $scope.$on('$destroy', function() {
                    delete window.onbeforeunload;
                    angular.element(window).off('online', onLineIndicatorHandler);
                    angular.element(window).off('offline', onLineIndicatorHandler);
                });



                $rootScope.refer = ""
                        ;
                $rootScope.isLogined = false;
                $rootScope.isUserModelUpdated = false;
                $rootScope.cloudImagePrefixUrl = APP_CONST.CLOUD_IMAGE_PREFIX_URL;
                $rootScope.autoSearchList = [];
                $rootScope.autoSearchLimit = 5;
                $rootScope.searchMapLocation = "";
                $rootScope.searchKeyword = "";
                $scope.cookieAcception = typeof $cookies.get('cookie-acception') == 'undefined' ? false : true;
                $scope.signupRedirectPopup = false;
                $scope.signinRedirectPopup = false;
                $scope.actionMsgSuccess = "";
                $scope.actionMsgWarning = "";
                $scope.actionMsgError = "";
                $scope.serverDisconnectError = "";
                $scope.errorMsgAliveTime = 5000;
                $scope.successMsgAliveTime = 5000;
                $scope.warningMsgAliveTime = 5000;
                $scope.autoSearchServerSyncDelayTime = 200;
                $scope.autoCompleteFilterList = '';
                $scope.mapSearchOptions = {
                    //country: 'uk',
                    //types: '(cities)'
                };
                //            $scope.search = {
                //
                //                autosuggestlist : []
                //
                //            };
                $scope.searchMapLocationGecodeDetail = null;
                $scope.searchMapLocationDetail = null;
                //$scope.searchMapLocation = "";
                $scope.autoSearchDetail = null;
                $scope.aliveAutoSearch = false;
                $scope.autoSearchList = [];
                $scope.autoSearchTimeoutPromise = null;
                $scope.autoSearchList1 = [{
                        name: 'Global',
                        searchType: 'Company'
                    }, {
                        name: 'Rosy',
                        searchType: 'Staff'
                    }];
                //            $scope.tags = [ 'bootstrap', 'list', 'angular' ];
                //            $scope.allTags = [ 'bootstrap', 'list', 'angular', 'directive', 'edit', 'label', 'modal', 'close', 'button', 'grid', 'javascript', 'html', 'badge', 'dropdown'];
                //
                $scope.$on('updateUserInfoEvent', updateUserInfoEventHandler);
                $scope.$on('httpGenericErrorEvent', genericHttpErrorHandler);
                $scope.$on('httpGenericResponseSuccessEvent', genericHttpResponseHandler);
                $scope.$on('genericErrorEvent', genericErrorHandler);
                $scope.$on('genericSuccessEvent', genericSuccessHandler);
                //temp fix for search keyword update.
                //Started temp fix to update the map location.
                $scope.keywoardChange = function(value) {
                    $rootScope.$broadcast('updateSearchKeywordEvent', value);
                }

                $scope.beyondChange = function(value) {
                    $rootScope.$broadcast('updateSearchBeyondEvent', value);
                }

                //$scope.$on('updateSearchKeywordEvent', updateSearchKeywordEventHandler);
                //$scope.$on('updateSearchBeyondEvent', updateSearchBeyondEventHandler);
                // $scope.$on('updateGeocodeEvent', updateGeocodeEventHandler);

                function updateSearchKeywordEventHandler($event, data)
                {
                    $scope.keyword = data;
                }

                function updateSearchBeyondEventHandler($event, data)
                {
                    $scope.homeSearchBeyond = data;
                }

                $scope.clearPreviousError = function()
                {
                    $scope.actionMsgError = '';
                    $scope.actionMsgSuccess = '';
                    $scope.actionMsgWarning = '';
                }

                $scope.setErrorMessage = function(msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgError = msg;
                    $timeout(function() {
                        $scope.actionMsgError = '';
                    }, $scope.errorMsgAliveTime);
                };
                $scope.setSuccessMessage = function(msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgSuccess = msg;
                    $timeout(function() {
                        $scope.actionMsgSuccess = '';
                    }, $scope.successMsgAliveTime);
                };
                $scope.setWarningMessage = function(msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgWarning = msg;
                    $timeout(function() {
                        $scope.actionMsgWarning = '';
                    }, $scope.warningMsgAliveTime);
                };
                $scope.logout = function()
                {
                    StutzenHttpService.get(APP_CONST.LOGOUT_API_URL, {}, true).then(function(response) {
                        $scope.setSuccessMessage("You have logout successfully.");
                        $timeout(function() {
                            Auth.setUserModel(null, '');
                        }, 100);
                    });
                }

                $scope.updateUserType = function(userType) {

                    $scope.homeSearchUserType = userType;
                }


                function updateUserInfoEventHandler($event, data)
                {

                    if (data.success == true)
                    {
                        $rootScope.isLogined = true;
                        $rootScope.userModel = data.data;
                        $scope.selectDefaultLoginRole();
                    }
                    else
                    {
                        $rootScope.isLogined = false;
                        $rootScope.userModel = {
                            userName: '',
                            role: ['']
                        };
                    }
                    $rootScope.isUserModelUpdated = true;
                }

                function genericHttpErrorHandler($event, response)
                {
                    if (response.status == "404")
                    {
                        $scope.setErrorMessage("Request Not Acceptable.");
                    }
                    else if (response.status == "406")
                    {
                        $scope.setErrorMessage("Request Not Acceptable.");
                    }
                    else if (response.status == "500")
                    {
                        $scope.setErrorMessage("Internal Server Error.");
                    }
                    else if (response.status == "503")
                    {
                        $scope.setErrorMessage("Service Unavailable");
                    }
                    else
                    {
                        $scope.setErrorMessage(response.statusText + " (" + response.status + ")");
                    }


                }

                function genericHttpResponseHandler($event, response)
                {

                    if (typeof response.data != 'undefined')
                    {
                        if (typeof response.data.success != 'undefined')
                        {
                            if (response.data.success == true)
                            {
                                $scope.setSuccessMessage(response.data.message);
                            }
                            else
                            {
                                $scope.setErrorMessage(response.data.message);
                            }

                        }
                    }
                }

                function genericErrorHandler($event, response)
                {
                    $scope.setErrorMessage(response);
                }
                function genericSuccessHandler($event, response)
                {
                    $scope.setSuccessMessage(response);
                }

                $scope.setCookie = function(key, value) {

                    var cookieOption = {
                        path: "/"
                    };
                    $cookies.put(key, value, cookieOption);
                };
                $scope.cookieAccept = function() {

                    $scope.setCookie('cookie-acception', true);
                    $scope.cookieAcception = typeof $cookies.get('cookie-acception') == 'undefined' ? false : true;
                };
                $scope.altSearch = function(userType) {

                    $scope.homeSearchUserType = userType;
                    $scope.search();
                }

                $scope.openPopup = function(value)
                {
                    if (value === 'register')
                    {
                        $scope.signupRedirectPopup = true;
                    }
                    else if (value === 'signin')
                    {
                        $scope.signinRedirectPopup = true;
                    }
                    else if (value === 'role')
                    {
                        if ($scope.app.roleSelectPopup)
                        {
                            $scope.app.roleSelectPopup = false;
                        }
                        else
                        {
                            $scope.app.roleSelectPopup = true;
                        }
                    }
                }

                $scope.closePopup = function(value)
                {
                    if (value === 'register')
                    {
                        $scope.signupRedirectPopup = false;
                    }
                    else if (value === 'signin')
                    {
                        $scope.signinRedirectPopup = false;
                    }
                    else if (value === 'role')
                    {
                        $scope.app.roleSelectPopup = false;
                    }

                }

                $scope.selectDefaultLoginRole = function(value)
                {
                    $rootScope.app.selectedRole = $rootScope.userModel.role[0];
                    $rootScope.app.selectedRoleIndex = -1;
                    $rootScope.app.selectedClinicId = '';
                    $rootScope.app.selectedClinicName = '';
                    $scope.closePopup('role');
                }

                $scope.companyChangeHandler = function(index)
                {
                    if (index < $rootScope.userModel.adminCompId.length)
                    {
                        $rootScope.app.selectedRoleIndex = index;
                        $rootScope.app.selectedRole = APP_CONST.ROLE.ADMIN;
                        $rootScope.app.selectedClinicId = $rootScope.userModel.adminCompId[index].id;
                        $rootScope.app.selectedClinicName = $rootScope.userModel.adminCompId[index].name;
                    }

                    $scope.closePopup('role');
                }


            }])
        .factory('Auth', function($http, $cookies, $location, $window, $rootScope, APP_CONST) {

            var factory = {};
            //factory.USER_INFO_SYNC_API ="/user/userInfo";

            factory.userModel = null;
            factory.isLogined = function()
            {
                if ($rootScope.isUserModelUpdated)
                    return $rootScope.isLogined;
                return false;
                //var session_key =  $cookies.get('JSESSIONID');
                //            var auth_key =  $cookies.get('f2f-auth-key');
                //            if( typeof auth_key == 'undefined')
                //            {
                //                return false;
                //            }
                ////            if( typeof session_key != 'undefined' && auth_key != session_key)
                ////            {
                ////                return false;
                ////            }
                //            if(factory.userModel == null)
                //            {
                //                factory.getUserInfo();
                //            }
                //            return true;
            }

            factory.getUserInfo = function() {

                return $http({
                    method: 'GET',
                    url: APP_CONST.USER_INFO_SYNC_API

                }).success(function(data, status, headers, config) {

                    if (typeof data.data != 'undefined')
                        factory.userModel = data.data;
                    $rootScope.$broadcast('updateUserInfoEvent', data);
                }).error(function(data, status, headers, config) {

                });
            }

            factory.loginAsync = function()
            {

                if (!factory.isLogined())
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + "/app/login/#/login-customer" + "?refer=" + encodeURIComponent($location.absUrl());
                    $window.location.href = landingUrl;
                }
            }

            factory.setUserModel = function(userData, referUrl)
            {
                if (userData != null)
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host;
                }
                else
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + "/admin";
                }
                var previousRole = "";
                if (factory.userModel != null)
                {
                    previousRole = factory.userModel.userType;
                }
                factory.userModel = userData;
                if (referUrl != "")
                {
                    landingUrl = decodeURIComponent(referUrl);
                }
                if (referUrl == "")
                {
                    if (factory.userModel != null)
                    {
                        var userRole = factory.userModel.role;
                        if (userRole[0] === APP_CONST.ROLE.STAFF)
                        {
                            landingUrl += APP_CONST.REDIRECT.STAFF_LOGIN_SUCCESS;
                        }
                        else if (userRole[0] === APP_CONST.ROLE.APP_MANAGER)
                        {
                            landingUrl += APP_CONST.REDIRECT.APP_MANAGER_LOGIN_SUCCESS;
                        }
                        else if (userRole[0] === APP_CONST.ROLE.ROOT_ADMIN)
                        {
                            if ($rootScope.isLogined)
                            {
                                landingUrl += APP_CONST.REDIRECT.ROOT_ADMIN_LOGIN_SUCCESS;

                            }
                            else
                            {
                                landingUrl += APP_CONST.REDIRECT.ROOT_ADMIN_LOGIN;
                            }
                        }
                        else if (userRole[0] == "-1")
                        {
                            landingUrl += APP_CONST.REDIRECT.UNMAPPED_ROLE_REDIRECT;
                        }
                    }
                }

                $window.location.href = landingUrl;
            };

            factory.validatePageAccess = function()
            {
                var landingUrl = $window.location.protocol + "//" + $window.location.host;

                if ($rootScope.app.selectedRole === APP_CONST.ROLE.STAFF)
                {
                    landingUrl += APP_CONST.REDIRECT.STAFF_LOGIN_SUCCESS;
                }
                else if ($rootScope.app.selectedRole === APP_CONST.ROLE.APP_MANAGER)
                {
                    landingUrl += APP_CONST.REDIRECT.APP_MANAGER_LOGIN_SUCCESS;
                }
                else if ($rootScope.app.selectedRole === APP_CONST.ROLE.ROOT_ADMIN)
                {
                    if ($rootScope.isLogined)
                    {
                        landingUrl += APP_CONST.REDIRECT.ROOT_ADMIN_LOGIN_SUCCESS;

                    }
                    else
                    {
                        landingUrl += APP_CONST.REDIRECT.ROOT_ADMIN_LOGIN;
                    }
                }
                else if ($rootScope.app.selectedRole == "-1")
                {
                    landingUrl += APP_CONST.REDIRECT.UNMAPPED_ROLE_REDIRECT;
                }
                $window.location.href = landingUrl;
            };


            return factory;
        })
        .factory('StutzenHttpInterceptor', ['$q', '$injector', '$rootScope', function($q, $injector, $rootScope) {

                var stutzenHttpInterceptor = {};
                stutzenHttpInterceptor.responseError = function(response) {

                    $rootScope.$broadcast('httpGenericErrorEvent', response);
                    return $q.reject(response);
                }

                stutzenHttpInterceptor.response = function(response) {
                    //Global Redirect for session out
                    var responseData = "" + response.data;
                    var searchKey = 'swf/login_check'
                    if (responseData.indexOf(searchKey) != -1)
                    {
                        window.location.href = window.location.protocol + "//" + window.location.host + "/admin/";

                    }

                    if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined')
                    {
                        var config = response.config.config;
                        if (config.handleResponse == true)
                        {
                            $rootScope.$broadcast('httpGenericResponseSuccessEvent', response);
                        }
                    }
                    return response;
                }

                return stutzenHttpInterceptor;
            }])

        .factory('StutzenHttpService', ['$http', function($http) {
                var stutzenHttpService = {};
                stutzenHttpService.get = function(url, data, handleResponse) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'GET',
                        url: url,
                        params: data, config: configOption
                    }).success(function(data, status, headers, config) {

                    }).error(function(data, status, headers, config) {

                    });
                }

                stutzenHttpService.post = function(url, data, handleResponse) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'POST',
                        url: url,
                        data: angular.toJson(data, 0), config: configOption
                    }).success(function(data, status, headers, config) {

                    }).error(function(data, status, headers, config) {

                    });
                }
                stutzenHttpService.formPost = function(url, data, handleResponse) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'POST',
                        url: url,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'handleResponse': handleResponse
                        },
                        transformRequest: function(obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: data,
                        config: configOption

                    }).success(function(data, status, headers, config) {

                    }).error(function(data, status, headers, config) {

                    });
                }
                return stutzenHttpService;
            }])
        .factory('MapLoaderService', function($timeout) {

            var mapLoaderService = {};
            mapLoaderService.map = null;
            mapLoaderService.markerList = [];
            mapLoaderService.init = function(domId, centerPoint, markers) {

                var latValue = 51.1464659;
                var lngValue = 0.875019;
                if (centerPoint != null)
                {
                    latValue = centerPoint.lagtitude;
                    lngValue = centerPoint.longitude;
                }
                else if (markers != null && markers.length)
                {
                    latValue = markers[0].lagtitude;
                    lngValue = markers[0].longitude;
                }

                mapLoaderService.map = new google.maps.Map(document.getElementById(domId), {
                    zoom: 10,
                    center: {
                        lat: latValue,
                        lng: lngValue
                    }
                });
                $timeout(function() {
                    mapLoaderService.loadMarkerPoint(markers);
                }, 500);
            }

            mapLoaderService.loadMarkerPoint = function(markers) {

                if (markers != null)
                {
                    for (var i = 0; i < markers.length; i++) {
                        var position = new google.maps.LatLng(markers[i]['lagtitude'], markers[i]['longitude']);
                        //bounds.extend(position);
                        var marker = new google.maps.Marker({
                            position: position,
                            map: mapLoaderService.map,
                            title: markers[i]['name']
                        });
                        mapLoaderService.markerList.push(marker);
                    }
                }

            }
            mapLoaderService.clearMarker = function() {

                if (mapLoaderService.markerList == null)
                    return;
                for (var i = 0; i < mapLoaderService.markerList.length; i++) {

                    mapLoaderService.markerList[i].setMap(null);
                }
            }

            mapLoaderService.createDragableMarkerPoint = function(markers) {

                mapLoaderService.clearMarker();
                var latValue = 51.1464659;
                var lngValue = 0.875019;
                if (markers != null)
                {
                    latValue = markers['lagtitude'];
                    lngValue = markers['longitude'];
                }
                var position = new google.maps.LatLng(latValue, lngValue);
                //bounds.extend(position);
                var marker = new google.maps.Marker({
                    position: position,
                    draggable: true,
                    map: mapLoaderService.map

                });
                mapLoaderService.map.panTo(marker.getPosition());
                google.maps.event.addListener(marker, 'dragend', function(event) {
                    //$('#position').val('* '+this.getPosition().lat()+','+this.getPosition().lng());
                    //saveData(map,event);
                });
                mapLoaderService.markerList.push(marker);
            }


            return mapLoaderService;
        })

        .config(function() {
            var styleEl = document.createElement('style'),
                    styleSheet,
                    rippleCSS,
                    rippleLightCSS,
                    rippleKeyframes,
                    rippleWebkitKeyframes;
            rippleCSS = [
                '-webkit-animation: ripple 800ms ease-out;',
                'animation: ripple 800ms ease-out;',
                'background-color: rgba(0, 0, 0, 0.16);',
                'border-radius: 100%;',
                'height: 10px;',
                'pointer-events: none;',
                'position: absolute;',
                'transform: scale(0);',
                'width: 10px;'
            ];
            rippleLightCSS = 'background-color: rgba(255, 255, 255, 0.32);';
            rippleKeyframes = [
                '@keyframes ripple {',
                'to {',
                'transform: scale(2);',
                'opacity: 0;',
                '}',
                '}'
            ];
            rippleWebkitKeyframes = [
                '@-webkit-keyframes ripple {',
                'to {',
                '-webkit-transform: scale(2);',
                'opacity: 0;',
                '}',
                '}'
            ];
            document.head.appendChild(styleEl);
            styleSheet = styleEl.sheet;
            styleSheet.insertRule('.ripple-effect {' + rippleCSS.join('') + '}', 0);
            styleSheet.insertRule('.ripple-light .ripple-effect {' + rippleLightCSS + '}', 0);
            if (CSSRule.WEBKIT_KEYFRAMES_RULE) { // WebKit                 styleSheet.insertRule(rippleWebkitKeyframes.join(''), 0);
            }
            else if (CSSRule.KEYFRAMES_RULE) { // W3C
                styleSheet.insertRule(rippleKeyframes.join(''), 0);
            }
        })
        .directive('ripple', function() {
            return {
                restrict: 'C',
                link: function(scope, element, attrs) {
                    element[0].style.position = 'relative';
                    element[0].style.overflow = 'hidden';
                    element[0].style.userSelect = 'none';
                    element[0].style.msUserSelect = 'none';
                    element[0].style.mozUserSelect = 'none';
                    element[0].style.webkitUserSelect = 'none';
                    function createRipple(evt) {
                        var ripple = angular.element('<span class="ripple-effect animate">'),
                                rect = element[0].getBoundingClientRect(),
                                radius = Math.max(rect.height, rect.width),
                                left = evt.pageX - rect.left - radius / 2 - document.body.scrollLeft,
                                top = evt.pageY - rect.top - radius / 2 - document.body.scrollTop;
                        ripple[0].style.width = ripple[0].style.height = radius + 'px';
                        ripple[0].style.left = left + 'px';
                        ripple[0].style.top = top + 'px';
                        ripple.on('animationend webkitAnimationEnd', function() {
                            this.remove();
                        });
                        element.append(ripple);
                    }

                    element.on('click', createRipple);
                }
            };
        })
        .directive('elemReady', function($parse) {
            return {
                restrict: 'A',
                link: function($scope, elem, attrs) {
                    enableSelectBoxes();
                }
            }
        })
        .directive('elemfocusSelect', function($parse) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.on('click', function() {
                        this.select();
                    });
                }
            };
        })
        .directive('staticInclude', function() {
            return {
                restrict: 'A',
                templateUrl: function(ele, attrs) {
                    return attrs.staticInclude;
                }
            };
        })
        .directive('googleMap', ['$googleMapService', '$timeout', function($googleMapService, $timeout) {
                return {
                    restrict: 'A',
                    scope: {
                        option: '='
                    },
                    link: function(scope, element, attrs) {

                        $timeout(function() {
                            $googleMapService.initMap(attrs.id, null, null, scope.option);
                        }, 300);
                    }
                };
            }])
//        .directive('elemFormField', function() {
//            return {
//                restrict: 'A',
//                // just postLink
//                link: function(scope, element, attrs, ngModelCtrl) {
//                    // do nothing in case of no 'name' attribiute
//                    if (!attrs.name) {
//                        return;
//                    }
//                    // fix what should be fixed
//                    ngModelCtrl.$name = attrs.name;
//                },
//                // ngModel's priority is 0
//                priority: '-100',
//                // we need it to fix it's behavior
//                require: 'ngModel'
//            };
//        })
//
//        $("#myInputField").focus(function(){
//            // Select input field contents
//            this.select();
//        });
        .filter('trustedhtml', function($sce) {
            return $sce.trustAsHtml;
        })
        .filter('offset', function() {
            return function(input, start) {
                start = parseInt(start, 10);
                if (typeof input != 'undefined' && typeof input.slice != 'undefined')
                    return input.slice(start);
                else
                    return input;
            };
        });




