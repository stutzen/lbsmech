<?php

class ReplacePlaceHolderForDelivery extends ReplacePlaceHolder {

    function getPlaceHolderValue($no, $con, $check_print_pdf, $thous_sep, $from_date, $to_date) {
        $replace_array = array();

        $bank = '';
        $addr = '';
        $configureDateFormat = $this->getDateFormat($con);
        $result = mysqli_query($con, "select * from tbl_delivery_instruction where id = '$no' ");
        while ($row = mysqli_fetch_array($result)) {
            $transport_id = $row['transport_id'];
            $driver_name = $row['driver_name'];
            $driver_mobile_no = $row['driver_mobile_no'];
            $freight = $row['freight'];
            $no_of_pkts = $row['no_of_pkts'];
            $dl_date = $this->formatDate($row['delivery_date'], $configureDateFormat);
            $delivery_date = $dl_date;
            $final_destination = $row['final_destination'];
            $vehicle_no = $row['vehicle_no'];
            $driver_mobile_no = $row['driver_mobile_no'];
            $vehicle_type = $row['vehicle_type'];
            $vehicle_no = $row['vehicle_no'];
            
        }



        $result = mysqli_query($con, "select * from tbl_transport where id = '$transport_id' ");
        while ($row = mysqli_fetch_array($result)) {
            $address = $row['address'];
            $mobile_no = $row['mobile_no'];
            $name = $row['name'];
            $landline_no = $row['landline_no'];
            $city_name = $row['city_name'];
            $state_name = $row['state_name'];
            $country_name = $row['country_name'];
            $postcode = $row['postcode'];
            $city_name = $row['city_name'];
            $state_name = $row['state_name'];
            $tran_address = $address . ' , ' . $state_name . " , " . $city_name . " , " . $country_name . " , " . $postcode;
        }
        $thous_sep = $this->getThosandSeperatorFormat($con);
        $replace_array['di_details'] = array();
        $net_amount = 0;
        $i = 0;
        $result = mysqli_query($con, "select * from tbl_delivery_instruction_detail where delivery_instruction_id = '$no' ");
        while ($row = mysqli_fetch_array($result)) {
            $ref_type = $row['ref_type'];
            if($i==0) {
                $customer_id = $row['customer_id'];
                $i++;
            }
            $ref_id = $row['ref_id'];
            if ($ref_type == 1) {
                $result1 = mysqli_query($con, "select * from tbl_invoice where id = '$ref_id' ");
                while ($row1 = mysqli_fetch_array($result1)) {
                    $date = $this->formatDate($row1['date'], $configureDateFormat);
                    $ref_no = $row1['invoice_code'] . " " . $date;
                    $ref_amount = $row1['total_amount'];
                }
            } else {
                $result1 = mysqli_query($con, "select * from tbl_sales_order where id = '$ref_id' ");
                while ($row1 = mysqli_fetch_array($result1)) {
                    $ref_no = $row1['sales_order_code'] . " " . $row1['date'];
                    $ref_amount = $row1['total_amount'];
                }
            }

            $di_det['ref_no'] = $ref_no;            
            $di_det['ref_amount'] = $ref_amount;
            $di_det['parcel'] = $row['no_of_pkts'];
            $fright = $row['freight'];
            $fright = number_format((float) $fright, 2, '.', '');
            $fright = $this->convertNumberFormat($fright, $thous_sep);
            $di_det['fright'] = $fright;
            $row_total = $row['freight'] + $ref_amount;
            $row_total = number_format((float) $row_total, 2, '.', '');
            $row_total = $this->convertNumberFormat($row_total, $thous_sep);

            $di_det['row_total'] = $row_total;
            $net_amount = $net_amount + $row['freight'] + $ref_amount;
            ;
            $ref_amount = number_format((float) $ref_amount, 2, '.', '');
            $ref_amount = $this->convertNumberFormat($ref_amount, $thous_sep);

            array_push($replace_array['di_details'], $di_det);
        }
         $result = mysqli_query($con, "select * from tbl_customer where id = '$customer_id' ");
        while ($row = mysqli_fetch_array($result)) {
            $cus_address = $row['fname'] . "\n". $row['billing_address'];
            $cus_phone = $row['phone'];
            $replace_array['cus_phone'] = $cus_phone;
        }
        $array = explode("\n", $cus_address);
        $billing = "";
        for ($i = 0; $i < count($array); $i++) {
            $address = $address . "<p style=margin-left:0px;>    " . $array[$i] . "</p>";
        }
        $replace_array['cus_address'] = $address;

        if ($ref_type == 1)
            $ref_type = 'Invoice';
        else
            $ref_type = 'Estimate';

        $word = $this->forWordinPaise($net_amount);
        $replace_array['amount_word'] = $word;
        $net_amount = number_format((float) $net_amount, 2, '.', '');
        $net_amount = $this->convertNumberFormat($net_amount, $thous_sep);
        $currency = $this->getCurrencySymbol($con);

        $replace_array['currency'] = $currency;
        $replace_array['vehicle_no'] = $vehicle_no;
        $replace_array['transport'] = $name;
        $replace_array['net_amount'] = $net_amount;
        $replace_array['driver_name'] = $driver_name;
        $replace_array['transport_addr'] = $tran_address;
        $replace_array['phone'] = $mobile_no;
        $replace_array['land_line'] = $landline_no;
        $replace_array['delivery_date'] = $delivery_date;
        $replace_array['lr_no'] = $no;

        $replace_array['to'] = $final_destination;
        $replace_array['lorry_no'] = $vehicle_no;
        $replace_array['driver_cell'] = $driver_mobile_no;
        $replace_array['type'] = $vehicle_type;
        $replace_array['from'] = $ref_type;
        

        return $replace_array;
    }

    function getDesign($con, $no, $replace_array, $check_print_pdf, $thous_sep) {

        $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'delivery_template'");
        while ($row = mysqli_fetch_array($result))
            $msg = $row['message'];
        $temp = 't2';
        if ($check_print_pdf == 'for_pdf') {
            $new_msg = $msg;
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $new_msg);
      }
       
        $replace_msg = $this->replacePlaceHolderValue($replace_array, $msg, $thous_sep);
        return $replace_msg;
    }

    function replacePlaceHolderValue($replace_array, $msg, $thous_sep) {
        $di_details = $replace_array['di_details'];

        $extract_string = $this->extractString($msg, '<!--di-row-start-->', '<!--di-row-end-->');

        $copy_string = $extract_string;
        $count = 0;
        $product_det = "";
        foreach ($di_details as $di_det) {
            $count = $count + 1;

            $extract_string = str_replace("{{ref_no}}", $di_det['ref_no'], $extract_string);
            $extract_string = str_replace("{{ref_aount}}", $di_det['ref_amount'], $extract_string);
            $extract_string = str_replace("{{parcel}}", $di_det['parcel'], $extract_string);
            $extract_string = str_replace("{{fright}}", $di_det['fright'], $extract_string);
            $extract_string = str_replace("{{row_total}}", $di_det['row_total'], $extract_string);
            $product_det = $product_det . $extract_string;
            $extract_string = $copy_string;
        }


        //$msg = preg_replace("/<!--product-row-start-->\n|\n <!--product-row-end-->/i", $product_det, $msg);
        $msg = str_replace("<!--di-row-start-->$extract_string<!--di-row-end-->", $product_det, $msg);

        $new_msg = str_replace("{{transport}}", $replace_array['transport'], $msg);

        $new_msg = str_replace("{{driver_name}}", $replace_array['driver_name'], $new_msg);
        $new_msg = str_replace("{{transport_addr}}", $replace_array['transport_addr'], $new_msg);
        $new_msg = str_replace("{{phone}}", $replace_array['phone'], $new_msg);
        $new_msg = str_replace("{{land_line}}", $replace_array['land_line'], $new_msg);
        $new_msg = str_replace("{{delivery_date}}", $replace_array['delivery_date'], $new_msg);
        $new_msg = str_replace("{{land_line}}", $replace_array['land_line'], $new_msg);



        $new_msg = str_replace("{{to}}", $replace_array['to'], $new_msg);
        $new_msg = str_replace("{{lorry_no}}", $replace_array['lorry_no'], $new_msg);
        $new_msg = str_replace("{{driver_cell}}", $replace_array['driver_cell'], $new_msg);
        $new_msg = str_replace("{{type}}", $replace_array['type'], $new_msg);
        $new_msg = str_replace("{{from}}", $replace_array['from'], $new_msg);
        $new_msg = str_replace("{{net_amount}}", $replace_array['net_amount'], $new_msg);
        $new_msg = str_replace("{{currency}}", $replace_array['currency'], $new_msg);
        $new_msg = str_replace("{{amount_word}}", $replace_array['amount_word'], $new_msg);
        $new_msg = str_replace("{{cus_address}}", $replace_array['cus_address'], $new_msg);
        $new_msg = str_replace("{{cus_phone}}", $replace_array['cus_phone'], $new_msg);
        $new_msg = str_replace("{{vehicle_no}}", $replace_array['vehicle_no'], $new_msg);
        $new_msg = str_replace("{{lr_no}}", $replace_array['lr_no'], $new_msg);
        return $new_msg;
    }
   

}

?>
