<?php

$debug = "";
include('ReplacePlaceHolder.php');

if (isset($_REQUEST['no'])) {
    $no = $_REQUEST['no'];
} else if (isset($_REQUEST['sales_order_no'])) {
    $sales_orde_no = $_REQUEST['sales_order_no'];
    include('ReplacePlaceHolderForSales.php');
} else if (isset($_REQUEST['sales_quote_no'])) {
    $sales_quote_no = $_REQUEST['sales_quote_no'];
    include('ReplacePlaceHolderForSalesQuote.php');
} else if (isset($_REQUEST['dl_no'])) {
    $dl_no = $_REQUEST['dl_no'];
    include('ReplacePlaceHolderForDelivery.php');
} else if (isset($_REQUEST['emp_adv_no'])) {
    $emp_adv_no = $_REQUEST['emp_adv_no'];
    include('ReplacePlaceForAdvanceVoucher.php');
} else if (isset($_REQUEST['purchase_order_no'])) {
    $purchase_order_no = $_REQUEST['purchase_order_no'];
    include('ReplacePlaceHolderForPurchaseOrder.php');
} else if (isset($_REQUEST['sales_receipt_no'])) {
    $sales_receipt_no = $_REQUEST['sales_receipt_no'];
    include('ReplacePlaceHolderForSalesReceipt.php');
} else if (isset($_REQUEST['purchase_payment_no'])) {
    $purchase_payment_no = $_REQUEST['purchase_payment_no'];
    include('ReplacePlaceHolderForPurchaseReceipt.php');
} else if (isset($_REQUEST['grn_no'])) {
    $grn_no = $_REQUEST['grn_no'];
    include('ReplacePlaceHolderForGrnReport.php');
} else if (isset($_REQUEST['exp_vocher_no'])) {
    $exp_vocher_no = $_REQUEST['exp_vocher_no'];
    include('ReplacePlaceHolderForExpensesReceipt.php');
}else if ((isset($_REQUEST['from_date']) && (isset($_REQUEST['to_date'])))) {
    $from_date = $_REQUEST['from_date'];
    $to_date = $_REQUEST['to_date'];

    if (isset($_REQUEST['type'])) {
        $type = $_REQUEST['type'];
        if ($type == 'esi')
            include('ReplacePlaceHolderForEsiReport.php');
        else if ($type == 'pf')
            include('ReplacePlaceHolderForPfReport.php');
        else if ($type == 'advance')
            include('ReplacePlaceHolderForAdvanceReport.php');
    }
}else if (isset($_REQUEST['adv_sales_receipt_no'])) {
    $adv_sales_receipt_no = $_REQUEST['adv_sales_receipt_no'];
    include('ReplacePlaceHolderForSalesAdvanceReceipt.php');
}else if (isset($_REQUEST['adv_purchase_payment_no'])) {
    $adv_purchase_payment_no = $_REQUEST['adv_purchase_payment_no'];
    include('ReplacePlaceHolderForAdvancePurchaseReceipt.php');
}

if (isset($_REQUEST['debug']))
    $debug = $_REQUEST['debug'];

if (isset($_REQUEST['no'])) {
    $replace = new ReplacePlaceHolder;
    $replace_msg = $replace->placeHolderFun($no, 'for_print', $debug, '', '');
} else if (isset($_REQUEST['sales_order_no'])) {
    $replace = new ReplacePlaceHolderForSales;
    $replace_msg = $replace->placeHolderFun($sales_orde_no, 'for_print', $debug, '', '');
} else if (isset($_REQUEST['sales_quote_no'])) {
    $replace = new ReplacePlaceHolderForSalesQuote;
    $replace_msg = $replace->placeHolderFun($sales_quote_no, 'for_print', $debug, '', '');
} else if (isset($_REQUEST['dl_no'])) {
    $replace = new ReplacePlaceHolderForDelivery;
    $replace_msg = $replace->placeHolderFun($dl_no, 'for_print', $debug, '', '');
} else if (isset($_REQUEST['emp_adv_no'])) {
    $replace = new ReplacePlaceHolderForAdvanceVoucher;
    $replace_msg = $replace->placeHolderFun($emp_adv_no, 'for_print', $debug, '', '');
} else if (isset($_REQUEST['purchase_order_no'])) {
    $replace = new ReplacePlaceHolderForPurchaseOrder;
    $replace_msg = $replace->placeHolderFun($purchase_order_no, 'for_print', $debug, '', '');
} else if (isset($_REQUEST['sales_receipt_no'])) {
    $replace = new ReplacePlaceHolderForSalesReceipt;
    $replace_msg = $replace->placeHolderFun($sales_receipt_no, 'for_print', $debug, '', '');
} else if (isset($_REQUEST['purchase_payment_no'])) {
    $replace = new ReplacePlaceHolderForPurchaseReceipt;
    $replace_msg = $replace->placeHolderFun($purchase_payment_no, 'for_print', $debug, '', '');
} else if (isset($_REQUEST['grn_no'])) {
    $replace = new ReplacePlaceHolderForGrnReport;
    $replace_msg = $replace->placeHolderFun($grn_no, 'for_print', $debug, '', '');
}else if (isset($_REQUEST['exp_vocher_no'])) {
    $replace = new ReplacePlaceHolderForExpensesReceipt;
    $replace_msg = $replace->placeHolderFun($exp_vocher_no, 'for_print', $debug, '', '');
}else if ((isset($_REQUEST['from_date']) && (isset($_REQUEST['to_date'])))) {
    if (isset($_REQUEST['type'])) {
        $type = $_REQUEST['type'];
        if ($type == 'esi') {
            $replace = new ReplacePlaceHolderForEsiReport;
            $replace_msg = $replace->placeHolderFun('', 'for_print', $debug, $from_date, $to_date);
        } else if ($type == 'pf') {

            $replace = new ReplacePlaceHolderForPfReport;
            $replace_msg = $replace->placeHolderFun('', 'for_print', $debug, $from_date, $to_date);
        } else if ($type == 'advance') {
            $emp_no = $_REQUEST['emp_no'];
            $replace = new ReplacePlaceHolderForAdvanceReport;
            $replace_msg = $replace->placeHolderFun($emp_no, 'for_print', $debug, $from_date, $to_date);
        }
    }
} else if (isset($_REQUEST['adv_sales_receipt_no'])) {
    $replace = new ReplacePlaceHolderForSalesAdvanceReceipt;
    $replace_msg = $replace->placeHolderFun($adv_sales_receipt_no, 'for_print', $debug, '', '');
}else if (isset($_REQUEST['adv_purchase_payment_no'])) {
    $replace = new ReplacePlaceHolderForAdvancePurchaseReceipt;
    $replace_msg = $replace->placeHolderFun($adv_purchase_payment_no, 'for_print', $debug, '', '');
}

if (isset($_REQUEST['debug'])) {
    echo "<pre>";
    print_r($replace_msg);
    echo "</pre>";
} ELSE
    print_r($replace_msg);
?>
