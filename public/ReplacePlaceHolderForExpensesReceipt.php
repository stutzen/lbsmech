<?php

class ReplacePlaceHolderForExpensesReceipt extends ReplacePlaceHolder {

    function getPlaceHolderValue($no, $con, $check_print_pdf, $thous_sep, $from_date, $to_date) {
        $replace_array = array();

        $bank = '';
        $addr = '';
        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'bank_detail' ");
        while ($row = mysqli_fetch_array($result))
            $bank = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'pay_to_address'");
        while ($row = mysqli_fetch_array($result))
            $addr = $row['value'];
        $array = explode("\n", $addr);
        $addr_no_line = $addr;

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $companyname = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $title = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'signature' ");
        while ($row = mysqli_fetch_array($result))
            $sign = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'website' ");
        while ($row = mysqli_fetch_array($result))
            $website = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'phone' ");
        while ($row = mysqli_fetch_array($result))
            $phone = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'fax' ");
        while ($row = mysqli_fetch_array($result))
            $fax = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'email' ");
        while ($row = mysqli_fetch_array($result))
            $email = $row['value'];

        $replace_array['website'] = $website;
        $replace_array['phone'] = $phone;
        $replace_array['fax'] = $fax;
        $replace_array['email'] = $email;


        $pay_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pay_det = $pay_det . "<p> " . $array[$i] . "</p>";
        }
        $array = explode("\n", $bank);
        $pank_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pank_det = $pank_det . "<p> " . $array[$i] . "</p>";
        }

        $result = mysqli_query($con, "select * from tbl_transaction where id = '" . $no . "'");
        $configureDateFormat = $this->getDateFormat($con);
        $thous_sep = $this->getThosandSeperatorFormat($con);
        $currency = $this->getCurrencySymbolNoBracket($con);
        $replace_array['currency'] = $currency;

        while ($row = mysqli_fetch_array($result)) {
            $account_id = $row['pmtcoderef'];
            $created_by = $row['created_by'];
             $date = $this->formatDate($row['transaction_date'], $configureDateFormat);

            $amount = $row['debit'];

             $commend = $row['particulars'];
            $mode = $row['payment_mode'];
             $other = '';
            $cheque = '';
             
            $word = $this->convertNumberToWord($amount);

            $amount = $this->convertNumberFormat($amount, $thous_sep);
        }

        $shipping_adr = "-";
        $billing = "-";

        $result = mysqli_query($con, "select * from tbl_accounts where id = " . $account_id);
        while ($row = mysqli_fetch_array($result)) {
            $account = $row['account'];
        }
       

         


        $result = mysqli_query($con, "select * from tbl_user where id = " . $created_by);
        while ($row = mysqli_fetch_array($result)) {
            $user_name = $row['f_name'] . "  " . $row['l_name'] . " ";
        }


        $domain = $_SERVER['SERVER_NAME'];
        $img_loc = "http:\\\\" . $domain . "\public\img\logo\\" . $domain . '.png';
        //echo $img_loc; 
        $img = "<img src=" . $img_loc . "   style=width:100px;height:50px;>";


         $replace_array['comment'] = $commend;
        $replace_array['currency'] = $currency;
        $replace_array['date'] = $date;
        $replace_array['address'] = $pay_det;
        $replace_array['bank'] = $pank_det;
        $replace_array['cus_name'] = $account;

        $replace_array['companyname'] = $companyname;
        $replace_array['title'] = $title;
         
        $replace_array['amount'] = $amount;
 
        $replace_array['mode'] = $mode;
        $replace_array['word'] = $word;
        $replace_array['user'] = $user_name;
        $replace_array['sign'] = $sign;
        $replace_array['no'] = $no;
        $replace_array['address_no_new_line'] = $addr_no_line;

        return $replace_array;
    }

    function getDesign($con, $no, $replace_array, $check_print_pdf, $thous_sep) {

        $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'expense_voucher_receipt'");
        $msg = '';
        while ($row = mysqli_fetch_array($result))
            $msg = $row['message'];
        $temp = '';
        if ($check_print_pdf == 'for_pdf') {
            $new_msg = $msg;
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $new_msg);
        }
        $replace_array = $this->getDesignStyles($no, $replace_array, $temp, $thous_sep);

        $replace_msg = $this->replacePlaceHolderValue($replace_array, $msg, $temp);
        return $replace_msg;
    }

    function getDesignStyles($no, $replace_array, $temp, $thous_sep) {

        return $replace_array;
    }

    function replacePlaceHolderValue($replace_array, $msg, $thous_sep) {



        $new_msg = str_replace("{{cus_name}}", $replace_array['cus_name'], $msg);
        $new_msg = str_replace("{{date}}", $replace_array['date'], $new_msg);
         $new_msg = str_replace("{{address}}", $replace_array['address'], $new_msg);
        $new_msg = str_replace("{{bank}}", $replace_array['bank'], $new_msg);
        $new_msg = str_replace("{{companyname}}", $replace_array['companyname'], $new_msg);


        $new_msg = str_replace("{{comment}}", $replace_array['comment'], $new_msg);
           $new_msg = str_replace("{{amount}}", $replace_array['amount'], $new_msg);

           $new_msg = str_replace("{{user}}", $replace_array['user'], $new_msg);
        $new_msg = str_replace("{{sign}}", $replace_array['sign'], $new_msg);
        $new_msg = str_replace("{{no}}", $replace_array['no'], $new_msg);
        $new_msg = str_replace("{{address_no_new_line}}", $replace_array['address_no_new_line'], $new_msg);
        $new_msg = str_replace("{{currency}}", $replace_array['currency'], $new_msg);
        $new_msg = str_replace("{{mode}}", $replace_array['mode'], $new_msg);
        $new_msg = str_replace("{{word}}", $replace_array['word'], $new_msg);

        $new_msg = str_replace("{{website}}", $replace_array['website'], $new_msg);
        $new_msg = str_replace("{{phone}}", $replace_array['phone'], $new_msg);
        $new_msg = str_replace("{{fax}}", $replace_array['fax'], $new_msg);
        $new_msg = str_replace("{{email}}", $replace_array['email'], $new_msg);


        return $new_msg;
    }

}

?>
