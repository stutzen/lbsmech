<?php

class ReplacePlaceHolderForAdvanceReport extends ReplacePlaceHolder {

    function getPlaceHolderValue($no, $con, $check_print_pdf, $thous_sep, $from_date, $to_date) {
        $replace_array = array();

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $companyname = $row['value'];
        $replace_array['companyname'] = $companyname;
        $bank = '';
        $addr = '';

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'pay_to_address'");
        while ($row = mysqli_fetch_array($result))
            $addr = $row['value'];
        $array = explode("\n", $addr);
        $pay_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pay_det = $pay_det . "<p> " . $array[$i] . "</p>";
        }

        $date = date('Y-m-d h:i:s A');

        $replace_array['address'] = $pay_det;
        $replace_array['date'] = $date;
        $configureDateFormat = $this->getDateFormat($con);
        $replace_array['emp_adv'] = array();
        $configureDateFormat = $this->getDateFormat($con);

        $grand_balance = 0;
        $grand_tot_debit = 0;
        $grand_tot_credit = 0;

//        $result = mysqli_query($con, "select  emp_code,f_name,l_name from tbl_employee    where  id=" . $no);
//        while ($row = mysqli_fetch_array($result)) {
//
//            $replace_array['emp_code'] = $row['emp_code'];
//            $replace_array['emp_name'] = $row['f_name'] . " " . $row['l_name'];
//        }
//        $result = mysqli_query($con, "select  sum(balance_amt) as balance  from tbl_employee_advance  where date  < '" . $from_date . "'   and emp_id=" . $no);
//        while ($row = mysqli_fetch_array($result)) {
//
//            $open_balance = $row['balance'];
//            $open_balance = $this->convertNumberFormat($open_balance, $thous_sep);
//            $replace_array['open_balance'] = $open_balance;
//        }


        $employee_advance_array = array();

        IF ($no == '')
            $result1 = mysqli_query($con, "select  * from tbl_employee_advance  where   date  >= '" . $from_date . "' and date  <='" . $to_date . "' and is_active=1 group by emp_id");
        else
            $result1 = mysqli_query($con, "select  * from tbl_employee_advance  where   date  >= '" . $from_date . "' and date  <='" . $to_date . "' and emp_id = " . $no . " and is_active=1");

        while ($row1 = mysqli_fetch_array($result1)) {
            //For Credit
            $emp_id = $row1['emp_id'];
            $employee_det = array();
            $employee_full_det['emp_det'] = array();

            $get_emp_det = mysqli_query($con, "select  emp_code,f_name,l_name from tbl_employee    where  id=" . $emp_id);
            while ($emp_det = mysqli_fetch_array($get_emp_det)) {

                $emp_code = $emp_det['emp_code'];
                $emp_name = $emp_det['f_name'] . " " . $row['l_name'];
                array_push($employee_det, $emp_det);
            }

            $employee_full_det['emp_det'] = $employee_det;

            //array_push($employee_advance_array, $employee_det);
            $employee_full_det['open'] = array();
            $get_emp_openbalance = mysqli_query($con, "select  sum(balance_amt) as balance  from tbl_employee_advance  where date  < '" . $from_date . "'   and emp_id=" . $emp_id);
            while ($emp_balance = mysqli_fetch_array($get_emp_openbalance)) {

                $open_balance = $emp_balance['balance'];
                $employee_full_det ['open'] = $open_balance;
            }
            // array_push($employee_advance_array, $open_balance);

            $result = mysqli_query($con, "SELECT X.*
  FROM ( SELECT  `date` FROM tbl_emp_adv_deduction        	
        	where emp_id =" . $emp_id . " and date >= '" . $from_date . "' and date <='" . $to_date . "'  
           UNION
          SELECT  `date`
            FROM tbl_employee_advance
        	where emp_id =" . $emp_id . " and date >= '" . $from_date . "' and date <='" . $to_date . "'  
         ) X  
ORDER BY `X`.`date`  ASC");

            $emp_adv_det = array();
            $employee_full_det['advance_det'] = array();


            $tot_debit = 0;
            $tot_credit = 0;


            while ($row = mysqli_fetch_array($result)) {

                $date_check = $row['date'];
                $result2 = mysqli_query($con, "select  * from tbl_employee_advance  where date  = '" . $date_check . "' and emp_id=" . $emp_id . " and is_active=1");
                while ($row2 = mysqli_fetch_array($result2)) {
                    //For Credit
                    $amount = $row2['adv_amount'];
                    $tot_credit = $tot_credit + $amount;


                    //For Debit

                    $zero = 0;
                    $zero = $this->convertNumberFormat($zero, $thous_sep);
                    $emp_adv['debit'] = $zero;

                    $open_balance = $open_balance + $amount;
                     $amount = $this->convertNumberFormat($amount, $thous_sep);
                    $emp_adv['credit'] = $amount;
                    $open_balance1 = $this->convertNumberFormat($open_balance, $thous_sep);
                    $emp_adv['balance'] = $open_balance1;


                    $emp_adv['adv_date'] = $row2['date'];
                    $emp_adv['id'] = "Adv-Amt: " . $row2['id'];
                    array_push($emp_adv_det, $emp_adv);
                }
                $result2 = mysqli_query($con, "select *  from tbl_emp_adv_deduction where date  = '" . $date_check . "' and emp_id=" . $emp_id . " and is_active=1");
                while ($row2 = mysqli_fetch_array($result2)) {
                    //For Credit
                    $amount = $row2['deduction_amt'];
                    $tot_debit = $tot_debit + $amount;
                    $amount = $this->convertNumberFormat($amount, $thous_sep);
                    $emp_adv['debit'] = $amount;

                    //For Debit

                    $zero = 0;
                    $zero = $this->convertNumberFormat($zero, $thous_sep);
                    $emp_adv['credit'] = $zero;
                    $open_balance = $open_balance - $amount;
                     $open_balance1 = $this->convertNumberFormat($open_balance, $thous_sep);
                    $emp_adv['balance'] = $open_balance1;
                    $emp_adv['adv_date'] = $row2['date'];

                    $emp_adv['id'] = "Adv-Ded : " . $row2['id'];

                    array_push($emp_adv_det, $emp_adv);
                }

                $employee_full_det['advance_det'] = $emp_adv_det;
            }
            

            $emp_tot = array();
            $employee_det1 = array();
            
            
            $grand_tot_debit = $grand_tot_debit + $tot_debit;
            $grand_tot_credit = $grand_tot_credit + $tot_credit;

            $tot_credit = $this->convertNumberFormat($tot_credit, $thous_sep);
            $emp_tot['tot_credit'] = $tot_credit;
            $tot_debit = $this->convertNumberFormat($tot_debit, $thous_sep);
            $emp_tot['tot_debit'] = $tot_debit;

            $result3 = mysqli_query($con, "select  sum(balance_amt)  as balance  from tbl_employee_advance  where date  < '" . $to_date . "'   and emp_id=" . $emp_id . " and is_active=1");
            while ($row3 = mysqli_fetch_array($result3)) {

                $balance = $row3['balance'];
                $grand_balance = $grand_balance + $balance;
                 $balance = $this->convertNumberFormat($balance, $thous_sep);

                $emp_tot['tot_balance'] = $balance;
            }
            array_push($employee_det1, $emp_tot);
            $employee_full_det['total_array'] = $employee_det1;

            array_push($employee_advance_array, $employee_full_det);
        }


        $grand_tot_debit = $this->convertNumberFormat($grand_tot_debit, $thous_sep);
        $grand_balance = $this->convertNumberFormat($grand_balance, $thous_sep);

        $grand_tot_credit = $this->convertNumberFormat($grand_tot_credit, $thous_sep);
        

        $replace_array['grand_credit'] = $grand_tot_credit;
        $replace_array['grand_debit'] = $grand_tot_debit;
        $replace_array['grand_balance'] = $grand_balance;
        
        $replace_array['emp_full_array'] = $employee_advance_array;

        $currency = $this->getCurrencySymbol($con);
        $replace_array['currency'] = $currency;
        $replace_array['no'] = $no;

        $replace_array['company'] = $companyname;
        $replace_array['address'] = $pay_det;

        $replace_array ['from_date'] = $from_date;
        $replace_array['to_date'] = $to_date;


        return $replace_array;
    }

    function getDesign($con, $no, $replace_array, $check_print_pdf, $thous_sep) {

        $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'employee_advance_template'");
        while ($row = mysqli_fetch_array($result))
            $msg = $row['message'];
        if ($check_print_pdf == 'for_pdf') {
            $new_msg = $msg;
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $new_msg);
        }

        $replace_msg = $this->replacePlaceHolderValue($replace_array, $msg, $thous_sep);
        return $replace_msg;
    }

    function replacePlaceHolderValue($replace_array, $msg, $thous_sep) {


        $adv_detail = $replace_array['emp_adv'];

        $employee_full_detail = $replace_array['emp_full_array'];


        $extract_string_full = $this->extractString($msg, '<!--employee-row-start-->', '<!--employee-row-end-->');

        $copy_string_ful = $extract_string_full;

        $ful_emp_det = '';
        foreach ($employee_full_detail as $emp_det) {
            $emp_basic_det = $emp_det['emp_det'];
            foreach ($emp_basic_det as $emp) {
                $extract_string_full = str_replace("{{emp_code}}", $emp['emp_code'], $extract_string_full);
                $extract_string_full = str_replace("{{emp_name}}", $emp['f_name'], $extract_string_full);
                $extract_string_full = str_replace("{{l_name}}", $emp['l_name'], $extract_string_full);
            }
            $emp_total_det = $emp_det['total_array'];

            //  var_dump($emp_total_det);
            foreach ($emp_total_det as $emp_tot) {
                $extract_string_full = str_replace("{{tot_credit}}", $emp_tot['tot_credit'], $extract_string_full);
                $extract_string_full = str_replace("{{tot_balance}}", $emp_tot['tot_balance'], $extract_string_full);
                $extract_string_full = str_replace("{{tot_debit}}", $emp_tot['tot_debit'], $extract_string_full);
            }
            $extract_string_full = str_replace("{{open_balance}}", $emp_det['open'], $extract_string_full);

            $extract_string = $this->extractString($extract_string_full, '<!--advance-row-start-->', '<!--advance-row-end-->');
            $adv_detail = $emp_det['advance_det'];

            $copy_string = $extract_string;
            $count = 0;
            $adv_det = "";
            foreach ($adv_detail as $adv) {
                $count = $count + 1;
                $extract_string = str_replace("{{sno}}", $count, $extract_string);
                $extract_string = str_replace("{{debit}}", $adv['debit'], $extract_string);
                $extract_string = str_replace("{{credit}}", $adv['credit'], $extract_string);
                $extract_string = str_replace("{{id}}", $adv['id'], $extract_string);
                $extract_string = str_replace("{{balance}}", $adv['balance'], $extract_string);
                $extract_string = str_replace("{{adv_date}}", $adv['adv_date'], $extract_string);

                $adv_det = $adv_det . $extract_string;
                $extract_string = $copy_string;
            }

            $extract_string_full = str_replace("<!--advance-row-start-->" . $extract_string . "<!--advance-row-end-->", $adv_det, $extract_string_full);


            $ful_emp_det = $ful_emp_det . $extract_string_full;
            $extract_string_full = $copy_string_ful;
        }

        $msg = str_replace("<!--employee-row-start-->" . $extract_string_full . "<!--employee-row-end-->", $ful_emp_det, $msg);

        $new_msg = str_replace("{{company}}", $replace_array['companyname'], $msg);
        $new_msg = str_replace("{{address}}", $replace_array['address'], $new_msg);

        $new_msg = str_replace("{{from_date}}", $replace_array['from_date'], $new_msg);
        $new_msg = str_replace("{{date}}", $replace_array['date'], $new_msg);
        $new_msg = str_replace("{{to_date}}", $replace_array['to_date'], $new_msg);
        
        $new_msg = str_replace("{{grand_debit}}", $replace_array['grand_debit'], $new_msg);
        $new_msg = str_replace("{{grand_credit}}", $replace_array['grand_credit'], $new_msg);
        $new_msg = str_replace("{{grand_balance}}", $replace_array['grand_balance'], $new_msg);



        return $new_msg;
    }

}

?>
