'use strict';

/**
 * A directive for adding google places autocomplete to a text box
 * google places autocomplete info: https://developers.google.com/maps/documentation/javascript/places
 *
 * Simple Usage:
 *
 * <input type="text" ng-autocomplete="result"/>
 *
 * creates the autocomplete text box and gives you access to the result
 *
 *   + `ng-autocomplete="result"`: specifies the directive, $scope.result will hold the textbox result
 *
 *
 * Advanced Usage:
 *
 * <input type="text" ng-autocomplete="result" details="details" options="options"/>
 *
 *   + `ng-autocomplete="result"`: specifies the directive, $scope.result will hold the textbox autocomplete result
 *
 *   + `details="details"`: $scope.details will hold the autocomplete's more detailed result; latlng. address components, etc.
 *
 *   + `options="options"`: options provided by the user that filter the autocomplete results
 *
 *      + options = {
 *           types: type,        string, values can be 'geocode', 'establishment', '(regions)', or '(cities)'
 *           bounds: bounds,     google maps LatLngBounds Object
 *           country: country    string, ISO 3166-1 Alpha-2 compatible country code. examples; 'ca', 'us', 'gb'
 *         }
 *
 *
 */

angular.module("ngMapAutocomplete", [])
        .directive('ngMapAutocomplete', ['$searchService', '$parse', function($searchService, $parse) {
                return {
                    scope: {
                        details: '=',
                        ngAutocomplete: '=',
                        options: '=',
                        ngModel: '='
                    },
                    require: 'ngModel',
                    link: function(scope, element, attrs, controller) {

                        scope.searchService = $searchService;
                        console.log('ng init = syedaa');
                        //options for autocomplete
                        var opts;

                        //convert options provided to opts
                        var initOpts = function() {
                            opts = {}
                            if (scope.options) {
                                if (scope.options.types) {
                                    opts.types = []
                                    opts.types.push(scope.options.types)
                                }
                                if (scope.options.bounds) {
                                    opts.bounds = scope.options.bounds
                                }
                                if (scope.options.country) {
                                    opts.componentRestrictions = {
                                        country: scope.options.country
                                    }
                                }
                            }
                        }
                        initOpts();

                        //create new autocomplete
                        //reinitializes on every change of the options provided
                        var newAutocomplete = function() {
                            scope.gPlace = new google.maps.places.Autocomplete(element[0], opts);
                            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                                scope.$apply(function() {


                                    var details = scope.gPlace.getPlace();
                                    scope.searchService.searchUIModel.selectedGeoAddressDetail = details;
                                    
                                    if (details != null && typeof details.formatted_address !== 'undefined')
                                    {
                                        scope.details = details.formatted_address;
                                    }
                                    else
                                    {
                                        scope.details = '';
                                    }
                                    //ngModelCtrl.$setViewValue(scope.details);
                                    scope.ngModel = scope.details;
                                    console.log('scope.model');
                                    console.log(scope.ngModel);
//              }   
                                    scope.ngAutocomplete = element.val();
                                    scope.$root.$broadcast('updateMapLocationEvent', scope.gPlace.getPlace());
                                });
                            })
                        }
                        newAutocomplete();

                        //watch options provided to directive
                        scope.watchOptions = function() {
                            return scope.options;
                        };


                        controller.$render = function() {
                            var location = controller.$viewValue;
                            element.val(location);

                        };



                    }
                }
                ;
            }
        ]);