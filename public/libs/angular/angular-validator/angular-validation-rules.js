 app.factory('ValidationFactory', function () {

       var validationFactory = {};

       validationFactory.email = function(email){

             if(!email){return;}
             //var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            // ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"
             var re = /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i; 
             return re.test(email);
 
       }
       validationFactory.international_phone = function(phoneNumber){

             if(!phoneNumber){return;}
             var re = /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/
             
             if(!re.test(phoneNumber))
             {
                 return "Please enter a valid phone number. For ex 213-456-7890."
             }
             return true;
       }
       
       
       validationFactory.password = function(password) {

		if(!password){return;}
		
		if (password.length < 6) {
			return "Password must be at least " + 6 + " characters long";
		}

//		if (!password.match(/[A-Z]/)) {
//			 return "Password must have at least one capital letter";
//		}
//
//		if (!password.match(/[0-9]/)) {
//			 return "Password must have at least one number";
//		}

		return true;
	};



    return validationFactory;

    })