<?php

class ReplacePlaceHolderForPfReport extends ReplacePlaceHolder {

    function getPlaceHolderValue($no, $con, $check_print_pdf, $thous_sep, $from_date, $to_date) {
        $replace_array = array();

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $companyname = $row['value'];
         $replace_array['companyname'] = $companyname;
        $bank = '';
        $addr = '';

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'pay_to_address'");
        while ($row = mysqli_fetch_array($result))
            $addr = $row['value'];
        $array = explode("\n", $addr);
        $pay_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pay_det = $pay_det . "<p> " . $array[$i] . "</p>";
        }
        
        $date = date('Y-m-d h:i:s A');

        $replace_array['address'] = $pay_det;
        $replace_array['date'] = $date;
        $configureDateFormat = $this->getDateFormat($con);
        $replace_array['emp_esi'] = array();
        $configureDateFormat = $this->getDateFormat($con);
        $result = mysqli_query($con, "select es.* , e.emp_code,e.f_name,e.l_name,s.week_salary from tbl_emp_salary as s left Join tbl_employee_pf_statement as es on  s.id = es.emp_salary_id left join `tbl_employee` as `e` on `es`.`emp_id` = `e`.`id` where s.from_date  >= '" . $from_date . "' and s.to_date <='" . $to_date."'");
        while ($row = mysqli_fetch_array($result)) {

            $pf_amount = $row['pf_amount'];
            $pf_amount = $this->convertNumberFormat($pf_amount, $thous_sep);

            $salary_amount = $row['week_salary'];
            $salary_amount = $this->convertNumberFormat($salary_amount, $thous_sep);

            $emp_esi['emp_code'] = $row['emp_code'];
            $emp_esi['emp_name'] = $row['f_name'] . " " . $row['l_name'];
            $emp_esi['pf_amount'] = $pf_amount;
            $emp_esi['days'] = $row['days'];
            
            $emp_esi['percentage'] = $row['pf_percentage'];
             
            
            $emp_esi['salary'] = $salary_amount;

            array_push($replace_array['emp_esi'], $emp_esi);
        }



        $result = mysqli_query($con, "select sum(s.week_salary) as salary_amount ,sum(es.pf_amount) as pf_amount,sum(es.pf_percentage) as total_percentage from tbl_emp_salary as s left Join tbl_employee_pf_statement as es on  s.id = es.emp_salary_id   where s.from_date  >= '" . $from_date . "' and s.to_date <='" . $to_date."'");
        while ($row = mysqli_fetch_array($result)) {
            $replace_array['total'] = $row['salary_amount'];
            $replace_array['pf_total'] = $row['pf_amount'];
            $replace_array['total_percentage'] = $row['total_percentage'];
        }
        $currency = $this->getCurrencySymbol($con);
        $replace_array['currency'] = $currency;
        $replace_array['no'] = $no;

        $replace_array['company'] = $companyname;
        $replace_array['address'] = $pay_det;

        $replace_array['from_date'] = $from_date;
        $replace_array['to_date'] = $to_date;


        return $replace_array;
    }

    function getDesign($con, $no, $replace_array, $check_print_pdf, $thous_sep) {

        $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'employee_pf_template'");
        while ($row = mysqli_fetch_array($result))
            $msg = $row['message'];
        if ($check_print_pdf == 'for_pdf') {
            $new_msg = $msg;
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $new_msg);
        }

        $replace_msg = $this->replacePlaceHolderValue($replace_array, $msg, $thous_sep);
        return $replace_msg;
    }

    function replacePlaceHolderValue($replace_array, $msg, $thous_sep) {


        $esi_detail = $replace_array['emp_esi'];

        $extract_string = $this->extractString($msg, '<!--pf-row-start-->', '<!--pf-row-end-->');

        $copy_string = $extract_string;
        $count = 0;
        $esi_det = "";
        foreach ($esi_detail as $esi) {
            $count = $count + 1;
            $extract_string = str_replace("{{sno}}", $count, $extract_string);
            $extract_string = str_replace("{{emp_code}}", $esi['emp_code'], $extract_string);
            $extract_string = str_replace("{{emp_name}}", $esi['emp_name'], $extract_string);
            $extract_string = str_replace("{{days}}", $esi['days'], $extract_string);
            $extract_string = str_replace("{{salary}}", $esi['salary'], $extract_string);
            $extract_string = str_replace("{{percentage}}", $esi['percentage'], $extract_string);
            $extract_string = str_replace("{{pf_amount}}", $esi['pf_amount'], $extract_string); 

            $esi_det = $esi_det . $extract_string;
            $extract_string = $copy_string;
        }

        $msg = str_replace("<!--pf-row-start-->$extract_string<!--pf-row-end-->", $esi_det, $msg);

        $new_msg = str_replace("{{company}}", $replace_array['companyname'], $msg);
        $new_msg = str_replace("{{address}}", $replace_array['address'], $new_msg);

        $new_msg = str_replace("{{from_date}}", $replace_array['from_date'], $new_msg);
        $new_msg = str_replace("{{date}}", $replace_array['date'], $new_msg);
        $new_msg = str_replace("{{to_date}}", $replace_array['to_date'], $new_msg);
        
        $new_msg = str_replace("{{total}}", $replace_array['total'], $new_msg);
         
        $new_msg = str_replace("{{pf_total}}", $replace_array['pf_total'], $new_msg);
        $new_msg = str_replace("{{total_percentage}}", $replace_array['total_percentage'], $new_msg);

        

        return $new_msg;
    }

}

?>
