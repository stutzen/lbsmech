<?php

$currentWorkingDir = getcwd();

$currentDir = substr($currentWorkingDir, 0, strlen($currentWorkingDir) - 6);
$envArray = parse_ini_file($currentDir . '.env');

$sso_server = $envArray['DB_HOST'];
$sso_db_username = $envArray['SSO_DB_USERNAME'];
$sso_db_password = $envArray['SSO_DB_PASSWORD'];
$sso_db_name = $envArray['SSO_DB_DATABASE'];
$app_user_name = '';
$domain = $_SERVER['SERVER_NAME'];

$lbs_server = '';
$lbs_db_username = '';
$lbs_db_password = '';
$lbs_db_name = '';

try {
    $sso_conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
} catch (PDOException $e) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "select db_host,db_name, db_username,db_password from tbl_bu where name ='" . $domain . "'";
if ($res = $sso_conn->query($sql)) {

    /* Check the number of rows that match the SELECT statement */
    if ($res->rowCount() > 0) {

        foreach ($res as $row) {

            $lbs_server = $row["db_host"];
            $lbs_db_username = $row["db_username"];
            $lbs_db_password = $row["db_password"];
            $lbs_db_name = $row["db_name"];
        }
    } else {

        die("Connection failed: " . $conn->connect_error);
    }
}
$con = mysqli_connect($lbs_server, $lbs_db_username, $lbs_db_password, $lbs_db_name);

// Check connection
if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
}

$file = fopen('D:/invoice4.csv', "r");
$count = 0;
$date = date('Y/m/d H:i:s');
$pro_qry = '';
$result = mysqli_query($con, "select id from  tbl_product ");
$pid=0;
while ($row = mysqli_fetch_array($result))
    $pid = $row['id'];
$pro_name = array();
$dup_count = 0;

$inv_qry = '';
while (($getData = fgetcsv($file, 1000, ",")) !== FALSE) {
   
     $search_key = array_search($getData[0], $pro_name);
     if ($search_key == 1)
        $dup_count = $dup_count + 1;
    if ($count > 0) {
         $pid = $pid + 1;
        if ($pro_qry == '')
            $pro_qry = $pro_qry . "INSERT INTO `tbl_product`( `name`, `uom`, `uom_id`, `sales_price`, `type`, `description`, `sku`, `category_id`, `is_active`, `is_sale`, `is_purchase`, `has_inventory`, `min_stock_qty`, `created_by`, `updated_by`, `updated_at`, `created_at`) VALUES ('" . $getData[0] . "','" . $getData[1] . "','" . $getData[2] . "','" . $getData[3] . "','" . $getData[4] . "','" . $getData[5] . "','" . $getData[6] . "','" . $getData[7] . "','" . $getData[8] . "','" . $getData[9] . "','" . $getData[10] . "','" . $getData[11] . "','" . $getData[12] . "','1','1','" . $date . "','" . $date . "')";
        else
            $pro_qry = $pro_qry . ", ('" . $getData[0] . "','" . $getData[1] . "','" . $getData[2] . "','" . $getData[3] . "','" . $getData[4] . "','" . $getData[5] . "','" . $getData[6] . "','" . $getData[7] . "','" . $getData[8] . "','" . $getData[9] . "','" . $getData[10] . "','" . $getData[11] . "','" . $getData[12] . "','1','1','" . $date . "','" . $date . "')";
        if ($getData[11] == 1) {
            if ($inv_qry == '')
                $inv_qry = $inv_qry . "INSERT INTO `tbl_inventory`( `product_id`, `uom_id`, `uom_name`, `sku`, `quantity`, `is_active`, `created_by`, `updated_by` , `created_at`, `updated_at`) VALUES ('" . $pid . "','" . $getData[2] . "','" . $getData[1] . "','" . $getData[6] . "','" . $getData[12] . "','" . $getData[8] . "' ,'1','1','" . $date . "','" . $date . "')";
            else
                $inv_qry = $inv_qry . ", ('" . $pid . "','" . $getData[2] . "','" . $getData[1] . "','" . $getData[6] . "','" . $getData[12] . "','" . $getData[8] . "' ,'1','1','" . $date . "','" . $date . "')";
        }
    }
    array_push($pro_name, $getData[0]);
            $count = $count + 1;
}
 fclose($file);
if ($con->multi_query($inv_qry) == TRUE) {
    echo "  ";
        }
if ($con->multi_query($pro_qry) == TRUE) {
    echo "  {
       success: true,<br>
       message :  'Product Imported',<br>
       total_no_product_import:" . ($count - 1) . ",<br>
       total_duplicate_no_product :" . $dup_count . "<br>
  }";
}
?>

