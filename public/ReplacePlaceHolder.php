<?php

class ReplacePlaceHolder {

    public function placeHolderFun($no, $check_print_pdf, $debug, $from_date, $to_date) {
        $currentWorkingDir = getcwd();
        $currentDir = substr($currentWorkingDir, 0, strlen($currentWorkingDir) - 6);
        $envArray = parse_ini_file($currentDir . '.env');

        $sso_server = $envArray['DB_HOST'];
        $sso_db_username = $envArray['SSO_DB_USERNAME'];
        $sso_db_password = $envArray['SSO_DB_PASSWORD'];
        $sso_db_name = $envArray['SSO_DB_DATABASE'];
        $app_user_name = '';
        $domain = $_SERVER['SERVER_NAME'];

        $lbs_server = '';
        $lbs_db_username = '';
        $lbs_db_password = '';
        $lbs_db_name = '';

        try {
            $sso_conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
        } catch (PDOException $e) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "select db_host,db_name, db_username,db_password from tbl_bu where name ='" . $domain . "'";
        if ($res = $sso_conn->query($sql)) {
            if ($res->rowCount() > 0) {

                foreach ($res as $row) {

                    $lbs_server = $row["db_host"];
                    $lbs_db_username = $row["db_username"];
                    $lbs_db_password = $row["db_password"];
                    $lbs_db_name = $row["db_name"];
                }
            } else {

                die("Connection failed: " . $conn->connect_error);
            }
        }
        $con = mysqli_connect($lbs_server, $lbs_db_username, $lbs_db_password, $lbs_db_name);

// Check connection
        if ($con->connect_error) {
            die("Connection failed: " . $con->connect_error);
        }

        $replace_array = array();
        $thous_sep = $this->getThosandSeperatorFormat($con);
        $replace_array = $this->getPlaceHolderValue($no, $con, $check_print_pdf, $thous_sep, $from_date, $to_date);
        $replace_msg = $this->getDesign($con, $no, $replace_array, $check_print_pdf, $thous_sep);
        if (!empty($debug)) {
            $replace_msg = $replace_msg . " " . htmlentities($replace_msg);
            /*  foreach ($replace_array as $key => $value) {
              if (is_array($value)) {
              foreach ($value as $key1 => $value1) {
              echo "$key1  ->  $value1" . "<br>";
              }
              } else {
              echo "$key  ->  $value" . "<br>";
              }
              } */
            echo "<pre>";
            print_r($replace_array);
            echo "</pre>";
        }
        return $replace_msg;
    }

    function getPlaceHolderValue($no, $con, $check_print_pdf, $thous_sep, $from_date, $to_date) {
        $replace_array = array();
        $result = mysqli_query($con, "select * from tbl_invoice");
        if (!$result) {
            printf("Error: %s\n", mysqli_error($con));
            exit();
        }
        $bank = '';
        $addr = '';
        $companyname = '';

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'bank_detail' ");
        while ($row = mysqli_fetch_array($result))
            $bank = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $companyname = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'app_title' ");
        while ($row = mysqli_fetch_array($result))
            $title = $row['value'];

        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'pay_to_address'");
        while ($row = mysqli_fetch_array($result))
            $addr = $row['value'];
        $array = explode("\n", $addr);
        $pay_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pay_det = $pay_det . "<p> " . $array[$i] . "</p>";
        }
        $array = explode("\n", $bank);
        $pank_det = "";
        for ($i = 0; $i < count($array); $i++) {
            $pank_det = $pank_det . "<p> " . $array[$i] . "</p>";
        }

        $result = mysqli_query($con, "select * from tbl_invoice where id = '" . $no . "'");
        $configureDateFormat = $this->getDateFormat($con);

        $currency = $this->getCurrencySymbol($con);

        while ($row = mysqli_fetch_array($result)) {
            $customer_id = $row['customer_id'];
            $replace_array['customer_id'] = $customer_id;
            $date = $this->formatDate($row['date'], $configureDateFormat);
            $duedate = $this->formatDate($row['duedate'], $configureDateFormat);
            $total_amount = $row['total_amount'];
            $new_tota1 = $row['total_amount'];
            $subtotal = $row['subtotal'];
            $sales = $row['quote_id'];

            $cus_addr = $row['customer_address'];
            $tax = $row['tax_amount'];
            $packing = $row['packing_charge'];
            $insurance = $row['insurance_charge'];
            $discount = $row['discount_amount'];
            $discount_percentage = $row['discount_percentage'];
            $discount_mode = $row['discount_mode'];
            $new_total_amount = ( $subtotal + $tax + $packing + $insurance) - $discount;
            $gross_total = $subtotal - $discount;

            $discount = $this->convertNumberFormat($discount, $thous_sep);
            $paid_amount = $row['paid_amount'];
            $invoice_no = $row['invoice_no'];
            $no1 = $row['invoice_code'];
            $status = $row['status'];
            $balance = $total_amount - $paid_amount;
            $paid_amount = $this->convertNumberFormat($paid_amount, $thous_sep);
            $tax = $this->convertNumberFormat($tax, $thous_sep);
            $payee_id = $row['payee'];
            $consignee_id = $row['consignee'];
        }
        $payee = " - ";
        $consignee = " - ";
        $shipping_adr = "-";
        $billing = "-";


        $payee_phone = "-";
        $payee_email = "-";
        $payee_ship_adr = "-";
        $payee_bill_adr = "-";
        $payee_pin = "-";

        $result = mysqli_query($con, "select * from tbl_customer where id = " . $payee_id);
        while ($row = mysqli_fetch_array($result)) {
            $payee = $row['fname'] . "  " . $row['lname'];
            $payee_phone = $row['phone'];
            $payee_email = $row['email'];
            $payee_ship_adr = $row['shopping_address'];
            $payee_bill_adr = $row['billing_address'];
            $payee_pin = $row['zip'];
        }

        $array = explode("\n", $payee_ship_adr);
        $payee_ship_address = "";
        for ($i = 0; $i < count($array); $i++) {
            $payee_ship_address = $payee_ship_address . "<p style=margin-left:100px;>    " . $array[$i] . "</p>";
        }
        $array = explode("\n", $payee_bill_adr);
        $payee_bill_address = "";
        for ($i = 0; $i < count($array); $i++) {
            $payee_bill_address = $payee_bill_address . "<p style=margin-left:100px;>    " . $array[$i] . "</p>";
        }

        $replace_array['payee_phone'] = $payee_phone;
        $replace_array['payee_ship_address'] = $payee_ship_address;
        $replace_array['payee_bill_address'] = $payee_bill_address;
        $replace_array['payee_pin'] = $payee_pin;
        $replace_array['payee_email'] = $payee_email;
        $replace_array['title_type'] = '';

        $consignee = "-";

        $consignee_phone = "-";
        $consignee_email = "-";
        $consignee_ship_adr = "-";
        $consignee_bill_adr = "-";
        $consignee_pin = "-";

        $result = mysqli_query($con, "select * from tbl_customer where id = " . $consignee_id);
        while ($row = mysqli_fetch_array($result)) {
            $consignee = $row['fname'] . "  " . $row['lname'];

            $consignee_phone = $row['phone'];
            $consignee_email = $row['email'];
            $consignee_ship_adr = $row['shopping_address'];
            $consignee_bill_adr = $row['billing_address'];
            $consignee_pin = $row['zip'];
        }
        $array = explode("\n", $consignee_ship_adr);
        $consignee_ship_address = "";
        for ($i = 0; $i < count($array); $i++) {
            $consignee_ship_address = $consignee_ship_address . "<p style=margin-left:100px;>    " . $array[$i] . "</p>";
        }
        $array = explode("\n", $consignee_bill_adr);
        $consignee_bill_address = "";
        for ($i = 0; $i < count($array); $i++) {
            $consignee_bill_address = $consignee_bill_address . "<p style=margin-left:100px;>    " . $array[$i] . "</p>";
        }

        $replace_array['consignee_phone'] = $consignee_phone;
        $replace_array['consignee_ship_address'] = $consignee_ship_address;
        $replace_array['consignee_bill_address'] = $consignee_bill_address;
        $replace_array['consignee_pin'] = $consignee_pin;
        $replace_array['consignee_email'] = $consignee_email;



        $sales_order = "-";
        $result = mysqli_query($con, "select * from tbl_sales_order where id = " . $sales);
        while ($row = mysqli_fetch_array($result)) {
            $sales_order = $row['sales_order_code'];
        }
        $result = mysqli_query($con, "select * from tbl_customer where id = " . $customer_id);
        while ($row = mysqli_fetch_array($result)) {
            $cus_phone = $row['phone'];
            $cus_email = $row['email'];
            $cus_name = $row['fname'] . "  " . $row['lname'];
            $shipping_adr = $row['shopping_address'];
            $billing_adr = $row['billing_address'];
            $cus_pin = $row['zip'];
        }
        $array = explode("\n", $shipping_adr);
        $shipping = "";
        for ($i = 0; $i < count($array); $i++) {
            $shipping = $shipping . "<p style=margin-left:100px;>    " . $array[$i] . "</p>";
        }
        $array = explode("\n", $billing_adr);
        $billing = "";
        for ($i = 0; $i < count($array); $i++) {
            $billing = $billing . "<p style=margin-left:100px;>    " . $array[$i] . "</p>";
        }
        $result = mysqli_query($con, "select * from tbl_tax as t left Join tbl_invoice_tax as i on  t.id = i.tax_id where i.invoice_id  = " . $no);
        $tax_detail = "";
        $replace_array['tax_detail'] = array();
        $replace_array['tax_detail_new'] = array();
        $replace_array['tax_percen_detail'] = array();
        while ($inv_tax = mysqli_fetch_array($result)) {
            $tax_name = $inv_tax['tax_name'];
            $tax_amount = $inv_tax['tax_amount'];
            $tax_percentage = $inv_tax['tax_percentage'];
            $tax_amount = $this->convertNumberFormat($tax_amount, $thous_sep);
            $tax_detail['tax_name'] = $tax_name;
            $tax_detail['tax_amount'] = $tax_amount;
            $tax_detail['tax_percen'] = $tax_percentage;
            $tax_detail['tax_taxable'] = $tax_percentage;
            array_push($replace_array['tax_detail'], $tax_detail);
        }

        $replace_array['total_tax'] = $tax;

        $domain = $_SERVER['SERVER_NAME'];
        $img_loc = "http:\\\\" . $domain . "\public\img\logo\\" . $domain . '.png';

        $img = "<img src=" . $img_loc . "   style=width:100px;height:50px;>";

        $replace_array['invoice_no'] = $no1;
        $replace_array['payee'] = $payee;

        $replace_array['days'] = '';
        $replace_array['consignee'] = $consignee;
        $replace_array['gross_total'] = $gross_total;

        $replace_array['shipping'] = $shipping;
        $replace_array['billing'] = $billing;
        $replace_array['cus_pin'] = $cus_pin;
        $replace_array['sales_order'] = $sales_order;
        $replace_array['packing'] = $packing;
        $replace_array['insurance'] = $insurance;
        $replace_array['currency'] = $currency;
        $replace_array['date'] = $date;
        $replace_array['duedate'] = $duedate;
        $replace_array['address'] = $pay_det;
        $replace_array['companyname'] = $companyname;
        $replace_array['bank'] = $pank_det;
        $replace_array['cus_name'] = $cus_name;
        $replace_array['cus_company'] = '';
        $replace_array['phone'] = $cus_phone;
        $replace_array['title'] = $title;
        $replace_array['email'] = $cus_email;
        $replace_array['notes'] = '';
        $replace_array['com_name'] = '';
        $replace_array['com_address'] = '';

        $result = mysqli_query($con, "select * from tbl_invoice as i left Join tbl_invoice_item as c on  i.id = c.invoice_id where i.id  = " . $no . " and c.is_active=1");
        $product_det = "";
        $totalQty = 0;
        $replace_array['product_detail'] = array();
        $replace_array['product_detail_new'] = array();
        $balance = number_format((float) $balance, 2, '.', '');
        while ($row = mysqli_fetch_array($result)) {
            $pro_id = $row['product_id'];
            $qty = $row['qty'];
            $prod = $row['product_name'];

            $cus_op1 = $row['custom_opt1'];
            $cus_op2 = $row['custom_opt2'];
            $cus_op3 = $row['custom_opt3'];
            $cus_op4 = $row['custom_opt4'];
            $cus_op5 = $row['custom_opt5'];


            $price = $row['unit_price'];
            $hsn_code = $row['hsn_code'];
            $uom = $row['uom_name'];
            $pro_amt = $price * $qty;
            $totalQty += $qty;
            $price = number_format((float) $price, 2, '.', '');
            $pro_amt = number_format((float) $pro_amt, 2, '.', '');
            $price = number_format((float) $price, 2, '.', '');
            $price = $this->convertNumberFormat($price, $thous_sep);
            $pro_amt = $this->convertNumberFormat($pro_amt, $thous_sep);

            $product_det['product'] = $prod;
            $product_det['cus_op1'] = $cus_op1;
            $product_det['cus_op2'] = $cus_op2;
            $product_det['cus_op3'] = $cus_op3;
            $product_det['cus_op4'] = $cus_op4;
            $product_det['cus_op5'] = $cus_op5;

            $product_det['hsn_code'] = $hsn_code;
            $product_det['qty'] = $qty;
            $product_det['price'] = $price;
            $product_det['amount'] = $pro_amt;
            $product_det['uom'] = $uom;

            array_push($replace_array['product_detail'], $product_det);
        }

        $result = mysqli_query($con, "select hsn_code, sum(total_price) as sum_price from tbl_invoice_item  where invoice_id  = " . $no . " and is_active=1  group by hsn_code order by id");
        $product_det = array();
        $totalQty = 0;
        $replace_array['product_detail_for_hsn'] = array();

        while ($row = mysqli_fetch_array($result)) {
            $hsn_code = $row['hsn_code'];
            $price = $row['sum_price'];

            $product_det['hsn_code'] = $hsn_code;
            $product_det['price'] = $price;

            array_push($replace_array['product_detail_for_hsn'], $product_det);
        }
        $subtotal = $this->convertNumberFormat($subtotal, $thous_sep);
        $replace_array['subtotal'] = $subtotal;
        $round1 = ceil($total_amount);

        $round = number_format((float) $round1, 2, '.', '');
        $reminder = $round - $new_total_amount;
        $reminder = number_format((float) $reminder, 2, '.', '');
        $replace_array['round_off'] = $reminder;
        $total_amount = $round;
        $subtotal = $this->convertNumberFormat($subtotal, $thous_sep);
        $word = $this->convertNumberToWord($total_amount);
        $balance = $this->convertNumberFormat($balance, $thous_sep);
        $total_amount = $this->convertNumberFormat($total_amount, $thous_sep);


        if ($status == 'paid')
            $status = "Paid";
        else if ($status == 'partiallyPaid')
            $status = "Partially Paid";
        else if ($status == 'unpaid')
            $status = "Un Paid";

        $replace_array['discount'] = $discount;
        $replace_array['discount_percentage'] = $discount_percentage;
        $replace_array['discount_mode'] = $discount_mode;
        $replace_array['paid'] = $paid_amount;
        $replace_array['grand'] = $total_amount;
        $replace_array['balance'] = $balance;
        $replace_array['amount_word'] = $word;



        $replace_array['amount'] = $total_amount;
        $sign = $this->setSignature($con);
        $replace_array['sign'] = $sign;
        $replace_array['logo'] = $img;
        $pay_det = "";
        $replace_array['payment_det'] = array();
        $result2 = mysqli_query($con, "select * from tbl_payment where invoice_id ='" . $no . "'");
        while ($inv = mysqli_fetch_array($result2)) {
            $pay_date = $inv['date'];
            $pay_date = date("Y-m-d", strtotime($pay_date));
            $pay_amount = $inv['amount'];
            $pay_mode = $inv['account'];
            $pay_date = $this->formatDate($inv['date'], $configureDateFormat);
            $pay_amount = $inv['amount'];
            $pay_amount = $this->convertNumberFormat($pay_amount, $thous_sep);
            $pay_type = $inv['payment_mode'];

            $pay_det['pay_type'] = $pay_type;
            $pay_det['pay_amount'] = $pay_amount;
            $pay_det['pay_date'] = $pay_date;
            $pay_det['pay_mode'] = $pay_mode;

            array_push($replace_array['payment_det'], $pay_det);
        }




        $replace_array['total_qty'] = $totalQty;

        $replace_array['payment'] = $pay_det;
        $replace_array['status'] = $status;

        //This is Custom Attribute Changes

        $replace_array['invoice_custom_detail'] = array();
        $result = mysqli_query($con, "select * from  tbl_attribute_value as t left Join tbl_attributes as i on  t.attributes_id = i.id where t.attributetype_id =  '1' and t.refernce_id = '" . $no . "' and   i.is_show_in_invoice =  1");
        $cust_attribute = array();
        while ($attr = mysqli_fetch_array($result)) {
            $key = '{{inv_cust_attr_' . $attr['attribute_code'] . '}}';
            $value = $attr['value'];
            $cust_attribute[$key] = $value;
            if (empty($value)) {
                $value = ' - ';
            }
        }

        $result = mysqli_query($con, "select * from    tbl_attributes  where is_show_in_invoice =  1");
        $replace_cust_attribute = array();
        while ($attr = mysqli_fetch_array($result)) {
            $key = '{{inv_cust_attr_' . $attr['attribute_code'] . '}}';
            $search_key = array_search($key, $cust_attribute);
            $key_det['key'] = $key;

            if (array_key_exists($key, $cust_attribute)) {
                $key_det['value'] = $cust_attribute[$key];
            } else {
                $value = '-';
                $cust_attribute[$key] = $value;
                $key_det['value'] = $value;
            }
            array_push($replace_array['invoice_custom_detail'], $key_det);
        }

        $replace_array['customer_custom_detail'] = array();
        $result = mysqli_query($con, "select * from  tbl_attribute_value as t left Join tbl_attributes as i on  t.attributes_id = i.id where t.attributetype_id =  '2' and t.refernce_id = '" . $customer_id . "' and   i.is_show_in_invoice =  1");
        $cust_attribute_crm = array();
        while ($attr = mysqli_fetch_array($result)) {
            $key = '{{crm_cust_attr_' . $attr['attribute_code'] . '}}';
            $value = $attr['value'];
            $cust_attribute_crm[$key] = $value;
            if (empty($value)) {
                $value = ' - ';
            }
        }
        $result = mysqli_query($con, "select * from    tbl_attributes  where is_show_in_invoice =  1");
        $replace_cust_attribute = array();
        while ($attr = mysqli_fetch_array($result)) {
            $key = '{{crm_cust_attr_' . $attr['attribute_code'] . '}}';
            $search_key = array_search($key, $cust_attribute);
            $key_det['key'] = $key;

            if (array_key_exists($key, $cust_attribute_crm)) {
                $key_det['value'] = $cust_attribute_crm[$key];
            } else {
                $value = '-';
                $cust_attribute_crm[$key] = $value;
                $key_det['value'] = $value;
            }
            array_push($replace_array['customer_custom_detail'], $key_det);
        }

        return $replace_array;
    }

    function getDesign($con, $no, $replace_array, $check_print_pdf, $thous_sep) {
        $customer_id = $replace_array['customer_id'];
        $result = mysqli_query($con, "select * from  tbl_app_config where setting = 'invoice_print_template'");
        while ($row = mysqli_fetch_array($result))
            $get_pdf_format = $row['value'];
        if ($get_pdf_format == 't1') {
            $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'invoice_print_template1'");
            while ($row = mysqli_fetch_array($result))
                $msg = $row['message'];
            $temp = 't1';
        } else if ($get_pdf_format == 't2') {
            $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'invoice_print_template2'");
            while ($row = mysqli_fetch_array($result))
                $msg = $row['message'];
            $temp = 't2';
        } else if ($get_pdf_format == 't4') {
            $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'invoice_print_template4'");
            while ($row = mysqli_fetch_array($result))
                $msg = $row['message'];
            $temp = 't4';
        } else if ($get_pdf_format == 't5') {
            $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'invoice_print_template5'");
            while ($row = mysqli_fetch_array($result))
                $msg = $row['message'];
            $temp = 't5';
        } else if ($get_pdf_format == 't6') {
            $result = mysqli_query($con, "select * from  tbl_email_templates where tplname = 'invoice_print_template6'");
            while ($row = mysqli_fetch_array($result))
                $msg = $row['message'];
            $temp = 't6';
        } else {
            $msg = "<html> <body> Template Missing</body></html>";
            $temp = '';
        }

        if ($check_print_pdf == 'for_pdf') {
            $new_msg = $msg;
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $new_msg);
        }
        $replace_array = $this->getDesignStyles($no, $replace_array, $temp, $thous_sep);

        $replace_msg = $this->replacePlaceHolderValue($replace_array, $msg, $temp);
        return $replace_msg;
    }

    function getDesignStyles($no, $replace_array, $temp, $thous_sep) {
        $product_detail = $replace_array['product_detail'];
        $product_detail_for_hsn = $replace_array['product_detail_for_hsn'];
        $tax_detail = $replace_array['tax_detail'];
        $payment_detail = $replace_array['payment_det'];
        $tax_amunt_word = "";
        $tax_percen_design_det = "";
        $tax_design_det = "";
        $product_det = "";
        $tax_pern_det = "";
        $tax_det = "";
        $pay_det = "";
        $count = 0;
        if ($temp == 't5') {
            $no = 1;

            $tax_design_det = "";
            $tax_design_det = "<tr>
                            <th rowspan=2 style=width:30%;>HSN/SAC</th>  
                             <th rowspan=2 style=width:25%;>Valuable Tax</th>  ";

            foreach ($tax_detail as $tax) {
                $tax_design_det = $tax_design_det . " 
                      <th colspan=2 style=width:35%;>" . $tax['tax_name'] . "</th>";
            }


            $tax_design_det = $tax_design_det . "</tr> <tr> ";
            foreach ($tax_detail as $tax) {
                $tax_design_det = $tax_design_det . " 
                     
                            <th style=width:10%;>Rate</th>
                            <th style=width:10%;>Amount</th>                             
                      ";
            }
            $tax_design_det = $tax_design_det . "</tr> ";

            $tax_percen_design_det = "";
            $total_valuable_tax_amount = 0;
            $discount_percentage = $replace_array['discount_percentage'];
            if ($replace_array['discount_mode'] == 'amount') {
                $subtotal_cal = $replace_array['subtotal'];
                $subtotal_cal = str_replace(',', '', $subtotal_cal);
                $subtotal_cal = str_replace(' ', '', $subtotal_cal);

                $calculate_discount_percentage = ($replace_array['discount'] * 100 ) / $subtotal_cal;

                $discount_percentage = floatval(number_format($calculate_discount_percentage, 2, '.', ''));
            }

            foreach ($product_detail_for_hsn as $pro_det) {
                $tax_percen_design_det = $tax_percen_design_det . "<tr>
                        <td>" . $pro_det['hsn_code'] .
                        "</td> ";
                $discount_amount = $pro_det['price'] * ($discount_percentage / 100);
                $valuable_amount = $pro_det['price'] - $discount_amount;
                $valuable_amount_display = $this->convertNumberFormat($valuable_amount, $thous_sep);
                $total_valuable_tax_amount += $valuable_amount;
                $tax_percen_design_det = $tax_percen_design_det . "<td style='text-align:right;'>" . $valuable_amount_display . "</td>";
                foreach ($tax_detail as $tax) {
                    $tax_amount_display = $this->convertNumberFormat($valuable_amount * ($tax['tax_percen'] / 100), $thous_sep);
                    $tax_percen_design_det = $tax_percen_design_det . "<td style='text-align: right;'>
                                " . $tax['tax_percen'] . " % " . " 
                            </td>   
                             <td style='text-align:right;'>
                                " . $tax_amount_display . "
                            </td> ";
                }
                $tax_percen_design_det = $tax_percen_design_det . "</tr>";
            }
            $total_valuable_tax_amount_display = $this->convertNumberFormat($total_valuable_tax_amount, $thous_sep);
            $tax_percen_design_det = $tax_percen_design_det . "<tr> <tr style= border-top: 1px solid #000; ><td><b>Total</b></td> "
                    . "<td style='text-align:right;'><b>" . $total_valuable_tax_amount_display . "</b></td>";
            $total_tax = 0;
            foreach ($tax_detail as $tax) {
                $total_tax = $total_tax + $tax['tax_amount'];
                $tax_percen_design_det = $tax_percen_design_det . "
                            <td style='text-align:right;'> </td>  <td style='text-align:right;'><b>  " . $tax['tax_amount'] . "</b>
                            </td>   
                             ";
            }
            $tax_percen_design_det = $tax_percen_design_det . "</tr>";
            $tax_amunt_word = $this->forWordinPaise($total_tax);
            //Replace Other Custome attribute
        }
        $tax_detail = $replace_array['tax_detail'];
        if (empty($tax_detail)) {
            $tax_percen_design_det = "";
            $tax_design_det = "";
            $tax_amunt_word = "-";
        }
        //   $replace_array['product_detail'] = $product_det;
        $replace_array['tax_amount_word'] = $tax_amunt_word;
        // $replace_array['tax_detail'] = $tax_det;
        $replace_array['tax_percen_detail'] = $tax_percen_design_det;
        $replace_array['tax_design_detail'] = $tax_design_det;
        //    $replace_array['payment_det'] = $pay_det;
        return $replace_array;
    }

    function replacePlaceHolderValue($replace_array, $msg, $temp) {
        $product_detail = $replace_array['product_detail'];
        $tax_detail = $replace_array['tax_detail'];
        $payment_detail = $replace_array['payment_det'];

        $extract_string = $this->extractString($msg, '<!--product-row-start-->', '<!--product-row-end-->');

        $copy_string = $extract_string;
        $count = 0;
        $product_det = "";
        foreach ($product_detail as $pro_det) {
            $count = $count + 1;
            $extract_string = str_replace("{{slno}}", $count, $extract_string);
            $extract_string = str_replace("{{product}}", $pro_det['product'], $extract_string);
            $extract_string = str_replace("{{cus_op1}}", $pro_det['cus_op1'], $extract_string);
            $extract_string = str_replace("{{cus_op2}}", $pro_det['cus_op2'], $extract_string);
            $extract_string = str_replace("{{cus_op3}}", $pro_det['cus_op3'], $extract_string);
            $extract_string = str_replace("{{cus_op4}}", $pro_det['cus_op4'], $extract_string);
            $extract_string = str_replace("{{cus_op5}}", $pro_det['cus_op5'], $extract_string);


            $extract_string = str_replace("{{uom}}", $pro_det['uom'], $extract_string);
            $extract_string = str_replace("{{hsn_code}}", $pro_det['hsn_code'], $extract_string);
            $extract_string = str_replace("{{qty}}", $pro_det['qty'], $extract_string);
            $extract_string = str_replace("{{price}}", $pro_det['price'], $extract_string);
            $extract_string = str_replace("{{row_total}}", $pro_det['amount'], $extract_string);
            $product_det = $product_det . $extract_string;
            $extract_string = $copy_string;
        }
        if ($temp == 't6') {
            for ($i = $count + 1; $i <= 10; $i++) {
                $extract_string = str_replace("{{slno}}", '', $extract_string);
                $extract_string = str_replace("{{product}}", "", $extract_string);
                $extract_string = str_replace("{{uom}}", "", $extract_string);
                $extract_string = str_replace("{{hsn_code}}", "", $extract_string);
                $extract_string = str_replace("{{qty}}", "", $extract_string);
                $extract_string = str_replace("{{price}}", "", $extract_string);
                $extract_string = str_replace("{{row_total}}", "", $extract_string);
                $product_det = $product_det . $extract_string;
                $extract_string = $copy_string;
            }
        }

        $msg = str_replace("<!--product-row-start-->$extract_string<!--product-row-end-->", $product_det, $msg);

        $tax_deta = "";
        $extract_string = $this->extractString($msg, '<!--tax-row-start-->', '<!--tax-row-end-->');
        $copy_string = $extract_string;
        $count = 0;
        $product_det = "";
        if (!empty($tax_detail)) {
            foreach ($tax_detail as $tax_det) {
                $count = $count + 1;
                $extract_string = str_replace("{{slno}}", $count, $extract_string);
                $extract_string = str_replace("{{tax_name}}", $tax_det['tax_name'], $extract_string);
                $extract_string = str_replace("{{tax_percen}}", $tax_det['tax_percen'], $extract_string);
                $extract_string = str_replace("{{tax_amount}}", $tax_det['tax_amount'], $extract_string);

                $tax_deta = $tax_deta . $extract_string;
                $extract_string = $copy_string;
            }
            $msg = str_replace("<!--tax-row-start-->$extract_string<!--tax-row-end-->", $tax_deta, $msg);
        } else {
            $extract_string = str_replace("{{slno}}", '', $extract_string);
            $extract_string = str_replace("{{tax_name}}", '', $extract_string);
            $extract_string = str_replace("{{tax_percen}}", '', $extract_string);
            $extract_string = str_replace("{{tax_amount}}", '', $extract_string);

            $tax_deta = $tax_deta . $extract_string;
            $extract_string = $copy_string;
            $msg = str_replace("<!--tax-row-start-->$extract_string<!--tax-row-end-->", '', $msg);
        }
        $payment_deta = "";
        $extract_string1 = $this->extractString($msg, '<!--payment-row-start-->', '<!--payment-row-end-->');
        $extract_string = $this->extractString($msg, '<!--payment-row-start-->', '<!--payment-row-end-->');

        // $extract_string = $this->extractString($msg, '<!--payment-row-start-det-->', '<!--payment-row-end-det-->');
        $copy_string = $extract_string;
        $count = 0;
        $payment_deta1 = "";
        if (count($payment_detail > 1) && $temp == 't2') {
            $extract_string = $this->extractString($msg, '<!--payment-row-start-head-->', '<!--payment-row-end-head-->');
            $payment_deta1 = $payment_deta . $extract_string;
            $extract_string = $this->extractString($msg, '<!--payment-row-start-det-->', '<!--payment-row-end-det-->');

            $copy_string = $extract_string;
        }

        foreach ($payment_detail as $pay_det) {
            $count = $count + 1;
            $extract_string = str_replace("{{pay_date}}", $pay_det['pay_date'], $extract_string);
            $extract_string = str_replace("{{pay_type}}", $pay_det['pay_type'], $extract_string);
            $extract_string = str_replace("{{pay_amount}}", $pay_det['pay_amount'], $extract_string);

            $payment_deta = $payment_deta . $extract_string;
            $extract_string = $copy_string;
        }
        $payment_deta = $payment_deta1 . $payment_deta;
        if (count($payment_detail) < 1) {
            $payment_deta = "";
        };
        $msg = str_replace("<!--payment-row-start-->$extract_string1<!--payment-row-end-->", $payment_deta, $msg);


        $new_msg = str_replace("{{invoice_no}}", $replace_array['invoice_no'], $msg);

        $new_msg = str_replace("{{packing}}", $replace_array['packing'], $new_msg);
        $new_msg = str_replace("{{insurance}}", $replace_array['insurance'], $new_msg);

        $new_msg = str_replace("{{payee}}", $replace_array['payee'], $new_msg);
        $new_msg = str_replace("{{consignee}}", $replace_array['consignee'], $new_msg);

        $new_msg = str_replace("{{shipping}}", $replace_array['shipping'], $new_msg);
        $new_msg = str_replace("{{billing}}", $replace_array['billing'], $new_msg);
        $new_msg = str_replace("{{sales_order}}", $replace_array['sales_order'], $new_msg);
        $new_msg = str_replace("{{notes}}", $replace_array['notes'], $new_msg);

        $new_msg = str_replace("{{currency}}", $replace_array['currency'], $new_msg);
        $new_msg = str_replace("{{date}}", $replace_array['date'], $new_msg);
        $new_msg = str_replace("{{duedate}}", $replace_array['duedate'], $new_msg);
        $new_msg = str_replace("{{tax_amount}}", $replace_array['total_tax'], $new_msg);

        $new_msg = str_replace("{{companyname}}", $replace_array['companyname'], $new_msg);
        $new_msg = str_replace("{{address}}", $replace_array['address'], $new_msg);
        $new_msg = str_replace("{{bank}}", $replace_array['bank'], $new_msg);
        $new_msg = str_replace("{{title}}", $replace_array['title'], $new_msg);
        $new_msg = str_replace("{{cus_name}}", $replace_array['cus_name'], $new_msg);
        $new_msg = str_replace("{{cus_pin}}", $replace_array['cus_pin'], $new_msg);

        $new_msg = str_replace("{{phone}}", $replace_array['phone'], $new_msg);
        $new_msg = str_replace("{{email}}", $replace_array['email'], $new_msg);
        $new_msg = str_replace("{{logo}}", $replace_array['logo'], $new_msg);
        $new_msg = str_replace("{{round_off}}", $replace_array['round_off'], $new_msg);

        // $new_msg = str_replace("{{product_dets}}", $replace_array['product_detail'], $new_msg);
        $new_msg = str_replace("{{subtotal}}", $replace_array ['subtotal'], $new_msg);
        $new_msg = str_replace("{{discount}}", $replace_array['discount'], $new_msg);
        //  $new_msg = str_replace("{{tax}}", $replace_array['tax_detail'], $new_msg);
        $new_msg = str_replace("{{tax_percen_detail}}", $replace_array['tax_percen_detail'], $new_msg);
        $new_msg = str_replace("{{tax_percen_design}}", $replace_array['tax_design_detail'], $new_msg);
        $new_msg = str_replace("{{paid}}", $replace_array['paid'], $new_msg);
        $new_msg = str_replace("{{grand}}", $replace_array['grand'], $new_msg);
        $new_msg = str_replace("{{balance}}", $replace_array['balance'], $new_msg);
        $new_msg = str_replace("{{amount_word}}", $replace_array['amount_word'], $new_msg);
        $new_msg = str_replace("{{tax_amount_word}}", $replace_array['tax_amount_word'], $new_msg);
        $new_msg = str_replace("{{sign}}", $replace_array['sign'], $new_msg);
        $new_msg = str_replace("{{days}}", $replace_array['days'], $new_msg);

        $new_msg = str_replace("{{status}}", $replace_array['status'], $new_msg);
        $new_msg = str_replace("{{total_qty}}", $replace_array['total_qty'], $new_msg);
        $new_msg = str_replace("{{gross_total}}", $replace_array['gross_total'], $new_msg);
        $new_msg = str_replace("{{cus_company}}", $replace_array['cus_company'], $new_msg);
        $new_msg = str_replace("{{com_name}}", $replace_array['com_name'], $new_msg);
        $new_msg = str_replace("{{com_address}}", $replace_array['com_address'], $new_msg);


        $new_msg = str_replace("{{payee_phone}}", $replace_array['payee_phone'], $new_msg);
        $new_msg = str_replace("{{payee_email}}", $replace_array['payee_email'], $new_msg);
        $new_msg = str_replace("{{payee_ship_address}}", $replace_array['payee_ship_address'], $new_msg);
        $new_msg = str_replace("{{payee_pin}}", $replace_array['payee_pin'], $new_msg);
        $new_msg = str_replace("{{payee_bill_address}}", $replace_array['payee_bill_address'], $new_msg);

        $new_msg = str_replace("{{consignee_phone}}", $replace_array['consignee_phone'], $new_msg);
        $new_msg = str_replace("{{consignee_email}}", $replace_array['consignee_email'], $new_msg);
        $new_msg = str_replace("{{consignee_ship_address}}", $replace_array['consignee_ship_address'], $new_msg);
        $new_msg = str_replace("{{consignee_pin}}", $replace_array['consignee_pin'], $new_msg);
        $new_msg = str_replace("{{consignee_bill_address}}", $replace_array['consignee_bill_address'], $new_msg);
        
        $new_msg = str_replace("{{title_type}}", $replace_array['title_type'], $new_msg);





        //  $new_msg = str_replace("{{payment}}", $replace_array['payment'], $new_msg);
        //$new_msg = str_replace("{{payment_det}}", $replace_array['payment_det'], $new_msg);
        //This is for Custom attribute
        $invoice_custom_detail = $replace_array['invoice_custom_detail'];
        //foreach() 

        foreach ($invoice_custom_detail as $custom) {
            $new_msg = str_replace($custom['key'], $custom['value'], $new_msg);
        }


        $customer_custom_detail = $replace_array['customer_custom_detail'];
        foreach ($customer_custom_detail as $custom) {
            $new_msg = str_replace($custom['key'], $custom['value'], $new_msg);
        }



        return $new_msg;
    }

    function extractString($string, $start, $end) {
        $string = " " . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return "";
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    function setCurrency($con) {
        $symbol = '';
        $result = mysqli_query($con, "select * from tbl_app_config where setting  = 'currency'");
        while ($row = mysqli_fetch_array($result))
            $symbol = $row['value'];
        return $symbol . ". " . $amount;
    }

    function forWordinPaise($number) {

        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'fourty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
            } else
                $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
        return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;

        // return $result;
    }

    function getCurrencySymbol($con) {
        $symbol = '';
        $result = mysqli_query($con, "select * from tbl_app_config where setting  = 'currency'");
        while ($row = mysqli_fetch_array($result))
            $symbol = $row['value'];
        return "( " . $symbol . ". " . ")";
    }

    function getCurrencySymbolNoBracket($con) {
        $symbol = '';
        $result = mysqli_query($con, "select * from tbl_app_config where setting  = 'currency'");
        while ($row = mysqli_fetch_array($result))
            $symbol = $row['value'];
        return "" . $symbol . "";
    }

    function setSignature($con) {
        $result = mysqli_query($con, "select * from tbl_app_config where setting =  'signature'");
        while ($row = mysqli_fetch_array($result))
            $sign = $row['value'];
        return $sign;
    }

    function getDateFormat($con) {
        $dateFormat = '';
        $result = mysqli_query($con, "select * from  tbl_app_config where setting =  'date_format'");
        while ($row = mysqli_fetch_array($result))
            $dateFormat = $row['value'];
        return $dateFormat;
    }

    function formatDate($date, $format) {


        if (empty($date) || empty($format)) {

            return $date;
        }
        $new_format = str_replace("YYYY", "Y", $format);
        $new_format = str_replace("yyyy", "y", $new_format);
        $new_format = str_replace("MM", "M", $new_format);
        $new_format = str_replace("mm", "m", $new_format);
        $new_format = str_replace("DD", "D", $new_format);
        $new_format = str_replace("dd", "d", $new_format);

        $new_date = date_create($date);

        return date_format($new_date, $new_format);
    }

    function convertNumberToWord($num) {
        $count = 0;
        global $ones, $tens, $triplets;
        $ones = array(
            '',
            ' One',
            ' Two',
            ' Three',
            ' Four',
            ' Five',
            ' Six',
            ' Seven',
            ' Eight',
            ' Nine',
            ' Ten',
            ' Eleven',
            ' Twelve',
            ' Thirteen',
            ' Fourteen',
            ' Fifteen',
            ' Sixteen',
            ' Seventeen',
            ' Eighteen',
            ' Nineteen'
        );
        $tens = array(
            '',
            '',
            ' Twenty',
            ' Thirty',
            ' Fourty',
            ' Fifty',
            ' Sixty',
            ' Seventy',
            ' Eighty',
            ' Ninety'
        );

        $triplets = array(
            '',
            ' Thousand',
            ' Million',
            ' Billion',
            ' Trillion',
            ' Quadrillion',
            ' Quintillion',
            ' Sextillion',
            ' Septillion',
            ' Octillion',
            ' Nonillion'
        );
        return $this->convertNum($num);
    }

    /**
     * Function to dislay tens and ones
     */
    function commonloop($val, $str1 = '', $str2 = '') {
        global $ones, $tens;
        $string = '';
        if ($val == 0)
            $string .= $ones[$val];
        else if ($val < 20)
            $string .= $str1 . $ones[$val] . $str2;
        else
            $string .= $str1 . $tens[(int) ($val / 10)] . $ones[$val % 10] . $str2;
        return $string;
    }

    /**
     * returns the number as an anglicized string
     */
    function convertNum($num) {
        $num = (int) $num;    // make sure it's an integer

        if ($num < 0)
            return 'negative' . $this->convertTri(-$num, 0);

        if ($num == 0)
            return 'Zero';
        return $this->convertTri($num, 0);
    }

    /**
     * recursive fn, converts numbers to words
     */
    function convertTri($num, $tri) {
        global $ones, $tens, $triplets, $count;
        $test = $num;
        $count++;
        // chunk the number, ...rxyy
        // init the output string
        $str = '';
        // to display hundred & digits
        if ($count == 1) {
            $r = (int) ($num / 1000);
            $x = ($num / 100) % 10;
            $y = $num % 100;
            // do hundreds
            if ($x > 0) {
                $str = $ones[$x] . ' Hundred';
                // do ones and tens
                $str .= $this->commonloop($y, ' and ', '');
            } else if ($r > 0) {
                // do ones and tens
                $str .= $this->commonloop($y, ' and ', '');
            } else {
                // do ones and tens
                $str .= $this->commonloop($y);
            }
        }
        // To display lakh and thousands
        else if ($count == 2) {
            $r = (int) ($num / 10000);
            $x = ($num / 100) % 100;
            $y = $num % 100;
            $str .= $this->commonloop($x, '', ' Lakh ');
            $str .= $this->commonloop($y);
            if ($str != '')
                $str .= $triplets[$tri];
        }
        // to display till hundred crore
        else if ($count == 3) {
            $r = (int) ($num / 1000);
            $x = ($num / 100) % 10;
            $y = $num % 100;
            // do hundreds
            if ($x > 0) {
                $str = $ones[$x] . ' Hundred';
                // do ones and tens
                $str .= $this->commonloop($y, ' and ', ' Crore ');
            } else if ($r > 0) {
                // do ones and tens
                $str .= $this->commonloop($y, ' and ', ' Crore ');
            } else {
                // do ones and tens
                $str .= $this->commonloop($y);
            }
        } else {
            $r = (int) ($num / 1000);
        }
        // add triplet modifier only if there
        // is some output to be modified...
        // continue recursing?
        if ($r > 0)
            return $this->convertTri($r, $tri + 1) . $str;
        else
            return $str;
    }

    function getThosandSeperatorFormat($con) {
        $thous_sep = 'dd,dd,ddd';
        $result = mysqli_query($con, "select * from tbl_app_config where setting = 'thousand_seperator'");
        while ($row = mysqli_fetch_array($result))
            $thous_sep = $row['value'];
        return $thous_sep;
    }

    function convertNumberFormat($number, $thous_sep) {
        // echo 'before number ='.$number;
        $number = number_format((float) $number, 2, '.', '');
        // echo 'after number ='.$number;
        list($whole, $decimal) = explode('.', $number);
        $number = (float) $number;
        if ($thous_sep == 'd,dddd,dddd') {
            $number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/", ",", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'd dddd dddd') {
            $number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/", " ", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'dd,dd,dd,ddd') {
            //$number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/",",",$whole);
            if (strlen($whole) < 4) {
                $number = $whole;
            } else {
                $tail = substr($whole, -3);
                $head = substr($whole, 0, -3);
                $head = preg_replace("/\B(?=(?:\d{2})+(?!\d))/", ",", $head);
                $whole = $head . "," . $tail;
            }
            $number = $whole . "." . $decimal;
        } else if ($thous_sep == 'ddd ddd ddd') {
            $number = preg_replace("/\B(?=(?:\d{3})+(?!\d))/", " ", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'ddd,ddd,ddd') {
            $number = preg_replace("/\B(?=(?:\d{3})+(?!\d))/", ",", $whole);
            $number = $number . "." . $decimal;
        }
        return $number;
    }

}

?>