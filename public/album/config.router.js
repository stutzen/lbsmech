'use strict';
/**
 * Config for the router
 */
angular.module('app')
        .run(
                ['$rootScope', '$state', '$stateParams', '$location', 'Auth',
                    function($rootScope, $state, $stateParams, $location, Auth) {
                        $rootScope.$state = $state;
                        $rootScope.$stateParams = $stateParams;
                        $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {

                            if (typeof $location.search().refer != 'undefined')
                            {
                                $rootScope.refer = $location.search().refer;
                            }
                            else
                            {
                                $rootScope.refer = "";
                            }

                            if (!$rootScope.isUserModelUpdated) {

//                                event.preventDefault();
//                                if (!$rootScope.isUserModelUpdatedProcessing)
//                                {
////                                    $rootScope.isUserModelUpdatedProcessing = true;
////                                    Auth.getUserInfo().then(function(response) {
////                                        $state.go(toState, toParams);
////                                    });
//                                }
//                                return;
                            }


                            if ($rootScope.getNavigationBlockMsg != null && $rootScope.getNavigationBlockMsg() != "")
                            {
                                if (!confirm($rootScope.getNavigationBlockMsg(false))) {
                                    event.preventDefault(); // user clicks cancel, wants to stay on page
                                } else {
                                    //No Script need to redirect another page
                                }
                            }


                        });
                        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

//                            if ($rootScope.isLogined)
//                            {
//                                if (typeof toState.accessRole != 'undefined')
//                                {
//                                    var avaAccessRole = toState.accessRole;
//                                    if (avaAccessRole.indexOf($rootScope.app.selectedRole) == -1)
//                                    {
//                                        Auth.validatePageAccess();
//                                    }
//                                }
//                            }

                            if (typeof toState.containerClass != "undefined")
                            {
                                $rootScope.containerClass = toState.containerClass;
                            }
                            else
                            {
                                $rootScope.containerClass = '';
                            }
                            //Current Page Update
                            if (typeof toState.pageName != "undefined")
                            {
                                $rootScope.currentPage = toState.pageName;
                            }
                            else
                            {
                                $rootScope.currentPage = '';
                            }
                        });
                    }
                ]
                )
        .config(
                ['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG', '$provide', 'COMPONENT_CONFIG',
                    function($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG, $provide, COMPONENT_CONFIG) {

                        $urlRouterProvider.otherwise(function($injector, $location) {
                            var $state = $injector.get("$state");
                            $state.go('album');
                        });
                        $stateProvider
                                .state('album', {
                                    url: '/:code',
                                    pageName: 'album',
                                    accessRole: 'GUEST',
                                    templateUrl: '../album/tpl/album.html',
                                    resolve: load(['controllor/albumCtrl.js', 'fileupload', 'photo-gallery-album', 'angularFileUpload', 'cloud-image-loader'])
                                })
                                .state('albumView', {
                                    url: '/album-view/:id',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: '../album/tpl/albumView.html',
                                    resolve: load(['controllor/albumViewCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery-album'])
                                })
                                .state('albumViewNew', {
                                    url: '/album-photo-view/:id',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: '../album/tpl/albumViewNew.html',
                                    resolve: load(['controllor/albumViewNewCtrl.js', 'fileupload', 'photo-gallery-album', 'angularFileUpload', 'cloud-image-loader'])
                                })


                        function load(srcs, callback) {
                            return {
                                deps: ['$ocLazyLoad', '$q',
                                    function($ocLazyLoad, $q) {
                                        var deferred = $q.defer();
                                        var promise = false;
                                        srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                                        if (!promise) {
                                            promise = deferred.promise;
                                        }
                                        angular.forEach(srcs, function(src) {
                                            promise = promise.then(function() {
                                                if (JQ_CONFIG[src]) {
                                                    return $ocLazyLoad.load(JQ_CONFIG[src]);
                                                }
                                                //lazy load the application component
                                                if (COMPONENT_CONFIG[src]) {
                                                    return $ocLazyLoad.load(COMPONENT_CONFIG[src]);
                                                }
                                                angular.forEach(MODULE_CONFIG, function(module) {
                                                    if (module.name == src) {
                                                        name = module.name;
                                                    } else {
                                                        name = src;
                                                    }
                                                });
                                                return $ocLazyLoad.load(name);
                                            });
                                        });
                                        deferred.resolve();
                                        return callback ? promise.then(function() {
                                            return callback();
                                        }) : promise;
                                    }]
                            }
                        }

                        $provide.decorator('$uiViewScroll', function($delegate) {
                            return function(uiViewElement) {
                                $('html,body').animate({
                                    scrollTop: uiViewElement.offset().top
                                }, 500);
                            };
                        });
                    }
                ]
                );
