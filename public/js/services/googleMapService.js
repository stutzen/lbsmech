app.service("$googleMapService", ['$rootScope', '$httpService', 'APP_CONST', '$document', '$timeout', function($rootScope, $httpService, APP_CONST, $document, $timeout) {

        var $googleMapService = this;
        this.map = {};
        this.directionsDisplay = [];
        this.directionsService = null;
        this.markerList = [];
        this.requestMarkerList = [];
        this.drivingPath = [];
        this.infowindow = null;
        this.defaultMapId = '';

        //Dragable Marker Postion
        this.dragableMarkerPostion = {
            lat: '',
            lng: ''
        };

        this.GEO_POINT_CHANGE_EVENT = "googleMapGeoPointChangeEvent";
        /*
         *
         * InitMap will draw the map on corresponding domid, which is recevied in domId argument
         *
         *
         * @param {type} domId - html dom element id
         * @param {type} centerPoint - Geo point
         * @param {type} markers - list  of marker to draw on inital  draw
         * @returns {undefined}
         */
        this.initMap = function(domId, centerPoint, markers, option) {

            //Wait to load the goole map api
            if (typeof window.google == 'undefined')
            {
                $timeout(function() {
                    $googleMapService.initMap(domId, centerPoint, markers, option);
                }, 1000);
                return;
            }

            var latValue = 55.378051;
            var lngValue = -3.435973;
            var mapZoomLevel = 5;
            if (centerPoint != null)
            {
                latValue = centerPoint.lat;
                lngValue = centerPoint.lng;
            }
            else if (markers != null && markers.length)
            {
                latValue = markers[0].lat;
                lngValue = markers[0].lng;
            }

            var mapOption = {
                zoom: mapZoomLevel,
                center: {
                    lat: latValue,
                    lng: lngValue
                }
            }
            if (option != null)
            {
                mapOption = option;
            }
            this.defaultMapId = domId;
            this.map[domId] = new google.maps.Map(document.getElementById(domId), mapOption);
            this.directionsService = new google.maps.DirectionsService();
            this.infowindow = new google.maps.InfoWindow({
                content: '<div>holding...</div>'
            });
            $timeout(function() {
                $googleMapService.loadMarkerPoint(markers);
            }, 1000);
        }


        /*
         * It used to move one point to another point
         *
         * @param {type} markers
         * @returns {undefined}
         */
        this.moveToPoint = function(markers, mapId) {
            var validMapId = this.defaultMapId;
            if (typeof mapId != 'undefined')
            {
                validMapId = mapId;
            }

            if (typeof window.google == 'undefined' || this.defaultMapId == '' || typeof this.map[validMapId] == 'undefined')
            {

                $timeout(function() {
                    $googleMapService.moveToPoint(markers);
                }, 1000);
                return;
            }
            var center = new google.maps.LatLng(markers['lat'], markers['lng']);
            if (typeof mapId == 'undefined')
            {
                this.map[this.defaultMapId].panTo(center);
            }
            else
            {
                this.map[mapId].panTo(center);
            }
        }

        this.refershMap = function(mapId) {
            if (typeof window.google === 'undefined' || this.defaultMapId === '')
            {
                $timeout(function() {
                    $googleMapService.refershMap(mapId);
                }, 1000);
                return;
            }
            if (typeof mapId === 'undefined')
            {
                google.maps.event.trigger(this.map[this.defaultMapId], 'resize');
            }
            else
            {
                console.log('map resize event');
                google.maps.event.trigger(this.map[mapId], 'resize');
            }
        }

        this.loadMarkerPoint = function(markers, mapId) {
            var validMapId = this.defaultMapId;
            if (typeof mapId != 'undefined')
            {
                validMapId = mapId;
            }

            if (typeof window.google == 'undefined' || this.defaultMapId == '' || typeof this.map[validMapId] == 'undefined')
            {
                $timeout(function() {
                    $googleMapService.loadMarkerPoint(markers, mapId);
                }, 1000);
                return;
            }
            if (markers != null)
            {
                for (var i = 0; i < markers.length; i++) {

                    var mapObject = this.map[this.defaultMapId];
                    if (typeof mapId !== 'undefined')
                    {
                        mapObject = this.map[mapId];
                    }

                    var position = new google.maps.LatLng(markers[i]['lat'], markers[i]['lng']);
                    //bounds.extend(position);
                    var marker = new google.maps.Marker({
                        position: position,
                        map: mapObject,
                        title: markers[i]['name'],
                        html: markers[i].html
                    });
                    this.markerList.push(marker);
                    google.maps.event.addListener(marker, 'click', function() {
                        // where I have added .html to the marker object.
                        $googleMapService.infowindow.setContent(this.html);
                        $googleMapService.infowindow.open(marker.map, this);
                    });
                }
            }
        }


        this.loadRequestMarkerPoint = function(markers) {

            if (typeof window.google == 'undefined' || this.defaultMapId != '')
            {
                $timeout(function() {
                    $googleMapService.loadRequestMarkerPoint(markers);
                }, 1000);
                return;
            }
        }

        this.showInfoWindow = function(index, mapId) {

            if (this.markerList == null || index > this.markerList.length)
                return;

            if (typeof mapId == 'undefined')
            {
                $googleMapService.infowindow.setContent($googleMapService.markerList[index].html);
                $googleMapService.infowindow.open($googleMapService.map[$googleMapService.defaultMapId], $googleMapService.markerList[index]);

            }
            else
            {
                $googleMapService.infowindow.setContent($googleMapService.markerList[index].html);
                $googleMapService.infowindow.open($googleMapService.map[mapId], $googleMapService.markerList[index]);
            }

        }


        /*
         * It used to clear the all marker point.
         */
        this.clearMarker = function() {

            if (this.markerList == null)
                return;
            for (var i = 0; i < this.markerList.length; i++) {

                this.markerList[i].setMap(null);
            }
            this.markerList = [];
        }

        /*
         *  DraggleMakerPoint is used to allow the user to select the geopoint
         *
         *
         */
        this.createDragableMarkerPoint = function(markers, mapId) {


            if (typeof window.google == 'undefined' || this.defaultMapId == '')
            {
                $timeout(function() {
                    $googleMapService.createDragableMarkerPoint(markers, mapId);
                }, 1000);
                return;
            }

            this.clearMarker();
            var defaultGeoPoint = {};
            defaultGeoPoint.lat = 51.1464659;
            defaultGeoPoint.lng = 0.875019;

            if (markers != null)
            {
                defaultGeoPoint.lat = markers.lat;
                defaultGeoPoint.lng = markers.lng;
            }
            var mapObject = this.map[this.defaultMapId];
            if (typeof mapId !== 'undefined')
            {
                mapObject = this.map[mapId];
            }


            var position = new google.maps.LatLng(defaultGeoPoint.lat, defaultGeoPoint.lng);
            //bounds.extend(position);
            var marker = new google.maps.Marker({
                position: position,
                draggable: true,
                map: mapObject

            });
            mapObject.panTo(marker.getPosition());
            //update default dragable marker position
            $googleMapService.dragableMarkerPostion = defaultGeoPoint;

            google.maps.event.addListener(marker, 'dragend', function(event) {

                var geoPoint = {};
                geoPoint.lat = this.getPosition().lat();
                geoPoint.lng = this.getPosition().lng();
                console.log(geoPoint.lat + ', ' + geoPoint.lng);
                $googleMapService.dragableMarkerPostion = geoPoint;
                $rootScope.$broadcast($googleMapService.GEO_POINT_CHANGE_EVENT, geoPoint);


            });
            this.markerList.push(marker);
        }

        this.createDragableMapWithFixedCenterPoint = function(markers, mapId) {

            if (typeof window.google == 'undefined' || this.defaultMapId == '')
            {
                $timeout(function() {
                    $googleMapService.createDragableMapWithFixedCenterPoint(markers, mapId);
                }, 1000);
                return;
            }

            this.clearMarker();
            var defaultGeoPoint = {};
            defaultGeoPoint.lat = 51.1464659;
            defaultGeoPoint.lng = 0.875019;
            if (markers != null)
            {
                defaultGeoPoint.lat = markers.lat;
                defaultGeoPoint.lng = markers.lng;
            }
            var mapObject = this.map[this.defaultMapId];
            if (typeof mapId !== 'undefined')
            {
                mapObject = this.map[mapId];
            }

            var position = new google.maps.LatLng(defaultGeoPoint.lat, defaultGeoPoint.lng);

            mapObject.panTo(position);

            $('<div/>').addClass('centerMarker').appendTo(mapObject.getDiv());

            //update default dragable marker position
            $googleMapService.dragableMarkerPostion = defaultGeoPoint;

            google.maps.event.addListener(mapObject, "dragend", function(event) {
                var latLngValue = mapObject.getCenter().toUrlValue();
                if (latLngValue !== '')
                {
                    var latLngArray = latLngValue.split(',');

                    var geoPoint = {};
                    geoPoint.lat = latLngArray[0];
                    geoPoint.lng = latLngArray[1];
                    console.log('geoPoint');
                    console.log(geoPoint);
                    $googleMapService.dragableMarkerPostion = geoPoint;
                    $rootScope.$broadcast($googleMapService.GEO_POINT_CHANGE_EVENT, geoPoint);
//                    $rootScope.$emit($googleMapService.GEO_POINT_CHANGE_EVENT, geoPoint);
                }

            });

            google.maps.event.addListener(mapObject, "zoom_changed", function(event) {
                var latLngValue = mapObject.getCenter().toUrlValue();
                if (latLngValue !== '')
                {
                    var latLngArray = latLngValue.split(',');

                    var geoPoint = {};
                    geoPoint.lat = latLngArray[0];
                    geoPoint.lng = latLngArray[1];
                    console.log('geoPoint');
                    console.log(geoPoint);
                    $googleMapService.dragableMarkerPostion = geoPoint;
                    $rootScope.$broadcast($googleMapService.GEO_POINT_CHANGE_EVENT, geoPoint);
                }
            });
        };

        this.getGeoCode = function(data)
        {
            return $httpService.get(APP_CONST.API.GEOCODE_SEARCH_URL, data, false);
        }

        this.getLocationAddress = function(data)
        {
            return $httpService.get(APP_CONST.API.GEOCODE_SEARCH_URL, data, false);
        }


        return $googleMapService;

    }]);




