app.service("commonService", ['$rootScope', '$httpService', 'APP_CONST', function($rootScope, $httpService, APP_CONST) {

        var commonService = this;

        this.getReverseYearList = function(reverseLimit)
        {
            var retValue = [];
            var limit = 100;
            if (typeof reverseLimit == 'undefined')
            {
                limit = parseInt(reverseLimit, 10);
            }
            var currentYear = new Date().getFullYear();
            for (var i = 0; i < limit; i++)
            {
                retValue.push(currentYear - i);
            }

            return retValue;
        };

        this.getExperienceList = function(expLimit)
        {
            var retValue = [];
            var limit = 100;
            if (typeof expLimit == 'undefined')
            {
                limit = parseInt(expLimit, 10);
            }

            for (var i = 1; i <= limit; i++)
            {
                retValue.push(i);
            }

            return retValue;
        };


        this.getFavouriteList = function(data) {
            return $httpService.get(APP_CONST.GET_FAVOURITE_API_URL, data, false);
        };
        this.getRecentBooking = function(data) {
            return $httpService.get(APP_CONST.GET_RECENT_BOOKING_API_URL, data, false);
        };
        this.getBookingList = function(data) {
            return $httpService.get(APP_CONST.GET_BOOKING_HISTORY_API_URL, data, false);
        };
        this.updateBookingStatus = function(data) {
            return $httpService.get(APP_CONST.BOOKING_STATUS_UPDATE_API_URL, data, true);
        };
        this.getInsuranceList = function(data) {
            return $httpService.get(APP_CONST.API.GENERAL_LIST, data, false);
        };
        this.getLanguageList = function(data) {

            return $httpService.get(APP_CONST.API.GENERAL_LIST, data, false);
        };
        return commonService;
    }]);




