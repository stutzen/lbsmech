'use strict';
app.controller('SliderController', function($scope) {
    $scope.images=[
        {src:'slides/f2f1.png',title:'Pic 1'},
        {src:'slides/f2f2.png',title:'Pic 2'},
        {src:'slides/f2f3.png',title:'Pic 3'},
        {src:'slides/f2f4.png',title:'Pic 4'},
        {src:'slides/f2f5.png',title:'Pic 5'}
    ];
});
 
app.directive('slider1', function ($timeout) {
  return {
    restrict: 'AE',
	replace: true,
	scope:{
		images: '='
	},
    link: function (scope, elem, attrs) {
	
		scope.currentIndex=0;

		scope.next=function(){
			scope.currentIndex<scope.images.length-1?scope.currentIndex++:scope.currentIndex=0;
		};
		
		scope.prev=function(){
			scope.currentIndex>0?scope.currentIndex--:scope.currentIndex=scope.images.length-1;
		};
		
		scope.$watch('currentIndex',function(){
			scope.images.forEach(function(image){
				image.visible=false;
			});
			scope.images[scope.currentIndex].visible=true;
		});
		
		/* Start: For Automatic slideshow*/
		
		var timer;
		
		var sliderFunc=function(){
			timer=$timeout(function(){
				scope.next();
				timer=$timeout(sliderFunc,2500);
			},2500);
		};
		
		sliderFunc();
		
		scope.$on('$destroy',function(){
			$timeout.cancel(timer);
		});
		
		/* End : For Automatic slideshow*/
		
    },
	templateUrl:'js/app/slider/templateurl.html'
  }
});