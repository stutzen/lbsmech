angular.module('app')
        .directive('freeslotGrid', ['$timeout', '$window', '$filter', '$rootScope', function($timeout, $window, $filter, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        typeselection: '@',
                        pgclass: '@'
                    },
                    controller: 'freeslotGridCtrl',
                    templateUrl: '/js/components/freeslot-grid/freeslot-grid.html',
                    link: function($scope, element) {

                        $scope.getFreeSlotList();
                    }
                };
            }]);