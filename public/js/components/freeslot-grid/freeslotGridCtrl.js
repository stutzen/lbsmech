
app.controller('freeslotGridCtrl', ['$scope', '$filter', 'bookingService', 'utilityService', 'APP_CONST', '$timeout', '$window', '$location', function($scope, $filter, bookingService, utilityService, APP_CONST, $timeout, $window, $location) {


        $scope.bookingService = bookingService;


        $scope.gotoNextFreeSlot = function()
        {
            $scope.bookingService.bookingModel.startDate = $scope.bookingService.bookingModel.endDate;
            $scope.bookingService.bookingModel.endDate = utilityService.incrementDate($scope.bookingService.bookingModel.startDate, $scope.bookingService.bookingModel.limit);
            $scope.bookingService.bookingModel.selectedTimeSlotVlaue = '';
            $scope.getFreeSlotList();
        }

        $scope.gotoPrevFreeSlot = function()
        {
            $scope.bookingService.bookingModel.endDate = $scope.bookingService.bookingModel.startDate;
            $scope.bookingService.bookingModel.startDate = utilityService.decrementDate($scope.bookingService.bookingModel.endDate, $scope.bookingService.bookingModel.limit);
            $scope.bookingService.bookingModel.selectedTimeSlotVlaue = '';
            $scope.getFreeSlotList();
        }
        $scope.updateSeletedTimeHandler = function(value)
        {
            $scope.bookingService.bookingModel.selectedTimeSlotVlaue = value;

            if ($scope.bookingService.bookingModel.selectedTimeSlotVlaue != '')
            {
                var parseSelectedTimeSlot = $scope.bookingService.bookingModel.selectedTimeSlotVlaue.split('_');
                var dateIndex = parseInt(parseSelectedTimeSlot[0], 10);
                var slotIndex = parseInt(parseSelectedTimeSlot[2], 10);
                $scope.bookingService.bookingModel.date = $scope.bookingService.bookingModel.uiList[dateIndex].date;
                $scope.bookingService.bookingModel.startTime = $scope.bookingService.bookingModel.uiList[dateIndex].slots[slotIndex].startTime;
                $scope.bookingService.bookingModel.endTime = $scope.bookingService.bookingModel.uiList[dateIndex].slots[slotIndex].endTime;
                $scope.bookingService.bookingModel.duration = $scope.bookingService.bookingModel.uiList[dateIndex].slots[slotIndex].duration;
            }

        }

        $scope.updateFreeSlotData = function()
        {
            var defaultUICount = 10;
            var uiList = [];
            for (var i = 0; i < $scope.bookingService.bookingModel.limit; i++)
            {
                var daySlot = {};
                daySlot.date = utilityService.incrementDate($scope.bookingService.bookingModel.startDate, i).valueOf();
                daySlot.slots = [];
                for (var j = 0; j < $scope.bookingService.bookingModel.list.length; j++)
                {
                    var utcDate = utilityService.convertToUTCDate($scope.bookingService.bookingModel.list[j].date).valueOf();
                    if (daySlot.date == utcDate)
                    {
                        daySlot.slots = angular.copy($scope.bookingService.bookingModel.list[j].slots);
                        break;
                    }
                }
                if (defaultUICount < daySlot.slots.length)
                {
                    defaultUICount = daySlot.slots.length;
                }
                uiList.push(daySlot);
            }

            //insert dummpy count
            for (var i = 0; i < uiList.length; i++)
            {
                for (var j = uiList[i].slots.length; j < defaultUICount; j++)
                {
                    var slotItem = {};
                    slotItem.booked = true;
                    slotItem.date = uiList[i].date;
                    slotItem.startTime = '';
                    slotItem.endTime = '';
                    uiList[i].slots.push(slotItem);
                }
            }


            $scope.bookingService.bookingModel.uiList = uiList;
        }

        $scope.$watch('bookingService.bookingModel.clinicId', function() {
            $scope.getFreeSlotList();
        }, true);

        $scope.$watch('bookingService.bookingModel.staffId', function() {
            $scope.getFreeSlotList()
        }, true);


        $scope.getFreeSlotList = function() {

            if (typeof $scope.bookingService.bookingModel.clinicId == 'undefined' || $scope.bookingService.bookingModel.clinicId == '' ||
                    typeof $scope.bookingService.bookingModel.staffId == 'undefied' || $scope.bookingService.bookingModel.staffId == '')
            {
                $scope.bookingService.bookingModel.list = [];
                $scope.updateFreeSlotData();
                return;
            }
            $scope.bookingService.bookingModel.isLoadingProgress = true;
            var freeSlotListParam = {};
            freeSlotListParam.compId = $scope.bookingService.bookingModel.clinicId;
            freeSlotListParam.staffId = $scope.bookingService.bookingModel.staffId;
            freeSlotListParam.sDate = utilityService.parseDateToStr($scope.bookingService.bookingModel.startDate, 'yyyy-MM-dd');
            freeSlotListParam.eDate = utilityService.parseDateToStr($scope.bookingService.bookingModel.endDate, 'yyyy-MM-dd');
            bookingService.getFreeSlot(freeSlotListParam).then(function(response) {
                var data = response.data.data;
                $scope.bookingService.bookingModel.list = data.list;
                $scope.bookingService.bookingModel.total = data.total;
                $scope.updateFreeSlotData();
                $scope.bookingService.bookingModel.isLoadingProgress = false;

            });
        };


    }]);




