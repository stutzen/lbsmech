angular.module('app')
        .directive('componentfield', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        type:'=',
                        ngModel: '=',                        
                        id:'=',
                        focusOn: '@',
                        label: '=',
                        val: '=',
                        fieldName:'=',
                        validate: '=',
                        req:'='                        
                    },
                    require: 'ngModel',
                    controller: 'componentFieldCtrl',
                    templateUrl: '../js/components/component-field/component-field.html',
                    link: function($scope, element, attrs, ngModelController) {
                        /* BASIC SETTINGS FOR DROPDOWN */
                        var $input = angular.element(element[0].getElementsByClassName('.autocomplete-input'));

                        $scope.enterDropdown = function(event) {
                            $scope.usingKeyboard = false;
                            $scope.dropdown.show = true;
                            if (event !== 'focus') {
                                // $input.focus();
                            }

                        };

//                        var key = {left: 37, up: 38, right: 39, down: 40, enter: 13, esc: 27, tab: 9};
//
//                        element[0].addEventListener("keydown", function(e) {
//                            var keycode = e.keyCode || e.which;
//
//                            var l = angular.element(this).find('li').length;
//
//                            var hasRecord = $scope.dropdown.suggestions.list.length > 0;
//                            var totalRecord = $scope.dropdown.suggestions.list.length;
//
//                            // this allows submitting forms by pressing Enter in the autocompleted field
//                            if (l == 0 || $scope.dropdown.suggestions.list.length == 0)
//                                return;
//
//
//                            // implementation of the up and down movement in the list of suggestions
//                            switch (keycode) {
//                                case key.up:
//                                    console.log('keypad event ' + key.up);
//                                    if (hasRecord)
//                                    {
//                                        if ($scope.selectedIndex > 0)
//                                        {
//                                            $scope.selectedIndex = $scope.selectedIndex - 1;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        $scope.selectedIndex = -1;
//                                    }
//                                    $scope.$apply();
//                                    break;
//                                case key.down:
//
//                                    console.log('keypad event ' + key.up);
//                                    if (hasRecord)
//                                    {
//                                        if ($scope.selectedIndex < totalRecord - 1)
//                                        {
//                                            $scope.selectedIndex = $scope.selectedIndex + 1;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        $scope.selectedIndex = -1;
//                                    }
//                                    $scope.$apply();
//                                    break;
//                                case key.left:
//                                    break;
//                                    //case key.right:
//                                case key.enter:
//                                    console.log('keypad event ' + key.enter);
//                                    if (hasRecord)
//                                    {
//                                        if ($scope.selectedIndex != -1)
//                                        {
//                                            //$scope.select($scope.dropdown.suggestions.list[$scope.selectedIndex], e);
//                                            var suggestion = $scope.dropdown.suggestions.list[$scope.selectedIndex];
//                                            $scope.dropdown.show = false;
//                                            $scope.searchKeyword = suggestion.name + '-' + suggestion.city;
//
//                                            var selectedValue = {
//                                                id: suggestion.id,
//                                                name: suggestion.name,
//                                                city: suggestion.city
//                                            }
//                                            $scope.ngModel = selectedValue;
//                                            $scope.$apply();
//                                            e.preventDefault();
//
//                                        }
//                                    }
//                                    else
//                                    {
//                                        $scope.hideDropdown();
//                                    }
//                                    break;
//                                case key.tab:
//                                case key.esc:
//                                    console.log('keypad event ' + key.tab);
//                                    $scope.dropdown.show = false;
//                                    $scope.$apply();
//                                    break;
//
//                                default:
//                                    return;
//                            }
//
//                        });
//                        // when model change, update our view (just update the div content)
//                        ngModelController.$render = function() {
//                            //iElement.find('div').text(ngModelController.$viewValue);
//                        };
//
//                        // update the model then the view
//                        $scope.updateModel = function(value) {
//                            // call $parsers pipeline then update $modelValue
//                            ngModelController.$setViewValue(value);
//                            // update the local view
//                            ngModelController.$render();
//                            ngModelController.$setValidity(false);
//                        }
//
//                        /* CLICK OUTSIDE */
//                        angular.element($window).on('click', function(event) {
//                            var $target = $(event.target);
//                            var $parents = $target.parents()
//                            var insideComponent = false;
//                            for (var i = 0; i < $parents.length; i++)
//                            {
//                                var domEle = $parents[i];
//                                if (typeof $(domEle).attr('name') != 'undefined' && $(domEle).attr('name') === attrs.name)
//                                {
//                                    insideComponent = true;
//                                    break;
//                                }
//                            }
//                            if (!insideComponent)
//                            {
//                                $scope.$apply(function() {
//                                    $scope.hideDropdown();
//                                    //$scope.dropdown.show = false;
//                                })
//
//                            }
//                        });
//
//                        //UpdateSeleted
//                        $scope.updateInitSelectedSuggestionCaption();
//                        //Update Search Data
//                        //  $scope.updateSearchData();


                    }
                }
            }]);