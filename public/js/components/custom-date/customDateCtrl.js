
app.controller('customDateCtrl', ['$scope', '$rootScope', '$filter', '$httpService', 'APP_CONST', '$timeout', 'ValidationFactory', function($scope, $rootScope, $filter, $httpService, APP_CONST, $timeout, ValidationFactory) {



        $scope.dataModel = {
            name: '',
            value: '',
            label: '',
            isRequired: false,
            isEditable: true,
            validatePattern: null

        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.fromDateOpen = false;
        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
        }
        $scope.initField = function() {

            $timeout(function() {
                if ($scope.fieldValue != undefined && $scope.fieldValue != '')
                {
                    if ($scope.fieldValue.value != undefined)
                    {
                        $scope.dataModel.value = $scope.fieldValue.value;
                    }
                    else if ($scope.fieldValue.default_value != undefined)
                    {
                        $scope.dataModel.value = $scope.fieldValue.default_value;
                    }

                    if ($scope.fieldValue.has_editable != undefined)
                    {
                        $scope.dataModel.isEditable = $scope.fieldValue.has_editable == 1 ? true : false;
                    }
                    if ($scope.fieldValue.is_required != undefined)
                    {
                        $scope.dataModel.isRequired = $scope.fieldValue.is_required == 1 ? true : false;
                    }
                    if ($scope.fieldValue.reg_pattern != undefined && $scope.fieldValue.reg_pattern != '')
                    {
                        if ($scope.fieldValue.validation_type == 'email')
                        {
                            $scope.fieldValue.reg_pattern = "^([a-z0-9,!\\#\\$%&'\\*\\+\\/=\\?\\^_`\\{\\|\\}~-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z0-9,!\\#\\$%&'\\*\\+\\/=\\?\\^_`\\{\\|\\}~-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*@([a-z0-9-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z0-9-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*\\.(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]){2,})$";
                        }
                        var patternStr = $scope.fieldValue.reg_pattern.replace(/\\/i, "\\");
                        patternStr = patternStr.replace(/,/i, "\,");
                        $scope.dataModel.validatePattern = new RegExp(patternStr);
                    }
                    if ($scope.fieldValue.attribute_code != undefined)
                    {
                        $scope.dataModel.name = $scope.fieldValue.attribute_code;
                    }

                    $scope.dataModel.label = $scope.fieldValue.attribute_label;

                }
            }, 0);

        };

        $scope.validateInput = function()
        {
            var retVal = false;

            if ($scope.dataModel.isRequired && $scope.dataModel.validatePattern != null && $scope.dataModel.value != '')
            {
                if ($scope.dataModel.validatePattern.test($scope.dataModel.value))
                {
                    retVal = true;
                }
            }
            else if (!$scope.dataModel.isRequired)
            {
                retVal = true;
            }
            return retVal;
        };



        $scope.initChangeTimeoutPromise = null;

        $scope.initChangeHandler = function()
        {
            if ($scope.initChangeTimeoutPromise != null)
            {
                $timeout.cancel($scope.initChangeTimeoutPromise);
            }
            $scope.initChangeTimeoutPromise = $timeout($scope.changeHandler, 300);
        };

        $scope.changeHandler = function()
        {
            var data = {};
            data.code = $scope.fieldValue.attribute_code;
            data.value = $scope.dataModel.value;

            $scope.$emit('customfieldValueChangeEvent', data);


        };




    }]);




