app.controller('photoGalleryCtrl', ['$scope', '$rootScope', '$timeout', function($scope, $rootScope, $timeout) {

        $scope.selected = "";
        $scope.selectedIndex = 0;
        $scope.prevNextEnable = true;

        //$scope.imageWidth = 540;
        $scope.listposition = {left: (0) + "px"};
        $scope.imageArr = [];
        $scope.scrollTo = function(image, ind) {
            $scope.listposition = {left: ($scope.imageWidth * (ind) * -1) + "px"};
            $scope.selected = image;
            $scope.selectedIndex = ind;
        };
        $scope.initCallScrollPrev = null;
        $scope.initCallScrollNext = null;
        $scope.callScrollPrev = function()
        {
            if ($scope.initCallScrollPrev != null)
            {
                $timeout.cancel($scope.initCallScrollPrev);
            }
            $scope.initCallScrollPrev = $timeout($scope.scrollPrev, 300);
        }
        $scope.callScrollNext = function()
        {
            if ($scope.initCallScrollNext != null)
            {
                $timeout.cancel($scope.initCallScrollNext);
            }
            $scope.initCallScrollNext = $timeout($scope.scrollNext, 300);
        }
        $scope.scrollPrev = function() {

            $scope.listposition = {left: (($scope.imageWidth * ($scope.selectedIndex) * -1) + $scope.imageWidth) + "px"};
            $scope.selectedIndex = $scope.selectedIndex - 1;
            $rootScope.$broadcast("initCommentEvent", $scope.selectedIndex);
        };
        $scope.scrollNext = function() {

            $scope.listposition = {left: (($scope.imageWidth * ($scope.selectedIndex) * -1) - $scope.imageWidth) + "px"};
            $scope.selectedIndex = $scope.selectedIndex + 1;
            $rootScope.$broadcast("initCommentEvent", $scope.selectedIndex);
        };

        //profileData.imgs



    }]);