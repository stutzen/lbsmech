
app.controller('customSelectOptionCtrl', ['$scope', '$filter', '$httpService', 'APP_CONST', '$timeout', 'ValidationFactory', function($scope, $filter, $httpService, APP_CONST, $timeout, ValidationFactory) {



        $scope.dataModel = {
            name: '',
            value: '',
            label: '',
            list: [],
            isRequired: false,
            isEditable: true,
            validatePattern: null

        };
        var count = 0;
        $scope.initField = function() {

            $timeout(function() {
                if ($scope.fieldValue != undefined && $scope.fieldValue != '')
                {
                    if ($scope.fieldValue.value != undefined)
                    {
                        $scope.dataModel.value = $scope.fieldValue.value;
                    }
                    else if ($scope.fieldValue.default_value != undefined)
                    {
                        $scope.dataModel.value = $scope.fieldValue.default_value;
                    }

                    if ($scope.fieldValue.has_editable != undefined)
                    {
                        $scope.dataModel.isEditable = $scope.fieldValue.has_editable == 1 ? true : false;
                    }
                    if ($scope.fieldValue.is_required != undefined)
                    {
                        $scope.dataModel.isRequired = $scope.fieldValue.is_required == 1 ? true : false;
                    }
                    if ($scope.fieldValue.reg_pattern != undefined && $scope.fieldValue.reg_pattern != '')
                    {
                        var patternStr = $scope.fieldValue.reg_pattern.replace(/\\/i, "\\");
                        patternStr = patternStr.replace(/,/i, "\,");
                        $scope.dataModel.validatePattern = new RegExp(patternStr);
                    }
                    if ($scope.fieldValue.attribute_code != undefined)
                    {
                        $scope.dataModel.name = $scope.fieldValue.attribute_code;
                    }

                    if ($scope.fieldValue.option_value != undefined)
                    {
                        console.log($scope.fieldValue.option_value);
                        $scope.dataModel.list = $scope.fieldValue.option_value.split(',');
                        if ($scope.dataModel.value != undefined)
                        {
                            for (var i = 0; i < $scope.dataModel.list.length; i++)
                            {
                                if ($scope.dataModel.value == $scope.dataModel.list[i])
                                {
                                    count = count + 1;
                                }
                            }
                            if (count == 0)
                            {
                                $scope.dataModel.list.push($scope.dataModel.value);
                            }
                        }
                        console.log($scope.dataModel.list);
                    }
                    $scope.dataModel.label = $scope.fieldValue.attribute_label;
                }
            }, 0);

        };

        $scope.validateInput = function()
        {
            var retVal = false;

            if ($scope.dataModel.isRequired)
            {
                if ($scope.dataModel.value != undefined && $scope.dataModel.value != '' && $scope.dataModel.value != null)
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else if (!$scope.dataModel.isRequired)
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.initChangeTimeoutPromise = null;

        $scope.initChangeHandler = function()
        {
            if ($scope.initChangeTimeoutPromise != null)
            {
                $timeout.cancel($scope.initChangeTimeoutPromise);
            }
            $scope.initChangeTimeoutPromise = $timeout($scope.changeHandler, 300);
        };

        $scope.changeHandler = function()
        {
            var data = {};
            data.code = $scope.fieldValue.attribute_code;
            data.value = $scope.dataModel.value;

            $scope.$emit('customfieldValueChangeEvent', data);
        };
    }]);




