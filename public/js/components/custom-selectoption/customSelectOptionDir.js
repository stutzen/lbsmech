angular.module('app')
        .directive('customSelectoption', ['$timeout', '$window', '$rootScope', function($timeout, $window, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        type:'=',
                        fieldValue:'=', 
                        formName:'=',
                        name:'@'
                    },
                    controller: 'customSelectOptionCtrl',
                    templateUrl: '../js/components/custom-selectoption/custom-selectoption.html',
                    link: function($scope, element, attrs, ngModelController) {
                        console.log("custom Select Option");
                        /* BASIC SETTINGS FOR DROPDOWN */
                        console.log("$scope.fieldValue");
                        console.log($scope.fieldValue);
                        $scope.initField();
                    }
                }
            }]);