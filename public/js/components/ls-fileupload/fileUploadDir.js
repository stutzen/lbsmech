angular.module('app')
        .directive('fileupload', ['$timeout', '$window', '$filter', '$rootScope', function($timeout, $window, $filter, $rootScope) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        typeselection: '@',
                        pgclass: '@',
                        cloudUploadPath: '@',
                        hasLimit: '@',
                        limit: '@',
                        imageList: '=',
                        showPath: '=',
                        showComment:'@',
                        type:'@'
                    },
                    controller: 'fileUploadCtrl',
                    templateUrl: '../js/components/ls-fileupload/file-upload.html',
                    link: function($scope, element) {

                        console.log('$scope.hasLimit = ' + $scope.hasLimit);
                        console.log(!$scope.hasLimit);
                        console.log($scope.hasLimit == true);
                        console.log($scope.hasLimit == 'false');
                    }
                };
            }]);