angular.module('app')
        .directive('cloudImageLoader', ['$http', '$parse', function($http, $parse) {
                return {
                    restrict: 'E',
                    replace: true,
                    scope: {
                        urlpath: '=',
                        preview: '=',
                        hasPublicRead: '=',
                        showActionIcon: '=',
                        imgheight: '=',
                        imgwidth: '='
                    },
                    controller: 'cloudImageLoaderCtrl',
                    template: '<div><img alt="loader" src="/img/loading.gif" ng-show="showloder" width="20px;"/><div class="download"  ng-show="showdownload" ng-click="download()"><img alt="download" src="/img/download.png"></div><div class="preview"  ng-show="showpreview" ng-click="download()"><img alt="preview" src="/img/preview.png"></div></div>',
                    //template:"<img ng-controller='cloudImageLoaderCtrl'  ng-src='{{imagepath}}' ng-if='showImage' alt='Image'/>",
                    link: function($scope, elem, attrs) {


                        if (typeof $scope.$root.cloudImageAccessKey.urlpath == 'undefined')
                        {
                            var requestParam = {};
                            requestParam.verb = 'GET';
                            requestParam.objName = $scope.urlpath;
                            requestParam.contentype = '';

                            $http({
                                method: 'GET',
                                url: '../urlsign/url',
                                params: requestParam,
                                config: {
                                    'key': $scope.urlpath
                                }
                            }).success(function(data, status, headers, config) {
                                /*
                                 * Update the singed Url in facory instance
                                 */
                                $scope.$root.cloudImageAccessKey[config.config.key] = data.data.url;
                                $scope.imagepath = $scope.$root.cloudImageAccessKey[config.config.key];
                                $scope.showloder = false;

                                var fileExt = $scope.urlpath.slice(-3);
                                var fileName = $scope.urlpath.substring($scope.urlpath.lastIndexOf('/') + 1);

                                if (fileExt === "gif" || fileExt === "jpg" || fileExt === "png")
                                {
                                    if ($scope.preview)
                                    {
                                        $scope.showdownload = true;
                                        $scope.showpreview = true;
                                    }
                                    var img = angular.element('<img src="' + $scope.$root.cloudImageAccessKey[config.config.key] + '" style="height:' + $scope.imgheight + 'px;width:' + $scope.imgwidth + 'px; "/>');
                                    elem.append(img);
                                }
                                else
                                {
                                    if ($scope.preview)
                                    {
                                        $scope.showdownload = true;
                                    }
                                    var img = angular.element('<img src="/img/note.png" />');
                                    elem.append(img);
                                    elem.append("<p>" + fileName + "</p>");
                                }


                            }).error(function(data, status, headers, config) {

                            });

                        }
                        else
                        {
                            $scope.showloder = false;
                            $scope.imagepath = $scope.$root.cloudImageAccessKey[config.config.key];
                            var fileExt = $scope.urlpath.slice(-3);
                            var fileName = $scope.urlpath.substring($scope.urlpath.lastIndexOf('/') + 1);

                            if (fileExt === "gif" || fileExt === "jpg" || fileExt === "png")
                            {
                                if ($scope.preview)
                                {
                                    $scope.showdownload = true;
                                    $scope.showpreview = true;
                                }
                                var img = angular.element('<img src="' + $scope.$root.cloudImageAccessKey[config.config.key] + '" style="height:' + $scope.imgheight + 'px;width:' + $scope.imgwidth + 'px; "/>');
                                elem.append(img);
                            }
                            else
                            {
                                if ($scope.preview)
                                {
                                    $scope.showdownload = true;
                                }
                                var img = angular.element('<img src="/img/note.png" />');
                                elem.append(img);
                                elem.append("<p>" + fileName + "</p>");
                            }

                        }

                    }
                }
            }]);