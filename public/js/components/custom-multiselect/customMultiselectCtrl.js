
app.controller('customMultiselectCtrl', ['$scope', '$filter', '$httpService', 'APP_CONST', '$timeout', 'ValidationFactory', function($scope, $filter, $httpService, APP_CONST, $timeout, ValidationFactory) {
    $scope.dataModel = {
        name:'',
        value: '',
        label: '',
        list:[],
        values:[],
        default_values:[],        
        isRequired:false,
        isEditable:true,
        validatePattern : null  
    };

    $scope.initField = function() {

        $timeout(function() {
            if ($scope.fieldValue != undefined && $scope.fieldValue != '')
            {
                if ($scope.fieldValue.value != undefined)
                {
                    $scope.dataModel.value = $scope.fieldValue.value;
                }
                if ($scope.fieldValue.has_editable != undefined)
                {
                    $scope.dataModel.isEditable = $scope.fieldValue.has_editable == 1 ? true:false;
                }
                if ($scope.fieldValue.is_required != undefined)
                {
                    $scope.dataModel.isRequired = $scope.fieldValue.is_required == 1? true:false;
                }
                if ($scope.fieldValue.reg_pattern != undefined && $scope.fieldValue.reg_pattern != '')
                {                      
                    var patternStr = $scope.fieldValue.reg_pattern.replace(/\\/i, "\\");
                    patternStr = patternStr.replace(/,/i, "\,");
                    $scope.dataModel.validatePattern = new RegExp( patternStr);
                }
                if ($scope.fieldValue.attribute_code != undefined)
                {
                    $scope.dataModel.name = $scope.fieldValue.attribute_code;
                }
                    
                if($scope.fieldValue.option_value != undefined)
                {
                    $scope.dataModel.list = $scope.fieldValue.option_value.split(',');
                    
                    if($scope.fieldValue.value != undefined && $scope.fieldValue.value != "")
                    {
                        $scope.dataModel.default_values = $scope.fieldValue.value.split(',');
                    }
                    else if ($scope.fieldValue.default_value != undefined)
                    {                 
                        $scope.dataModel.default_values = $scope.fieldValue.default_value.split(',');                     
                    }    
                    var loop,innerLoop,statusFlag;
                    for ( loop=0; loop<$scope.dataModel.list.length;loop++ )
                    {
                        statusFlag = false;
                        for(innerLoop=0;innerLoop<$scope.dataModel.default_values.length;innerLoop++)
                        {
                            if($scope.dataModel.list[loop]==$scope.dataModel.default_values[innerLoop])
                            {
                                $scope.dataModel.values[loop] = true;
                                statusFlag = true;
                                break;
                            }
                        }    
                        if(statusFlag == false)
                        {
                            $scope.dataModel.values[loop] = false;     
                        }
                    }  
                }
                $scope.dataModel.label = $scope.fieldValue.attribute_label;                    
            }
        }, 0);      
    };

    $scope.validateInput = function()
    {
        var retVal = false;
            
        if($scope.dataModel.isRequired && $scope.dataModel.validatePattern != null)
        {
            if ($scope.dataModel.validatePattern.test($scope.dataModel.value))
            {
                retVal = true;
            } 
        }
        else
        {
            retVal = true;
        }
        return retVal;
    };

    $scope.initChangeTimeoutPromise = null;

    $scope.initChangeHandler = function()
    {
        if ($scope.initChangeTimeoutPromise != null)
        {
            $timeout.cancel($scope.initChangeTimeoutPromise);
        }
        $scope.initChangeTimeoutPromise = $timeout($scope.changeHandler, 300);
    };
        
    $scope.changeHandler = function( index )
    {        
        var data =  {};
        data.code = $scope.fieldValue.attribute_code;

        var loop,innerLoop,statusFlag;
        data.value='';
        var statusFlag = false;
        for ( loop=0; loop<$scope.dataModel.list.length;loop++ )
        {
            
            if($scope.dataModel.values[loop] == true)
            {       
                if ( statusFlag == false )
                {    
                    data.value = $scope.dataModel.list[loop];       
                }
                else
                {
                    data.value = data.value + ',' + $scope.dataModel.list[loop];           
                }
                statusFlag = true;
            }   
        }    
                
        console.log("Data Value ",data.value);        
        $scope.$emit('customfieldValueChangeEvent', data);                      
    };                     
}]);