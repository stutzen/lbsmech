
angular.module('app')
        .component('customAdd', {
            template: '<a data-ng-show="vm.show" data-ui-sref="{{vm.redirect}}" ui-scroll-to="app" class="add_button add-dash" >Add New</a>  ',
            controller: function componentCtrl($element, $scope, $rootScope, $compile, $timeout, $filter) {
                // something here
                var vm = this;
                vm.show = false;

                vm.initUpdateDetailTimeoutPromise = null;
                vm.$onInit = function ()
                {
                    vm.show = false;
                    if (vm.initUpdateDetailTimeoutPromise != null)
                    {
                        $timeout.cancel(vm.initUpdateDetailTimeoutPromise);
                    }
                    if ($rootScope.isScreenDataLoaded)
                    {
                        vm.loadComponents();
                    } else
                    {
                        vm.initUpdateDetailTimeoutPromise = $timeout(vm.$onInit, 300);
                    }

                };

                vm.loadComponents = function ()
                {
                    vm.show = false;
                    if ($rootScope.screenAccessDetail[vm.screencode].is_create == true)
                    {
                        vm.show = true;
                    }
                };

            },
            controllerAs: "vm",

            bindings: {
                screencode: '@',
                redirect: '@'
            }
        });


//angular.module('app')
//        .directive('customAddCtrl', ['$timeout', '$window', '$rootScope', function ($timeout, $window, $rootScope) {
//                return {
//                    restrict: 'E',
//                    replace: true,
//                    scope: {
//                        screenCode: '=',
//                        redirectTo: '='
//                    },
//                    controller: 'customYesOrNoCtrl',
//                    templateUrl: '<a data-ng-show="show" data-ui-sref="{{redirectTo}}" ui-scroll-to="app" class="add_button add-dash" >Add New</a>  ',
//                    link: function ($scope, element, attrs, ngModelController) {
//
//                        /* BASIC SETTINGS FOR DROPDOWN */
//                        console.log("$scope.fieldValue");
//                        console.log($scope.fieldValue);
//                        $scope.initField();
//                    }
//                }
//            }])
//        .controller('customAddCtrl', ['$scope', '$filter', '$httpService', 'APP_CONST', '$timeout', 'ValidationFactory', '$rootScope', function ($scope, $filter, $httpService, APP_CONST, $timeout, ValidationFactory, $rootScope) {
//
//                $scope.initUpdateDetailTimeoutPromise = null;
//                $scope.initField = function () {
//                    $scope.show = false;
//                    if ($scope.initUpdateDetailTimeoutPromise != null)
//                    {
//                        $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
//                    }
//                    if ($rootScope.isScreenDataLoaded)
//                    {
//                        $scope.loadComponents();
//                    } else
//                    {
//                        $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initField, 300);
//                    }
//                };
//                
//                $scope.loadComponents = function ()
//                {
//                    $scope.show = false;
//                    if ($rootScope.screenAccessDetail[$scope.screencode].is_create == true)
//                    {
//                        $scope.show = true;
//                    }
//                };
//
//            }]);