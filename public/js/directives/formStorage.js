angular.module('app')
        .directive('formStorage', ['$http', '$parse', 'utilityService', '$rootScope', '$filter', '$localStorageService', function ($http, $parse, utilityService, $rootScope, $filter, $localStorageService) {
                return {
                    restrict: 'A',
                    replace: true,
//                    controller: 'billWidgetCtrl',
//                    templateUrl: '../js/widgets/bill-widget/bill-widget.html',
                    link: function ($scope, element, attrs) {

                        $scope.sno = 0;
                        var formElement = angular.element(element)[0];
                        var formName = $(formElement).attr('name');
                        var localStorageFiledcount = 0;
                        var storage = {};
                        storage[formName] = {};
                        for (var i = 0; i < formElement.length; i++) {
                            // This ensures we are only watching form fields
                            if (i in formElement) {
                                var element = angular.element(formElement[i]);
                                var localStorageArrributeValue = $(element).attr('name');
                                var data = angular.element(element).val();
                                storage[formName][localStorageArrributeValue] = data;
                                localStorageFiledcount++;
                            }
                        }
                        var stringObject = JSON.stringify(storage);
                        $localStorageService.set(formName, JSON.stringify(stringObject));
                        var getItem = $localStorageService.get(formName);
                        console.log('formName = ' + formName);
                        console.log('storage = ' + storage);
                        console.log('getItem = ' + getItem);
                        console.log('localStorageFiledcount = ' + localStorageFiledcount);

                    }

                };
            }]);