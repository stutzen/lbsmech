

app.controller('cityEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.cityModel = {
            id: '',
            name: '',
            isActive: true,
            list: {},
            code: '',
            country_id:'',
            country_name:'',
            country_list:[],
            state_list:[],
            state_id:'',
            state_name:''
          //  isLoadingProgress: false

        }

        $scope.validationFactory = ValidationFactory;
       


        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.edit_city_form != 'undefined' && typeof $scope.edit_city_form.$pristine != 'undefined' && !$scope.edit_city_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
         $scope.formReset = function() {
            if (typeof $scope.edit_city_form != 'undefined')
            {
                $scope.edit_city_form.$setPristine();
                $scope.updateCityInfo();
            }

        }
        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedCountryList )
            {
                $scope.updateCityInfo();
            }
//            if($scope.isLoadedStateList)
//             {
//                 $scope.updateCityInfo();
//             }
////              }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.modifycity = function() {

            if ($scope.isDataSavingProcess==false)
            {
            $scope.isDataSavingProcess = true;
            var addCityParam = {};
            var headers = {};
            headers['screen-code'] = 'city';
            addCityParam.id = $stateParams.id;
            addCityParam.name = $scope.cityModel.name;
            addCityParam.code = $scope.cityModel.code;
            addCityParam.is_active = $scope.cityModel.isActive;
            addCityParam.country_id = $scope.cityModel.country_id;
            addCityParam.state_id = $scope.cityModel.state_id;
             for (var i = 0; i < $scope.cityModel.country_list.length; i++)
            {
                if ($scope.cityModel.country_id == $scope.cityModel.country_list[i].id)
                {
                    addCityParam.country_name = $scope.cityModel.country_list[i].name;
                }
            }
             for (var i = 0; i < $scope.cityModel.state_list.length; i++)
            {
                if ($scope.cityModel.state_id == $scope.cityModel.state_list[i].id)
                {
                    addCityParam.state_name = $scope.cityModel.state_list[i].name;
                }
            }
            adminService.editCity(addCityParam, $scope.cityModel.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.city');
                }
                $scope.isDataSavingProcess = false;
            });
            }
        };

        $scope.updateCityInfo = function()
        {
            $scope.cityModel.is_active = $scope.cityModel.list.is_active;
            $scope.cityModel.name = $scope.cityModel.list.name;
            
            $scope.cityModel.id = $scope.cityModel.list.id;
            $scope.cityModel.code = $scope.cityModel.list.code;
            $scope.cityModel.country_id = $scope.cityModel.list.country_id;
            $scope.cityModel.state_id= $scope.cityModel.list.state_id;
            $scope.getstateList();
            $scope.cityModel.isLoadingProgress = false;
            
        }
        $scope.getCityInfo = function()
        {
            $scope.cityModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var stateListParam = {};
                stateListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getCityList(stateListParam, configOption).then(function(response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.cityModel.list = data.list[0];

                    }
                    $scope.initUpdateDetail();

                });
            }
        };
        $scope.getCityInfo();
        
         $scope.isLoadedCountryList = false;
        $scope.getcountryList = function()
        {
            var getListParam = {};
            getListParam.id = '';
            $scope.isLoadedCountryList = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.cityModel.country_list = data.list;
                $scope.isLoadedCountryList = true;
                
            });
        };
        
        
         $scope.isLoadedStateList = false;
        $scope.getstateList = function()
        {
           // $scope.cityModel.state_name='';
            var getListParam = {};
            getListParam.id = '';
            getListParam.country_id = $scope.cityModel.country_id;
            $scope.isLoadedStateList = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.cityModel.state_list = data.list;
                $scope.isLoadedStateList = true;
            });
        };
        
         $scope.CountryChange = function () 
        {
            $scope.cityModel.state_id = '';
            $scope.getstateList();
            
        }
        $scope.getcountryList();
    }]);









