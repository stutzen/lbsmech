





app.controller('transexpenseCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.transexpenseModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            user: '',
            id: '',
            date: '',
            particular: '',
            category: '',
            created_by: '',
            incomeCategoryInfo: '',
            amount: '',
            is_active: 1,
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };
        $scope.searchFilter = {
            accountid: '',
            customerid: '',
            limit: '',
            start: '',
            fromdate: '',
            todate: ''
        };

        $scope.pagePerCount = [50, 100];

        $scope.transexpenseModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date();
      //  $scope.searchFilter.fromdate = $scope.currentDate;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.validationFactory = ValidationFactory;
        $scope.adminService = adminService;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };



        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.selectTransexpenseId = '';
        $scope.selectTransexpenseVoucher_number = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id, voucherno)
        {
            $scope.selectTransexpenseId = id;
            $scope.selectTransexpenseVoucher_number = voucherno;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                incomeCategoryInfo: '',
                user: ''
            };
           // $scope.searchFilter.fromdate = $filter('date')($scope.currentDate, $scope.dateFormat);
            $scope.initTableFilter();
        }

        $scope.deleteTransexpenseInfo = function( )
        {

            if ($scope.isdeleteLoadingProgress == false)
            {

                $scope.isdeleteLoadingProgress = true;

                if ($scope.selectTransexpenseVoucher_number === '' || $scope.selectTransexpenseVoucher_number == 0)
                {
                     var headers = {};
                    headers['screen-code'] = 'transexpense';
                    
                    var getListParam = {};
                    getListParam.id = $scope.selectTransexpenseId;
                    adminService.deleteTransexpense(getListParam, getListParam.id, headers).then(function(response)
                    {
                        var data = response.data;
                        if (data.success === true)
                        {
                            $scope.closePopup();
                            $scope.getList();
                        }
                         $scope.isdeleteLoadingProgress = false;
                    });
                }
                else
                {
                    var getListParam = {};
                    getListParam.id = $scope.selectTransexpenseVoucher_number;
                    var headers = {};
                    headers['screen-code'] = 'transexpense';
                    adminService.deleteTransexpensePayment(getListParam, getListParam.id, headers).then(function(response)
                    {
                        var data = response.data;
                        if (data.success === true)
                        {
                            $scope.closePopup();
                            $scope.getList();
                        }
                         $scope.isdeleteLoadingProgress = false;
                    });
                }
            }

        };
        $scope.$watch('searchFilter.incomeCategoryInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });
        $scope.getList = function() {

            $scope.transexpenseModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'transexpense';

            getListParam.from_Date = $scope.searchFilter.fromdate;
            getListParam.to_Date = $scope.searchFilter.todate;

            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.adminService.appConfig.date_format);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.adminService.appConfig.date_format);

            }

            getListParam.id = '',
             getListParam.account_id = '';
            getListParam.customer_id = '';
            getListParam.name = '';
            getListParam.created_by = $scope.searchFilter.user;
            if ($scope.searchFilter.incomeCategoryInfo != null && typeof $scope.searchFilter.incomeCategoryInfo != 'undefined' && typeof $scope.searchFilter.incomeCategoryInfo.id != 'undefined')
            {
                getListParam.account_category = $scope.searchFilter.incomeCategoryInfo.name;
            }
            else
            {
                getListParam.account_category = '';
            }
            getListParam.start = ($scope.transexpenseModel.currentPage - 1) * $scope.transexpenseModel.limit;
            getListParam.limit = $scope.transexpenseModel.limit;
            getListParam.is_active = 1;
            $scope.transexpenseModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getTransexpenseList(getListParam, configOption, headers).then(function(response) {
//                var balanceamt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.transexpenseModel.list = data;
                    for (var i = 0; i < $scope.transexpenseModel.list.length; i++)
                    {
                        $scope.transexpenseModel.list[i].newdate = utilityService.parseStrToDate($scope.transexpenseModel.list[i].transaction_date);
                        $scope.transexpenseModel.list[i].transaction_date = utilityService.parseDateToStr($scope.transexpenseModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                    }
                    $scope.transexpenseModel.total = data.total;
                }
                $scope.transexpenseModel.isLoadingProgress = false;
            });

        };
        $scope.getIncomeCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.type = 'expense';
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatincomeCategoryModel = function(model) {

            if (model !== null && model != undefined)
            {

                return model.name;
            }
            return  '';
        };
        $scope.getUserList = function() {

            var getListParam = {};
            getListParam.date = '';
            getListParam.particular = '';
            getListParam.category = '';
            getListParam.credited_by = '';
            getListParam.amount = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function(response) {
                var data = response.data;
                $scope.transexpenseModel.userlist = data.list;

            });

        };
        $scope.getListPrint = function(val)
        {
            var getListParam = {};
            getListParam.Id = "";
            getListParam.start = 0;
            $scope.transexpenseModel.isLoadingProgress = true;
            adminService.getTransexpenseList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.transexpenseModel.printList = data;
                $scope.transexpenseModel.isLoadingProgress = false;
            });
        };


        $scope.getList();
        $scope.getUserList();
        //$scope.getListPrint();
    }]);




