

app.controller('mointernalreceivedreportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.moModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            productname: '',
            uom: '',
            quantity: '',
            amount: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.moModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0)); 
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.company_name = $rootScope.appConfig.app_tile;
        $scope.company_address = $rootScope.appConfig.pay_to_address;
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            customerInfo : {}
        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                customerInfo : ''
            };
            $scope.moModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
        }

        $scope.print = function()
        {
            $window.print();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.show = function(index)
        {
                    if ($scope.showDetails[index])
                    {
                        $scope.showDetails[index] = false;
                    }
                    else
                    {    
                        $scope.showDetails[index] = true;
                    }
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'mointernalreceivedreport';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            }
            else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            }
            else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }

            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.moModel.isLoadingProgress = true;
            $scope.showDetails = [];
            adminService.getMOInternalReceivedReport(getListParam, headers).then(function(response)
            {
                var totalQty = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.moModel.list = data;

                    for (var i = 0; i < $scope.moModel.list.length; i++)
                    {
                        totalQty += $scope.moModel.list[i].qty;
                        $scope.moModel.list[i].newdate = utilityService.parseStrToDate($scope.moModel.list[i].date);
                        $scope.moModel.list[i].date = utilityService.parseDateToStr($scope.moModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        $scope.moModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                        for (var j = 0; j < $scope.moModel.list[i].detail.length; j++)
                        {
                            $scope.moModel.list[i].detail[j].flag = i;
                        }
                    }
                    $scope.moModel.total_Amount = totalQty;
                    $scope.moModel.total_Amount = parseFloat($scope.moModel.total_Amount).toFixed(2);
                    $scope.moModel.total_Amount = utilityService.changeCurrency($scope.moModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                }
                $scope.moModel.isLoadingProgress = false;
            });

        };

        $scope.getList();
    }]);




