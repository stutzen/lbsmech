




app.controller('deliveryInstructionEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.deliveryModel =
                {
                    "id": 0,
                    "transportId": '',
                    "driverName": '',
                    "driverMobileNo": '',
                    "vehicleType": "",
                    "vehicleNo": '',
                    "status": "new",
                    "type": '',
                    "finalDestination": '',
                    "shipmentDate": '',
                    "deliveryDate": '',
                    "billingUnit": '',
                    "store": '',
                    "transportList": [],
                    "transportInfo": {},
                    "invoiceList": [],
                    "list": {},
                    "unMappedLimit": '',
                    "unMappedtotal": '',
                    "unMappedcurrentPage": 1,
                    "isLoadingProgress": false,
                    "mappedInvoiceList": []
                };

        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.pagePerCount = [10, 20, 50, 100];
        $scope.deliveryModel.unMappedLimit = $scope.pagePerCount[0];

        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.edit_deliveryinstruction_form != 'undefined' && typeof $scope.edit_deliveryinstruction_form.$pristine != 'undefined' && !$scope.edit_deliveryinstruction_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.isDataSavingProcess = false;
        $scope.isUnMappedInvoiceLoading = false;
        $scope.isMappedInvoiceLoading = false;

        $scope.shipmentDateOpen = false;
        $scope.deliveryDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 1)
            {
                $scope.shipmentDateOpen = true;
                $scope.deliveryDateOpen = false;
            }
            if (index == 2)
            {
                $scope.deliveryDateOpen = true;
                $scope.shipmentDateOpen = false;
            }
        };

        $scope.tabChange = function (value) {
            if (value == 0)
            {
                $scope.getUnMappedInvoiceList();
            }
            if (value == 1)
            {
                $scope.getMappedInvoiceList();
            }
            $scope.currentStep = value;
        };

        $scope.formReset = function ()
        {
            $scope.edit_deliveryinstruction_form.$setPristine();
            $scope.initUpdateDetail();
        };

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            $scope.initUpdateDetailTimeoutPromise = $timeout($scope.updateDeliveryInfo, 300);
        }

        $scope.selectedInvoiceId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectedInvoiceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteAdvancePaymentItem = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;
                var deleteparam = [];
                var getListParam = {};
                getListParam.id = $scope.selectedInvoiceId;
                deleteparam.push(getListParam);
                var headers = {};
                headers['screen-code'] = 'deliveryinstruction';
                adminService.deleteDeliveryInstructionInvoiceDetail(deleteparam, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $timeout(function () {
                            $scope.getUnMappedInvoiceList();
                        }, 200);
                        $timeout(function () {
                            $scope.getMappedInvoiceList();
                        }, 200);
                    }

                    $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.updateDeliveryInfo = function ()
        {
            $scope.deliveryModel.transportId = $scope.deliveryModel.list.transport_id;
            $scope.deliveryModel.driverName = $scope.deliveryModel.list.driver_name;
            $scope.deliveryModel.driverMobileNo = $scope.deliveryModel.list.driver_mobile_no;
            $scope.deliveryModel.vehicleType = $scope.deliveryModel.list.vehicle_type;
            $scope.deliveryModel.vehicleNo = $scope.deliveryModel.list.vehicle_no;
            $scope.deliveryModel.status = $scope.deliveryModel.list.status;
            $scope.deliveryModel.type = $scope.deliveryModel.list.type;
            $scope.deliveryModel.finalDestination = $scope.deliveryModel.list.final_destination;
            $scope.deliveryModel.shipmentDate = utilityService.parseStrToDate($scope.deliveryModel.list.shipment_date);
            $scope.deliveryModel.deliveryDate = utilityService.parseStrToDate($scope.deliveryModel.list.delivery_date);
            $scope.deliveryModel.billingUnit = $scope.deliveryModel.list.billing_unit;
            $scope.deliveryModel.store = $scope.deliveryModel.list.store;
            $scope.deliveryModel.transportInfo =
                    {
                        id: $scope.deliveryModel.list.transport_id,
                        name: $scope.deliveryModel.list.transport_name
                    };
            $scope.deliveryModel.isLoadingProgress = false;
        };

        $scope.getTransportList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.TRANSPORT_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatTransportModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined && model.mobile_no != undefined && model.email != undefined)
                {
                    return model.name + '(' + model.mobile_no + ',' + model.email + ')';
                } else if (model.name != undefined && model.mobile_no != undefined)
                {
                    return model.name + '(' + model.mobile_no + ')';
                } else
                {
                    return model.name;
                }
            }
            return '';
        };

        $scope.modifyDeliveryInstruction = function ()
        {
            var createAdvanceParam = {};

            if (typeof $scope.deliveryModel.shipmentDate == 'object')
            {
                $scope.deliveryModel.shipmentDate = utilityService.parseDateToStr($scope.deliveryModel.shipmentDate, 'yyyy-MM-dd');
            }
            createAdvanceParam.shipment_date = utilityService.changeDateToSqlFormat($scope.deliveryModel.shipmentDate,'yyyy-MM-dd');
            if (typeof $scope.deliveryModel.deliveryDate == 'object')
            {
                $scope.deliveryModel.deliveryDate = utilityService.parseDateToStr($scope.deliveryModel.deliveryDate, 'yyyy-MM-dd');
            }
            createAdvanceParam.delivery_date = utilityService.changeDateToSqlFormat($scope.deliveryModel.deliveryDate, 'yyyy-MM-dd');
            if ($scope.deliveryModel.transportInfo != null && typeof $scope.deliveryModel.transportInfo != 'undefined' && typeof $scope.deliveryModel.transportInfo.id != 'undefined')
            {
                createAdvanceParam.transport_id = $scope.deliveryModel.transportInfo.id;
            } else
            {
                createAdvanceParam.transport_id = '';
            }
            createAdvanceParam.driver_name = $scope.deliveryModel.driverName;
            createAdvanceParam.driver_mobile_no = $scope.deliveryModel.driverMobileNo;
            createAdvanceParam.vehicle_type = $scope.deliveryModel.vehicleType;
            createAdvanceParam.vehicle_no = $scope.deliveryModel.vehicleNo;
            createAdvanceParam.status = $scope.deliveryModel.status;
            createAdvanceParam.type = $scope.deliveryModel.type;
            createAdvanceParam.final_destination = $scope.deliveryModel.finalDestination;
            createAdvanceParam.billing_unit = $scope.deliveryModel.billingUnit;
            createAdvanceParam.store = $scope.deliveryModel.store;

            createAdvanceParam.is_active = 1;
            var headers = {};
            headers['screen-code'] = 'deliveryinstruction';
            adminService.modifyDeliveryInstruction(createAdvanceParam, $stateParams.id, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.deliveryInstruction');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getList = function () {
            $scope.deliveryModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDeliveryInstructionDetail(getListParam, configOption).then(function (response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.deliveryModel.list = data.data[0];
                }
                $scope.initUpdateDetail();

            });
        };

        $scope.getUnMappedInvoiceList = function ()
        {
            var getListParam = {};
            $scope.isUnMappedInvoiceLoading = true;
            $scope.deliveryModel.invoiceList = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUnMappedDIInvoiceList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.data;
                    if (data.length > 0)
                    {
                        $scope.deliveryModel.invoiceList = data;
                        for (var i = 0; i < $scope.deliveryModel.invoiceList.length; i++)
                        {
                            $scope.deliveryModel.invoiceList[i].isChecked = false;
                            var newrefdate = utilityService.parseStrToDate($scope.deliveryModel.invoiceList[i].date);
                            $scope.deliveryModel.invoiceList[i].date = newrefdate;
                            var newduedate = utilityService.parseStrToDate($scope.deliveryModel.invoiceList[i].duedate);
                            $scope.deliveryModel.invoiceList[i].duedate = newduedate;
                        }
                    } else
                    {
                        $scope.deliveryModel.invoiceList = [];
                    }
                    $scope.deliveryModel.unMappedtotal = response.data.total;
                    $scope.isUnMappedInvoiceLoading = false;
                }
            });
        };

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };

        $scope.searchFilter =
                {
                    ref_id: '',
                    customerInfo: {}
                };

        $scope.initGetMappedListTimeoutPromise = null;

        $scope.initGetMappedList = function ()
        {
            if ($scope.initGetMappedListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initGetMappedListTimeoutPromise);
            }
            $scope.initGetMappedListTimeoutPromise = $timeout($scope.getMappedInvoiceList, 400);
        };

        $scope.getMappedInvoiceList = function ()
        {
            var getListParam = {};
            getListParam.delivery_instruction_id = $stateParams.id;
            getListParam.ref_id = $scope.searchFilter.ref_id;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }
            $scope.isMappedInvoiceLoading = true;
            $scope.deliveryModel.mappedInvoiceList = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDeliveryInstructionDetailInvoiceList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    if (data.length > 0)
                    {
                        $scope.deliveryModel.mappedInvoiceList = data;
                        for (var i = 0; i < $scope.deliveryModel.mappedInvoiceList.length; i++)
                        {
                            var newduedate = utilityService.parseStrToDate($scope.deliveryModel.mappedInvoiceList[i].ref_date);
                            $scope.deliveryModel.mappedInvoiceList[i].ref_date = newduedate;
                            $scope.deliveryModel.mappedInvoiceList[i].isChecked = true;
                            $scope.calculatetotal(i);
                        }
                    } else
                    {
                        $scope.deliveryModel.mappedInvoiceList = [];
                    }
                    $scope.deliveryModel.Mappedtotal = response.data.total;
                    $scope.isMappedInvoiceLoading = false;
                }
            });
        };

        $scope.addDeliveryInvoiceMapping = function ()
        {
            var invoiceMapping = [];
            for (var i = 0; i < $scope.deliveryModel.invoiceList.length; i++)
            {
                if ($scope.deliveryModel.invoiceList[i].isChecked == true)
                {
                    var temp = 0;
                    if ($scope.deliveryModel.invoiceList[i].customattribute.length > 0)
                    {
                        for (var j = 0; j < $scope.deliveryModel.invoiceList[i].customattribute.length; j++)
                        {
                            if ($scope.deliveryModel.invoiceList[i].customattribute[j].attribute_code == 'no_of_bundles')
                            {
                                temp = $scope.deliveryModel.invoiceList[i].customattribute[j].value;
                            }
                        }
                    }
                    var invoiceParam = {};
                    invoiceParam.id = 0;
                    invoiceParam.delivery_instruction_id = $stateParams.id;
                    invoiceParam.ref_id = $scope.deliveryModel.invoiceList[i].id;
                    invoiceParam.ref_type = 1;
                    invoiceParam.freight = 0;
                    invoiceParam.driver_beta = 0;
                    invoiceParam.net_weight = 0;
                    invoiceParam.gross_weight = 0;
                    invoiceParam.volume = 0;
                    invoiceParam.no_of_pkts = temp;
                    invoiceParam.unloading = 0;
                    invoiceParam.drop_seq = 0;
                    invoiceParam.billing_unit = 0;
                    invoiceParam.store = 0;
                    invoiceParam.row_total = 0;
                    invoiceMapping.push(invoiceParam);
                }
            }
            if (invoiceMapping.length > 0)
            {
                var headers = {};
                headers['screen-code'] = 'deliveryinstruction';
                adminService.addDeliveryInstructionInvoiceDetail(invoiceMapping, headers).then(function (response)
                {
                    if (response.data.success == true)
                    {
                        $scope.getUnMappedInvoiceList();
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateRowtotal = function (index)
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal(index), 300);
        }

        $scope.calculatetotal = function (index)
        {
            $scope.deliveryModel.mappedInvoiceList[index].row_total = 0;
            var row_total = 0;
            var driver = parseFloat($scope.deliveryModel.mappedInvoiceList[index].driver_beta);
            var unloading = parseFloat($scope.deliveryModel.mappedInvoiceList[index].unloading);
            var freight = parseFloat($scope.deliveryModel.mappedInvoiceList[index].freight);
            var pkts = parseFloat($scope.deliveryModel.mappedInvoiceList[index].no_of_pkts);
            row_total = parseFloat((pkts * freight) + (pkts * unloading) + driver);
            $scope.deliveryModel.mappedInvoiceList[index].row_total = row_total.toFixed(2);
        }

        $scope.updateMappedInvoice = function ()
        {
            var invoiceMapping = [];
            for (var i = 0; i < $scope.deliveryModel.mappedInvoiceList.length; i++)
            {
                if ($scope.deliveryModel.mappedInvoiceList[i].isChecked == true)
                {
                    var invoiceParam = {};
                    invoiceParam.id = $scope.deliveryModel.mappedInvoiceList[i].id;
                    invoiceParam.delivery_instruction_id = $scope.deliveryModel.mappedInvoiceList[i].delivery_instruction_id;
                    invoiceParam.ref_id = $scope.deliveryModel.mappedInvoiceList[i].ref_id;
                    invoiceParam.ref_type = $scope.deliveryModel.mappedInvoiceList[i].ref_type;
                    invoiceParam.freight = $scope.deliveryModel.mappedInvoiceList[i].freight;
                    invoiceParam.driver_beta = $scope.deliveryModel.mappedInvoiceList[i].driver_beta;
                    invoiceParam.net_weight = $scope.deliveryModel.mappedInvoiceList[i].net_weight;
                    invoiceParam.gross_weight = $scope.deliveryModel.mappedInvoiceList[i].gross_weight;
                    invoiceParam.volume = $scope.deliveryModel.mappedInvoiceList[i].volume;
                    invoiceParam.no_of_pkts = $scope.deliveryModel.mappedInvoiceList[i].no_of_pkts;
                    invoiceParam.unloading = $scope.deliveryModel.mappedInvoiceList[i].unloading;
                    invoiceParam.drop_seq = $scope.deliveryModel.mappedInvoiceList[i].drop_seq;
                    invoiceParam.billing_unit = $scope.deliveryModel.mappedInvoiceList[i].billing_unit;
                    invoiceParam.store = $scope.deliveryModel.mappedInvoiceList[i].store;
                    invoiceParam.row_total = $scope.deliveryModel.mappedInvoiceList[i].row_total;
                    invoiceMapping.push(invoiceParam);
                }
            }
            if (invoiceMapping.length > 0)
            {
                var headers = {};
                headers['screen-code'] = 'deliveryinstruction';
                adminService.addDeliveryInstructionInvoiceDetail(invoiceMapping, headers).then(function (response)
                {
                    if (response.data.success == true)
                    {
                        $timeout(function () {
                            $scope.getUnMappedInvoiceList();
                        }, 200);
                        $timeout(function () {
                            $scope.getMappedInvoiceList();
                        }, 200);
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getList();
        $scope.getUnMappedInvoiceList();
        $scope.getMappedInvoiceList();
    }]);




