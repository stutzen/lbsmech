app.controller('issuescreenEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', '$stateParams', '$state', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, $stateParams, $state) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.issueModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            userList: [],
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            issueDetail: [],
            issueDate: '',
            employee: {}
        };
        $scope.isDataSavingProcess = false;
        $scope.isIssueDataSavingProcess = false;
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.issueModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.searchFilter =
                {
                    id: '',
                    bomTaskInfo: {},
                };

        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.issueModel.newIssue = 0;
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getWorkOrderList, 300);
        }

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter.bomTaskInfo = '';
            $scope.noResults = false;
            $scope.issueModel.list = [];
            //$scope.initTableFilter();
        };
        $scope.clearFilters = function ()
        {
            $scope.initTableFilter();
        }
        $scope.startDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
        }

        $scope.getTaskList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.WORK_ORDER_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatTaskModel = function (model)
        {
            if (model != null)
            {
                return model.bomName;
            }
            return  '';
        };
        $scope.validateCurrentIssue = function (index)
        {
            var retVal = false;
            if (($scope.issueModel.list[index].newIssue <= parseInt($scope.issueModel.list[index].balance)) || ($scope.issueModel.list[index].newIssue == 0))
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.updateIssueListInfo = function ()
        {
            $scope.searchFilter.bomTaskInfo = {
                id: $scope.issueModel.issueDetail.mo_id,
                bomName: $scope.issueModel.issueDetail.bom_name
            }
            $scope.issueModel.employee = {
                id: $scope.issueModel.issueDetail.responsible_by,
                f_name: $scope.issueModel.issueDetail.emp_name
            }
//            $scope.issueModel.responsible_person = $scope.issueModel.issueDetail.responsible_by;
            $scope.issueModel.issueDate = utilityService.parseStrToDate($scope.issueModel.issueDetail.date, adminService.appConfig.date_format);
            for (var i = 0; i < $scope.issueModel.issueDetail.issue_detail.length; i++)
            {
                var newRow = {
                    "id": $scope.issueModel.issueDetail.issue_detail[i].id,
                    "product_id": $scope.issueModel.issueDetail.issue_detail[i].product_id,
                    "input_pdt_name": $scope.issueModel.issueDetail.issue_detail[i].product_name,
                    "to_consume": $scope.issueModel.issueDetail.issue_detail[i].consumed_qty,
                    "reserved_qty": $scope.issueModel.issueDetail.issue_detail[i].issued_qty,
                    "newIssue": $scope.issueModel.issueDetail.issue_detail[i].issued_qty,
                    "balance": $scope.issueModel.issueDetail.issue_detail[i].balance_qty,
                    "is_active": $scope.issueModel.issueDetail.issue_detail[i].is_active
                }
                $scope.issueModel.list.push(newRow);
            }
        };

        $scope.getList = function () {
            var getListParam = {};
            $scope.issueModel.isLoadingProgress = true;
            getListParam.id = $stateParams.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getIssueDetail(getListParam, configOption).then(function (response) {
                var data = response.data;
                $scope.issueModel.issueDetail = data.list[0];
                $scope.issueModel.isLoadingProgress = false;
                $scope.updateIssueListInfo();
            });
        };
        $scope.getWorkOrderList = function () {
            var getListParam = {};
            $scope.issueModel.isSearchLoadingProgress = true;
            $scope.issueModel.isLoadingProgress = true;
            $scope.issueModel.isDataSavingProcess = true;
            if ($scope.searchFilter.bomTaskInfo != null && typeof $scope.searchFilter.bomTaskInfo != 'undefined' && typeof $scope.searchFilter.bomTaskInfo.id != 'undefined')
            {
                getListParam.moId = $scope.searchFilter.bomTaskInfo.id;
            } else
            {
                getListParam.moId = '';
            }
            getListParam.isActive = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getWorkOrderInputList(getListParam, configOption).then(function (response) {
                var data = response.data;
                if (data.total !== 0)
                {
                    $scope.issueModel.list = data.list;
                    for (var i = 0; i < $scope.issueModel.list.length; i++)
                    {
                        if ($scope.issueModel.list[i].reserved_qty != 'undefined' && $scope.issueModel.list[i].to_consume != 'undefined')
                        {
                            $scope.issueModel.list[i].balance = parseFloat($scope.issueModel.list[i].to_consume) - parseFloat($scope.issueModel.list[i].reserved_qty);
                            $scope.issueModel.list[i].newIssue = 0;
                        } else {
                            $scope.issueModel.list[i].balance = 0;
                        }
                        if ($scope.issueModel.list[i].to_consume != 'undefined' && $scope.issueModel.list[i].to_consume != '' && $scope.issueModel.list[i].to_consume != null && $scope.issueModel.list[i].quantity != '' && $scope.issueModel.list[i].quantity != null)
                        {
                            if ($scope.issueModel.list[i].to_consume < $scope.issueModel.list[i].quantity)
                            {
                                $scope.isEditable = true;
                            }
                        }
                    }
                }
                $scope.issueModel.isSearchLoadingProgress = false;
                $scope.issueModel.isLoadingProgress = false;
                $scope.issueModel.isDataSavingProcess = false;
            });
        };
        $scope.modifyIssueQty = function ()
        {
            var getListParam = {};
//            getListParam.responsible_by = $scope.searchFilter.bomTaskInfo.userName;
            getListParam.mo_id = $scope.searchFilter.bomTaskInfo.id;
            if (typeof $scope.issueModel.issueDate == 'object')
            {
                $scope.issueModel.issueDate = utilityService.parseDateToStr($scope.issueModel.issueDate, 'yyyy-MM-dd');
            }
            if ($scope.issueModel.employee != 'undefined' && $scope.issueModel.employee != '' && $scope.issueModel.employee != undefined && $scope.issueModel.employee.id != '')
            {
                getListParam.responsible_by = $scope.issueModel.employee.id;
            } else {
                getListParam.responsible_by = '';
            }
            getListParam.date = utilityService.changeDateToSqlFormat($scope.issueModel.issueDate, 'yyyy-MM-dd');
            getListParam.status = 'new';
            getListParam.comments = '';
            getListParam.issue_detail = [];
            for (var i = 0; i < $scope.issueModel.list.length; i++)
            {
                var getList = {};
                getList.product_id = $scope.issueModel.list[i].product_id;
                getList.work_order_input_id = $scope.issueModel.list[i].id;
                getList.product_name = $scope.issueModel.list[i].input_pdt_name;
                getList.consumed_qty = $scope.issueModel.list[i].to_consume;
                getList.issued_qty = $scope.issueModel.list[i].newIssue;
                getList.balance_qty = $scope.issueModel.list[i].balance;
                getList.status = '';
                getList.comments = '';
                getListParam.issue_detail.push(getList);
            }
            $scope.isIssueDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.modifyIssue(getListParam, $stateParams.id, configOption).then(function (response) {
                var data = response.data;
                if (data.success === true)
                {
                    $state.go('app.issuescreen');
                }
                $scope.refreshScreen();
                $scope.isIssueDataSavingProcess = false;
            });
        };
        $scope.updateResponsiblePerson = function ()
        {
            $scope.issueModel.responsible_person = $scope.searchFilter.bomTaskInfo.userName;
        }

        $scope.getEmployeeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });

        };
        $scope.formatEmployeeModel = function (model)
        {
            if (model != null)
            {
                if (model.f_name != undefined)
                {
                    return model.f_name;
                }
            }
            return  '';
        };
        $scope.getList();
    }]);






