


app.controller('paymentSettlementPrintCtrl', ['$scope', '$rootScope', 'adminService', 'APP_CONST', '$httpService', '$filter', 'Auth', '$state','$stateParams', '$timeout', function($scope, $rootScope, adminService, APP_CONST, $httpService, $filter, Auth, $state,$stateParams, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.paymentModel = {
            currentPage: 1,
            id: '',
            total: 0,
            limit: 10,
            userid: '',
            clientId: '',
            list: [],
            isSettled: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            userInfo: '',
            clientInfo: ''
        };
        $scope.pagePerCount = [50, 100];
        $scope.paymentModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
            clientemail: '',
            userInfo: '',
            clientInfo: '',
            payment_type: ''
        };
        $scope.currentDate = new Date();
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }




        $scope.formatUserModel = function(model) {

            if (model != null)
            {
                return model.username;
            }
            return  '';
        };



        $scope.getUser = function() {

            var getListParam = {};
            getListParam.userid = $stateParams.id;
            adminService.getUserList(getListParam).then(function(response) {
                var data = response.data.data;
                if (data.list.length != 0)
                {
                    $scope.paymentModel.userInfo = data[0];
                     $timeout(function() {
                        $scope.print()
                    }, 500);
                }
               
            });
        };



        
        $scope.print = function(){
             window.print();
        }
	
        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = "";
            getListParam.companyId = $rootScope.userModel.companyId;
            getListParam.userid = $stateParams.id;
            getListParam.clientId = '';
            getListParam.clientEmail = '';
            getListParam.isSettled = 0;
            $scope.paymentModel.isLoadingProgress = true;
            adminService.getPaymentList(getListParam).then(function(response) {
                var data = response.data.data;
                $scope.paymentModel.list = data.list;
                $scope.paymentModel.total = data.total;
                $scope.paymentModel.isLoadingProgress = false;
                $scope.getUser();
            });
        };
        $scope.getList();

       
    }]);








