





app.controller('bonusReportListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.bonusReportListModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            TeamList: [],
            date: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.bonusReportListModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            empCode: "",
            fromdate: '',
            todate: '',
            teamInfo: '',         

        };
        $scope.searchFilter.todate = $scope.currentDate;
        
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                empCode:'',
                fromdate: '',
                todate: '',
                teamInfo: ''

            };
            $scope.bonusReportListModel.list = [];
            $scope.searchFilter.todate = $scope.currentDate;

        }


        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

       /* $scope.$watch('searchFilter.teamInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                {
                    $scope.initTableFilter();
                }
            }
        });*/

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empBonus';
            //getListParam.team_id = $scope.searchFilter.teamInfo;
            getListParam.emp_code=$scope.searchFilter.empCode;
            getListParam.date = $scope.searchFilter.fromdate;
            //getListParam.to_date = $scope.searchFilter.todate;

            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            /*if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }*/

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.date = utilityService.changeDateToSqlFormat(getListParam.date, $scope.dateFormat);

            }
            /*if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }*/
            
            $scope.bonusReportListModel.isLoadingProgress = true;
            $scope.bonusReportListModel.isSearchLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getBonusList(getListParam, configOption, headers).then(function(response)
            {
                
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.bonusReportListModel.list = data;
                    for (var i = 0; i < $scope.bonusReportListModel.list.length; i++)
                    {
                        $scope.bonusReportListModel.list[i].newfromdate = utilityService.parseStrToDate($scope.bonusReportListModel.list[i].from_date);
                        $scope.bonusReportListModel.list[i].from_date = utilityService.parseDateToStr($scope.bonusReportListModel.list[i].newfromdate, $scope.adminService.appConfig.date_format);
                        $scope.bonusReportListModel.list[i].newtodate = utilityService.parseStrToDate($scope.bonusReportListModel.list[i].to_date);
                        $scope.bonusReportListModel.list[i].to_date = utilityService.parseDateToStr($scope.bonusReportListModel.list[i].newtodate, $scope.adminService.appConfig.date_format);
                        
                    }
                    $scope.bonusReportListModel.total = data.total;
                    
                }
                $scope.bonusReportListModel.isLoadingProgress = false;
                $scope.bonusReportListModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getTeamList = function ()
        {
            $scope.bonusReportListModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeTeamList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.bonusReportListModel.TeamList = data.list;
                }
                $scope.bonusReportListModel.isLoadingProgress = false;
            });
        };

        $scope.print = function()
        {
            $window.print();
        };
        console.log("inside");
        $scope.getList();
        console.log("middle")
        $scope.getTeamList();
        console.log("outside");
    }]);




