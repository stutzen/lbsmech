




app.controller('productionAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', 'sweet', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, sweet, $stateParams, $timeout, ValidationFactory) {

        $scope.productionModel = {
            "date": "",
            "next_date": "",
            "teamList": [],
            "empList": [],
            "teamId": '',
            "user_id": "",
            "user_name": "",
            "userInfo": ""
        };
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.adminService = adminService;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_production_form !== 'undefined' && typeof $scope.create_production_form.$pristine !== 'undefined' && !$scope.create_production_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            $scope.create_production_form.$setPristine();
            $scope.productionModel.date = "";
            $scope.productionModel.next_date = "";
            $scope.productionModel.teamId = true;
            $scope.productionModel.empList = [];
            $scope.productionModel.user_id = '';
            $scope.productionModel.user_name = '';
            $scope.productionModel.userInfo = "";
            //  $scope.setDate();
        };

//    $scope.setDate = function() {
//             // $scope.productionModel.date = previousDay.setDate(myDate.getDate()-1);
//            var yesterday = new Date();
//            $scope.productionModel.date = yesterday.setDate(yesterday.getDate() - 1);
//           // $scope.productionModel.date = moment(new Date()).add(-1, 'day');
//            $scope.productionModel.next_date = new Date();
//            var newdate = utilityService.parseStrToDate($scope.productionModel.date,$scope.dateFormat);
//            $scope.newdate = newdate;
//            $scope.productionModel.date = utilityService.parseDateToStr($scope.newdate, $scope.dateFormat);
//            var newnextdate = utilityService.parseStrToDate($scope.productionModel.next_date);
//            $scope.newnextdate = newnextdate;
//            $scope.productionModel.next_date = utilityService.parseDateToStr(newnextdate, $scope.dateFormat);
//           $scope.getnoProduction ();
//    };

        $scope.isDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 1)
            {
                $scope.isDateOpen = true;
            }
        }

//    $scope.getEmployeeTeam = function()
//    {
//        var getListParam = {};
//        var configOption = adminService.handleOnlyErrorResponseConfig;
//        adminService.getEmployeeTeamList(getListParam, configOption).then(function(response) {
//            if (response.data.success === true)
//            {
//                var data = response.data.list;
//                $scope.productionModel.teamList = data;
//            }
//        });
//    };
        $scope.getnoProduction = function ()
        {
            var getListParam = {};
            getListParam.date = utilityService.changeDateToSqlFormat($scope.productionModel.date, 'yyyy-mm-dd');
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDuplicateDetail(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.productionModel.teamList = data.list;
                }
            });
        };
        $scope.getnoProduction();

        $scope.getEmployeeList = function ()
        {
            if ($scope.productionModel.teamId != 'undefined' && $scope.productionModel.teamId != '' && $scope.productionModel.teamId != null)
            {
                var getListParam = {};
                getListParam.team_id = $scope.productionModel.teamId;
                getListParam.is_active = 1;
                $scope.productionModel.empList = [];
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.getEmployeeList(getListParam, configOption).then(function (response) {
                    if (response.data.success === true)
                    {
                        var data = response.data;
                        $scope.productionModel.empList = data.list;
                        for (var i = 0; i < $scope.productionModel.empList.length; i++)
                        {
                            $scope.productionModel.empList[i].is_present = false;
                            $scope.productionModel.empList[i].details = [];
                            $scope.productionModel.empList[i].allowance = [];
                            $scope.productionModel.empList[i].allowance_amount = 0;
                            $scope.productionModel.empList[i].amount = 0;
                            $scope.productionModel.empList[i].index = i;
                        }
                    }
                });
            } else
            {
                $scope.productionModel.empList = [];
            }
        };

        $scope.deleteTask = function (empIndex, index)
        {
            $scope.productionModel.empList[empIndex].details.splice(index, 1);
            $scope.updateEmployeeSalary(empIndex);
        };

        $scope.changePresentStatus = function (i)
        {
            if ($scope.productionModel.empList[i].is_present == false)
            {
                $scope.productionModel.empList[i].details = [];
                $scope.productionModel.empList[i].amount = 0;
            } else
            {
                $scope.addNewTask(i);
            }
        };

        $scope.addNewTask = function (i)
        {
            var type = $scope.productionModel.empList[i].salary_type;
            var newRow = {
                empIndex: i,
                taskId: '',
                name: '',
                type: type,
                duration: '1',
                output_pdt_id: '',
                product_for_id: '',
                product_for_name: '',
                production_qty: '',
                production_unit_price: '',
                task_amount: 0,
                inputs: []
            };
            $scope.productionModel.empList[i].details.push(newRow);
            $scope.calculatetotal(i);
        };

        $scope.getTaskList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCTION_TASK_DETAIL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatTaskModel = function (model, index, empIndex)
        {
            if (model != null)
            {
                //$scope.updateTaskInfo(model, index, empIndex);
                //$scope.getProdTaskDetail(index,empIndex)
                return model.name;
            }
            return  '';
        }

        $scope.getProdTaskDetail = function (index, empIndex)
        {
            var getListParam = {};
            var headers = {};
            if ($scope.productionModel.empList[empIndex].details[index].taskInfo.id != null && $scope.productionModel.empList[empIndex].details[index].taskInfo.id != 'undefined' && $scope.productionModel.empList[empIndex].details[index].taskInfo.id != '')
            {
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                getListParam.is_active = 1;
                getListParam.id = $scope.productionModel.empList[empIndex].details[index].taskInfo.id;
                adminService.getProdTaskDetail(getListParam, configOption, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var taskdata = data.list[0];
                        $scope.updateTaskInfo(taskdata, index, empIndex);
                    }
                });
            }
        };

        $scope.updateTaskInfo = function (model, index, empIndex)
        {
            if ($scope.productionModel.empList.length != 0 && $scope.productionModel.empList[empIndex].details.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.productionModel.empList[empIndex].details[index].id = model.id;
                $scope.productionModel.empList[empIndex].details[index].name = model.name;
                $scope.productionModel.empList[empIndex].details[index].output_pdt_id = model.output_pdt_id;
                $scope.productionModel.empList[empIndex].details[index].product_for_name = model.product_for_name;
                $scope.productionModel.empList[empIndex].details[index].production_qty = 1;
                $scope.productionModel.empList[empIndex].details[index].output_pdt_name = model.output_pdt_name;
                $scope.productionModel.empList[empIndex].details[index].output_pdt_uom = model.output_pdt_uom;
                $scope.productionModel.empList[empIndex].details[index].production_unit_price = model.unit_production_price;
                $scope.productionModel.empList[empIndex].details[index].input = [];
                for (var i = 0; i < model.input.length; i++)
                {
                    var newRow = {
                        config_input_id: model.input[i].id,
                        task_id: model.input[i].task_config_id,
                        input_pdt_id: model.input[i].input_pdt_id,
                        input_pdt_name: model.input[i].input_pdt_name,
                        input_qty: model.input[i].input_qty,
                        required_input_qty: model.input[i].input_qty,
                        taskIndex: index,
                        input_pdt_uom: model.input[i].input_pdt_uom
                    };
                    $scope.productionModel.empList[empIndex].details[index].input.push(newRow);
                }

                $scope.updateEmployeeSalary(empIndex);

                return;
            }
        }

        $scope.deleteAllowance = function (empIndex, index)
        {
            $scope.productionModel.empList[empIndex].allowance.splice(index, 1);
            $scope.updateEmployeeSalary(empIndex);
        };

        $scope.addNewAllowance = function (i)
        {
            var newRow = {
                allowanceInfo: '',
                allowanceId: '',
                amount: '',
                name: ''
            };
            $scope.productionModel.empList[i].allowance.push(newRow);
            $scope.calculatetotal(i);
        };

        $scope.getAllowanceList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.ALLOWANCE_MASTER_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatAllowanceModel = function (model, index, empIndex)
        {
            if (model != null)
            {
                $scope.updateAllowanceInfo(model, index, empIndex);
                return model.name;
            }
            return  '';
        }

        $scope.updateAllowanceInfo = function (model, index, empIndex)
        {
            if ($scope.productionModel.empList.length != 0 && $scope.productionModel.empList[empIndex].allowance.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.productionModel.empList[empIndex].allowance[index].allowanceId = model.id;
                $scope.productionModel.empList[empIndex].allowance[index].name = model.name;
                $scope.productionModel.empList[empIndex].allowance[index].amount = model.amount;

                $scope.updateEmployeeSalary(empIndex);
                return;
            }
        }

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateEmployeeSalary = function (empIndex)
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal(empIndex), 300);
        }

        $scope.calculatetotal = function (index)
        {
            var perdayamount = 0;
            var allowance_amount = 0;
            var empDuration = 0;
            $scope.productionModel.empList[index].amount = 0;
            if ($scope.productionModel.empList[index].is_present == true)
            {
                for (var i = 0; i < $scope.productionModel.empList[index].details.length; i++)
                {
                    if ($scope.productionModel.empList[index].details[i].type == 'attendance')
                    {
                        $scope.productionModel.empList[index].details[i].task_amount = parseFloat(parseFloat($scope.productionModel.empList[index].salary_amount) * parseFloat($scope.productionModel.empList[index].details[i].duration));
                        empDuration = empDuration + parseFloat($scope.productionModel.empList[index].details[i].duration);
                    } else if ($scope.productionModel.empList[index].details[i].type == 'piece')
                    {
                        if ($scope.productionModel.empList[index].details[i].production_unit_price == '' || $scope.productionModel.empList[index].details[i].production_unit_price == '')
                            $scope.productionModel.empList[index].details[i].production_unit_price = 0;
                        if ($scope.productionModel.empList[index].details[i].production_qty == '' || $scope.productionModel.empList[index].details[i].production_qty == '')
                            $scope.productionModel.empList[index].details[i].production_qty = 0;
                        $scope.productionModel.empList[index].details[i].task_amount = parseFloat(parseFloat($scope.productionModel.empList[index].details[i].production_unit_price) * parseFloat($scope.productionModel.empList[index].details[i].production_qty));
                    }
                    perdayamount = parseFloat(perdayamount) + parseFloat($scope.productionModel.empList[index].details[i].task_amount);
                    $scope.productionModel.empList[index].details[i].task_amount = $scope.productionModel.empList[index].details[i].task_amount.toFixed(2);
                }
                for (var i = 0; i < $scope.productionModel.empList[index].allowance.length; i++)
                {
                    if ($scope.productionModel.empList[index].allowance[i].allowanceId != '' && $scope.productionModel.empList[index].allowance[i].allowanceId != undefined)
                    {
                        allowance_amount = parseFloat(allowance_amount) + parseFloat($scope.productionModel.empList[index].allowance[i].amount);
                    }
                }
                if ($scope.productionModel.empList[index].salary_type == 'attendance')
                {
                    if(empDuration == 0 || empDuration == '' || empDuration == undefined || empDuration > 1)
                        empDuration = 1;
                    perdayamount = parseFloat(empDuration) * parseFloat($scope.productionModel.empList[index].salary_amount);
                }
                $scope.productionModel.empList[index].allowance_amount = allowance_amount.toFixed(2);
                $scope.productionModel.empList[index].amount = parseFloat(perdayamount) + parseFloat(allowance_amount);
                $scope.productionModel.empList[index].amount = $scope.productionModel.empList[index].amount.toFixed(2);
            }
        };

        $scope.inputQtycalculationTimeoutPromise != null
        $scope.updateInputQty = function (empIndex, index)
        {
            if ($scope.inputQtycalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.inputQtycalculationTimeoutPromise);
            }
            $scope.inputQtycalculationTimeoutPromise = $timeout($scope.calculateInputQty(empIndex, index), 300);
        }

        $scope.calculateInputQty = function (empIndex, index)
        {
            var production_qty = $scope.productionModel.empList[empIndex].details[index].production_qty;
            if ($scope.productionModel.empList[empIndex].details[index].input.length > 0)
            {
                for (var i = 0; i < $scope.productionModel.empList[empIndex].details[index].input.length; i++)
                {
                    $scope.productionModel.empList[empIndex].details[index].input[i].input_qty = parseFloat($scope.productionModel.empList[empIndex].details[index].input[i].required_input_qty * production_qty).toFixed(3);
                }
            }
        };

        $scope.createProduction = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createProductionParam = {};
                var headers = {};
                headers['screen-code'] = 'production';
                createProductionParam.date = '';
                if (typeof $scope.productionModel.date == 'object')
                {
                    $scope.productionModel.date = utilityService.parseDateToStr($scope.productionModel.date, 'yyyy-MM-dd');
                }
                createProductionParam.date = utilityService.changeDateToSqlFormat($scope.productionModel.date, 'yyyy-MM-dd');
                createProductionParam.emp_team_id = $scope.productionModel.teamId;
                createProductionParam.employees = [];
                for (var i = 0; i < $scope.productionModel.empList.length; i++)
                {
                    var employeeParam = {};
                    employeeParam.id = 0;
                    employeeParam.emp_id = $scope.productionModel.empList[i].id;
                    employeeParam.amount = $scope.productionModel.empList[i].amount;
                    employeeParam.team_id = $scope.productionModel.empList[i].team_id;
                    employeeParam.is_present = $scope.productionModel.empList[i].is_present == true ? "1" : "0";
                    employeeParam.details = [];
                    employeeParam.ot = [];
                    for (var j = 0; j < $scope.productionModel.empList[i].details.length; j++)
                    {
                        var detailsParam = {};
                        detailsParam.task_id = $scope.productionModel.empList[i].details[j].id;
                        detailsParam.output_pdt_id = $scope.productionModel.empList[i].details[j].output_pdt_id;
                        detailsParam.type = $scope.productionModel.empList[i].details[j].type;
                        detailsParam.duration = $scope.productionModel.empList[i].details[j].duration;
                        detailsParam.production_qty = $scope.productionModel.empList[i].details[j].production_qty;
                        detailsParam.production_unit_price = $scope.productionModel.empList[i].details[j].production_unit_price;
                        detailsParam.amount = $scope.productionModel.empList[i].details[j].task_amount;
                        detailsParam.comments = '';
                        detailsParam.inputs = [];
                        for (var loop = 0; loop < $scope.productionModel.empList[i].details[j].input.length; loop++)
                        {
                            var inputParam = {};
                            inputParam.config_input_id = $scope.productionModel.empList[i].details[j].input[loop].config_input_id;
                            inputParam.input_pdt_id = $scope.productionModel.empList[i].details[j].input[loop].input_pdt_id;
                            inputParam.task_id = $scope.productionModel.empList[i].details[j].id;
                            inputParam.input_qty = $scope.productionModel.empList[i].details[j].input[loop].input_qty;
                            detailsParam.inputs.push(inputParam);
                        }
                        employeeParam.details.push(detailsParam);
                    }
                    for (var loop = 0; loop < $scope.productionModel.empList[i].allowance.length; loop++)
                    {
                        if ($scope.productionModel.empList[i].allowance[loop].allowanceId != '' && $scope.productionModel.empList[i].allowance[loop].allowanceId != undefined)
                        {
                            var allowanceParam = {};
                            allowanceParam.allowance_master_id = $scope.productionModel.empList[i].allowance[loop].allowanceId;
                            allowanceParam.amount = $scope.productionModel.empList[i].allowance[loop].amount;
                            employeeParam.ot.push(allowanceParam);
                        }
                    }
                    createProductionParam.employees.push(employeeParam);
                }

                adminService.addProduction(createProductionParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.production');
                    }
                    $scope.isDataSavingProcess = false;
                });
            } else
            {
                return;
            }

        };

        // $scope.setDate();  
    }]);




