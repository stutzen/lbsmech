
app.controller('attributeEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, $stateParams) {

        $scope.attributeEditModel = {
            "id": 0,
            "is_required": '',
            "isactive": 1,
            "has_editable": '',
            "attributeTypeList": {},
            "list": {},
            isLoadingProgress: false
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.update_attribute_form != 'undefined' && typeof $scope.update_attribute_form.$pristine != 'undefined' && !$scope.update_attribute_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {
            if (typeof $scope.update_attribute_form != 'undefined')
            {
                $scope.update_attribute_form.$setPristine();
                $scope.attributeEditModel.list = [];
                $scope.getList();
            }
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isloadedAttType)
            {
                $scope.attributeEditModel.isLoadingProgress = false;
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $scope.updateAttributeDetails = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var attributeParam = {};
                var headers = {};
                headers['screen-code'] = 'attribute';
                var loop;
                //attributeParam.id = $stateParams.id;
                attributeParam.id = $scope.attributeEditModel.list.id;
                attributeParam.attributetype_id = $scope.attributeEditModel.list.attributetype_id;
                attributeParam.attribute_code = $scope.attributeEditModel.list.attribute_code;
                attributeParam.attribute_label = $scope.attributeEditModel.list.attribute_label;
                attributeParam.sno = $scope.attributeEditModel.list.sno;
                attributeParam.is_permission = 1;
                attributeParam.input_type = $scope.attributeEditModel.list.input_type;
                attributeParam.default_value = $scope.attributeEditModel.list.default_value;
                attributeParam.option_value = $scope.attributeEditModel.list.option_value;
                attributeParam.validation_type = $scope.attributeEditModel.list.validation_type;
                attributeParam.is_required = ($scope.attributeEditModel.list.is_required == true ? 1 : 0);
                attributeParam.has_editable = ($scope.attributeEditModel.list.has_editable == true ? 1 : 0);
                attributeParam.is_show_in_list = ($scope.attributeEditModel.list.is_show_in_list == true ? 1 : 0);
                attributeParam.is_show_in_invoice = ($scope.attributeEditModel.list.is_show_in_invoice == true ? 1 : 0);
                if (attributeParam.validation_type == "number")
                {
                    attributeParam.reg_pattern = "^[0-9//\s]*$";
                }
                else if (attributeParam.validation_type == "email")
                {
                    attributeParam.reg_pattern = "^([a-z0-9,!\\#\\$%&'\\*\\+\\/=\\?\\^_`\\{\\|\\}~-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z0-9,!\\#\\$%&'\\*\\+\\/=\\?\\^_`\\{\\|\\}~-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*@([a-z0-9-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z0-9-]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*\\.(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]){2,})$";
                }
                else if (attributeParam.validation_type == "phoneno")
                {
                    attributeParam.reg_pattern = "^(\\+|\\d)[\\d]{5,}\\d*$";
                }
                else if (attributeParam.validation_type == "alphanumeric")
                {
                    attributeParam.reg_pattern = "^[a-zA-Z0-9//\s]*$";
                }
                adminService.editAttribute(attributeParam, $scope.attributeEditModel.list.id, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.attribute');
                    }
                    $scope.isDataSavingProcess = false;

                });
            }
        };

        $scope.validateCodeInput = function( )
        {
            var attributeCode = $scope.attributeEditModel.list.attribute_code;
            var returnVal = true;


            //var re = /^[a-zA-Z,a-zA-Z]+[a-zA-Z,a-zA-Z0-9\-\//\s,!\#\$%&@()'\*\+\/=\?\^`\{\|\}~]*$/;        
//        if (re.test(attributeCode)) 
//        {
//            return "attribute code should not contain space, special characters & symbols";
//        }             
            return returnVal;
        }

        $scope.isloadedAttType = false;
        $scope.getAttributeTypeList = function()
        {
            $scope.isloadedAttType == false;
            var attrTypeListParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeTypeList(attrTypeListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.attributeEditModel.attributeTypeList = data;
                $scope.isloadedAttType = true;
            });
        };

        $scope.getList = function(val)
        {
            $scope.attributeEditModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            //getListParam.id =attributeEditModel.id;
            getListParam.start = ($scope.attributeEditModel.currentPage - 1) * $scope.attributeEditModel.limit;
            getListParam.limit = $scope.attributeEditModel.limit;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.attributeEditModel.list = data[0];
                $scope.attributeEditModel.list.id = $scope.attributeEditModel.list.id;
                if ($scope.attributeEditModel.list.is_required == 1)
                {
                    $scope.attributeEditModel.list.is_required = true;
                }
                else
                {
                    $scope.attributeEditModel.list.is_required = false;
                }
                if ($scope.attributeEditModel.list.has_editable == 1)
                {
                    $scope.attributeEditModel.list.has_editable = true;
                }
                else
                {
                    $scope.attributeEditModel.list.has_editable = false;
                }
                if ($scope.attributeEditModel.list.is_show_in_list == 1)
                {
                    $scope.attributeEditModel.list.is_show_in_list = true;
                }
                else
                {
                    $scope.attributeEditModel.list.is_show_in_list = false;
                }
                if ($scope.attributeEditModel.list.is_show_in_invoice == 1)
                {
                    $scope.attributeEditModel.list.is_show_in_invoice = true;
                }
                else
                {
                    $scope.attributeEditModel.list.is_show_in_invoice = false;
                }
                $scope.attributeEditModel.total = response.data.total;
                $scope.initUpdateDetail();
                $scope.getAttributeTypeList();
            });
        };

        $scope.getList();
    }]);

