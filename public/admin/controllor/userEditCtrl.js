

app.controller('userEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

            $scope.userAddModel = {
                  "userid": 0,
                  "username": "",
                  "password": "",
                  "f_name": "",
                  "l_name": "",
                  "gender": "",
                  "dob": '',
                  "email": "",
                  "mobile": "",
                  "city": "",
                  "state": "",
                  "country": "",
                  "postcode": "",
                  "userType": "",
                  "companyId": "",
                  "address": "",
                  "rolename": "",
                  "isactive": 1,
                  "roleList": {},
                  currentPage: 1,
                  total: 0,
                  showPassword: false,
                  role: '',
                  isLoadingProgress: false

            };
            $scope.validationFactory = ValidationFactory;
            $scope.userRoleList = ['ADMIN', 'OWNER', 'WORKFORCE'];
            $scope.getNavigationBlockMsg = function (pageReload)
            {
                  if (typeof $scope.user_edit_form != 'undefined' && typeof $scope.user_edit_form.$pristine != 'undefined' && !$scope.user_edit_form.$pristine)
                  {
                        return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
                  }

                  return "";
            }

            $scope.currentDate = new Date();
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.startDateOpen = false;
            $scope.rolehide = false;

            $scope.openDate = function (index) {

                  if (index == 0)
                  {
                        $scope.startDateOpen = true;
                  }

            }
            $scope.isDataSavingProcess = false;
            $scope.formReset = function () {
                  if (typeof $scope.user_edit_form != 'undefined')
                  {
                        $scope.user_edit_form.$setPristine();
                        $scope.updateUserInfo();
                  }

            }

            $scope.initUpdateDetailTimeoutPromise = null;

            $scope.initUpdateDetail = function ()
            {
                  if ($scope.initUpdateDetailTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                  }
                  if ($scope.isLoadedRoleList)
                  {
                        $scope.updateUserInfo();
                  } else
                  {
                        $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                  }
            }

            $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

            $scope.updateUserInfo = function ()
            {
                  $scope.userAddModel.userid = $scope.userAddModel.list.id;
                  $scope.userAddModel.username = $scope.userAddModel.list.username;
                  $scope.userAddModel.f_name = $scope.userAddModel.list.f_name;
                  $scope.userAddModel.l_name = $scope.userAddModel.list.l_name;
                  console.log("Entered PWD ", $scope.userAddModel.list.password);
                  $scope.userAddModel.password = "";
                  $scope.userAddModel.mobile = $scope.userAddModel.list.ph_no;
                  $scope.userAddModel.email = $scope.userAddModel.list.email;
                  $scope.userAddModel.country = $scope.userAddModel.list.country;
                  $scope.userAddModel.role = $scope.userAddModel.list.role_id + '';
                  $scope.userAddModel.city = $scope.userAddModel.list.city;
                  $scope.userAddModel.state = $scope.userAddModel.list.state;
                  $scope.userAddModel.address = $scope.userAddModel.list.address;
                  $scope.userAddModel.postcode = $scope.userAddModel.list.post_code;
                  $scope.userAddModel.rolename = $scope.userAddModel.list.rolename;
                  if ($scope.userAddModel.list.dob != null)
                  {
                        $scope.userAddModel.dob = utilityService.parseStrToDate($scope.userAddModel.list.dob);
                  } else
                  {
                        $scope.userAddModel.dob = '';
                  }

                  $scope.userAddModel.gender = $scope.userAddModel.list.gender;
                  $scope.userAddModel.companyId = $scope.userAddModel.list.comp_id;
                  $scope.userAddModel.imagePath = $scope.userAddModel.list.imagePath;
                  $scope.userAddModel.isActive = 1;
                  $scope.userAddModel.isLoadingProgress = false;
            }

            $scope.isLoadedRoleList = false;
            $scope.getRoleList = function ()
            {
                  var getListParam = {};
                  getListParam.id = '';
                  $scope.isLoadedRoleList = false;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getUserRoleList(getListParam, configOption).then(function (response)
                  {
                        var data = response.data;
                        $scope.userAddModel.roleList = data.list;
                        $scope.isLoadedRoleList = true;
                  });
            };

            $scope.modifyUser = function () {
                  if ($scope.isDataSavingProcess == false)
                  {
                        $scope.isDataSavingProcess = true;
                        var modifyUserParam = {};
                        var headers = {};
                        headers['screen-code'] = 'users';
                        modifyUserParam.id = $scope.userAddModel.userid;
                        modifyUserParam.f_name = $scope.userAddModel.f_name;
                        modifyUserParam.l_name = $scope.userAddModel.l_name;
                        modifyUserParam.username = $scope.userAddModel.username;
                        if ($scope.userAddModel.password != "")
                              modifyUserParam.password = $scope.userAddModel.password;
                        modifyUserParam.user_type = $scope.userAddModel.role == "ROLE_ADMIN" ? 'admin' : $scope.userAddModel.role == "ROLE_OWNER" ? 'owner' : 'workforce';
                        modifyUserParam.comp_id = $scope.userAddModel.companyId;
                        modifyUserParam.ph_no = $scope.userAddModel.mobile;
                        modifyUserParam.email = $scope.userAddModel.email;
                        modifyUserParam.city = $scope.userAddModel.city;
                        modifyUserParam.country = $scope.userAddModel.country;
                        modifyUserParam.role_id = $scope.userAddModel.role;
                        for (var i = 0; i < $scope.userAddModel.roleList.length; i++)
                        {
                              if ($scope.userAddModel.role == $scope.userAddModel.roleList[i].id)
                              {
                                    modifyUserParam.rolename = $scope.userAddModel.roleList[i].name;
                              }
                        }
                        modifyUserParam.state = $scope.userAddModel.state;
                        modifyUserParam.dob = utilityService.parseDateToStr($scope.userAddModel.dob, 'yyyy-MM-dd');
                        modifyUserParam.gender = $scope.userAddModel.gender;
                        modifyUserParam.post_code = $scope.userAddModel.postcode;
                        modifyUserParam.address = $scope.userAddModel.address;
                        modifyUserParam.device_id = $scope.userAddModel.device_id;
                        modifyUserParam.isactive = 1;
                        modifyUserParam.imagepath = "";

                        adminService.modifyUser(modifyUserParam, $scope.userAddModel.userid, headers).then(function (response) {
                              if (response.data.success == true)
                              {
                                    $scope.formReset();
                                    $state.go('app.userlist');
                              }
                              $scope.isDataSavingProcess = false;
                        });
                  }
            };
            $scope.getUser = function () {
                  $scope.userAddModel.isLoadingProgress = true;
                  var getListParam = {};
                  $scope.rolehide = false;
                  getListParam.userId = $stateParams.userid;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getUserList(getListParam, configOption).then(function (response) {
                        var data = response.data;
                        if (data.total != 0)
                        {
                              $scope.userAddModel.list = data.list[0];
                              if ($scope.userAddModel.list.rolename.toLowerCase() != 'superadmin' && $scope.userAddModel.list.rolename.toLowerCase() != 'admin')
                              {
                                    $scope.rolehide = false;
                              } else {
                                    $scope.rolehide = true;
                              }
                        }
                        $scope.userAddModel.total = data.total;
                        $scope.initUpdateDetail();

                  });
            };
            $scope.getUser();
            $scope.getRoleList();
      }]);




