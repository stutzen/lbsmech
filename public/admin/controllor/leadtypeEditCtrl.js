
    app.controller('leadtypeEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, utilityService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

       $scope.leadModel = {
            id: '',
            name: '',
            comments:'',
            list : [],
            type:''
        }
        $scope.validationFactory = ValidationFactory;
        $scope.leadModel.isLoadingProgress = false;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.edit_lead_form != 'undefined' && typeof $scope.edit_lead_form.$pristine != 'undefined' && !$scope.edit_lead_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.edit_lead_form.$setPristine();
            if (typeof $scope.edit_lead_form != 'undefined')
            {
                $scope.leadModel.name = "";
                $scope.leadModel.comments = "";
                $scope.leadModel.is_active = 1;
                $scope.leadModel.type = "";
                
            }
             $scope.updateLeadInfo();
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.isSourceLoaded = false;
        $scope.initUpdateTimeoutPromise = null;
        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateTimeoutPromise);
            }
            if ($scope.isSourceLoaded == true)
            {
                $scope.updateLeadInfo();
            }
            else
            {
                $scope.initTableFilterTimeoutPromise = $timeout($scope.initUpdateDetail(), 300);
            }
        };
        $scope.updateLeadInfo = function()
        {
            $scope.leadModel.is_active = $scope.leadModel.list.is_active;
            $scope.leadModel.name = $scope.leadModel.list.name;
            $scope.leadModel.comments = $scope.leadModel.list.comments;
            $scope.leadModel.type = $scope.leadModel.list.type;
            $scope.leadModel.isLoadingProgress = false;
            $scope.leadModel.id = $scope.leadModel.list.id;
        }
        $scope.modifylead = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addCompanyParam = {};
            var headers = {};
            headers['screen-code'] = 'leadtype';
            addCompanyParam.id = $stateParams.id;
            addCompanyParam.name = $scope.leadModel.name;
            addCompanyParam.comments = $scope.leadModel.comments;
            addCompanyParam.type = $scope.leadModel.type;
            addCompanyParam.is_active = 1;
            adminService.modifyLeadType(addCompanyParam, $scope.leadModel.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.leadtype');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getLeadInfo = function()
        {

            if (typeof $stateParams.id != 'undefined')
            {
                $scope.leadModel.isLoadingProgress = true;
                var companyListParam = {};
                companyListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getLeadTypeList(companyListParam, configOption).then(function(response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.leadModel.list = data.list[0];

                    }
                    $scope.isSourceLoaded = true;
                    $scope.initUpdateDetail();

                });
            }
        };
          $scope.getLeadInfo();
    }]);





