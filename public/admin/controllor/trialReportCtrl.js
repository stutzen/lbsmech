


app.controller('trialReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, $state, $timeout, ValidationFactory, APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.trialModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            serverList: null,
            isLoadingProgress: false,
            totaldebit: 0,
            totalcredit: 0,
            totalamt: 0,
            totalbalance: 0,
            openingBalance: 0,
            categoryList: [],
            reportDate: '',
            credit :0,
            debit:0

        };
        $scope.pagePerCount = [50, 100];
        $scope.trialModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = 'dd-MM-yyyy';
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }
        $scope.searchFilter = {
            fromdate: new Date(),
            todate: '',
            subcategory: '',
            category: '1',
            clientInfo: {}
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }


        $scope.calculateTotal = function()
        {

            var totDebit = 0;
            var totCredit = 0;
            var totamt = 0;
            for (var i = 0; i < $scope.trialModel.list.length; i++)
            {

                totDebit += $scope.trialModel.list[i].debit;
                $scope.trialModel.totaldebit = totDebit;
                totCredit += $scope.trialModel.list[i].credit;
                $scope.trialModel.totalcredit = totCredit;
                totamt += $scope.trialModel.list[i].amount;
                $scope.trialModel.totalamt = totamt;

            }
        }

        $scope.formatClientModel = function(model) {

            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.getClientList = function(val) {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.Id = "";
            autosearchParam.phno = "";
            autosearchParam.email = "";
            autosearchParam.country = '';
            autosearchParam.city = "";
            autosearchParam.state = "";
            autosearchParam.mode = 1;
            autosearchParam.sort = 1;
            autosearchParam.start = 0;
            autosearchParam.limit = 10;
            autosearchParam.companyId = $rootScope.userModel.companyId;
            return $httpService.get(APP_CONST.API.GET_CLIENT_LIST, autosearchParam, false).then(function(responseData) {
                var data = responseData.data;
                var hits = data.list;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.getCategoryList = function() {
            var categoryListParam = {};
            categoryListParam.id = "";
            categoryListParam.name = "";
            adminService.getCategoryList(categoryListParam).then(function(response) {
                var data = response.data;
                $scope.trialModel.categoryList = data.list;

            });

        };

        $scope.updateBalanceAmount = function()
        {
            if ($scope.trialModel.list.length == 1)
            {
                var openingBalance = parseFloat($scope.trialModel.list[0].debit) - parseFloat($scope.trialModel.list[0].credit);
                $scope.trialModel.list[0].balance = openingBalance;
                if ($scope.trialModel.list[0].cId != null && $scope.trialModel.list[0].cId == 0)
                {
                    $scope.trialModel.list[0].cId = '';

                }
            }
            
            if ($scope.trialModel.list.length > 1)
            {

                var openingBalance =  parseFloat($scope.trialModel.list[0].debit) - parseFloat($scope.trialModel.list[0].credit);
                $scope.trialModel.list[0].balance = openingBalance;
                if ($scope.trialModel.list[0].cId != null && $scope.trialModel.list[0].cId == 0)
                {
                    $scope.trialModel.list[0].cId = '';
                }

                for (var i = 1; i < $scope.trialModel.list.length; i++)
                {
                    var credit = $scope.trialModel.list[i].credit == null ? 0 : $scope.trialModel.list[i].credit;
                    var debit = $scope.trialModel.list[i].debit == null ? 0 : $scope.trialModel.list[i].debit;
                    if (debit != 0)
                    {
                        openingBalance = openingBalance + debit;
                    }
                    else if (credit != 0)
                    {
                        openingBalance = openingBalance - credit;
                    }
                    $scope.trialModel.list[i].balance = openingBalance;
                }

                $scope.trialModel.totalbalance = openingBalance;

            }
            else
            {
                $scope.trialModel.totalbalance = 0;
            }


        }

        $scope.getList = function() {

            $scope.trialModel.isLoadingProgress = true;

            var getListParam = {};
            getListParam.start = 0;
            getListParam.limit = 0;
            getListParam.fromDate = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
            getListParam.toDate = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');

            if (getListParam.fromDate === '' || getListParam.toDate === '')
            {
                if (getListParam.fromDate != '')
                {
                    getListParam.toDate = getListParam.fromDate;
                }
                if (getListParam.toDate != '')
                {
                    getListParam.fromDate = getListParam.toDate;
                }
            }
            if (getListParam.fromDate === getListParam.toDate)
            {
                $scope.trialModel.reportDate = getListParam.fromDate;
            }
            else
            {
                $scope.trialModel.reportDate = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd') + ' - ' + $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');
            }

            if (typeof getListParam.fromDate == 'undefined' || getListParam.fromDate == '')
            {
                $scope.trialModel.list = [];
                $scope.trialModel.openingBalance = 0;
                $scope.trialModel.isLoadingProgress = false;
                $scope.trialModel.totaldebit = 0;
                $scope.trialModel.totalcredit = 0;
                $scope.trialModel.totalamt = 0;
                $scope.trialModel.totalbalance = 0;
                return;
            }

            getListParam.sourceType = '';
            getListParam.category = $scope.searchFilter.category;
            getListParam.subCategory = $scope.searchFilter.subcategory;
            if ($scope.searchFilter.clientInfo != null && typeof $scope.searchFilter.clientInfo != 'undefined' && typeof $scope.searchFilter.clientInfo.id != 'undefined')
            {
                getListParam.clientId = $scope.searchFilter.clientInfo.id;
            }
            else
            {
                getListParam.clientId = '';
            }

            adminService.getAccountlist(getListParam).then(function(response) {
                var data = response.data;
                var openingBalance = {};
                openingBalance.debit = data.debit;
                openingBalance.credit = data.credit;
                openingBalance.cId = 0;
                openingBalance.particulars = 'Opening balance';
                
                                     
                $scope.trialModel.list = data.list;
                $scope.trialModel.list.splice(0, 0, openingBalance);
 
                $scope.updateBalanceAmount();
                $scope.calculateTotal();
                $scope.trialModel.total = data.total;
                $scope.trialModel.isLoadingProgress = false;
            });

        };

        $scope.$watch('searchFilter.clientInfo', function(newVal, oldVal) {

            if (typeof newVal == 'undefined' || newVal == '') {

                $scope.initTableFilter();
            }
        });

        $scope.getList();
        $scope.getCategoryList();
    }]);








