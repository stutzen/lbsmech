




app.controller('deliveryInstructionAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.deliveryModel =
                {
                    "id": 0,
                    "transportId": '',
                    "driverName": '',
                    "driverMobileNo": '',
                    "vehicleType": "",
                    "vehicleNo": '',
                    "status": "new",
                    "type": '',
                    "finalDestination": '',
                    "shipmentDate": '',
                    "deliveryDate": '',
                    "billingUnit": '',
                    "store": '',
                    "transportList": [],
                    "transportInfo": {}
                };


        $scope.adminService = adminService;
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.save_deliveryinstruction_form != 'undefined' && typeof $scope.save_deliveryinstruction_form.$pristine != 'undefined' && !$scope.save_deliveryinstruction_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.isDataSavingProcess = false;
        $scope.isInvoiceLoading = false;

        $scope.shipmentDateOpen = false;
        $scope.deliveryDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 1)
            {
                $scope.shipmentDateOpen = true;
                $scope.deliveryDateOpen = false;
            }
            if (index == 2)
            {
                $scope.deliveryDateOpen = true;

                $scope.shipmentDateOpen = false;
            }
        };

        $scope.formReset = function ()
        {
            $scope.deliveryModel.transportId = '';
            $scope.deliveryModel.driverName = '';
            $scope.deliveryModel.driverMobileNo = "";
            $scope.deliveryModel.vehicleType = "";
            $scope.deliveryModel.vehicleNo = '';
            $scope.deliveryModel.status = "new";
            $scope.deliveryModel.type = "";
            $scope.deliveryModel.finalDestination = "";
            $scope.deliveryModel.shipmentDate = "";
            $scope.deliveryModel.deliveryDate = "";
            $scope.deliveryModel.billingUnit = "";
            $scope.deliveryModel.store = "";
            $scope.deliveryModel.transportInfo = '';
        };

//        $scope.$watch('searchFilter.customerInfo', function (newVal, oldVal)
//        {
//            if (typeof newVal === 'undefined' || newVal == null || newVal === '')
//            {
//                {
//                    $scope.initTableFilter();
//                }
//            }
//        });

        $scope.getTransportList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.TRANSPORT_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatTransportModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined && model.mobile_no != undefined && model.email != undefined)
                {
                    return model.name + '(' + model.mobile_no + ',' + model.email + ')';
                } else if (model.name != undefined && model.mobile_no != undefined)
                {
                    return model.name + '(' + model.mobile_no + ')';
                } else
                {
                    return model.name;
                }
            }
            return '';
        };

        $scope.createDeliveryInstruction = function ()
        {
            var createAdvanceParam = {};
            $scope.isDataSavingProcess = true;
            if (typeof $scope.deliveryModel.shipmentDate == 'object')
            {
                $scope.deliveryModel.shipmentDate = utilityService.parseDateToStr($scope.deliveryModel.shipmentDate, 'yyyy-MM-dd');
            }
            createAdvanceParam.shipment_date = utilityService.changeDateToSqlFormat($scope.deliveryModel.shipmentDate, 'yyyy-MM-dd');
            if (typeof $scope.deliveryModel.deliveryDate == 'object')
            {
                $scope.deliveryModel.deliveryDate = utilityService.parseDateToStr($scope.deliveryModel.deliveryDate, 'yyyy-MM-dd');
            }
            createAdvanceParam.delivery_date = utilityService.changeDateToSqlFormat($scope.deliveryModel.deliveryDate, 'yyyy-MM-dd');
            if ($scope.deliveryModel.transportInfo != null && typeof $scope.deliveryModel.transportInfo != 'undefined' && typeof $scope.deliveryModel.transportInfo.id != 'undefined')
            {
                createAdvanceParam.transport_id = $scope.deliveryModel.transportInfo.id;
            }
            else
            {
                createAdvanceParam.transport_id = '';
            }            
            createAdvanceParam.driver_name = $scope.deliveryModel.driverName;
            createAdvanceParam.driver_mobile_no = $scope.deliveryModel.driverMobileNo;
            createAdvanceParam.vehicle_type = $scope.deliveryModel.vehicleType;
            createAdvanceParam.vehicle_no = $scope.deliveryModel.vehicleNo;
            createAdvanceParam.status = $scope.deliveryModel.status;
            createAdvanceParam.type = $scope.deliveryModel.type;
            createAdvanceParam.final_destination = $scope.deliveryModel.finalDestination;
            createAdvanceParam.billing_unit = $scope.deliveryModel.billingUnit;
            createAdvanceParam.store = $scope.deliveryModel.store;           

            createAdvanceParam.is_active = 1;
            var headers = {};
            headers['screen-code'] = 'deliveryinstruction';
            adminService.createDeliveryInstruction(createAdvanceParam, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.isDataSavingProcess = false;
                    //$scope.formReset();
                    $state.go('app.deliveryInstructionEdit', {'id': response.data.id });
                }                
            });
        };

    }]);




