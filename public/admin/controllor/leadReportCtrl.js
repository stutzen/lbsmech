
app.controller('leadReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.leadModel = {
        currentPage: 1,
        total: 0,
        total_Amount: 0,
        limit: 10,
        list: [],
        length: [],
        serverList: null,
        isLoadingProgress: false,
        isSearchLoadingProgress: false
    };
    $scope.showDetail = false;
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.leadModel.limit = $scope.pagePerCount[0];
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.validationFactory = ValidationFactory;
    $scope.openDate = function(index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index === 1)
        {
            $scope.toDateOpen = true;
        }

    };

    $scope.searchFilter = {
        fromdate: '',
        todate : '',
        customerInfo: {},
        employeeInfo : {}
    };

    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            fromdate: '',
            todate : '',
            customerInfo: '',
            employeeInfo : ''
        };
        $scope.leadModel.list = [];
    }
    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise !== null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    $scope.validateDateFilterData = function()
    {
        var retVal = false;
        if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
        {
            retVal = true;
        }
        return retVal;
    };
    $scope.getEmployeelist = function (val)
    {
        var autosearchParam = {};
        autosearchParam.fname = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };

    $scope.formatEmployeeModel = function (model) {

        if (model !== null && model != undefined)
        {
            return model.f_name;
        }
        return  '';
    };

    $scope.getList = function() {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'leadwithoutdeal';
        if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
        {
            getListParam.contact_id = $scope.searchFilter.customerInfo.id;
        }
        else
        {
            getListParam.contact_id = '';
        }
        if ($scope.searchFilter.employeeInfo != 'undefined' && $scope.searchFilter.employeeInfo != null &&
            $scope.searchFilter.employeeInfo.id != 'undefined' && $scope.searchFilter.employeeInfo.id != '' && $scope.searchFilter.employeeInfo.id != null)
            {
            getListParam.contact_owner_id = $scope.searchFilter.employeeInfo.id;
        }
        else
        {
            getListParam.contact_owner_id = '';
        }    
        //        getListParam.from_date = $scope.searchFilter.fromdate;
        //        getListParam.to_date = $scope.searchFilter.todate;
        //
        //        if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
        //        {
        //            getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
        //        }
        //        if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
        //        {
        //            getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
        //        }
        //
        //        if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
        //        {
        //            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
        //
        //        }
        //        if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
        //        {
        //            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
        //        }
        getListParam.is_active = 1;
        getListParam.start = 0;
        getListParam.limit = 0;
        $scope.leadModel.isLoadingProgress = true;
        $scope.leadModel.isSearchLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getLeadWithoutDealReport(getListParam, configOption, headers).then(function(response)
        {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.leadModel.list = data;
                for (var i = 0; i < $scope.leadModel.list.length; i++)
                {
                    $scope.leadModel.list[i].newdate = utilityService.parseStrToDate($scope.leadModel.list[i].date);
                    $scope.leadModel.list[i].date = utilityService.parseDateToStr($scope.leadModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                }
            }
            $scope.leadModel.isLoadingProgress = false;
            $scope.leadModel.isSearchLoadingProgress = false;
        });

    };
    $scope.getCustomerList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.search = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.CONTACT_SEARCH_LIST, autosearchParam, false).then(function(responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.formatcustomerModel = function(model)
    {
        if (model != null)
        {
            if (model.fname != undefined && model.phone != undefined && model.email != undefined)
            {
                return model.fname + '(' + model.phone + ',' + model.email + ')';
            }
            else if (model.fname != undefined && model.phone != undefined)
            {
                return model.fname + '(' + model.phone + ')';
            }
        }
        return  '';
    };
    $scope.print = function()
    {
        $window.print();
    };
    $scope.getList();
}]);




