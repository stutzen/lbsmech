





app.controller('employeeteamListCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function ($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.teamModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            isLoadingProgress: false
        };
        $scope.searchFilter = {
            id: '',
            name: '',
            user_id: '',
            user_name: '',
            userInfo: ''
        };
        $scope.validationFactory = ValidationFactory;
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                id: '',
                name: '',
                user_id: '',
                user_name: '',
                userInfo: ''
            };
            $scope.initTableFilter();
        }
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.teamModel.limit = $scope.pagePerCount[0];

        $scope.validationFactory = ValidationFactory;

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };

        $scope.getEmployeelist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeeModel = function (model) {

            if (model !== null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };

        $scope.$watch('searchFilter.employeeInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
            else if(typeof newVal != 'undefined' || newVal != '' || newVal != null)
            {
                $scope.initTableFilter();
            }
        });
        
        $scope.selectEmployeeTeamId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteEmployeeLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectEmployeeTeamId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteEmployeeLoadingProgress = false;
        };

        $scope.deleteEmployeeTeamInfo = function( )
        {
            if ($scope.isdeleteEmployeeLoadingProgress == false)
            {
                $scope.isdeleteEmployeeLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectEmployeeTeamId;
                var headers = {};
                headers['screen-code'] = 'team';
                adminService.deleteEmployeeTeam(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteEmployeeLoadingProgress = false;
                });
            }
        };
        
        $scope.getList = function () {

            $scope.teamModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'team';
            getListParam.user_id = '';
            getListParam.is_active = 1;
            if ($scope.searchFilter.employeeInfo != 'undefined' && $scope.searchFilter.employeeInfo != null &&
                    $scope.searchFilter.employeeInfo.id != 'undefined' && $scope.searchFilter.employeeInfo.id != '' && $scope.searchFilter.employeeInfo.id != null)
            {
                getListParam.emp_id = $scope.searchFilter.employeeInfo.id;
            }
            getListParam.name = $scope.searchFilter.name;
            getListParam.start = ($scope.teamModel.currentPage - 1) * $scope.teamModel.limit;
            getListParam.limit = $scope.teamModel.limit;
            $scope.teamModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeTeamList(getListParam, configOption, headers).then(function (response) {

                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.teamModel.list = data;

                    $scope.teamModel.total = data.total;
                }
                $scope.teamModel.isLoadingProgress = false;
            });

        };

        $scope.getList();

    }]);




