
app.controller('purchaseQuoteViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": 0,
                    "roundoff": "",
                    "taxAmt": 0,
                    "taxInfo": "",
                    "discountAmt": 0,
                    list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "customerInfo": "",
                    "invoiceDetail": {},
                    "paymentList": {},
                    //"paymenttype": "CASH",
                    "couponcode": "",
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "discountamount": "",
                    "taxamount": "",
                    "cardDetail": "",
                    "duedate": "",
                    "tax_id": "",
                    "balance_amount": "",
                    "advance_amount": "",
                    "isLoadingProgress": false,
                    "dueAmount": '',
                    "status": "",
                    "created_at": "",
                    "accounts": '',
                    "accountsList": [],
                    "paymentlist": [],
                    "payment_mode": "",
                    "paymentdefault": ""
                }

        $scope.currentDate = new Date();
        $scope.stateParamData = $stateParams.id;
        $scope.colsPan = $rootScope.appConfig.purchase_uom;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        //$scope.orderListModel.billdate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.order_view_form != 'undefined' && typeof $scope.order_view_form.$pristine != 'undefined' && !$scope.order_view_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.isDataDeletingProcess = false;
        $scope.formReset = function()
        {
            $scope.order_view_form.$setPristine();
            $scope.orderListModel.invoiceDetail = {};
            $scope.orderListModel.list = [];
            $scope.getInvoiceInfo( );
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function() {

            $scope.orderListModel.user = '';
        }

        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.dueDateOpen = false;
        $scope.invoiceDateOpen = false;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.dueDateOpen = true;
            }
            else if (index == 2)
            {
                $scope.invoiceDateOpen = true;
            }
        }
        $scope.paymentAddModel =
                {
                    "id": 0,
                    "account": '',
                    list: [],
                    "comments": " ",
                    "invoicedate": "",
                    "amount": '',
                    "payment_mode": '',
                    "paymentdefault": '',
                    "paymentlist": '',
                    "parchase_quote_id": '',
                    "cheque_no": '',
                    "customer_id": '',
                    "categoryList": '',
                    "category": ''
                }

        $scope.reset_payment_form = function( )
        {
            $scope.paymentAddModel.parchase_quote_id = '';
            $scope.paymentAddModel.customer_id = '';
            $scope.paymentAddModel.account = "";
            $scope.paymentAddModel.invoicedate = "";
            $scope.paymentAddModel.paymentdefault = '';
            $scope.paymentAddModel.payment_mode = '';
            $scope.paymentAddModel.amount = "";
            $scope.paymentAddModel.comments = "";
            $scope.showPaymentPopup( );
        }

        $scope.showPayPopup = false;
        $scope.showPaymentPopup = function(index)
        {
            $scope.getpaymenttermslist();
            $scope.showPayPopup = true;
            $scope.invoiceDateOpen = false;
            $scope.paymentAddModel.purchase_quote_id = $scope.orderListModel.invoiceDetail.id;
            $scope.paymentAddModel.customer_id = $scope.orderListModel.invoiceDetail.customer_id;
            $scope.paymentAddModel.account = $scope.orderListModel.invoiceDetail.customer_fname;
            $scope.paymentAddModel.category = '';
            $scope.paymentAddModel.accouts = '';
            $scope.paymentAddModel.comments = '';
            //$scope.paymentAddModel.invoicedate = $filter('date')($scope.currentDate, $scope.dateFormat);            
            if (typeof index == 'undefined')
                index = 0;

            if (index == 0)
            {
                $scope.paymentAddModel.invoicedate = $filter('date')($scope.currentDate, $scope.dateFormat);
                $scope.paymentAddModel.amount = $scope.orderListModel.balance_amount;
                $scope.paymentAddModel.comments = '';
                $scope.paymentAddModel.payment_mode = $scope.paymentAddModel.paymentdefault;
                $scope.isNewPayment = true;
            }
            else
            {
                for (var loop = 0; loop < $scope.orderListModel.paymentList.length; loop++)
                {
                    if ($scope.orderListModel.paymentList[loop].id == index)
                    {
                        var invoiceDate = utilityService.parseStrToDate($scope.orderListModel.paymentList[loop].created_at);
                        $scope.paymentAddModel.invoicedate = $filter('date')(invoiceDate, $scope.dateFormat);
                        $scope.paymentAddModel.amount = $scope.orderListModel.paymentList[loop].amount;
                        $scope.paymentAddModel.payment_mode = $scope.orderListModel.paymentList[loop].paymentdefault;
                        $scope.paymentAddModel.comments = $scope.orderListModel.paymentList[loop].comments;
                        $scope.paymentAddModel.category = $scope.orderListModel.paymentList[loop].tra_category;
                        $scope.paymentId = $scope.orderListModel.paymentList[loop].id;
                        $scope.isNewPayment = false;
                        break;
                    }
                }
            }
        };

        $scope.closePaymentPopup = function( )
        {
            $scope.isDataSavingProcess = false;
            $scope.showPayPopup = false;
        }

        $scope.paymentRemovePopup = false;
        $scope.removePaymentId = 'undefined';
        $scope.showRemovePaymentPopup = function(index)
        {
            $scope.removePaymentId = index;

            for (var loop = 0; loop < $scope.orderListModel.paymentList.length; loop++)
            {
                if ($scope.orderListModel.paymentList[loop].id == index)
                {
                    $scope.removePaymentAmount = $scope.orderListModel.paymentList[loop].amount;
                    break;
                }
            }
            $scope.paymentRemovePopup = true;
        };

        $scope.closeRemovePaymentPopup = function( )
        {
            $scope.paymentRemovePopup = false;
        }

        $scope.removePayment = function( )
        {
            if ($scope.isDataDeletingProcess == false)
            {
                $scope.isDataDeletingProcess = true;
                var headers = {};
                headers['screen-code'] = 'purchaseestimate';
                var deletePaymentParam = {};
                deletePaymentParam.id = $scope.removePaymentId;

                adminService.deletePurchasePayment(deletePaymentParam, deletePaymentParam.id, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.closeRemovePaymentPopup( );
                        $scope.getInvoiceInfo( );
                    }
                    $scope.isDataDeletingProcess = false;
                });
            }
        }

        $scope.senderEmail = '';
        $scope.receiver = [];
        $scope.ccEmail = '';

        $scope.showSendInvoicePopup = false;
        $scope.showSendInvPopup = function( )
        {
            $scope.showSendInvoicePopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;
            $scope.subject = "Invoice No " + $scope.orderListModel.invoiceDetail.id + " from company name";
            $scope.sendMsg = "";
            $scope.isAttachInvoicePDF = true;
            $scope.$broadcast("initPopupEvent");
        };

        $scope.addMoreReceiverEmail = function( )
        {
            var newRow = {
                "email": ""
            };
            $scope.receiver.push(newRow);
        }

        $scope.showpopup = false;
        $scope.showPopup = function(index) {
            var removePaymentAmount;

            $scope.showpopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            $scope.receiver = [];
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.receiverEmail = $scope.orderListModel.customerInfo.email;
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;
            $scope.subject = "Payment Receipt for Invoice No " + $scope.orderListModel.invoiceDetail.id;
            for (var loop = 0; loop < $scope.orderListModel.paymentList.length; loop++)
            {
                if ($scope.orderListModel.paymentList[loop].id == index)
                {
                    removePaymentAmount = $scope.orderListModel.paymentList[loop].amount;
                    break;
                }
            }
            $scope.isAttachInvoicePDF = false;
            $scope.sendMsg = "Hi " + "\n" + "Here's your Payment receipt for invoice no " + $scope.orderListModel.invoiceDetail.id + " of payment amount " + removePaymentAmount + ".\n  If you have any questions, Please let me know. \n Thanks";
            //        $scope.subject = "Invoice " + $scope.orderListModel.invoiceDetail.id + "from company name";
            $scope.$broadcast("initPopupEvent");
        };

        $scope.closePopup = function()
        {
            $scope.showSendInvoicePopup = false;
            $scope.showpopup = false;
            $scope.getInvoiceInfo( );
        }

        $scope.validationChequeNo = function( )
        {
            var retVal = true;
            if ($scope.paymentAddModel.payment_mode.toLowerCase() == 'cheque')
            {
                if ($scope.paymentAddModel.cheque_no == '')
                {
                    return false;
                }
            }
            return retVal;
        };

        $scope.print = function()
        {
            window.print();
        }

        $scope.savePayment = function( )
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var headers = {};
                headers['screen-code'] = 'purchaseestimate';
                var createPaymentParam = {};
                createPaymentParam.customer_id = $scope.paymentAddModel.customer_id;
                createPaymentParam.comments = $scope.paymentAddModel.comments;
                createPaymentParam.amount = $scope.paymentAddModel.amount;
                createPaymentParam.date = utilityService.changeDateToSqlFormat($scope.paymentAddModel.invoicedate, 'yyyy-MM-dd');
                createPaymentParam.is_settled = 1;
                createPaymentParam.purchase_quote_id = $scope.paymentAddModel.purchase_quote_id;
                createPaymentParam.payment_mode = $scope.paymentAddModel.payment_mode;
                createPaymentParam.cheque_no = $scope.paymentAddModel.cheque_no;
                createPaymentParam.tra_category = $scope.paymentAddModel.category;
                //createPaymentParam.status = "paid"; 
                for (var i = 0; i < $scope.paymentAddModel.accountsList.length; i++)
                {
                    if ($scope.paymentAddModel.accounts == $scope.paymentAddModel.accountsList[i].id)
                    {
                        createPaymentParam.account = $scope.paymentAddModel.accountsList[i].account;
                    }
                }
                createPaymentParam.account_id = $scope.paymentAddModel.accouts;
                var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                adminService.createPurchasePayment(createPaymentParam, configOption, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.getInvoiceInfo();
                        $scope.showPayPopup = false;
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        }

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_purchase = 1;

            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                return model.fname;
            }
            return  '';
        };
        $scope.getpaymenttermslist = function() {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            var defaultlist = [];
            adminService.getpaymenttermslist(getListParam, configOption).then(function(response) {
                var data = response.data.list;
                $scope.paymentAddModel.paymentlist = data;
                for (var i = 0; i < $scope.paymentAddModel.paymentlist.length; i++)
                {
                    if ($scope.paymentAddModel.paymentlist[i].is_default == 1)
                    {
                        defaultlist.push($scope.paymentAddModel.paymentlist[i]);
//                        $scope.paymentAddModel.paymentdefault = $scope.paymentAddModel.paymentlist[i].name;
//                        break;
                    }
                }
                $scope.paymentAddModel.paymentdefault = defaultlist[0].name;
                var latest = defaultlist[0].updated_at;
                for (var i = 0; i < defaultlist.length; i++)
                {
                    if (defaultlist[i].updated_at > latest)
                    {
                        latest = defaultlist[i].updated_at;
                        $scope.paymentAddModel.paymentdefault = defaultlist[i].name;
                    }
                }
            });

        };
        $scope.getpaymenttermslist();
        $scope.isLoadedTax = false;
        $scope.getTaxListInfo = function( )
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getTaxList(getListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.orderListModel.taxInfo = data;
                $scope.isLoadedTax = true;
            });
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isLoadedTax)
            {
                $scope.updateInvoiceDetails();
                $scope.getCustomerInfo();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.getCustomerInfo = function(val)
        {
            var getListParam = {};
            getListParam.id = $scope.orderListModel.invoiceDetail.customer_id;
            getListParam.fname = $scope.orderListModel.invoiceDetail.customer_fname;
            getListParam.is_purchase = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCustomersList(getListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.orderListModel.customerInfo = data[0];
            });
        };

        $scope.updateInvoiceDetails = function( )
        {
            // $scope.orderListModel.customerInfo = {};
            // $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
            //  $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_fname;
            //  $scope.orderListModel.customerInfo.address = $scope.orderListModel.invoiceDetail.customer_address;
            $scope.orderListModel.subtotal = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.subtotal, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.taxAmt = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.tax_amount, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.discountAmt = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.discount_amount, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.total = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.total_amount, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.roundoff = $scope.orderListModel.invoiceDetail.round_off;
            $scope.orderListModel.advance_amount = utilityService.changeCurrency($scope.orderListModel.invoiceDetail.advance_amount, $rootScope.appConfig.thousand_seperator);
            $scope.orderListModel.advance_amount = parseFloat($scope.orderListModel.advance_amount).toFixed(2);
            $scope.orderListModel.balance_amount = $scope.orderListModel.invoiceDetail.total_amount - $scope.orderListModel.invoiceDetail.advance_amount;
            $scope.orderListModel.balance_amount = parseFloat($scope.orderListModel.balance_amount).toFixed(2);
            $scope.orderListModel.new_balance_amount = utilityService.changeCurrency($scope.orderListModel.balance_amount, $rootScope.appConfig.thousand_seperator);
            if ($scope.orderListModel.invoiceDetail.status == "paid")
                $scope.orderListModel.status = true;
            else
                $scope.orderListModel.status = false;
            $scope.orderListModel.invoicePrefix = $scope.orderListModel.invoiceDetail.prefix;
            var billdate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date);
            $scope.orderListModel.billdate = billdate;
            var billdate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.validuntil);
            $scope.orderListModel.duedate = billdate;
            var newdate=new Date($scope.orderListModel.invoiceDetail.date).toISOString();
            $scope.orderListModel.billdatehr=newdate;
            //        var date = utilityService.parseDateToStr($scope.orderListModel.billdate,"yyyy-MM-dd")
            //        $scope.orderListModel.billdate = $filter('date')($scope.currentDate, $scope.dateFormat);        
            //        updateOrderParam.date = utilityService.parseDateToStr($scope.orderListModel.billdate,"yyyy-MM-dd");                
            //        $scope.orderListModel.billdate = $scope.orderListModel.invoiceDetail.date;
            //        $scope.orderListModel.duedate = $scope.orderListModel.invoiceDetail.validuntil;

            $scope.orderListModel.discountPercentage = $scope.orderListModel.invoiceDetail.discount_percentage + '';
            $scope.orderListModel.tax_id = $scope.orderListModel.invoiceDetail.tax_id + '';
            var loop;

            for (loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
            {
                if ($scope.orderListModel.taxInfo[loop].id == $scope.orderListModel.invoiceDetail.tax_id)
                {
                    $scope.orderListModel.taxPercentage = $scope.orderListModel.invoiceDetail.tax_percentage;
                    break;
                }
            }

            for (loop = 0; loop < $scope.orderListModel.invoiceDetail.item.length; loop++)
            {
                var newRow =
                        {
                            "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                            "productName": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                            "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                            "sales_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                            "qty": $scope.orderListModel.invoiceDetail.item[loop].qty,
                            "rowtotal": (parseFloat($scope.orderListModel.invoiceDetail.item[loop].unit_price * $scope.orderListModel.invoiceDetail.item[loop].qty)).toFixed(2),
                            "uom": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                            "uomid": $scope.orderListModel.invoiceDetail.item[loop].uom_id,
                            "purchase_invoice_item_custom1_label_value": $scope.orderListModel.invoiceDetail.item[loop].custom_opt1,
                            "purchase_invoice_item_custom2_label_value": $scope.orderListModel.invoiceDetail.item[loop].custom_opt2,
                            "purchase_invoice_item_custom3_label_value": $scope.orderListModel.invoiceDetail.item[loop].custom_opt3,
                            "purchase_invoice_item_custom4_label_value": $scope.orderListModel.invoiceDetail.item[loop].custom_opt4,
                            "purchase_invoice_item_custom5_label_value": $scope.orderListModel.invoiceDetail.item[loop].custom_opt5

                        };
                $scope.orderListModel.list.push(newRow);
            }
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].rowtotal = utilityService.changeCurrency($scope.orderListModel.list[i].rowtotal, $rootScope.appConfig.thousand_seperator);
            }
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if ($scope.orderListModel.list[i].purchase_invoice_item_custom1_label_value != null && $scope.orderListModel.list[i].purchase_invoice_item_custom1_label_value != '')
                {
                    $scope.orderListModel.list[i].purchase_invoice_item_custom1_label_name = $rootScope.appConfig.purchase_invoice_item_custom1_label;
                }
                if ($scope.orderListModel.list[i].purchase_invoice_item_custom2_label_value != null && $scope.orderListModel.list[i].purchase_invoice_item_custom2_label_value != '')
                {
                    $scope.orderListModel.list[i].purchase_invoice_item_custom2_label_name = $rootScope.appConfig.purchase_invoice_item_custom2_label;
                }
                if ($scope.orderListModel.list[i].purchase_invoice_item_custom3_label_value != null && $scope.orderListModel.list[i].purchase_invoice_item_custom3_label_value != '')
                {
                    $scope.orderListModel.list[i].purchase_invoice_item_custom3_label_name = $rootScope.appConfig.purchase_invoice_item_custom3_label;
                }
                if ($scope.orderListModel.list[i].purchase_invoice_item_custom4_label_value != null && $scope.orderListModel.list[i].purchase_invoice_item_custom4_label_value != '')
                {
                    $scope.orderListModel.list[i].purchase_invoice_item_custom4_label_name = $rootScope.appConfig.purchase_invoice_item_custom4_label;
                }
                if ($scope.orderListModel.list[i].purchase_invoice_item_custom5_label_value != null && $scope.orderListModel.list[i].purchase_invoice_item_custom5_label_value != '')
                {
                    $scope.orderListModel.list[i].purchase_invoice_item_custom5_label_name = $rootScope.appConfig.purchase_invoice_item_custom5_label;
                }
            }
            $scope.orderListModel.isLoadingProgress = false;
        }

        $scope.taxList = [];
        $scope.calculatetotal = function()
        {
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            console.log("bfr loop", $scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].rowtotal = (parseFloat($scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty)).toFixed(2);
                subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                if ($scope.orderListModel.list[i].taxPercentage == null)
                {
                    $scope.orderListModel.list[i].taxPercentage = 0;
                }
                if ($scope.orderListModel.list[i].discountPercentage == null)
                {
                    $scope.orderListModel.list[i].discountPercentage = 0;
                }
            }
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            totTaxPercentage = parseFloat($scope.orderListModel.taxPercentage);
            totDiscountPercentage = parseFloat($scope.orderListModel.discountPercentage);

            if (totDiscountPercentage > 0)
            {
                $scope.orderListModel.discountAmt = parseFloat((subTotal) * (totDiscountPercentage / 100)).toFixed(2);
            }

            if (totTaxPercentage > 0)
            {
                $scope.orderListModel.taxAmt = parseFloat((subTotal - $scope.orderListModel.discountAmt) * (totTaxPercentage / 100)).toFixed(2);
            }

            $scope.orderListModel.total = (parseFloat(subTotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.advance_amount)).toFixed(2);
        }
        $scope.orderListModel.totalTax = 0;
        for (i = 0; i < $scope.taxList.length; i++)
        {
            $scope.orderListModel.totalTax += $scope.taxList[i].taxPercentage;
        }
        $scope.orderListModel.totalTaxPercentage = $scope.orderListModel.totalTax;
        $scope.$on("updateSelectedProductEvent", function(event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }
                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                    $scope.updateInvoiceTotal();
                }
                else
                {
                    var newRow =
                            {
                                "id": selectedItems.id,
                                "productName": selectedItems.name,
                                "sku": selectedItems.sku,
                                "sales_price": selectedItems.sales_price,
                                "qty": 1,
                                "rowtotal": selectedItems.sales_price,
                                "discountPercentage": selectedItems.discountPercentage,
                                "taxPercentage": selectedItems.taxPercentage,
                                "uom": selectedItems.uom,
                                "uomid": selectedItems.uom_id
                            }
                    $scope.orderListModel.list.push(newRow);
                    console.log("upd 2");
                    $scope.updateInvoiceTotal();
                    return;
                }
            }
            else
            {

                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "sales_price": selectedItems.sales_price,
                    "qty": 1,
                    "rowtotal": selectedItems.sales_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom,
                    "uomid": selectedItems.uom_id
                }
                $scope.orderListModel.list.push(newRow);
                console.log("upd 1");
                $scope.updateInvoiceTotal();
            }
        });


        $scope.getInvoiceInfo = function( )
        {
            $scope.orderListModel.isLoadingProgress = true;
            var headers = {};
            headers['screen-code'] = 'purchaseestimate';
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;

            $scope.orderListModel.list = [];
            var configOption = adminService.handleOnlyErrorResponseConfig;

            adminService.getPurchaseQuoteDetail(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.data;
                    $scope.orderListModel.invoiceDetail = data;
                    $scope.initUpdateDetail();
                    $scope.getInvoicePaymentList( );
                    $scope.getTaxListInfo( );
                }
                else
                {
                    $scope.orderListModel.isLoadingProgress = false;
                }
            });
        }

        $scope.getInvoicePaymentList = function( )
        {
            var getListParam = {};
            getListParam.purchase_quote_id = $stateParams.id;
            getListParam.is_active = 1;
            var headers = {};
            headers['screen-code'] = 'purchaseestimate';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getPurchasePaymentList(getListParam, configOption, headers).then(function(response)
            {
                var data = response.data.list;
                $scope.orderListModel.paymentList = data;

                console.log("Payment List All ", $scope.orderListModel.paymentList);
                for (var loop = 0; loop < $scope.orderListModel.paymentList.length; loop++)
                {
                    var date = $scope.orderListModel.paymentList[loop].date.split(' ');
                    if (date.length > 0)
                    {
                        $scope.orderListModel.paymentList[loop].newdate = $filter('date')(date[0], $scope.dateFormat);
                    }
                    else
                    {
                          $scope.orderListModel.paymentList[loop].newdate =   $filter('date')($scope.orderListModel.paymentList[loop].date, $scope.dateFormat);
                    }
                    var dateinfo = $filter('date')($scope.orderListModel.paymentList[loop].created_at, $scope.dateFormat);
                    $scope.orderListModel.paymentList[loop].amount = utilityService.changeCurrency($scope.orderListModel.paymentList[loop].amount, $rootScope.appConfig.thousand_seperator);

                    //var timeinfo = $scope.orderListModel.paymentList[loop].split(' ');    
                    //$scope.orderListModel.paymentList[loop].created_at = dateinfo + " " + timeinfo;    
                }
            });
        }

        $scope.getExpenseCategoryList = function() {

            var getListParam = {};
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.type = 'expense';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getIncomeCategoryList(getListParam, configOption).then(function(response) {
                var data = response.data.list;
                $scope.paymentAddModel.categoryList = data;
            });

        };

        $scope.getAccountlist = function() {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getAccountlist(getListParam, configOption).then(function(response) {
                var data = response.data.list;
                $scope.paymentAddModel.accountsList = data;
            });

        };

        $scope.getpaymenttermslist();
        $scope.getExpenseCategoryList();
        $scope.getInvoiceInfo( );
        $scope.getAccountlist();
        $scope.updateOrder = function()
        {
            $scope.isDataSavingProcess = true;
            var updateOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'purchaseestimate';
            //updateOrderParam.status = 'unpaid';                
            updateOrderParam.prefix = $scope.orderListModel.invoicePrefix;

            var billdate = utilityService.parseStrToDate($scope.orderListModel.billdate);
            updateOrderParam.date = $filter('date')(billdate, "yyyy-MM-dd");
            var billdate = utilityService.parseStrToDate($scope.orderListModel.duedate);
            updateOrderParam.validuntil = $filter('date')(billdate, "yyyy-MM-dd");
            updateOrderParam.customer_address = $scope.orderListModel.customerInfo.address;
            updateOrderParam.subtotal = $scope.orderListModel.subtotal;

            updateOrderParam.tax_amount = $scope.orderListModel.taxAmt;
            updateOrderParam.discount_amount = $scope.orderListModel.discountAmt;
            updateOrderParam.total_amount = $scope.orderListModel.total;
            updateOrderParam.paymentmethod = "",
                    updateOrderParam.notes = "",
                    updateOrderParam.item = [];
            updateOrderParam.item.date = utilityService.parseDateToStr($scope.orderListModel.billdate, "yyyy-MM-dd");
            updateOrderParam.item.outletId = 1;
            updateOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            updateOrderParam.item.orderDetails = [];
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                var ordereditems = {};
                ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                ordereditems.product_name = $scope.orderListModel.list[i].productName;
                ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].sales_price);
                ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty);
                ordereditems.uom_name = $scope.orderListModel.list[i].uom;
                ordereditems.uom_id = $scope.orderListModel.list[i].uomid;
                ordereditems.total_price = (parseFloat($scope.orderListModel.list[i].sales_price * ordereditems.qty)) / 100;
                updateOrderParam.item.push(ordereditems);
            }
            adminService.editPurchaseInvoice(updateOrderParam, $stateParams.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.isDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.purchaseQuote');
                }
            });
        };
        $scope.steColspan = function() {

            $scope.colsPanValue = '';
            if ($scope.colsPan == true)
            {
                $scope.colsPanValue = 5;

            }
            else
            {
                $scope.colsPanValue = 4;
            }

        };
        $scope.amountColspan = function() {

            $scope.amountcolsPan = '';
            if ($scope.colsPan == true)
            {
                $scope.amountcolsPan = 3;

            }
            else
            {
                $scope.amountcolsPan = 2;
            }

        };
        $scope.steColspan();
        $scope.amountColspan();
    }]);