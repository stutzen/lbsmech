
app.controller('workorderViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.workModel =
                {
                    "date": "",
                    "next_date": "",
                    "taskList": [],
                    "details": [],
                    "list": [],
                    "teamId": '',
                    "user_id": "",
                    "user_name": "",
                    "userInfo": "",
                    "inputProducts": [],
                    "outputProducts": []
                }
        $scope.validationFactory = ValidationFactory;
        $scope.stateParamData = $stateParams.id;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.adminService = adminService;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.work_add_form !== 'undefined' && typeof $scope.work_add_form.$pristine !== 'undefined' && !$scope.work_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            $scope.work_add_form.$setPristine();

        };
        
        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                $scope.updateWorkInfo();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

//            $scope.updateWorkInfo = function ()
//            {
//                if ($scope.workModel.list != null)
//                {
//                    $scope.workModel.date = new Date($scope.workModel.list.date);
//                    $scope.workModel.details = [];
//                    for (var i = 0; i < $scope.workModel.list.details.length; i++)
//                    {
//                        var detailsParam = {};
//                        detailsParam.id = $scope.workModel.list.details[i].id;
//                        detailsParam.taskInfo = {
//                            old: true,
//                            id: $scope.workModel.list.details[i].task_id,
//                            name: $scope.workModel.list.details[i].task_name,
//                                
//                            output_pdt_id: $scope.workModel.list.details[i].output_pdt_id,
//                            product_for_name: $scope.workModel.list.details[i].product_for_name,
//                            output_pdt_name: $scope.workModel.list.details[i].output_pdt_name,
//                            output_pdt_uom: $scope.workModel.list.details[i].output_pdt_uom,
//                            inputs: []
//                        };
//                        detailsParam.id = $scope.workModel.list.details[i].task_id;
//                        detailsParam.output_pdt_id = $scope.workModel.list.details[i].output_pdt_id;
//                        detailsParam.product_for_name = $scope.workModel.list.details[i].product_for_name;
//                        detailsParam.output_pdt_uom = $scope.workModel.list.details[i].output_pdt_uom;
//                        detailsParam.inputs = [];
//                        for (var j = 0; j < $scope.workModel.list.details[i].inputs.length; j++)
//                        {
//                            var inputParam = {};
//                            inputParam.id = $scope.workModel.list.details[i].inputs[j].config_input_id;
//                            inputParam.input_pdt_id = $scope.workModel.list.details[i].inputs[j].input_pdt_id;
//                            inputParam.input_qty = $scope.workModel.list.details[i].inputs[j].input_qty;
//                            inputParam.input_pdt_name = $scope.workModel.list.details[i].inputs[j].input_pdt_name;
//                            inputParam.input_pdt_uom = $scope.workModel.list.details[i].inputs[j].input_pdt_uom;
//                            detailsParam.inputs.push(inputParam);
//                            detailsParam.taskInfo.inputs.push(inputParam);
//                        }
//                        $scope.workModel.details.push(detailsParam);
//                    }
//                      
//                }
//            };
       
        $scope.getWorkInfo = function () {

            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                $scope.isLoadedList = false;
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getWorkOrderDetail(getListParam, configOption).then(function (response) {

                    var data = response.data;
                    if (data.total != 0)
                    {
                        $scope.workModel.list = data.list[0];
                        if ($scope.workModel.list != null)
                        {
                            var newdate = utilityService.parseStrToDate($scope.workModel.list.date,$scope.dateFormat);
                            $scope.workModel.date =newdate;
                            $scope.workModel.details = [];
                            for (var i = 0; i < $scope.workModel.list.details.length; i++)
                            {
                                var detailsParam = {};
                                detailsParam.id = $scope.workModel.list.details[i].id;
                                detailsParam.taskInfo = {
                                    old: true,
                                    id: $scope.workModel.list.details[i].task_id,
                                    name: $scope.workModel.list.details[i].task_name,
                                    qty: $scope.workModel.list.details[i].qty,
                                    output_pdt_id: $scope.workModel.list.details[i].output_pdt_id,
                                    product_for_name: $scope.workModel.list.details[i].product_for_name,
                                    output_pdt_name: $scope.workModel.list.details[i].output_pdt_name,
                                    output_qty: $scope.workModel.list.details[i].output_qty,
                                    output_pdt_uom: $scope.workModel.list.details[i].output_pdt_uom,
                                    inputs: []
                                };
                                detailsParam.id = $scope.workModel.list.details[i].task_id;
                                detailsParam.output_pdt_id = $scope.workModel.list.details[i].output_pdt_id;
                                detailsParam.product_for_name = $scope.workModel.list.details[i].product_for_name;
                                detailsParam.output_pdt_name = $scope.workModel.list.details[i].output_pdt_name;
                                detailsParam.output_qty = $scope.workModel.list.details[i].output_qty;
                                detailsParam.output_pdt_uom = $scope.workModel.list.details[i].output_pdt_uom;
                                detailsParam.inputs = [];
                                for (var j = 0; j < $scope.workModel.list.details[i].inputs.length; j++)
                                {
                                    var inputParam = {};
                                    inputParam.id = $scope.workModel.list.details[i].inputs[j].config_input_id;
                                    inputParam.input_pdt_id = $scope.workModel.list.details[i].inputs[j].input_pdt_id;
                                    inputParam.input_qty = $scope.workModel.list.details[i].inputs[j].input_qty;
                                    inputParam.input_pdt_name = $scope.workModel.list.details[i].inputs[j].input_pdt_name;
                                    inputParam.input_pdt_uom = $scope.workModel.list.details[i].inputs[j].input_pdt_uom;
                                    detailsParam.inputs.push(inputParam);
                                    detailsParam.taskInfo.inputs.push(inputParam);
                                }
                                $scope.workModel.details.push(detailsParam);
                            }
                            $scope.formatArray();
                        }
                        // $scope.initUpdateDetail();
                    }

                });
            }
        };
        
        $scope.formatArray = function ()
        {
            var inputArray = [];
            var outputArray = [];

            var startInput = 0;
            var startOutput = 0;

            $scope.workModel.outputProducts = [];
            $scope.workModel.inputProducts = [];
            for (var i = 0; i < $scope.workModel.details.length; i++)
            {
                var newoutputRow = {
                    output_pdt_id: $scope.workModel.details[i].output_pdt_id,
                    output_pdt_qty: parseFloat($scope.workModel.details[i].output_qty),
                    output_pdt_name: $scope.workModel.details[i].output_pdt_name,
                    output_pdt_uom : $scope.workModel.details[i].output_pdt_uom
                };
                $scope.workModel.outputProducts.push(newoutputRow);
                for (var j = 0; j < $scope.workModel.details[i].inputs.length; j++)
                {
                    var newinputRow = {
                        input_pdt_id: $scope.workModel.details[i].inputs[j].input_pdt_id,
                        input_pdt_name: $scope.workModel.details[i].inputs[j].input_pdt_name,
                        input_qty: parseFloat($scope.workModel.details[i].inputs[j].input_qty),
                        input_pdt_uom : $scope.workModel.details[i].inputs[j].input_pdt_uom
                    };
                    $scope.workModel.inputProducts.push(newinputRow);
                }
            }

            if ($scope.workModel.inputProducts.length > 0)
            {
                inputArray.push($scope.workModel.inputProducts[0]);
                startInput = 1;
            }
            if ($scope.workModel.inputProducts.length > 1)
            {
                for (var i = startInput; i < $scope.workModel.inputProducts.length; i++)
                {
                    var found = false;
                    for (var j = 0; j < inputArray.length; j++)
                    {
                        if (inputArray[j].input_pdt_id == $scope.workModel.inputProducts[i].input_pdt_id)
                        {
                            inputArray[j].input_qty = parseFloat(inputArray[j].input_qty) + parseFloat($scope.workModel.inputProducts[i].input_qty)
                            found = true;
                        }
                    }
                    if (found == false)
                    {
                        inputArray.push($scope.workModel.inputProducts[i]);
                    }
                }
            }
            $scope.workModel.inputProducts = inputArray;
            console.log(inputArray);

            if ($scope.workModel.outputProducts.length > 0)
            {
                outputArray.push($scope.workModel.outputProducts[0]);
                startOutput = 1;
            }
            if ($scope.workModel.outputProducts.length > 1)
            {
                for (var i = startOutput; i < $scope.workModel.outputProducts.length; i++)
                {
                    var found = false;
                    for (var j = 0; j < outputArray.length; j++)
                    {
                        if (outputArray[j].output_pdt_id == $scope.workModel.outputProducts[i].output_pdt_id)
                        {
                            outputArray[j].output_pdt_qty = parseFloat(outputArray[j].output_pdt_qty) + parseFloat($scope.workModel.outputProducts[i].output_pdt_qty)
                            found = true;
                        }
                    }
                    if (found == false)
                    {
                        outputArray.push($scope.workModel.outputProducts[i]);
                    }
                }
            }
            $scope.workModel.outputProducts = outputArray;
            console.log(outputArray);
        };

        $scope.getWorkInfo();
    }]);








