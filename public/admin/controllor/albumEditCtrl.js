

app.controller('albumEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.albumAddModel = {
            "id": "",
            "name": "",
            "code": "",
            "type": "",
            "comments": "",
            isLoadingProgress: false

        };
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.album_edit_form != 'undefined' && typeof $scope.album_edit_form.$pristine != 'undefined' && !$scope.album_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            if (typeof $scope.album_edit_form != 'undefined')
            {
                $scope.album_edit_form.$setPristine();
                $scope.updateAlbumInfo();
            }

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.updateAlbumInfo = function()
        {
            $scope.albumAddModel.id = $scope.albumAddModel.list.id;
            $scope.albumAddModel.name = $scope.albumAddModel.list.name;
            $scope.albumAddModel.code = $scope.albumAddModel.list.code;
            $scope.albumAddModel.type = $scope.albumAddModel.list.type;
            $scope.albumAddModel.comments = $scope.albumAddModel.list.comments;
            $scope.albumAddModel.is_active = 1;
            $scope.albumAddModel.isLoadingProgress = false;
        }

        $scope.editAlbum = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var modifyAlbumParam = {};
                var headers = {};
                headers['screen-code'] = 'albumlist';
                modifyAlbumParam.id = $scope.albumAddModel.id;
                modifyAlbumParam.name = $scope.albumAddModel.name;
                // modifyAlbumParam.code = $scope.albumAddModel.code;
                modifyAlbumParam.type = $scope.albumAddModel.type;
                modifyAlbumParam.comments = $scope.albumAddModel.comments;
                modifyAlbumParam.is_active = 1;

                adminService.modifyAlbum(modifyAlbumParam, modifyAlbumParam.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.albumlist');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };
        $scope.getAlbum = function() {
            $scope.albumAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getalbumlist(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.albumAddModel.list = data.list[0];
                }
                $scope.albumAddModel.total = data.total;
                $scope.updateAlbumInfo();

            });
        };
        $scope.getAlbum();
    }]);




