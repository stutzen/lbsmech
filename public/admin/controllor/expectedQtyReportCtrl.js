
app.controller('expectedQtyReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout','$localStorageService','$localStorage', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout,$localStorageService,$localStorage) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.expectedQtyModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            productname: '',
            uom: '',
            quantity: '',
            amount: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.expectedQtyModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0)); 
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.company_name = $rootScope.appConfig.app_tile;
        $scope.company_address = $rootScope.appConfig.pay_to_address;
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: ''
        };
        //$scope.searchFilter.todate = $scope.currentDate;
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                customerInfo :{}
            };
            $scope.expectedQtyModel.list = [];
           // $scope.searchFilter.todate = $scope.currentDate;
        }

        $scope.print = function()
        {
            $window.print();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.show = function(index)
        {
                    if ($scope.showDetails[index])
                    {
                        $scope.showDetails[index] = false;
                    }
                    else
                        $scope.showDetails[index] = true;
            };
            $scope.getCustomerList = function (val)
            {
                var autosearchParam = {};
                autosearchParam.fname = val;
                autosearchParam.is_active = 1;
                return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            };
             $scope.formatCustomerModel = function (model)
                {
                    if (model != null)
                    {
                        if (model.fname != undefined)
                        {
                            return model.fname;
                        }
                    }
                    return  '';
                };
        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'receivedqty';
            $scope.expectedQtyModel.isLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getExpectedQtyReport(getListParam,configOption,headers).then(function(response)
            {
                var totalQty = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.expectedQtyModel.list = data;
                    for (var i = 0; i < $scope.expectedQtyModel.list.length; i++)
                    {
                        $scope.expectedQtyModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                        totalQty = parseInt(totalQty) + parseInt($scope.expectedQtyModel.list[i].qty);
                        $scope.expectedQtyModel.totalQty = totalQty;
                    }

                }
                $scope.expectedQtyModel.isLoadingProgress = false;
            });

        };
        $scope.redirect = function(id,date)
        {
            $localStorage.user_id = id;
            $localStorage.date = date;
        }
        

        $scope.getList();
    }]);




