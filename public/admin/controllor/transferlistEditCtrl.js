




app.controller('transferlistEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.transferlistModel = {
            "account": "",
            "credit": "",
            "particulars": "",
            "transaction_date": "",
            "status": "",
            "length": "",
            "isActive": true



        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.transferlist_edit_form !== 'undefined' && typeof $scope.transferlist_edit_form.$pristine !== 'undefined' && !$scope.transferlist_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            $scope.transferlist_edit_form.$setPristine();
            $scope.updateTransferlistInfo();

        };
//        $scope.currentDate = new Date();
//       $scope.transferlistModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTabletCreate = function() {

            $scope.transferlistModel.user = '';

        };
        $scope.modifyTransferlist = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var modifyTransferlistParam = {};
                var headers = {};
                headers['screen-code'] = 'transfer';
                //modifyTransferlistParam.transaction_date = $filter('date')($scope.transferlistModel.transaction_date,  $rootScope.appConfig.date_format);
                modifyTransferlistParam.account = $scope.transferlistModel.account;
                modifyTransferlistParam.particulars = $scope.transferlistModel.particulars;
                modifyTransferlistParam.credit = $scope.transferlistModel.credit;
                modifyTransferlistParam.isactive = 1;
                adminService.modifytransferlist(modifyTransferlistParam, $scope.transferlistModel.id, headers).then(function(response) {
                    if (response.data.success === true)
                    {
                        $scope.isDataSavingProcess = false;
                        data.list[0].transaction_date = utilityService.parseDateToStr(data.list[0].transaction_date,$rootScope.appConfig.date_format);
                        $scope.formReset();
                        $state.go('app.transferlist');
                    }
                });
            }
        };

        $scope.updateTransferlistInfo = function()
        {
            if ($scope.transferlistModel.list !== null)
            {
                //$scope.transferlistModel.transaction_date = $scope.transferlistModel.list.transaction_date;
                $scope.transferlistModel.account = $scope.transferlistModel.list.account;
                $scope.transferlistModel.particulars = $scope.transferlistModel.list.particulars;
                $scope.transferlistModel.id = $scope.transferlistModel.list.id;
                var date = utilityService.parseStrToDate($scope.transferlistModel.list.date , $rootScope.appConfig.date_format);
                $scope.transferlistModel.transaction_date = $filter('date')($scope.transferlistModel.list.transaction_date);
                $scope.transferlistModel.credit = $scope.transferlistModel.list.credit;
                // $scope.transferlistModel.accountlistInfo.id = $scope.transferlistModel.list.desig_account;

            }
        };




        $scope.getTransferlistInfo = function() {

            if (typeof $stateParams.id !== 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.gettransferlist(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        data.list[0].transaction_date = utilityService.parseDateToStr(data.list[0].transaction_date, $rootScope.appConfig.date_format);
                        $scope.transferlistModel.list = data.list[0];
                        $scope.updateTransferlistInfo();
                    }

                });
            }
        };

        $scope.getAccountlist = function(val)
        {
            var autosearchParam = {};
            autosearchParam.account = val;
            return $httpService.get(APP_CONST.API.GET_ACCOUNT_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formataccountModel = function(model) {

            if (model !== null && model != undefined)
            {

                return model.account;
            }
            return  '';
        };

        $scope.getTransferlistInfo();


    }]);




