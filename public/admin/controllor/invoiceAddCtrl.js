
app.controller('invoiceAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": '0.00',
                    "taxAmt": '0.00',
                    "taxInfo": "",
                    "discountAmt": '0.00',
                    list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "customerInfo": '',
                    "payeeInfo": '',
                    "consigneeInfo": '',
                    "invoiceDetail": {},
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "duedate": "",
                    "balance_amount": "",
                    "paid_amount": "",
                    "tax_id": "",
                    "productList": [],
                    "productId": '',
                    "notes": '',
                    "discountMode": '%',
                    "roundoff": '0.00',
                    "invoiceno": '',
                    "taxAmountDetail": {
                        "tax_amount": ''
                    },
                    //"taxAmountDetail": '',
                    "subtotalWithDiscount": '',
                    "round": '0.00',
                    "dealId": '',
                    "dealName": '',
                    "insuranceCharge": 0,
                    "insuranceAmt": '0.00',
                    "packagingCharge": '0',
                    "packagingAmt": '0.00',
                    "is_next_autogenerator_num_changed": false,
                    "next_autogenerator_num": "",
                    "prefixList": [],
                    "serialno": "",
                    "companyInfo": {},
                    "billtype": '',
                    "service_machine_id": '',
                    "serviceRequestDetail": '',
                    "amcDetails": ''
                };
        $scope.adminService = adminService;
        $scope.customAttributeList = [];
        $scope.orderListModel.invoicePrefix = adminService.appConfig.invoice_prefix;
        $scope.senderEmail = '';
        $scope.receiver = [];
        $scope.ccEmail = '';
        $scope.showPopup = function (index) {
            $scope.showpopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            $scope.receiver = [];
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;

            $scope.$broadcast("initPopupEvent");
        };

        $scope.addMoreReceiverEmail = function ( )
        {
            var newRow = {
                "email": ""
            };
            $scope.receiver.push(newRow);
        }

        $scope.closePopup = function () {
            $scope.showpopup = false;
        }

        $scope.onFocus = function (e) {
            $timeout(function () {
                $(e.target).trigger('input');
            });
        };

        $scope.showItems = false;
        $scope.showAreaPopup = function ()
        {
            $scope.showItems = true;
        }
        $scope.showCustomerAddPopup = false;
        $scope.showCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = true;
            if (typeof $scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo != null)
            {
                $rootScope.$broadcast('customerInfo', $scope.orderListModel.customerInfo);
            }
        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
        }

        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.orderListModel.customerInfo = customerDetail;
        });

        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.order_add_form != 'undefined' && typeof $scope.order_add_form.$pristine != 'undefined' && !$scope.order_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {

            $scope.order_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function () {

            $scope.orderListModel.user = '';
        }

        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date();
        $scope.orderListModel.billdate = $scope.currentDate;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            }
        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.getCompanyList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCompanyModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        
        $scope.custDrop = function () {
            if ($scope.orderListModel.customerInfo.id == undefined) {
                $scope.orderListModel.customerInfo = '';
            }
        };



//        $scope.$watch('orderListModel.customerInfo', function (newVal, oldVal)
//        {
//            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
//            {
//                $scope.orderListModel.customerInfo = '';
//            } else if (typeof $stateParams.id == 'undefined' || $stateParams.id == null || $stateParams.id == '' || $stateParams.id == 0)
//            {
//                var addrss = "";
//
//                if (newVal.billing_address != undefined && newVal.billing_address != '')
//                {
//                    addrss += newVal.billing_address;
//                }
//                if (newVal.billing_city != undefined && newVal.billing_city != '')
//                {
//                    addrss += '\n' + newVal.billing_city;
//                }
//                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
//                {
//                    if (newVal.billing_city != undefined && newVal.billing_city != '')
//                    {
//                        addrss += '-' + newVal.billing_pincode;
//                    } else
//                    {
//                        addrss += newVal.billing_pincode;
//                    }
//                }
//
//                if (newVal.billing_state != undefined && newVal.billing_state != '')
//                {
//                    addrss += '\n' + newVal.billing_state;
//                }
//
//                $scope.orderListModel.customerInfo.shopping_address = addrss;
//            }
//        });


        $scope.getUomList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.isLoadedTax = false;
        $scope.getTaxListInfo = function ( )
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getTaxList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.orderListModel.taxInfo = data;
                    if (typeof $stateParams.id != 'undefined' && $stateParams.id != null && $stateParams.id != '' && $stateParams.id != 0)
                    {

                        $scope.orderListModel.tax_id = $scope.orderListModel.invoiceDetail.tax_id + '';
                    } else
                    {
                        if ($scope.orderListModel.taxInfo.length > 0) {

                            $scope.orderListModel.tax_id = $scope.orderListModel.taxInfo[0].id + '';
                        }

                    }
                    $scope.isLoadedTax = true;
                }
            });
        };

        $scope.calculateDueDate = function ( )
        {
            if ($scope.orderListModel.paymentTerm != "")
            {
                var billdate = utilityService.parseStrToDate($scope.orderListModel.billdate);
                $scope.orderListModel.duedate = utilityService.incrementDate(billdate, $scope.orderListModel.paymentTerm);
            } else
            {
                $scope.orderListModel.duedate = $scope.orderListModel.billdate;
            }
        }

        $scope.deleteProduct = function (id)
        {
            var index = -1;
            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
                if (id == $scope.orderListModel.list[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.calculatetotal();
            $scope.calculateTaxAmt();
        };

        $scope.totalcalculationTimeoutPromise = null;
        $scope.updateInvoiceTotal = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedTax == true && $stateParams.id != "" && $stateParams.id != undefined)
            {
                $scope.updateInvoiceDetails();
            }
        }

        $scope.showMailPopup = false;
        $scope.showPaymentPopup = function ()
        {
            $scope.showMailPopup = true;
            $scope.invoiceDateOpen = false;
            $scope.paymentAddModel.invoice_id = $scope.orderListModel.invoiceDetail.id;
            $scope.paymentAddModel.customer_id = $scope.orderListModel.invoiceDetail.customer_id;
            $scope.paymentAddModel.account = $scope.orderListModel.invoiceDetail.customer_fname;
            $scope.paymentAddModel.invoicedate = $filter('date')($scope.currentDate, $scope.dateFormat);
            $scope.paymentAddModel.amount = $scope.orderListModel.invoiceDetail.balance_amount;
            $scope.paymentAddModel.comments = $scope.orderListModel.invoiceDetail.comments;
        };

        $scope.closePaymentPopup = function ( )
        {
            $scope.showMailPopup = false;
        }

        //    $scope.getProductList = function(val)
        //    {
        //        var autosearchParam = {};
        //        autosearchParam.name = val;
        //        autosearchParam.is_active = 1;
        //        autosearchParam.is_sale = 1;
        //        if(typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
        //        {    
        //            autosearchParam.scopeId = $scope.selectedAssignFrom.id;
        //        }                           
        //
        //        return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
        //        {
        //            var data = responseData.data.list;
        //            var hits = data;
        //            if (hits.length > 10)
        //            {
        //                hits.splice(10, hits.length);
        //            }
        //            return hits;
        //        });        
        //    }

        $scope.formatProductModel = function (model, index)
        {
            if (model != null)
            {
                $scope.updateProductInfo(model, index);
                return model.name;
            }
            return  '';
        }

        $scope.updateProductInfo = function (model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].sales_price = model.sales_price;
                if (model.qty != null && model.qty != '')
                {
                    $scope.orderListModel.list[index].qty = model.qty;
                } else
                {
                    $scope.orderListModel.list[index].qty = 1;
                }

                $scope.orderListModel.list[index].rowtotal = model.sales_price;
                $scope.orderListModel.list[index].discountPercentage = model.discountPercentage;
                $scope.orderListModel.list[index].taxPercentage = model.taxPercentage;
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                }
                if (model.uomid != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uomid;
                }
                if (model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = model.uom_id;
                }
                if (model.uomid != undefined && model.uom_id != undefined)
                {
                    $scope.orderListModel.list[index].uomid = "";
                }
                $scope.orderListModel.list[index].hsn_code = model.hsn_code;
                $scope.updateInvoiceTotal();

                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
                $scope.createNewProduct(index);
                return;
            } else
            {
                $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.initValidateProductDataAndAddNew = function (index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function () {
                        $scope.createNewProduct(index);
                    },
                    300);
        };

        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.order_add_form.$submitted)
            {
                $timeout(function () {
                    $scope.order_add_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function () {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.createNewProduct = function (index)
        {
            var formDataError = false;
            if (index != -1)
            {
                var productContainer = window.document.getElementById('invoice_product_container');
                var errorCell = angular.element(productContainer).find('.has-error').length;
                if (errorCell > 0)
                {
                    formDataError = true;
                }
            }

            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1)
                {

                    var newRow = {
                        "id": '',
                        "productName": '',
                        "sku": '',
                        "sales_price": '',
                        "serialno": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "taxPercentage": '',
                        "uom": '',
                        "uomid": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": '',
                        "hsn_code": "",
                        "duplicationChecked": false,
                        "isDuplicate": false
                    }
                    $scope.orderListModel.list.push(newRow);
                }
            }

        }
        $scope.addNewProduct = function ()
        {
            var newRow = {
                "id": '',
                "productName": '',
                "sku": '',
                "sales_price": '',
                "serialno": '',
                "qty": '',
                "rowtotal": '',
                "discountPercentage": '',
                "taxPercentage": '',
                "uom": '',
                "uomid": '',
                "custom_opt1": '',
                "custom_opt2": '',
                "custom_opt3": '',
                "custom_opt4": '',
                "custom_opt5": '',
                "hsn_code": '',
                "duplicationChecked": false,
                "isDuplicate": false
            }
            $scope.orderListModel.list.push(newRow);
        }

        //        $scope.deleteProduct = function(id)
        //        {
        //            var index = -1;
        //            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
        //                if (id == $scope.orderListModel.list[i].id) {
        //                    index = i;
        //                    break;
        //                }
        //            }
        //            $scope.orderListModel.list.splice(index, 1);
        //            $scope.updateInvoiceTotal();
        //        };

        $scope.taxList = [];
        $scope.calculatetotal = function ()
        {
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            console.log("bfr loop", $scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                if ($scope.orderListModel.list[i].taxPercentage == null)
                {
                    $scope.orderListModel.list[i].taxPercentage = 0;
                }
                if ($scope.orderListModel.list[i].discountPercentage == null)
                {
                    $scope.orderListModel.list[i].discountPercentage = 0;
                }
            }
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);

            $scope.orderListModel.taxPercentage = '';
            for (var loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
            {
                if ($scope.orderListModel.tax_id == $scope.orderListModel.taxInfo[loop].id)
                {
                    $scope.orderListModel.taxPercentage = $scope.orderListModel.taxInfo[loop].tax_percentage;
                }
            }

            totTaxPercentage = parseFloat($scope.orderListModel.taxPercentage);
            totDiscountPercentage = parseFloat($scope.orderListModel.discountPercentage);
            if ($scope.orderListModel.taxPercentage == '' || isNaN($scope.orderListModel.taxPercentage))
            {
                totTaxPercentage = 0;
            }
            totTaxPercentage = parseFloat($scope.orderListModel.taxPercentage);
            if ($scope.orderListModel.discountPercentage == '' || isNaN($scope.orderListModel.discountPercentage))
            {
                totDiscountPercentage = 0;
            }
            totDiscountPercentage = parseFloat($scope.orderListModel.discountPercentage);

            if (totDiscountPercentage > 0)
            {
                if ($scope.orderListModel.discountMode == '%')
                {

                    $scope.orderListModel.discountAmt = parseFloat((subTotal) * (totDiscountPercentage / 100)).toFixed(2);
                } else if ($scope.orderListModel.discountMode == 'amount')
                {
                    $scope.orderListModel.discountAmt = parseFloat(totDiscountPercentage).toFixed(2);
                }
            } else
            {
                $scope.orderListModel.discountAmt = '0.00';
            }
            if (totTaxPercentage > 0 && typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
            {
                $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmountDetail.tax_amount).toFixed(2);
            } else if (totTaxPercentage > 0)
            {
                $scope.orderListModel.taxAmt = parseFloat((subTotal - $scope.orderListModel.discountAmt) * (totTaxPercentage / 100)).toFixed(2);
            } else
            {
                $scope.orderListModel.taxAmt = parseFloat(0).toFixed(2);
            }
            if ($scope.orderListModel.taxAmt == '' || isNaN($scope.orderListModel.taxAmt))
            {
                $scope.orderListModel.taxAmt = '0.00';
            }
            if ($scope.orderListModel.insuranceCharge == '' || isNaN($scope.orderListModel.insuranceCharge))
            {
                $scope.orderListModel.insuranceAmt = '0.00';
            } else
            {
                $scope.orderListModel.insuranceAmt = parseFloat($scope.orderListModel.insuranceCharge).toFixed(2);
            }
            if ($scope.orderListModel.packagingCharge == '' || isNaN($scope.orderListModel.packagingCharge))
            {
                $scope.orderListModel.packagingAmt = '0.00';
            } else
            {
                $scope.orderListModel.packagingAmt = parseFloat($scope.orderListModel.packagingCharge).toFixed(2);
            }
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                $scope.orderListModel.total = (parseFloat(subTotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt) + parseFloat($scope.orderListModel.insuranceAmt) + parseFloat($scope.orderListModel.packagingAmt)).toFixed(2);
            } else
            {
                $scope.orderListModel.subtotalWithDiscount = 0.00;
                $scope.orderListModel.total = 0.00;
            }
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.updateRoundoff();
        }
        $scope.updateRoundoff = function ()
        {
            $scope.orderListModel.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
        }
        $scope.orderListModel.totalTax = 0;
        for (i = 0; i < $scope.taxList.length; i++)
        {
            $scope.orderListModel.totalTax += $scope.taxList[i].taxPercentage;
        }
        $scope.orderListModel.totalTaxPercentage = $scope.orderListModel.totalTax;

        $scope.changeSerialNo = function (index)
        {
            $scope.orderListModel.list[index].duplicationChecked = false;
        };

        $scope.checkSerialNo = function (index)
        {
            var getListParam = {};
            if ($scope.orderListModel.list[index].serialno !== '' && $scope.orderListModel.list[index].serialno !== null && typeof $scope.orderListModel.list[index].serialno !== 'undefined')
            {
                getListParam.serial_no = $scope.orderListModel.list[index].serialno;
                getListParam.itemId = '';
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.checkSerialNo(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    $scope.orderListModel.list[index].duplicationChecked = true;
                    if (data.success === true)
                    {
                        $scope.orderListModel.list[index].isDuplicate = false;
                        for (var i = 0; i < $scope.orderListModel.list.length; i++)
                        {
                            if (index !== i)
                            {
                                if ($scope.orderListModel.list[index].serialno == $scope.orderListModel.list[i].serialno)
                                {
                                    $scope.orderListModel.list[index].isDuplicate = true;
                                }
                            }
                        }
                    } else
                    {
                        $scope.orderListModel.list[index].isDuplicate = true;
                    }
                });
            } else
            {
                $scope.orderListModel.list[index].duplicationChecked = false;
                $scope.orderListModel.list[index].isDuplicate = false;
            }
        };

        $scope.$watch('orderListModel.subtotalWithDiscount', function (newVal, oldVal)
        {
            if (typeof newVal != 'undefined' && newVal != '' && newVal != null)
            {
                $scope.calculateTaxAmt(false);
            }
        });

        $scope.calculateTaxAmt = function (forceSave)
        {
            if (($scope.orderListModel.subtotalWithDiscount != 0 && $scope.orderListModel.subtotalWithDiscount != '' && $scope.orderListModel.subtotalWithDiscount >= 0) &&
                    ($scope.orderListModel.total != 0 && $scope.orderListModel.total != '' && $scope.orderListModel.total >= 0))
            {
                var getListParam = {};
                getListParam.amount = $scope.orderListModel.subtotalWithDiscount;
                getListParam.tax_id = $scope.orderListModel.tax_id;

                adminService.calculateTaxAmt(getListParam).then(function (response)
                {
                    var data = response.data;
                    $scope.orderListModel.taxAmountDetail = data;
                    $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmountDetail.tax_amount).toFixed(2);

                    $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                    $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                    $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                    $scope.updateRoundoff();

                    if (forceSave)
                    {
                        $scope.createOrder(forceSave);
                    }

                    console.log('taxamtdetail');
                    console.log($scope.orderListModel.taxAmountDetail);
                });

            } else
            {
                $scope.orderListModel.taxAmountDetail = '';
                $scope.orderListModel.taxAmt = parseFloat(0).toFixed(2);
                if ($scope.orderListModel.subtotal > 0)
                {
                    $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                    $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                    $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                    $scope.updateRoundoff();
                }


                if (forceSave)
                {
                    $scope.createOrder(forceSave);
                }
                console.log('taxamtdetail');
                console.log($scope.orderListModel.taxAmountDetail);
            }
        }

        $scope.$on("updateSelectedProductEvent", function (event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }

                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                    $scope.updateInvoiceTotal();
                } else
                {
                    var newRow = {
                        "id": selectedItems.id,
                        "productName": selectedItems.name,
                        "sku": selectedItems.sku,
                        "sales_price": selectedItems.sales_price,
                        "serialno": '',
                        "qty": 1,
                        "rowtotal": selectedItems.sales_price,
                        "discountPercentage": selectedItems.discountPercentage,
                        "taxPercentage": selectedItems.taxPercentage,
                        "uom": selectedItems.uom,
                        "uomid": selectedItems.uom_id,
                        "hsn_code": selectedItems.hsn_code,
                        "duplicationChecked": false,
                        "isDuplicate": false
                    }
                    $scope.orderListModel.list.push(newRow);
                    console.log("upd 2");
                    $scope.updateInvoiceTotal();
                    return;
                }
            } else
            {
                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "sales_price": selectedItems.sales_price,
                    "qty": 1,
                    "serialno": '',
                    "rowtotal": selectedItems.sales_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom,
                    "uomid": selectedItems.uom_id,
                    "hsn_code": selectedItems.hsn_code,
                    "duplicationChecked": false,
                    "isDuplicate": false
                }
                $scope.orderListModel.list.push(newRow);
                $scope.updateInvoiceTotal();
            }
        });
        $scope.display_balance = false;
        $scope.updateInvoiceDetails = function ( )
        {
            if (typeof $scope.orderListModel.invoiceDetail != 'undefined' && $scope.orderListModel.invoiceDetail != null)
            {
                $scope.orderListModel.customerInfo = {};
                $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
                $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_fname;
                $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
                $scope.orderListModel.customerInfo.phone = $scope.orderListModel.invoiceDetail.phone;
                $scope.orderListModel.customerInfo.email = $scope.orderListModel.invoiceDetail.email;
                $scope.orderListModel.payeeInfo = {};
                $scope.orderListModel.payeeInfo.id = $scope.orderListModel.invoiceDetail.payee;
                $scope.orderListModel.payeeInfo.fname = $scope.orderListModel.invoiceDetail.payee_name;
                $scope.orderListModel.consigneeInfo = {};
                $scope.orderListModel.consigneeInfo.id = $scope.orderListModel.invoiceDetail.consignee;
                $scope.orderListModel.consigneeInfo.fname = $scope.orderListModel.invoiceDetail.consignee_name;
                $scope.orderListModel.subtotal = $scope.orderListModel.invoiceDetail.subtotal;
                $scope.orderListModel.taxAmt = $scope.orderListModel.invoiceDetail.tax_amount;
                $scope.orderListModel.insuranceCharge = $scope.orderListModel.invoiceDetail.insurance_charge;
                $scope.orderListModel.packagingCharge = $scope.orderListModel.invoiceDetail.packing_charge;
                $scope.orderListModel.taxAmountDetail.tax_amount = $scope.orderListModel.invoiceDetail.tax_amount;
                $scope.orderListModel.discountAmt = $scope.orderListModel.invoiceDetail.discount_amount;
                $scope.orderListModel.total = $scope.orderListModel.invoiceDetail.total_amount;
                $scope.display_balance = true;
                $scope.orderListModel.paid_amount = $scope.orderListModel.invoiceDetail.advance_amount;
                $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                $scope.orderListModel.invoicePrefix = $scope.orderListModel.invoiceDetail.prefix;
                //                var date = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date);
                //                $scope.orderListModel.billdate = $filter('date')(date, $scope.dateFormat);
                $scope.orderListModel.duedate = $scope.orderListModel.invoiceDetail.duedate;
                $scope.orderListModel.billtype = $scope.orderListModel.invoiceDetail.type;
                $scope.orderListModel.service_machine_id = $scope.orderListModel.invoiceDetail.service_request_id;
                $scope.orderListModel.discountPercentage = $scope.orderListModel.invoiceDetail.discount_percentage + '';
                $scope.orderListModel.tax_id = $scope.orderListModel.invoiceDetail.tax_id + '';
                $scope.orderListModel.discountMode = $scope.orderListModel.invoiceDetail.discount_mode;
                var loop;

                for (loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
                {
                    if ($scope.orderListModel.taxInfo[loop].id == $scope.orderListModel.invoiceDetail.tax_id)
                    {
                        $scope.orderListModel.taxPercentage = $scope.orderListModel.invoiceDetail.tax_percentage;
                        break;
                    }
                }

                for (loop = 0; loop < $scope.orderListModel.invoiceDetail.item.length; loop++)
                {
                    var newRow =
                            {
                                "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                                "name": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                                "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                                "serialno": $scope.orderListModel.invoiceDetail.item[loop].serialno,
                                "sales_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                                "qty": $scope.orderListModel.invoiceDetail.item[loop].qty,
                                "rowtotal": $scope.orderListModel.invoiceDetail.item[loop].total_price,
                                "uom": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                                "uomid": $scope.orderListModel.invoiceDetail.item[loop].uom_id,
                                "duplicationChecked": false,
                                "isDuplicate": false
                            };
                    $scope.orderListModel.list.push(newRow);
                    $scope.orderListModel.list[loop].productname = $scope.orderListModel.list[loop];
                }
                var newRow = {
                    "id": '',
                    "productName": '',
                    "sku": '',
                    "sales_price": '',
                    "serialno": '',
                    "qty": '',
                    "rowtotal": '',
                    "discountPercentage": '',
                    "taxPercentage": '',
                    "uom": '',
                    "uomid": '',
                    "hsn_code": '',
                    "duplicationChecked": false,
                    "isDuplicate": false
                }
                $scope.orderListModel.list.push(newRow);
                $scope.orderListModel.isLoadingProgress = false;
            }
        }
        $scope.getServiceRequestInfo = function ()
        {

            var getListParam = {};
            getListParam.id = $stateParams.service_id;
            $scope.orderListModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getServiceRequestList(getListParam, configOption).then(function (response) {
                var data = response.data;
                if (data.total !== 0)
                {
                    $scope.orderListModel.serviceRequestDetail = data.list[0];
                    $scope.orderListModel.billtype = 'service_request';
                    $scope.orderListModel.customerInfo = {
                        'fname': $scope.orderListModel.serviceRequestDetail.customer_name,
                        'id': $scope.orderListModel.serviceRequestDetail.customer_id
                    }
                }
            });
            $scope.orderListModel.isLoadingProgress = false;
        }

        $scope.getInvoiceInfo = function ( )
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            if ($stateParams.id != "")
            {
                getListParam.id = $stateParams.id;
                getListParam.is_active = 1;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

                adminService.getSalesOrderDetail(getListParam, configOption).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data.data;
                        $scope.orderListModel.invoiceDetail = data;
                        $scope.initUpdateDetail();

                    }
                });
            }
            $scope.orderListModel.isLoadingProgress = false;
        }
        $scope.getProductList = function ()
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getParam = {};

            getParam.is_active = 1;
            getParam.is_sale = 1;
            getParam.type = 'Product';
            getParam.localData = 1;
            if (typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
            {
                getParam.scopeId = $scope.selectedAssignFrom.id;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductList(getParam, configOption).then(function (response) {

                var data = response.data;
                $scope.orderListModel.productList = data.list;
                $scope.orderListModel.isLoadingProgress = false;
            });
        }
        $scope.dealButton = false;
        $scope.checkId = function ()
        {
            if (typeof $stateParams.id != 'undefined' && $stateParams.id != null && $stateParams.id != '' && $stateParams.id != 0) {
                $scope.getTaxListInfo();
                $scope.getInvoiceInfo();
            }
            if (typeof $stateParams.deal_id != 'undefined' && $stateParams.deal_id != null && $stateParams.deal_id != '')
            {
                $scope.getDealDetail($stateParams.deal_id);
                $scope.dealButton = true;
            }
            if (typeof $stateParams.cust_id != 'undefined' && $stateParams.cust_id != null && $stateParams.cust_id != '' && $stateParams.cust_id != 0) {
                $scope.getCustomerInfo();
            }
            if (typeof $stateParams.service_id != 'undefined' && $stateParams.service_id != null && $stateParams.service_id != '' && $stateParams.service_id != 0) {
                $scope.getServiceRequestInfo();
            }
            if (typeof $stateParams.amc_id != 'undefined' && $stateParams.amc_id != null && $stateParams.amc_id != '' && $stateParams.amc_id != 0) {
                $scope.getAmcInfo();
            }
        }
        $scope.getAmcInfo = function () {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.amc_id;
            adminService.getAmcList(getListParam).then(function (response) {
                var data = response.data.list;
                $scope.orderListModel.amcDetails = data[0];
                $scope.orderListModel.billtype = 'amc'
                $scope.orderListModel.customerInfo = {};
                $scope.orderListModel.customerInfo.id = $scope.orderListModel.amcDetails.customer_id;
                $scope.orderListModel.customerInfo.fname = $scope.orderListModel.amcDetails.customer_name;

            });
        };

        $scope.getCustomerInfo = function () {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.cust_id;
            getListParam.name = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.is_active = 1;
            adminService.getCustomerDetail(getListParam).then(function (response) {
                var data = response.data;
                if (data.total != 0)
                {
                    var customerInfo = data.data;
                    $scope.orderListModel.customerInfo = {};
                    $scope.orderListModel.customerInfo.id = customerInfo.id;
                    $scope.orderListModel.customerInfo.fname = customerInfo.fname;
                    $scope.orderListModel.customerInfo.billing_address = customerInfo.billing_address;
                    $scope.orderListModel.customerInfo.phone = customerInfo.phone;
                    $scope.orderListModel.customerInfo.email = customerInfo.email;
                    $scope.orderListModel.isLoadingProgress = false;
                }
            });
        };

        $scope.getDealDetail = function (id)
        {
            var getListParam = {};
            if (typeof id != 'undefined')
            {
                getListParam.id = id;
                getListParam.is_active = 1;
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.getDealList(getListParam, configOption).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data;
                        if (data.list.length > 0)
                        {
                            $scope.dealDetail = data.list[0];
                            $scope.orderListModel.dealId = $scope.dealDetail.id;
                            $scope.orderListModel.dealName = $scope.dealDetail.name;
                        }
                    }

                });
            }
        }
        $scope.getTaxListInfo();
        if (typeof $stateParams.id == 'undefined' || $stateParams.id == null || $stateParams.id == '' || $stateParams.id == 0)
        {
            $scope.updateProductInfo();
        }
        $scope.getProductList( );

        $scope.createOrder = function (forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if (i != $scope.orderListModel.list.length - 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != null &&
                                $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                        }
                    } else
                    {
                        if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                        }
                    }
                }
                if ($scope.orderListModel.list.length == 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.orderListModel.list[0].productname != '' && $scope.orderListModel.list[0].productname != null &&
                                $scope.orderListModel.list[0].uomInfo != '' && $scope.orderListModel.list[0].uomInfo != null &&
                                $scope.orderListModel.list[0].qty != '' && $scope.orderListModel.list[0].qty != null &&
                                $scope.orderListModel.list[0].sales_price != '' && $scope.orderListModel.list[0].sales_price != null &&
                                $scope.orderListModel.list[0].rowtotal != '' && $scope.orderListModel.list[0].rowtotal != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                        }
                    } else
                    {
                        if ($scope.orderListModel.list[0].productname != '' && $scope.orderListModel.list[0].productname != null &&
                                $scope.orderListModel.list[0].qty != '' && $scope.orderListModel.list[0].qty != null &&
                                $scope.orderListModel.list[0].sales_price != '' && $scope.orderListModel.list[0].sales_price != null &&
                                $scope.orderListModel.list[0].rowtotal != '' && $scope.orderListModel.list[0].rowtotal != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                        }
                    }
                }
            }

            if ($scope.isSave)
            {
                if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
                {
                    if ($scope.orderListModel.taxAmountDetail.tax_amount == $scope.orderListModel.taxAmt)
                    {
                        $scope.saveOrder();
                    } else
                    {
                        //                        $scope.orderListModel.taxAmt = $scope.orderListModel.taxAmountDetail.tax_amount;
                        //                        $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                        //                        $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                        //                        $scope.updateRoundoff();
                        //                        $scope.saveOrder();
                        $scope.calculateTaxAmt(true);
                    }
                } else
                {
                    $scope.calculateTaxAmt(true);
                }
            } else
            {
                sweet.show('Oops...', 'Fill Product Detail..', 'error');
                $scope.isDataSavingProcess = false;
            }
        }

        $scope.currentDate = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.createNotes = function ()
        {
            var createParam = {};
            var headers = {};
            headers['screen-code'] = 'salesinvoice';
            createParam.description = ''
            createParam.type = 'estimate';
            createParam.deal_id = $scope.orderListModel.dealId;
            createParam.customer_id = $scope.orderListModel.customerInfo.id;
            createParam.customer_name = $scope.orderListModel.customerInfo.fname;
            createParam.is_active = 1;
            createParam.date = $filter('date')($scope.currentDate, $scope.dateFormat);
            createParam.user_id = $rootScope.userModel.id;
            createParam.user_name = $rootScope.userModel.f_name;

            adminService.addDealActivity(createParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $state.go('app.dealedit', {
                        'id': $stateParams.id
                    }, {
                        'reload': true
                    });
                }
                $scope.isDataSavingProcess = false;
            });

        };

        $scope.saveOrder = function ()
        {
            var createOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'salesinvoice';
            createOrderParam.quote_id = $stateParams.id;
            //   createOrderParam.status = 'unpaid';
            createOrderParam.prefix = $scope.orderListModel.invoicePrefix;
            createOrderParam.invoice_no = $scope.orderListModel.invoiceno;
            //createOrderParam.id = $scope.orderListModel.invoiceno;
            $scope.calculateDueDate();
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                $scope.orderListModel.billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, 'yyyy-MM-dd');
            }
            createOrderParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, 'yyyy-MM-dd');
            if (typeof $scope.orderListModel.duedate == 'object')
            {
                createOrderParam.duedate = utilityService.parseDateToStr($scope.orderListModel.duedate, 'yyyy-MM-dd');
            } else
            {
                createOrderParam.duedate = utilityService.changeDateToSqlFormat($scope.orderListModel.duedate, 'yyyy-MM-dd');
            }
            if ($scope.orderListModel.billtype == 'sales_order')
            {
                createOrderParam.ref_id = $stateParams.id;
                createOrderParam.ref_name = 'sales_order';
            } else if ($scope.orderListModel.billtype == 'spare_request')
            {
                createOrderParam.ref_id = $stateParams.id;
                createOrderParam.ref_name = 'spare_request';
            } else if ($scope.orderListModel.billtype == 'service_request')
            {
                createOrderParam.ref_id = $stateParams.service_id;
                createOrderParam.ref_name = 'service_request';
            } else if ($scope.orderListModel.billtype == 'amc')
            {
                createOrderParam.ref_id = $stateParams.amc_id;
                createOrderParam.ref_name = 'amc';
            } else
            {
                createOrderParam.ref_id = '';
                createOrderParam.ref_name = 'sales_invoice';
            }
            createOrderParam.datepaid = '';
            createOrderParam.customer_address = $scope.orderListModel.customerInfo.shopping_address;
            createOrderParam.subtotal = (parseFloat($scope.orderListModel.subtotal)).toFixed(2);
            if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
            {
                createOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmountDetail.tax_amount)).toFixed(2);
            } else
            {
                createOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
            }
            createOrderParam.discount_amount = (parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            createOrderParam.discount_percentage = $scope.orderListModel.discountPercentage;
            createOrderParam.discount_mode = $scope.orderListModel.discountMode;
            createOrderParam.tax_id = $scope.orderListModel.tax_id;
            createOrderParam.total_amount = $scope.orderListModel.round;
            createOrderParam.round_off = (parseFloat($scope.orderListModel.roundoff)).toFixed(2);
            createOrderParam.paymentmethod = "",
                    createOrderParam.notes = $scope.orderListModel.notes;
            createOrderParam.item = [];
            //createOrderParam.item.date = utilityService.parseDateToStr($scope.orderListModel.billdate, "yyyy-MM-dd");
            createOrderParam.item.outletId = 1;
            createOrderParam.item.status = 'initiated';
            createOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            createOrderParam.payee = 0;
            if ($scope.orderListModel.payeeInfo != null && $scope.orderListModel.payeeInfo.id != undefined && $scope.orderListModel.payeeInfo != '')
            {
                createOrderParam.payee = $scope.orderListModel.payeeInfo.id;
            } else
            {
                createOrderParam.payee = $scope.orderListModel.customerInfo.id;
            }
            createOrderParam.consignee = 0;
            if ($scope.orderListModel.consigneeInfo != null && $scope.orderListModel.consigneeInfo.id != undefined && $scope.orderListModel.consigneeInfo != '')
            {
                createOrderParam.consignee = $scope.orderListModel.consigneeInfo.id;
            } else
            {
                createOrderParam.consignee = $scope.orderListModel.customerInfo.id;
            }
            if ($scope.orderListModel.companyInfo != null && $scope.orderListModel.companyInfo.id != undefined && $scope.orderListModel.companyInfo != '')
            {
                createOrderParam.company_id = $scope.orderListModel.companyInfo.id;
            } else
            {
                createOrderParam.company_id = '';
            }
            createOrderParam.insurance_charge = parseFloat($scope.orderListModel.insuranceAmt).toFixed(2);
            createOrderParam.packing_charge = parseFloat($scope.orderListModel.packagingAmt).toFixed(2);
            createOrderParam.insurance_percentage = parseFloat($scope.orderListModel.insuranceCharge) / 100;
            createOrderParam.packing_percentage = parseFloat($scope.orderListModel.packagingCharge) / 100;

            createOrderParam.item.orderDetails = [];
            if ($scope.orderListModel.list.length < 1 || createOrderParam.total_amount <= 0)
            {
                return;
            }
            var length = 0;
            if ($scope.orderListModel.list.length == 1)
            {
                length = 2;
            } else
            {
                length = $scope.orderListModel.list.length;
            }
            for (var i = 0; i < length - 1; i++)
            {
                var ordereditems = {};
                ordereditems.product_id = $scope.orderListModel.list[i].id;
                ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                if ($scope.orderListModel.list[i].productname.name != undefined && $scope.orderListModel.list[i].productname.name != null && $scope.orderListModel.list[i].productname.name != '')
                {
                    ordereditems.product_name = $scope.orderListModel.list[i].productname.name;
                } else
                {
                    ordereditems.product_name = $scope.orderListModel.list[i].productname;
                }

                ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].sales_price);
                ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty);
                if (typeof $scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo != null && $scope.orderListModel.list[i].uomInfo != '') {
                    if ($scope.orderListModel.list[i].uomInfo.name != null && $scope.orderListModel.list[i].uomInfo.name != '')
                    {
                        ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                    }
                    //                            else
                    //                            {
                    //                                ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo;
                    //                            }
                }
                if ($scope.orderListModel.list[i].serialno != 'undefined' && $scope.orderListModel.list[i].serialno != '')
                {
                    ordereditems.serial_no = $scope.orderListModel.list[i].serialno;
                } else
                {
                    ordereditems.serial_no = '';
                }
                ordereditems.uom_id = $scope.orderListModel.list[i].uomid;
                if ($scope.orderListModel.list[i].hsn_code != 'undefined' && $scope.orderListModel.list[i].hsn_code != null)
                {
                    ordereditems.hsn_code = $scope.orderListModel.list[i].hsn_code;
                } else
                    ordereditems.hsn_code = '';
                ordereditems.custom_opt1 = '';
                ordereditems.custom_opt2 = '';
                ordereditems.custom_opt3 = '';
                ordereditems.custom_opt4 = '';
                ordereditems.custom_opt5 = '';
                if ($scope.orderListModel.list[i].custom_opt1 != '' && $scope.orderListModel.list[i].custom_opt1 != null)
                {
                    ordereditems.custom_opt1 = $scope.orderListModel.list[i].custom_opt1;
                }
                if ($scope.orderListModel.list[i].custom_opt2 != '' && $scope.orderListModel.list[i].custom_opt2 != null)
                {
                    ordereditems.custom_opt2 = $scope.orderListModel.list[i].custom_opt2;
                }
                if ($scope.orderListModel.list[i].custom_opt3 != '' && $scope.orderListModel.list[i].custom_opt3 != null)
                {
                    ordereditems.custom_opt3 = $scope.orderListModel.list[i].custom_opt3;
                }
                if ($scope.orderListModel.list[i].custom_opt4 != '' && $scope.orderListModel.list[i].custom_opt4 != null)
                {
                    ordereditems.custom_opt4 = $scope.orderListModel.list[i].custom_opt4;
                }
                if ($scope.orderListModel.list[i].custom_opt5 != '' && $scope.orderListModel.list[i].custom_opt5 != null)
                {
                    ordereditems.custom_opt5 = $scope.orderListModel.list[i].custom_opt5;
                }
                ordereditems.total_price = $scope.orderListModel.list[i].sales_price * ordereditems.qty;
                createOrderParam.item.push(ordereditems);
            }
            createOrderParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var customattributeItem = {};
                if ($scope.customAttributeList[i].value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].value;
                } else if ($scope.customAttributeList[i].default_value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].default_value;
                } else
                {
                    customattributeItem.value = "";
                }
                customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                customattributeItem.sno = $scope.customAttributeList[i].id;
                createOrderParam.customattribute.push(customattributeItem);
            }
            createOrderParam.deal_id = $scope.orderListModel.dealId;
            createOrderParam.deal_name = $scope.orderListModel.dealName;
            adminService.addInvoice(createOrderParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    //                    if (typeof $stateParams.deal_id != 'undefined' && $stateParams.deal_id != null && $stateParams.deal_id != '')
                    //                    {
                    //                        $scope.createNotes();
                    //                    }
                    if ($scope.orderListModel.dealId != '' && $scope.orderListModel.dealId != 'undefined' && $scope.orderListModel.dealId != null)
                    {
                        $state.go('app.dealedit', {
                            'id': $stateParams.deal_id
                        });
                        $scope.isDataSavingProcess = false;
                    } else
                    {
                        $scope.formReset();
                        if ($rootScope.appConfig.invoice_print_template === 't1')
                        {
                            $state.go('app.invoiceView1', {
                                'id': response.data.id
                            }, {
                                'reload': true
                            });
                        } else if ($rootScope.appConfig.invoice_print_template === 't2')
                        {
                            $state.go('app.invoiceView2', {
                                'id': response.data.id
                            }, {
                                'reload': true
                            });
                        } else
                        {
                            $state.go('app.invoiceView1', {
                                'id': response.data.id
                            }, {
                                'reload': true
                            });
                        }
                    }
                }
                $scope.isDataSavingProcess = false;


            });
        }
        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "invoice";
            getListParam.mode = "1";

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.customAttributeList = data;

            });
        };

        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });
        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.getCustomAttributeList();

        /*
         * Listening the Custom field value change
         */
        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });
        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }
        $scope.checkId();

        $scope.isPrefixListLoaded = false;
        $scope.getPrefixList = function ()
        {
            var getListParam = {};
            getListParam.setting = 'sales_invoice_prefix';
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            $scope.orderListModel.prefixList.push(perfix[i]);
                        }
                    }

                }
                $scope.isPrefixListLoaded = true;
            });

        };

        $scope.isNextAutoGeneratorNumberLoaded = false;
        $scope.getNextInvoiceno = function ()
        {
            $scope.isNextAutoGeneratorNumberLoaded = true;
            var getListParam = {};
            getListParam.type = "sales_invoice";
            getListParam.prefix = $scope.orderListModel.invoicePrefix;

            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            configOption.perfix = getListParam.prefix;
            adminService.getNextInvoiceNo(getListParam, configOption, headers).then(function (response)
            {
                var config = response.config.config;
                if (response.data.success == true && typeof config.perfix != 'undefined' && config.perfix == $scope.orderListModel.invoicePrefix)
                {
                    if (!$scope.orderListModel.is_next_autogenerator_num_changed)
                    {
                        $scope.orderListModel.next_autogenerator_num = response.data.no;
                    }
                }
                $scope.isNextAutoGeneratorNumberLoaded = false;

            });

        };

        $scope.invoiceNumberChange = function ()
        {

            $scope.orderListModel.is_next_autogenerator_num_changed = true;
            $timeout(function () {
                $scope.orderListModel.invoiceno = $scope.orderListModel.next_autogenerator_num;
            }, 0);
        }

        $scope.getPrefixList();
        $scope.getNextInvoiceno();
    }]);




