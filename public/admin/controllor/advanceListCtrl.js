app.controller('advanceListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', 'utilityService', '$httpService', 'APP_CONST', function ($scope, $rootScope, adminService, $filter, Auth, $timeout, utilityService, $httpService, APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.advanceModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            isLoadingProgress: true
        };
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.pagePerCount = [50, 100];
        $scope.advanceModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            emp_code: '',
            date: '',
            employeeInfo: {}
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectdAdvanceId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function (id)
        {
            $scope.selectdAdvanceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.selectdAdvanceId = '';
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.currentDate = new Date();

        $scope.advanceDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.advanceDateOpen = true;
            }
        }

        $scope.$watch('searchFilter.employeeInfo', function(newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal === '')
            {
                {
                    $scope.initTableFilter();
                }
            }
        });
        
        $scope.deleteAdvanceInfo = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getAdvanceParam = {};
                getAdvanceParam.id = $scope.selectdAdvanceId;
                var headers = {};
                headers['screen-code'] = 'advance';
                adminService.deleteAdvance(getAdvanceParam, getAdvanceParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                emp_code: '',
                employeeInfo: {},
                date: ''
            };
            $scope.initTableFilter();
        }

        $scope.getList = function () {

            $scope.advanceModel.isLoadingProgress = true;
            var getAdvanceParam = {};
            var headers = {};
            headers['screen-code'] = 'advance';
             if ($scope.searchFilter.employeeInfo != null && typeof $scope.searchFilter.employeeInfo != 'undefined' && typeof $scope.searchFilter.employeeInfo.id != 'undefined')
            {
                getAdvanceParam.emp_id = $scope.searchFilter.employeeInfo.id;
            }
            else
            {
                getAdvanceParam.emp_id = '';
            }
            getAdvanceParam.emp_code = $scope.searchFilter.emp_code;
            getAdvanceParam.date = '';
            if ($scope.searchFilter.date != null && typeof $scope.searchFilter.date == 'object')
            {
                getAdvanceParam.date = utilityService.parseDateToStr($scope.searchFilter.date, $scope.adminService.appConfig.date_format);
            }
            getAdvanceParam.date = utilityService.changeDateToSqlFormat(getAdvanceParam.date, $scope.adminService.appConfig.date_format);
            getAdvanceParam.is_active = 1;
            getAdvanceParam.start = ($scope.advanceModel.currentPage - 1) * $scope.advanceModel.limit;
            getAdvanceParam.limit = $scope.advanceModel.limit;

            adminService.getAdvanceList(getAdvanceParam, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.advanceModel.list = data.list;
                    for (var i = 0; i < $scope.advanceModel.list.length; i++)
                    {
                        var datevalue = moment($scope.advanceModel.list[i].date).valueOf();
                        if (datevalue > 0)
                        {
                            var nextdate = $scope.advanceModel.list[i].date;
                            var newnextdate = utilityService.convertToUTCDate(nextdate);
                            $scope.advanceModel.list[i].date = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                        } else
                            $scope.advanceModel.list[i].date = '';
                    }
                    $scope.advanceModel.total = data.total;
                }
                $scope.advanceModel.isLoadingProgress = false;
            });

        };
        
        $scope.getEmployeList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            //autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.LEAD_TYPE_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatEmployeModel = function(model)
        {
            if (model != null)
            {
                if (model.f_name != undefined && model.team_name != undefined && model.emp_code != undefined)
                {
                    return model.f_name + '(' + model.team_name + ',' + model.emp_code + ')';
                }
                else if (model.f_name != undefined && model.team_name != undefined && model.company  != undefined)
                {
                    return model.f_name + '(' + model.team_name + ')';
                }
            }
            return  '';
        };

        $scope.getList();

    }]);




