

app.controller('employeeEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.employeeEditModel = {
            "fname": "",
            "lname": "",
            "gender": "",
            "dob": '',
            "doj": '',
            "salarytype": '',
            "salary": '',
            "teamid": '',
            "email": "",
            "mobile": "",
            "city_id": "",
            "state_id": "",
            "country_id": "",
            "employeetype": "",
            "address": "",
            "country_list": [],
            "state_list": [],
            "city_list": [],
            "teamList": [],
            "isUser": false,
            "isactive": true,
            "empcode": '',
            "country_name": '',
            "state_name": '',
            currentPage: 1,
            total: 0,
            allowanceList: [],
            deletedAllowanceList: [],
            "maritalstatus": false,
            "employee": false,
            "employeer": false,
            "pfpercentage": '',
            "pfamount": '',
            "employeerpf": '',
            "employeeramount": '',
            "employeeresi": '',
            "employeeresiamount": '',
            "esipercentage": '',
            "esiamount": '',
            "relationship": '',
            "nomineename": '',
            "leavecount": '',
            "payrolltype": '',
            isLoadingProgress: false,
            "bonus_percentage": '',
            "img": []
        };
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.employee_edit_form != 'undefined' && typeof $scope.employee_edit_form.$pristine != 'undefined' && !$scope.employee_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $scope.currentDate = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.startDateOpen = false;
        $scope.DOJDateOpen = false;
        $scope.RelevingDateOpen = false;
        $scope.openDate = function (index) {

            if (index == 1)
            {
                $scope.startDateOpen = true;
            }
            if (index == 2)
            {
                $scope.DOJDateOpen = true;
            }
            if (index == 3)
            {
                $scope.RelevingDateOpen = true;
            }

        }
        $scope.leave = false;
        $scope.attendance = function ()
        {
            if ($scope.employeeEditModel.salarytype == 'attendance')
            {
                $scope.leave = true;
            } else
            {
                $scope.leave = false;
            }
        }
        $scope.dateOptions = {
            maxDate: new Date(),
            showWeeks: false
        };
        $scope.isDataSavingProcess = false;
        $scope.isLoadedTeamList = false;
        $scope.formReset = function () {
            if (typeof $scope.employee_edit_form != 'undefined')
            {
                $scope.employee_edit_form.$setPristine();
                $scope.updateEmployeeInfo();
            }

        }
        $scope.dateOptions = {
            maxDate: new Date(), // set this to whatever date you want to set
        }
        $scope.currentDate = new Date();
        $scope.showUploadFilePopup = false;
        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            } else if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }


        }

        $scope.closePopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;

            } else if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }

        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.isImageUploadComplete = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.validateFileUploadStatus();
        }

        $scope.validateFileUploadStatus = function ()
        {
            $scope.isImageSavingProcess = true;
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = false;
                $scope.isImageUploadComplete = true;
            }

        }
        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.employeeEditModel.img = $scope.uploadedFileQueue;
                $scope.closePopup('fileupload');
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedTeamList)
            {
                $scope.updateEmployeeInfo();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateEmployeeInfo = function ()
        {
            $scope.employeeEditModel.fname = $scope.employeeEditModel.list.f_name;
            $scope.employeeEditModel.lname = $scope.employeeEditModel.list.l_name;
            $scope.employeeEditModel.mobile = $scope.employeeEditModel.list.ph_no;
            $scope.employeeEditModel.email = $scope.employeeEditModel.list.email;
            $scope.employeeEditModel.teamid = $scope.employeeEditModel.list.team_id + '';

            var isAvailable = false;
            for (var i = 0; i < $scope.employeeEditModel.teamList.length; i++)
            {
                if ($scope.employeeEditModel.list.team_id == $scope.employeeEditModel.teamList[i].id)
                {
                    isAvailable = true;
                }
            }
            if (!isAvailable)
            {
                var newRow = {
                    "id": $scope.employeeEditModel.list.team_id + '',
                    "name": $scope.employeeEditModel.list.team_name
                };
                $scope.employeeEditModel.teamList.push(newRow);
            }
            //            if ($scope.employeeEditModel.list.country_id != 0)
            //            {
            $scope.employeeEditModel.country_id = $scope.employeeEditModel.list.country;
            //            } else
            //            {
            //                $scope.employeeEditModel.country_id = '';
            //            }
            //            if ($scope.employeeEditModel.list.city_id != 0)
            //            {
            $scope.employeeEditModel.city_id = $scope.employeeEditModel.list.city;
            //            } else
            //            {
            //                $scope.employeeEditModel.city_id = '';
            //            }
            //            if ($scope.employeeEditModel.list.state_id != 0)
            //            {
            $scope.employeeEditModel.state_id = $scope.employeeEditModel.list.state;
            //            } else
            //            {
            //                $scope.employeeEditModel.state_id = '';
            //            }
            $scope.employeeEditModel.address = $scope.employeeEditModel.list.address;
            var date = moment(new Date($scope.employeeEditModel.list.dob)).valueOf();
            if (!isNaN(date) && date != undefined && date != null)
            {
                $scope.employeeEditModel.dob = utilityService.parseStrToDate($scope.employeeEditModel.list.dob);
            } else
            {
                $scope.employeeEditModel.dob = '';
            }

            $scope.employeeEditModel.gender = $scope.employeeEditModel.list.gender;
            var date1 = moment(new Date($scope.employeeEditModel.list.doj)).valueOf();
            if (!isNaN(date1) && date1 != undefined && date1 != null && date1 > 0)
            {
                $scope.employeeEditModel.doj = utilityService.parseStrToDate($scope.employeeEditModel.list.doj);
            } else
            {
                $scope.employeeEditModel.doj = '';
            }
            $scope.employeeEditModel.isActive = 1;
            if ($scope.employeeEditModel.list.is_user == '')
            {
                $scope.employeeEditModel.list.is_user = 0;
            }
            $scope.employeeEditModel.list.is_user = parseInt($scope.employeeEditModel.list.is_user, 10);
            $scope.employeeEditModel.isUser = $scope.employeeEditModel.list.is_user == 1 ? true : false;
            $scope.employeeEditModel.gender = $scope.employeeEditModel.list.gender;
            $scope.employeeEditModel.salarytype = $scope.employeeEditModel.list.salary_type;
            if ($scope.employeeEditModel.salarytype == 'attendance')
            {
                $scope.leave = true;
            } else
            {
                $scope.leave = false;
            }
            var date2 = moment($scope.employeeEditModel.list.reliving_date).valueOf();
            if (!isNaN(date2) && date2 != undefined && date2 != null && date2 > 0)
            {
                $scope.employeeEditModel.releving = utilityService.parseStrToDate($scope.employeeEditModel.list.reliving_date);
            } else
            {
                $scope.employeeEditModel.releving = '';
            }
            $scope.employeeEditModel.employeetype = $scope.employeeEditModel.list.type_of_emp;
            $scope.employeeEditModel.empcode = $scope.employeeEditModel.list.emp_code;
            $scope.employeeEditModel.salary = $scope.employeeEditModel.list.salary_amount;
            $scope.employeeEditModel.reason = $scope.employeeEditModel.list.reliving_reason;
            $scope.employeeEditModel.isactive = $scope.employeeEditModel.list.is_active == 1 ? true : false;
            $scope.employeeEditModel.maritalstatus = $scope.employeeEditModel.list.marital_status == 'married' ? true : false;
            $scope.employeeEditModel.empcode = $scope.employeeEditModel.list.emp_code;
            $scope.employeeEditModel.relationship = $scope.employeeEditModel.list.relationship;
            $scope.employeeEditModel.nomineename = $scope.employeeEditModel.list.nominee_name;
            $scope.employeeEditModel.pfpercentage = $scope.employeeEditModel.list.pf_percentage;
            // $scope.employeeEditModel.pfamount = $scope.employeeEditModel.list.Pf_amount;
            //$scope.employeeEditModel.employeeramount = $scope.employeeEditModel.list.employer_pf_amount;
            $scope.employeeEditModel.employeerpf = $scope.employeeEditModel.list.employer_pf_percentage;
            $scope.employeeEditModel.esipercentage = $scope.employeeEditModel.list.esi_percentage;
            // $scope.employeeEditModel.esiamount = $scope.employeeEditModel.list.esi_amount;
            $scope.employeeEditModel.employeeresi = $scope.employeeEditModel.list.employer_esi_percentage;
            // $scope.employeeEditModel.employeeresiamount = $scope.employeeEditModel.list.employer_esi_amount;
            $scope.employeeEditModel.leavecount = $scope.employeeEditModel.list.max_leave_count;
            $scope.employeeEditModel.bonus_percentage = parseFloat($scope.employeeEditModel.list.bonus_percentage);
            $scope.employeeEditModel.payrolltype = $scope.employeeEditModel.list.payroll_type;
            if ($scope.employeeEditModel.list.img != '' && $scope.employeeEditModel.list.img != null)
            {
                var fileItem = {}
                fileItem.urlpath = $scope.employeeEditModel.list.img;
                $scope.employeeEditModel.img.push(fileItem);
                console.log('imgtest');
                console.log($scope.employeeEditModel.img);
            } else
            {
                $scope.employeeEditModel.img = [];

            }
        }

        $scope.modifyEmployee = function () {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var modifyEmployeeParam = {};
                var headers = {};
                headers['screen-code'] = 'employee';
                modifyEmployeeParam.team_id = $scope.employeeEditModel.teamid;
                modifyEmployeeParam.f_name = $scope.employeeEditModel.fname;
                modifyEmployeeParam.l_name = $scope.employeeEditModel.lname;
                modifyEmployeeParam.ph_no = $scope.employeeEditModel.mobile;
                modifyEmployeeParam.email = $scope.employeeEditModel.email;
                modifyEmployeeParam.city_id = $scope.employeeEditModel.city_id;
                modifyEmployeeParam.country_id = $scope.employeeEditModel.country_id;
                modifyEmployeeParam.state_id = $scope.employeeEditModel.state_id;
                modifyEmployeeParam.bonus_percentage = $scope.employeeEditModel.bonus_percentage;
                for (var i = 0; i < $scope.employeeEditModel.country_list.length; i++)
                {
                    if ($scope.employeeEditModel.country_id == $scope.employeeEditModel.country_list[i].id)
                    {
                        modifyEmployeeParam.country = $scope.employeeEditModel.country_list[i].name;
                    }
                }
                for (var i = 0; i < $scope.employeeEditModel.state_list.length; i++)
                {
                    if ($scope.employeeEditModel.state_id == $scope.employeeEditModel.state_list[i].id)
                    {
                        modifyEmployeeParam.state = $scope.employeeEditModel.state_list[i].name;
                    }
                }
                for (var i = 0; i < $scope.employeeEditModel.city_list.length; i++)
                {
                    if ($scope.employeeEditModel.city_id == $scope.employeeEditModel.city_list[i].id)
                    {
                        modifyEmployeeParam.city = $scope.employeeEditModel.city_list[i].name;
                    }
                }
                if ($scope.employeeEditModel.releving != null && $scope.employeeEditModel.releving != '')
                {
                    modifyEmployeeParam.reliving_date = utilityService.parseDateToStr($scope.employeeEditModel.releving, 'yyyy-MM-dd');
                } else
                {
                    modifyEmployeeParam.reliving_date = '';
                }
                modifyEmployeeParam.reliving_reason = $scope.employeeEditModel.reason;
                modifyEmployeeParam.is_active = $scope.employeeEditModel.isactive == true ? 1 : 0;
                modifyEmployeeParam.marital_status = $scope.employeeEditModel.maritalstatus == true ? 'married' : 'unmarried';
                modifyEmployeeParam.emp_code = $scope.employeeEditModel.empcode;
                modifyEmployeeParam.relationship = $scope.employeeEditModel.relationship;
                modifyEmployeeParam.nominee_name = $scope.employeeEditModel.nomineename;
                modifyEmployeeParam.pf_percentage = $scope.employeeEditModel.pfpercentage;
                //   modifyEmployeeParam.Pf_amount = $scope.employeeEditModel.pfamount;
                //   modifyEmployeeParam.employer_pf_amount = $scope.employeeEditModel.employeeramount;
                modifyEmployeeParam.employer_pf_percentage = $scope.employeeEditModel.employeerpf;
                modifyEmployeeParam.esi_percentage = $scope.employeeEditModel.esipercentage;
                //    modifyEmployeeParam.esi_amount = $scope.employeeEditModel.esiamount;
                modifyEmployeeParam.employer_esi_percentage = $scope.employeeEditModel.employeeresi;
                //    modifyEmployeeParam.employer_esi_amount = $scope.employeeEditModel.employeeresiamount;
                modifyEmployeeParam.max_leave_count = $scope.employeeEditModel.leavecount;
                modifyEmployeeParam.dob = utilityService.parseDateToStr($scope.employeeEditModel.dob, 'yyyy-MM-dd');
                modifyEmployeeParam.doj = utilityService.parseDateToStr($scope.employeeEditModel.doj, 'yyyy-MM-dd');
                modifyEmployeeParam.gender = $scope.employeeEditModel.gender;
                modifyEmployeeParam.salary_type = $scope.employeeEditModel.salarytype;
                modifyEmployeeParam.salary_amount = $scope.employeeEditModel.salary;
                modifyEmployeeParam.type_of_emp = $scope.employeeEditModel.employeetype;
                modifyEmployeeParam.address = $scope.employeeEditModel.address;
                modifyEmployeeParam.emp_code = $scope.employeeEditModel.empcode;
                modifyEmployeeParam.payroll_type = $scope.employeeEditModel.payrolltype;
                modifyEmployeeParam.is_user = $scope.employeeEditModel.isUser == true ? 1 : 0;
                if ($scope.employeeEditModel.img.length > 0)
                {
                    modifyEmployeeParam.img = $scope.employeeEditModel.img[0].urlpath;
                } else
                {
                    modifyEmployeeParam.img = '';
                }
                adminService.editEmployee(modifyEmployeeParam, $stateParams.id, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.createEmployeeAllowance();
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };
        $scope.getcountryList = function () {

            $scope.employeeEditModel.isLoadingProgress = true;
            var countryListParam = {};
            var headers = {};
            headers['screen-code'] = 'country';

            countryListParam.id = '';

            countryListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeEditModel.country_list = data.list;
                }
                $scope.employeeEditModel.isLoadingProgress = false;
            });

        };
        $scope.getcountryList();

        $scope.getstateList = function () {

            $scope.employeeEditModel.isLoadingProgress = true;
            var stateListParam = {};
            var headers = {};
            headers['screen-code'] = 'state';

            stateListParam.id = '';

            stateListParam.is_active = 1;
            stateListParam.country_id = $scope.employeeEditModel.country_id;
            stateListParam.country_name = $scope.employeeEditModel.country_name;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeEditModel.state_list = data.list;
                }
                $scope.employeeEditModel.isLoadingProgress = false;
            });

        };

        $scope.getcityList = function () {

            $scope.employeeEditModel.isLoadingProgress = true;
            var cityListParam = {};
            var headers = {};
            headers['screen-code'] = 'city';

            cityListParam.id = '';

            cityListParam.is_active = 1;
            cityListParam.country_id = $scope.employeeEditModel.country_id;
            cityListParam.country_name = $scope.employeeEditModel.country_name;
            cityListParam.state_id = $scope.employeeEditModel.state_id;
            cityListParam.state_name = $scope.employeeEditModel.state_name;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCityList(cityListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeEditModel.city_list = data.list;
                }
                $scope.employeeEditModel.isLoadingProgress = false;
            });

        };

        $scope.CountryChange = function ()
        {
            $scope.employeeEditModel.state_id = '';
            $scope.employeeEditModel.city_id = '';
            $scope.getstateList();

        }
        $scope.StateChange = function ()
        {
            $scope.employeeEditModel.city_id = '';
            $scope.getcityList();

        }
        $scope.getEmployeeTeam = function ()
        {
            var getListParam = {};
            $scope.isLoadedTeamList = false;
            getListParam.type = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeTeamList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.employeeEditModel.teamList = data;
                    $scope.isLoadedTeamList = true;
                }
            });
        };

        $scope.getAllowanceList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.ALLOWANCE_MASTER_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.addNewRow = function ()
        {
            var allowance = {};
            allowance.id = 0;
            allowance.emp_id = '';
            allowance.emp_code = '';
            allowance.emp_allow_mas_id = '';
            allowance.date = '';
            allowance.is_active = '';
            allowance.type = '';
            allowance.amount = '';
            allowance.isNew = true;
            $scope.employeeEditModel.allowanceList.push(allowance);
        }

        $scope.deleteAllowance = function (index)
        {
            $scope.employeeEditModel.deletedAllowanceList.push($scope.employeeEditModel.allowanceList[index]);
            $scope.employeeEditModel.allowanceList.splice(index, 1);
        }

        $scope.getEmployeeAllowanceList = function () {
            var getListParam = {};
            getListParam.emp_id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeAllowanceList(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.employeeEditModel.allowanceList = data.list;
                for (var i = 0; i < $scope.employeeEditModel.allowanceList.length; i++)
                {
                    $scope.employeeEditModel.allowanceList[i].isNew = false;
                    $scope.employeeEditModel.allowanceList[i].allowanceInfo = {
                        id: $scope.employeeEditModel.allowanceList[i].id,
                        name: $scope.employeeEditModel.allowanceList[i].name,
                        amount: $scope.employeeEditModel.allowanceList[i].amount,
                        emp_allow_mas_id: $scope.employeeEditModel.allowanceList[i].emp_allow_mas_id
                    };
                }
            });
        };

        $scope.setEmployeeAllowanceDetail = function (model, index)
        {
            model.emp_allow_mas_id = model.id;
            model.id = 0;
            $scope.updateAllowanceInfo(model, index);
        };

        $scope.createEmployeeAllowance = function () {

            var updateAllowanceParam = {};
            updateAllowanceParam.emp_id = $stateParams.id;
            updateAllowanceParam.allowance = [];
            var headers = {};
            headers['screen-code'] = 'employee';
            for (var i = 0; i < $scope.employeeEditModel.allowanceList.length; i++)
            {
                var allow = {};
                //allow.id = $scope.employeeEditModel.allowanceList[i].id;
                allow.emp_id = $stateParams.id;
                allow.emp_code = $scope.employeeEditModel.empcode;
                allow.emp_allow_mas_id = $scope.employeeEditModel.allowanceList[i].emp_allow_mas_id;
                allow.date = '';
                allow.type = '';
                allow.amount = $scope.employeeEditModel.allowanceList[i].amount;
                allow.is_active = 1;
                updateAllowanceParam.allowance.push(allow);
            }
            if (updateAllowanceParam.allowance.length > 0)
            {
                adminService.addEmployeeAllowance(updateAllowanceParam, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.deleteEmployeeAllowance();
                    }
                });
            } else
            {
                $scope.deleteEmployeeAllowance();
            }
        };

        $scope.deleteEmployeeAllowance = function () {

            var deleteAllowanceParam = [];
            var headers = {};
            headers['screen-code'] = 'employee';
            for (var i = 0; i < $scope.employeeEditModel.deletedAllowanceList.length; i++)
            {
                if ($scope.employeeEditModel.deletedAllowanceList[i].isNew == false)
                {
                    var allowance = {};
                    allowance.id = $scope.employeeEditModel.deletedAllowanceList[i].id;
                    deleteAllowanceParam.push(allowance);
                }
            }
            if (deleteAllowanceParam.length > 0)
            {
                adminService.deleteEmployeeAllowance(deleteAllowanceParam, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $scope.isDataSavingProcess = false;
                        $state.go('app.employee');
                    }
                });
            } else
            {
                $scope.formReset();
                $scope.isDataSavingProcess = false;
                $state.go('app.employee');
            }
        };

        $scope.formatAllowanceModel = function (model, index)
        {
            if (model != null)
            {
                if (model.name != null && model.name != "")
                {

                    return model.name;
                }
            }
            return  '';
        };

        $scope.updateAllowanceInfo = function (model, index)
        {
            if ($scope.employeeEditModel.allowanceList.length != 0)
            {
                if (typeof model == 'undefined')
                    return;
                $scope.employeeEditModel.allowanceList[index].id = model.id;
                $scope.employeeEditModel.allowanceList[index].name = model.name;
                $scope.employeeEditModel.allowanceList[index].amount = model.amount;
                $scope.employeeEditModel.allowanceList[index].emp_allow_mas_id = model.emp_allow_mas_id;
                if (model.isNew == undefined || model.isNew == '')
                    $scope.employeeEditModel.allowanceList[index].isNew = true;

            }
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'employee';
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeList(getListParam, configOption, headers).then(function (response) {
                var data = response.data;
                if (response.data.success === true)
                {
                    $scope.employeeEditModel.list = data.list[0];
                    $scope.initUpdateDetail();
                }
            });

        };

        $scope.getList();
        $scope.getEmployeeAllowanceList();
        $scope.getEmployeeTeam();
    }]);






