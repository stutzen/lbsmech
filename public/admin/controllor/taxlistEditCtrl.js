

app.controller('taxlistEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.taxlistModel = {
            "id": "",
            "tax_no": "",
            "tax_name": "",
            "tax_percentage": "",
            "tax_applicable_amt": "",
            "start_date": "",
            "end_date": "",
            "comments": "",
            isLoadingProgress: false,
            "tax_type": '',
            "isdisplay":true

        };
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.taxlist_edit_form != 'undefined' && typeof $scope.taxlist_edit_form.$pristine != 'undefined' && !$scope.taxlist_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $scope.isDataSavingProcess = false;
        
        $scope.startDateOpen = false;
        $scope.endDateOpen = false;
        $scope.openDate = function (index)
        {            
            if (index == 1)
            {
                $scope.startDateOpen = true;
            }
            if (index == 2)
            {
                $scope.endDateOpen = true;
            }
        }
        
        $scope.formReset = function() {
            if (typeof $scope.taxlist_edit_form != 'undefined')
            {
                $scope.taxlist_edit_form.$setPristine();
                $scope.updateTaxlistInfo();
            }

        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedRoleList)
            {
                $scope.updateTaxlistInfo();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.updateTaxlistInfo = function()
        {
            $scope.taxlistModel.id = $scope.taxlistModel.list.id;
            $scope.taxlistModel.tax_no = $scope.taxlistModel.list.tax_no;
            $scope.taxlistModel.tax_name = $scope.taxlistModel.list.tax_name;
            $scope.taxlistModel.tax_percentage = $scope.taxlistModel.list.tax_percentage;
            $scope.taxlistModel.tax_applicable_amt = $scope.taxlistModel.list.tax_applicable_amt;
            
            $scope.taxlistModel.start_date = null;
            $scope.taxlistModel.end_date = null;
            if($scope.taxlistModel.list.start_date != null)
            {
                $scope.taxlistModel.start_date = new Date($scope.taxlistModel.list.start_date);
            }
            if($scope.taxlistModel.list.end_date != null)
            {
                $scope.taxlistModel.end_date = new Date($scope.taxlistModel.list.end_date);
            }
            
            
            $scope.taxlistModel.tax_type = $scope.taxlistModel.list.tax_type;
            $scope.taxlistModel.comments = $scope.taxlistModel.list.comments;
            $scope.taxlistModel.isdisplay= $scope.taxlistModel.list.is_display == 1 ? true : false;
            $scope.taxlistModel.isActive = 1;
            $scope.taxlistModel.isLoadingProgress = false;
        }

        // $scope.isLoadedRoleList = false;
        $scope.editTax = function() {

            $scope.isDataSavingProcess = true;
            var modifyTaxlistParam = {};
            var headers = {};
            headers['screen-code'] = 'tax';
            modifyTaxlistParam.id = '';
            modifyTaxlistParam.tax_no = $scope.taxlistModel.tax_no;
            modifyTaxlistParam.tax_name = $scope.taxlistModel.tax_name;
            modifyTaxlistParam.tax_percentage = $scope.taxlistModel.tax_percentage;
            modifyTaxlistParam.tax_applicable_amt = $scope.taxlistModel.tax_applicable_amt;
            modifyTaxlistParam.is_group = 0;
            modifyTaxlistParam.tax_type = $scope.taxlistModel.tax_type;
           
            modifyTaxlistParam.start_date = null;
            modifyTaxlistParam.end_date = null;
            if ($scope.taxlistModel.start_date != null && $scope.taxlistModel.start_date != "")
            {
                modifyTaxlistParam.start_date = utilityService.parseDateToStr($scope.taxlistModel.start_date, 'yyyy-MM-dd');
            }
            if ($scope.taxlistModel.end_date != null && $scope.taxlistModel.end_date != "")
            {
                modifyTaxlistParam.end_date = utilityService.parseDateToStr($scope.taxlistModel.end_date, 'yyyy-MM-dd');
            }
            modifyTaxlistParam.comments = $scope.taxlistModel.comments;
            modifyTaxlistParam.is_display= $scope.taxlistModel.isdisplay == true ? 1 : 0;
            modifyTaxlistParam.isactive = 1;

            adminService.editTax(modifyTaxlistParam, $scope.taxlistModel.id, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.taxlist');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getTaxlist = function() {
            $scope.taxlistModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_group = 0;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaxList(getListParam,configOption).then(function(response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.taxlistModel.list = data.list[0];
                }
                $scope.taxlistModel.total = data.total;
                $scope.updateTaxlistInfo();

            });
        };
        $scope.getTaxlist()
    }]);




