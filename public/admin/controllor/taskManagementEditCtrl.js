





app.controller('taskManagementEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', '$httpService', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, $httpService, APP_CONST, sweet) {

            $rootScope.getNavigationBlockMsg = null;
            $scope.taskManagementEditModel = {
                  currentPage: 1,
                  total: 0,
                  limit: 10,
                  taskList: [],
                  staffList: [],
                  typeList: [],
                  userList: [],
                  statusList: [],
                  priorityList: [],
                  isLoadingProgress: false,
                  title: '',
                  task: '',
                  taskId: '',
                  staffId: '',
                  typeId: '',
                  ref_id: '',
                  ref_name: '',
                  date: '',
                  status: '',
                  priority: '',
                  configType: '',
                  estimatedDate: '',
                  attachments: [],
                  comments: '',
                  commentsList: [],
                  createdFor: '',
                  followers: [],
                  employeeInfo: ''
            };
            $scope.adminService = adminService;
            $scope.taskManagementEditModel.isCreateTask = true;
            $scope.taskManagementEditModel.isStaffLoaded = false;
            $scope.taskManagementEditModel.isTypeLoaded = false;
            $scope.taskManagementEditModel.isUserLoaded = false;
            $scope.isCommentSavingProcess = false;
            $scope.pagePerCount = [10, 20, 50, 100];
            $scope.taskManagementEditModel.limit = $scope.pagePerCount[2];
            $scope.searchFilter = {
                  typeIds: [],
                  assignTo: [],
                  createdBy: [],
                  status: [],
                  priority: [],
                  //        followers: [],
                  employeeInfo: ''
            };
            $scope.taskDetail = '';
            $scope.searchFilterValue = "";
            $scope.clearFilter = function ()
            {
                  $scope.searchFilter = {
                  };
                  $scope.initTableFilter();
            }
            $scope.dateOptions = {
                  minDate: new Date()
            }
            $scope.initUpdateDetailTimeoutPromise = null;
            $scope.initUpdateDetail = function ()
            {

                  if ($scope.initUpdateDetailTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                  }

                  if ($scope.taskManagementEditModel.isTypeLoaded && $scope.taskManagementEditModel.isStaffLoaded && $scope.taskManagementEditModel.isUserLoaded)
                  {
                        $scope.updateTaskAtivityDetail();
                  } else
                  {
                        $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                  }
            }

            $scope.showAddPopup = function ()
            {
                  $scope.taskManagementEditModel.isCreateTask = true;
            };
            $scope.closeAddPopup = function ()
            {
                  $scope.taskManagementEditModel.isCreateTask = false;
                  $state.go('app.taskManagement');
            };
            $scope.startDateOpen = false;
            $scope.estiDateOpen = false;
            $scope.currentDate = new Date();
            $scope.dateFormat = 'dd-MM-yyyy';
            $scope.validationFactory = ValidationFactory;
            $scope.openDate = function (index) {

                  if (index == 0)
                  {
                        $scope.startDateOpen = true;
                  } else if (index == 1)
                  {
                        $scope.estiDateOpen = true;
                  }

            }

            $scope.formReset = function () {

                  if (typeof $scope.task_activity_edit_form != 'undefined')
                  {
                        $scope.taskManagementEditModel.employeeInfo = '';
                        $scope.task_activity_edit_form.$setPristine();
                        $scope.updateTaskAtivityDetail();
                  }
            };
            $scope.updateTaskAtivityDetail = function ()
            {
                  if ($scope.taskDetail != undefined && $scope.taskDetail != '' && $scope.taskDetail != null)
                  {
                        $scope.taskManagementEditModel.task = {
                              task_name: $scope.taskDetail.title,
                              id: $scope.taskDetail.task_id
                        };
                        $scope.taskManagementEditModel.id = $scope.taskDetail.id;
                        $scope.taskManagementEditModel.title = $scope.taskDetail.title;
                        $scope.taskManagementEditModel.taskId = $scope.taskDetail.task_id;
                        $scope.taskManagementEditModel.staffId = $scope.taskDetail.emp_id + '';
                        $scope.taskManagementEditModel.typeId = $scope.taskDetail.type_id + '';
                        $scope.taskManagementEditModel.ref_id = $scope.taskDetail.ref_id;
                        $scope.taskManagementEditModel.ref_name = $scope.taskDetail.ref_name;
                        var newdate = moment($scope.taskDetail.date).valueOf();
                        if (newdate > 0)
                        {
                              var idate = utilityService.parseStrToDate($scope.taskDetail.date);
                              $scope.taskManagementEditModel.date = idate;
                        } else
                        {
                              $scope.taskManagementEditModel.date = '';
                        }
                        var duedate = '';
                        var newestidate = moment($scope.taskDetail.estimated_date).valueOf();
                        if (newestidate > 0)
                        {
                              duedate = utilityService.parseStrToDate($scope.taskDetail.estimated_date);
                        }
                        $scope.taskManagementEditModel.estimatedDate = duedate;
                        $scope.taskManagementEditModel.createdFor = $scope.taskDetail.created_for + '';
                        $scope.taskManagementEditModel.status = $scope.taskDetail.status;
                        $scope.taskManagementEditModel.priority = $scope.taskDetail.priority;
                        $scope.taskManagementEditModel.configType = $scope.taskDetail.configType;
                        $scope.taskManagementEditModel.attachments = $scope.taskDetail.attachment;
                        if ($scope.taskManagementEditModel.attachments.length > 0)
                        {
                              for (var i = 0; i < $scope.taskManagementEditModel.attachments.length; i++)
                              {
                                    $scope.taskManagementEditModel.attachments[i].urlpath = $scope.taskManagementEditModel.attachments[i].url;
                                    $scope.taskManagementEditModel.attachments[i].path = $scope.taskManagementEditModel.attachments[i].url;
                              }
                        }
                        $scope.taskManagementEditModel.followers = [];
                        var followers = $scope.taskDetail.followers;
                        //follower.emp_id=$scope.taskManagementEditModel.followers[i].id;
                        for (var i = 0; i < followers.length; i++)
                        {
                              var followerParam = {};
                              followerParam.id = followers[i].emp_id;
                              followerParam.f_name = followers[i].emp_name;
                              $scope.taskManagementEditModel.followers.push(followerParam);
                        }
                        $scope.taskManagementEditModel.comments = $scope.taskDetail.comments;
                        $scope.taskManagementEditModel.isLoadingProgress = false;
                        // $scope.formatTaskModel($scope.taskManagementEditModel.task);
                  }
            }
            $scope.getTaskInfo = function (val) {
                  var getGroupListParam = {};
                  getGroupListParam.task_name = val;
                  getGroupListParam.is_active = 1;
                  return $httpService.get(APP_CONST.API.TASK_LIST, getGroupListParam, false).then(function (responseData) {
                        var data = responseData.data;
                        var hits = data.list;
                        if (hits.length > 10)
                        {
                              hits.splice(10, hits.length);
                        }
                        return hits;
                  });
            };
            $scope.formatTaskModel = function (model) {

                  if (model != null)
                  {
                        $scope.updateTaskInfo(model);
                        return model.task_name;
                  }
                  return  '';
            };
            $scope.updateTaskInfo = function (model)
            {
                  if (typeof model == undefined)
                        return;
                  if (model.task_name != undefined && model.task_name != '')
                  {
                        $scope.taskManagementEditModel.title = model.task_name;
                        $scope.taskManagementEditModel.taskId = model.id;
                        return;
                  } else
                  {
                        $scope.taskManagementEditModel.title = model;
                        $scope.taskManagementEditModel.taskId = 0;
                        return;
                  }
            };
            $scope.getTaskMgtlist = function () {

                  $scope.taskManagementEditModel.isLoadingProgress = true;
                  var headers = {};
                  headers['screen-code'] = 'taskManagement';
                  var getListParam = {};
                  getListParam.is_active = 1;
                  getListParam.id = $stateParams.id;
                  getListParam.ref_id = '';
                  getListParam.ref_name = '';
                  getListParam.start = ($scope.taskManagementEditModel.currentPage - 1) * $scope.taskManagementEditModel.limit;
                  getListParam.limit = $scope.taskManagementEditModel.limit;
                  var getArrayParam = {};
                  getArrayParam.assign_to = [];
                  getArrayParam.type = [];
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getTaskMgtDetail(getArrayParam, configOption, getListParam.is_active, getListParam.id, getListParam.ref_id, getListParam.ref_name, getListParam.start, getListParam.limit, headers).then(function (response) {
                        var data = response.data;
                        if (data.list.length > 0)
                        {
                              $scope.taskDetail = data.list[0];
                        }
                        $scope.initUpdateDetail();
                  });
            };
            $scope.getEmployeeList = function () {

                  var getListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'employee';
                  getListParam.is_active = 1;
                  $scope.taskManagementEditModel.isStaffLoaded = false;
                  $scope.taskManagementEditModel.isLoadingProgress = true;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getEmployeeList(getListParam, configOption, headers).then(function (response) {
                        var data = response.data;
                        $scope.taskManagementEditModel.staffList = data.list;
                        for (var i = 0; i < $scope.taskManagementEditModel.staffList.length; i++)
                        {
                              $scope.taskManagementEditModel.staffList[i].isChecked = false;
                        }
                        $scope.taskManagementEditModel.isStaffLoaded = true;
                        $scope.taskManagementEditModel.isLoadingProgress = false;
                  });
            };
            $scope.getUserList = function () {

                  var getListParam = {};
                  getListParam.isactive = 1;
                  var headers = {};
                  headers['screen-code'] = 'users';
                  $scope.taskManagementEditModel.isUserLoaded = false;
                  $scope.taskManagementEditModel.isLoadingProgress = true;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getUserList(getListParam, configOption, headers).then(function (response) {
                        var data = response.data;
                        $scope.taskManagementEditModel.userList = data.list;
                        for (var i = 0; i < $scope.taskManagementEditModel.userList.length; i++)
                        {
                              $scope.taskManagementEditModel.userList[i].isChecked = false;
                        }
                        $scope.taskManagementEditModel.isUserLoaded = true;
                        $scope.taskManagementEditModel.isLoadingProgress = false;
                  });
            };
            $scope.getTaskTypeList = function () {

                  $scope.taskManagementEditModel.isLoadingProgress = true;
                  var headers = {};
                  headers['screen-code'] = 'empTaskType';
                  var getListParam = {};
                  getListParam.is_active = 1;
                  $scope.taskManagementEditModel.isTypeLoaded = false;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getEmployeeTaskTypeList(getListParam, configOption, headers).then(function (response) {
                        var data = response.data;
                        $scope.taskManagementEditModel.typeList = data.list;
                        for (var i = 0; i < $scope.taskManagementEditModel.typeList.length; i++)
                        {
                              $scope.taskManagementEditModel.typeList[i].isChecked = false;
                        }
                        $scope.taskManagementEditModel.isTypeLoaded = true;
                        $scope.taskManagementEditModel.isLoadingProgress = false;
                  });
            };
            $scope.setStatus = function ()
            {
                  $scope.taskManagementEditModel.statusList = [];
                  var status1 = {
                        name: "New",
                        ischecked: false
                  };
                  $scope.taskManagementEditModel.statusList.push(status1);
                  var status2 = {
                        name: "InProgress",
                        ischecked: false
                  };
                  $scope.taskManagementEditModel.statusList.push(status2);
                  var status3 = {
                        name: "Completed",
                        ischecked: false
                  };
                  $scope.taskManagementEditModel.statusList.push(status3);
                  var status4 = {
                        name: "Closed",
                        ischecked: false
                  };
                  $scope.taskManagementEditModel.statusList.push(status4);
            };
            $scope.setPriority = function ()
            {
                  $scope.taskManagementEditModel.priorityList = [];
                  var priority1 = {
                        name: "High",
                        ischecked: false
                  };
                  $scope.taskManagementEditModel.priorityList.push(priority1);
                  var priority2 = {
                        name: "Medium",
                        ischecked: false
                  };
                  $scope.taskManagementEditModel.priorityList.push(priority2);
                  var priority3 = {
                        name: "Low",
                        ischecked: false
                  };
                  $scope.taskManagementEditModel.priorityList.push(priority3);
            };
            $scope.getTaskActivityCommentsList = function () {

                  $scope.taskManagementEditModel.isLoadingProgress = true;
                  var headers = {};
                  headers['screen-code'] = 'taskManagement';
                  var getListParam = {};
                  getListParam.is_active = 1;
                  getListParam.task_activity_id = $stateParams.id;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getTaskActivityCommentsList(getListParam, configOption, headers).then(function (response) {
                        var data = response.data;
                        $scope.taskManagementEditModel.commentsList = data.list;
                        for( var i=0;i< $scope.taskManagementEditModel.commentsList.length ; i++)
                        {
                            var date = $scope.taskManagementEditModel.commentsList[i].created_at.split(' ');
                            var newdate = utilityService.convertToUTCDate(date[0]);
                            $scope.taskManagementEditModel.commentsList[i].created_at = utilityService.parseDateToStr(newdate, $rootScope.appConfig.date_format);

                        }
                        $scope.taskManagementEditModel.isLoadingProgress = false;
                  });
            };
            $scope.modifyTaskMgt = function () {

                  var headers = {};
                  headers['screen-code'] = 'taskManagement';
                  $scope.isDataSavingProcess = true;
                  var createTaskActivityParam = {};
                  if (typeof $scope.taskManagementEditModel.task.id != 'undefined' && typeof $scope.taskManagementEditModel.task.task_name != 'undefined' && typeof $scope.taskManagementEditModel.task == 'object')
                  {
                        createTaskActivityParam.title = $scope.taskManagementEditModel.task.task_name;
                        createTaskActivityParam.task_name = $scope.taskManagementEditModel.task.task_name;
                        createTaskActivityParam.task_id = $scope.taskManagementEditModel.taskId;
                  } else
                  {
                        createTaskActivityParam.title = $scope.taskManagementEditModel.task;
                        createTaskActivityParam.task_name = $scope.taskManagementEditModel.task;
                        createTaskActivityParam.task_id = 0;
                  }
                  if ($scope.taskManagementEditModel.ref_id != '' && $scope.taskManagementEditModel.ref_id != null && $scope.taskManagementEditModel.ref_id != 'undefined')
                  {
                        createTaskActivityParam.ref_id = $scope.taskManagementEditModel.ref_id;
                        createTaskActivityParam.ref_name = $scope.taskManagementEditModel.ref_name;
                  } else
                  {
                        createTaskActivityParam.ref_id = '';
                        createTaskActivityParam.ref_name = '';
                  }
                  createTaskActivityParam.emp_id = $scope.taskManagementEditModel.staffId;
                  for (var i = 0; i < $scope.taskManagementEditModel.staffList.length; i++)
                  {
                        if ($scope.taskManagementEditModel.staffId == $scope.taskManagementEditModel.staffList[i].id)
                        {
                              createTaskActivityParam.emp_name = $scope.taskManagementEditModel.staffList[i].f_name;
                        }
                  }
                  createTaskActivityParam.created_for = $scope.taskManagementEditModel.createdFor;
                  createTaskActivityParam.type_id = $scope.taskManagementEditModel.typeId;
                  for (var i = 0; i < $scope.taskManagementEditModel.typeList.length; i++)
                  {
                        if ($scope.taskManagementEditModel.typeId == $scope.taskManagementEditModel.typeList[i].id)
                        {
                              createTaskActivityParam.type_name = $scope.taskManagementEditModel.typeList[i].name;
                        }
                  }
                  createTaskActivityParam.configType = '';
                  if ($scope.taskManagementEditModel.estimatedDate != '' && $scope.taskManagementEditModel.estimatedDate != undefined && $scope.taskManagementEditModel.estimatedDate != null)
                  {
                        if (typeof $scope.taskManagementEditModel.estimatedDate == 'object')
                        {
                              $scope.taskManagementEditModel.estimatedDate = utilityService.parseDateToStr($scope.taskManagementEditModel.estimatedDate, $scope.adminService.appConfig.date_format);
                        }
                        createTaskActivityParam.estimated_date = utilityService.changeDateToSqlFormat($scope.taskManagementEditModel.estimatedDate, $scope.adminService.appConfig.date_format);
                  } else
                  {
                        createTaskActivityParam.estimated_date = null;
                  }
                  if ($scope.taskManagementEditModel.date != '' && $scope.taskManagementEditModel.date != undefined && $scope.taskManagementEditModel.date != null)
                  {
                        if (typeof $scope.taskManagementEditModel.date == 'object')
                        {
                              $scope.taskManagementEditModel.date = utilityService.parseDateToStr($scope.taskManagementEditModel.date, $scope.adminService.appConfig.date_format);
                        }
                        createTaskActivityParam.date = utilityService.changeDateToSqlFormat($scope.taskManagementEditModel.date, $scope.adminService.appConfig.date_format);
                  } else
                  {
                        createTaskActivityParam.date = null;
                  }
                  createTaskActivityParam.from_time = '';
                  createTaskActivityParam.to_time = '';
                  createTaskActivityParam.comments = '';
                  createTaskActivityParam.status = $scope.taskManagementEditModel.status;
                  createTaskActivityParam.priority = $scope.taskManagementEditModel.priority;
                  createTaskActivityParam.attachments = [];
                  for (var i = 0; i < $scope.taskManagementEditModel.attachments.length; i++)
                  {
                        var imageParam = {};
                        imageParam.id = $scope.taskManagementEditModel.attachments[i].id;
                        imageParam.url = $scope.taskManagementEditModel.attachments[i].url;
                        imageParam.comments = $scope.taskManagementEditModel.attachments[i].comments;
                        createTaskActivityParam.attachments.push(imageParam);
                  }
                  createTaskActivityParam.followers = [];
                  for (var i = 0; i < $scope.taskManagementEditModel.followers.length; i++)
                  {
                        var followerParam = {};
                        followerParam.emp_id = $scope.taskManagementEditModel.followers[i].id;
                        createTaskActivityParam.followers.push(followerParam);
                  }

                  adminService.editTaskMgt(createTaskActivityParam, $scope.taskManagementEditModel.id, headers).then(function (response) {
                        if (response.data.success == true)
                        {
                              //$scope.formReset();
                              $scope.isDataSavingProcess = false;
                              //                    $state.go('app.taskManagement');
                              $state.go('app.taskManagementEdit', {
                                    'id': $stateParams.id
                              }, {
                                    'reload': true
                              });
                        } else
                        {
                              $scope.isDataSavingProcess = false;
                        }
                  });
            };
            $scope.createTaskActivityComments = function ()
            {
                  if ($scope.taskManagementEditModel.comments != null && $scope.taskManagementEditModel.comments != '')
                  {
                        $scope.isCommentSavingProcess = true;
                        var commentsParam = [];
                        var createTaskActivityCommentsParam = {};
                        createTaskActivityCommentsParam.task_activity_id = $stateParams.id;
                        createTaskActivityCommentsParam.message = $scope.taskManagementEditModel.comments;
                        createTaskActivityCommentsParam.comments = $scope.taskManagementEditModel.comments;
                        commentsParam.push(createTaskActivityCommentsParam);
                        adminService.saveTaskActivityComments(commentsParam).then(function (response) {
                              if (response.data.success == true)
                              {
                                    $scope.isCommentSavingProcess = false;
                                    $scope.taskManagementEditModel.comments = '';
                                    $scope.getTaskActivityCommentsList();
                              } else
                              {
                                    $scope.isCommentSavingProcess = false;
                              }
                        });
                  } else
                  {
                        sweet.show('Oops...', 'Type Comments...', 'error');
                  }
            };
            $scope.showPhotoGalaryPopup = false;
            $scope.showPopup = function (index, attachindex)
            {
                  if (index == 'morefileupload')
                  {
                        $scope.showUploadMoreFilePopup = true;
                  } else if (index === 'photogalary')
                  {
                        $scope.showPhotoGalaryPopup = true;
                  }
            }
            $scope.closePopup = function (index)
            {
                  if (index == 'morefileupload')
                  {
                        $scope.showUploadMoreFilePopup = false;
                  } else if (index === 'photogalary')
                  {
                        $scope.showPhotoGalaryPopup = false;
                  }
            }

            $scope.uploadedFileQueue = [];
            $scope.uploadedFileCount = 0;
            $scope.isImageSavingProcess = false;
            $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
            function updateFileUploadEventHandler(event, data, category, imgcaption, index)
            {
                  $scope.uploadedFileQueue = data;
                  if (typeof category != undefined && category != null && category != '')
                  {
                        $scope.uploadedFileQueue[index].category = category;
                  }
                  if (typeof imgcaption != undefined && imgcaption != null && imgcaption != '')
                  {
                        $scope.uploadedFileQueue[index].imgcaption = imgcaption;
                  }
                  console.log('$scope.uploadedFileQueue');
                  console.log($scope.uploadedFileQueue);
            }

            $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
            function deletedUploadFileEventHandler(event, index)
            {
                  if ($scope.showUploadMoreFilePopup)
                  {
                        if ($scope.taskManagementEditModel.attachments.length > index)
                        {
                              $scope.taskManagementEditModel.attachments.splice(index, 1);
                        }
                  }
            }


            $scope.deleteImage = function ($index)
            {
                  $scope.taskManagementEditModel.attachments.splice($index, 1);
            }

            $scope.saveImagePath = function ()
            {
                  if ($scope.uploadedFileQueue.length == 0)
                  {
                        if ($scope.showUploadMoreFilePopup)
                        {
                              $scope.closePopup('morefileupload');
                        }
                  }
                  var hasUploadCompleted = true;
                  for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                  {
                        if (!$scope.uploadedFileQueue[i].isSuccess)
                        {
                              hasUploadCompleted = false;
                              $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                              break;
                        }
                  }
                  if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
                  {
                        $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                        $scope.isImageSavingProcess = true;
                        for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                        {
                              //               $scope.taskManagementEditModel.attachments.push($scope.uploadedFileQueue[i]);
                              var imageUpdateParam = {};
                              imageUpdateParam.id = 0;
                              imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                              imageUpdateParam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                              imageUpdateParam.comments = '';
                              imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
                              imageUpdateParam.path = $scope.uploadedFileQueue[i].signedUrl;
                              $scope.taskManagementEditModel.attachments.push(imageUpdateParam);
                              if (i == $scope.uploadedFileQueue.length - 1)
                              {
                                    $scope.closePopup('morefileupload');
                              }
                        }
                        $scope.modifyTaskMgt();
                  } else
                  {
                        $scope.closePopup('morefileupload');
                  }
            };
            $scope.addNewFollower = function () {
                  var addFollwerParam = {};

                  if ($scope.taskManagementEditModel.employeeInfo != null && $scope.taskManagementEditModel.employeeInfo != 'undefined' && $scope.taskManagementEditModel.employeeInfo != '' && $scope.taskManagementEditModel.employeeInfo.id != undefined)
                  {
                        addFollwerParam.id = $scope.taskManagementEditModel.employeeInfo.id;
                        addFollwerParam.f_name = $scope.taskManagementEditModel.employeeInfo.f_name;
                        var found = false;
                        for (var i = 0; i < $scope.taskManagementEditModel.followers.length; i++)
                        {
                              if ($scope.taskManagementEditModel.followers[i].id == addFollwerParam.id)
                              {
                                    found = true;
                                    $scope.taskManagementEditModel.employeeInfo = '';
                              }
                        }
                        if (found == false)
                        {
                              $scope.taskManagementEditModel.followers.push(addFollwerParam);
                              $scope.taskManagementEditModel.employeeInfo = '';
                        } else
                        {
                              swal("oops!", "Follower already added!", "error");

                        }

                  }
            };
            $scope.getEmployeeSearchList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.fname = val;
                  autosearchParam.is_active = 1;
                  return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
                  {
                        var data = responseData.data.list;
                        var hits = data;
                        if (hits.length > 10)
                        {
                              hits.splice(10, hits.length);
                        }
                        return hits;
                  });
            };

            $scope.formatEmployeModel = function (model)
            {
                  if (model != null)
                  {
                        if (model.f_name != undefined && model.emp_code != undefined)
                        {
                              return model.f_name + '(' + model.emp_code + ')';
                        } else if (model.f_name != undefined && model.company != undefined)
                        {
                              return model.f_name + '(' + model.team_name + ')';
                        }
                        return model.f_name;
                  }
                  return  '';
            };

            $scope.getTaskMgtlist();
            $scope.getTaskActivityCommentsList();
            $scope.setStatus();
            $scope.setPriority();
            $scope.getTaskTypeList();
            $scope.getEmployeeList();
            $scope.getUserList();
//   $scope.getMediaList();
      }]);




