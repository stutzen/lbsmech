
app.controller('clientAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST) {

        $scope.clientAddModel = {
            "id": 0,
            "name": "",
            "companyId": "",
            "email": "",
            "categoryList": [],
            "category": '',
            "mobile": "",
            "city": "",
            "state": "",
            "country": "",
            "postcode": "",
            "balanceAmount": 0,
            "contactPersonName": "",
            "contactPersonRole": "",
            "contactPersonPhno": "",
            "address": "",
            "isactive": 1,
            "userList": [],
            "accno": '',
            "familycode": '',
            "belongto": '',
            "commission": ''

        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_client_form != 'undefined' && typeof $scope.create_client_form.$pristine != 'undefined' && !$scope.create_client_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {

            if (typeof $scope.create_client_form != 'undefined')
            {
                $scope.create_client_form.$setPristine();
                $scope.clientAddModel.name = '';
                $scope.clientAddModel.mobile = '';
                $scope.clientAddModel.category = '';
                $scope.clientAddModel.email = '';
                $scope.clientAddModel.address = '';
                $scope.clientAddModel.city = '';
                $scope.clientAddModel.postcode = '';
                $scope.clientAddModel.state = '';
                $scope.clientAddModel.country = '';
                $scope.clientAddModel.familycode = '';
                $scope.clientAddModel.belongto = '';
                $scope.clientAddModel.commission = '';
                $scope.clientAddModel.contactPersonName = '';
                $scope.clientAddModel.contactPersonPhno = '';
                $scope.clientAddModel.contactPersonRole = '';

            }

        }

        $scope.getBelongtoUserList = function() {
            var userListParam = {};
            userListParam.id = "";
            userListParam.name = "";
            userListParam.start = 0;
            userListParam.limit = 100;
            adminService.getUserList(userListParam).then(function(response) {
                var data = response.data.data;
                $scope.clientAddModel.userList = data;

            });

        };


        $scope.getCategoryList = function() {
            var categoryListParam = {};
            categoryListParam.id = "";
            categoryListParam.name = "";
            categoryListParam.start = 0;
            categoryListParam.limit = 100;
            adminService.getCategoryList(categoryListParam).then(function(response) {
                var data = response.data.list;
                $scope.clientAddModel.categoryList = data;
                $scope.isCategoryLoaded = true;
            });

        };




        $scope.createClient = function() {

            $scope.isDataSavingProcess = true;
            var createClientParam = {};
            createClientParam.id = 0;
            createClientParam.name = $scope.clientAddModel.name;
            createClientParam.email = $scope.clientAddModel.email;
            createClientParam.state = $scope.clientAddModel.state;
            createClientParam.Country = $scope.clientAddModel.country;
            createClientParam.ph_no = $scope.clientAddModel.mobile;
            createClientParam.address = $scope.clientAddModel.address;
            createClientParam.post_code = $scope.clientAddModel.postcode;
            createClientParam.balance_amount = $scope.clientAddModel.balanceAmount;
            createClientParam.contactpersonname = $scope.clientAddModel.contactPersonName;
            createClientParam.city = $scope.clientAddModel.city;
            createClientParam.contactpersonrole = $scope.clientAddModel.contactPersonRole;      
            createClientParam.category_id = parseInt($scope.clientAddModel.category);
            for (i = 0; i < $scope.clientAddModel.categoryList.length; i++)
            {
                if ($scope.clientAddModel.category == $scope.clientAddModel.categoryList[i].id)
                {
                    createClientParam.category_name = $scope.clientAddModel.categoryList[i].name;
                    break;
                }
            }
            createClientParam.family_code = $scope.clientAddModel.familycode;
            createClientParam.customer_of = $scope.clientAddModel.belongto;
            createClientParam.commission = $scope.clientAddModel.commission;

            createClientParam.companyId = $rootScope.userModel.companyId;            
//            createClientParam.city = $scope.clientAddModel.city;                                             
//            createClientParam.contactPersonPhno = $scope.clientAddModel.contactPersonPhno;                                   
//            createClientParam.isactive = 1;
//            createClientParam.imagePath = '';
            adminService.createClient(createClientParam).then(function(response) {

                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.clientlist')
                }
                $scope.isDataSavingProcess = false;

            });
        };

        $scope.getCategoryList();
        $scope.getBelongtoUserList();

    }]);




