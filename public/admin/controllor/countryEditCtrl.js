

app.controller('countryEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.countryModel = {
            id: '',
            name: '',
            isActive: true,
            list: {},
            code: ''
         
        }

        $scope.validationFactory = ValidationFactory;
       


        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_country_form != 'undefined' && typeof $scope.add_country_form.$pristine != 'undefined' && !$scope.add_country_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_country_form.$setPristine();
            if (typeof $scope.add_country_form != 'undefined')
            {
                $scope.countryModel.name = "";
                $scope.countryModel.isActive = true;
                $scope.countryModel.code="";
            }
            $scope.updateCountryInfo();
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.modifycountry = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addCountryParam = {};
            var headers = {};
            headers['screen-code'] = 'country';
            addCountryParam.id = $stateParams.id;
            addCountryParam.name = $scope.countryModel.name;
            addCountryParam.code = $scope.countryModel.code;
            addCountryParam.is_active = $scope.countryModel.isActive;

            adminService.editCountry(addCountryParam, $scope.countryModel.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.country');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.updateCountryInfo = function()
        {
            $scope.countryModel.is_active = $scope.countryModel.list.is_active;
            $scope.countryModel.name = $scope.countryModel.list.name;
            $scope.countryModel.isLoadingProgress = false;
            $scope.countryModel.id = $scope.countryModel.list.id;
            $scope.countryModel.code = $scope.countryModel.list.code;
        }
        $scope.getCountryInfo = function()
        {
            $scope.countryModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var countryListParam = {};
                countryListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getCountryList(countryListParam, configOption).then(function(response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.countryModel.list = data.list[0];

                    }
                    $scope.updateCountryInfo();

                });
            }
        };
        $scope.getCountryInfo();
    }]);



