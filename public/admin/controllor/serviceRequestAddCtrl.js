




app.controller('serviceRequestAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', 'sweet', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, sweet, $stateParams, $timeout, ValidationFactory) {

            $scope.serviceModel = {
                  "name": "",
                  "current_count": 0,
                  "is_active": 1,
                  "productname": "",
                  "customerInfo": "",
                  "empInfo": "",
                  "machine_status": "",
                  "problem": "",
                  "date": "",
                  "model_name": "",
                  "status": "",
                  "attachment": [],
                  "serviceList": [],
                  "productList": [],
                  "productInfo": [],
                  "serviceListCurrentPage": 1,
                  "serviceListTotal": 0,
                  "serviceListLimit": 10
            };
            $scope.validationFactory = ValidationFactory;
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.currencyFormat = $rootScope.appConfig.currency;
            $scope.pagePerCount = [50, 100];
            $scope.serviceModel.status = "new";
            $scope.adminService = adminService;
            $scope.serviceModel.serviceListLimit = $scope.pagePerCount[0];
            $scope.getNavigationBlockMsg = function (pageReload)
            {
                  if (typeof $scope.create_servicerequest_form !== 'undefined' && typeof $scope.create_servicerequest_form.$pristine !== 'undefined' && !$scope.create_servicerequest_form.$pristine)
                  {
                        return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
                  }

                  return "";
            };
            $scope.isDataSavingProcess = false;
            $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
            $scope.formReset = function ()
            {
                  $scope.create_servicerequest_form.$setPristine();
                  $scope.serviceModel.productname = "";
                  $scope.serviceModel.is_active = 1;
                  $scope.serviceModel.customerInfo = 0;
                  $scope.serviceModel.empInfo = '';
                  $scope.serviceModel.machine_status = '';
                  $scope.serviceModel.problem = "";
                  $scope.serviceModel.date = "";
                  $scope.serviceModel.model_name = "";
                  $scope.serviceModel.status = "";
                  $scope.serviceModel.problem = "";
                  $scope.serviceModel.attachement = [];
            };
            $scope.currentDate = new Date();
            $scope.serviceModel.date = $scope.currentDate;
            $scope.showServiceRequestPopup = false;
            $scope.startDateOpen = false;
            $scope.openDate = function (index)
            {
                  if (index == 0)
                  {
                        $scope.startDateOpen = true;
                  }
            }
            $scope.createServiceRequest = function ()
            {
                  if ($scope.isDataSavingProcess == false)
                  {
                        $scope.isDataSavingProcess = true;
                        var createServiceRequestParam = {};
                        var headers = {};
                        headers['screen-code'] = 'servicerequest';
                        createServiceRequestParam.customer_id = $scope.serviceModel.customerInfo.id;
                        createServiceRequestParam.product_id = $scope.serviceModel.productname.product_id;
                        createServiceRequestParam.serial_no = $scope.serviceModel.productname.serial_no;
                        if ($scope.serviceModel.productname.invoice_item_id == 0)
                        {
                              createServiceRequestParam.product_id = $scope.serviceModel.productname.prod_id;
                        }
                        createServiceRequestParam.product_name = $scope.serviceModel.productname.product_name;
                        createServiceRequestParam.invoice_id = $scope.serviceModel.productname.invoice_id;
                        createServiceRequestParam.invoice_item_id = $scope.serviceModel.productname.invoice_item_id;
                        createServiceRequestParam.is_active = $scope.serviceModel.is_active;
                        createServiceRequestParam.assigned_emp_id = $scope.serviceModel.empInfo.id;
                        createServiceRequestParam.machine_status = $scope.serviceModel.machine_status;
                        if (typeof $scope.serviceModel.date == 'object')
                        {
                              $scope.serviceModel.date = utilityService.parseDateToStr($scope.serviceModel.date, 'yyyy-MM-dd');
                        }
                        createServiceRequestParam.date = utilityService.changeDateToSqlFormat($scope.serviceModel.date, 'yyyy-MM-dd');
                        createServiceRequestParam.model = $scope.serviceModel.model_name;
                        createServiceRequestParam.status = $scope.serviceModel.status;
                        createServiceRequestParam.problem = $scope.serviceModel.problem;
                        createServiceRequestParam.attachement = [];
                         var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                        adminService.createServiceRequestList(createServiceRequestParam,configOption, headers).then(function (response) {
                              if (response.data.success === true)
                              {
                                    $scope.formReset();
                                    $state.go('app.servicerequest');
                              }
                              $scope.isDataSavingProcess = false;
                        });
                  } else
                  {
                        return;
                  }

            };

            $scope.getCustomerList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.search = val;
                  autosearchParam.is_sale = 1;
                  return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                  {
                        var data = responseData.data.list;
                        var hits = [];
                        if (data.length > 0)
                        {
                              for (var i = 0; i < data.length; i++)
                              {
                                    if (data[i].is_sale == '1' || data[i].is_sale == 1)
                                    {
                                          hits.push(data[i]);
                                    }
                              }
                        }
                        if (hits.length > 10)
                        {
                              hits.splice(10, hits.length);
                        }
                        return hits;
                  });
            };

            $scope.formatCustomerModel = function (model)
            {
                  if (model != null)
                  {
                        var phoneNumber = '';
                        if (model.phone != undefined)
                        {
                              if (model.phone.indexOf(','))
                              {
                                    phoneNumber = model.phone.split(',')[0];
                              }
                        }
                        if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                        {

                              return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                        } else if (model.fname != undefined && phoneNumber != '')
                        {

                              return model.fname + '(' + phoneNumber + ')';
                        } else if (model.fname != undefined)
                        {
                              return model.fname;
                        }
                  }
                  return  '';
            };

            $scope.getEmployeeList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.fname = val;
                  autosearchParam.is_active = 1;
                  if (autosearchParam.fname != '')
                  {
                        return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
                        {
                              var data = responseData.data.list;
                              var hits = data;
                              if (hits.length > 10)
                              {
                                    hits.splice(10, hits.length);
                              }
                              return hits;
                        });
                  }
            };

            $scope.formatEmployeeModel = function (model)
            {
                  if (model != null)
                  {
                        if (model.f_name != undefined && model.id != undefined && model.emp_code != undefined)
                        {
                              return model.f_name + '(' + model.id + ',' + model.emp_code + ')';
                        } else if (model.f_name != undefined && model.id != undefined)
                        {
                              return model.f_name + '(' + model.id + ')';
                        }
                  }
                  return  '';
            };

            $scope.getProductList = function (val)
            {
                  if ($scope.serviceModel.customerInfo != '' && $scope.serviceModel.customerInfo != 'undefined' && $scope.serviceModel.customerInfo != null && $scope.serviceModel.customerInfo.id != 'undefined')
                  {
                        var autoSearchParam = {};
                        autoSearchParam.search = val;
                        autoSearchParam.customer_id = $scope.serviceModel.customerInfo.id;
                        return $httpService.get(APP_CONST.API.GET_CUSTOMER_INVOICE_ITEM_LIST, autoSearchParam, false).then(function (response)
                        {
                              var data = response.data.list;
                              var hits = data;
                              if (hits.length > 10)
                              {
                                    hits.splice(100, hits.length);
                              }
                              return hits;
                        });
                  } else
                  {
                       sweet.show('Oops...', 'Please select atleast one customer', 'error');
                  }
            }
            $scope.formatProductModel = function (model)
            {
                  if (model != null)
                  {
                        if (model.product_name != undefined && model.prod_id != undefined)
                        {
                              return model.product_name;
                        }
                  }
                  return  '';
            };
            $scope.updateProductDetails = function()
            {
                if($scope.serviceModel.customerInfo == '' || $scope.serviceModel.customerInfo == 'undefined' || $scope.serviceModel.customerInfo == null )
                  {
                      $scope.serviceModel.productname = '';
                     
                  }
              }
            $scope.updateProductInfo = function () {
                  $scope.serviceModel.productname.product_name = $scope.serviceModel.productname.product_name;
                  var duedate = utilityService.parseStrToDate($scope.serviceModel.productname.duedate);
                  $scope.serviceModel.productname.date = $filter('date')(duedate, $rootScope.appConfig.date_format);
                  var amount = utilityService.changeCurrency($scope.serviceModel.productname.total_price, $rootScope.appConfig.thousand_seperator);
                  var newamount = $scope.currencyFormat + '.' + amount;
                  $scope.serviceModel.productname.amount = newamount;
                  $scope.serviceModel.productname.life_time = $scope.serviceModel.productname.life_time;
                  $scope.serviceModel.productname.amc_status = $scope.serviceModel.productname.machine_status;
                  if ($scope.serviceModel.productname.amc_status == 'installation')
                  {
                        $scope.serviceModel.productname.amc_covered = "Installation";
                  } else if ($scope.serviceModel.productname.amc_status == 'i/w')
                  {
                        $scope.serviceModel.productname.amc_covered = "In Warranty";
                  } else if ($scope.serviceModel.productname.amc_status == 'o/w')
                  {
                        $scope.serviceModel.productname.amc_covered = "Out Warranty";
                  } else if ($scope.serviceModel.productname.amc_status == 'amc')
                  {
                        $scope.serviceModel.productname.amc_covered = "AMC";
                  }
                  $scope.serviceModel.productname.amc_status = $scope.serviceModel.productname.machine_status;
                  $scope.serviceModel.machine_status = $scope.serviceModel.productname.amc_status;

            };
            $scope.updateStatusInfo = function ()
            {
                  if ($scope.serviceModel.productname == undefined)
                  {
                        $scope.serviceModel.machine_status = '';
                  }
            }

            $scope.openServiceRequestPopup = function ()
            {
                  $scope.showServiceRequestPopup = true;
            };

            $scope.closeServiceRequestPopup = function ()
            {
                  $scope.showServiceRequestPopup = false;
            };

            $scope.getServiceRequestList = function ()
            {
                  if ($scope.serviceModel.customerInfo != '' && $scope.serviceModel.customerInfo != 'undefined' && $scope.serviceModel.customerInfo != null && $scope.serviceModel.customerInfo.id != 'undefined')
                  {
                        var getListParam = {};
                        getListParam.customer_id = $scope.serviceModel.customerInfo.id;
                        getListParam.product_id = '';
                        if ($scope.serviceModel.productname != '' && $scope.serviceModel.productname != 'undefined' && $scope.serviceModel.productname != null && $scope.serviceModel.productname.id != 'undefined')
                        {
                              getListParam.product_id = $scope.serviceModel.productname.product_id;
                        }
                        $scope.serviceModel.isLoadingProgress = true;
                        var headers = {};
                        headers['screen-code'] = 'servicerequest';
                        var configOption = adminService.handleOnlyErrorResponseConfig;
                        adminService.getServiceRequestList(getListParam, configOption, headers).then(function (response) {
                              if (response.data.success === true)
                              {
                                    var data = response.data.list;
                                    $scope.serviceModel.serviceList = data;
                                    $scope.serviceModel.serviceListTotal = data.total;
                                    if ($scope.serviceModel.serviceList.length > 0)
                                    {
                                          for (var i = 0; i < $scope.serviceModel.serviceList.length; i++)
                                          {
                                                var datevalue = moment($scope.serviceModel.serviceList[i].date).valueOf();
                                                if (datevalue > 0)
                                                {
                                                      var nextdate = $scope.serviceModel.serviceList[i].date.split(' ');
                                                      var newnextdate = utilityService.convertToUTCDate(nextdate[0]);
                                                      $scope.serviceModel.serviceList[i].date = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                                                } else
                                                      $scope.serviceModel.serviceList[i].date = '';
                                          }
                                    }
                              }
                              $scope.serviceModel.isLoadingProgress = false;
                        });
                  } else
                  {
                        sweet.show('Oops...', 'Please select atleast one customer', 'error');
                  }
            };


      }
]);




