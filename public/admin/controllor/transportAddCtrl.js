app.controller('transportAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', function ($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state) {
        $scope.transportModel = {
            "name": "",
            "landline_no": "",
            "email": "",
            "mobile": "",
            "city_id": "",
            "city_name": "",
            "state_id": "",
            "state_name": "",
            "country_id": "",
            "country_name": "",
            "postcode": "",
            "address": "",
            "land_mark": "",
            "stateList": [],
            "cityList": [],
            "countryList": [],
            "comments": ""
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_transport_form != 'undefined' && typeof $scope.create_transport_form.$pristine != 'undefined' && !$scope.create_transport_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function () {

            $scope.create_transport_form.$setPristine();
            $scope.transportModel.name = '';
            $scope.transportModel.landline_no = '';
            $scope.transportModel.mobile = '';
            $scope.transportModel.email = '';
            $scope.transportModel.city_id = '';
            $scope.transportModel.city_name = '';
            $scope.transportModel.postcode = '';
            $scope.transportModel.state_id = '';
            $scope.transportModel.state_name = '';
            $scope.transportModel.country_id = '';
            $scope.transportModel.country_name = '';
            $scope.transportModel.address = '';
            $scope.transportModel.land_mark = '';
            $scope.transportModel.comments = '';
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.createTransport = function () {

            $scope.isDataSavingProcess = true;
            var createTransportParam = {};
            var headers = {};
            headers['screen-code'] = 'transport';
            createTransportParam.id = 0;
            createTransportParam.name = $scope.transportModel.name;
            createTransportParam.landline_no = $scope.transportModel.landline_no;
            createTransportParam.mobile_no = $scope.transportModel.mobile;
            createTransportParam.email = $scope.transportModel.email;
            createTransportParam.city_id = $scope.transportModel.city_id;
            for (var i = 0; i < $scope.transportModel.cityList.length; i++)
            {
                if ($scope.transportModel.city_id == $scope.transportModel.cityList[i].id)
                {
                    createTransportParam.city_name = $scope.transportModel.cityList[i].name;
                }
            }
            createTransportParam.country_id = $scope.transportModel.country_id;
            for (var i = 0; i < $scope.transportModel.countryList.length; i++)
            {
                if ($scope.transportModel.country_id == $scope.transportModel.countryList[i].id)
                {
                    createTransportParam.country_name = $scope.transportModel.countryList[i].name;
                }
            }
            createTransportParam.state_id = $scope.transportModel.state_id;
            for (var i = 0; i < $scope.transportModel.stateList.length; i++)
            {
                if ($scope.transportModel.state_id == $scope.transportModel.stateList[i].id)
                {
                    createTransportParam.state_name = $scope.transportModel.stateList[i].name;
                }
            }
            createTransportParam.postcode = $scope.transportModel.postcode;
            createTransportParam.address = $scope.transportModel.address;
            createTransportParam.land_mark = $scope.transportModel.land_mark;
            createTransportParam.comments = $scope.transportModel.comments;
            adminService.createTransport(createTransportParam, headers).then(function (response) {

                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.transport')
                }
                $scope.isDataSavingProcess = false;
            });
        };
        
        $scope.countryChange = function () 
        {
            $scope.transportModel.state_id = '';
            $scope.transportModel.city_id = '';            
            $scope.transportModel.cityList = [];
            $scope.transportModel.stateList = [];            
        }
        
        $scope.stateChange = function () 
        {            
            $scope.transportModel.city_id = '';  
            $scope.transportModel.cityList = [];                                  
        }

        $scope.getCountryList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.code = "";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.transportModel.countryList = data;
            });
        };

        $scope.initAddressStateListTimeoutPromise = null;

        $scope.initAddressStateList = function (searchkey)
        {
            if ($scope.initAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initAddressStateListTimeoutPromise);
            }
            $scope.initAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey);
            }, 300);

        };

        $scope.getStateList = function (searchkey) {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_name = "";

            getListParam.country_id = $scope.transportModel.country_id;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;           

            adminService.getStateList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                $scope.transportModel.stateList = data;

            });

        };

        $scope.initAddressCityListTimeoutPromise = null;

        $scope.initAddressCityList = function (searchkey)
        {
            if ($scope.initAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initAddressCityListTimeoutPromise);
            }
            $scope.initAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey);
            }, 300);

        };

        $scope.getCityList = function (searchkey) {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;            
            getListParam.state_id = $scope.transportModel.state_id;
            getListParam.country_id = $scope.transportModel.country_id;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;           

            adminService.getCityList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                $scope.transportModel.cityList = data;                
            });
        };
        
        $scope.getCountryList();

    }]);




