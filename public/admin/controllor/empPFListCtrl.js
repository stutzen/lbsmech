





app.controller('empPFListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.empPFListModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            total_emp_PF_Amount: 0,
            total_emp_PF_Percentage:0,
            total_PF_Amount:0,
            total_PF_Percentage:0,
            total_salary_amount:0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.empPFListModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            empInfo:{}
        };
        $scope.searchFilter.todate = $scope.currentDate;

        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                empInfo:{}
            };
            $scope.searchFilter.todate = $scope.currentDate;
            $scope.empPFListModel.list = [];
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };


        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empPFStatement';
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            if ($scope.searchFilter.empInfo != null && typeof $scope.searchFilter.empInfo != 'undefined' && typeof $scope.searchFilter.empInfo.id != 'undefined')
            {
                getListParam.emp_id = $scope.searchFilter.empInfo.id;
            }
            else
            {
                getListParam.emp_id = '';
            }
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            }
            else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            }
            else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }
            $scope.empPFListModel.isLoadingProgress = true;
            $scope.empPFListModel.isSearchLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeePFList(getListParam, configOption, headers).then(function(response)
            {
                var totalAmt = 0.00;
                var total_emp_PF_amt =0.00;
                var total_emp_PF_percent=0.00;
                var pf_amount=0.00;
                var pf_percentage=0.00;
                var salary_amount=0;
                if (response.data.list.length > 0)
                {
                    var data = response.data.list;
                    $scope.empPFListModel.list = data;
                    for (var i = 0; i < $scope.empPFListModel.list.length; i++)
                    {
                        totalAmt = totalAmt + parseFloat($scope.empPFListModel.list[i].total_pf_amount);
                        $scope.empPFListModel.list[i].total_pf_amount = utilityService.changeCurrency($scope.empPFListModel.list[i].total_pf_amount, $rootScope.appConfig.thousand_seperator);
                        
                        total_emp_PF_amt = total_emp_PF_amt + parseFloat($scope.empPFListModel.list[i].employer_pf_amount);
                        $scope.empPFListModel.list[i].employer_pf_amount = utilityService.changeCurrency($scope.empPFListModel.list[i].employer_pf_amount, $rootScope.appConfig.thousand_seperator);
                        
                        total_emp_PF_percent = total_emp_PF_percent + parseFloat($scope.empPFListModel.list[i].employer_pf_percentage);
                        $scope.empPFListModel.list[i].employer_pf_percentage = utilityService.changeCurrency($scope.empPFListModel.list[i].employer_pf_percentage, $rootScope.appConfig.thousand_seperator);
                        
                        pf_amount = pf_amount + parseFloat($scope.empPFListModel.list[i].pf_amount);
                        $scope.empPFListModel.list[i].pf_amount = utilityService.changeCurrency($scope.empPFListModel.list[i].pf_amount, $rootScope.appConfig.thousand_seperator);
                        
                        pf_percentage = pf_percentage + parseFloat($scope.empPFListModel.list[i].pf_percentage);
                        $scope.empPFListModel.list[i].pf_percentage = utilityService.changeCurrency($scope.empPFListModel.list[i].pf_percentage, $rootScope.appConfig.thousand_seperator);
                        
                        salary_amount = salary_amount + parseFloat($scope.empPFListModel.list[i].salary_amount);
                        $scope.empPFListModel.list[i].salary_amount = utilityService.changeCurrency($scope.empPFListModel.list[i].salary_amount, $rootScope.appConfig.thousand_seperator);
                    }
                    $scope.empPFListModel.total = data.total;
                    
                    $scope.empPFListModel.total_Amount = totalAmt;
                    $scope.empPFListModel.total_Amount = parseFloat($scope.empPFListModel.total_Amount).toFixed(2);
                    $scope.empPFListModel.total_Amount = utilityService.changeCurrency($scope.empPFListModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    
                    $scope.empPFListModel.total_emp_PF_Amount= total_emp_PF_amt;
                    $scope.empPFListModel.total_emp_PF_Amount=parseFloat($scope.empPFListModel.total_emp_PF_Amount).toFixed(2);
                    $scope.empPFListModel.total_emp_PF_Amount = utilityService.changeCurrency($scope.empPFListModel.total_emp_PF_Amount, $rootScope.appConfig.thousand_seperator);
                    
                    $scope.empPFListModel.total_emp_PF_Percentage= total_emp_PF_percent;
                    $scope.empPFListModel.total_emp_PF_Percentage=parseFloat($scope.empPFListModel.total_emp_PF_Percentage).toFixed(2);
                    $scope.empPFListModel.total_emp_PF_Percentage = utilityService.changeCurrency($scope.empPFListModel.total_emp_PF_Percentage, $rootScope.appConfig.thousand_seperator);
                    
                    $scope.empPFListModel.total_PF_Amount= pf_amount;
                    $scope.empPFListModel.total_PF_Amount=parseFloat($scope.empPFListModel.total_PF_Amount).toFixed(2);
                    $scope.empPFListModel.total_PF_Amount = utilityService.changeCurrency($scope.empPFListModel.total_PF_Amount, $rootScope.appConfig.thousand_seperator);
                    
                    $scope.empPFListModel.total_PF_Percentage= pf_percentage;
                    $scope.empPFListModel.total_PF_Percentage=parseFloat($scope.empPFListModel.total_PF_Percentage).toFixed(2);
                    $scope.empPFListModel.total_PF_Percentage = utilityService.changeCurrency($scope.empPFListModel.total_PF_Percentage, $rootScope.appConfig.thousand_seperator);
                    
                    $scope.empPFListModel.total_salary_amount= salary_amount;
                    $scope.empPFListModel.total_salary_amount=parseFloat($scope.empPFListModel.total_salary_amount).toFixed(2);
                    $scope.empPFListModel.total_salary_amount = utilityService.changeCurrency($scope.empPFListModel.total_salary_amount, $rootScope.appConfig.thousand_seperator);
                }
                else
                {
                    $scope.empPFListModel.list = [];
                }
                $scope.empPFListModel.isLoadingProgress = false;
                $scope.empPFListModel.isSearchLoadingProgress = false;
            });

        };
        
       $scope.getEmployeeList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            if (autosearchParam.fname != '')
            {
                return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatEmployeeModel = function(model)
        {
            if (model != null)
            {
                if (model.f_name != undefined && model.id != undefined && model.emp_code != undefined)
                {
                    return model.f_name + '(' + model.id + ',' + model.emp_code + ')';
                }
                else if (model.f_name != undefined && model.id != undefined)
                {
                    return model.f_name + '(' + model.id + ')';
                }
            }
            return  '';
        };
       
        $scope.print = function()
        {
            $window.print();
        };

        //$scope.getList();
    }]);




