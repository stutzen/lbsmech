

app.controller('groupEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$window', '$stateParams', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $window, $stateParams) {

        $scope.groupEditModel = {
            name: '',
            comments: '',
            currentPage: 1,
            total: 0,
            limit: 7,
            isLoadingProgress: false,
            customerList: [],
            groupList: [],
            list: [],
            stateList :[],
            cityList : []
        }
        $scope.list = '';
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [7, 20, 100];
        $scope.groupEditModel.limit = $scope.pagePerCount[0];

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.group_customer_form != 'undefined' && typeof $scope.group_customer_form.$pristine != 'undefined' && !$scope.group_customer_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.searchFilter = {
            email: '',
            name: '',
            company_name: '',
            state: '',
            category: '',
            city: '',
            familycode: '',
            customerOf: '',
            accno: '',
            salesOrPurchase: '',
            customAttribute: []
        };
        $scope.customerFormReset = function()
        {
            $scope.searchFilter = {
                email: '',
                name: '',
                company_name: '',
                state: '',
                category: '',
                city: '',
                familycode: '',
                customerOf: '',
                accno: '',
                salesOrPurchase: '',
                customAttribute: []
            };
            $scope.getStateList();
            $scope.getCityList ();
        }
        $scope.isDataSavingProcess = false;
        $scope.stateParamsData = $stateParams.id;
        $scope.formReset = function()
        {
            $scope.groupEditModel.groupList = [];
            $scope.getGroupInfo();
        };
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDetailTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.updateGroupInfo, 300);
            // $scope.initTableFilterTimeoutPromise = $timeout($scope.getListPrint, 300);
        }
        $scope.initCustomerTableFilter = function()
        {
            if ($scope.initDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDetailTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getCustomerList, 300);
            // $scope.initTableFilterTimeoutPromise = $timeout($scope.getListPrint, 300);
        }
        $scope.updateGroupInfo = function()
        {
            if ($scope.list != 'undefined' && $scope.list != '' && $scope.list != null)
            {
                $scope.groupEditModel.name = $scope.list.name;
                $scope.groupEditModel.comments = $scope.list.comment;
                $scope.groupEditModel.groupList = $scope.list.detail;
            }
             $scope.groupEditModel.isLoadingProgress = false;
        };
        $scope.selectAll = false;
        $scope.checkAllCustomer = function()
        {
            for (var i = 0; i < $scope.groupEditModel.customerList.length; i++)
            {
                if ($scope.selectAll == true)
                {
                    $scope.groupEditModel.customerList[i].customerChecked = true;
                } else
                {
                    $scope.groupEditModel.customerList[i].customerChecked = false;
                }
            }
        };
        $scope.getCustomerList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'customer';

            getListParam.fname = $scope.searchFilter.name;
            getListParam.company_name = $scope.searchFilter.company_name;
            getListParam.email = $scope.searchFilter.email;
            getListParam.state_id = $scope.searchFilter.state_id;
            getListParam.city_id = $scope.searchFilter.city_id;
            if ($scope.searchFilter.salesOrPurchase == 1)
            {
                getListParam.is_sale = 1;
            }
            else if ($scope.searchFilter.salesOrPurchase == 2)
            {
                getListParam.is_purchase = 1;
            }
            getListParam.is_active = 1;
            getListParam.start = ($scope.groupEditModel.currentPage - 1) * $scope.groupEditModel.limit;
            getListParam.limit = $scope.groupEditModel.limit;
            $scope.groupEditModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCustomersList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.groupEditModel.customerList = data;
                    $scope.groupEditModel.total = response.data.total;
                    for (var i = 0; i < $scope.groupEditModel.customerList.length; i++)
                    {
                        $scope.groupEditModel.customerList[i].customerChecked = false;
                    }
                }
                $scope.groupEditModel.isLoadingProgress = false;
            });
        };
         $scope.getStateList = function() {          

            var stateListParam = {};
            var headers = {};
            headers['screen-code'] = 'state';
            stateListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.groupEditModel.stateList = data.list;
                }
            });

        };
        $scope.getCityList = function() {          
            var cityListParam = {};
            var headers = {};
            headers['screen-code'] = 'city';
            if($scope.searchFilter.state_id != '' && $scope.searchFilter.state_id != 'undefined' && $scope.searchFilter.state_id != null)
            {
                cityListParam.state_id = $scope.searchFilter.state_id;
            }
            cityListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCityList(cityListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.groupEditModel.cityList = data.list;
                }
            });

        };
        $scope.groupCustomerAdd = function()
        {
            for (var i = 0; i < $scope.groupEditModel.customerList.length; i++)
            {
                if ($scope.groupEditModel.customerList[i].customerChecked == true)
                {
                    var id = $scope.groupEditModel.customerList[i].id;
                    $scope.checkGroupList(id);
                    if ($scope.groupList == false)
                    {
                        $scope.groupEditModel.groupList.push($scope.groupEditModel.customerList[i]);
                        $scope.groupEditModel.customerList[i].customerChecked = false;
                    }
                }

            }
            $scope.selectAll = false;
            $scope.checkAllCustomer();
        }
        $scope.groupList = false;
        $scope.checkGroupList = function(id)
        {
            $scope.groupList = false;
            if ($scope.groupEditModel.groupList.length > 0)
            {
                for (var j = 0; j < $scope.groupEditModel.groupList.length; j++)
                {
                    if (id == $scope.groupEditModel.groupList[j].customer_id)
                    {
                        $scope.groupList = true;
                    }

                }
            }
            else
            {
                $scope.groupList = false;
            }

        };
        $scope.getGroupInfo = function() {

            $scope.groupEditModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var groupListParam = {};
                groupListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getCustomerGroupList(groupListParam, configOption).then(function(response) {
                    var data = response.data;
                    $scope.list = data.list[0];
                    $scope.initTableFilter();
                });
            }
        };
        $scope.removeGroupCustomer = function(index)
        {
            //$scope.selectGroupId = id;
            $scope.groupEditModel.groupList.splice(index, 1);
            $scope.isdeleteProgress = true;
            $scope.closePopup();

        };

        $scope.getGroupInfo( );

        $scope.modifyGroupInfo = function() {

            $scope.isDataSavingProcess = true;
            var headers = {};
            headers['screen-code'] = 'customergroup';
            var modifyGrouptParam = {};
            modifyGrouptParam.comment = $scope.groupEditModel.comments;
            modifyGrouptParam.name = $scope.groupEditModel.name;
            modifyGrouptParam.detail = [];
            for (var i = 0; i < $scope.groupEditModel.groupList.length; i++)
            {
                if ($scope.groupEditModel.groupList.length > 0)
                {
                    if ($scope.groupEditModel.groupList[i].customer_group_id == undefined)
                    {
                        var customerId = {
                            customer_id: $scope.groupEditModel.groupList[i].id
                        };
                        modifyGrouptParam.detail.push(customerId);
                    }
                    else
                    {
                        var customerId = {
                            customer_id: $scope.groupEditModel.groupList[i].customer_id
                        };
                        modifyGrouptParam.detail.push(customerId);
                    }
                }

            }
            adminService.editCustomerGroup(modifyGrouptParam,$stateParams.id,headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.group');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.stateChange = function () 
        {
            $scope.searchFilter.city_id = '';
            $scope.getCityList();
            
        }
        $scope.getCustomerList();
        $scope.getStateList();
        $scope.getCityList ();

    }]);
