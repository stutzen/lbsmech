app.controller('productiontaskEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', '$stateParams', 'sweet', '$httpService', 'APP_CONST', function ($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $stateParams, sweet, $httpService, APP_CONST) {
        $scope.productionTaskModel = {
            id: '',
            inputProducts: [],
            productList: [],
            taskList: [],
            outputqty: '',
            outputupp: '',
            parentTask: '',
            taskcode: '',
            outputProducts: [],
            taskInfo: '',
            deletedProducts: [],
            productionTaskInfo: '',
            productFor: '',
            byProducts :[],
        isactive: true,
        taskGroupList : [],
        taskgroup : ''
        }
         $scope.currentStep = 1;
         $scope.tabChange = function(value) {
            if(value == 2)
            {
                if($scope.productionTaskModel.byProducts.length == 0)
                {
                    $scope.addNewRow("byProduct");
                }
            }
            $scope.currentStep = value;
            
        }
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_production_task != 'undefined' && typeof $scope.create_production_task.$pristine != 'undefined' && !$scope.create_production_task.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function () {

            $scope.create_production_task.$setPristine();
            $scope.productionTaskModel = {
                inputProducts: [],
                productList: [],
                taskList: [],
                outputqty: '',
                outputupp: '',
                parentTask: '',
                outputProducts: [],
                taskInfo: '',
                deletedProducts: [],
                id: '',
                productFor: ''

            }
            $scope.updateTaskInfo();
            $scope.getProdTaskDetail();
//            $scope.getProductList();
            $scope.getTaskList();
        }

        $scope.addNewRow = function (value)
        {
            if(value == "product")
            {
               var newinput = {};
               newinput.id = 0;
               newinput.name = '';
               newinput.isNew = true;
               newinput.input_qty = '';
               newinput.taskgroupdetail = '';
               $scope.productionTaskModel.inputProducts.push(newinput); 
            }
            if(value == "byProduct")
            {
               var newinput = {};
               newinput.id = 0;
               newinput.name = '';
               newinput.isNew = true;
               newinput.input_qty = '';
               newinput.uom_id = '';
               $scope.productionTaskModel.byProducts.push(newinput); 
            }
        }

        $scope.deleteInputProduct = function (index,value)
        {
            if(value == 'byProduct')
            {
                $scope.productionTaskModel.byProducts.splice(index, 1);
            }
            if(value == 'product')
            {
                $scope.productionTaskModel.inputProducts.splice(index, 1);
            }
        }
        $scope.event = null;
        $scope.keyupHandler = function(event,type)
        {
            //event.preventDefault();
            if (event.keyCode == 13 || event.keyCode == 9)
            {
                if(type == "product")
                {
                    event.preventDefault();
                    var currentFocusField = angular.element(event.currentTarget);
                    var nextFieldId = $(currentFocusField).attr('next-focus-id');
                    var currentFieldId = $(currentFocusField).attr('id');
                    var productIndex = $(currentFocusField).attr('product-index');
                    if (nextFieldId == undefined || nextFieldId == '')
                    {
                        return;
                    }

                    if (currentFieldId.indexOf('_qty') != -1)
                    {
                        var filedIdSplitedValue = currentFieldId.split('_');
                        var existingBrunchCount = $scope.productionTaskModel.inputProducts.length - 1;
                        if (existingBrunchCount == filedIdSplitedValue[filedIdSplitedValue.length - 1])
                        {
                            $scope.addNewRow(type);
                            $timeout(function() {
                                            $scope.focusNextField (nextFieldId);
                                        },
                                        300);
                            
                        }
                    }


                    $("#" + nextFieldId).focus();
                }
                if(type == "byProduct")
                {
                    event.preventDefault();
                    var currentFocusField = angular.element(event.currentTarget);
                    var nextFieldId = $(currentFocusField).attr('next-focus-id');
                    var currentFieldId = $(currentFocusField).attr('id');
                    var productIndex = $(currentFocusField).attr('product-index');
                    if (nextFieldId == undefined || nextFieldId == '')
                    {
                        return;
                    }

                    if (currentFieldId.indexOf('_qty') != -1)
                    {
                        var filedIdSplitedValue = currentFieldId.split('_');
                        var existingBrunchCount = $scope.productionTaskModel.byProducts.length - 1;
                        if (existingBrunchCount == filedIdSplitedValue[filedIdSplitedValue.length - 1])
                        {
                            $scope.addNewRow(type);
                            $timeout(function() {
                                            $scope.focusNextField (nextFieldId);
                                        },
                                        300);
                            
                        }
                    }


                    $("#" + nextFieldId).focus();
                }
            }
           
            

        }

        $scope.focusNextField = function(nextFieldId)
        {
            if (nextFieldId != '')
            {
                $("#" + nextFieldId).focus();
            }
        }
        $scope.getProductList = function (val)
        {
            var getParam = {};
            getParam.is_active = 1;
            getParam.localData = 1;
            getParam.type = 'Product';
            getParam.name = val;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, getParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }

        $scope.formatProductModel = function (model, index,type)
        {
            if(type == "product")
            {
                if (model != null)
                {
                    $scope.updateProductInfo(model, index);
                    return model.name;
                }
                return  '';
            }
            if(type == "byProduct")
            {
                if (model != null)
                {
                    $scope.updateByproductInfo(model, index);
                    return model.name;
                }
                return  '';
            }
            
        }
        $scope.validateData = function()
        {

        }
        $scope.formatOutputProductModel = function (model, index)
        {
            if (model != null)
            {
                $scope.updateOutputProductInfo(model, index);
                return model.name;
            }
            return  '';
        }

        $scope.updateOutputProductInfo = function (model, index)
        {

            if (typeof model == 'undefined')
                return;
            if (model != undefined && model != null && model != '')
            {
                $scope.productionTaskModel.outputProducts = model;
                $scope.productionTaskModel.outputprodname = model.name;
                $scope.productionTaskModel.outputqty = 1;
            }

        }

        $scope.updateProductInfo = function (model, index)
        {

            if (typeof model == 'undefined')
                return;
            console.log(typeof model.isNew);
            if (typeof model.isNew == 'undefined')
            {
                $scope.productionTaskModel.inputProducts[index].id = 0;
                $scope.productionTaskModel.inputProducts[index].input_pdt_id = model.id;
                $scope.productionTaskModel.inputProducts[index].name = model.name;
                $scope.productionTaskModel.inputProducts[index].input_qty = 1;
            } else if (model.isNew == false)
            {
                $scope.productionTaskModel.inputProducts[index].id = model.id;
                $scope.productionTaskModel.inputProducts[index].input_pdt_id = model.input_pdt_id;
                $scope.productionTaskModel.inputProducts[index].name = model.name;
                $scope.productionTaskModel.inputProducts[index].input_qty = model.input_qty;
            }
            return;
        }
        $scope.updateByproductInfo = function (model, index)
        {

            if (typeof model == 'undefined')
                return;
            console.log(typeof model.isNew);
            if (typeof model.isNew == 'undefined')
            {
                $scope.productionTaskModel.byProducts[index].id = 0;
                $scope.productionTaskModel.byProducts[index].input_pdt_id = model.id;
                $scope.productionTaskModel.byProducts[index].name = model.name;
                $scope.productionTaskModel.byProducts[index].uom_id = model.uom_id;
                $scope.productionTaskModel.byProducts[index].input_qty = 1;
            } else if (model.isNew == false)
            {
                $scope.productionTaskModel.byProducts[index].id = model.id;
                $scope.productionTaskModel.byProducts[index].input_pdt_id = model.input_pdt_id;
                $scope.productionTaskModel.byProducts[index].name = model.name;
                $scope.productionTaskModel.byProducts[index].uom_id = model.uom_id;
                $scope.productionTaskModel.byProducts[index].input_qty = model.input_qty;
            }
            return;
        }
        $scope.formatForProductModel = function (model, index)
        {
            if (model != null)
            {
                $scope.updateForProductInfo(model, index);
                return model.name;
            }
            return  '';
        };

        $scope.updateForProductInfo = function (model, index)
        {

            if (typeof model == 'undefined')
                return;
            if (model != undefined && model != null && model != '')
            {
                $scope.productionTaskModel.productFor = model;
                $scope.productionTaskModel.productFor.id = model.id;
                $scope.productionTaskModel.productFor.name = model.name;
                $scope.productionTaskModel.productForName = model.name;
            }

        };

        $scope.getTaskList = function ()
        {
            var getParam = {};
            getParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductionTaskList(getParam, configOption).then(function (response) {

                var data = response.data;
                $scope.productionTaskModel.taskList = data.list;
            });
        }

        $scope.formatTaskModel = function (model, index)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        }

        $scope.updateTaskInfo = function ()
        {
            if ($scope.productionTaskModel.productionTaskInfo != undefined && $scope.productionTaskModel.productionTaskInfo != null
                    && $scope.productionTaskModel.productionTaskInfo != '')
            {
                $scope.productionTaskModel.id = $scope.productionTaskModel.productionTaskInfo.id;
                $scope.productionTaskModel.taskname = $scope.productionTaskModel.productionTaskInfo.name;
                $scope.productionTaskModel.taskcode = $scope.productionTaskModel.productionTaskInfo.bom_code;
            $scope.productionTaskModel.taskgroup = $scope.productionTaskModel.productionTaskInfo.task_group_id + '';
            $scope.getTaskGroupDetailList();
                $scope.productionTaskModel.parentTask =
                        {
                            name: $scope.productionTaskModel.productionTaskInfo.prev_bom_name,
                            id: $scope.productionTaskModel.productionTaskInfo.prev_bom_id
                        }
                $scope.productionTaskModel.outputupp = $scope.productionTaskModel.productionTaskInfo.unit_production_price;
                $scope.productionTaskModel.outputqty = 1;
                $scope.productionTaskModel.inputProducts = $scope.productionTaskModel.productionTaskInfo.input;
                $scope.productionTaskModel.outputProducts =
                        {
                            name: $scope.productionTaskModel.productionTaskInfo.output_pdt_name,
                            id: $scope.productionTaskModel.productionTaskInfo.output_pdt_id
                        };
                $scope.productionTaskModel.productFor =
                        {
                            name: $scope.productionTaskModel.productionTaskInfo.product_for_name,
                            id: $scope.productionTaskModel.productionTaskInfo.product_for_id
                        };
                if ($scope.productionTaskModel.productionTaskInfo.detail != undefined && $scope.productionTaskModel.productionTaskInfo.detail.length > 0)
                {
                    $scope.productionTaskModel.inputProducts = [];
                    for (var i = 0; i < $scope.productionTaskModel.productionTaskInfo.detail.length; i++)
                    {
                        var inputtask = {};
                        inputtask.id = '';
                        inputtask.input_pdt_id = '';
                        inputtask.name = '';
                        inputtask.input_qty = '';
                        inputtask.productiontask = {};
                        $scope.productionTaskModel.inputProducts.push(inputtask);
                    }
                    for (var i = 0; i < $scope.productionTaskModel.productionTaskInfo.detail.length; i++)
                    {
                        $scope.productionTaskModel.inputProducts[i].id = $scope.productionTaskModel.productionTaskInfo.detail[i].id;
                        $scope.productionTaskModel.inputProducts[i].input_pdt_id = $scope.productionTaskModel.productionTaskInfo.detail[i].input_pdt_id;
                        $scope.productionTaskModel.inputProducts[i].name = $scope.productionTaskModel.productionTaskInfo.detail[i].name;
                        $scope.productionTaskModel.inputProducts[i].input_qty = $scope.productionTaskModel.productionTaskInfo.detail[i].input_qty;
                        $scope.productionTaskModel.inputProducts[i].taskgroup = $scope.productionTaskModel.productionTaskInfo.detail[i].task_group_detail_name;
                        $scope.productionTaskModel.inputProducts[i].taskgroup_id = $scope.productionTaskModel.productionTaskInfo.detail[i].task_group_detail_id;
                        $scope.productionTaskModel.inputProducts[i].isNew = false;
                        $scope.productionTaskModel.inputProducts[i].productiontask = {
                            id: $scope.productionTaskModel.productionTaskInfo.detail[i].id,
                            input_pdt_id: $scope.productionTaskModel.productionTaskInfo.detail[i].input_pdt_id,
                            name: $scope.productionTaskModel.productionTaskInfo.detail[i].name,
                            input_qty: $scope.productionTaskModel.productionTaskInfo.detail[i].input_qty,
                            isNew: false
                        };
                        $scope.productionTaskModel.inputProducts[i].taskgroupdetail = $scope.productionTaskModel.inputProducts[i];
                        console.log($scope.productionTaskModel.inputProducts[i].taskgroupdetail);

//                        $scope.productionTaskModel.inputProducts[i].taskgroupdetail = {
//                            id: $scope.productionTaskModel.productionTaskInfo.detail[i].id,
//                            operation: $scope.productionTaskModel.productionTaskInfo.detail[i].task_group_detail_name,
//                            isNew: false
//                        };
                    }
                }
                if ($scope.productionTaskModel.productionTaskInfo.proByProduct != undefined && $scope.productionTaskModel.productionTaskInfo.proByProduct.length > 0)
                {
                    $scope.productionTaskModel.byProducts = [];
                    for (var i = 0; i < $scope.productionTaskModel.productionTaskInfo.proByProduct.length; i++)
                    {
                        var inputtask = {};
                        inputtask.id = '';
                        inputtask.input_pdt_id = '';
                        inputtask.name = '';
                        inputtask.byProductiontask = '';
                        inputtask.productiontask = {};
                        $scope.productionTaskModel.byProducts.push(inputtask);
                    }
                    for (var i = 0; i < $scope.productionTaskModel.productionTaskInfo.proByProduct.length; i++)
                    {
                        $scope.productionTaskModel.byProducts[i].id = $scope.productionTaskModel.productionTaskInfo.proByProduct[i].id;
                        $scope.productionTaskModel.byProducts[i].input_pdt_id = $scope.productionTaskModel.productionTaskInfo.proByProduct[i].product_id;
                        $scope.productionTaskModel.byProducts[i].name = $scope.productionTaskModel.productionTaskInfo.proByProduct[i].product_name;
                        $scope.productionTaskModel.byProducts[i].input_qty = $scope.productionTaskModel.productionTaskInfo.proByProduct[i].qty;
                        $scope.productionTaskModel.byProducts[i].uom_id = $scope.productionTaskModel.productionTaskInfo.proByProduct[i].uom_id;
                        $scope.productionTaskModel.byProducts[i].isNew = false;
                        $scope.productionTaskModel.byProducts[i].productiontask = {
                            id: $scope.productionTaskModel.productionTaskInfo.proByProduct[i].id,
                            input_pdt_id: $scope.productionTaskModel.productionTaskInfo.proByProduct[i].input_pdt_id,
                            name: $scope.productionTaskModel.productionTaskInfo.proByProduct[i].name,
                            input_qty: $scope.productionTaskModel.productionTaskInfo.proByProduct[i].input_qty,
                            uom_id: $scope.productionTaskModel.productionTaskInfo.proByProduct[i].uom_id,
                            isNew: false
                        };
                      }
                }
            }
        };

        $scope.editProductionTask = function () {

            if ($scope.productionTaskModel.inputProducts.length > 0)
            {
                $scope.isDataSavingProcess = true;
                var createProdTaskParam = {};
                var headers = {};
                headers['screen-code'] = 'billofmaterial';
                createProdTaskParam.id = $stateParams.id;
                createProdTaskParam.name = $scope.productionTaskModel.taskname;
                createProdTaskParam.bom_code = $scope.productionTaskModel.taskcode;
                createProdTaskParam.is_active = $scope.productionTaskModel.isactive == true ? 1 : 0;
                if ($scope.productionTaskModel.parentTask != undefined && $scope.productionTaskModel.parentTask.id != '')
                {
                    createProdTaskParam.prev_bom_id = $scope.productionTaskModel.parentTask.id;
                } else
                {
                    createProdTaskParam.prev_bom_id = '';
                }
                createProdTaskParam.output_pdt_id = $scope.productionTaskModel.outputProducts.id;
                createProdTaskParam.product_for_name = $scope.productionTaskModel.productFor.name;
                createProdTaskParam.product_for_id = $scope.productionTaskModel.productFor.id;
                createProdTaskParam.unit_production_price = $scope.productionTaskModel.outputupp;
            createProdTaskParam.task_group_id = $scope.productionTaskModel.taskgroup;
                createProdTaskParam.comments = '';
                createProdTaskParam.detail = [];
                for (var i = 0; i < $scope.productionTaskModel.inputProducts.length; i++)
                {
                    var input = {};
                    input.id = $scope.productionTaskModel.inputProducts[i].id;
                    input.name = $scope.productionTaskModel.inputProducts[i].name;
                    input.input_pdt_id = $scope.productionTaskModel.inputProducts[i].input_pdt_id;
                    input.input_qty = $scope.productionTaskModel.inputProducts[i].input_qty;
                    input.task_group_detail_id = 0;
                    createProdTaskParam.detail.push(input);
                }
                createProdTaskParam.productBom = [];
                for (var i = 0; i < $scope.productionTaskModel.byProducts.length; i++)
                {
                    var input = {};
                    input.product_id = $scope.productionTaskModel.byProducts[i].input_pdt_id;
                    input.qty = $scope.productionTaskModel.byProducts[i].input_qty;
                    input.uom_id = $scope.productionTaskModel.byProducts[i].uom_id;
                    createProdTaskParam.productBom.push(input);
                }
                adminService.editProductionTask(createProdTaskParam, $stateParams.id, headers).then(function (response) {

                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.billofmaterial')
                    }
                    $scope.isDataSavingProcess = false;

                });

            } else
            {
                sweet.show('Oops...', 'Select a Input Product...', 'error');
            }
        };

        $scope.getProdTaskDetail = function ()
        {
            $scope.productionTaskModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'billofmaterial';

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            getListParam.id = $stateParams.id;
            adminService.getProdTaskDetail(getListParam, configOption, headers).then(function (response)
            {
                var data = response.data;
                if (data.list.length > 0)
                {
                    $scope.productionTaskModel.productionTaskInfo = data.list[0];

                    $scope.updateTaskInfo();

                } else
                {
                    $scope.productionTaskModel.isLoadingProgress = false;
                }
            });
        };
    $scope.getTaskGroupListAll = function ()
    {
        $scope.isLoadedGroupList = false;
        $scope.productionTaskModel.isLoadingProgress = true;
        var getParam = {};
        getParam.is_active = 1;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getTaskGroupList(getParam, configOption).then(function (response) {
            if(response.data.success == true)
            {    
                var data = response.data;
                $scope.productionTaskModel.taskGroupList = data.list;
                $scope.isLoadedGroupList = true;
            }
            $scope.productionTaskModel.isLoadingProgress = false;
        });
    };
        $scope.getTaskGroupDetailList = function ()
        {
        for (var i = 0; i < $scope.productionTaskModel.inputProducts.length; i++)
        {
            $scope.productionTaskModel.inputProducts[i].taskgroupdetail = '';
        }
            $scope.productionTaskModel.isLoadingProgress = true;
            var getParam = {};
        getParam.id = $scope.productionTaskModel.taskgroup;
            getParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaskGroupList(getParam, configOption).then(function (response) {
            if(response.data.success == true)
            {    
                var data = response.data;
                $scope.productionTaskModel.operationList = data.list[0].detail;
            }
                $scope.productionTaskModel.isLoadingProgress = false;
            });
        };
        $scope.formatTaskGroupModel = function (model, index)
        {
            if (model != null)
            {
                $scope.updateTaskGroupInfo(model, index);
                if (model.isNew == false)
                {
                    return model.taskgroup;
                } else {
                    return model.operation;
                }
            }
            return  '';
        };
        $scope.updateTaskGroupInfo = function (model, index)
        {
            if (typeof model == 'undefined')
                return;
            if (model.id != null && model.id != '')
            {
                if (model.isNew == false)
                {
                    $scope.productionTaskModel.inputProducts[index].taskgroupdetail.taskgroup = model.taskgroup;
                    $scope.productionTaskModel.inputProducts[index].taskgroupdetail.id = model.taskgroup_id;
                } else {
                    $scope.productionTaskModel.inputProducts[index].taskgroupdetail.taskgroup = model.operation;
                    $scope.productionTaskModel.inputProducts[index].taskgroupdetail.id = model.id;
                }
            }
            return;
        };
        $scope.tabChange(1);
        $scope.getTaskGroupListAll();
        $scope.getTaskList();
        $scope.getProdTaskDetail();
    }]);




