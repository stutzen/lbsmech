
app.controller('paymentPrintCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST) {

        $scope.paymentListModel =
                {
                    paymentInfo: [],
                    customerInfo: {}
                }

        $scope.currentDate = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.order_view_form != 'undefined' && typeof $scope.order_view_form.$pristine != 'undefined' && !$scope.order_view_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.print = function()
        {
            window.print();
        }

        $scope.updateRoundoff = function()
        {
            $scope.round = Math.round($scope.paymentListModel.paymentInfo[0].amount);
            $scope.paymentListModel.paymentInfo[0].roundoff = $scope.round - parseFloat($scope.paymentListModel.paymentInfo[0].amount);
            $scope.paymentListModel.paymentInfo[0].roundoff = parseFloat($scope.paymentListModel.paymentInfo[0].roundoff).toFixed(2);
            $scope.round = parseFloat($scope.round).toFixed(2);
            $scope.paymentListModel.paymentInfo[0].amountinwords = utilityService.convertNumberToWords($scope.paymentListModel.paymentInfo[0].amount);

        }

        $scope.getCustomerInfo = function()
        {
            var getListParam = {};
            getListParam.id = $scope.paymentListModel.paymentInfo.customer_id;
            getListParam.is_sale = 1;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCustomersList(getListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.paymentListModel.customerInfo = data[0];
            });
        };

        $scope.getInvoicePaymentList = function( )
        {
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;

            adminService.getPaymentList(getListParam).then(function(response)
            {
                var data = response.data.list;
                $scope.paymentListModel.paymentInfo = data;
                $scope.paymentListModel.paymentInfo[0].newdate = utilityService.parseStrToDate($scope.paymentListModel.paymentInfo[0].date);
                $scope.paymentListModel.paymentInfo[0].date = utilityService.parseDateToStr($scope.paymentListModel.paymentInfo[0].newdate, $scope.dateFormat);
                $scope.getCustomerInfo();
                $scope.updateRoundoff();
            });
        }
        $scope.getInvoicePaymentList( );


    }]);