

app.controller('incomecategoryEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.incomecategoryAddModel = {
            id: '',
            name: "",
            comments: '',
            isActive: true,
            isLoadingProgress: false

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.category_form != 'undefined' && typeof $scope.category_form.$pristine != 'undefined' && !$scope.category_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.category_form.$setPristine();
            $scope.updateCategoryInfo();

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.updateCategoryInfo = function()
        {
            if ($scope.incomeCategoryDetail != null)
            {
                $scope.incomecategoryAddModel.name = $scope.incomeCategoryDetail.name;
                $scope.incomecategoryAddModel.comments = $scope.incomeCategoryDetail.comment;
            }
            $scope.incomecategoryAddModel.isLoadingProgress = false;
        }
        $scope.updateIncomeCategory = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'incomecategory';
                createParam.seq_no = '';
                createParam.name = $scope.incomecategoryAddModel.name;
                createParam.comment = $scope.incomecategoryAddModel.comments;
                createParam.type = 'income';

                adminService.editIncomeCategory(createParam, $stateParams.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.incomecategory');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            getListParam.type = 'income';

            $scope.incomecategoryAddModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getIncomeCategoryList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.incomeCategoryDetail = data.list[0];
                    $scope.updateCategoryInfo();
                }

            });

        };


        $scope.getList();

    }]);




