




app.controller('accountEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.accountModel = {
            "account": "",
            "description": "",
            "balance": "",
            "contact_person": "",
            "contact_phone": "",
            "account_number": "",
            "website": "",
            "bank_name": "",
            "currency": "",
            "branch": "",
            "address": "",
            "notes": "",
            "sno": "",
            "token": "",
            "created_by": "",
            "updated_by": "",
            "created_at": "",
            "updated_at": "",
            "status": "",
            "isActive": true,
            isLoadingProgress: false,
            "acode": ''


        };

        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.account_add_form !== 'undefined' && typeof $scope.account_add_form.$pristine !== 'undefined' && !$scope.account_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            if (typeof $scope.account_edit_form !== 'undefined')
            {
                $scope.account_edit_form.$setPristine();
                $scope.updateAccountInfo();
            }

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTabletCreate = function() {

            $scope.accountModel.user = '';

        };
        $scope.modifyAccount = function() {

            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var modifyAccountParam = {};
                var headers = {};
                headers['screen-code'] = 'account';
                modifyAccountParam.account = $scope.accountModel.account;
                modifyAccountParam.description = $scope.accountModel.description;
                modifyAccountParam.balance = $scope.accountModel.balance;
                modifyAccountParam.contact_person = $scope.accountModel.contact_person;
                modifyAccountParam.contact_phone = $scope.accountModel.contact_phone;
                modifyAccountParam.account_number = $scope.accountModel.account_number;
                modifyAccountParam.website = $scope.accountModel.website;
                modifyAccountParam.bank_name = '';
                modifyAccountParam.currency = '';
                modifyAccountParam.branch = '';
                modifyAccountParam.address = '';
                modifyAccountParam.notes = '';
                modifyAccountParam.sno = '';
                modifyAccountParam.token = '';
                modifyAccountParam.created_by = '';
                modifyAccountParam.updated_by = '';
                modifyAccountParam.created_at = '';
                modifyAccountParam.updated_at = '';
                modifyAccountParam.isactive = 1;
                modifyAccountParam.acode = '';
                if (typeof $scope.accountModel.acode != undefined && $scope.accountModel.acode != null && $scope.accountModel.acode != '')
                {
                    modifyAccountParam.acode = $scope.accountModel.acode;
                }
                adminService.modifyAccount(modifyAccountParam, $scope.accountModel.id, headers).then(function(response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.account');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.updateAccountInfo = function()
        {
            if ($scope.accountModel.list !== null)
            {
                $scope.accountModel.account = $scope.accountModel.list.account;
                $scope.accountModel.description = $scope.accountModel.list.description;
                $scope.accountModel.balance = $scope.accountModel.list.balance;
                $scope.accountModel.account_number = $scope.accountModel.list.account_number;
                $scope.accountModel.contact_person = $scope.accountModel.list.contact_person;
                $scope.accountModel.contact_phone = $scope.accountModel.list.contact_phone;
                $scope.accountModel.website = $scope.accountModel.list.website;
                $scope.accountModel.bank_name = '';
                $scope.accountModel.currency = '';
                $scope.accountModel.address = '';
                $scope.accountModel.branch = '';
                $scope.accountModel.token = '';
                $scope.accountModel.sno = '';
                $scope.accountModel.notes = '';
                $scope.accountModel.created_by = '';
                $scope.accountModel.updated_by = '';
                $scope.accountModel.created_at = '';
                $scope.accountModel.updated_at = '';
                $scope.accountModel.acode =  $scope.accountModel.list.acode;                      
                $scope.accountModel.id = $scope.accountModel.list.id;

            }
            $scope.accountModel.isLoadingProgress = false;
        };



        $scope.getAccountInfo = function() {
            if (typeof $stateParams.id !== 'undefined')
            {
                $scope.accountModel.isLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getAccountlist(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.accountModel.list = data.list[0];
                        $scope.updateAccountInfo();
                    }

                });
            }
        };

        $scope.getAccountInfo();



    }]);




