app.controller('paymenttermsEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', '$stateParams', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $stateParams) {

        $scope.paymenttermsAddModel = {
            "id": 0,
            "name": "",
            "comments": "",
            "is_active": '',
            "is_default":'',
            currentPage: 1,
            total: 0,
            isLoadingProgress: false,
            paymentDetail: {}
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_paymentterms_form != 'undefined' && typeof $scope.create_paymentterms_form.$pristine != 'undefined' && !$scope.create_paymentterms_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function()
        {
            $scope.create_paymentterms_form.$setPristine();
             $scope.paymenttermsInfo();
        }

        $scope.modifypaymentterms = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var modifypaymenttermsParam = {};
                var headers = {};
                headers['screen-code'] = 'paymentterms';
                modifypaymenttermsParam.name = $scope.paymenttermsAddModel.name;
                modifypaymenttermsParam.comments = $scope.paymenttermsAddModel.comments;
               modifypaymenttermsParam.is_default = $scope.paymenttermsAddModel.is_default == true ? 1:0;
                modifypaymenttermsParam.is_active = 1;

                adminService.modifypaymentterms(modifypaymenttermsParam, $stateParams.id, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.paymentterms')
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.paymenttermsInfo = function()
        {
            if ($scope.paymenttermsAddModel.paymenttermsDetail != null)
            {
                $scope.paymenttermsAddModel.name = $scope.paymenttermsAddModel.paymenttermsDetail.name;
                $scope.paymenttermsAddModel.comments = $scope.paymenttermsAddModel.paymenttermsDetail.comments;
                 $scope.paymenttermsAddModel.is_default = $scope.paymenttermsAddModel.paymenttermsDetail.is_default == 1 ? true:false;
                //createPaymenttermsParam.rolename = $scope.paymenttermsAddModel.rolename;            
                $scope.paymenttermsAddModel.is_active = 1;
            }
        }

        $scope.getList = function()
        {
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getpaymenttermslist(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.paymenttermsAddModel.paymenttermsDetail = data.list[0];
                    $scope.paymenttermsInfo();
                }
            });
        };

        $scope.getList();


    }]);




