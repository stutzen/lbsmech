

app.controller('productAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$window', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $window) {

        $scope.productModel = {
            id: '',
            name: '',
            sku: '',
            description: '',
            uomList: [],
            uomName: "",
            salesPrice: '',
            category_id: '',
            isActive: true,
            isPurchase: true,
            isSale: true,
            hasinventory: false,
            minstock: '',
            openstock: '',
            hsncode: '',
            mrp_price: '',
            attachment: [],
            type: '',
            width: '',
            thickness: '',
            diameter: '',
            af: '',
            square: '',
            prodlength: '',
            color: '',
            gauge: '',
            brand: '',
            pitch: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.product_add_form != 'undefined' && typeof $scope.product_add_form.$pristine != 'undefined' && !$scope.product_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.product_add_form.$setPristine();
            if (typeof $scope.product_add_form != 'undefined')
            {
                $scope.productModel.name = "";
                $scope.productModel.sku = "";
                $scope.productModel.salesPrice = '';
                $scope.productModel.description = "";
                $scope.productModel.uomName = "";
                $scope.productModel.isActive = true;
                $scope.productModel.isSale = true;
                $scope.productModel.isPurchase = true;
                $scope.productModel.hasinventory = '';
                $scope.productModel.minstock = '';
                $scope.productModel.id = '';
                $scope.productModel.openstock = '';
                $scope.productModel.hsncode = '';
                $scope.productModel.mrp_price = '';
                $scope.productModel.type = '';
                $scope.productModel.width = '';
                $scope.productModel.thickness = '';
                $scope.productModel.diameter = '';
                $scope.productModel.af = '';
                $scope.productModel.square = '';
                $scope.productModel.prodlength = '';
                $scope.productModel.color = '';
                $scope.productModel.gauge = '';
                $scope.productModel.brand = '';
                $scope.productModel.pitch = '';
            }
        }
        $scope.getcategoryList = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.category_id = "";

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.productModel.categorylist = data.list;

            });
        };
        $scope.getProductTypeList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'producttype';
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getProductTypeList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.productModel.productTypeList = data;
                }
            });

        };
        $scope.getCUOMNameList = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = ($scope.productModel.currentPage - 1) * $scope.productModel.limit;
            getListParam.limit = $scope.productModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUom(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.productModel.uomList = data.list;
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.ShowAlert = function() {
            if ($scope.productModel.salesPrice > $scope.productModel.mrp_price)
            {
                $window.alert("Sales Price must be less than MRP Price");

                $scope.productModel.salesPrice = '';
                $scope.isDataSavingProcess = false;
            }
        }
        $scope.getCUOMNameList( );
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        /*
         *  It used to format the speciality model value in UI
         */
        $scope.addProductInfo = function() {

                if ($scope.isDataSavingProcess)
                {
                    return;
                }

                $scope.isDataSavingProcess = true;
                var addProductParam = {};
                var detailProductParam = {};
                addProductParam.detail = [];
                var headers = {};
                headers['screen-code'] = 'product';
                // addProductParam.id = 0;
                addProductParam.name = $scope.productModel.name;
                addProductParam.sku = $scope.productModel.sku;
                addProductParam.sales_price = parseFloat($scope.productModel.salesPrice);
                addProductParam.mrp_price = parseFloat($scope.productModel.mrp_price);
                addProductParam.description = $scope.productModel.description;
                addProductParam.category_id = $scope.productModel.id;
                addProductParam.attachment = [];
                addProductParam.type = "Product";
                // addProductParam.uom = $scope.productModel.uomName;
                if ($scope.productModel.uomName != "")
                {
                    for (var loopIndex = 0; loopIndex < $scope.productModel.uomList.length; loopIndex++)
                    {
                        if ($scope.productModel.uomName == $scope.productModel.uomList[loopIndex].name)
                        {
                            addProductParam.uom_id = $scope.productModel.uomList[loopIndex].id;
                            break;
                        }
                    }
                }
                else
                {
                    addProductParam.uom_id = "";
                }
                addProductParam.is_active = 1;
                //addProductParam.is_active = $scope.productModel.isActive == true ? 1 : 0;
                addProductParam.is_purchase = $scope.productModel.isPurchase == true ? 1 : 0;
                addProductParam.is_sale = $scope.productModel.isSale == true ? 1 : 0;
                addProductParam.min_stock_qty = $scope.productModel.minstock;
                addProductParam.opening_stock = $scope.productModel.openstock;
                addProductParam.has_inventory = $scope.productModel.hasinventory == true ? 1 : 0;
                addProductParam.hsn_code = $scope.productModel.hsncode;
                detailProductParam.type = $scope.productModel.type;
                detailProductParam.width = $scope.productModel.width;
                detailProductParam.thickners = $scope.productModel.thickness;
                detailProductParam.dia = $scope.productModel.diameter;
                detailProductParam.af = $scope.productModel.af;
                detailProductParam.square = $scope.productModel.square;
                detailProductParam.length = $scope.productModel.prodlength;
                detailProductParam.color = $scope.productModel.color;
                detailProductParam.gauge = $scope.productModel.gauge;
                detailProductParam.brand = $scope.productModel.brand;
                detailProductParam.pitch = $scope.productModel.pitch;
                addProductParam.detail.push(detailProductParam);
                adminService.addProduct(addProductParam, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.productList');
                    }
                    $scope.isDataSavingProcess = false;
                });
        };
        $scope.getcategoryList();
        $scope.getProductTypeList();
    }]);
