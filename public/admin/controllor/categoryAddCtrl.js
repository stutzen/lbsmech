

app.controller('categoryAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $scope.categoryAddModel = {
            id: '',
            name: "",
            description: '',
            isActive: true

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.category_form != 'undefined' && typeof $scope.category_form.$pristine != 'undefined' && !$scope.category_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {


            $scope.categoryAddModel.name = '';
            $scope.categoryAddModel.description = '';
            $scope.category_form.$setPristine();

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.createCategory = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
            var createParam = {};
            var headers = {};
            headers['screen-code'] = 'category';
            createParam.id = 0;
            createParam.name = $scope.categoryAddModel.name;
            createParam.description = $scope.categoryAddModel.description;
            createParam.comments = "";
            createParam.isActive = 1;

            adminService.createCategory(createParam, adminService.handleSuccessAndErrorResponse, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.categorylist');
                }
                $scope.isDataSavingProcess = false;
            });
        }
        };

    }]);




