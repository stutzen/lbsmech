


app.controller('customeraniReportCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'utilityService', 'APP_CONST', 'Auth', '$state', '$timeout', '$window', 'ValidationFactory', 'sweet', function($scope, $rootScope, adminService, $filter, utilityService, APP_CONST, Auth, $state, $timeout, $window, ValidationFactory, sweet) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.customeranniReportModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            dateList: [],
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            isSmsLoadingProgress: false,
            appSettList: []
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [10, 20, 50, 100];
        $scope.customeranniReportModel.limit = $scope.pagePerCount[2];
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.adminService = adminService;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.searchFilter = {
            from_date: '',
            to_date: '',
            type: 'dob'
        }
        $scope.enableSendSms = true;
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                from_date: '',
                to_date: '',
                type: 'dob'
            };
            $scope.enableSendSms = true;
            $scope.customeranniReportModel.list = [];
        }
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        };
        $scope.print = function()
        {
            $window.print();
        }
        $scope.checkDateEvent = function()
        {
            if ($scope.searchFilter.type == 'dob' || $scope.searchFilter.type == 'customer_special_date1')
            {
                $scope.enableSendSms = true;
            } else
            {
                $scope.enableSendSms = false;
            }
            $scope.customeranniReportModel.list = [];
        }
        $scope.selectAll = false;
        $scope.checkAllCustomer = function()
        {
            for (var i = 0; i < $scope.customeranniReportModel.list.length; i++)
            {
                if ($scope.selectAll == true)
                {
                    $scope.customeranniReportModel.list[i].sendSms = true;
                } else
                {
                    $scope.customeranniReportModel.list[i].sendSms = false;
                }
            }
        };

        $scope.sendSms = function() {
            if ($scope.customeranniReportModel.isSmsLoadingProgress == false)
            {
                $scope.customeranniReportModel.isSmsLoadingProgress = true;
                var isSmsSend = 0;
                if ($scope.customeranniReportModel.list.length > 0)
                {
                    for (var i = 0; i < $scope.customeranniReportModel.list.length; i++)
                    {
                        if ($scope.customeranniReportModel.list[i].sendSms == true)
                        {
                            isSmsSend = isSmsSend + 1;
                        }
                    }
                }
                if (isSmsSend == 0)
                {
                    sweet.show('Oops...', 'Select Customer', 'error');
                    $scope.customeranniReportModel.isSmsLoadingProgress = false;
                } else
                {
                    var sendSmsParam = [];
                    var headers = {};
                    headers['screen-code'] = 'anniversaryreport';
                    for (var i = 0; i < $scope.customeranniReportModel.list.length; i++)
                    {
                        if ($scope.customeranniReportModel.list[i].sendSms == true)
                        {
                            var customerId = {};
                            customerId.id = $scope.customeranniReportModel.list[i].id;
                            sendSmsParam.push(customerId);
                        }
                    }
                    //sendSmsParam.type = $scope.searchFilter.type;
                    adminService.sendSms(sendSmsParam, $scope.searchFilter.type, headers).then(function(response) {
                        var data = response.data;
                        if (data.success == true)
                        {
                            $scope.selectAll = false;
                            $scope.checkAllCustomer();
                        }
                        $scope.customeranniReportModel.isSmsLoadingProgress = false;
                    });
                }
            }

        };
        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'anniversaryreport';
            getListParam.from_date = $filter('date')($scope.searchFilter.from_date, 'yyyy-MM-dd');
            getListParam.to_date = $filter('date')($scope.searchFilter.to_date, 'yyyy-MM-dd');
            getListParam.type = $scope.searchFilter.type;
            $scope.customeranniReportModel.isLoadingProgress = true;
            $scope.customeranniReportModel.isSearchLoadingProgress = true;
            adminService.getCustomerAnniversaryReport(getListParam, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.customeranniReportModel.list = data;
                    $scope.customeranniReportModel.total = data.total;
                    $scope.customeranniReportModel.isLoadingProgress = false;
                    $scope.customeranniReportModel.isSearchLoadingProgress = false;
                }


            });
        };

        $scope.getappSettingsInfo = function()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'customersettings';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getAppSettingsList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.customeranniReportModel.appSettList = data.list;
                        $scope.updateSettingsInfo();
                    }
                }
            });

        };

        $scope.updateSettingsInfo = function()
        {
            var loop;

            for (loop = 0; loop < $scope.customeranniReportModel.appSettList.length; loop++)
            {
                if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date1")
                {
                    $rootScope.appConfig.customer_special_date1 = $scope.customeranniReportModel.appSettList[loop].value == 1 ? true : false;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date2")
                {
                    $rootScope.appConfig.customer_special_date2 = $scope.customeranniReportModel.appSettList[loop].value == 1 ? true : false;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date3")
                {
                    $rootScope.appConfig.customer_special_date3 = $scope.customeranniReportModel.appSettList[loop].value == 1 ? true : false;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date4")
                {
                    $rootScope.appConfig.customer_special_date4 = $scope.customeranniReportModel.appSettList[loop].value == 1 ? true : false;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date5")
                {
                    $rootScope.appConfig.customer_special_date5 = $scope.customeranniReportModel.appSettList[loop].value == 1 ? true : false;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date1_label")
                {
                    $rootScope.appConfig.special_date1_label = $scope.customeranniReportModel.appSettList[loop].value;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date2_label")
                {
                    $rootScope.appConfig.special_date2_label = $scope.customeranniReportModel.appSettList[loop].value;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date3_label")
                {
                    $rootScope.appConfig.special_date3_label = $scope.customeranniReportModel.appSettList[loop].value;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date4_label")
                {
                    $rootScope.appConfig.special_date4_label = $scope.customeranniReportModel.appSettList[loop].value;
                }
                else if ($scope.customeranniReportModel.appSettList[loop].setting == "customer_special_date5_label")
                {
                    $rootScope.appConfig.special_date5_label = $scope.customeranniReportModel.appSettList[loop].value;
                }

            }
            $scope.getDateEvent();
        }

        $scope.getDateEvent = function()
        {
            var dateEvent = [];
            var dobDate = {};
            dobDate.type = 'dob';
            dobDate.label = 'Birthday';
            dateEvent.push(dobDate);
            if ($rootScope.appConfig.customer_special_date1 == true)
            {
                var eventDate = {};
                eventDate.type = 'customer_special_date1';
                eventDate.label = $rootScope.appConfig.customer_special_date1_label;
                dateEvent.push(eventDate);
            }
            if ($rootScope.appConfig.customer_special_date2 == true)
            {
                var eventDate = {};
                eventDate.type = 'customer_special_date2';
                eventDate.label = $rootScope.appConfig.customer_special_date2_label;
                dateEvent.push(eventDate);
            }
            if ($rootScope.appConfig.customer_special_date3 == true)
            {
                var eventDate = {};
                eventDate.type = 'customer_special_date3';
                eventDate.label = $rootScope.appConfig.customer_special_date3_label;
                dateEvent.push(eventDate);
            }
            if ($rootScope.appConfig.customer_special_date4 == true)
            {
                var eventDate = {};
                eventDate.type = 'customer_special_date4';
                eventDate.label = $rootScope.appConfig.customer_special_date4_label;
                dateEvent.push(eventDate);
            }
            if ($rootScope.appConfig.customer_special_date5 == true)
            {
                var eventDate = {};
                eventDate.type = 'customer_special_date5';
                eventDate.label = $rootScope.appConfig.customer_special_date5_label;
                dateEvent.push(eventDate);
            }
            $scope.customeranniReportModel.dateList = dateEvent;
        };

        $scope.getappSettingsInfo();
        // $scope.getList();
    }]);











