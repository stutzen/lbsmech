

app.controller('acodeEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.acodeModel = {
            id: '',
            name: '',
            isActive: true,
            list: {},
            code: ''
         
        }

        $scope.validationFactory = ValidationFactory;
       


        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_acode_form != 'undefined' && typeof $scope.add_acode_form.$pristine != 'undefined' && !$scope.add_acode_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_acode_form.$setPristine();
            if (typeof $scope.add_acode_form != 'undefined')
            {
                $scope.acodeModel.name = "";
                $scope.acodeModel.isActive = true;
                $scope.acodeModel.code="";
            }
            $scope.updateAcodeInfo();
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.modifyacode = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addAcodeParam = {};
            var headers = {};
            headers['screen-code'] = 'acodesettings';
            addAcodeParam.id = $stateParams.id;
            addAcodeParam.name = $scope.acodeModel.name;
            addAcodeParam.code = $scope.acodeModel.code;
            addAcodeParam.is_active = $scope.acodeModel.isActive;

            adminService.editAcode(addAcodeParam, $scope.acodeModel.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.acodesettings');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.updateAcodeInfo = function()
        {
            $scope.acodeModel.isLoadingProgress = false;
            $scope.acodeModel.id = $scope.acodeModel.list.id;
            $scope.acodeModel.code = $scope.acodeModel.list.code;
            $scope.acodeModel.name = $scope.acodeModel.list.name;
        }
        $scope.getAcodeInfo = function()
        {
            $scope.acodeModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var acodeListParam = {};
                acodeListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getAcodeList(acodeListParam, configOption).then(function(response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.acodeModel.list = data.list[0];

                    }
                    $scope.updateAcodeInfo();

                });
            }
        };
        $scope.getAcodeInfo();
    }]);



