

app.controller('deductionEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.deductionModel = {
            id: '',
            date: '',
            adv_amount: '',            
            emp_id: '',
            emp_code: '',
            emp_name: '',
            deduction_amt: '',
            balance_amt: '',
            is_active: '',
            total_deduction_amt: '',
            advance_amt_id: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.deduction_edit_form != 'undefined' && typeof $scope.deduction_edit_form.$pristine != 'undefined' && !$scope.deduction_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.deduction_edit_form.$setPristine();
            if (typeof $scope.deduction_edit_form != 'undefined')
            {
                $scope.deductionModel.id = "";
                $scope.deductionModel.date = "";
                $scope.deductionModel.adv_amount = '';                
                $scope.deductionModel.emp_id = '';
                $scope.deductionModel.emp_code = '';
                $scope.deductionModel.emp_name = '';
                $scope.deductionModel.balance_amt = "";
                $scope.deductionModel.deduction_amt ='';
                $scope.deductionModel.total_deduction_amt ='';
                $scope.deductionModel.advance_amt_id = '';
            }
            $scope.updateDeductionInfo();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.adminService = adminService;
        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            $scope.initUpdateDetailTimeoutPromise = $timeout($scope.updateDeductionInfo, 300);
        }
        
        $scope.currentDate = new Date();

        $scope.deductionDateOpen = false;        
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.deductionDateOpen = true;
            }            
        }        

        $scope.updateDeductionInfo = function ()
        {
            $scope.deductionModel.is_active = $scope.deductionModel.deductionDetail.is_active;
            $scope.deductionModel.id = $scope.deductionModel.deductionDetail.id;
            $scope.deductionModel.advance_amt_id = $scope.deductionModel.deductionDetail.advance_amt_id;
            $scope.deductionModel.date = new Date($scope.deductionModel.deductionDetail.date);
            $scope.deductionModel.adv_amount = $scope.deductionModel.deductionDetail.adv_amount;
            $scope.deductionModel.balance_amt = $scope.deductionModel.deductionDetail.balance_amt;
            $scope.deductionModel.deduction_amt = $scope.deductionModel.deductionDetail.deduction_amt;
            $scope.deductionModel.total_deduction_amt = $scope.deductionModel.deductionDetail.total_deduction_amt;                  
            $scope.deductionModel.emp_id = $scope.deductionModel.deductionDetail.emp_id;
            $scope.deductionModel.emp_name = $scope.deductionModel.deductionDetail.f_name;
            $scope.deductionModel.emp_code = $scope.deductionModel.deductionDetail.emp_code;
            $scope.deductionModel.isLoadingProgress = false;
        }                        

        $scope.getDeductionInfo = function () {

            $scope.deductionModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var advanceListParam = {};
                advanceListParam.id = $stateParams.id;

                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getDeductionList(advanceListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.deductionModel.deductionDetail = data.list[0];
                    }
                    $scope.initUpdateDetail();
                });
            }
        };

        $scope.editDeduction = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var modifyAdvanceParam = {};
            var headers = {};
            headers['screen-code'] = 'advancededuction';
             modifyAdvanceParam.advance_amt_id = $scope.deductionModel.advance_amt_id;
            modifyAdvanceParam.emp_id = $scope.deductionModel.emp_id;
            modifyAdvanceParam.emp_code = $scope.deductionModel.emp_code;
            modifyAdvanceParam.date = $filter('date')($scope.deductionModel.date, 'yyyy-MM-dd');            
            modifyAdvanceParam.deduction_amt = $scope.deductionModel.deduction_amt;            
            modifyAdvanceParam.is_active = $scope.deductionModel.is_active;            
            
            adminService.editDeduction(modifyAdvanceParam, $stateParams.id, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.deductionList');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getDeductionInfo();
    }]);



