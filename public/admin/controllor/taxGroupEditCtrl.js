

app.controller('taxGroupEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, utilityService, $httpService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.taxGroupAddModel = {
            "id": "",
            "tax_no": "",
            "tax_name": "",
            "tax_percentage": "",
            "tax_applicable_amt": "",
            "start_date": "",
            "end_date": "",
            "comments": "",
            isLoadingProgress: false,
            "tax_type": '',
            "tax": '',
            "isdisplay": true

        };
        $scope.validationFactory = ValidationFactory;
        //$scope.taxlistRoleList = ['ADMIN', 'OWNER', 'WORKFORCE'];
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.taxlist_edit_form != 'undefined' && typeof $scope.taxlist_edit_form.$pristine != 'undefined' && !$scope.taxlist_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $scope.isDataSavingProcess = false;

        $scope.startDateOpen = false;
        $scope.endDateOpen = false;
        $scope.openDate = function(index)
        {
            if (index == 1)
            {
                $scope.startDateOpen = true;
            }
            if (index == 2)
            {
                $scope.endDateOpen = true;
            }
        }

        $scope.formReset = function() {
            if (typeof $scope.taxlist_edit_form != 'undefined')
            {
                $scope.taxlist_edit_form.$setPristine();
                $scope.taxGroupAddModel.tax = '';
                $scope.mappedTax = {"belongTo": [$scope.tax], "items": []};
                $scope.getTaxlist();
            }

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.tax = 'tax';
        $scope.mappedTax = {"belongTo": [$scope.tax], "items": []};
        $scope.taxGroupMapping = function()
        {
            if (typeof $scope.taxGroupAddModel.tax != undefined && $scope.taxGroupAddModel.tax != '' && $scope.taxGroupAddModel.tax != null && $scope.taxGroupAddModel.tax.id != '')
            {
                var count = 0;
                if ($scope.mappedTax.items.length > 0)
                {
                    for (var i = 0; i < $scope.mappedTax.items.length; i++)
                    {
                        if ($scope.taxGroupAddModel.tax.id == $scope.mappedTax.items[i].id)
                        {
                            count = count + 1;
                        }
                    }
                }
                if (count > 0)
                {
                    sweet.show('Oops...', 'This Tax is Already Mapped .. ', 'error');
                }
                else
                {
                    $scope.mappedTax.items.push($scope.taxGroupAddModel.tax);
                    angular.forEach($scope.mappedTax.items, function(item) {
                        item.belongTo = $scope.tax;
                    });
                    $scope.taxGroupAddModel.tax = '';
                }
            }
        }

        $scope.removeTaxMapping = function(id)
        {
            var index = -1;
            for (var i = 0; i < $scope.mappedTax.items.length; i++) {
                if (id == $scope.mappedTax.items[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.mappedTax.items.splice(index, 1);
        };
        $scope.getTaxList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.tax_name = val;
            autosearchParam.is_group = 0;
            return $httpService.get(APP_CONST.API.TAX_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatTaxModel = function(model) {

            if (model !== null && model != undefined)
            {

                return model.tax_name;
            }
            return  '';
        };

        $scope.onMoved = function(list) {
            list.items = list.items.filter(function(item) {
                return !item.selected;
            });
        };

        $scope.onDragstart = function(list, event) {
            list.dragging = true;
            if (event.dataTransfer.setDragImage) {
                var img = new Image();
                img.src = '../img/ic_content_copy_black_24dp_2x.png';
                event.dataTransfer.setDragImage(img, 0, 0);
            }
        };

        $scope.onDrop = function(list, items, index) {
            angular.forEach(items, function(item) {
                item.selected = false;
            });
            list.items = list.items.slice(0, index)
                    .concat(items)
                    .concat(list.items.slice(index));
            return true;
        }

        $scope.getSelectedItemsIncluding = function(list, item) {
            item.selected = true;
            return list.items.filter(function(item) {
                return item.selected;

            });
        };

        $scope.updateTaxlistInfo = function()
        {
            $scope.taxGroupAddModel.id = $scope.taxGroupAddModel.list.id;
            $scope.taxGroupAddModel.tax_no = $scope.taxGroupAddModel.list.tax_no;
            $scope.taxGroupAddModel.tax_name = $scope.taxGroupAddModel.list.tax_name;
            $scope.taxGroupAddModel.tax_percentage = $scope.taxGroupAddModel.list.tax_percentage;
            $scope.taxGroupAddModel.tax_applicable_amt = $scope.taxGroupAddModel.list.tax_applicable_amt;

            $scope.taxGroupAddModel.start_date = null;
            $scope.taxGroupAddModel.end_date = null;
            if ($scope.taxGroupAddModel.list.start_date != null)
            {
                $scope.taxGroupAddModel.start_date = new Date($scope.taxGroupAddModel.list.start_date);
            }
            if ($scope.taxGroupAddModel.list.end_date != null)
            {
                $scope.taxGroupAddModel.end_date = new Date($scope.taxGroupAddModel.list.end_date);
            }
            $scope.taxGroupAddModel.isdisplay = $scope.taxGroupAddModel.list.is_display == 1 ? true : false;
            $scope.taxGroupAddModel.tax_type = $scope.taxGroupAddModel.list.tax_type;
            $scope.taxGroupAddModel.comments = $scope.taxGroupAddModel.list.comments;
            $scope.taxGroupAddModel.isActive = 1;
            $scope.mappedTax.items = $scope.taxGroupAddModel.list.tax_list;
            $scope.taxGroupAddModel.isLoadingProgress = false;

        }

        // $scope.isLoadedRoleList = false;
        $scope.modifyTaxGroup = function() {

            $scope.isDataSavingProcess = true;
            var modifyTaxlistParam = {};
            var headers = {};
            headers['screen-code'] = 'tax';
            modifyTaxlistParam.id = '';
            modifyTaxlistParam.tax_no = $scope.taxGroupAddModel.tax_no;
            modifyTaxlistParam.tax_name = $scope.taxGroupAddModel.tax_name;
            modifyTaxlistParam.tax_percentage = $scope.taxGroupAddModel.tax_percentage;
            modifyTaxlistParam.tax_applicable_amt = $scope.taxGroupAddModel.tax_applicable_amt;
            modifyTaxlistParam.is_group = 1;
            modifyTaxlistParam.tax_type = $scope.taxGroupAddModel.tax_type;
            modifyTaxlistParam.start_date = $scope.taxGroupAddModel.start_date;
            modifyTaxlistParam.end_date = $scope.taxGroupAddModel.end_date;
            modifyTaxlistParam.comments = $scope.taxGroupAddModel.comments;
            modifyTaxlistParam.isactive = 1;
            modifyTaxlistParam.group_tax_ids = '';
            var groupTax = [];
            for (var i = 0; i < $scope.mappedTax.items.length; i++)
            {
                $scope.mappedTax.items[i].id = $scope.mappedTax.items[i].id + '';
                groupTax.push($scope.mappedTax.items[i].id);
            }
            modifyTaxlistParam.group_tax_ids = groupTax.toString();
            modifyTaxlistParam.start_date = null;
            modifyTaxlistParam.end_date = null;
            if ($scope.taxGroupAddModel.start_date != null && $scope.taxGroupAddModel.start_date != "")
            {
                modifyTaxlistParam.start_date = utilityService.parseDateToStr($scope.taxGroupAddModel.start_date, 'yyyy-MM-dd');
            }
            if ($scope.taxGroupAddModel.end_date != null && $scope.taxGroupAddModel.end_date != "")
            {
                modifyTaxlistParam.end_date = utilityService.parseDateToStr($scope.taxGroupAddModel.end_date, 'yyyy-MM-dd');
            }


            modifyTaxlistParam.is_display = $scope.taxGroupAddModel.isdisplay == true ? 1 : 0;


            adminService.editTax(modifyTaxlistParam, $scope.taxGroupAddModel.id, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.taxgroup');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getTaxlist = function() {
            $scope.taxGroupAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_group = 1;
            adminService.getTaxList(getListParam).then(function(response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.taxGroupAddModel.list = data.list[0];
                }
                $scope.taxGroupAddModel.total = data.total;
                $scope.updateTaxlistInfo();

            });
        };

        $scope.getTaxlist = function() {
            $scope.taxGroupAddModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'taxgroup';
            getListParam.id = $stateParams.id;
            //   getListParam.is_group = 1;
            adminService.getTaxGroupDetail(getListParam,headers).then(function(response) {
                var data = response.data.data;
                if (data.length != 0)
                {
                    $scope.taxGroupAddModel.list = data;
                }
               $scope.updateTaxlistInfo();

            });
        };

        $scope.getTaxlist()
    }]);




