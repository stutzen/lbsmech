app.controller('salesCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.salesModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.showDetail = false;
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.salesModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
       // $scope.now = new Date();
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0));       
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.company_name = $rootScope.appConfig.app_tile;
        $scope.company_address = $rootScope.appConfig.pay_to_address;
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            customerInfo: {}

        };
        $scope.searchFilter.fromdate = $scope.currentDate;
       // $scope.searchFilter.fromdate=$filter('date')($scope.currentDate,'dd-MM-yyyy');
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                customerInfo: {}

            };
            $scope.salesModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
            // $scope.searchFilter.fromdate = $filter('date')($scope.currentDate, $scope.dateFormat);
            // $scope.initTableFilter();
        }


        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                {
                    $scope.initTableFilter();
                }
            }
        });

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesreport';
            getListParam.name = '';
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
            if($scope.showDetail)
            {
                getListParam.show_all = 1;
            }
            else
            {
                getListParam.show_all = '';
            }
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;

            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }

            getListParam.is_active = 1;
            getListParam.mode = 1;
            getListParam.from_duedate = '';
            getListParam.to_duedate = '';
            getListParam.discount_persentage = '';
            getListParam.status = '';
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.salesModel.isLoadingProgress = true;
            $scope.salesModel.isSearchLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getInvoiceList(getListParam, configOption, headers).then(function(response)
            {
                var totalAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.salesModel.list = data;
                    for (var i = 0; i < $scope.salesModel.list.length; i++)
                    {
                        $scope.salesModel.list[i].newdate = utilityService.parseStrToDate($scope.salesModel.list[i].date);
                        $scope.salesModel.list[i].date = utilityService.parseDateToStr($scope.salesModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        totalAmt = totalAmt + parseFloat($scope.salesModel.list[i].total_amount);
                        $scope.salesModel.list[i].total_amount = utilityService.changeCurrency($scope.salesModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.salesModel.list[i].paid_amount = utilityService.changeCurrency($scope.salesModel.list[i].paid_amount, $rootScope.appConfig.thousand_seperator);
                    }
                    $scope.salesModel.total = data.total;
                    $scope.salesModel.total_Amount = totalAmt;
                    $scope.salesModel.total_Amount = parseFloat($scope.salesModel.total_Amount).toFixed(2);
                    $scope.salesModel.total_Amount = utilityService.changeCurrency($scope.salesModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    if($scope.showDetail)
                    {
                        for(var i=0; i < $scope.salesModel.list.length; i++)
                        {
                            for(var j=0; j < $scope.salesModel.list[i].item.length; j++)
                            {
                                var rowtotal = (parseFloat($scope.salesModel.list[i].item[j].qty, 10)) * (parseInt($scope.salesModel.list[i].item[j].unit_price, 10));
                                $scope.salesModel.list[i].item[j].rowtotal = rowtotal;
                            }
                        }
                    }
                }
                $scope.salesModel.isLoadingProgress = false;
                $scope.salesModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            //autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatcustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined && model.company  != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ',' + model.company + ')';
                }
                else if (model.fname != undefined && model.phone != undefined && model.company  != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };

        $scope.print = function()
        {
            $window.print();
        };

        $scope.getList();
    }]);




