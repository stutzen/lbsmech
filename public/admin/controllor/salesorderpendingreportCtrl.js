
app.controller('salesorderpendingreportCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.salesOrderPendingModel = {
        currentPage: 1,
        total: 0,
        limit: 10,
        total_Amount: 0,
        list: [],
        id: '',
        is_active: 1,
        isSearchLoadingProgress: false,
        isLoadingProgress: false
    };


    $scope.searchFilter = {
        limit: '',
        start: '',
        customerInfo: '',
        fromdate: '',
        todate: '',
        city : ''
    };

    $scope.adminService = adminService;
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currencyFormat = $rootScope.appConfig.currency;
    $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0));
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
    $scope.searchFilter.todate = $scope.currentDate;

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.validationFactory = ValidationFactory;

    $scope.openDate = function(index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index === 1)
        {
            $scope.toDateOpen = true;
        }

    };
    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            limit: '',
            start: '',
            customerInfo: '',
            fromdate: '',
            todate: '',
            city : ''
        };
        $scope.salesOrderPendingModel.list = [];
        $scope.searchFilter.todate = $scope.currentDate;
        $scope.initTableFilter();
    }


    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    $scope.getCityList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.CITY_LIST, autosearchParam, false).then(function(responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };

    $scope.formatcityModel = function(model)
    {
        if (model !== null && model !== undefined)
        {
            return model.name;
        }
        return  '';
    }
    $scope.getList = function() {

        $scope.salesOrderPendingModel.isLoadingProgress = true;
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'salesorderpending';
        getListParam.is_active = 1;
        //getListParam.mode = 1;
        getListParam.start = 0;
        getListParam.limit = 0;
        $scope.salesOrderPendingModel.isLoadingProgress = true;
        $scope.salesOrderPendingModel.isSearchLoadingProgress = true;
        if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
        {
            getListParam.customer_id = $scope.searchFilter.customerInfo.id;
        }
        else
        {
            getListParam.customer_id = '';
        }
        if ($scope.searchFilter.city != null && typeof $scope.searchFilter.city != 'undefined' && typeof $scope.searchFilter.city.id != 'undefined')
        {
            getListParam.city = $scope.searchFilter.city.id;
        }
        else
        {
            getListParam.city = '';
        }
        if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
        {
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
            }
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
        }
        if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
        }
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getPendingSalesOrderReport(getListParam, configOption, headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.salesOrderPendingModel.list = data;
                for (var i = 0; i < $scope.salesOrderPendingModel.list.length; i++)
                {
                    $scope.salesOrderPendingModel.list[i].newdate = utilityService.parseStrToDate($scope.salesOrderPendingModel.list[i].date);
                    $scope.salesOrderPendingModel.list[i].date = utilityService.parseDateToStr($scope.salesOrderPendingModel.list[i].newdate, $scope.adminService.appConfig.date_format);

                }
            }
            $scope.salesOrderPendingModel.isLoadingProgress = false;
            $scope.salesOrderPendingModel.isSearchLoadingProgress = false;
        });

    };

    $scope.getCustomerList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.search = val;
        if (autosearchParam.search != '')
        {
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }
    };

    $scope.formatCustomerModel = function(model)
    {
        if (model != null)
        {
            if (model.fname != undefined && model.phone != undefined && model.email != undefined)
            {
                return model.fname + '(' + model.phone + ',' + model.email + ',' + model.customer_code + ')';
            }
            else if (model.fname != undefined && model.phone != undefined)
            {
                return model.fname + '(' + model.phone + model.customer_code + ')';
            }
        }
        return  '';
    };

    $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal) {
        if (newVal != undefined)
        {
            if (typeof newVal.id != 'undefined' && newVal.id != '')
            {
                $scope.initTableFilter();
            }
        }
        else
        {
            console.log(typeof newVal);
            if (typeof newVal == 'object' || newVal == null)
            {
                $scope.initTableFilter();
            }
        }
    });

    $scope.print = function()
    {
        $window.print();
    };

    $scope.getList();

}]);




