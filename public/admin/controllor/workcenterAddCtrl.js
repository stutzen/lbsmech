

app.controller('workcenterAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $scope.workcenterModel = {
            id: '',
            centername: "",
            code: '',
            comments: ''
        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.workcenter_form != 'undefined' && typeof $scope.workcenter_form.$pristine != 'undefined' && !$scope.workcenter_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            $scope.workcenterModel.centername = '';
            $scope.workcenterModel.comments = '';
            $scope.workcenterModel.code = '';
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.createWorkCenter = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var getListParam = {};
                var headers = {};
                headers['screen-code'] = 'workcenter';                
                getListParam.name = $scope.workcenterModel.centername;
                getListParam.code = $scope.workcenterModel.code;
                getListParam.comments = $scope.workcenterModel.comments;
                adminService.saveWorkCenter(getListParam, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.workcenter');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

    }]);




