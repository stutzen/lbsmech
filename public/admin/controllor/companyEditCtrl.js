
app.controller('companyEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function ($scope, $rootScope, adminService, utilityService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

            $scope.companyModel = {
                  id: '',
                  name: '',
                  city: '',
                  district: '',
                  state: '',
                  pin_code: '',
                  country: '',
                  address: '',
                  website: '',
                  list: [],
                  city_list: [],
                  state_list: [],
                  country_list: []
            }
            $scope.validationFactory = ValidationFactory;
            $scope.companyModel.isLoadingProgress = false;
            $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
            $scope.getNavigationBlockMsg = function ()
            {
                  if (typeof $scope.edit_company_form != 'undefined' && typeof $scope.edit_company_form.$pristine != 'undefined' && !$scope.edit_company_form.$pristine)
                  {
                        return $scope.NAVIGATION_BLOCKER_MSG;
                  }

                  return "";
            }
            $scope.isDataSavingProcess = false;
            $scope.formReset = function ()
            {
                  $scope.edit_company_form.$setPristine();
                  if (typeof $scope.edit_company_form != 'undefined')
                  {
                        $scope.companyModel.name = "";
                        $scope.companyModel.city = "";
                        $scope.companyModel.country = "";
                        $scope.companyModel.pin_code = "";
                        $scope.companyModel.state = "";
                        $scope.companyModel.website = "";
                        $scope.companyModel.address = "";
                        $scope.companyModel.isActive = true;
                        $scope.companyModel.district = "";

                  }
                  $scope.updateCompanyInfo();
            }
            $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
            $scope.isCompanyLoaded = false;
            $scope.isCountryLoaded = false;
            $scope.isStateLoaded = false;
            $scope.isCityLoaded = false;
            $scope.initUpdateTimeoutPromise = null;
            $scope.initUpdateDetail = function ()
            {
                  if ($scope.initUpdateTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.initUpdateTimeoutPromise);
                  }
                  if ($scope.isCompanyLoaded == true && $scope.isCityLoaded == true)
                  {
                        $scope.updateCompanyInfo();
                  } else
                  {
                        $scope.initUpdateTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                  }
            };
            $scope.updateCompanyInfo = function ()
            {
                  $scope.companyModel.is_active = $scope.companyModel.list.is_active;
                  $scope.companyModel.name = $scope.companyModel.list.name;
                  $scope.companyModel.pin_code = $scope.companyModel.list.pin_code;
                  //$scope.companyModel.city = $scope.companyModel.list.city;
                  if ($scope.companyModel.list.city_id != 0)
                  {
                        $scope.companyModel.city_id = $scope.companyModel.list.city_id;
                  } else
                  {
                        $scope.companyModel.city_id = '';
                  }
                  if ($scope.companyModel.list.country_id != 0)
                  {
                        $scope.companyModel.country_id = $scope.companyModel.list.country_id;
                  } else
                  {
                        $scope.companyModel.country_id = '';
                  }
                  if ($scope.companyModel.list.state_id != 0)
                  {
                        $scope.companyModel.state_id = $scope.companyModel.list.state_id;
                  } else
                  {
                        $scope.companyModel.state_id = '';
                  }
                  $scope.companyModel.isLoadingProgress = false;
                  $scope.companyModel.id = $scope.companyModel.list.id;
                  $scope.companyModel.countryInfo = {
                        name: $scope.companyModel.list.country,
                  };
                  $scope.companyModel.stateInfo = {
                        name: $scope.companyModel.list.state,
                  };
                  //$scope.companyModel.country = $scope.companyModel.list.country;
                  //$scope.companyModel.state = $scope.companyModel.list.state;
                  $scope.companyModel.website = $scope.companyModel.list.website;
                  $scope.companyModel.address = $scope.companyModel.list.address;
                  $scope.companyModel.district = $scope.companyModel.list.district;
            }
            $scope.modifycompany = function () {

                  if ($scope.isDataSavingProcess)
                  {
                        return;
                  }

                  $scope.isDataSavingProcess = true;
                  var addCompanyParam = {};
                  var headers = {};
                  headers['screen-code'] = 'company';
                  addCompanyParam.id = $stateParams.id;
                  addCompanyParam.name = $scope.companyModel.name;
                  addCompanyParam.pin_code = $scope.companyModel.pin_code;
                  //addCompanyParam.city = $scope.companyModel.city;
                  addCompanyParam.country_id = $scope.companyModel.country_id;
                  for (var i = 0; i < $scope.companyModel.countryList.length; i++)
                  {
                        if ($scope.companyModel.country_id == $scope.companyModel.countryList[i].id)
                        {
                              addCompanyParam.country_name = $scope.companyModel.countryList[i].name;
                        }
                  }
                  addCompanyParam.state_id = $scope.companyModel.state_id;
                  for (var i = 0; i < $scope.companyModel.stateList.length; i++)
                  {
                        if ($scope.companyModel.state_id == $scope.companyModel.stateList[i].id)
                        {
                              addCompanyParam.state_name = $scope.companyModel.stateList[i].name;
                        }
                  }
                  addCompanyParam.city_id = $scope.companyModel.city_id;
                  for (var i = 0; i < $scope.companyModel.cityList.length; i++)
                  {
                        if ($scope.companyModel.city_id == $scope.companyModel.cityList[i].id)
                        {
                              addCompanyParam.city_name = $scope.companyModel.cityList[i].name;
                        }
                  }
                  //addCompanyParam.state = $scope.companyModel.state;
                  //addCompanyParam.country = $scope.companyModel.country;
                  addCompanyParam.address = $scope.companyModel.address;
                  addCompanyParam.website = $scope.companyModel.website;
                  addCompanyParam.is_active = $scope.companyModel.isActive;
                  addCompanyParam.district = $scope.companyModel.district;
                  adminService.editCompany(addCompanyParam, $scope.companyModel.id, headers).then(function (response)
                  {
                        if (response.data.success == true)
                        {
                              $scope.formReset();
                              $state.go('app.company');
                        }
                        $scope.isDataSavingProcess = false;
                  });
            };
            $scope.getCompanyInfo = function ()
            {

                  if (typeof $stateParams.id != 'undefined')
                  {
                        $scope.companyModel.isLoadingProgress = true;
                        $scope.isCompanyLoaded = false;
                        var companyListParam = {};
                        companyListParam.id = $stateParams.id;
                        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                        adminService.getCompanyList(companyListParam, configOption).then(function (response) {

                              var data = response.data;
                              if (data.list.length > 0)
                              {
                                    $scope.companyModel.list = data.list[0];

                              }
                              $scope.isCompanyLoaded = true;
                              $scope.initUpdateDetail();

                        });
                  }
            };
            $scope.getcountryList = function () {

                  $scope.companyModel.isLoadingProgress = true;
                  var countryListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'company';

                  countryListParam.id = '';

                  countryListParam.is_active = 1;
                  $scope.isCountryLoaded = false;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getCountryList(countryListParam, configOption, headers).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              $scope.companyModel.countryList = data.list;
                        }
                        $scope.isCountryLoaded = true;
                        $scope.companyModel.isLoadingProgress = false;
                  });

            };
            $scope.getstateList = function () {

                  $scope.companyModel.isLoadingProgress = true;
                  var stateListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'company';

                  stateListParam.id = '';

                  stateListParam.is_active = 1;
                  stateListParam.country_id = $scope.companyModel.country_id;
                  stateListParam.country_name = $scope.companyModel.country_name;
                  $scope.isStateLoaded = false;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getStateList(stateListParam, configOption, headers).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              $scope.companyModel.stateList = data.list;
                        }
                        $scope.isStateLoaded = true;
                        $scope.companyModel.isLoadingProgress = false;
                  });

            };

            $scope.getcityList = function () {

                  $scope.companyModel.isLoadingProgress = true;
                  var cityListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'company';

                  cityListParam.id = '';

                  cityListParam.is_active = 1;
                  cityListParam.country_id = $scope.companyModel.country_id;
                  cityListParam.country_name = $scope.companyModel.country_name;
                  cityListParam.state_id = $scope.companyModel.state_id;
                  cityListParam.start = 0;
                  cityListParam.limit = 0;
                  $scope.isCityLoaded = false;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getCityList(cityListParam, configOption, headers).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              $scope.companyModel.cityList = data.list;
                        }
                        $scope.isCityLoaded = true;
                        $scope.companyModel.isLoadingProgress = false;
                  });

            };

            $scope.CountryChange = function ()
            {
                  $scope.companyModel.state_id = '';
                  $scope.companyModel.city_id = '';
                  $scope.getstateList();

            }
            $scope.StateChange = function ()
            {
                  $scope.companyModel.city_id = '';
                  $scope.getcityList();

            }
            $scope.getCompanyInfo();
            $scope.getcityList();
            $scope.getcountryList();
            $scope.getstateList();
      }]);





