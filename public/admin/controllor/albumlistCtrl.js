





app.controller('albumlistCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.albumlistModel = {
            currentPage: 1,
            total: 0,
            id: '',
            limit: 10,
            list: [],
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.albumlistModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: '',
            type: '',
            code: '',
            customer_id: ''
        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: '',
                type: '',
                code: '',
                customer_id: ''
            };
            $scope.initTableFilter();
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.selectalbumlistId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectalbumlistId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deleteAlbumlistInfo = function( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectalbumlistId;
                var headers = {};
                headers['screen-code'] = 'albumlist';
                adminService.deletealbumlist(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };
        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'albumlist';
            getListParam.id = $scope.albumlistModel.id;
            getListParam.name = $scope.searchFilter.name;
            getListParam.type = $scope.searchFilter.type;
            getListParam.code = $scope.searchFilter.code;
            //getListParam.is_active = 1;
            getListParam.customer_id = $scope.searchFilter.customer_id;
            getListParam.start = ($scope.albumlistModel.currentPage - 1) * $scope.albumlistModel.limit;
            getListParam.limit = $scope.albumlistModel.limit;
            $scope.albumlistModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getalbumlist(getListParam, configOption, headers).then(function(response) {
                var data = response.data.list;
                $scope.albumlistModel.list = data;
                console.log('test');
                console.log($scope.albumlistModel.list);
                for (var i = 0; i < $scope.albumlistModel.list.length; i++)
                {
                    if ($scope.albumlistModel.list[i].asset.length > 0)
                    {
                        for (var j = 0; j < $scope.albumlistModel.list[i].asset.length; j++)
                        {
                            $scope.albumlistModel.list[i].asset[j].path = $scope.albumlistModel.list[i].asset[j].url;
                        }
                    }
                }
                $scope.albumlistModel.total = data.total;
                $scope.albumlistModel.isLoadingProgress = false;
            });

        };

        $scope.getList();
    }]);




