app.controller('customerTimelineCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'sweet', '$window','$cookies', '$location','customerFactory', function($scope, $rootScope, adminService, $httpService, APP_CONST, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, sweet, $window,$cookies,$location,customerFactory) {

    $scope.customerModel = {
        id: '',
        name: '',
        currentPage: 1,
        total: 0,
        limit: 10,
        sku: '',
        description: '',
        orderList : [],
        list : [],
        customerInfo: {},
        isLoadingProgress : false,
        invoiceList : [],
        quoteList : [],
        paymentList : [],
        purchaseorderList : [],
        purchaseInvoiceList : [],
        purchasepaymentList : [],
        amcList : []
    };
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.pagePerCount = [10, 20, 50, 100];
    $scope.customerFactory = customerFactory;
    $scope.stateParamData = $stateParams.id;
    $scope.customerModel.limit = $scope.pagePerCount[0];
    $scope.getNavigationBlockMsg = function(pageReload)
    {
        if (typeof $scope.product_form != 'undefined' && typeof $scope.product_form.$pristine != 'undefined' && !$scope.product_form.$pristine)
        {
            return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
        }

        return "";
    }
    $scope.tabChange = function(value) {

        $scope.currentStep = value;  
    }
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $scope.isDataSavingProcess = false;
    $scope.getOrderList = function()
    {
        var headers = {};
        headers['screen-code'] = 'salesorder';
        var getListParam = {};
        getListParam.customerid = $stateParams.id;
        getListParam.is_active = 1;
        $scope.customerModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getSalesOrderList(getListParam,configOption,headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.customerModel.orderList = data;
                $scope.viewOrderFlag = false;
                if($scope.customerModel.orderList.length > 10)
                {
                    $scope.viewOrderFlag = true;
                } 
                for (var i = 0; i < $scope.customerModel.orderList.length; i++)
                {
                    $scope.customerModel.orderList[i].newdate = utilityService.parseStrToDate($scope.customerModel.orderList[i].date);
                    $scope.customerModel.orderList[i].date = utilityService.parseDateToStr($scope.customerModel.orderList[i].newdate, $scope.dateFormat);
                    $scope.customerModel.orderList[i].duedate = utilityService.parseStrToDate($scope.customerModel.orderList[i].validuntil);
                    $scope.customerModel.orderList[i].validuntil = utilityService.parseDateToStr($scope.customerModel.orderList[i].duedate, $scope.dateFormat);
                }
                $scope.customerModel.total = data.total;
            }
            $scope.customerModel.isLoadingProgress = false;
       
        });

    };
    $scope.getPurchaseOrderList = function()
    {
        var headers = {};
        headers['screen-code'] = 'purchaseestimate';
        var getListParam = {};
        getListParam.customer_id = $stateParams.id;
        getListParam.is_active = 1;
        $scope.customerModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getPurchaseQuoteList(getListParam,configOption,headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.customerModel.purchaseorderList = data;
                if($scope.customerModel.purchaseorderList.length > 10)
                {
                    $scope.viewPurchaseorderFlag = true;
                } 
                for (var i = 0; i < $scope.customerModel.purchaseorderList.length; i++)
                {
                    $scope.customerModel.purchaseorderList[i].newdate = utilityService.parseStrToDate($scope.customerModel.purchaseorderList[i].date);
                    $scope.customerModel.purchaseorderList[i].date = utilityService.parseDateToStr($scope.customerModel.purchaseorderList[i].newdate, $scope.dateFormat);
                    $scope.customerModel.purchaseorderList[i].duedate = utilityService.parseStrToDate($scope.customerModel.purchaseorderList[i].validuntil);
                    $scope.customerModel.purchaseorderList[i].validuntil = utilityService.parseDateToStr($scope.customerModel.purchaseorderList[i].duedate, $scope.dateFormat);
                }
                $scope.customerModel.total = data.total;
            }
            $scope.customerModel.isLoadingProgress = false;
       
        });

    };
    $scope.getPurchaseInvoiceList = function()
    {
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'purchaseinvoice';
        getListParam.customer_id = $stateParams.id;
        getListParam.is_active = 1;
        $scope.customerModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getPurchaseInvoiceList(getListParam,configOption,headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.customerModel.purchaseInvoiceList = data;
                if($scope.customerModel.purchaseInvoiceList.length > 10)
                {
                    $scope.viewPurchaseinvoiceFlag = true;
                }    
                for (var i = 0; i < $scope.customerModel.purchaseInvoiceList.length; i++)
                {
                    $scope.customerModel.purchaseInvoiceList[i].newdate = utilityService.parseStrToDate($scope.customerModel.purchaseInvoiceList[i].date);
                    $scope.customerModel.purchaseInvoiceList[i].date = utilityService.parseDateToStr($scope.customerModel.purchaseInvoiceList[i].newdate, $scope.dateFormat);
                    $scope.customerModel.purchaseInvoiceList[i].due = utilityService.parseStrToDate($scope.customerModel.purchaseInvoiceList[i].duedate);
                    $scope.customerModel.purchaseInvoiceList[i].duedate = utilityService.parseDateToStr($scope.customerModel.purchaseInvoiceList[i].due, $scope.dateFormat);
                }
                $scope.customerModel.total = data.total;
            }
            $scope.customerModel.isLoadingProgress = false;
       
        });

    };
    $scope.getInvoiceList = function()
    {
        var headers = {};
        headers['screen-code'] = 'salesinvoice';
        var getListParam = {};
        getListParam.customer_id = $stateParams.id;
        getListParam.is_active = 1;
        $scope.customerModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getInvoiceList(getListParam,configOption,headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.customerModel.invoiceList = data;
                if($scope.customerModel.invoiceList.length > 10)
                {
                    $scope.viewinvoiceFlag = true;
                }    
                for (var i = 0; i < $scope.customerModel.invoiceList.length; i++)
                {
                    $scope.customerModel.invoiceList[i].newdate = utilityService.parseStrToDate($scope.customerModel.invoiceList[i].date);
                    $scope.customerModel.invoiceList[i].date = utilityService.parseDateToStr($scope.customerModel.invoiceList[i].newdate, $scope.dateFormat);
                    $scope.customerModel.invoiceList[i].due = utilityService.parseStrToDate($scope.customerModel.invoiceList[i].duedate);
                    $scope.customerModel.invoiceList[i].duedate = utilityService.parseDateToStr($scope.customerModel.invoiceList[i].due, $scope.dateFormat);
                }
                $scope.customerModel.total = data.total;
            }
            $scope.customerModel.isLoadingProgress = false;
       
        });

    };
    $scope.getQuotationList = function()
    {
        var headers = {};
        headers['screen-code'] = 'salesestimate';
        var getListParam = {};
        getListParam.contactid = $stateParams.id;
        getListParam.is_active = 1;
        $scope.customerModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getQuoteList(getListParam,configOption,headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.customerModel.quoteList = data;
                $scope.viewquoteFlag = false;
                if($scope.customerModel.quoteList.length > 10)
                {
                    $scope.viewquoteFlag = true;
                }    
                for (var i = 0; i < $scope.customerModel.quoteList.length; i++)
                {
                    $scope.customerModel.quoteList[i].newdate = utilityService.parseStrToDate($scope.customerModel.quoteList[i].date);
                    $scope.customerModel.quoteList[i].date = utilityService.parseDateToStr($scope.customerModel.quoteList[i].newdate, $scope.dateFormat);
                    $scope.customerModel.quoteList[i].due = utilityService.parseStrToDate($scope.customerModel.quoteList[i].validuntil);
                    $scope.customerModel.quoteList[i].validuntil = utilityService.parseDateToStr($scope.customerModel.quoteList[i].due, $scope.dateFormat);
                }
                $scope.customerModel.total = data.total;
            }
            $scope.customerModel.isLoadingProgress = false;
       
        });

    };

    $scope.getSalesPayment = function()
    {
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'advancepayment';
        getListParam.customer_id = $stateParams.id;
        getListParam.is_active = 1;
        getListParam.type = 1;
        $scope.customerModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getAdvancepaymentList(getListParam,configOption,headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.customerModel.paymentList = data;
                if($scope.customerModel.paymentList.length > 10)
                {
                    $scope.viewpaymentFlag = true;
                } 
                for (var i = 0; i < $scope.customerModel.paymentList.length; i++)
                {
                    $scope.customerModel.paymentList[i].newdate = utilityService.parseStrToDate($scope.customerModel.paymentList[i].date);
                    $scope.customerModel.paymentList[i].date = utilityService.parseDateToStr($scope.customerModel.paymentList[i].newdate, $scope.dateFormat);
                }
                $scope.customerModel.total = data.total;
            }
            $scope.customerModel.isLoadingProgress = false;
       
        });

    };
    $scope.getPurchasePayment = function()
    {
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'purchaseAdvPayReceipt';
        getListParam.customer_id = $stateParams.id;
        getListParam.is_active = 1;
        getListParam.type= 2;
        $scope.customerModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getAdvancepaymentList(getListParam,configOption,headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.customerModel.purchasepaymentList = data;
                if($scope.customerModel.purchasepaymentList.length > 10)
                {
                    $scope.viewpurchasepaymentFlag = true;
                } 
                for (var i = 0; i < $scope.customerModel.purchasepaymentList.length; i++)
                {
                    $scope.customerModel.purchasepaymentList[i].newdate = utilityService.parseStrToDate($scope.customerModel.purchasepaymentList[i].date);
                    $scope.customerModel.purchasepaymentList[i].date = utilityService.parseDateToStr($scope.customerModel.purchasepaymentList[i].newdate, $scope.dateFormat);
                }
                $scope.customerModel.total = data.total;
            }
            $scope.customerModel.isLoadingProgress = false;
       
        });

    };
    $scope.getAmc = function()
    {
        var headers = {};
        headers['screen-code'] = 'amc';
        var getListParam = {};
        getListParam.customer_id = $stateParams.id;
        getListParam.is_active = 1;
        $scope.customerModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getAmcList(getListParam,configOption,headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.customerModel.amcList = data;
                if($scope.customerModel.amcList.length > 10)
                {
                    $scope.viewamcFlag = true;
                }    
                for (var i = 0; i < $scope.customerModel.amcList.length; i++)
                {
                    $scope.customerModel.amcList[i].newdate = utilityService.parseStrToDate($scope.customerModel.amcList[i].from_date);
                    $scope.customerModel.amcList[i].from_date = utilityService.parseDateToStr($scope.customerModel.amcList[i].newdate, $scope.dateFormat);
                }
                $scope.customerModel.total = data.total;
            }
            $scope.customerModel.isLoadingProgress = false;
       
        });

    };
    
    $scope.getCustomerList = function ()
    {
        var headers = {};
        headers['screen-code'] = 'customer';
        $scope.customerModel.isLoadingProgress = true;
        if (typeof $stateParams.id != 'undefined')
        {
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCustomersList(getListParam, configOption,headers).then(function (response) {

                var data = response.data;
                if (data.list.length > 0)
                {
                    $scope.customerModel.customerInfo = data.list[0];
                    $scope.customerModel.customerInfo.is_sale = $scope.customerModel.customerInfo.is_sale == 1 ? true : false
                    $scope.customerModel.customerInfo.is_purchase = $scope.customerModel.customerInfo.is_purchase == 1 ? true : false

                }
            });
        }
        $scope.customerModel.isLoadingProgress = false;
    };
    $scope.viewMore = function(type)
    {
          
        var customer = {};
        customer.fname = $scope.customerModel.customerInfo.fname;
        customer.id = $scope.customerModel.customerInfo.id;
        // customerFactory.set($scope.customerModel.customerInfo);
        customerFactory.set(customer);
        if(type == 'invoice')
        {  
            $state.go('app.invoice');
        }
        if(type == 'quote')
        {
            $state.go('app.quote');
        }   
        if(type == 'salesorder')
        {
            $state.go('app.salesorder');
        }
        if(type == 'salespayment')
        {
            $state.go('app.advancepayment');
        } 
        if(type == 'amc')
        {
            $state.go('app.amc')
        }    
        if(type == 'purchaseadvance')
        {
            $state.go('app.purchaseAdvancePayReceipt')
        } 
        if(type == 'purchaseorder')
        {
            $state.go('app.purchaseQuote')
        } 
        if(type == 'purchaseinvoice')
        {
            $state.go('app.purchaseInvoice')
        } 
    }
    $scope.getCustomerList();
    $scope.getOrderList();
    $scope.getInvoiceList();
    $scope.getQuotationList();
    $scope.getSalesPayment();
    $scope.getPurchaseOrderList();
    $scope.getPurchaseInvoiceList();
    $scope.getPurchasePayment();
    $scope.getAmc();
}]);