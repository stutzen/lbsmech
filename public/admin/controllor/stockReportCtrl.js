app.controller('stockReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$timeout', '$window', 'APP_CONST', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, $timeout, $window, APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.stockReportModel = {
            scopeId: "",
            configId: 5,
            type: "",
            currentPage: 1,
            total: 0,
            limit: '',
            list: [],
            status: '',
            start: 0,
            currencyFormat: '',
            serverList: null,
            isLoadingProgress: true
        };

        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.stockReportModel.limit = $scope.pagePerCount[0];
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.searchFilter = {
            productInfo: ''
        };

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                productInfo: ''
            };
            //$scope.searchFilter.productInfo = {};
            //$scope.stockReportModel.list = [];
            $scope.initTableFilter();
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getStockListInfo, 300);
        }


        $scope.print = function ()
        {
            $window.print();
        };
        $scope.formatproductlistModel = function (model) {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };


        $scope.getproductlist = function (val) {

            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = "Product";
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData) {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.getStockListInfo = function () {

            $scope.stockReportModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'inventorystock';
          //  materialListParam.id = '';
            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
            {
                materialListParam.product_name = $scope.searchFilter.productInfo.name;
            } else
            {
                materialListParam.product_name = '';
            }
//            materialListParam.start = ($scope.stockReportModel.currentPage - 1) * $scope.stockReportModel.limit;
//            materialListParam.limit = $scope.stockReportModel.limit;
//            $scope.stockReportModel.start = materialListParam.start;
            adminService.getInventoryStockListInfo(materialListParam, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.stockReportModel.list = data.list;
                    for (var i = 0; i < $scope.stockReportModel.list.length; i++)
                    {
                        $scope.stockReportModel.list[i].index = $scope.stockReportModel.start + i + 1;
                    }
                    $scope.stockReportModel.total = data.total;
                }
                $scope.stockReportModel.isLoadingProgress = false;
            });

        };

        $scope.getStockListInfo();

    }]);




