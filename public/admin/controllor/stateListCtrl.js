/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
app.controller('stateListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.stateModel = {
            
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            country_name:'',
            country_list:[],
            isLoadingProgress: true

        };
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.stateModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: '',
            country_name:'',
            country_id:''
            
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function(id)
        {
            $scope.selectStageId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteStateInfo = function( )
        {
            if($scope.isdeleteProgress == false)
            {
               $scope.isdeleteProgress = true;
               
                var getListParam = {};
                getListParam.id = $scope.selectStageId;
                var headers = {};
                headers['screen-code'] = 'state';
                adminService.deleteState(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                     $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: '',
                country_name:'',
                country_id:''
            };
            $scope.initTableFilter();
        }

        $scope.getList = function() {          

            $scope.stateModel.isLoadingProgress = true;
            var stateListParam = {};
            var headers = {};
            headers['screen-code'] = 'state';
            stateListParam.name = $scope.searchFilter.name;
            stateListParam.country_id = $scope.searchFilter.country_id;
//            stateListParam.country_name = $scope.searchFilter.country_name;
//            stateListParam.id = '';
            
            stateListParam.is_active = 1;
            stateListParam.start = ($scope.stateModel.currentPage - 1) * $scope.stateModel.limit;
            stateListParam.limit = $scope.stateModel.limit;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.stateModel.list = data.list;
                    $scope.stateModel.total = data.total;
                }
                $scope.stateModel.isLoadingProgress = false;
            });

        };

        $scope.getList();
        
         $scope.isLoadedCountryList = false;
        $scope.getcountryList = function()
        {
            var getListParam = {};
            getListParam.id = '';
            $scope.isLoadedCountryList = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.stateModel.country_list = data.list;
                $scope.isLoadedCountryList = true;
            });
        };
        $scope.getcountryList();
    }]);






