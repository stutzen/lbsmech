app.controller('stockwastageCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, ValidationFactory, utilityService, $httpService, APP_CONST, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.stockwastageModel = {
            scopeId: "",
            configId: 5,
            type: "",
            from_date: "",
            currentPage: 1,
            total: 0,
            productInfo: '',
            product_id: '',
            id: '',
            limit: 4,
            list: [],
            status: '',
//        currencyFormat:'',
//        serverList: null,
            isLoadingProgress: true
        };
        $scope.searchFilterNameValue = ''
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.stockwastageModel.limit = $scope.pagePerCount[0];
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;

        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            from_date: '',
            to_date: '',
            id: '',
            product_id: ''

        };
        $scope.stockwastageModel.productInfo = "";
        $scope.searchFilter.from_date = $scope.currentDate;
        // $scope.searchFilter.from_date = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };


        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                from_date: '',
                to_date: '',
                id: '',
                product_id: ''
            };
            $scope.stockwastageModel.list = [];
            $scope.stockwastageModel.productInfo = "";
            $scope.stockwastage_form.$setPristine();
            $scope.searchFilter.from_date = $scope.currentDate;
//            $scope.searchFilterValue = "";
//            $scope.searchFilter.from_date = $filter('date')($scope.currentDate, $scope.dateFormat);
            $scope.initTableFilter();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getStockwastageListInfo, 300);
        }

        $scope.getProductList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatproductModel = function(model) {

            if (model !== null)
            {
                $scope.searchFilter.id = model.id;
                return model.name;
            }
            return  '';
        };
        $scope.$watch('searchFilter.productInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });

        $scope.getStockwastageListInfo = function() {

            // $scope.stockwastageModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'stockwastage';
            materialListParam.type = 2;
            //materialListParam.type = "stock_wastage";
            //materialListParam.id = $scope.stockwastageModel.productInfo.id;
            materialListParam.id = '';
            materialListParam.from_date = $scope.searchFilter.from_date;
            materialListParam.to_date = $scope.searchFilter.to_date;
            if ($scope.searchFilter.to_date != '' && ($scope.searchFilter.from_date == null || $scope.searchFilter.from_date == ''))
            {
                if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                {
                    materialListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.dateFormat);
                
                materialListParam.to_date = utilityService.changeDateToSqlFormat(materialListParam.to_date, $scope.dateFormat);
                materialListParam.from_date = materialListParam.to_date;
            }
        }
            else if ($scope.searchFilter.from_date != '' && ($scope.searchFilter.to_date == null || $scope.searchFilter.to_date == ''))
            {
                if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                {
                    materialListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.dateFormat);
                
                materialListParam.from_date = utilityService.changeDateToSqlFormat(materialListParam.from_date, $scope.dateFormat);
                materialListParam.to_date = materialListParam.from_date;
            }
            }
            else if ($scope.searchFilter.to_date != null && $scope.searchFilter.to_date != '' && $scope.searchFilter.from_date != null && $scope.searchFilter.from_date != '')
            {
                if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                {
                    materialListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.dateFormat);
                }
                if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                {
                    materialListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.dateFormat);
                }
                materialListParam.to_date = utilityService.changeDateToSqlFormat(materialListParam.to_date, $scope.dateFormat);
                materialListParam.from_date = utilityService.changeDateToSqlFormat(materialListParam.from_date, $scope.dateFormat);
            }
            if ($scope.stockwastageModel.productInfo != null && typeof $scope.stockwastageModel.productInfo != 'undefined' && typeof $scope.stockwastageModel.productInfo.id != 'undefined')
            {
                materialListParam.product_id = $scope.stockwastageModel.productInfo.id;
            }
            else
            {
                materialListParam.product_id = '';
            }            
            materialListParam.is_active = 1;
            // materialListParam.product_id = $scope.searchFilter.product_id;
            materialListParam.start = ($scope.stockwastageModel.currentPage - 1) * $scope.stockwastageModel.limit;
            materialListParam.limit = $scope.stockwastageModel.limit;
            $scope.stockwastageModel.isLoadingProgress = true;
            adminService.getStockwastageList(materialListParam, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.stockwastageModel.list = data.list;
                    for (var i = 0; i < $scope.stockwastageModel.list.length; i++)
                    {
                        $scope.stockwastageModel.list[i].newdate = utilityService.parseStrToDate($scope.stockwastageModel.list[i].from_date);
                        $scope.stockwastageModel.list[i].from_date = utilityService.parseDateToStr($scope.stockwastageModel.list[i].newdate, $rootScope.appConfig.date_format);
                    }
                    $scope.stockwastageModel.total = data.total;
                }
                $scope.stockwastageModel.isLoadingProgress = false;
            });

        };

        $scope.getStockwastageListInfo();

    }]);




