

app.controller('transportEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.transportModel = {
            "id": "",
            "name": "",
            "landline_no": "",
            "email": "",
            "mobile": "",
            "city_id": "",
            "city_name": "",
            "state_id": "",
            "state_name": "",
            "country_id": "",
            "country_name": "",
            "postcode": "",
            "address": "",
            "land_mark": "",
            "stateList": [],
            "cityList": [],
            "countryList": [],
            "isLoadingProgress": false

        };
        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.modify_transport_form != 'undefined' && typeof $scope.modify_transport_form.$pristine != 'undefined' && !$scope.modify_transport_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            if (typeof $scope.modify_transport_form != 'undefined')
            {
                $scope.isLoadedCountryList = false;
                $scope.isLoadedCityList = false;
                $scope.isLoadedStateList = false;
                $scope.transportModel.city_id = '';
                $scope.transportModel.country_id = '';
                $scope.transportModel.state_id = '';
                $scope.modify_transport_form.$setPristine();
                $scope.getCountryList();
                $scope.getStateList();
                $scope.getCityList();
                $scope.initUpdateDetail();
            }

        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedCityList && $scope.isLoadedCountryList && $scope.isLoadedStateList)
            {
                $timeout($scope.updateTransportInfo(), 300);
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.updateTransportInfo = function()
        {
//            $scope.getCountryList();
//            $scope.getStateList();
//            $scope.getCityList();
            $scope.transportModel.id = $scope.transportModel.list.id;
            $scope.transportModel.name = $scope.transportModel.list.name;
            $scope.transportModel.address = $scope.transportModel.list.address;
            if (parseInt($scope.transportModel.list.city_id, 10) > 0)
            {
                $scope.transportModel.city_id = $scope.transportModel.list.city_id + '';
            }
            else
            {
                $scope.transportModel.city_id = '';
            }
            $scope.transportModel.city_name = $scope.transportModel.list.city_name;
            $scope.transportModel.email = $scope.transportModel.list.email;
            if (parseInt($scope.transportModel.list.country_id, 10) > 0)
            {
                $scope.transportModel.country_id = $scope.transportModel.list.country_id + '';
            }
            else
            {
                $scope.transportModel.country_id = '';
            }
            $scope.transportModel.country_name = $scope.transportModel.list.country_name;
            $scope.transportModel.land_mark = $scope.transportModel.list.land_mark;
            $scope.transportModel.landline_no = $scope.transportModel.list.landline_no;
            $scope.transportModel.mobile = $scope.transportModel.list.mobile_no;
            if (parseInt($scope.transportModel.list.state_id, 10) > 0)
            {
                $scope.transportModel.state_id = $scope.transportModel.list.state_id + '';
            }
            else
            {
                $scope.transportModel.state_id = '';
            }
            $scope.transportModel.state_name = $scope.transportModel.list.state_name;
            $scope.transportModel.postcode = $scope.transportModel.list.postcode;
            $scope.transportModel.comments = $scope.transportModel.list.comments;
            $scope.transportModel.isLoadingProgress = false;
        }


        $scope.modifyTransport = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var modifyTransportParam = {};
                var headers = {};
                headers['screen-code'] = 'transport';
                modifyTransportParam.id = $scope.transportModel.id;
                modifyTransportParam.name = $scope.transportModel.name;
                modifyTransportParam.landline_no = $scope.transportModel.landline_no;
                modifyTransportParam.mobile_no = $scope.transportModel.mobile;
                modifyTransportParam.email = $scope.transportModel.email;
                modifyTransportParam.city_id = $scope.transportModel.city_id;
                for (var i = 0; i < $scope.transportModel.cityList.length; i++)
                {
                    if ($scope.transportModel.city_id == $scope.transportModel.cityList[i].id)
                    {
                        modifyTransportParam.city_name = $scope.transportModel.cityList[i].name;
                    }
                }
                modifyTransportParam.country_id = $scope.transportModel.country_id;
                for (var i = 0; i < $scope.transportModel.countryList.length; i++)
                {
                    if ($scope.transportModel.country_id == $scope.transportModel.countryList[i].id)
                    {
                        modifyTransportParam.country_name = $scope.transportModel.countryList[i].name;
                    }
                }
                modifyTransportParam.state_id = $scope.transportModel.state_id;
                for (var i = 0; i < $scope.transportModel.stateList.length; i++)
                {
                    if ($scope.transportModel.state_id == $scope.transportModel.stateList[i].id)
                    {
                        modifyTransportParam.state_name = $scope.transportModel.stateList[i].name;
                    }
                }
                modifyTransportParam.postcode = $scope.transportModel.postcode;
                modifyTransportParam.address = $scope.transportModel.address;
                modifyTransportParam.land_mark = $scope.transportModel.land_mark;
                modifyTransportParam.comments = $scope.transportModel.comments;

                adminService.modifyTransport(modifyTransportParam, $scope.transportModel.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.transport');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getList = function() {
            $scope.transportModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTransportList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.transportModel.list = data.list[0];
                }
                $scope.getCountryList();
                $scope.initUpdateDetail();

            });
        };

        $scope.countryChange = function()
        {
            $scope.transportModel.state_id = '';
            $scope.transportModel.city_id = '';
            $scope.transportModel.cityList = [];
            $scope.transportModel.stateList = [];
        }

        $scope.stateChange = function()
        {
            $scope.transportModel.city_id = '';
            $scope.transportModel.cityList = [];
        }

        $scope.isLoadedCountryList = false;
        $scope.getCountryList = function() {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.code = "";
            $scope.isLoadedCountryList = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.transportModel.countryList = data;
                $scope.isLoadedCountryList = true;
            });
        };

        $scope.initAddressStateListTimeoutPromise = null;

        $scope.initAddressStateList = function(searchkey)
        {
            if ($scope.initAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initAddressStateListTimeoutPromise);
            }
            $scope.initAddressStateListTimeoutPromise = $timeout(function() {
                $scope.getStateList(searchkey);
            }, 300);

        };

        $scope.isLoadedStateList = false;
        $scope.getStateList = function(searchkey) {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_name = "";

            getListParam.country_id = $scope.transportModel.country_id;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            $scope.isLoadedStateList = false;
            adminService.getStateList(getListParam, configOption).then(function(response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                $scope.transportModel.stateList = data;
                $scope.isLoadedStateList = true;
            });

        };

        $scope.initAddressCityListTimeoutPromise = null;

        $scope.initAddressCityList = function(searchkey)
        {
            if ($scope.initAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initAddressCityListTimeoutPromise);
            }
            $scope.initAddressCityListTimeoutPromise = $timeout(function() {
                $scope.getCityList(searchkey);
            }, 300);

        };

        $scope.isLoadedCityList = false;
        $scope.getCityList = function(searchkey) {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.state_id = $scope.transportModel.state_id;
            getListParam.country_id = $scope.transportModel.country_id;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            $scope.isLoadedCityList = false;
            adminService.getCityList(getListParam, configOption).then(function(response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                $scope.transportModel.cityList = data;
                $scope.isLoadedCityList = true;
            });
        };

        $scope.getList();


    }]);




