

app.controller('incomecategoryAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $scope.incomecategoryAddModel = {
            id: '',
            name: "",
            comments: '',
            isActive: true

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.category_form != 'undefined' && typeof $scope.category_form.$pristine != 'undefined' && !$scope.category_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {


            $scope.incomecategoryAddModel.name = '';
            $scope.incomecategoryAddModel.comments = '';
            $scope.category_form.$setPristine();

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.createIncomeCategory = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'incomecategory';
                createParam.seq_no = '';
                createParam.name = $scope.incomecategoryAddModel.name;
                createParam.comment = $scope.incomecategoryAddModel.comments;
                createParam.type = 'income';

                adminService.addIncomeCategory(createParam, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.incomecategory');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

    }]);




