app.controller('userAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state) {
        $scope.userAddModel = {
            "userid": 0,
            "username": "",
            "password": "",
            "fname": "",
            "lname": "",
            "gender": "",
            "dob": '',
            "email": "",
            "mobile": "",
            "city": "",
            "state": "",
            "country": "",
            "postcode": "",
            "userType": "",
            "companyId": 2,
            "address": "",
            "rolename": "",
            "device_id": "",
            "isactive": 1,
            "roleList": {},
            currentPage: 1,
            total: 0,
            showPassword: false,
            role: ''
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_user_form != 'undefined' && typeof $scope.create_user_form.$pristine != 'undefined' && !$scope.create_user_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function() {

            $scope.create_user_form.$setPristine();
            $scope.userAddModel.fname = '';
            $scope.userAddModel.lname = '';
            $scope.userAddModel.dob = '';
            $scope.userAddModel.gender = '';
            $scope.userAddModel.mobile = '';
            $scope.userAddModel.email = '';
            $scope.userAddModel.address = '';
            $scope.userAddModel.city = '';
            $scope.userAddModel.postcode = '';
            $scope.userAddModel.state = '';
            $scope.userAddModel.country = '';
            $scope.userAddModel.username = '';
            $scope.userAddModel.password = '';
            $scope.userAddModel.role = '';
            $scope.userAddModel.device_id = '';

        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.currentDate = new Date();
        $scope.startDateOpen = false;

        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            }

        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createUser, 300);
        }

        $scope.getRoleList = function()
        {
            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserRoleList(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.userAddModel.roleList = data.list;
            });
        };

        $scope.createUser = function() {

            $scope.isDataSavingProcess = true;
            var createClientParam = {};
            var headers = {};
            headers['screen-code'] = 'users';
            createClientParam.id = 0;
            createClientParam.f_name = $scope.userAddModel.fname;
            createClientParam.l_name = $scope.userAddModel.lname;
            createClientParam.username = $scope.userAddModel.username;
            createClientParam.password = $scope.userAddModel.password;
            //createClientParam.userType = $scope.userAddModel.role == "ROLE_ADMIN" ? 1 : $scope.userAddModel.role == "ROLE_OWNER" ? 2 : 3;
            //          createClientParam.user_type = $scope.userAddModel.role == "ROLE_ADMIN" ? 'admin' : $scope.userAddModel.role == "ROLE_OWNER" ? 'owner' : 'workforce';
            //createClientParam.user_type = $scope.userAddModel.role;
            createClientParam.role_id = $scope.userAddModel.role;
            for (var i = 0; i < $scope.userAddModel.roleList.length; i++)
            {
                if ($scope.userAddModel.role == $scope.userAddModel.roleList[i].id)
                {
                    createClientParam.rolename = $scope.userAddModel.roleList[i].name;
                }
            }
            createClientParam.comp_id = $rootScope.userModel.companyId;
            createClientParam.ph_no = $scope.userAddModel.mobile;
            createClientParam.email = $scope.userAddModel.email;
            createClientParam.city = $scope.userAddModel.city;
            createClientParam.country = $scope.userAddModel.country;

            createClientParam.address = $scope.userAddModel.address;
            createClientParam.state = $scope.userAddModel.state;
            if($scope.userAddModel.dob != null && $scope.userAddModel.dob != '')
            {    
                createClientParam.dob = utilityService.parseDateToStr($scope.userAddModel.dob,'yyyy-MM-dd');
            }
            else
            {
                createClientParam.dob = '';
            }
            createClientParam.gender = $scope.userAddModel.gender;
            createClientParam.post_code = $scope.userAddModel.postcode;
            createClientParam.device_id = $scope.userAddModel.device_id;
            createClientParam.isactive = 1;
            createClientParam.imagepath = "";

            adminService.createUser(createClientParam, headers).then(function(response) {

                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.userlist')
                }
                $scope.isDataSavingProcess = false;

            });
        };

        $scope.getRoleList( );


    }]);




