
app.controller('customerAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', '$stateParams', 'APP_CONST', function ($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $stateParams, APP_CONST) {



        $scope.customerAddModel = {
            "id": 0,
            "customerno": "",
            "customerprefix": "",
            "name": "",
            "companyName": "",
            "email": "",
            "phone": "",
            "city": "",
            "state": "",
            "country": "",
            "postcode": "",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "iswelcomeEmail": true,
            "isactive": 1,
            "isPurchase": true,
            "isSale": true,
            "userList": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "billingCountryList": [],
            "billingStateList": [],
            "billingCityList": [],
            "shoppingCountryList": [],
            "shoppingStateList": [],
            "shoppingCityList": [],
            "contact_id": '',
            "deal_id": '',
            "acode": '',
            "dob": '',
            "special_date1": '',
            "special_date2": '',
            "special_date3": '',
            "special_date4": '',
            "special_date5": ''
        }

        $scope.customerDualAddModel = {
            "id": 0,
            "customer_id": 0,
            "name": "",
            "companyName": "",
            "email": "",
            "phone": "",
            "city": "",
            "state": "",
            "country": "",
            "postcode": "",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "iswelcomeEmail": true,
            "isactive": 1,
            "isPurchase": true,
            "isSale": true,
            "userList": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "billingCountryList": [],
            "billingStateList": [],
            "billingCityList": [],
            "shoppingCountryList": [],
            "shoppingStateList": [],
            "shoppingCityList": [],
            "contact_id": '',
            "deal_id": '',
            "acode": '',
            "dob": ''
        }

        $scope.customAttributeList = [];
        $scope.customAttributeBaseList = [];
        $scope.customAttributeDualSupportList = [];
        $scope.contactInfo = '';
        $scope.isLoadingProgress = false;
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_customer_form != 'undefined' && typeof $scope.create_customer_form.$pristine != 'undefined' && !$scope.create_customer_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function () {
            if (typeof $scope.create_customer_form != 'undefined')
            {
                $scope.create_customer_form.$setPristine();
                $scope.customerAddModel = {
                    "id": 0,
                    "customerno": "",
                    "customerprefix": "",
                    "name": "",
                    "companyName": "",
                    "email": "",
                    "phone": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "postcode": "",
                    "address": "",
                    "tags": "",
                    "currency": "",
                    "group": "",
                    "password": "",
                    "confirmpassword": "",
                    "iswelcomeEmail": true,
                    "isactive": 1,
                    "isPurchase": true,
                    "isSale": true,
                    "userList": [],
                    "billingaddr": '',
                    "billingcity": '',
                    "billingstate": '',
                    "billingpostcode": '',
                    "billingcountry": '',
                    "shoppingaddr": '',
                    "shoppingcity": '',
                    "shoppingstate": '',
                    "shoppingpostcode": '',
                    "shoppingcountry": '',
                    "isSameAddr": '',
                    "contact_id": '',
                    "deal_id": '',
                    "dob": '',
                    "special_date1": '',
                    "special_date2": '',
                    "special_date3": '',
                    "special_date4": '',
                    "special_date5": ''
                }
                $scope.customerDualAddModel = {
                    "id": 0,
                    "customer_id": 0,
                    "name": "",
                    "companyName": "",
                    "email": "",
                    "phone": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "postcode": "",
                    "address": "",
                    "tags": "",
                    "currency": "",
                    "group": "",
                    "password": "",
                    "confirmpassword": "",
                    "iswelcomeEmail": true,
                    "isactive": 1,
                    "isPurchase": true,
                    "isSale": true,
                    "userList": [],
                    "billingaddr": '',
                    "billingcity": '',
                    "billingstate": '',
                    "billingpostcode": '',
                    "billingcountry": '',
                    "shoppingaddr": '',
                    "shoppingcity": '',
                    "shoppingstate": '',
                    "shoppingpostcode": '',
                    "shoppingcountry": '',
                    "isSameAddr": '',
                    "contact_id": '',
                    "dob": "",
                    "special_date1": '',
                    "special_date2": '',
                    "special_date3": '',
                    "special_date4": '',
                    "special_date5": ''
                }
                $scope.checkId();
                $scope.customAttributeList = [];
                $scope.getCustomAttributeList();
            }
        }
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.dobDateOpen = false;
        $scope.specialDate1 = false;
        $scope.specialDate2 = false;
        $scope.specialDate3 = false;
        $scope.specialDate4 = false;
        $scope.specialDate5 = false;
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.dobDateOpen = true;
            }
            if (index == 1)
            {
                $scope.specialDate1 = true;
            }
            if (index == 2)
            {
                $scope.specialDate2 = true;
            }
            if (index == 3)
            {
                $scope.specialDate3 = true;
            }
            if (index == 4)
            {
                $scope.specialDate4 = true;
            }
            if (index == 5)
            {
                $scope.specialDate5 = true;
            }

        }
        $scope.getAcodeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.code = val;
            return $httpService.get(APP_CONST.API.ACODE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatAcodeModel = function (model)
        {
            if (model != null)
            {
                return model.code;
            }
            return  '';
        };
        $scope.updateShoppingAddr = function (string)
        {
            if ($scope.customerAddModel.isSameAddr == true)
            {
                $scope.customerAddModel.shoppingaddr = $scope.customerAddModel.billingaddr;
                $scope.customerAddModel.shoppingcity = $scope.customerAddModel.billingcity;
                $scope.customerAddModel.shoppingstate = $scope.customerAddModel.billingstate;
                $scope.customerAddModel.shoppingpostcode = $scope.customerAddModel.billingpostcode;
                $scope.customerAddModel.shoppingcountry = $scope.customerAddModel.billingcountry;
                $scope.customerDualAddModel.shoppingaddr = $scope.customerDualAddModel.billingaddr;
                $scope.customerDualAddModel.shoppingcity = $scope.customerDualAddModel.billingcity;
                $scope.customerDualAddModel.shoppingstate = $scope.customerDualAddModel.billingstate;
                $scope.customerDualAddModel.shoppingpostcode = $scope.customerDualAddModel.billingpostcode;
                $scope.customerDualAddModel.shoppingcountry = $scope.customerDualAddModel.billingcountry;
            } else if ($scope.customerAddModel.isSameAddr == false)
            {
                if (string != 'update')
                {
                    $scope.customerAddModel.shoppingaddr = '';
                    $scope.customerAddModel.shoppingcity = '';
                    $scope.customerAddModel.shoppingstate = '';
                    $scope.customerAddModel.shoppingpostcode = '';
                    $scope.customerAddModel.shoppingcountry = '';
                }
                $scope.customerDualAddModel.shoppingaddr = '';
                $scope.customerDualAddModel.shoppingcity = '';
                $scope.customerDualAddModel.shoppingstate = '';
                $scope.customerDualAddModel.shoppingpostcode = '';
                $scope.customerDualAddModel.shoppingcountry = '';
            }
        }

        $scope.createcustomer = function () {           
                $scope.isDataSavingProcess = true;
                var headers = {};
                headers['screen-code'] = 'customer';
                var createcustomerParam = {};
                createcustomerParam.id = 0;
                createcustomerParam.customer_no = $scope.customerAddModel.customerno;
                createcustomerParam.contact_id = $scope.customerAddModel.contact_id;
                createcustomerParam.prefix = $scope.customerAddModel.customerprefix;
                createcustomerParam.fname = $scope.customerAddModel.name;
                createcustomerParam.company = $scope.customerAddModel.companyName;
                createcustomerParam.email = $scope.customerAddModel.email;
                createcustomerParam.phone = $scope.customerAddModel.mobile;
                createcustomerParam.address = $scope.customerAddModel.address;
                createcustomerParam.city = $scope.customerAddModel.city;
                createcustomerParam.state = $scope.customerAddModel.state;
                createcustomerParam.country = $scope.customerAddModel.country;
                createcustomerParam.zip = $scope.customerAddModel.postcode;
                createcustomerParam.tags = $scope.customerAddModel.tags;
                createcustomerParam.currency = $scope.customerAddModel.currency;
                createcustomerParam.group = $scope.customerAddModel.group;
                createcustomerParam.password = $scope.customerAddModel.password;
                createcustomerParam.confirmPassword = $scope.customerAddModel.confirmpassword;
                createcustomerParam.iswelcomeEmail = ($scope.customerAddModel.iswelcomeEmail == true ? 1 : 0);
                createcustomerParam.is_purchase = $scope.customerAddModel.isPurchase == true ? 1 : 0;
                createcustomerParam.is_sale = $scope.customerAddModel.isSale == true ? 1 : 0;
                createcustomerParam.billing_address = $scope.customerAddModel.billingaddr;
                createcustomerParam.billing_city_id = $scope.customerAddModel.billingcity;
                createcustomerParam.billing_city = '';
                for (var i = 0; i < $scope.customerAddModel.billingCityList.length; i++)
                {
                    if ($scope.customerAddModel.billingCityList[i].id == $scope.customerAddModel.billingcity)
                    {
                        createcustomerParam.billing_city = $scope.customerAddModel.billingCityList[i].name;
                    }
                }
                createcustomerParam.billing_state_id = $scope.customerAddModel.billingstate;
                createcustomerParam.billing_state = "";
                for (var i = 0; i < $scope.customerAddModel.billingStateList.length; i++)
                {
                    if ($scope.customerAddModel.billingStateList[i].id == $scope.customerAddModel.billingstate)
                    {
                        createcustomerParam.billing_state = $scope.customerAddModel.billingStateList[i].name;
                    }
                }

                createcustomerParam.billing_country_id = $scope.customerAddModel.billingcountry;
                createcustomerParam.billing_country = "";
                for (var i = 0; i < $scope.customerAddModel.billingCountryList.length; i++)
                {
                    if ($scope.customerAddModel.billingCountryList[i].id == $scope.customerAddModel.billingcountry)
                    {
                        createcustomerParam.billing_country = $scope.customerAddModel.billingCountryList[i].name;
                    }
                }
                createcustomerParam.acode = '';
                if (typeof $scope.customerAddModel.acode != undefined && $scope.customerAddModel.acode != null && $scope.customerAddModel.acode != '')
                {
                    createcustomerParam.acode = $scope.customerAddModel.acode.code;
                }
                createcustomerParam.billing_pincode = $scope.customerAddModel.billingpostcode;
                createcustomerParam.shopping_address = $scope.customerAddModel.shoppingaddr;
                createcustomerParam.shopping_city_id = $scope.customerAddModel.shoppingcity;
                createcustomerParam.shopping_city = '';
                for (var i = 0; i < $scope.customerAddModel.shoppingCityList.length; i++)
                {
                    if ($scope.customerAddModel.shoppingCityList[i].id == $scope.customerAddModel.shoppingcity)
                    {
                        createcustomerParam.shopping_city = $scope.customerAddModel.shoppingCityList[i].name;
                    }
                }
                createcustomerParam.shopping_state_id = $scope.customerAddModel.shoppingstate;
                createcustomerParam.shopping_state = "";
                for (var i = 0; i < $scope.customerAddModel.shoppingStateList.length; i++)
                {
                    if ($scope.customerAddModel.shoppingStateList[i].id == $scope.customerAddModel.shoppingstate)
                    {
                        createcustomerParam.shopping_state = $scope.customerAddModel.shoppingStateList[i].name;
                    }
                }

                createcustomerParam.shopping_country_id = $scope.customerAddModel.shoppingcountry;
                createcustomerParam.shopping_country = "";
                for (var i = 0; i < $scope.customerAddModel.shoppingCountryList.length; i++)
                {
                    if ($scope.customerAddModel.shoppingCountryList[i].id == $scope.customerAddModel.shoppingcountry)
                    {
                        createcustomerParam.shopping_country = $scope.customerAddModel.shoppingCountryList[i].name;
                    }
                }


                createcustomerParam.shopping_pincode = $scope.customerAddModel.shoppingpostcode;
                createcustomerParam.is_active = 1;
                createcustomerParam.customattribute = [];
                for (var i = 0; i < $scope.customAttributeList.length; i++)
                {
                    var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                    if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                    {
                        continue;
                    }

                    var customattributeItem = {};
                    if ($scope.customAttributeList[i].value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].value;
                    } else if ($scope.customAttributeList[i].default_value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].default_value;
                    } else
                    {
                        customattributeItem.value = "";
                    }

                    customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                    createcustomerParam.customattribute.push(customattributeItem);
                }
                if (typeof $scope.customerAddModel.dob == 'object')
                {
                    $scope.customerAddModel.dob = utilityService.parseDateToStr($scope.customerAddModel.dob, 'yyyy-MM-dd');
                }
                createcustomerParam.dob = utilityService.changeDateToSqlFormat($scope.customerAddModel.dob, 'yyyy-MM-dd');
                if (createcustomerParam.dob == "")
                {
                    createcustomerParam.dob = null;
                }
                if (typeof $scope.customerAddModel.special_date1 == 'object')
                {
                    $scope.customerAddModel.special_date1 = utilityService.parseDateToStr($scope.customerAddModel.special_date1, 'yyyy-MM-dd');
                }

                createcustomerParam.special_date1 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date1, 'yyyy-MM-dd');
                if (createcustomerParam.special_date1 == "")
                {
                    createcustomerParam.special_date1 = '';
                }
                if (typeof $scope.customerAddModel.special_date2 == 'object')
                {
                    $scope.customerAddModel.special_date2 = utilityService.parseDateToStr($scope.customerAddModel.special_date2, 'yyyy-MM-dd');
                }
                createcustomerParam.special_date2 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date2, 'yyyy-MM-dd');
                if (createcustomerParam.special_date2 == "")
                {
                    createcustomerParam.special_date2 = '';
                }
                if (typeof $scope.customerAddModel.special_date3 == 'object')
                {
                    $scope.customerAddModel.special_date3 = utilityService.parseDateToStr($scope.customerAddModel.special_date3, 'yyyy-MM-dd');
                }
                createcustomerParam.special_date3 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date3, 'yyyy-MM-dd');
                if (createcustomerParam.special_date3 == "")
                {
                    createcustomerParam.special_date3 = '';
                }
                if (typeof $scope.customerAddModel.special_date4 == 'object')
                {
                    $scope.customerAddModel.special_date4 = utilityService.parseDateToStr($scope.customerAddModel.special_date4, 'yyyy-MM-dd');
                }
                createcustomerParam.special_date4 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date4, 'yyyy-MM-dd');
                if (createcustomerParam.special_date4 == "")
                {
                    createcustomerParam.special_date4 = '';
                }
                if (typeof $scope.customerAddModel.special_date5 == 'object')
                {
                    $scope.customerAddModel.special_date5 = utilityService.parseDateToStr($scope.customerAddModel.special_date5, 'yyyy-MM-dd');
                }
                createcustomerParam.special_date5 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date5, 'yyyy-MM-dd');
                if (createcustomerParam.special_date5 == "")
                {
                    createcustomerParam.special_date5 = '';
                }
                var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                if ($rootScope.appConfig.dual_language_support)
                {
                    configOption = adminService.handleOnlyErrorResponseConfig;
                }
                adminService.addCustomer(createcustomerParam, configOption, headers).then(function (response) {

                    if (response.data.success == true)
                    {
                        $scope.customerDualAddModel.customer_id = response.data.id;
                        if ($rootScope.appConfig.dual_language_support)
                        {
                            $scope.createcustomerDualInfo();
                        } else
                        {
                            if ($stateParams.deal_id != '' && $stateParams.deal_id != null && $stateParams.deal_id != 'undefined')
                            {
                                $scope.formReset();
                                $state.go('app.dealedit', {
                                    'id': $stateParams.deal_id
                                });
                                $scope.isDataSavingProcess = false;
                            } else if ($scope.customerAddModel.contact_id != '' && $scope.customerAddModel.contact_id != null && $scope.customerAddModel.contact_id != 'undefined')
                            {
                                $scope.formReset();
                                $state.go('app.contactEdit', {
                                    'id': $stateParams.id
                                });
                                $scope.isDataSavingProcess = false;
                            } else
                            {
                                $scope.formReset();
                                $state.go('app.customers');
                                $scope.isDataSavingProcess = false;
                            }
                        }
                    } else
                    {
                        $scope.isDataSavingProcess = false;
                    }
                });        
        };
        $scope.createcustomerDualInfo = function () {
            $scope.isDataSavingProcess = true;
            var headers = {};
            headers['screen-code'] = 'customer';
            var createcustomerParam = {};
            createcustomerParam.id = 0;
            createcustomerParam.customer_id = $scope.customerDualAddModel.customer_id;
            createcustomerParam.contact_id = $scope.customerDualAddModel.contact_id;
            createcustomerParam.fname = $scope.customerDualAddModel.name;
            createcustomerParam.company = $scope.customerDualAddModel.companyName;
            createcustomerParam.email = $scope.customerAddModel.email;
            createcustomerParam.phone = $scope.customerAddModel.mobile;
            createcustomerParam.address = $scope.customerAddModel.address;
            createcustomerParam.city = $scope.customerAddModel.city;
            createcustomerParam.state = $scope.customerAddModel.state;
            createcustomerParam.country = $scope.customerAddModel.country;
            createcustomerParam.zip = $scope.customerAddModel.postcode;
            createcustomerParam.tags = $scope.customerAddModel.tags;
            createcustomerParam.currency = $scope.customerAddModel.currency;
            createcustomerParam.group = $scope.customerAddModel.group;
            createcustomerParam.password = $scope.customerAddModel.password;
            createcustomerParam.confirmPassword = $scope.customerAddModel.confirmpassword;
            createcustomerParam.iswelcomeEmail = ($scope.customerAddModel.iswelcomeEmail == true ? 1 : 0);
            createcustomerParam.is_purchase = $scope.customerAddModel.isPurchase == true ? 1 : 0;
            createcustomerParam.is_sale = $scope.customerAddModel.isSale == true ? 1 : 0;
            createcustomerParam.billing_address = $scope.customerDualAddModel.billingaddr;
            createcustomerParam.billing_city = $scope.customerDualAddModel.billingcity;
            createcustomerParam.billing_state = $scope.customerDualAddModel.billingstate;
            createcustomerParam.billing_country = $scope.customerDualAddModel.billingcountry;
            createcustomerParam.billing_pincode = $scope.customerDualAddModel.billingpostcode;
            createcustomerParam.shopping_address = $scope.customerDualAddModel.shoppingaddr;
            createcustomerParam.shopping_city = $scope.customerDualAddModel.shoppingcity;
            createcustomerParam.shopping_state = $scope.customerDualAddModel.shoppingstate;
            createcustomerParam.shopping_country = $scope.customerDualAddModel.shoppingcountry;
            createcustomerParam.shopping_pincode = $scope.customerDualAddModel.shoppingpostcode;
            createcustomerParam.is_active = 1;
            createcustomerParam.customattribute = [];
            if (typeof $scope.customerAddModel.dob == 'object')
            {
                $scope.customerAddModel.dob = utilityService.parseDateToStr($scope.customerAddModel.dob, 'yyyy-MM-dd');
            }
            createcustomerParam.dob = utilityService.changeDateToSqlFormat($scope.customerAddModel.dob, 'yyyy-MM-dd');
            if (createcustomerParam.dob == "")
            {
                createcustomerParam.dob = null;
            }
            if (typeof $scope.customerAddModel.special_date1 == 'object')
            {
                $scope.customerAddModel.special_date1 = utilityService.parseDateToStr($scope.customerAddModel.special_date1, 'yyyy-MM-dd');
            }
            createcustomerParam.special_date1 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date1, 'yyyy-MM-dd');
            if (createcustomerParam.special_date1 == "")
            {
                createcustomerParam.special_date1 = '';
            }
            if (typeof $scope.customerAddModel.special_date2 == 'object')
            {
                $scope.customerAddModel.special_date2 = utilityService.parseDateToStr($scope.customerAddModel.special_date2, 'yyyy-MM-dd');
            }
            createcustomerParam.special_date2 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date2, 'yyyy-MM-dd');
            if (createcustomerParam.special_date2 == "")
            {
                createcustomerParam.special_date2 = '';
            }
            if (typeof $scope.customerAddModel.special_date3 == 'object')
            {
                $scope.customerAddModel.special_date3 = utilityService.parseDateToStr($scope.customerAddModel.special_date3, 'yyyy-MM-dd');
            }
            createcustomerParam.special_date3 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date3, 'yyyy-MM-dd');
            if (createcustomerParam.special_date3 == "")
            {
                createcustomerParam.special_date3 = '';
            }
            if (typeof $scope.customerAddModel.special_date4 == 'object')
            {
                $scope.customerAddModel.special_date4 = utilityService.parseDateToStr($scope.customerAddModel.special_date4, 'yyyy-MM-dd');
            }
            createcustomerParam.special_date4 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date4, 'yyyy-MM-dd');
            if (createcustomerParam.special_date4 == "")
            {
                createcustomerParam.special_date4 = '';
            }
            if (typeof $scope.customerAddModel.special_date5 == 'object')
            {
                $scope.customerAddModel.special_date5 = utilityService.parseDateToStr($scope.customerAddModel.special_date5, 'yyyy-MM-dd');
            }
            createcustomerParam.special_date5 = utilityService.changeDateToSqlFormat($scope.customerAddModel.special_date5, 'yyyy-MM-dd');
            if (createcustomerParam.special_date5 == "")
            {
                createcustomerParam.special_date5 = '';
            }
            createcustomerParam.acode = '';
            if (typeof $scope.customerAddModel.acode != undefined && $scope.customerAddModel.acode != null && $scope.customerAddModel.acode != '')
            {
                createcustomerParam.acode = $scope.customerAddModel.acode.code;
            }
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                {
                    var customattributeItem = {};
                    if ($scope.customAttributeList[i].value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].value;
                    } else if ($scope.customAttributeList[i].default_value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].default_value;
                    } else
                    {
                        customattributeItem.value = "";
                    }

                    customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                    createcustomerParam.customattribute.push(customattributeItem);
                }



            }
            var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
            adminService.addCustomerDualInfo(createcustomerParam, configOption, headers).then(function (response) {

                if (response.data.success == true)
                {
                    if ($stateParams.deal_id != '' && $stateParams.deal_id != null && $stateParams.deal_id != 'undefined')
                    {
                        $scope.formReset();
                        $state.go('app.dealedit', {
                            'id': $stateParams.deal_id
                        });
                        $scope.isDataSavingProcess = false;
                    } else if ($scope.customerAddModel.contact_id != '' && $scope.customerAddModel.contact_id != null && $scope.customerAddModel.contact_id != 'undefined')
                    {
                        $scope.formReset();
                        $state.go('app.contactEdit', {
                            'id': $stateParams.id
                        });
                        $scope.isDataSavingProcess = false;
                    } else
                    {
                        $scope.formReset();
                        $state.go('app.customers');
                    }
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "crm";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $scope.customAttributeList = data;
                $scope.customAttributeBaseList = [];
                $scope.customAttributeDualSupportList = [];
                if ($rootScope.appConfig.dual_language_support)
                {
                    for (var i = 0; i < $scope.customAttributeList.length; i++)
                    {
                        var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                        if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                        {
                            $scope.customAttributeDualSupportList.push($scope.customAttributeList[i]);
                        } else
                        {
                            $scope.customAttributeBaseList.push($scope.customAttributeList[i]);
                        }
                    }
                } else
                {
                    $scope.customAttributeBaseList = $scope.customAttributeList;
                }

            });
        };
        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });
        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.getCustomAttributeList();
        /*
         * Listening the Custom field value change
         */
        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });
        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }




        $scope.getCountryList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.code = "";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                $timeout(function () {
                    $scope.customerAddModel.billingCountryList = data;
                    $scope.customerAddModel.shoppingCountryList = data;
                    $scope.customerDualAddModel.billingCountryList = data;
                    $scope.customerDualAddModel.shoppingCountryList = data;
                });
            });
        };
        $scope.initBillingAddressStateListTimeoutPromise = null;
        $scope.initBillingAddressStateList = function (searchkey)
        {
            if ($scope.initBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressStateListTimeoutPromise);
            }
            $scope.initBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'billingstate');
            }, 300);
        };
        $scope.initShippingAddressStateListTimeoutPromise = null;
        $scope.initShippingAddressStateList = function (searchkey)
        {
            if ($scope.initShippingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initShippingAddressStateListTimeoutPromise);
            }
            $scope.initShippingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'shippingstate');
            }, 300);
        };
        $scope.initDualBillingAddressStateListTimeoutPromise = null;
        $scope.initDualBillingAddressStateList = function (searchkey)
        {
            if ($scope.initDualBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualBillingAddressStateListTimeoutPromise);
            }
            $scope.initDualBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'dualbillingstate');
            }, 300);
        };
        $scope.initDualShippingAddressStateListTimeoutPromise = null;
        $scope.initDualShippingAddressStateList = function (searchkey)
        {
            if ($scope.initDualShippingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualShippingAddressStateListTimeoutPromise);
            }
            $scope.initDualShippingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'dualshippingstate');
            }, 300);
        };
        $scope.getStateList = function (searchkey, fieldName) {

            console.log('searchkey  = ' + searchkey);
            console.log('fieldName  = ' + fieldName);
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_id = "";
            if (fieldName == 'billingstate')
            {
                getListParam.country_id = $scope.customerAddModel.billingcountry;
            } else if (fieldName == 'shippingstate')
            {
                getListParam.country_id = $scope.customerAddModel.shoppingcountry;
            } else if (fieldName == 'dualbillingstate')
            {
                getListParam.country_id = $scope.customerDualAddModel.shoppingcountry;
            } else if (fieldName == 'dualshippingstate')
            {
                getListParam.country_id = $scope.customerDualAddModel.shoppingcountry;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.fieldName = fieldName;
            configOption.searchkey = searchkey;
            adminService.getStateList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined'
                        && response.config.config.fieldName != undefined && response.config.config.searchkey != undefined)
                {

                    //                    if (data.length == 0)
                    //                    {
                    //                        data.push({"name": response.config.config.searchkey});
                    //                    }
                    $timeout(function () {
                        if (fieldName == 'billingstate')
                        {
                            $scope.customerAddModel.billingStateList = data;
                        } else if (fieldName == 'shippingstate')
                        {
                            $scope.customerAddModel.shoppingStateList = data;
                        } else if (fieldName == 'dualbillingstate')
                        {
                            $scope.customerDualAddModel.billingStateList = data;
                        } else if (fieldName == 'dualshippingstate')
                        {
                            $scope.customerDualAddModel.shoppingStateList = data;
                        }
                    }, 0);
                }

            });
        };
        $scope.initBillingAddressCityListTimeoutPromise = null;
        $scope.initBillingAddressCityList = function (searchkey)
        {
            if ($scope.initBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressCityListTimeoutPromise);
            }
            $scope.initBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'billingcity');
            }, 300);
        };
        $scope.initShippingAddressCityListTimeoutPromise = null;
        $scope.initShippingAddressCityList = function (searchkey)
        {
            if ($scope.initShippingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initShippingAddressCityListTimeoutPromise);
            }
            $scope.initShippingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'shippingcity');
            }, 300);
        };
        $scope.initDualBillingAddressCityListTimeoutPromise = null;
        $scope.initDualBillingAddressCityList = function (searchkey)
        {
            if ($scope.initDualBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualBillingAddressCityListTimeoutPromise);
            }
            $scope.initDualBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'dualbillingcity');
            }, 300);
        };
        $scope.initDualShippingAddressCityListTimeoutPromise = null;
        $scope.initDualShippingAddressCityList = function (searchkey)
        {
            if ($scope.initDualShippingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualShippingAddressCityListTimeoutPromise);
            }
            $scope.initDualShippingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'dualshippingcity');
            }, 300);
        };
        $scope.getCityList = function (searchkey, fieldName) {

            console.log('searchkey  = ' + searchkey);
            console.log('fieldName  = ' + fieldName);
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_name = "";
            getListParam.state_name = "";
            if (fieldName == 'billingcity')
            {
                getListParam.state_id = $scope.customerAddModel.billingstate;
                getListParam.country_id = $scope.customerAddModel.billingcountry;
            } else if (fieldName == 'shippingcity')
            {
                getListParam.state_id = $scope.customerAddModel.shoppingstate;
                getListParam.country_id = $scope.customerAddModel.shoppingcountry;
            } else if (fieldName == 'dualbillingcity')
            {
                getListParam.state_id = $scope.customerDualAddModel.billingstate;
                getListParam.country_id = $scope.customerDualAddModel.billingcountry;
            } else if (fieldName == 'dualshippingcity')
            {
                getListParam.state_id = $scope.customerDualAddModel.shoppingstate;
                getListParam.country_id = $scope.customerDualAddModel.shoppingcountry;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.fieldName = fieldName;
            configOption.searchkey = searchkey;
            adminService.getCityList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined'
                        && response.config.config.fieldName != undefined && response.config.config.searchkey != undefined)
                {

                    //                    if (data.length == 0)
                    //                    {
                    //                        data.push({"name": response.config.config.searchkey});
                    //                    }
                    $timeout(function () {
                        if (fieldName == 'billingcity')
                        {
                            $scope.customerAddModel.billingCityList = data;
                        } else if (fieldName == 'shippingcity')
                        {
                            $scope.customerAddModel.shoppingCityList = data;
                        } else if (fieldName == 'dualbillingcity')
                        {
                            $scope.customerDualAddModel.billingCityList = data;
                        } else if (fieldName == 'dualshippingcity')
                        {
                            $scope.customerDualAddModel.shoppingCityList = data;
                        }
                    }, 0);
                }

            });
        };
        $scope.billingAddressCountryChange = function ()
        {
            $scope.customerAddModel.billingstate = '';
            $scope.customerAddModel.billingcity = '';
            $scope.customerAddModel.billingCityList = [];
            $scope.customerAddModel.billingStateList = [];
            $scope.updateShoppingAddr();
        }

        $scope.billingAddressStateChange = function ()
        {

            $scope.customerAddModel.billingcity = '';
            $scope.customerAddModel.billingCityList = [];
            $scope.updateShoppingAddr();
        }

        $scope.shippingAddressCountryChange = function ()
        {
            $scope.customerAddModel.shoppingstate = '';
            $scope.customerAddModel.shoppingcity = '';
            $scope.customerAddModel.shoppingCityList = [];
            $scope.customerAddModel.shoppingStateList = [];
            $scope.updateShoppingAddr('update');
        }

        $scope.shippingAddressStateChange = function ()
        {
            $scope.customerAddModel.shoppingcity = '';
            $scope.customerAddModel.shoppingCityList = [];
            $scope.updateShoppingAddr('update');
        }
        $scope.contactView = false;
        $scope.fromDeal = false;
        $scope.checkId = function ()
        {
            $scope.contactView = false;
            $scope.fromDeal = false;
            if ($stateParams.id != '' && typeof $stateParams.id != 'undefined' && $stateParams.id != null)
            {
                $scope.getContactInfo($stateParams.id);
                $scope.contactView = true;
            }
            if ($stateParams.deal_id != '' && typeof $stateParams.deal_id != 'undefined' && $stateParams.deal_id != null)
            {
                $scope.fromDeal = true;
            }
        }


        $scope.getContactInfo = function (id)
        {
            if (typeof id != 'undefined')
            {
                $scope.isLoadingProgress = true;
                var contactListParam = {};
                contactListParam.id = id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getContactList(contactListParam, configOption).then(function (response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.contactInfo = data.list[0];
                    }
                    $scope.updateContactInfo(id);
                });
            }
        };
        $scope.updateContactInfo = function (id)
        {
            $scope.customerAddModel.name = $scope.contactInfo.fname;
            $scope.customerAddModel.companyName = $scope.contactInfo.company_name;
            $scope.customerAddModel.email = $scope.contactInfo.email;
            $scope.customerAddModel.mobile = $scope.contactInfo.phone;
            $scope.customerAddModel.contact_id = id;
            $scope.customerDualAddModel.contact_id = id;
        }

        $scope.checkId();
        $scope.getCountryList();
    }]);




