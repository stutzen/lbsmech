

app.controller('newContactAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.contactModel = {
            id: '',
            fname: '',
            lname:'',
            isActive: true,
            email:'',
            phone:'',
            company:'',
            notes: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_contact_form != 'undefined' && typeof $scope.add_contact_form.$pristine != 'undefined' && !$scope.add_contact_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_contact_form.$setPristine();
            if (typeof $scope.add_contact_form != 'undefined')
            {
                $scope.contactModel.fname = "";
                 $scope.contactModel.lname = "";
                $scope.contactModel.email = "";
                $scope.contactModel.phone = "";
                $scope.contactModel.company = "";
                $scope.contactModel.notes = "";
                $scope.contactModel.isActive = true;
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.createcontact = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addContactParam = {};
            var headers = {};
            headers['screen-code'] = 'contact';
            addContactParam.id = 0;
            addContactParam.fname = $scope.contactModel.fname;
            addContactParam.lname = $scope.contactModel.lname;
            addContactParam.email = $scope.contactModel.email;
            addContactParam.phone = $scope.contactModel.phone;
            addContactParam.company = $scope.contactModel.company;
            addContactParam.notes = $scope.contactModel.notes;
            addContactParam.is_active = $scope.contactModel.isActive == true ? 1 : 0;

            adminService.saveContact(addContactParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    addContactParam.id = response.data.id;
                    $scope.isDataSavingProcess = false;
                     
                    $rootScope.$broadcast('updateSavedContactDetail', addContactParam);
                }
                
            });
        };

    }]);





