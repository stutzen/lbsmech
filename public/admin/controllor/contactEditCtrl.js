

app.controller('contactEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function ($scope, $rootScope, adminService, utilityService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.contactModel = {
            id: '',
            fname: '',
            lname: '',
            isActive: true,
            list: {},
            dealList: [],
            email: '',
            phone: '',
            pin_code: '',
            companyList: [],
            company: '',
            currentPage: 1,
            total: 0,
            limit: 50,
            notes: '',
            companyInfo: '',
            sourceList: [],
            attachments: []
        }
        $scope.pagePerCount = [50, 100];
        $scope.contactModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.stateParamData = $stateParams.id;

        $scope.contactModel.isLoadingProgress = false;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.edit_contact_form != 'undefined' && typeof $scope.edit_contact_form.$pristine != 'undefined' && !$scope.edit_contact_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.edit_contact_form.$setPristine();
            if (typeof $scope.edit_contact_form != 'undefined')
            {
                $scope.contactModel.fname = "";
                $scope.contactModel.lname = "";
                $scope.contactModel.isActive = true;
                $scope.contactModel.email = "";
                $scope.contactModel.phone = "";
                $scope.contactModel.pin_code = "";
                $scope.contactModel.company = "";
                $scope.contactModel.notes = "";
                $scope.contactModel.companyInfo = "";
            }
            $scope.updateContactInfo();
        }
        $scope.isSourceLoaded = false;
        $scope.isLeadTypeLoaded = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.initUpdateTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateTimeoutPromise);
            }
            if ($scope.isSourceLoaded == true && $scope.isLeadTypeLoaded == true)
            {
                $scope.updateContactInfo();
            } else
            {
                $scope.initUpdateTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        };
        $scope.modifycontact = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addContactParam = {};
            var headers = {};
            headers['screen-code'] = 'contact';
            addContactParam.id = $stateParams.id;
            addContactParam.fname = $scope.contactModel.fname;
            addContactParam.lname = $scope.contactModel.lname;
            addContactParam.email = $scope.contactModel.email;
            addContactParam.phone = $scope.contactModel.phone;
            addContactParam.pin_code = $scope.contactModel.pin_code;
            addContactParam.company_id = $scope.contactModel.companyInfo.id;
            addContactParam.contact_owner_id = $scope.contactModel.empInfo.id;
            addContactParam.source_id = $scope.contactModel.source;
            addContactParam.lead_type_id = $scope.contactModel.lead;
            addContactParam.is_active = $scope.contactModel.isActive;
            addContactParam.notes = $scope.contactModel.notes;
            addContactParam.attachment = [];
            for (var i = 0; i < $scope.contactModel.attachments.length; i++)
            {
                var imageParam = {};
                imageParam.id = $scope.contactModel.attachments[i].id;
                imageParam.url = $scope.contactModel.attachments[i].url;
                imageParam.ref_id = $scope.contactModel.attachments[i].ref_id;
                imageParam.type = $scope.contactModel.attachments[i].type;
                addContactParam.attachment.push(imageParam);
            }
            adminService.editContact(addContactParam, $scope.contactModel.id, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.contact');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.redirect = function ()
        {
            $state.go('app.dealadd', {
                'id': $stateParams.id
            });
        };

        $scope.redirectToCustomer = function ()
        {
            $state.go('app.customeradd', {
                'id': $stateParams.id
            });
        };
        $scope.checkId = function ()
        {
            if ($stateParams.id != '' && $stateParams.id != undefined && $stateParams.company_id != '' && $stateParams.company_id != undefined)
            {
                var id = $stateParams.company_id;
                $scope.getCompanyInfo(id);

            }
        }
        $scope.getCompanyInfo = function (id)
        {
            $scope.companyLoaded = false;
            if (id != 'undefined' && id != '')
            {

                var companyListParam = {};
                companyListParam.id = $stateParams.company_id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getCompanyList(companyListParam, configOption).then(function (response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.companyInfo = data.list[0];
                        $scope.companyLoaded = true;
                    }
                });
            }
        };

        $scope.updateContactInfo = function ()
        {
            if ($stateParams.company_id != '' && $stateParams.company_id != undefined)
            {
                $scope.contactModel.companyInfo = {
                    name: $scope.companyInfo.name,
                    id: $scope.companyInfo.id
                }
            } else
            {
                $scope.contactModel.companyInfo = {
                    name: $scope.contactModel.list.company_name,
                    id: $scope.contactModel.list.company_id
                }
            }
            $scope.contactModel.empInfo = {
                f_name: $scope.contactModel.list.contact_owner_name,
                id: $scope.contactModel.list.contact_owner_id
            }
            $scope.contactModel.is_active = $scope.contactModel.list.is_active;
            $scope.contactModel.fname = $scope.contactModel.list.fname;
            $scope.contactModel.lname = $scope.contactModel.list.lname;
            $scope.contactModel.isLoadingProgress = false;
            $scope.contactModel.id = $scope.contactModel.list.id;
            $scope.contactModel.email = $scope.contactModel.list.email;
            $scope.contactModel.phone = $scope.contactModel.list.phone;
            $scope.contactModel.pin_code = $scope.contactModel.list.pin_code;
            $scope.contactModel.company = $scope.contactModel.list.company_id + '';
            $scope.contactModel.notes = $scope.contactModel.list.notes;
            $scope.contactModel.source = $scope.contactModel.list.source_id + '';
            $scope.contactModel.lead = $scope.contactModel.list.lead_type_id + '';
            $scope.contactModel.attachments = $scope.contactModel.list.attachment;
            for (var i = 0; i < $scope.contactModel.attachments.length; i++)
            {
                $scope.contactModel.attachments[i].path = $scope.contactModel.attachments[i].url;
            }
        }
        $scope.getContactInfo = function ()
        {

            if (typeof $stateParams.id != 'undefined')
            {
                $scope.contactModel.isLoadingProgress = true;
                var headers = {};
                headers['screen-code'] = 'contact';
                var countryListParam = {};
                countryListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getContactList(countryListParam, configOption, headers).then(function (response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.contactModel.list = data.list[0];

                    }

                    $scope.initUpdateDetail();

                });
            }
        };

        $scope.getDealList = function ()
        {
            $scope.contactModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.contact_id = $stateParams.id;
            getListParam.start = ($scope.contactModel.currentPage - 1) * $scope.contactModel.limit;
            getListParam.limit = $scope.contactModel.limit;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDealList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.contactModel.dealList = data.list;
                    $scope.contactModel.total = data.total;
                    for (var i = 0; i < $scope.contactModel.dealList.length; i++)
                    {
                        $scope.contactModel.dealList[i].amount = utilityService.changeCurrency($scope.contactModel.dealList[i].amount, $rootScope.appConfig.thousand_seperator);
                        var datevalue = moment($scope.contactModel.dealList[i].next_follow_up).valueOf();
                        if (datevalue > 0)
                        {
                            var newdate = new Date($scope.contactModel.dealList[i].next_follow_up).toISOString();
                            $scope.contactModel.dealList[i].next_follow_up = newdate;
                        } else
                        {
                            $scope.contactModel.dealList[i].next_follow_up = '';
                        }
                        $scope.contactModel.isLoadingProgress = false;
                    }
                }
            });
        };
        $scope.showCusPopup = false;
        $scope.showCustomerPopup = function ()
        {

            $scope.getcustomerList();
            $scope.showCusPopup = true;

        };
        $scope.closeCustomerPopup = function ()
        {
            $scope.isDataSavingProcess = false;
            $scope.showCusPopup = false;
        };
        $scope.getSourceList = function () {
            var getSourceListParam = {};
            getSourceListParam.is_active = 1;
            $scope.isSourceLoaded = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getSourceList(getSourceListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    $scope.contactModel.sourceList = response.data.list;
                }
                $scope.isSourceLoaded = true;
            });
        };

        $scope.getcustomerList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'customer';
            getListParam.contact_id = $stateParams.id;

            getListParam.start = ($scope.contactModel.currentPage - 1) * $scope.contactModel.limit;
            getListParam.limit = $scope.contactModel.limit;
            $scope.contactModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCustomersList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.contactModel.sourceList = data;
                    //$scope.contactModel.total = response.data.total;
                }
                $scope.contactModel.isLoadingProgress = false;
            });
        };
        $scope.getLeadTypeList = function ()
        {
            $scope.contactModel.isLoadingProgress = true;
            var leadtypeListParam = {};
            $scope.isLeadTypeLoaded = false;
            leadtypeListParam.is_active = 1;
            leadtypeListParam.type = 'lead';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getLeadTypeList(leadtypeListParam, configOption).then(function (response)
            {
                if (response.data.success == true)
                {
                    var data = response.data;
                    $scope.contactModel.leadList = data.list;
                    $scope.isLeadTypeLoaded = true;
                }
                $scope.contactModel.isLoadingProgress = false;
            });

        }
        $scope.getCompanyList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCompanyModel = function (model)
        {
            if (model != null)
            {

                return model.name;

            }
            return  '';
        };
        $scope.showCompanyPopup = false;
        $scope.showAddCompanyPopup = function ()
        {
            $scope.showCompanyPopup = true;
            $rootScope.$broadcast('companyInfo', $scope.contactModel.companyInfo);
        }

        $scope.$on("updateSavedCompanyDetail", function (event, companyDetail)
        {
            if (typeof companyDetail === 'undefined')
                return;
            $scope.contactModel.companyInfo = companyDetail;
            $scope.closeCompanyPopup();
        });

        $scope.closeCompanyPopup = function ()
        {
            $scope.showCompanyPopup = false;
        }

        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function (index, attachindex)
        {
            if (index == 'fileupload')
            {
                $scope.isImageSavingProcess = false;
                $scope.showUploadMoreFilePopup = true;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }
        }
        $scope.closePopup = function (index)
        {
            if (index == 'fileupload')
            {
                $scope.showUploadMoreFilePopup = false;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }
        }

        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data, category, imgcaption, index)
        {
            $scope.uploadedFileQueue = data;
            if (typeof category != undefined && category != null && category != '')
            {
                $scope.uploadedFileQueue[index].category = category;
            }
            if (typeof imgcaption != undefined && imgcaption != null && imgcaption != '')
            {
                $scope.uploadedFileQueue[index].imgcaption = imgcaption;
            }
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
        }

        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.showUploadMoreFilePopup)
            {
                if ($scope.contactModel.attachments.length > index)
                {
                    $scope.contactModel.attachments.splice(index, 1);
                }
            }
        }


        $scope.deleteImage = function ($index)
        {
            $scope.contactModel.attachments.splice($index, 1);
        }

        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                if ($scope.showUploadMoreFilePopup)
                {
                    $scope.closePopup('fileupload');
                }
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    //               $scope.contactModel.attachments.push($scope.uploadedFileQueue[i]);
                    var imageUpdateParam = {};
                    imageUpdateParam.id = 0;
                    imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                    imageUpdateParam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                    imageUpdateParam.ref_id = $stateParams.id;
                    imageUpdateParam.type = 'contact';
                    imageUpdateParam.comments = '';
                    imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
                    imageUpdateParam.path = $scope.uploadedFileQueue[i].urlpath;
                    $scope.contactModel.attachments.push(imageUpdateParam);
                    if (i == $scope.uploadedFileQueue.length - 1)
                    {
                        $scope.closePopup('fileupload');
                    }
                }
            } else
            {
                $scope.closePopup('fileupload');
            }
        };

        $scope.getEmployeeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            if (autosearchParam.fname != '')
            {
                return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatEmployeeModel = function (model)
        {
            if (model != null)
            {
                if (model.f_name != undefined && model.id != undefined && model.emp_code != undefined)
                {
                    return model.f_name + '(' + model.id + ',' + model.emp_code + ')';
                } else if (model.f_name != undefined && model.id != undefined)
                {
                    return model.f_name + '(' + model.id + ')';
                }
            }
            return  '';
        };
        $scope.checkId();
        $scope.getSourceList();
        $scope.getDealList();
        $scope.getContactInfo();
        $scope.getLeadTypeList();
    }]);






