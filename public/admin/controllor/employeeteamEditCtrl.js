




app.controller('employeeteamEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.teamModel = {
            "name": "",
            "count": "",
            "current_count": "",
            "is_active": '',
            "user_id": "",
            "user_name": "",
            "userInfo": "",
            "list": ""
        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.edit_employeeteam_form !== 'undefined' && typeof $scope.edit_employeeteam_form.$pristine !== 'undefined' && !$scope.edit_employeeteam_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            $scope.edit_employeeteam_form.$setPristine();
            $scope.updateEmployeeTeamInfo();

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        
        $scope.modifyEmployeeTeam = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var modifyEmployeeTeamParam = {};
                var headers = {};
                headers['screen-code'] = 'team';
                modifyEmployeeTeamParam.name = $scope.teamModel.name;
                modifyEmployeeTeamParam.count = $scope.teamModel.count;
                modifyEmployeeTeamParam.id = $scope.teamModel.id;
                modifyEmployeeTeamParam.current_count = $scope.teamModel.current_count;
                modifyEmployeeTeamParam.is_active = $scope.teamModel.is_active;
                if($scope.teamModel.employeeInfo != undefined && $scope.teamModel.employeeInfo !=null && $scope.teamModel.employeeInfo !='')
                {   
                modifyEmployeeTeamParam.emp_name = $scope.teamModel.employeeInfo.f_name;
                modifyEmployeeTeamParam.emp_id = $scope.teamModel.employeeInfo.id;
                }
                else
                {
                    modifyEmployeeTeamParam.emp_name = '';
                    modifyEmployeeTeamParam.emp_id = '';
                }    
                adminService.modifyEmployeeTeam(modifyEmployeeTeamParam, $scope.teamModel.id, headers).then(function(response) {
                    if (response.data.success === true)
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.formReset();
                        $state.go('app.employeeteamlist');
                    }
                });
            }
        };

        $scope.updateEmployeeTeamInfo = function()
        {
            if ($scope.teamModel.list != null)
            {                
                $scope.teamModel.name = $scope.teamModel.list.name;
                $scope.teamModel.count = $scope.teamModel.list.count;
                $scope.teamModel.id = $scope.teamModel.list.id;
                $scope.teamModel.current_count = $scope.teamModel.list.current_count;
                $scope.teamModel.employeeInfo = {
                    id : $scope.teamModel.list.emp_id,
                    f_name : $scope.teamModel.list.emp_name
                };
                $scope.teamModel.is_active = $scope.teamModel.list.is_active;
            }
        };

        $scope.getEmployeeTeamInfo = function() {

            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getEmployeeTeamList(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.teamModel.list = data.list[0];
                        $scope.updateEmployeeTeamInfo();
                    }

                });
            }
        };

        $scope.getEmployeelist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeeModel = function (model) {

            if (model !== null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };

        $scope.getEmployeeTeamInfo();

    }]);




