





app.controller('stocktracelistCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', 'APP_CONST', '$window', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, APP_CONST, $window, $httpService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.stocktracelistModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            companyname: '',
            companyno: '',
            companycode: '',
            companyphone: '',
            companymail: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false

        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [10, 20, 50, 100];
        $scope.stocktracelistModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        //$scope.dateFormat = 'dd-MM-yyyy';
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)

        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            type: ''
        };
        $scope.searchFilter.fromdate = $scope.currentDate;
        //$scope.searchFilter.fromdate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                productInfo: {}

            };
            $scope.stocktracelistModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
            //$scope.searchFilter.fromdate = $filter('date')($scope.currentDate, $scope.dateFormat);

        }

        // $scope.avaLangsupport = [{"id": 5, "name": "tamil"}, {"id": 6, "name": "tamil1"}, {"id": 7, "name": "tamil2"}];

        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
//        $scope.showSaveShoplistPopup = false;
//        $scope.showPopup = function() {
//
//            $scope.showSaveShoplistPopup = true;
//            $scope.shoplistModel.items = '';
//            $scope.shoplistModel.shopitemamt = '';
//        };
//        $scope.closePopup = function() {
//
//            $scope.showSaveShoplistPopup = false;
//        };
//        $scope.issaveNameLoadingProgress = false;
//        $scope.saveName = function() {
//
//            var saveNameParam = {};
//
//            saveNameParam.productname = $scope.stocktracelistModel.id;
//            saveNameParam.uom = $scope.stocktracelistModel.product_id;
//            saveNameParam.description = $scope.stocktracelistModel.sku;
//            saveNameParam.quantity = $scope.stocktracelistModel.uom_name;
//            saveNameParam.stockvalue = $scope.stocktracelistModel.total_stock;
//
//            $scope.issaveNameLoadingProgress = true;
//            adminService.saveName(saveNameParam).then(function(response) {
//                var data = response.data.list;
//                if (data.success == true)
//                {
//                    $scope.issaveNameLoadingProgress = false;
//                    $scope.closePopup();
//                    $state.go('app.stocktracelist');
//                    //$scope.getList();
//                }
//
//            });
//        };
        $scope.$watch('searchFilter.productInfo', function(newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal === '')
            {
//            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
                {
                    $scope.initTableFilter();
                }
            }
        });
        $scope.formatproductlistModel = function(model) {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };


        $scope.getproductlist = function(val) {

            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = "Product";
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData) {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.getList = function()
        {
            var headers = {};
            headers['screen-code'] = 'stocktrace';
            var getListParam = {};
            //getListParam.fromDate = $scope.searchFilter.fromdate;
            //getListParam.toDate = $scope.searchFilter.todate;

            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            }
            else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            }
            else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }
//            getListParam.fromDate = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
//            getListParam.toDate = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');
//            if (getListParam.fromDate === '' || getListParam.toDate === '')
//            {
//                if (getListParam.fromDate !== '')
//                {
//                    getListParam.toDate = getListParam.fromDate;
//                }
//                if (getListParam.toDate !== '')
//                {
//                    getListParam.fromDate = getListParam.toDate;
//                }
//            }

            getListParam.stocktracelist = $scope.searchFilter.stocktracelist;
            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
            {
                getListParam.product_id = $scope.searchFilter.productInfo.id;
            }
            else
            {
                getListParam.product_id = '';
            }
            $scope.stocktracelistModel.isLoadingProgress = true;
            $scope.stocktracelistModel.isSearchLoadingProgress = true;
            adminService.getstocktracelist(getListParam, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.stocktracelistModel.list = data;
                    for (var i = 0; i < $scope.stocktracelistModel.list.length; i++)
                    {
                        $scope.stocktracelistModel.list[i].newdate = utilityService.parseStrToDate($scope.stocktracelistModel.list[i].fromdate);
                        $scope.stocktracelistModel.list[i].fromdate = utilityService.parseDateToStr($scope.stocktracelistModel.list[i].newdate, $scope.dateFormat);
                    }
                    $scope.stocktracelistModel.total = data.total;
                }
                $scope.stocktracelistModel.isLoadingProgress = false;
                $scope.stocktracelistModel.isSearchLoadingProgress = false;
            });

        };
        $scope.print = function()
        {
            $window.print();
        };


        //      $scope.getList();
    }]);
