
app.controller('expensetrackercategoryReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', '$localStorageService', '$localStorage', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, $localStorageService, $localStorage) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.expenCategoryModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            length: [],
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            teamList:[]
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.expenCategoryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0));
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.company_name = $rootScope.appConfig.app_tile;
        $scope.company_address = $rootScope.appConfig.pay_to_address;
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            teamid:''
        };
        $scope.searchFilter.todate = $scope.currentDate;
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                teamid:''
            };
            $scope.expenCategoryModel.list = [];
            $scope.searchFilter.todate = $scope.currentDate;
        }

        $scope.print = function()
        {
            $window.print();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.show = function(index)
        {
//            for (var i = 0; i < $scope.expenCategoryModel.list[index].data; i++)
//            {
//                if ($scope.expenCategoryModel.list[index].data[i].flag == index)
//                {
            if ($scope.showDetails[index])
            {
                $scope.showDetails[index] = false;
            }
            else
                $scope.showDetails[index] = true;
//                }
//            }
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'expensetrackerreport';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.toDate = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.toDate = utilityService.changeDateToSqlFormat(getListParam.toDate, $scope.dateFormat);
                getListParam.fromDate = getListParam.toDate;
            }
            else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.fromDate = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.fromDate = utilityService.changeDateToSqlFormat(getListParam.fromDate, $scope.dateFormat);
                getListParam.toDate = getListParam.fromDate;
            }
            else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.toDate = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.fromDate = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.toDate = utilityService.changeDateToSqlFormat(getListParam.toDate, $scope.dateFormat);
                getListParam.fromDate = utilityService.changeDateToSqlFormat(getListParam.fromDate, $scope.dateFormat);
            }
            if ($scope.searchFilter.employeeInfo != null && typeof $scope.searchFilter.employeeInfo != 'undefined' && typeof $scope.searchFilter.employeeInfo.id != 'undefined')
            {
                getListParam.empId = $scope.searchFilter.employeeInfo.id;
            } else
            {
                getListParam.empId = '';
            }
            getListParam.teamId = $scope.searchFilter.teamid;
            getListParam.show_all = 1;
            $scope.expenCategoryModel.isLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getExpenseTrackerUserBasedReport(getListParam, configOption, headers).then(function(response)
            {

                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.expenCategoryModel.list = data;
                    var total_amount = 0.00;
                    for (var i = 0; i < $scope.expenCategoryModel.list.length; i++)
                    {
                        var totalAmt = 0;
                        for (var j = 0; j < $scope.expenCategoryModel.list[i].detail.length; j++)
                        {
                            totalAmt = totalAmt + parseFloat($scope.expenCategoryModel.list[i].detail[j].spentAmount);
                            //$scope.expenCategoryModel.list[i].detail[j].spentAmount = parseFloat($scope.expenCategoryModel.list[i].detail[j].spentAmount).toFixed(2);

                        }
                        $scope.expenCategoryModel.list[i].amount = parseFloat(totalAmt).toFixed(2);
                        total_amount = total_amount + parseFloat($scope.expenCategoryModel.list[i].amount);
                        $scope.expenCategoryModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                    }
                    $scope.expenCategoryModel.total_Amount = total_amount;
                    $scope.expenCategoryModel.total_Amount = parseFloat($scope.expenCategoryModel.total_Amount).toFixed(2);
                    $scope.expenCategoryModel.total_Amount = utilityService.changeCurrency($scope.expenCategoryModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                }
                $scope.expenCategoryModel.isLoadingProgress = false;
            });

        };
        $scope.redirect = function(id, date)
        {
            $localStorage.user_id = id;
            $localStorage.date = date;
        }

        $scope.getEmployeList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatEmployeModel = function(model)
        {
            if (model != null)
            {
                if (model.f_name != undefined && model.team_name != undefined && model.emp_code != undefined)
                {
                    return model.f_name + '(' + model.team_name + ',' + model.emp_code + ')';
                }
                else if (model.f_name != undefined && model.team_name != undefined && model.company != undefined)
                {
                    return model.f_name + '(' + model.team_name + ')';
                }
            }
            return  '';
        };
        $scope.getEmployeeTeam = function()
        {
            var getListParam = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeTeamList(getListParam, configOption).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.expenCategoryModel.teamList = data;
                }
            });
        };
        $scope.getEmployeeTeam();

        $scope.getList();
    }]);




