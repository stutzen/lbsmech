app.controller('grnListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.goodsReceivedNoteModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            isLoadingProgress: true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.goodsReceivedNoteModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.searchFilter =
                {
                    id: '',
                    quoteId: '',
                    toDate: '',
                    fromDate : '',
                    customerInfo: {},
                    status: ''
                };

        $scope.dateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.fromdateOpen = true;
            }
            if(index == 1)
            {
                $scope.todateOpen = true;
            }    
           
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getGRNListInfo, 300);
        }

        $scope.refreshScreen = function()
        {
            $scope.searchFilter =
                    {
                        id: '',
                        quoteId: '',
                        toDate: '',
                        fromDate : '',
                        customerInfo: {},
                        status: ''
                    };
            $scope.initTableFilter();
        }
        $scope.selectedGRNId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectedGRNId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteGRNInfo = function( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectedGRNId;
                var headers = {};
                headers['screen-code'] = 'goodsReceivedNote';
                adminService.deleteGRNInfo(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getGRNListInfo();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };

        $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                $scope.initTableFilter();
            }
        });

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };

        $scope.getGRNListInfo = function()
        {
            $scope.goodsReceivedNoteModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesorder';

            var configOption = adminService.handleOnlyErrorResponseConfig;

//            getListParam.id = $scope.searchFilter.quoteId;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }

            if ($scope.searchFilter.fromDate != null && $scope.searchFilter.fromDate != '')
            {
                if ($scope.searchFilter.fromDate != null && typeof $scope.searchFilter.fromDate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromDate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
                getListParam.to_date = getListParam.from_date;
            }
            
            if ($scope.searchFilter.toDate != null && $scope.searchFilter.toDate != '' && $scope.searchFilter.toDate != null && $scope.searchFilter.toDate != '')
            {
                if ($scope.searchFilter.toDate != null && typeof $scope.searchFilter.toDate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.toDate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
                getListParam.to_date = getListParam.to_date;
            }
            getListParam.start = ($scope.goodsReceivedNoteModel.currentPage - 1) * $scope.goodsReceivedNoteModel.limit;
            getListParam.limit = $scope.goodsReceivedNoteModel.limit;
            getListParam.is_active = 1;
//            getListParam.status = $scope.searchFilter.status;
            adminService.grnListAll(getListParam, configOption, headers).then(function(response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.goodsReceivedNoteModel.list = data.list;
                    if ($scope.goodsReceivedNoteModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.goodsReceivedNoteModel.list.length; i++)
                        { 
                            $scope.goodsReceivedNoteModel.list[i].newdate = utilityService.parseStrToDate($scope.goodsReceivedNoteModel.list[i].date);
                            $scope.goodsReceivedNoteModel.list[i].date = utilityService.parseDateToStr($scope.goodsReceivedNoteModel.list[i].newdate,$scope.adminService.appConfig.date_format);
                            $scope.goodsReceivedNoteModel.list[i].qty = parseFloat($scope.goodsReceivedNoteModel.list[i].qty).toFixed(2);
                            $scope.goodsReceivedNoteModel.list[i].pack_size = parseFloat($scope.goodsReceivedNoteModel.list[i].pack_size).toFixed(2);
                           
                        }
                    }
                    $scope.goodsReceivedNoteModel.total = data.total;
                }
                $scope.goodsReceivedNoteModel.isLoadingProgress = false;
            });
        };
        $scope.getGRNListInfo();
    }]);




