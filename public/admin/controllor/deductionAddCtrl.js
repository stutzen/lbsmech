

app.controller('deductionAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', 'utilityService', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, utilityService) {

        $scope.deductionModel = {

            date: '',
            advanceList: []
        }

        $scope.validationFactory = ValidationFactory;
        $scope.adminService = adminService;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.deduction_add_form != 'undefined' && typeof $scope.deduction_add_form.$pristine != 'undefined' && !$scope.deduction_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.deduction_add_form.$setPristine();
            if (typeof $scope.deduction_add_form != 'undefined')
            {
                $scope.deductionModel.date = new Date();
                $scope.deductionModel.advanceList = [];
                $scope.getAdvanceList();
            }
        }
        $scope.deductionModel.date = new Date();
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.currentDate = new Date();

        $scope.deductionDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.deductionDateOpen = true;
            }
        }

        $scope.getAdvanceList = function ()
        {
            $scope.deductionModel.isLoadingProgress = true;
            var getAdvanceParam = {};
            getAdvanceParam.type = 1;
            getAdvanceParam.is_active = '1';

            adminService.getAdvanceList(getAdvanceParam).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.deductionModel.advanceList = data.list;
                    for (var i = 0; i < $scope.deductionModel.advanceList.length; i++)
                    {
                        var datevalue = moment($scope.deductionModel.advanceList[i].date).valueOf();
                        if (datevalue > 0)
                        {
                            var nextdate = $scope.deductionModel.advanceList[i].date;
                            var newnextdate = utilityService.convertToUTCDate(nextdate);
                            $scope.deductionModel.advanceList[i].date = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                        } else
                            $scope.deductionModel.advanceList[i].date = '';
                    }
                }
                $scope.deductionModel.isLoadingProgress = false;
            });
        };

        $scope.addDeduction = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addDeductionParam = [];
            var headers = {};
            headers['screen-code'] = 'advancededuction';

            for (var i = 0; i < $scope.deductionModel.advanceList.length; i++)
            {
                var advanceParam = {};
                advanceParam.emp_id = $scope.deductionModel.advanceList[i].emp_id;
                advanceParam.emp_code = $scope.deductionModel.advanceList[i].emp_code;
                advanceParam.date = $filter('date')($scope.deductionModel.date, 'yyyy-MM-dd');
                advanceParam.advance_amt_id = $scope.deductionModel.advanceList[i].id;
                advanceParam.deduction_amt = $scope.deductionModel.advanceList[i].deduction_amt;
                advanceParam.is_active = 1;
                if (parseFloat($scope.deductionModel.advanceList[i].deduction_amt) > 0)
                {
                    addDeductionParam.push(advanceParam);
                }
            }
            if (addDeductionParam.length > 0)
            {
                adminService.addDeduction(addDeductionParam, headers).then(function (response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.deductionList');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
            else
            {
                swal("oops!", "Please enter deduction amount greater than zero", "error");
            }
        };

        $scope.getAdvanceList();

    }]);
