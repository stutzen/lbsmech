





app.controller('taskListCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', '$filter', 'Auth', '$state', '$timeout', '$rootScope', 'adminService', 'ValidationFactory', '$filter', 'Auth', '$state', '$timeout', '$httpService', 'APP_CONST', function($scope, $rootScope, adminService, ValidationFactory, $filter, Auth, $state, $timeout, $httpService, APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.taskModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            isLoadingProgress: false,
            id: ''
        };


        $scope.searchFilter = {
            taskInfo: {},
            name: ''
        };
        $scope.validationFactory = ValidationFactory;
        $scope.refreshScreen = function()
        {
            $scope.initTableFilter();
        }

        $scope.searchFilterValue = "";
        $scope.clearFilter = function()
        {
            $scope.searchFilter = {
                taskInfo: {},
                name: ''
            };
            $scope.initTableFilter();
        }

        $scope.$watch('searchFilter.taskInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == null || newVal == '')
            {
                $scope.initTableFilter();
            }
        });

        $scope.formatTaskModel = function(model)
        {
            if (model != null)
            {
                return model.task_name;
            }
            return  '';
        };

        $scope.getTask = function(val)
        {
            var autosearchParam = {};
            autosearchParam.task_name = val;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            return adminService.getEmployeeTaskList(autosearchParam, configOption, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };


        $scope.adminService = adminService;
        $scope.pagePerCount = [10, 20, 50, 100];
        $scope.taskModel.limit = $scope.pagePerCount[0];

        $scope.validationFactory = ValidationFactory;
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getTaskList, 300);
        }

        $scope.selectTaskId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectTaskId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.selectTaskId = '';
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deleteTaskTypeInfo = function( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectTaskId;
                var headers = {};
                headers['screen-code'] = 'empTask';
                adminService.deleteEmployeeTask(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getTaskList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };

        $scope.getTaskList = function() {

            $scope.taskModel.isLoadingProgress = true;
            var headers = {};
            headers['screen-code'] = 'empTask';
            var getListParam = {};
            getListParam.id = '';
            getListParam.task_name = $scope.searchFilter.name;
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeTaskList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.taskModel.list = data;

                    $scope.taskModel.total = data.total;
                }
                $scope.taskModel.isLoadingProgress = false;

            });

        };

        $scope.getTaskList();
    }]);




