

app.controller('crmDashboardCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', '$localStorageService', '$localStorage', 'dealFactory', function ($scope, $rootScope, $state, $stateParams, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, $localStorageService, $localStorage, dealFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.dashboardModel = {
            stageList: {},
            userBasedList: {},
            dealCityList: {},
            dealTypeList: {},
            productList: {},
            dealCreatorsList: {},
            hovering: false
        };
        $scope.adminService = adminService;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.todayDate = new Date();
        $scope.validationFactory = ValidationFactory;
        $scope.dealFactory = dealFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            if (index === 1)
            {
                $scope.toDateOpen = true;
            }
        };
        $scope.setFactory = function (type, value, name, data)
        {
            var deal = {};
            if (type == 'deal')
            {
                var arr = data.employee_name.split(' ');
                var newData =
                        {
                            id: data.emp_id,
                            f_name: arr[0]
                        }
                deal.emp_id = value;
                deal.emp_name = name;
                $localStorage["deal_form-city_dropdown"] = {};
                $localStorage["deal_form-deal_company_dropdown"] = "";
                $localStorage["deal_form-deal_customer_dropdown"] = {};
                $localStorage["deal_form-deal_user_dropdown"] = newData;
                $localStorage["deal_form-dealdropdown"] = "";
                $localStorage["deal_form-dealstagetypedropdown"] = "all";
                $localStorage["deal_form-fromdate"] = "";
                $localStorage["deal_form-stagedropdown"] = "";
                $localStorage["deal_form-subject"] = "";
                $localStorage["deal_form-todaydeal"] = "";
                $localStorage["deal_form-user_dropdown"] = {};
            }
            if (type == 'product')
            {
                deal.deal_name = value;
                $localStorage["deal_form-city_dropdown"] = {};
                $localStorage["deal_form-deal_company_dropdown"] = "";
                $localStorage["deal_form-deal_customer_dropdown"] = {};
                $localStorage["deal_form-deal_user_dropdown"] = {};
                $localStorage["deal_form-dealdropdown"] = "";
                $localStorage["deal_form-dealstagetypedropdown"] = "all";
                $localStorage["deal_form-fromdate"] = "";
                $localStorage["deal_form-stagedropdown"] = "";
                $localStorage["deal_form-subject"] = value;
                $localStorage["deal_form-todaydeal"] = "";
                $localStorage["deal_form-user_dropdown"] = {};
            }
            if (type == 'city')
            {
                deal.city_id = value
                deal.city_name = name;
                var newData =
                        {
                            id: data.city_id,
                            name: data.city_name
                        }
                $localStorage["deal_form-city_dropdown"] = newData;
                $localStorage["deal_form-deal_company_dropdown"] = "";
                $localStorage["deal_form-deal_customer_dropdown"] = {};
                $localStorage["deal_form-deal_user_dropdown"] = {};
                $localStorage["deal_form-dealdropdown"] = "";
                $localStorage["deal_form-dealstagetypedropdown"] = "all";
                $localStorage["deal_form-fromdate"] = "";
                $localStorage["deal_form-stagedropdown"] = "";
                $localStorage["deal_form-subject"] = "";
                $localStorage["deal_form-todaydeal"] = "";
                $localStorage["deal_form-user_dropdown"] = {};
            }
            if (type == 'creators')
            {
                deal.user_id = value;
                deal.emp_name = name;
                var newData =
                        {
                            id: data.user_id,
                            f_name: data.employee_name
                        }
                $localStorage["deal_form-city_dropdown"] = {};
                $localStorage["deal_form-deal_company_dropdown"] = "";
                $localStorage["deal_form-deal_customer_dropdown"] = {};
                $localStorage["deal_form-deal_user_dropdown"] = {};
                $localStorage["deal_form-dealdropdown"] = "";
                $localStorage["deal_form-dealstagetypedropdown"] = "all";
                $localStorage["deal_form-fromdate"] = "";
                $localStorage["deal_form-stagedropdown"] = "";
                $localStorage["deal_form-subject"] = "";
                $localStorage["deal_form-todaydeal"] = "";
                $localStorage["deal_form-user_dropdown"] = newData;
            }
            if (type == 'stage')
            {
                deal.stage_id = value;
                $localStorage["deal_form-city_dropdown"] = {};
                $localStorage["deal_form-deal_company_dropdown"] = "";
                $localStorage["deal_form-deal_customer_dropdown"] = {};
                $localStorage["deal_form-deal_user_dropdown"] = {};
                $localStorage["deal_form-dealdropdown"] = "";
                $localStorage["deal_form-dealstagetypedropdown"] = "all";
                $localStorage["deal_form-fromdate"] = "";
                $localStorage["deal_form-stagedropdown"] = String(value);
                $localStorage["deal_form-subject"] = "";
                $localStorage["deal_form-todaydeal"] = "";
                $localStorage["deal_form-user_dropdown"] = {};
            }
            if (type == 'dealtype')
            {

                deal.deal_type_id = value;
                $localStorage["deal_form-city_dropdown"] = {};
                $localStorage["deal_form-deal_company_dropdown"] = "";
                $localStorage["deal_form-deal_customer_dropdown"] = {};
                $localStorage["deal_form-deal_user_dropdown"] = {};
                $localStorage["deal_form-dealdropdown"] = String(value);
                $localStorage["deal_form-dealstagetypedropdown"] = "all";
                $localStorage["deal_form-fromdate"] = "";
                $localStorage["deal_form-stagedropdown"] = "";
                $localStorage["deal_form-subject"] = "";
                $localStorage["deal_form-todaydeal"] = "";
                $localStorage["deal_form-user_dropdown"] = {};

            }
            deal.type = type;
            deal.fromDate = $scope.searchFilter.fromdate;
            deal.toDate = $scope.searchFilter.todate;
            dealFactory.set(deal);
            $state.go('app.deal');
        }
        $scope.searchFilter =
                {
                    fromdate: '',
                    todate: ''
                }
        $scope.searchFilter.todate = $scope.todayDate;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getUserCountListInfo, 100);
        }

        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.userLimit = false;
        $scope.getUserCountListInfo = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'crmdashboard';
            if ($scope.userLimit == false)
            {
                getListParam.start = 0;
                getListParam.limit = 10;
            }
            if ($scope.userLimit == true)
            {
                getListParam.start = '';
                getListParam.limit = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserCountList(getListParam, configOption,headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dashboardModel.userBasedList = data.list;
                    for (var i = 0; i < $scope.dashboardModel.userBasedList.length; i++)
                    {
                        $scope.dashboardModel.userBasedList[i].hovering = false;
                    }
                    $scope.getStageCountListInfo();
                }
            });
        };
        $scope.creatorLimit = false;
        $scope.getDealCreatorsInfo = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'crmdashboard';
            if ($scope.creatorLimit == false)
            {
                getListParam.start = 0;
                getListParam.limit = 10;
            }
            if ($scope.creatorLimit == true)
            {
                getListParam.start = '';
                getListParam.limit = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDealCreatorsList(getListParam, configOption,headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dashboardModel.dealCreatorsList = data.list;
                    //$scope.getStageCountListInfo();
                }
            });
        };


        $scope.getStageCountListInfo = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'crmdashboard';
            getListParam.start = 0;
            getListParam.limit = 10;
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getStageCountList(getListParam, configOption,headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.dashboardModel.stageList = data;
                    $scope.dashboardModel.total = data.total;
                    $scope.getCityCountListInfo();
                }

            });

        };
        $scope.cityLimit = false;
        $scope.getCityCountListInfo = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'crmdashboard';
            if ($scope.cityLimit == false)
            {
                getListParam.start = 0;
                getListParam.limit = 10;
            }
            if ($scope.cityLimit == true)
            {
                getListParam.start = '';
                getListParam.limit = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCityCountList(getListParam, configOption,headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.dashboardModel.dealCityList = data;
                    $scope.dashboardModel.total = data.total;
                    $scope.getDealTypeCountListInfo();
                }

            });

        };

        $scope.getDealTypeCountListInfo = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'crmdashboard';
            getListParam.start = 0;
            getListParam.limit = 10;
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDealTypeCountList(getListParam, configOption,headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.dashboardModel.dealTypeList = data;
                    $scope.dashboardModel.total = data.total;
                    $scope.getProductCountListInfo();
                }

            });

        };

        $scope.passLimit = false;
        $scope.getProductCountListInfo = function (passLimit)
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'crmdashboard';
            if ($scope.passLimit == false)
            {
                getListParam.start = 0;
                getListParam.limit = 10;
            }
            if ($scope.passLimit == true)
            {
                getListParam.start = '';
                getListParam.limit = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getProductBasedCountList(getListParam, configOption,headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.dashboardModel.productList = data;
                    $scope.dashboardModel.total = data.total;
                    $scope.getDealCreatorsInfo();
                }

            });

        };

        $scope.visible = false;
        $scope.expand = function ()
        {
            $scope.passLimit = true;
            $scope.visible = !$scope.visible;
            $scope.getProductCountListInfo();
        };

        $scope.showTab = false;
        $scope.expandTab = function ()
        {
            $scope.cityLimit = true;
            $scope.showTab = !$scope.showTab;
            $scope.getCityCountListInfo();
        };
        $scope.showUser = false;
        $scope.expandUser = function ()
        {
            $scope.userLimit = true;
            $scope.showUser = !$scope.showUser;
            $scope.getUserCountListInfo();

        };
        $scope.expandCreators = function ()
        {
            $scope.creatorLimit = true;
            $scope.getDealCreatorsInfo();
        }
        $scope.initTableFilter();

        $scope.hoveringFunc = function (index)
        {
            for (var i = 0; i < $scope.dashboardModel.userBasedList.length; i++)
            {
                if (index != i)
                {
                    $scope.dashboardModel.userBasedList[i].hovering = false;
                }
            }
            if ($scope.dashboardModel.userBasedList[index].hovering == false)
            {
                $scope.dashboardModel.userBasedList[index].hovering = true;
            } else if ($scope.dashboardModel.userBasedList[index].hovering == true)
            {
                $scope.dashboardModel.userBasedList[index].hovering = false;
            }
        }
    }]);








