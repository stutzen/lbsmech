




app.controller('employeeteamAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', 'sweet', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, sweet, $stateParams, $timeout, ValidationFactory) {

    $scope.teamModel = {
        "name": "",
        "count": "",
        "current_count": 0,
        "is_active": true,
        "user_id": "",
        "user_name": "",
        "userInfo": ""
    };
    $scope.validationFactory = ValidationFactory;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.getNavigationBlockMsg = function (pageReload)
    {
        if (typeof $scope.create_employeeteam_form !== 'undefined' && typeof $scope.create_employeeteam_form.$pristine !== 'undefined' && !$scope.create_employeeteam_form.$pristine)
        {
            return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
        }

        return "";
    };
    $scope.isDataSavingProcess = false;
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $scope.formReset = function ()
    {
        $scope.create_employeeteam_form.$setPristine();
        $scope.teamModel.name = "";
        $scope.teamModel.count = "";
        $scope.teamModel.is_active = true;
        $scope.teamModel.current_count = 0;
        $scope.teamModel.user_id = '';
        $scope.teamModel.user_name = '';
        $scope.teamModel.userInfo = "";
    };

    $scope.getEmployeelist = function (val)
    {
        var autosearchParam = {};
        autosearchParam.fname = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };

    $scope.formatEmployeeModel = function (model) {

        if (model !== null && model != undefined)
        {
            return model.f_name;
        }
        return  '';
    };

    $scope.createEmployeeTeam = function ()
    {
        if ($scope.isDataSavingProcess == false)
        {
            $scope.isDataSavingProcess = true;
            var createEmployeeTeamParam = {};
            var headers = {};
            headers['screen-code'] = 'team';
            createEmployeeTeamParam.name = $scope.teamModel.name;
            createEmployeeTeamParam.count = $scope.teamModel.count;
            createEmployeeTeamParam.is_active = $scope.teamModel.is_active;
            createEmployeeTeamParam.current_count = $scope.teamModel.current_count;
            if($scope.teamModel.employeeInfo != undefined && $scope.teamModel.employeeInfo !=null && $scope.teamModel.employeeInfo !='')
            {    
            createEmployeeTeamParam.emp_id = $scope.teamModel.employeeInfo.id;  
            createEmployeeTeamParam.emp_name = $scope.teamModel.employeeInfo.f_name;  
            }
            else
            {
                createEmployeeTeamParam.emp_id = '';
                createEmployeeTeamParam.emp_name = '';
            }    
            
            adminService.createEmployeeTeam(createEmployeeTeamParam, headers).then(function (response) {
                if (response.data.success === true)
                {
                    $scope.formReset();
                    $state.go('app.employeeteamlist');
                }
                $scope.isDataSavingProcess = false;
            });
        } else
{
            return;
        }

    };

}
]);




