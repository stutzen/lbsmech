

app.controller('productTypeEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.productTypeModel = {
            id: '',
            name: "",
            code: '',
            comments: '',
            isLoadingProgress: false

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.product_type_form != 'undefined' && typeof $scope.product_type_form.$pristine != 'undefined' && !$scope.product_type_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.product_type_form.$setPristine();
            $scope.updateWorkcenterInfo();

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.updateWorkcenterInfo = function()
        {
            if ($scope.workcenterDetail != null)
            {
                $scope.productTypeModel.name = $scope.workcenterDetail.name;
                $scope.productTypeModel.code = $scope.workcenterDetail.code;
                $scope.productTypeModel.comments = $scope.workcenterDetail.comments;
            }
            $scope.productTypeModel.isLoadingProgress = false;
        }
        $scope.modifyType = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'producttype';
                createParam.name = $scope.productTypeModel.name;
                createParam.code = $scope.productTypeModel.code;
                createParam.comments = $scope.productTypeModel.comments;
                adminService.editProductType(createParam, $stateParams.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.producttype');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            $scope.productTypeModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductTypeList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.workcenterDetail = data.list[0];
                    $scope.updateWorkcenterInfo();
                }

            });

        };


        $scope.getList();

    }]);




