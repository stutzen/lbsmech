app.controller('userRoleEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', '$stateParams', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, $stateParams) {

        $scope.userRoleAddModel = {
            "id": 0,
            "username": "",
            "rolename": "",
            "name": "",
            "comments": "",
            "isactive": 1,
            "userList": {},
            currentPage: 1,
            total: 0,
            showPassword: false,
            role: '',
            isLoadingProgress: false,
            userDetail: {}
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_user_role_form != 'undefined' && typeof $scope.create_user_role_form.$pristine != 'undefined' && !$scope.create_user_role_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function()
        {
            $scope.create_user_role_form.$setPristine();
            $scope.updateRoleInfo();
        }

        $scope.modifyUserrole = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var modifyUserroleParam = {};
                var headers = {};
                headers['screen-code'] = 'role';
                modifyUserroleParam.name = $scope.userRoleAddModel.rolename;
                modifyUserroleParam.comments = $scope.userRoleAddModel.comments;
                modifyUserroleParam.is_active = 1;

                adminService.modifyUserRole(modifyUserroleParam, $stateParams.id, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.userrole')
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.updateRoleInfo = function()
        {
            if ($scope.userRoleAddModel.userDetail != null)
            {
                $scope.userRoleAddModel.rolename = $scope.userRoleAddModel.userDetail.name;
                $scope.userRoleAddModel.comments = $scope.userRoleAddModel.userDetail.comments;
            }
            $scope.userRoleAddModel.isLoadingProgress = false;
        }

        $scope.getList = function()
        {
            $scope.userRoleAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserRoleList(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.userRoleAddModel.userDetail = data.list[0];
                    $scope.updateRoleInfo();
                }
            });
        };

        $scope.getList();


    }]);




