app.controller('purchaseInvoiceListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory','customerFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory,customerFactory) {

    $rootScope.getNavigationBlockMsg = null;
    $scope.invoiceModel = {
        currentPage: 1,
        total: 0,
        limit: 4,
        list: [],
        status: '',
        filterCustomerId: '',
        isLoadingProgress: true
    };
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.invoiceModel.limit = $scope.pagePerCount[0];
    $scope.customerFilter = customerFactory.get();
    customerFactory.set('');
    $scope.searchFilter =
    {
        id: '',
        invoice_number: '',
        duedate: '',
        invoicedate: '',
        customerInfo: {},
        status: '',
        customAttribute: []
    };

    $scope.refreshScreen = function()
    {
        $scope.searchFilter = {
            invoicedate: '',
            duedate: '',
            customerInfo: {},
            invoice_number: '',
            status: '',
            customAttribute: []
        };
        $scope.initTableFilter();
    }
    $scope.update = function(isfilteropen)
    {
        $scope.searchFilter.customAttribute = [];
        if (isfilteropen == false)
        {
            $scope.initTableFilter();
        }
    }


    $scope.invoicedateOpen = false;
    $scope.duedateOpen = false;
    //    'dd-MM-yyyy';
    $scope.openDate = function(index)
    {
        if (index == 0)
        {
            $scope.invoicedateOpen = true;
        }
        else if (index == 1)
        {
            $scope.duedateOpen = true;
        }
    }
    $scope.validationFactory = ValidationFactory;
    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getinvoiceListInfo, 300);
    }
    $scope.customAttributeList = [];
    $scope.selectInvoiceId = '';
    $scope.showdeletePopup = false;
    $scope.isdeleteCategoryLoadingProgress = false;
    $scope.showPopup = function(id)
    {
        $scope.selectInvoiceId = id;
        $scope.showdeletePopup = true;
    };

    $scope.closePopup = function()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteCategoryLoadingProgress = false;
    };

    $scope.getCustomAttributeList = function() {

        var getListParam = {};
        getListParam.id = "";
        getListParam.attributetype_id = 3;
        getListParam.attribute_code = "";
        getListParam.attributetypecode = "purchaseinvoice";
        getListParam.is_show_in_list = "1";
        getListParam.is_active = 1;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getAttributeList(getListParam, configOption).then(function(response)
        {
            var data = response.data.list;
            $scope.customAttributeList = data;
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].input_type == 'dropdown')
                {
                    $scope.optionList = [];
                    $scope.optionList = $scope.customAttributeList[i].option_value.split(',');
                    $scope.customAttributeList[i].option_value = $scope.optionList;
                }
            }

        });
    };

    $scope.deleteInvoiceItem = function( )
    {
        if ($scope.isdeleteCategoryLoadingProgress == false)
        {
            $scope.isdeleteCategoryLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $scope.selectInvoiceId;
            var headers = {};
            headers['screen-code'] = 'purchaseinvoice';
            adminService.deletePurchaseInvoice(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getinvoiceListInfo();
                }
                $scope.isdeleteCategoryLoadingProgress = false;
            });
        }
    };

    $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
    {
        if (typeof newVal == 'undefined' || newVal == '')
        {
            $scope.initTableFilter();
        }
    });

    $scope.getCustomerList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.search = val;
        if (autosearchParam.search != '')
        {
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }
    };
    if($scope.customerFilter != '' && $scope.customerFilter != null && $scope.customerFilter != undefined)
    {    
        $scope.searchFilter.customerInfo = $scope.customerFilter;
    }

    $scope.formatCustomerModel = function(model)
    {
        if (model != null)
        {
            if (model.fname != undefined && model.phone != undefined && model.email != undefined)
            {
                return model.fname + '(' + model.phone + ',' + model.email + ')';
            }
            else if (model.fname != undefined && model.phone != undefined)
            {
                return model.fname + '(' + model.phone + ')';
            }
            else
            {
                return model.fname;
            }    
        }
        return  '';
    };

    $scope.getinvoiceListInfo = function()
    {
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.invoiceModel.isLoadingProgress = true;
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'purchaseinvoice';
        getListParam.reference_no = $scope.searchFilter.invoice_number;
        getListParam.from_date = $scope.searchFilter.invoicedate;
        getListParam.from_duedate = $scope.searchFilter.duedate;
        if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
        {
            getListParam.customer_id = $scope.searchFilter.customerInfo.id;
        }
        else
        {
            getListParam.customer_id = '';
        }
        if ($scope.searchFilter.duedate != '' && ($scope.searchFilter.invoicedate == null || $scope.searchFilter.invoicedate == ''))
        {
            if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object' && $scope.searchFilter.duedate != undefined)
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.dateFormat);
                
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            }
        }
        else if ($scope.searchFilter.invoicedate != '' && ($scope.searchFilter.duedate == null || $scope.searchFilter.duedate == ''))
        {
            if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object' && $scope.searchFilter.invoicedate != undefined)
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.dateFormat);
                
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            }
        }
        else if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '' && $scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
        {
            if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object' && $scope.searchFilter.duedate != undefined )
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.dateFormat);
            }
            if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object' && typeof $scope.searchFilter.invoicedate != undefined)
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
        }
        //            if ($scope.searchFilter.duedate != '' && $scope.searchFilter.duedate != null)
        //            {
        //                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
        //                {
        //                    getListParam.to_duedate = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
        //                }
        //                getListParam.to_duedate = utilityService.changeDateToSqlFormat(getListParam.to_duedate, $scope.adminService.appConfig.date_format);
        //                getListParam.from_duedate = getListParam.to_duedate;
        //            }
        //            if ($scope.searchFilter.invoicedate != '' && $scope.searchFilter.invoicedate != null)
        //            {
        //                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
        //                {
        //                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.adminService.appConfig.date_format);
        //                }
        //                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_duedate, $scope.adminService.appConfig.date_format);
        //                getListParam.to_date = getListParam.from_date;
        //            }
        //            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '' && $scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
        //            {
        //                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
        //                {
        //                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.adminService.appConfig.date_format);
        //                }
        //                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_validdate, $scope.adminService.appConfig.date_format);
        //                getListParam.to_date = getListParam.from_date;
        //                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
        //                {
        //                    getListParam.from_duedate = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
        //                }
        //                getListParam.from_duedate = utilityService.changeDateToSqlFormat(getListParam.from_duedate, $scope.adminService.appConfig.date_format);
        //                getListParam.to_duedate = getListParam.from_duedate;
        //            }
        //            else if (($scope.searchFilter.duedate != '' && ($scope.searchFilter.invoicedate == null || $scope.searchFilter.invoicedate == '')) && ($scope.searchFilter.invoicedate != '' && ($scope.searchFilter.duedate == null || $scope.searchFilter.duedate == '')))
        //            {
        //                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
        //                {
        //                    getListParam.to_duedate = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.dateFormat);
        //                }
        //                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
        //                {
        //                    getListParam.from_duedate = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.dateFormat);
        //                }
        //                getListParam.to_duedate = utilityService.changeDateToSqlFormat(getListParam.to_duedate, $scope.dateFormat);
        //                getListParam.from_duedate = utilityService.changeDateToSqlFormat(getListParam.from_duedate, $scope.dateFormat);
        //            }
        var j = 0;
        for (var i = 0; i < $scope.customAttributeList.length; i++)
        {
            if ($scope.searchFilter.customAttribute[i] != 'undefined' && $scope.searchFilter.customAttribute[i] != null && $scope.searchFilter.customAttribute[i] != '')
            {
                var j = j + 1;
                getListParam['cus_attr_' + j] = $scope.customAttributeList[i].attribute_code + '::' + $scope.searchFilter.customAttribute[i];
            }
        }
        getListParam.start = ($scope.invoiceModel.currentPage - 1) * $scope.invoiceModel.limit;
        getListParam.limit = $scope.invoiceModel.limit;
        getListParam.is_active = 1;
        getListParam.status = $scope.searchFilter.status;
        adminService.getPurchaseInvoiceList(getListParam, headers).then(function(response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.invoiceModel.list = data.list;
                if ($scope.invoiceModel.list.length > 0)
                {
                    for (var i = 0; i < $scope.invoiceModel.list.length; i++)
                    {
                        $scope.invoiceModel.list[i].newdate = utilityService.parseStrToDate($scope.invoiceModel.list[i].date);
                        $scope.invoiceModel.list[i].date = utilityService.parseDateToStr($scope.invoiceModel.list[i].newdate, $rootScope.appConfig.date_format);
                        $scope.invoiceModel.list[i].newduedate = utilityService.parseStrToDate($scope.invoiceModel.list[i].duedate);
                        $scope.invoiceModel.list[i].duedate = utilityService.parseDateToStr($scope.invoiceModel.list[i].newduedate, $rootScope.appConfig.date_format);
                        $scope.invoiceModel.list[i].total_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.invoiceModel.list[i].paid_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].paid_amount, $rootScope.appConfig.thousand_seperator);

                    }
                }
                $scope.invoiceModel.total = data.total;
            }
            $scope.invoiceModel.isLoadingProgress = false;
        });
    };
    $scope.getinvoiceListInfo();
    $scope.getCustomAttributeList();
}]);




