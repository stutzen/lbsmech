

app.controller('albumViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'sweet', function($scope, $rootScope, adminService, $httpService, APP_CONST, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, sweet) {

        $scope.albumAddModel = {
            "id": "",
            "name": "",
            "code": "",
            "type": "",
            "comments": "",
            "customerInfo": "",
            "newcomments": "",
            "imglist": [],
            isLoadingProgress: false

        };
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.comment_form != 'undefined' && typeof $scope.comment_form.$pristine != 'undefined' && !$scope.comment_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.isDataSavingProcess = false;
        $scope.validationFactory = ValidationFactory;
        $scope.showUploadFilePopup = false;
        $scope.showPhotoGalaryPopup = false;
        $scope.selectimgId = '';
        $scope.selectimgcomments = '';
        $scope.showdeletePopup = false;
        $scope.showPopup = function() {

            $state.go('app.albumViewNew', {'id': $stateParams.id}, {'reload': true});
        }

        $scope.closePopup = function(value) {
            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;
            }
            if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }
            else if (value == 'imgdelete')
            {
                $scope.showdeletePopup = false;
            }
        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
        }
        $scope.saveImagePath = function()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    $scope.isImageSavingProcess = false;
                    $scope.albumAddModel.imglist = $scope.uploadedFileQueue;
                    var assetParam = {};
                    //createParam.id = 0;
                    assetParam.url = $scope.uploadedFileQueue[i].urlpath;
                    assetParam.album_id = $scope.albumAddModel.id;
                    assetParam.album_code = $scope.albumAddModel.code;
                    assetParam.comments = $scope.albumAddModel.comments;
                    assetParam.seq_no = $scope.albumAddModel.seq_no;
                    adminService.saveAsset(assetParam).then(function(response) {
                        if (response.data.success == true)
                        {
                            $scope.getAlbum();
                            $state.go('app.albumView', {'id': $stateParams.id}, {'reload': true});
                        }
                        $scope.isDataSavingProcess = false;
                    });
                    $scope.closePopup('fileupload');
                }
            }


        }
        $scope.image = 'image';
        $scope.mappedImage = {"belongTo": [$scope.image], "items": []};
        $scope.updateImageSortby = function(belongTo) {
            var list = {};
            if (belongTo == $scope.image)
            {
                list = $scope.mappedImage;
            }
            var getListParam = [];
            for (var i = 0; i < list.items.length; i++)
            {
                var listitem = {};
                listitem.id = list.items[i].id;
                listitem.sortBy = i + 1;
                getListParam.push(listitem);
            }
            var configData = {};
            configData.belongTo = belongTo;
            adminService.updateImageSortby(getListParam, configData).then(function(response) {
                var config = response.config.config;
                if (typeof config.belongTo !== 'undefined')
                {
                    var belongTo = config.belongTo;
                    $scope.getCategoryAssociationMappingList(belongTo);
                }

            });

        };
        $scope.onMoved = function(list) {
            list.items = list.items.filter(function(item) {
                return !item.selected;
            });
        };

        $scope.onDragstart = function(list, event) {
            list.dragging = true;
            if (event.dataTransfer.setDragImage) {
                var img = new Image();
                img.src = '../img/ic_content_copy_black_24dp_2x.png';
                event.dataTransfer.setDragImage(img, 0, 0);
            }
        };

        $scope.onDrop = function(list, items, index) {
            angular.forEach(items, function(item) {
                item.selected = false;
            });
            list.items = list.items.slice(0, index)
                    .concat(items)
                    .concat(list.items.slice(index));

            if (list.items.length > 0)
            {
                var belongTo = list.items[0].belongTo;
                if (belongTo == $scope.image)
                {
                    $timeout(function() {
                        $scope.updateImageSortby($scope.image);
                    }, 1000);

                }
            }
            return true;
        };

        $scope.getSelectedItemsIncluding = function(list, item) {
            item.selected = true;
            return list.items.filter(function(item) {
                return item.selected;

            });
        };

        $scope.showUploadFilePopup = false;
        $scope.showFileUploadPopup = function() {

            $scope.showUploadFilePopup = true;

        }
        $scope.closeFileUploadPopup = function(value) {

            $scope.showUploadFilePopup = false;

        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;

        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
        }


        $scope.deleteImage = function($index)
        {
            $scope.albumAddModel.imgs.splice($index, 1);
        }


        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.albumAddModel.img.length > index)
            {
                $scope.albumAddModel.img.splice(index, 1);
            }
        }


        $scope.getCommentlist = function(id, index) {
            var getCommentListParam = {};
            getCommentListParam.album_asset_id = id;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getcommentlist(getCommentListParam, configOption).then(function(response) {
                var data = response.data;
                $scope.albumAddModel.imglist[index].commentList = [];
                $scope.albumAddModel.imglist[index].commentList = data.list;
                //$scope.albumAddModel.imglist[index].commentList.push( data.list);
            });

        };
        $scope.saveComment = function(id, album_id, code, $index) {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                //createParam.id = 0;
                createParam.album_asset_id = id;
                createParam.album_id = album_id;
                createParam.album_code = code;
                createParam.created_by_name = '';
                createParam.updated_by_name = '';
                createParam.comments = $scope.albumAddModel.newcomments[$index];
                adminService.addcommentlist(createParam).then(function(response) {
                    if (response.data.success == true)
                    {
                        //$scope.getAlbum();
                        $state.go('app.albumView', {'id': $stateParams.id}, {'reload': true});
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };
        $scope.getSharelist = function() {
            var getShareListParam = {};
            getShareListParam.album_id = $scope.albumAddModel.id;
            getShareListParam.code = '';
            getShareListParam.customer_id = '';
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getSharelist(getShareListParam, configOption).then(function(response) {
                var data = response.data.list;
                $scope.albumAddModel.shareList = data;
            });

        };
        $scope.saveShare = function() {
            if ($scope.isDataSavingProcess == false)
            {
                var count = 0;
                if ($scope.albumAddModel.shareList.length > 0)
                {
                    for (var i = 0; i < $scope.albumAddModel.shareList.length; i++)
                    {
                        if ($scope.albumAddModel.customerInfo.id == $scope.albumAddModel.shareList[i].customer_id)
                        {
                            count = count + 1;
                            break;
                        }
                    }
                }
                if (count == 0)
                {
                    $scope.isDataSavingProcess = true;
                    var createshareParam = [];
                    var shareInfo = {};
                    shareInfo.album_id = parseInt($scope.albumAddModel.id);
                    shareInfo.album_code = $scope.albumAddModel.code;
                    shareInfo.customer_id = $scope.albumAddModel.customerInfo.id;
                    shareInfo.is_active = 1;
                    createshareParam.push(shareInfo);
                    adminService.addSharelist(createshareParam).then(function(response) {
                        if (response.data.success == true)
                        {
                            $scope.albumAddModel.customerInfo = '';
                            $scope.getSharelist();
                            // $state.go('app.albumView', {'id': $stateParams.id}, {'reload': true});
                        }
                        $scope.isDataSavingProcess = false;
                    });
                }
                else
                {
                    sweet.show('Oops...', 'Already Permission Given...', 'error');
                }
            }
        };
        $scope.selectalbumlistId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showsharePopup = function(id)
        {
            $scope.selectalbumlistId = id;
            $scope.showdeletePopup = true;
        };
        $scope.closesharePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deleteAlbumlistInfo = function()
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectalbumlistId;
                adminService.deleteshare(getListParam, getListParam.id).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closesharePopup();
                        $scope.getSharelist();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };

        $scope.redirectPage = function()
        {
            $state.go('app.albumViewNew', {'id': $scope.albumAddModel.id}, {'reload': true});
        }

        $scope.getAlbum = function() {
            $scope.albumAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.code = $stateParams.id;
            adminService.viewAlbum(getListParam).then(function(response) {
                var data = response.data.data;
                $scope.albumAddModel.list = data;
                $scope.albumAddModel.id = $scope.albumAddModel.list.id;
                $scope.albumAddModel.name = $scope.albumAddModel.list.name;
                $scope.albumAddModel.code = $scope.albumAddModel.list.code;
                $scope.albumAddModel.type = $scope.albumAddModel.list.type;
                $scope.albumAddModel.comments = $scope.albumAddModel.list.comments;
//                $scope.albumAddModel.imglist = data.asset;
//                for (var i = 0; i < $scope.albumAddModel.imglist.length; i++)
//                {
//                    if ($scope.albumAddModel.imglist[i].url != '' && $scope.albumAddModel.imglist[i].url != null)
//                    {
//                        $scope.getCommentlist($scope.albumAddModel.imglist[i].id, i);
//                    }
//                }
//                $scope.mappedImage.items = $scope.albumAddModel.imglist;


                $scope.albumAddModel.total = data.total;
                //$scope.formReset();
                $scope.albumAddModel.isLoadingProgress = false;
                $scope.getAlbumAssetList();
                $scope.getSharelist();
            });
        };

        $scope.getAlbumAssetList = function() {
            var getShareListParam = {};
            getShareListParam.album_id = $scope.albumAddModel.id;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAlbumAssetList(getShareListParam, configOption).then(function(response) {
                var data = response.data.list;
                $scope.albumAddModel.imglist = data;
                if ($scope.albumAddModel.imglist.length > 0)
                {
                    for (var i = 0; i < $scope.albumAddModel.imglist.length; i++)
                    {
                        $scope.albumAddModel.imglist[i].path = $scope.albumAddModel.imglist[i].url;
                    }
                    $scope.mappedImage.items = $scope.albumAddModel.imglist;
                }
            });

        };

        $scope.getAlbum();


    }]);




