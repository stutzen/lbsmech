app.controller('stageCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.stageModel = {
            
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
           
            isLoadingProgress: true

        };
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.stageModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''
            
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getStageList, 300);
        }

        
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function(id)
        {
            $scope.selectStageId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteStageInfo = function( )
        {
            if($scope.isdeleteProgress == false)
            {
               $scope.isdeleteProgress = true;
               
                var getListParam = {};
                getListParam.id = $scope.selectStageId;
                var headers = {};
                headers['screen-code'] = 'stage';
                adminService.deleteStage(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getStageList();
                    }
                     $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: ''
            };
            $scope.initTableFilter();
        }

        $scope.getStageList = function() {          

            $scope.stageModel.isLoadingProgress = true;
            var stageListParam = {};
            var headers = {};
            headers['screen-code'] = 'stage';
            stageListParam.name = $scope.searchFilter.name;
           
            stageListParam.id = '';
            
            stageListParam.is_active = 1;
            stageListParam.start = ($scope.stageModel.currentPage - 1) * $scope.stageModel.limit;
            stageListParam.limit = $scope.stageModel.limit;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStageList(stageListParam, configOption, headers).then(function(response)
            {
                $scope.hidebutton=false;
                if (response.data.success === true)
                {
                   
                    var data = response.data;
                    $scope.stageModel.list = data.list;
                    $scope.stageModel.total = data.total;
                    if($scope.stageModel.list.length>=10)
                        {
                            $scope.hidebutton=true;
                        }
                }
                $scope.stageModel.isLoadingProgress = false;
            });

        };

        $scope.getStageList();

    }]);




