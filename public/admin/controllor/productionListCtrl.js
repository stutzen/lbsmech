





app.controller('productionListCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function ($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.productionModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            isLoadingProgress: false,
            teamList : [],
            present: ''
        };
        $scope.searchFilter = {
            emp_name: '',
            date: '',
             teamid : '',
             present: ''
        };   
        $scope.validationFactory = ValidationFactory;
         $scope.productiondateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.productiondateOpen = true;
            }
            
        }
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                emp_name: '',
                prodution_date:'',
                teamid : '',
                present: ''
            };
            $scope.initTableFilter();
        }
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.productionModel.limit = $scope.pagePerCount[0];
         $scope.dateFormat = $rootScope.appConfig.date_format;

        $scope.validationFactory = ValidationFactory;

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        
        $scope.getTeamlist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.EMP_TEAM_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatTeamModel = function (model) {

            if (model !== null && model != undefined)
            {
                return model.name;
            }
            return  '';
        };

        $scope.$watch('searchFilter.teamInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            } else if (typeof newVal != 'undefined' || newVal != '' || newVal != null)
            {
                $scope.initTableFilter();
            }
        });

        $scope.getList = function () {

            $scope.productionModel.isLoadingProgress = true;
            var headers = {};
            headers['screen-code'] = 'production';
            var getListParam = {};
            getListParam.emp_name = $scope.searchFilter.emp_name;
            getListParam.start = ($scope.productionModel.currentPage - 1) * $scope.productionModel.limit;
            getListParam.limit = $scope.productionModel.limit;
            $scope.productionModel.isLoadingProgress = true;
            getListParam.emp_team_id = $scope.searchFilter.teamid;
            getListParam.is_present = $scope.searchFilter.present;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            
            if ($scope.searchFilter.production_date != null && $scope.searchFilter.production_date != '')
            {
                if ($scope.searchFilter.production_date != null && typeof $scope.searchFilter.production_date == 'object')
                {
                    getListParam.date = utilityService.parseDateToStr($scope.searchFilter.production_date, 'yyyy-MM-dd');
                }
                
            }
            
            adminService.getProductionList(getListParam, configOption, headers).then(function (response) {

                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.productionModel.list = data;
                    for (var i = 0; i < $scope.productionModel.list.length; i++)
                    {
                        var datevalue = moment($scope.productionModel.list[i].date).valueOf();
                        if (datevalue > 0)
                        {
//                            var newdate = new Date($scope.productionModel.list[i].date).toISOString();
//                            $scope.productionModel.list[i].date = newdate;
                            $scope.productionModel.list[i].newdate = utilityService.parseStrToDate($scope.productionModel.list[i].date);
                            $scope.productionModel.list[i].date = utilityService.parseDateToStr($scope.productionModel.list[i].newdate, $rootScope.appConfig.date_format);
                        } else
                            $scope.productionModel.list[i].date = '';
                    }

                    $scope.productionModel.total = data.total;
                }
                $scope.productionModel.isLoadingProgress = false;
            });

        };

        $scope.getList();
        $scope.getEmployeeTeam = function ()
       {            
           var getListParam = {};  
           var configOption = adminService.handleOnlyErrorResponseConfig;
           adminService.getEmployeeTeamList(getListParam, configOption).then(function (response) {
               if (response.data.success === true)
               {
                   var data = response.data.list;
                   $scope.productionModel.teamList = data;                    
               }                
           });
       };
       $scope.getEmployeeTeam();
    }]);




