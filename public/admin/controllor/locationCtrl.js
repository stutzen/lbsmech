app.controller('locationCtrl', ['$scope', '$http', '$rootScope', 'adminService','utilityService', '$filter', 'Auth', '$timeout', 'uiGmapLogger', 'uiGmapGoogleMapApi', function($scope, $http, $rootScope, adminService, utilityService,$filter, Auth, $timeout, $log, GoogleMapApi) {


        $rootScope.getNavigationBlockMsg = null;
        $scope.locationModel = {
            currentPage: 1,
            total: 0,
            limit: 5,
            username:'',
            list: [],
            updated_at:'',
            userList: [],
            isUserLoaded: false,
            isLoadingProgress: true,
            showMap: false

        };
        $scope.map = {
            center: {latitude: 40.1451, longitude: -99.6680},
            zoom: 15
        };

        $scope.searchFilterNameValue = '';

//        $scope.pagePerCount = [50,100];
//        $scope.locationModel.limit = $scope.pagePerCount[0];

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getUserList, 300);
        }
        $scope.searchFilter = {
           username:''
        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: '',
                country_name: '',
                state_name: '',
                country_id: '',
                state_id: '',
                username:''
            };
            $scope.initTableFilter();
        }

        $scope.getUserList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'location';
            $scope.locationModel.isLoadingProgress = true;
            getListParam.isactive = 1;
            getListParam.username = $scope.searchFilter.username;
            var configOption = adminService.handleOnlyErrorResponseConfig;
//            getListParam.start = ($scope.locationModel.currentPage - 1) * $scope.locationModel.limit;
//            getListParam.limit = $scope.locationModel.limit;
            adminService.getUserList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    $scope.locationModel.userList = [];
                    var data = response.data;
                    $scope.locationModel.total = data.total;
                    $scope.locationModel.list = data.list;
                    for (var i = 0; i < $scope.locationModel.list.length; i++)
                    {
                            $scope.locationModel.list[i].newdate = utilityService.parseStrToDate($scope.locationModel.list[i].updated_at);
                            $scope.locationModel.list[i].updated_at = utilityService.parseDateToStr($scope.locationModel.list[i].newdate,'dd/MM/yyyy HH:MM a');
                        if ($scope.locationModel.list[i].latitude != '' && $scope.locationModel.list[i].latitude != 'undefined' && $scope.locationModel.list[i].latitude != null &&
                                $scope.locationModel.list[i].longitude != '' && $scope.locationModel.list[i].longitude != 'undefined' && $scope.locationModel.list[i].longitude != null)
                        {
                            var lat = parseFloat($scope.locationModel.list[i].latitude);
                            var long = parseFloat($scope.locationModel.list[i].longitude);
                            if (lat > 0 || long > 0)
                            {
                                var userInfo = $scope.locationModel.list[i];
                                userInfo.showWindow = false;
//                                userInfo.options = {};
//                                userInfo.options.labelContent = '[46,-77]';
//                                userInfo.options.labelAnchor = "22 0";
//                                userInfo.options.labelClass = "marker-labels"

                                $scope.locationModel.userList.push(userInfo);
                            }
                        }
                    }
                    if ($scope.locationModel.userList.length > 0)
                    {
                        $scope.map.center.latitude = $scope.locationModel.userList[0].latitude;
                        $scope.map.center.longitude = $scope.locationModel.userList[0].longitude;
                    }
                    $scope.locationModel.userList.forEach(function(marker) {
                        marker.onClicked = function() {
                            marker.showWindow = true;
                            $scope.map.center.latitude = marker.latitude;
                            $scope.map.center.longitude = marker.longitude;
                            $scope.$evalAsync();
                        };
                        marker.closeClick = function() {
                            marker.showWindow = false;
                            $scope.$evalAsync();
                        };
                    });



                }
                $scope.locationModel.isUserLoaded = true;
                $scope.locationModel.isLoadingProgress = false;

            });

        };

        $scope.getUserList();
       



        GoogleMapApi.then(function(maps) {
            console.log("GoogleMapApi resolved");
            $timeout(function() {
                $scope.locationModel.showMap = true;
                //  $scope.map = {center: {latitude: 40.1451, longitude: -99.6680}, zoom: 10};
                //$scope.initUpdateDetail();

            }, 1000);


        });

    }
]);





