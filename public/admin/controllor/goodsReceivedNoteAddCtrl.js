
app.controller('goodsReceivedNoteAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.goodsReceivedNoteModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "qty": "",
                    "comments": "",
                    "pack_size": "",
                    "isActive": "1",
                    "customername": "",
                    list: [],
                    "customerInfo": '',
                    "productList": [],
                    "productId": '',
                    "isLoadingProgress": false
                };
        $scope.adminService = adminService;

        $scope.onFocus = function(e) {
            $timeout(function() {
                $(e.target).trigger('input');
            });
        };

        $scope.$on("updateSavedCustomerDetail", function(event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.goodsReceivedNoteModel.customerInfo = customerDetail;
        });

        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.order_add_form != 'undefined' && typeof $scope.order_add_form.$pristine != 'undefined' && !$scope.order_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.order_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function() {

            $scope.goodsReceivedNoteModel.user = '';
        }

        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date();
        $scope.goodsReceivedNoteModel.billdate = $scope.currentDate;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            }
        }

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };


        $scope.getUomList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.isLoadedTax = false;



        $scope.deleteProduct = function(id)
        {
            var index = -1;
            for (var i = 0; i < $scope.goodsReceivedNoteModel.list.length; i++) {
                if (id == $scope.goodsReceivedNoteModel.list[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.goodsReceivedNoteModel.list.splice(index, 1);
        };

        //    $scope.getProductList = function(val)
        //    {
        //        var autosearchParam = {};
        //        autosearchParam.name = val;
        //        autosearchParam.is_active = 1;
        //        autosearchParam.is_sale = 1;
        //        if(typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
        //        {    
        //            autosearchParam.scopeId = $scope.selectedAssignFrom.id;
        //        }                           
        //
        //        return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
        //        {
        //            var data = responseData.data.list;
        //            var hits = data;
        //            if (hits.length > 10)
        //            {
        //                hits.splice(10, hits.length);
        //            }
        //            return hits;
        //        });        
        //    }

        $scope.formatProductModel = function(model, index)
        {
            if (model != null)
            {
                $scope.updateProductInfo(model, index);
                return model.name;
            }
            return  '';
        }

        $scope.updateProductInfo = function(model, index)
        {
            if ($scope.goodsReceivedNoteModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.goodsReceivedNoteModel.list[index].id = model.id;
                $scope.goodsReceivedNoteModel.list[index].sku = model.sku;
                $scope.goodsReceivedNoteModel.list[index].sales_price = model.sales_price;
                if (model.qty != null && model.qty != '')
                {
                    $scope.goodsReceivedNoteModel.list[index].qty = model.qty;
                } else
                {
                    $scope.goodsReceivedNoteModel.list[index].qty = 1;
                }
                $scope.goodsReceivedNoteModel.list[index].pack_size = model.pack_size;
                $scope.goodsReceivedNoteModel.list[index].comments = model.comments;
                $scope.goodsReceivedNoteModel.list[index].uomInfo = {
                    "name": model.uom,
                }
                if (model.uomid != undefined)
                {
                    $scope.goodsReceivedNoteModel.list[index].uomid = model.uomid;
                }
                if (model.uom_id != undefined)
                {
                    $scope.goodsReceivedNoteModel.list[index].uomid = model.uom_id;
                }
                if (model.uomid != undefined && model.uom_id != undefined)
                {
                    $scope.goodsReceivedNoteModel.list[index].uomid = "";
                }

                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.goodsReceivedNoteModel.list.length);
                $scope.createNewProduct(index);
                return;
            } else
            {
                $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.initValidateProductDataAndAddNew = function(index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function() {
                        $scope.createNewProduct(index);
                    },
                    300);
        };

        $scope.formValidator = function()
        {
            //performace issue in form validator bug
            if ($scope.order_add_form.$submitted)
            {
                $timeout(function() {
                    $scope.order_add_form.$submitted = false;
                }, 0);
                $timeout(function() {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            } else
            {
                $timeout(function() {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.createNewProduct = function(index)
        {
            var formDataError = false;
            if (index != -1)
            {
                var productContainer = window.document.getElementById('invoice_product_container');
                var errorCell = angular.element(productContainer).find('.has-error').length;
                if (errorCell > 0)
                {
                    formDataError = true;
                }
            }

            if (!formDataError)
            {
                if (index == $scope.goodsReceivedNoteModel.list.length - 1)
                {

                    var newRow = {
                        "id": '',
                        "productName": '',
                        "sku": '',
                        "qty": '',
                        "comments": '',
                        "pack_size": '',
                        "uom": '',
                        "uomid": ''
                    }
                    $scope.goodsReceivedNoteModel.list.push(newRow);
                }
            }

        }
        $scope.addNewProduct = function()
        {
            var newRow = {
                "id": '',
                "productName": '',
                "sku": '',
                "qty": '',
                "comments": '',
                "pack_size": '',
                "uom": '',
                "uomid": ''
            }
            $scope.goodsReceivedNoteModel.list.push(newRow);
        }



        $scope.$on("updateSelectedProductEvent", function(event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.goodsReceivedNoteModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.goodsReceivedNoteModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.goodsReceivedNoteModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }

                }
                if (isDuplicateProduct)
                {
                    $scope.goodsReceivedNoteModel.list[duplicateProductIndex].qty = $scope.goodsReceivedNoteModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                } else
                {
                    var newRow = {
                        "id": selectedItems.id,
                        "productName": selectedItems.name,
                        "sku": selectedItems.sku,
                        "qty": 1,
                        "comments": selectedItems.comments,
                        "pack_size": selectedItems.pack_size,
                        "uom": selectedItems.uom,
                        "uomid": selectedItems.uom_id,
                    }
                    $scope.goodsReceivedNoteModel.list.push(newRow);
                    console.log("upd 2");
                    return;
                }
            } else
            {
                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "qty": 1,
                    "comments": selectedItems.comments,
                    "pack_size": selectedItems.pack_size,
                    "uom": selectedItems.uom,
                    "uomid": selectedItems.uom_id,
                }
                $scope.goodsReceivedNoteModel.list.push(newRow);
            }
        });


        $scope.getProductList = function()
        {
            $scope.goodsReceivedNoteModel.isLoadingProgress = true;
            var getParam = {};

            getParam.is_active = 1;
            getParam.is_sale = 1;
            getParam.type = 'Product';
            getParam.localData = 1;
            if (typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
            {
                getParam.scopeId = $scope.selectedAssignFrom.id;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductList(getParam, configOption).then(function(response) {

                var data = response.data;
                $scope.goodsReceivedNoteModel.productList = data.list;
                $scope.goodsReceivedNoteModel.isLoadingProgress = false;
            });
        }

        $scope.getCustomerInfo = function() {
            $scope.goodsReceivedNoteModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.cust_id;
            getListParam.name = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.is_active = 1;
            adminService.getCustomerDetail(getListParam).then(function(response) {
                var data = response.data;
                if (data.total != 0)
                {
                    var customerInfo = data.data;
                    $scope.goodsReceivedNoteModel.customerInfo = {};
                    $scope.goodsReceivedNoteModel.customerInfo.id = customerInfo.id;
                    $scope.goodsReceivedNoteModel.customerInfo.fname = customerInfo.fname;
                    $scope.goodsReceivedNoteModel.customerInfo.billing_address = customerInfo.billing_address;
                    $scope.goodsReceivedNoteModel.customerInfo.phone = customerInfo.phone;
                    $scope.goodsReceivedNoteModel.customerInfo.email = customerInfo.email;
                    $scope.goodsReceivedNoteModel.isLoadingProgress = false;
                }
            });
        };

        if (typeof $stateParams.id == 'undefined' || $stateParams.id == null || $stateParams.id == '' || $stateParams.id == 0)
        {
            $scope.updateProductInfo();
        }
        $scope.getProductList( );

        $scope.createOrder = function(forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.goodsReceivedNoteModel.list.length; i++)
            {
                if (i != $scope.goodsReceivedNoteModel.list.length - 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.goodsReceivedNoteModel.list[i].productname != '' && $scope.goodsReceivedNoteModel.list[i].productname != null &&
                                $scope.goodsReceivedNoteModel.list[i].uomInfo != '' && $scope.goodsReceivedNoteModel.list[i].uomInfo != null &&
                                $scope.goodsReceivedNoteModel.list[i].qty != '' && $scope.goodsReceivedNoteModel.list[i].qty != null &&
                                $scope.goodsReceivedNoteModel.list[i].pack_size != '' && $scope.goodsReceivedNoteModel.list[i].pack_size != null)
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                        }
                    } else
                    {
                        if ($scope.goodsReceivedNoteModel.list[i].productname != '' && $scope.goodsReceivedNoteModel.list[i].productname != null &&
                                $scope.goodsReceivedNoteModel.list[i].qty != '' && $scope.goodsReceivedNoteModel.list[i].qty != null &&
                                $scope.goodsReceivedNoteModel.list[i].pack_size != '' && $scope.goodsReceivedNoteModel.list[i].pack_size != null
                                )
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                        }
                    }
                }
                if ($scope.goodsReceivedNoteModel.list.length == 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.goodsReceivedNoteModel.list[0].productname != '' && $scope.goodsReceivedNoteModel.list[0].productname != null &&
                                $scope.goodsReceivedNoteModel.list[0].uomInfo != '' && $scope.goodsReceivedNoteModel.list[0].uomInfo != null &&
                                $scope.goodsReceivedNoteModel.list[0].qty != '' && $scope.goodsReceivedNoteModel.list[0].qty != null &&
                                $scope.goodsReceivedNoteModel.list[i].pack_size != '' && $scope.goodsReceivedNoteModel.list[i].pack_size != null 
                               )
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                        }
                    } else
                    {
                        if ($scope.goodsReceivedNoteModel.list[0].productname != '' && $scope.goodsReceivedNoteModel.list[0].productname != null &&
                                $scope.goodsReceivedNoteModel.list[0].qty != '' && $scope.goodsReceivedNoteModel.list[0].qty != null &&
                                $scope.goodsReceivedNoteModel.list[i].pack_size != '' && $scope.goodsReceivedNoteModel.list[i].pack_size != null )
                        {
                            $scope.isSave = true;
                        } else
                        {
                            $scope.isSave = false;
                        }
                    }
                }
            }

            if ($scope.isSave)
            {
                $scope.saveOrder();
            } else
            {
                sweet.show('Oops...', 'Fill Product Detail..', 'error');
                $scope.isDataSavingProcess = false;
            }
        }



        $scope.saveOrder = function()
        {
            var createOrderParam = [];
            var headers = {};
            headers['screen-code'] = 'goodsReceivedNote';
            if ($scope.goodsReceivedNoteModel.list.length < 1)
            {
                return;
            }
            var length = 0;
            if ($scope.goodsReceivedNoteModel.list.length == 1)
            {
                length = 2;
            } else
            {
                length = $scope.goodsReceivedNoteModel.list.length;
            }
            for (var i = 0; i < length - 1; i++)
            {
                var ordereditems = {};
                if (typeof $scope.goodsReceivedNoteModel.billdate == 'object')
                {
                    $scope.goodsReceivedNoteModel.billdate = utilityService.parseDateToStr($scope.goodsReceivedNoteModel.billdate, 'yyyy-MM-dd');
                }
                ordereditems.date = utilityService.changeDateToSqlFormat($scope.goodsReceivedNoteModel.billdate, 'yyyy-MM-dd');
                ordereditems.customer_id = $scope.goodsReceivedNoteModel.customerInfo.id;

                ordereditems.product_id = $scope.goodsReceivedNoteModel.list[i].id;
                if ($scope.goodsReceivedNoteModel.list[i].productname.name != undefined && $scope.goodsReceivedNoteModel.list[i].productname.name != null && $scope.goodsReceivedNoteModel.list[i].productname.name != '')
                {
                    ordereditems.product_name = $scope.goodsReceivedNoteModel.list[i].productname.name;
                } else
                {
                    ordereditems.product_name = $scope.goodsReceivedNoteModel.list[i].productname;
                }

                ordereditems.qty = parseFloat($scope.goodsReceivedNoteModel.list[i].qty).toFixed(2);
                ordereditems.pack_size = parseFloat($scope.goodsReceivedNoteModel.list[i].pack_size).toFixed(2);
                ordereditems.comments = $scope.goodsReceivedNoteModel.list[i].comments;
                if (typeof $scope.goodsReceivedNoteModel.list[i].uomInfo != undefined && $scope.goodsReceivedNoteModel.list[i].uomInfo != null && $scope.goodsReceivedNoteModel.list[i].uomInfo != '') {
                    if ($scope.goodsReceivedNoteModel.list[i].uomInfo.name != null && $scope.goodsReceivedNoteModel.list[i].uomInfo.name != '')
                    {
                        ordereditems.uom = $scope.goodsReceivedNoteModel.list[i].uomInfo.name;
                    }
                }
                ordereditems.uom_id = $scope.goodsReceivedNoteModel.list[i].uomid;
                createOrderParam.push(ordereditems);
            }
            adminService.grnAdd(createOrderParam, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.goodsReceivedNoteList');
                }
                $scope.isDataSavingProcess = false;


            });
        }

    }]);




