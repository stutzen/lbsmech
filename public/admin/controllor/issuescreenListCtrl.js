app.controller('issuescreenListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', '$stateParams', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, $stateParams) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.issueModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            userList: [],
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.isDataSavingProcess = false;
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.issueModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.searchFilter =
                {
                    id: '',
                    bomTaskInfo: {}
                };

        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.issueModel.newIssue = 0;
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter.bomTaskInfo = '';
            $scope.noResults = false;
            $scope.initTableFilter();
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter.bomTaskInfo = '';
            $scope.noResults = false;
            $scope.initTableFilter();
        }
        $scope.showPopup = function (id)
        {
            $scope.selectdIssueId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.selectdIssueId = '';
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };
        $scope.deleteIssueInfo = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;
                var getAdvanceParam = {};
                getAdvanceParam.id = $scope.selectdIssueId;
                var headers = {};
                headers['screen-code'] = 'issuescreen';
                adminService.deleteIssue(getAdvanceParam, getAdvanceParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success === true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.getTaskList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.WORK_ORDER_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatTaskModel = function (model)
        {
            if (model != null)
            {
                return model.bomName;
            }
            return  '';
        };
        $scope.validateCurrentIssue = function (index)
        {
            var retVal = false;
            if (($scope.issueModel.list[index].newIssue <= parseInt($scope.issueModel.list[index].balance)) || ($scope.issueModel.list[index].newIssue == 0))
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.getList = function () {
            var getListParam = {};
            $scope.issueModel.isSearchLoadingProgress = true;
            $scope.issueModel.isLoadingProgress = true;
            $scope.issueModel.isDataSavingProcess = true;
            if ($scope.searchFilter.bomTaskInfo != null && typeof $scope.searchFilter.bomTaskInfo != 'undefined' && typeof $scope.searchFilter.bomTaskInfo.id != 'undefined')
            {
                getListParam.mo_id = $scope.searchFilter.bomTaskInfo.id;
            } else
            {
                getListParam.mo_id = '';
            }
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getIssueList(getListParam, configOption).then(function (response) {
                var data = response.data;
                $scope.issueModel.list = data.list;
                for (var i = 0; i < $scope.issueModel.list.length; i++)
                {
                    $scope.issueModel.list[i].newdate = utilityService.parseStrToDate($scope.issueModel.list[i].date, $rootScope.appConfig.date_format);
                    $scope.issueModel.list[i].date = utilityService.parseDateToStr($scope.issueModel.list[i].date, $rootScope.appConfig.date_format);
                }
                $scope.issueModel.isSearchLoadingProgress = false;
                $scope.issueModel.isLoadingProgress = false;
                $scope.issueModel.isDataSavingProcess = false;
            });
        };
        $scope.getList();
    }]);






