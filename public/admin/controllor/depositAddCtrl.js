




app.controller('depositAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.depositModel = {
            "debit": "",
            "customerid": "",
            "account": "",
            "accountid": "",
            "particulars": "",
            "account_category": "",
            "name": "",
            "date": "",
            "amount": "",
            "created_by": "",
            "status": "",
            "length": "",
            "incomeCategoryInfo": "",
            "isActive": true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.deposit_add_form !== 'undefined' && typeof $scope.deposit_add_form.$pristine !== 'undefined' && !$scope.deposit_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function()
        {
            $scope.create_deposit_form.$setPristine();
            $scope.depositModel.account = "";
            $scope.depositModel.date = "";
            $scope.depositModel.particulars = "";
            $scope.depositModel.incomeCategoryInfo = '';
            $scope.depositModel.amount = "";
            $scope.depositModel.isActive = true;
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

//       $scope.currentDate = new Date();
//       $scope.depositModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };
        $scope.getAccountlist = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.account_id = $scope.depositModel.accountid;
            getListParam.customer_id = $scope.depositModel.customerid;
            getListParam.start = ($scope.depositModel.currentPage - 1) * $scope.depositModel.limit;
            getListParam.limit = $scope.depositModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.depositModel.Accountlist = data.list;
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getIncomeCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = 'income';
            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatincomeCategoryModel = function(model) {

            if (model !== null && model != undefined)
            {

                return model.name;
            }
            return  '';
        };
        $scope.createDeposit = function() {
            if($scope.isDataSavingProcess == false)
            {
                
                $scope.isDataSavingProcess = true;
                var createDepositParam = {};
                var headers = {};
                headers['screen-code'] = 'transdeposit';
                for (var i = 0; i < $scope.depositModel.Accountlist.length; i++)
                {
                    if ($scope.depositModel.account == $scope.depositModel.Accountlist[i].id)
                    {
                        createDepositParam.account = $scope.depositModel.Accountlist[i].account;
                    }
                }
                createDepositParam.account_id = $scope.depositModel.account;
                createDepositParam.transaction_date = $filter('date')($scope.depositModel.date, 'yyyy-MM-dd');
                createDepositParam.particulars = $scope.depositModel.particulars;
                createDepositParam.account_category = '';
                if ($scope.depositModel.incomeCategoryInfo != undefined && $scope.depositModel.incomeCategoryInfo.name != undefined)
                {
                    createDepositParam.account_category = $scope.depositModel.incomeCategoryInfo.name;
                }
                createDepositParam.credit = $scope.depositModel.amount;
                createDepositParam.name = "",
                        createDepositParam.is_active = 1,
                        createDepositParam.customer_id = "",
                        //createDepositParam.account = "",
                        createDepositParam.payment_mode = "",
                        adminService.createDeposit(createDepositParam, headers).then(function(response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.deposit');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getAccountlist();

    }]);




