

app.controller('serviceEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.serviceModel = {
            id: '',
            name: '',
            sku: '',
            uom: '',
            uomList: [],
            description: '',
            salesPrice: '',
            is_active: '',
            is_purchase: '',
            is_sale: '',
            hsncode: '',
            isLoadingProgress: false,
            attachment:[]
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.service_edit_form != 'undefined' && typeof $scope.service_edit_form.$pristine != 'undefined' && !$scope.service_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.service_edit_form.$setPristine();
            if (typeof $scope.service_edit_form != 'undefined')
            {
                $scope.serviceModel.name = "";
                $scope.serviceModel.sku = "";
                $scope.serviceModel.uom = "";
                $scope.serviceModel.salesPrice = '';
                $scope.serviceModel.description = "";
                $scope.serviceModel.is_active = '';
                $scope.serviceModel.is_purchase = '';
                $scope.serviceModel.is_sale = '';
                $scope.serviceModel.hsncode = '';
            }
            $scope.getServiceInfo();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isUOMLoaded)
            {
                $scope.serviceModel.isLoadingProgress = false;
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.isUOMLoaded = false;
        $scope.getCUOMList = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = ($scope.serviceModel.currentPage - 1) * $scope.serviceModel.limit;
            getListParam.limit = $scope.serviceModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUom(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.serviceModel.uomList = data.list;
                $scope.isUOMLoaded = true;
                $scope.isDataSavingProcess = false;
            });
        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.getServiceInfo = function() {

            $scope.serviceModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var serviceListParam = {};
                serviceListParam.id = $stateParams.id;

                serviceListParam.start = 0;
                serviceListParam.limit = 1;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getProductList(serviceListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.serviceModel = data.list[0];
                        $scope.serviceModel.hsncode = $scope.serviceModel.hsn_code;
                        $scope.serviceModel.is_active = $scope.serviceModel.is_active == 1 ? true : false;
                        $scope.serviceModel.is_sale = $scope.serviceModel.is_sale == 1 ? true : false;
                        $scope.serviceModel.is_purchase = $scope.serviceModel.is_purchase == 1 ? true : false;
                        $scope.getCUOMList();
                    }

                });
            }
        };

        $scope.getServiceInfo( );

        /*
         *  It used to format the speciality model value in UI
         */
        $scope.editServiceInfo = function() {
            
            if($scope.isDataSavingProcess)
            {
                return;
            }
            
            $scope.isDataSavingProcess = true;
            var addServiceParam = {};
            var headers = {};
            headers['screen-code'] = 'service';
            //  addServiceParam.id = 0;
            addServiceParam.name = $scope.serviceModel.name;
            addServiceParam.sku = $scope.serviceModel.sku;
            addServiceParam.hsn_code = $scope.serviceModel.hsncode;
            addServiceParam.sales_price = parseFloat($scope.serviceModel.sales_price);
            addServiceParam.comments = $scope.serviceModel.description;
            // addServiceParam.uom = $scope.serviceModel.uom;
            addServiceParam.type = "Service";
            addServiceParam.is_active = $scope.serviceModel.is_active ? 1 : 0;
            addServiceParam.is_sale = $scope.serviceModel.is_sale ? 1 : 0;
            addServiceParam.is_purchase = $scope.serviceModel.is_purchase ? 1 : 0;
            addServiceParam.attachment = [];
            if ($scope.serviceModel.uom != "")
            {
                for (var loopIndex = 0; loopIndex < $scope.serviceModel.uomList.length; loopIndex++)
                {
                    if ($scope.serviceModel.uom == $scope.serviceModel.uomList[loopIndex].name)
                    {
                        addServiceParam.uom_id = $scope.serviceModel.uomList[loopIndex].id;
                        break;
                    }
                }
            }
            else
            {
                addServiceParam.uom_id = "";
            }
            addServiceParam.min_stock_qty = '';
            addServiceParam.opening_stock = '';
            addServiceParam.has_inventory = 0;
            adminService.editService(addServiceParam, $stateParams.id, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.serviceList');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getCUOMList();

    }]);



