


app.controller('departmentListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.departmentModel = {
        currentPage: 1,
        total: 0,
        limit: 10,
        list: [],
        status: '',
        isLoadingProgress: false
    };

    $scope.pagePerCount = [50, 100];
    $scope.departmentModel.limit = $scope.pagePerCount[0];

    $scope.searchFilter = {
        departmentname: ''
    };

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }
    $scope.refreshScreen = function()
    {
        $scope.searchFilter = {
            departmentname: ''
        };
        $scope.initTableFilter();
    }

    $scope.selectCategoryId = '';
    $scope.showdeletePopup = false;
    $scope.isdeleteCategoryLoadingProgress = false;
    $scope.showPopup = function(id)
    {
        $scope.selectCategoryId = id;
        $scope.showdeletePopup = true;
    };

    $scope.closePopup = function()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteCategoryLoadingProgress = false;
    };

    $scope.deleteDepartment = function( )
    {
        if($scope.isdeleteCategoryLoadingProgress == false)
        {
            $scope.isdeleteCategoryLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $scope.selectCategoryId;
            var headers = {};
            headers['screen-code'] = 'department';
            adminService.deleteDepartment(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }
                $scope.isdeleteCategoryLoadingProgress = false;
            });
        } 
    };

    $scope.getList = function() {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'department';
        getListParam.id = '';
        getListParam.name = $scope.searchFilter.departmentname;
        getListParam.start = ($scope.departmentModel.currentPage - 1) * $scope.departmentModel.limit;
        getListParam.limit = $scope.departmentModel.limit;
        $scope.departmentModel.isLoadingProgress = true;
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getDepartmentList(getListParam, configOption, headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.departmentModel.list = data;
                $scope.departmentModel.total = response.data.total;
            }
            $scope.departmentModel.isLoadingProgress = false;
        });

    };


    $scope.getList();
}]);








