/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
app.controller('cityListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $timeout) {

    $rootScope.getNavigationBlockMsg = null;
    $scope.cityModel = {
            
        currentPage: 1,
        total: 0,
        limit: 4,
        list: [],
        country_list:[],
        state_list:[],
        isLoadingProgress: true

    };
    $scope.searchFilterNameValue = ''

    $scope.pagePerCount = [50, 100];
    $scope.cityModel.limit = $scope.pagePerCount[0];

    $scope.searchFilter = {
        name: '',
        country_name: '',
        state_name: '',
        country_id: '',
        state_id: ''
            
    };

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }

        
    $scope.showdeletePopup = false;
    $scope.isdeleteProgress = false;

    $scope.showPopup = function(id)
    {
        $scope.selectStageId = id;
        $scope.showdeletePopup = true;
    };

    $scope.closePopup = function()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
    };

    $scope.deleteCityInfo = function( )
    {
        if($scope.isdeleteProgress == false)
        {
            $scope.isdeleteProgress = true;
               
            var getListParam = {};
            getListParam.id = $scope.selectStageId;
            var headers = {};
            headers['screen-code'] = 'city';
            adminService.deleteCity(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }
                $scope.isdeleteProgress = false;
            });
        }
    };

    $scope.refreshScreen = function()
    {
        $scope.searchFilter = {
            name: '',
            country_name:'',
            state_name:'',
            country_id:'',
            state_id:''
        };
        $scope.initTableFilter();
    }

    $scope.getList = function() {          

        $scope.cityModel.isLoadingProgress = true;
        var cityListParam = {};
        var headers = {};
        headers['screen-code'] = 'city';
        cityListParam.id='';
        cityListParam.country_id = $scope.searchFilter.country_id;
        cityListParam.state_id = $scope.searchFilter.state_id;
        cityListParam.name = $scope.searchFilter.name;
//        cityListParam.country_name = $scope.searchFilter.country_name;
//        cityListParam.state_name = $scope.searchFilter.state_name;
        cityListParam.is_active = 1;
        cityListParam.start = ($scope.cityModel.currentPage - 1) * $scope.cityModel.limit;
        cityListParam.limit = $scope.cityModel.limit;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getCityList(cityListParam, configOption, headers).then(function(response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.cityModel.list = data.list;
                $scope.cityModel.total = data.total;
            }
            $scope.cityModel.isLoadingProgress = false;
        });

    };

    $scope.getList();
        
    $scope.isLoadedCountryList = false;
    $scope.getcountryList = function()
    {
        var getListParam = {};
        getListParam.id = '';
        $scope.isLoadedCountryList = false;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getCountryList(getListParam, configOption).then(function(response)
        {
            var data = response.data;
            $scope.cityModel.country_list = data.list;
            $scope.isLoadedCountryList = true;
        });
    };
    $scope.getcountryList();
        
    $scope.isLoadedStateList = false;
    $scope.getstateList = function()
    {
            
        var getListParam = {};
        getListParam.country_id = $scope.searchFilter.country_id;
        $scope.isLoadedStateList = false;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getStateList(getListParam, configOption).then(function(response)
        {
            var data = response.data;
            $scope.cityModel.state_list = data.list;
            $scope.isLoadedStateList = true;
        });
    };
     $scope.CountryChange = function () 
        {
            $scope.searchFilter.state_id = '';
            $scope.getstateList();
            
        }
// $scope.getstateList();

}]);







