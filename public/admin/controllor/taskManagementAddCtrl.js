
app.controller('taskManagementAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', '$httpService', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, $httpService, APP_CONST, sweet) {

            $rootScope.getNavigationBlockMsg = null;

            $scope.taskManagementAddModel = {
                  currentPage: 1,
                  total: 0,
                  limit: 10,
                  taskList: [],
                  staffList: [],
                  typeList: [],
                  userList: [],
                  statusList: [],
                  priorityList: [],
                  isLoadingProgress: false,
                  title: '',
                  task: '',
                  taskId: '',
                  staffId: '',
                  typeId: '',
                  date: '',
                  status: '',
                  id: '',
                  priority: '',
                  configType: '',
                  estimatedDate: '',
                  attachments: [],
                  comments: '',
                  "dealId": '',
                  "dealName": '',
                  commentsList: [],
                  createdFor: '',
                  employeeInfo: '',
                  followers: []
            };

            $scope.taskManagementAddModel.isCreateTask = true;
            $scope.taskManagementAddModel.isStaffLoaded = false;
            $scope.taskManagementAddModel.isTypeLoaded = false;
            $scope.taskManagementAddModel.isUserLoaded = false;
            $scope.isCommentSavingProcess = false;
            $scope.pagePerCount = [10, 20, 50, 100];
            $scope.taskManagementAddModel.limit = $scope.pagePerCount[2];
            $scope.adminService = adminService;
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.searchFilter = {
                  typeIds: [],
                  assignTo: [],
                  createdBy: [],
                  status: [],
                  priority: []
            };

            $scope.taskDetail = '';

            $scope.searchFilterValue = "";
            $scope.clearFilter = function ()
            {
                  $scope.searchFilter = {
                  };
                  $scope.initTableFilter();
            }

            $scope.initUpdateDetailTimeoutPromise = null;
            $scope.initUpdateDetail = function ()
            {

                  if ($scope.initUpdateDetailTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                  }

                  if ($scope.taskManagementAddModel.isTypeLoaded && $scope.taskManagementAddModel.isStaffLoaded && $scope.taskManagementAddModel.isUserLoaded)
                  {
                        $scope.updateTaskAtivityDetail();
                  } else
                  {
                        $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                  }
            }

            $scope.showAddPopup = function ()
            {
                  $scope.taskManagementAddModel.isCreateTask = true;
            };

            $scope.closeAddPopup = function ()
            {
                  $scope.taskManagementAddModel.isCreateTask = false;
                  $state.go('app.taskManagement');
            };

            $scope.startDateOpen = false;
            $scope.estiDateOpen = false;
            $scope.currentDate = new Date();
            $scope.dateFormat = 'dd-MM-yyyy';
            $scope.validationFactory = ValidationFactory;
            $scope.openDate = function (index) {

                  if (index == 0)
                  {
                        $scope.startDateOpen = true;
                  } else if (index == 1)
                  {
                        $scope.estiDateOpen = true;
                  }

            }
            $scope.dateOptions = {
                  minDate: new Date()
            }

            $scope.formReset = function () {

                  if (typeof $scope.task_activity_add_form != 'undefined')
                  {
                        $scope.task_activity_add_form.$setPristine();

                  }

                  $scope.taskManagementAddModel = {
                        currentPage: 1,
                        total: 0,
                        limit: 10,
                        taskList: [],
                        staffList: [],
                        typeList: [],
                        userList: [],
                        statusList: [],
                        priorityList: [],
                        isLoadingProgress: false,
                        title: '',
                        task: '',
                        taskId: '',
                        staffId: '',
                        typeId: '',
                        date: '',
                        status: '',
                        priority: '',
                        configType: '',
                        estimatedDate: '',
                        attachments: [],
                        comments: '',
                        commentsList: [],
                        createdFor: '',
                        employeeInfo: '',
                        followers: []
                  };
                  $scope.setStatus();
                  $scope.setPriority();
                  $scope.getTaskTypeList();
                  $scope.getEmployeeList();
                  $scope.getUserList();
                  $scope.getdealListInfo();
            };

            $scope.getTaskInfo = function (val) {
                  var getGroupListParam = {};
                  getGroupListParam.task_name = val;
                  getGroupListParam.is_active = 1;
                  return $httpService.get(APP_CONST.API.TASK_LIST, getGroupListParam, false).then(function (responseData) {
                        var data = responseData.data;
                        var hits = data.list;
                        if (hits.length > 10)
                        {
                              hits.splice(10, hits.length);
                        }
                        return hits;
                  });
            };

            $scope.formatTaskModel = function (model) {

                  if (model != null)
                  {
                        $scope.updateTaskInfo(model);
                        return model.task_name;
                  }
                  return  '';
            };

            $scope.updateTaskInfo = function (model)
            {
                  if (typeof model == undefined)
                        return;
                  if (model.task_name != undefined && model.task_name != '')
                  {
                        $scope.taskManagementAddModel.title = model.task_name;
                        $scope.taskManagementAddModel.taskId = model.id;
                        return;
                  } else
                  {
                        $scope.taskManagementAddModel.title = model;
                        $scope.taskManagementAddModel.taskId = 0;
                        return;
                  }
            };


            $scope.getEmployeeList = function () {

                  var getListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'employee';
                  getListParam.is_active = 1;
                  $scope.taskManagementAddModel.isStaffLoaded = false;
                  $scope.taskManagementAddModel.isLoadingProgress = true;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getEmployeeList(getListParam, configOption, headers).then(function (response) {
                        var data = response.data;
                        $scope.taskManagementAddModel.staffList = data.list;
                        for (var i = 0; i < $scope.taskManagementAddModel.staffList.length; i++)
                        {
                              $scope.taskManagementAddModel.staffList[i].isChecked = false;
                        }
                        $scope.taskManagementAddModel.isStaffLoaded = true;
                        $scope.taskManagementAddModel.isLoadingProgress = false;
                  });

            };

            $scope.getUserList = function () {

                  var getListParam = {};
                  getListParam.isactive = 1;
                  var headers = {};
                  headers['screen-code'] = 'users';
                  $scope.taskManagementAddModel.isUserLoaded = false;
                  $scope.taskManagementAddModel.isLoadingProgress = true;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getUserList(getListParam, configOption, headers).then(function (response) {
                        var data = response.data;
                        $scope.taskManagementAddModel.userList = data.list;
                        for (var i = 0; i < $scope.taskManagementAddModel.userList.length; i++)
                        {
                              $scope.taskManagementAddModel.userList[i].isChecked = false;
                        }
                        $scope.taskManagementAddModel.isUserLoaded = true;
                        $scope.taskManagementAddModel.isLoadingProgress = false;
                  });

            };

            $scope.getTaskTypeList = function () {

                  $scope.taskManagementAddModel.isLoadingProgress = true;
                  var headers = {};
                  headers['screen-code'] = 'empTaskType';
                  var getListParam = {};
                  getListParam.is_active = 1;
                  $scope.taskManagementAddModel.isTypeLoaded = false;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getEmployeeTaskTypeList(getListParam, configOption, headers).then(function (response) {
                        var data = response.data;
                        $scope.taskManagementAddModel.typeList = data.list;
                        for (var i = 0; i < $scope.taskManagementAddModel.typeList.length; i++)
                        {
                              $scope.taskManagementAddModel.typeList[i].isChecked = false;
                        }
                        $scope.taskManagementAddModel.isTypeLoaded = true;
                        $scope.taskManagementAddModel.isLoadingProgress = false;

                  });

            };

            $scope.setStatus = function ()
            {
                  $scope.taskManagementAddModel.statusList = [];
                  var status1 = {
                        name: "New",
                        ischecked: false
                  };
                  $scope.taskManagementAddModel.statusList.push(status1);
                  var status2 = {
                        name: "InProgress",
                        ischecked: false
                  };
                  $scope.taskManagementAddModel.statusList.push(status2);
                  var status3 = {
                        name: "Completed",
                        ischecked: false
                  };
                  $scope.taskManagementAddModel.statusList.push(status3);
                  var status4 = {
                        name: "Closed",
                        ischecked: false
                  };
                  $scope.taskManagementAddModel.statusList.push(status4);
            };

            $scope.setPriority = function ()
            {
                  $scope.taskManagementAddModel.priorityList = [];
                  var priority1 = {
                        name: "High",
                        ischecked: false
                  };
                  $scope.taskManagementAddModel.priorityList.push(priority1);
                  var priority2 = {
                        name: "Medium",
                        ischecked: false
                  };
                  $scope.taskManagementAddModel.priorityList.push(priority2);
                  var priority3 = {
                        name: "Low",
                        ischecked: false
                  };
                  $scope.taskManagementAddModel.priorityList.push(priority3);
            };


            $scope.saveTaskMgt = function () {

                  $scope.isDataSavingProcess = true;
                  var headers = {};
                  headers['screen-code'] = 'taskManagement';
                  var createTaskActivityParam = {};

                  if (typeof $scope.taskManagementAddModel.task.id != 'undefined' && typeof $scope.taskManagementAddModel.task.task_name != 'undefined' && typeof $scope.taskManagementAddModel.task == 'object')
                  {
                        createTaskActivityParam.title = $scope.taskManagementAddModel.task.task_name;
                        createTaskActivityParam.task_name = $scope.taskManagementAddModel.task.task_name;
                        createTaskActivityParam.task_id = $scope.taskManagementAddModel.taskId;
                  } else
                  {
                        createTaskActivityParam.title = $scope.taskManagementAddModel.task;
                        createTaskActivityParam.task_name = $scope.taskManagementAddModel.task;
                        createTaskActivityParam.task_id = 0;
                  }
                  createTaskActivityParam.emp_id = $scope.taskManagementAddModel.staffId;
                  for (var i = 0; i < $scope.taskManagementAddModel.staffList.length; i++)
                  {
                        if ($scope.taskManagementAddModel.staffId == $scope.taskManagementAddModel.staffList[i].id)
                        {
                              createTaskActivityParam.emp_name = $scope.taskManagementAddModel.staffList[i].f_name;
                        }
                  }
                  createTaskActivityParam.created_for = $scope.taskManagementAddModel.createdFor;
                  createTaskActivityParam.type_id = $scope.taskManagementAddModel.typeId;
                  for (var i = 0; i < $scope.taskManagementAddModel.typeList.length; i++)
                  {
                        if ($scope.taskManagementAddModel.typeId == $scope.taskManagementAddModel.typeList[i].id)
                        {
                              createTaskActivityParam.type_name = $scope.taskManagementAddModel.typeList[i].name;
                        }
                  }
                  createTaskActivityParam.configType = '';
                  if ($scope.taskManagementAddModel.dealId != '' && $scope.taskManagementAddModel.dealId != null && $scope.taskManagementAddModel.dealId != 'undefined')
                  {
                        createTaskActivityParam.ref_id = $stateParams.deal_id;
                        createTaskActivityParam.ref_name = 'deal';
                  } else
                  {
                        createTaskActivityParam.ref_id = '';
                        createTaskActivityParam.ref_name = '';
                  }
                  if ($scope.taskManagementAddModel.estimatedDate != '' && $scope.taskManagementAddModel.estimatedDate != undefined && $scope.taskManagementAddModel.estimatedDate != null)
                  {
                        if (typeof $scope.taskManagementAddModel.estimatedDate == 'object')
                        {
                              $scope.taskManagementAddModel.estimatedDate = utilityService.parseDateToStr($scope.taskManagementAddModel.estimatedDate, $scope.adminService.appConfig.date_format);
                        }
                        createTaskActivityParam.estimated_date = utilityService.changeDateToSqlFormat($scope.taskManagementAddModel.estimatedDate, $scope.adminService.appConfig.date_format);
                  } else
                  {
                        createTaskActivityParam.estimated_date = null;
                  }
                  if ($scope.taskManagementAddModel.date != '' && $scope.taskManagementAddModel.date != undefined && $scope.taskManagementAddModel.date != null)
                  {
                        if (typeof $scope.taskManagementAddModel.date == 'object')
                        {
                              $scope.taskManagementAddModel.date = utilityService.parseDateToStr($scope.taskManagementAddModel.date, $scope.adminService.appConfig.date_format);
                        }
                        createTaskActivityParam.date = utilityService.changeDateToSqlFormat($scope.taskManagementAddModel.date, $scope.adminService.appConfig.date_format);
                  } else
                  {
                        createTaskActivityParam.date = null;
                  }
                  //            createTaskActivityParam.estimatedDate = $filter('date')($scope.taskManagementAddModel.estimatedDate, 'yyyy-MM-dd');
                  //           createTaskActivityParam.date = $filter('date')($scope.taskManagementAddModel.date, 'yyyy-MM-dd');
                  createTaskActivityParam.from_time = '';
                  createTaskActivityParam.to_time = '';
                  createTaskActivityParam.comments = '';
                  createTaskActivityParam.status = $scope.taskManagementAddModel.status;
                  createTaskActivityParam.priority = $scope.taskManagementAddModel.priority;
                  createTaskActivityParam.attachments = [];
                  createTaskActivityParam.followers = [];
                  for (var i = 0; i < $scope.taskManagementAddModel.followers.length; i++)
                  {
                        var saveFollowerParam = {};
                        saveFollowerParam.emp_id = $scope.taskManagementAddModel.followers[i].id;
                        createTaskActivityParam.followers.push(saveFollowerParam);
                  }
                  var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                  adminService.saveTaskMgt(createTaskActivityParam, configOption, headers).then(function (response) {
                        if (response.data.success == true)
                        {
                              //                    if ($scope.taskManagementAddModel.dealId != '' && $scope.taskManagementAddModel.dealId != null && $scope.taskManagementAddModel.dealId != 'undefined')
                              //                    {
                              //                        $state.go('app.dealedit', {'id': $stateParams.deal_id});
                              //                        $scope.isDataSavingProcess = false;
                              //                    }
                              //                    else
                              //                    {
                              $scope.formReset();
                              $scope.isDataSavingProcess = false;
                              $state.go('app.taskManagement');
                              //                    }
                        } else
                        {
                              $scope.isDataSavingProcess = false;
                        }
                  });
            };

            $scope.getEmployeeSearchList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.fname = val;
                  autosearchParam.is_active = 1;
                  return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
                  {
                        var data = responseData.data.list;
                        var hits = data;
                        if (hits.length > 10)
                        {
                              hits.splice(10, hits.length);
                        }
                        return hits;
                  });
            };

            $scope.formatEmployeModel = function (model)
            {
                  if (model != null)
                  {
                        if (model.f_name != undefined && model.team_name != undefined && model.emp_code != undefined)
                        {
                              return model.f_name + '(' + model.team_name + ',' + model.emp_code + ')';
                        } else if (model.f_name != undefined && model.team_name != undefined && model.company != undefined)
                        {
                              return model.f_name + '(' + model.team_name + ')';
                        }
                        return model.f_name;
                  }
                  return  '';
            };

            $scope.addNewFollower = function () {
                  var addFollwerParam = {};
                  var found = false;
                  if ($scope.taskManagementAddModel.employeeInfo != null && $scope.taskManagementAddModel.employeeInfo != 'undefined' && $scope.taskManagementAddModel.employeeInfo != '' && $scope.taskManagementAddModel.employeeInfo.id != undefined)
                  {
                        addFollwerParam.id = $scope.taskManagementAddModel.employeeInfo.id;
                        addFollwerParam.f_name = $scope.taskManagementAddModel.employeeInfo.f_name;
                        for (var i = 0; i < $scope.taskManagementAddModel.followers.length; i++)
                        {
                              if ($scope.taskManagementAddModel.followers[i].id == addFollwerParam.id)
                              {
                                    found = true;
                                    $scope.taskManagementAddModel.employeeInfo = '';

                              }
                        }
                        if (found == false)
                        {
                              $scope.taskManagementAddModel.followers.push(addFollwerParam);
                              $scope.taskManagementAddModel.employeeInfo = '';
                        } else {
                              swal("oops!", "Follower already added!", "error");

                        }
                  }
            };

            $scope.showDealInfo = false;
            $scope.checkId = function ()
            {
                  if (typeof $stateParams.deal_id != 'undefined' && $stateParams.deal_id != null && $stateParams.deal_id != '' && $stateParams.deal_id != 0) {
                        $scope.getdealListInfo();
                        $scope.showDealInfo = true;
                  }
            }
            $scope.getdealListInfo = function ()
            {
                  $scope.id = '';
                  var getListParam = {};
                  getListParam.id = $stateParams.deal_id;
                  getListParam.is_active = 1;
                  var configOption = adminService.handleOnlyErrorResponseConfig;
                  adminService.getDealList(getListParam, configOption).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              if (data.list.length > 0)
                              {
                                    $scope.dealDetail = data.list[0];
                                    $scope.taskManagementAddModel.dealId = $scope.dealDetail.id;
                                    $scope.taskManagementAddModel.dealName = $scope.dealDetail.name;
                                    $scope.taskManagementAddModel.createdFor = $scope.dealDetail.user_id + '';
                                    console.log("created for id", $scope.taskManagementAddModel.createdFor);
                              }
                        }

                  });
            };

            $scope.setStatus();
            $scope.setPriority();
            $scope.getTaskTypeList();
            $scope.getEmployeeList();
            $scope.getUserList();
            $scope.checkId();
      }]);




