


app.controller('expensetrackerListCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, ValidationFactory, $httpService, $filter, Auth, $state, $timeout, APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.expenseTrackerModel = {
            currentPage: 1,
            total: 0,
            start: 0,
            limit: 10,
            list: [],
            isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.expenseTrackerModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
        };

        $scope.initTableFilterTimeoutPromise = null;
        $scope.validationFactory = ValidationFactory;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.paid_form != 'undefined' && typeof $scope.paid_form.$pristine != 'undefined' && !$scope.paid_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {

            if (typeof $scope.paid_form != 'undefined')
            {
                $scope.paid_form.$setPristine();

            }
            $scope.status = '',
                    $scope.expenseTrackerModel.refNo = ''

        };

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                status:'',
                userInfo:''
            };                       
            $scope.initTableFilter();
        }
        $scope.selectExpenseId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectExpenseId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deleteExpense = function( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectExpenseId;

                adminService.deleteExpenseTracker(getListParam, getListParam.id).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };
        $scope.showStatusAddPopup = false;
        $scope.showStatusPopup = function()
        {
            $scope.showStatusAddPopup = true;
        }
        $scope.closeStatusPopup = function()
        {
            $scope.showStatusAddPopup = false;
        }
        $scope.showPaidAddPopup = false;
        $scope.showPaidPopup = function()
        {
            $scope.showPaidAddPopup = true;
            $scope.showStatusAddPopup = false;
        }
        $scope.closePaidPopup = function()
        {
            $scope.showPaidAddPopup = false;
        }
        $scope.showCheckAddPopup = false;

        $scope.selectAll = function()
        {
            $scope.showCheckAddPopup = true;
            if ($scope.expenseTrackerModel.selectall)
            {
                for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
                {
                    $scope.showCheckAddPopup = true;
                    $scope.expenseTrackerModel.list[i].is_select = true;
                }
            } else
            {
                for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
                {
                    $scope.showCheckAddPopup = false;
                    $scope.expenseTrackerModel.list[i].is_select = false;
                }
            }
        }
        $scope.showCheckbox = function()
        {
            for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
            {
                if ($scope.expenseTrackerModel.list[i].is_select == true)
                {
                    $scope.showCheckAddPopup = true;
                    //return;
                    break;
                } else if ($scope.expenseTrackerModel.list[i].is_select == false)
                {
                    $scope.showCheckAddPopup = false;
                }
            }
        }

        $scope.getList = function() {
            var getListParam = {};
            getListParam.start = ($scope.expenseTrackerModel.currentPage - 1) * $scope.expenseTrackerModel.limit;
            getListParam.limit = $scope.expenseTrackerModel.limit;
            getListParam.is_active = 1;
            getListParam.id = '';
            $scope.expenseTrackerModel.isLoadingProgress = true;
            var headers = {};
            headers['screen-code'] = 'expensetracker';
            getListParam.status = $scope.searchFilter.status;
           if ($scope.searchFilter.userInfo != null && typeof $scope.searchFilter.userInfo != 'undefined' && typeof $scope.searchFilter.userInfo.id != 'undefined')
            {
                getListParam.user_id = $scope.searchFilter.userInfo.id;
            } else
            {
                getListParam.user_id = '';
            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getExpenseTrackerList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.expenseTrackerModel.list = data;
                    $scope.expenseTrackerModel.total = data.total;
                    $scope.showCheckAddPopup = false;
                    $scope.expenseTrackerModel.selectall = '';
                }
                $scope.expenseTrackerModel.isLoadingProgress = false;
            });
        };

        $scope.statusUpdate = function(status) {
            $scope.isDataSavingProcess = true;
            $scope.status = status;
            var statusParam = [];
            if ($scope.expenseTrackerModel.list != undefined && $scope.expenseTrackerModel.list != '' && $scope.expenseTrackerModel.list != null)
            {
                for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
                {
                    if ($scope.expenseTrackerModel.list[i].is_select == true)
                    {
                        var savestatusParam = {};
                        savestatusParam.id = $scope.expenseTrackerModel.list[i].id;
                        savestatusParam.status = $scope.status;
                        statusParam.push(savestatusParam);
                    }
                }
            }
            adminService.statusSave(statusParam, $scope.status).then(function(response) {

                if (response.data.success == true)
                {
                    $scope.getList();
//                    $scope.formReset();
//                    $state.go('app.expensetracker');
                }
 //               $scope.formReset();
                $scope.isDataSavingProcess = false;
                $scope.showStatusAddPopup = false;
                $scope.showPaidAddPopup = false;
            });
        };
        $scope.getUserlist = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.isactive = 1;
            return $httpService.get(APP_CONST.API.GET_USER_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUserModel = function(model) {

            if (model !== null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };
        $scope.getList();
    }]);








