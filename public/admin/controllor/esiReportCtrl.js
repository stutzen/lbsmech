app.controller('esiReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.esiModel = {
        list: [],
        length: [],
        total_Amount: 0,
        salary_Amount : 0,
        date: '',
        serverList: null,
        salarytype :'',
        isLoadingProgress: false,
        isSearchLoadingProgress: false
    };
    $scope.showDetail = false;
    $scope.adminService = adminService;
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.validationFactory = ValidationFactory;
    $scope.openDate = function(index) {

        if (index == 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index == 1)
        {
            $scope.toDateOpen = true;
        }

    };

    $scope.searchFilter = {
        fromdate: '',
        todate: ''
        

    };
    // $scope.searchFilter.todate = $scope.currentDate;
        
    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            fromdate: '',
            todate: ''
            

        };
        $scope.esiModel.list = [];
    //  $scope.searchFilter.todate = $scope.currentDate;

    }


    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise !== null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    //        $scope.validateDateFilterData = function()
    //        {
    //            var retVal = false;
    //            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
    //            {
    //                retVal = true;
    //            }
    //            return retVal;
    //        };
    $scope.getList = function() {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'esireport';
        if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
        {
            getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
        }
        if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
        {
            getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
        }

        if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
        {
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);

        }
        if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
        {
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
        }
        $scope.esiModel.isLoadingProgress = true;
        $scope.esiModel.isSearchLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getEsiReport(getListParam, configOption, headers).then(function(response)
        {
            var totalAmt = 0.00;  
            var salaryAmt = 0.00;
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.esiModel.list = data;
                for (var i = 0; i < $scope.esiModel.list.length; i++)
                    {
                        totalAmt = totalAmt + parseFloat($scope.esiModel.list[i].esi_amount);
                        $scope.esiModel.list[i].esi_amount = utilityService.changeCurrency($scope.esiModel.list[i].esi_amount, $rootScope.appConfig.thousand_seperator);
                        salaryAmt = salaryAmt + parseFloat($scope.esiModel.list[i].salary_amount);
                        $scope.esiModel.list[i].salary_amount = utilityService.changeCurrency($scope.esiModel.list[i].salary_amount, $rootScope.appConfig.thousand_seperator);
                    }
                    $scope.esiModel.total_Amount = totalAmt;
                    $scope.esiModel.total_Amount = parseFloat($scope.esiModel.total_Amount).toFixed(2);
                    $scope.esiModel.total_Amount = utilityService.changeCurrency($scope.esiModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.esiModel.salary_Amount = salaryAmt;
                    $scope.esiModel.salary_Amount = parseFloat($scope.esiModel.salary_Amount).toFixed(2);
                    $scope.esiModel.salary_Amount = utilityService.changeCurrency($scope.esiModel.salary_Amount, $rootScope.appConfig.thousand_seperator);
                    
            }
            $scope.esiModel.isLoadingProgress = false;
            $scope.esiModel.isSearchLoadingProgress = false;
        });

    };
    $scope.print = function()
    {
        $window.print();
    };

  // $scope.getList();
}]);







