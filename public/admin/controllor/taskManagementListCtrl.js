





app.controller('taskManagementListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.taskListModel = {
        currentPage: 1,
        total: 0,
        id: '',
        limit: 10,
        list: [],
        typeList: [],
        staffList: [],
        followers: [],
        status: '',
        //             sku: '',

        serverList: null,
        isLoadingProgress: false
    };

    $scope.pagePerCount = [50, 100];
    $scope.taskListModel.limit = $scope.pagePerCount[0];
    $scope.adminService = adminService;
    $scope.searchFilter = {
        low: false,
        medium: false,
        high: false,
        initiated: false,
        inprogress: false,
        completed: false,
        closed: false,
        emp_id: '',
        emp_name: '',
        task_id: '',
        taskname: '',
        typeCount: '',
        assignCount: '',
        from_date: '',
        to_date: '',
        startdate: '',
        duedate: '',
        taskInfo: ''

    };
    $scope.refreshScreen = function ()
    {
        $scope.searchFilter = {
            low: false,
            medium: false,
            high: false,
            initiated: false,
            inprogress: false,
            completed: false,
            closed: false,
            emp_id: '',
            emp_name: '',
            task_id: '',
            taskname: '',
            typeCount: '',
            assignCount: '',
            from_date: '',
            to_date: '',
            startdate: '',
            duedate: '',
            taskInfo: ''

        };
        $scope.initTableFilter();
    }
    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function ()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }
    $scope.selecttasklistId = '';
    $scope.showdeletePopup = false;
    $scope.isdeleteLoadingProgress = false;
    $scope.showPopup = function (id)
    {
        $scope.selecttasklistId = id;
        $scope.showdeletePopup = true;
    };

    $scope.closePopup = function ()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
    };

    $scope.startdateOpen = false;
    $scope.duedateOpen = false;
    //    'dd-MM-yyyy';
    $scope.openDate = function (index)
    {
        if (index == 0)
        {
            $scope.startdateOpen = true;
        } else if (index == 1)
{
            $scope.duedateOpen = true;
        }
    }

    $scope.deleteTaskInfo = function ( )
    {
        if ($scope.isdeleteLoadingProgress == false)
        {
            $scope.isdeleteLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $scope.selecttasklistId;
            var headers = {};
            headers['screen-code'] = 'taskManagement';
            adminService.deleteTaskMgt(getListParam, getListParam.id, headers).then(function (response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }
                $scope.isdeleteLoadingProgress = false;
            });
        }
    };

    $scope.$watch('searchFilter.taskInfo', function (newVal, oldVal)
    {
        if (typeof newVal === 'undefined' || newVal === '')
        {
        //            if ($scope.searchFilter.taskInfo != null && typeof $scope.searchFilter.taskInfo != 'undefined' && typeof $scope.searchFilter.taskInfo.id != 'undefined')
        {
            $scope.initTableFilter();
        }
        }
    });

    $scope.getTaskTypeList = function () {
        var headers = {};
        headers['screen-code'] = 'empTaskType';
        var getListParam = {};
        getListParam.isActive = 1;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getEmployeeTaskTypeList(getListParam, configOption, headers).then(function (response) {
            var data = response.data;
            $scope.taskListModel.typeList = data.list;
            for (var i = 0; i < $scope.taskListModel.typeList.length; i++)
            {
                $scope.taskListModel.typeList[i].isChecked = false;
            }

        });

    };

    $scope.getEmployeeList = function () {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'employee';
        getListParam.is_active = 1;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getEmployeeList(getListParam, configOption, headers).then(function (response) {
            var data = response.data;
            $scope.taskListModel.staffList = data.list;
            for (var i = 0; i < $scope.taskListModel.staffList.length; i++)
            {
                $scope.taskListModel.staffList[i].isChecked = false;
            }
        });

    };

    $scope.setShow = function ($index)
    {
        for (var i = 0; i < $scope.taskListModel.list.length; i++)
        {
            $scope.taskListModel.list[i].isshow = false;
        }
        $scope.taskListModel.list[$index].isshow = true;
    }

    $scope.statusChange = function ($index) {

        $scope.currentTask = '';
        $scope.currentTask = $scope.taskListModel.list[$index];
        var headers = {};
        headers['screen-code'] = 'taskManagement';
        var createTaskActivityParam = {};
        createTaskActivityParam.title = $scope.currentTask.title;
        createTaskActivityParam.task_name = $scope.currentTask.task_name;
        createTaskActivityParam.task_id = $scope.currentTask.task_id;
            
        createTaskActivityParam.emp_id = $scope.currentTask.emp_id;
        createTaskActivityParam.emp_name = $scope.currentTask.emp_name;
        createTaskActivityParam.created_for = $scope.currentTask.created_for;
        createTaskActivityParam.type_id = $scope.currentTask.type_id;
        createTaskActivityParam.type_name = $scope.currentTask.type_name;
        createTaskActivityParam.configType = '';
        if($scope.currentTask.estimated_date != null && $scope.currentTask.estimated_date != '' && $scope.currentTask.estimated_date != undefined)
        {
            
            createTaskActivityParam.estimated_date = $scope.currentTask.estimated_date;
        }
        else
        {
            createTaskActivityParam.estimated_date = null;
        }
        if($scope.currentTask.date != null && $scope.currentTask.date != '' && $scope.currentTask.date != undefined)
        {
            
            createTaskActivityParam.date = $scope.currentTask.date;
        }    
        else
        {
            createTaskActivityParam.date = null;
        }
        createTaskActivityParam.from_time = '';
        createTaskActivityParam.to_time = '';
        createTaskActivityParam.comments = '';
        createTaskActivityParam.status = $scope.currentTask.status;
        if ($scope.currentTask.status == 'New')
        {
            createTaskActivityParam.status = 'InProgress';
            $scope.currentTask.status = 'InProgress';
        } else if ($scope.currentTask.status == 'InProgress')
{
            createTaskActivityParam.status = 'Completed';
            $scope.currentTask.status = 'Completed';
        } else if ($scope.currentTask.status == 'Completed')
{
            createTaskActivityParam.status = 'Closed';
            $scope.currentTask.status = 'Closed';
        }
        createTaskActivityParam.priority = $scope.currentTask.priority;
        createTaskActivityParam.attachments = $scope.currentTask.attachment;
        createTaskActivityParam.followers = $scope.currentTask.followers;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.editTaskMgt(createTaskActivityParam, $scope.currentTask.id, configOption, headers).then(function (response) {
            if (response.data.success == true)
            {

            }

        });
    };


    $scope.getList = function () {

        $scope.taskListModel.isLoadingProgress = true;
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'taskManagement';
        getListParam.is_active = 1;
        getListParam.low = $scope.searchFilter.low == true ? 1 : 0;
        getListParam.medium = $scope.searchFilter.medium == true ? 1 : 0;
        getListParam.high = $scope.searchFilter.high == true ? 1 : 0;
        getListParam.initiated = $scope.searchFilter.initiated == true ? 1 : 0;
        getListParam.inprogress = $scope.searchFilter.inprogress == true ? 1 : 0;
        getListParam.completed = $scope.searchFilter.completed == true ? 1 : 0;
        getListParam.closed = $scope.searchFilter.closed == true ? 1 : 0;
        //            getListParam.emp_id = '';
        //            getListParam.emp_name = '';
        var getArrayParam = {};
        getArrayParam.assign_to = [];
        getArrayParam.type = [];
        //            $scope.typecount = 0;
        //            $scope.checkedTypeList = [];            
        for (var i = 0; i < $scope.taskListModel.typeList.length; i++)
        {
            if ($scope.taskListModel.typeList[i].isChecked == true)
            {
                //                    count = count + 1;
                getArrayParam.type.push($scope.taskListModel.typeList[i].id);
            }
        }
        //            $scope.typecount = count;
        //            $scope.assigntocount = 0;
        //            $scope.checkedAssignList = [];
        //            var count = 0;
        for (var i = 0; i < $scope.taskListModel.staffList.length; i++)
        {
            if ($scope.taskListModel.staffList[i].isChecked == true)
            {
                //                    count = count + 1;
                getArrayParam.assign_to.push($scope.taskListModel.staffList[i].id);
            }

        }
        //            $scope.assigntocount = count;
        //            if ($scope.typecount > 0)
        //            {
        //                getListParam.typeCount = $scope.typecount;
        //                for (var m = 1; m <= $scope.checkedTypeList.length; m++)
        //                {
        //                    $scope.m = m;
        //                    getListParam.type_m = $scope.checkedTypeList[m].id;
        //                }
        //            } else
        //            {
        //                getListParam.typeCount = 0;
        //            }
        //            if ($scope.assigntocount > 0)
        //            {
        //                getListParam.assignCount = $scope.assigntocount;
        //                for (var m = 0; m < $scope.checkedAssignList.length; m++)
        //                {
        //                    getListParam.assigntype_m = $scope.checkedAssignList[m].id;
        //                }
        //            } else
        //            {
        //                getListParam.assignCount = 0;
        //            }
        //            getListParam.task_id = '';
        //            if ($scope.searchFilter.taskInfo != null && typeof $scope.searchFilter.taskInfo != 'undefined' && typeof $scope.searchFilter.taskInfo.id != 'undefined')
        //            {
        //                getListParam.task_id = $scope.searchFilter.taskInfo.id;
        //            } else
        //            {
        //                getListParam.task_id = '';
        //            }
        getListParam.date = '';
        getListParam.estimated_date = '';
        getListParam.task_name = $scope.searchFilter.taskname;
        //            getListParam.from_date = $scope.searchFilter.from_date;
        //            getListParam.to_date = $scope.searchFilter.to_date;
        if ($scope.searchFilter.startdate != null && $scope.searchFilter.startdate != '')
        {
            if (typeof $scope.searchFilter.startdate == 'object')
            {
                getListParam.date = utilityService.parseDateToStr($scope.searchFilter.startdate, $scope.adminService.appConfig.date_format);
            }
            getListParam.date = utilityService.changeDateToSqlFormat(getListParam.date, $scope.adminService.appConfig.date_format);
        }
        if ($scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
        {
            if (typeof $scope.searchFilter.duedate == 'object')
            {
                getListParam.estimated_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
            }
            getListParam.estimated_date = utilityService.changeDateToSqlFormat(getListParam.estimated_date, $scope.adminService.appConfig.date_format);
        }
        getListParam.start = ($scope.taskListModel.currentPage - 1) * $scope.taskListModel.limit;
        getListParam.limit = $scope.taskListModel.limit;
        $scope.taskListModel.isLoadingProgress = true;
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getTaskMgtlist(getArrayParam, configOption, getListParam.is_active, getListParam.low, getListParam.medium, getListParam.high, getListParam.initiated, getListParam.inprogress,
            getListParam.completed, getListParam.closed, getListParam.task_name, getListParam.date, getListParam.estimated_date, getListParam.start,getListParam.limit, headers).then(function (response) {
            var data = response.data.list;
            $scope.taskListModel.list = data;
            for (var i = 0; i < $scope.taskListModel.list.length; i++)
            {
                $scope.taskListModel.list[i].isshow = false;
            }
            $scope.taskListModel.total = data.total;
            $scope.taskListModel.isLoadingProgress = false;
        });

    };

    $scope.getList();
    $scope.getTaskTypeList();
    $scope.getEmployeeList();
}]);




