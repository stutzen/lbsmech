





app.controller('empSalaryListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', 'sweet', '$stateParams', '$q', '$log', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, sweet, $stateParams, $q, $log) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.empSalaryListModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            teamid: '',
            length: [],
            teamList: [],
            taskdetail: [],
            Accountlist: {},

            date: '',
            serverList: null,
            salarytype: '',
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            monthName: '',
            teamName: '',
            totalweeksalary: '0.00',
            totalesi: '0.00',
            totalpfamt: '0.00',
            totaldedamt: '0.00',
            totalot: '0.00',
            totalallowable: '0.00',
            totalAmt: '0.00',
            totalqty: '0.00',
            account: '',
            accountbalance: '',
            salarytotal: 0,
            account_id: ''
        };
        $scope.showDetail = false;
        $scope.adminService = adminService;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.pagePerCount = [50, 100];
        $scope.empSalaryListModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.company_name = $rootScope.appConfig.app_tile;
        $scope.company_address = $rootScope.appConfig.pay_to_address;
        $scope.dt = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;
        $scope.isDataSavingProcess = false;
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            } else if (index == 1)
            {
                $scope.toDateOpen = true;
            } else if (index == 2)
            {
                $scope.fromDateOpen = true;
            } else if (index == 3)
            {
                $scope.toDateOpen = true;
            }

        };
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            teamid: '',
            salarytype: '',
            month: '',
            account: '',
            payroll_type: ''

        };
        // $scope.searchFilter.todate = $scope.currentDate;

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                teamid: '',
                salarytype: '',
                month: '',
                account: '',
                payroll_type: ''

            };
            $scope.showDetail = false;
            $scope.empSalaryListModel.salarytype = '';
            $scope.empSalaryListModel.payroll_type = '';
            $scope.empSalaryListModel.list = [];
            $scope.monthwise = false;
            $scope.weekly = false;
            $scope.daily = false;
            //  $scope.searchFilter.todate = $scope.currentDate;

        }
        $scope.showsalaryPopup = false;
        $scope.showAccount = false;
        $scope.showPopup = function ()
        {
            $scope.showAccount = false;
            $scope.showsalaryPopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showsalaryPopup = false;
            $scope.empSalaryListModel.account = '';
        };
        $scope.showaccountPopup = function ()
        {
            var isDataSelected = 0;
            if ($scope.empSalaryListModel.list.length > 0)
            {
                for (var i = 0; i < $scope.empSalaryListModel.list.length; i++)
                {
                    if ($scope.empSalaryListModel.list[i].isSelected == true)
                    {
                        isDataSelected = isDataSelected + 1;
                    }
                }
            }
            if (isDataSelected == 0)
            {
                sweet.show('Oops...', 'Select Employee Entry', 'error');
                $scope.isDataSavingProcess = false;
            } else
            {
                $scope.showAccount = true;
            }
        };
        $scope.closeaccountPopup = function ()
        {
            $scope.showAccount = false;
            $scope.empSalaryListModel.account = '';
        };

        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        //        $scope.validateDateFilterData = function()
        //        {
        //            var retVal = false;
        //            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
        //            {
        //                retVal = true;
        //            }
        //            return retVal;
        //        };
        $scope.team = function ()
        {
            if ($scope.searchFilter.teamid != null && $scope.searchFilter.teamid != '')
            {
                for (var i = 0; i < $scope.empSalaryListModel.teamList.length; i++)
                {
                    if ($scope.searchFilter.teamid == $scope.empSalaryListModel.teamList[i].id)
                    {
                        $scope.empSalaryListModel.teamName = $scope.empSalaryListModel.teamList[i].name;
                    }
                }
            }
        }

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dateOptions = {
            customClass: getWeekClass,
            showWeeks: false,
            startingDay: 0
        };

        var dateChangeDeferred = $q.defer();
        $scope.changePromise = dateChangeDeferred.promise;

        // Disable weekend selection
        //        function disabled(data) {
        //            var date = data.date,
        //                    mode = data.mode;
        //            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        //        }

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };


        $scope.setDate = function (day, month, year) {
            $scope.dt = new Date(day, month, year);
        };

        $scope.format = $rootScope.appConfig.date_format;

        $scope.popup1 = {
            opened: false
        };
        //$scope.empSalaryListModel.daily = $scope.currentDate;
        $scope.openDate1 = function () {
            $scope.startDateOpen = true;
        }
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getWeekClass(data) {
            var date = data.date,
                    mode = data.mode;
            if (mode === 'day') {
                $log.info($scope.dt == date);
                var old_inicio = moment($scope.dt).startOf('week').toDate();
                var inicio = moment(old_inicio).add(0, 'days').toDate();
                var fin = moment(inicio).add(1, 'week');
                var end = moment(inicio).add(6, 'days');
                if (inicio <= date && fin > date) {
                    $scope.searchFilter.fromdate = $filter('date')(inicio, 'yyyy-MM-dd');
                    if (typeof end == 'object')
                    {
                        end = utilityService.parseDateToStr(end, 'yyyy-MM-dd');
                    }
                    $scope.searchFilter.todate = end;

                    return 'full';
                }
            }
            return '';
        }

        $scope.monthList = [{
                "id": '1',
                "name": 'January'
            }, {
                "id": '2',
                "name": 'February'
            }, {
                "id": '3',
                "name": 'March'
            },
            {
                "id": '4',
                "name": 'April'
            }, {
                "id": '5',
                "name": 'May'
            }, {
                "id": '6',
                "name": 'June'
            }, {
                "id": '7',
                "name": 'July'
            },
            {
                "id": '8',
                "name": 'August'
            }, {
                "id": '9',
                "name": 'September'
            }, {
                "id": '10',
                "name": 'October'
            },
            {
                "id": '11',
                "name": 'November'
            }, {
                "id": '12',
                "name": 'December'
            }]
        $scope.monthwise = false;
        $scope.weekly = false;
        $scope.daily = false;
        $scope.payrolltype = function ()
        {

            if ($scope.searchFilter.payroll_type == 'daily')
            {                
                $scope.monthwise = false;
                $scope.weekly = false;
                $scope.daily = true;
            }
            if ($scope.searchFilter.payroll_type == 'weekly')
            {
                //$scope.searchFilter.month = '';
                $scope.monthwise = false;                
                $scope.daily = false;
                $scope.weekly = true;
            }
            if ($scope.searchFilter.payroll_type == 'monthly')
            {                
                $scope.weekly = false;
                $scope.daily = false;
                $scope.monthwise = true;
            }
            if ($scope.searchFilter.payroll_type == '' || $scope.searchFilter.payroll_type == null)
            {
                $scope.daily = false;
                $scope.monthwise = false;
                $scope.weekly = false;
            }
            $scope.searchFilter.daily = '';
            $scope.searchFilter.month = '';
            $scope.dt = '';
            $scope.searchFilter.fromdate = '';
            $scope.searchFilter.todate = '';
        }
        $scope.check = function ()
        {
            $scope.selectAll = false;
            $scope.empSalaryListModel.salarytotal = 0;

        }
        $scope.selectMonth = function ()
        {
            if ($scope.searchFilter.month != null && $scope.searchFilter.month != '')
            {
                for (var i = 0; i < $scope.monthList.length; i++)
                {
                    if ($scope.searchFilter.month == $scope.monthList[i].id)
                    {
                        $scope.empSalaryListModel.monthName = $scope.monthList[i].name;
                    }
                }
            }
        }

        $scope.selectAll = false;
        $scope.checkAllData = function ()
        {
            for (var i = 0; i < $scope.empSalaryListModel.list.length; i++)
            {
                if ($scope.selectAll == true)
                {
                    $scope.empSalaryListModel.list[i].isSelected = true;
                } else
                {
                    $scope.empSalaryListModel.list[i].isSelected = false;
                }
            }
            $scope.calculateTotal();
        };
        $scope.checkbalance = function (type)
        {
            if ($scope.empSalaryListModel.account != '')
            {
                for (var i = 0; i < $scope.empSalaryListModel.Accountlist.length > 0; i++)
                {
                    if ($scope.empSalaryListModel.account == $scope.empSalaryListModel.Accountlist[i].id)
                    {
                        $scope.empSalaryListModel.accountbalance = $scope.empSalaryListModel.Accountlist[i].balance;
                    }
                }
                var balance = parseFloat($scope.empSalaryListModel.accountbalance);
                var salary_amount = parseFloat($scope.empSalaryListModel.salarytotal);
                if (balance < salary_amount)
                {
                    sweet.show('Oops...', 'Posting amount is greater than balance amount', 'error');
                    $scope.allowPosting = false;
                } else
                {
                    if (type == 'save')
                    {
                        $scope.showPopup();
                    }
                }
            }
        }
        $scope.getAccountlist = function ()
        {
            var getListParam = {};
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.empSalaryListModel.Accountlist = data.list;
            });
        };

        $scope.calculateTotal = function ()
        {
            $scope.empSalaryListModel.salarytotal = 0;
            if ($scope.empSalaryListModel.list.length > 0)
            {
                for (var i = 0; i < $scope.empSalaryListModel.list.length; i++)
                {
                    if ($scope.empSalaryListModel.list[i].isSelected == true)
                    {
                        $scope.empSalaryListModel.salarytotal = parseFloat($scope.empSalaryListModel.salarytotal) + parseFloat($scope.empSalaryListModel.list[i].salay_amount);

                    }
                }
            }

        };

        $scope.saveSalary = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var saveSalaryParam = [];
                var headers = {};
                headers['screen-code'] = 'empSalary';
                for (var i = 0; i < $scope.empSalaryListModel.list.length; i++)
                {
                    if ($scope.empSalaryListModel.list[i].isSelected == true)
                    {
                        var saveSalaryInfo = {};
                        if ($scope.searchFilter.payroll_type == 'daily')
                        {
                            if (typeof $scope.searchFilter.fromdate == 'object')
                            {
                                $scope.searchFilter.daily = utilityService.parseDateToStr($scope.searchFilter.daily, $scope.adminService.appConfig.date_format);
                            }
                            saveSalaryInfo.from_date = utilityService.changeDateToSqlFormat($scope.searchFilter.daily, 'yyyy-MM-dd');
                            if (typeof $scope.searchFilter.todate == 'object')
                            {
                                $scope.searchFilter.daily = utilityService.parseDateToStr($scope.searchFilter.daily, $scope.adminService.appConfig.date_format);
                            }
                            saveSalaryInfo.to_date = utilityService.changeDateToSqlFormat($scope.searchFilter.daily, 'yyyy-MM-dd');
                            saveSalaryInfo.month = '';
                        }

                        if ($scope.searchFilter.payroll_type == 'weekly')
                        {
                            if (typeof $scope.searchFilter.fromdate == 'object')
                            {
                                $scope.searchFilter.fromdate = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                            }
                            saveSalaryInfo.from_date = utilityService.changeDateToSqlFormat($scope.searchFilter.fromdate, 'yyyy-MM-dd');
                            if (typeof $scope.searchFilter.todate == 'object')
                            {
                                $scope.searchFilter.todate = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                            }
                            saveSalaryInfo.to_date = utilityService.changeDateToSqlFormat($scope.searchFilter.todate, 'yyyy-MM-dd');
                            saveSalaryInfo.month = '';
                        }

                        if ($scope.searchFilter.payroll_type == 'monthly')
                        {
                            saveSalaryInfo.month = $scope.searchFilter.month;
                            saveSalaryInfo.from_date = '';
                            saveSalaryInfo.to_date = '';
                        }

                        saveSalaryInfo.emp_id = $scope.empSalaryListModel.list[i].emp_id;
                        saveSalaryInfo.emp_name = $scope.empSalaryListModel.list[i].emp_name;
                        saveSalaryInfo.emp_code = $scope.empSalaryListModel.list[i].emp_code;
                        saveSalaryInfo.emp_team_id = $scope.searchFilter.teamid;
                        saveSalaryInfo.esi_percentage = $scope.empSalaryListModel.list[i].esi_percentage;
                        saveSalaryInfo.adv_deduction_amt = $scope.empSalaryListModel.list[i].adv_deduction_amt;
                        saveSalaryInfo.pf_percentage = $scope.empSalaryListModel.list[i].pf_percentage;
                        saveSalaryInfo.week_salary = $scope.empSalaryListModel.list[i].week_salary;
                        saveSalaryInfo.esi_amount = $scope.empSalaryListModel.list[i].esi_amount;
                        saveSalaryInfo.pf_amount = $scope.empSalaryListModel.list[i].pf_amount;
                        saveSalaryInfo.deduction_amount = $scope.empSalaryListModel.list[i].deduction_amount;
                        saveSalaryInfo.salary_amount = $scope.empSalaryListModel.list[i].salay_amount;
                        saveSalaryInfo.overall_qty = $scope.empSalaryListModel.list[i].overall_qty;
                        saveSalaryInfo.ot_amount = $scope.empSalaryListModel.list[i].ot_amount;
                        saveSalaryInfo.employer_esi_percentage = $scope.empSalaryListModel.list[i].employer_esi_percentage;
                        saveSalaryInfo.employer_pf_percentage = $scope.empSalaryListModel.list[i].employer_pf_percentage;
                        saveSalaryInfo.fixed_allowance_amount = $scope.empSalaryListModel.list[i].fixed_allowance_amount;
                        saveSalaryInfo.ot_detail = $scope.empSalaryListModel.list[i].ot_detail;
                        saveSalaryInfo.task_detail = $scope.empSalaryListModel.list[i].task_detail;
                        saveSalaryInfo.fixed_allowance = $scope.empSalaryListModel.list[i].fixed_allowance;
                        saveSalaryInfo.leave = $scope.empSalaryListModel.list[i].leave;
                        saveSalaryInfo.salary_type = $scope.empSalaryListModel.list[i].salary_type;
                        saveSalaryInfo.payroll_type = $scope.empSalaryListModel.list[i].payroll_type;
                        saveSalaryParam.push(saveSalaryInfo);
                    }

                }

                adminService.saveSalary(saveSalaryParam, $scope.empSalaryListModel.account, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.clearFilters();
                        $state.go('app.empSalaryList');

                    }
                    $scope.isDataSavingProcess = false;

                });
            }
        };


        $scope.getList = function () {
            $scope.selectAll = false;
            $scope.empSalaryListModel.salarytotal = 0;

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empSalary';
            getListParam.month = $scope.searchFilter.month;
            getListParam.payroll_type = $scope.searchFilter.payroll_type;
            getListParam.show_all = 1;
            getListParam.team_id = $scope.searchFilter.teamid;
            //            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            //            {
            //                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            //            }
            //            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            //            {
            //                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            //            }
            //
            //            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            //            {
            //                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            //            }
            //            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            //            {
            //                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            //            }
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            getListParam.salary_type = $scope.searchFilter.salary_type;
            getListParam.payroll_type = $scope.searchFilter.payroll_type;
            $scope.empSalaryListModel.isLoadingProgress = true;
            $scope.empSalaryListModel.isSearchLoadingProgress = true;
            if ($scope.searchFilter.daily != null && $scope.searchFilter.daily != '')
            {
                if ($scope.searchFilter.daily != null && typeof $scope.searchFilter.daily == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.daily, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
                getListParam.to_date = getListParam.from_date;
            }

            if ($scope.searchFilter.daily != null && $scope.searchFilter.daily != '' && $scope.searchFilter.daily != null && $scope.searchFilter.duedate != '')
            {
                if ($scope.searchFilter.daily != null && typeof $scope.searchFilter.daily == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.daily, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
                getListParam.to_date = getListParam.from_date;
            }
            //            if ($scope.searchFilter.dt != null && $scope.searchFilter.dt != '')
            //            {
            //                if ($scope.searchFilter.dt != null && typeof $scope.searchFilter.dt == 'object')
            //                {
            //                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.dt, $scope.adminService.appConfig.date_format);
            //                }
            //                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            //                getListParam.to_date = getListParam.from_date;
            //            }
            //            
            //            if ($scope.searchFilter.dt != null && $scope.searchFilter.dt != '' && $scope.searchFilter.daily != null && $scope.searchFilter.dt != '')
            //            {
            //                if ($scope.searchFilter.dt != null && typeof $scope.searchFilter.dt == 'object')
            //                {
            //                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.daily, $scope.adminService.appConfig.date_format);
            //                }
            //                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            //                getListParam.to_date = getListParam.from_date;
            //            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeSalaryList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.empSalaryListModel.list = data;
                    var weeksalary = 0;
                    var esi = 0;
                    var pf = 0;
                    var dedamt = 0;
                    var ot = 0;
                    var allow = 0;
                    var total = 0;
                    var qty = 0;
                    var final_total = 0;
                    var advance_amt = 0;
                    for (var i = 0; i < $scope.empSalaryListModel.list.length; i++)
                    {
                        qty = qty + parseFloat($scope.empSalaryListModel.list[i].overall_qty);
                        $scope.empSalaryListModel.list[i].isSelected = false;
                        $scope.empSalaryListModel.list[i].salay_amount = parseFloat($scope.empSalaryListModel.list[i].salay_amount).toFixed(2);
                        $scope.empSalaryListModel.list[i].week_salary = parseFloat($scope.empSalaryListModel.list[i].week_salary).toFixed(2);
                        weeksalary = weeksalary + parseFloat($scope.empSalaryListModel.list[i].week_salary);
                        $scope.empSalaryListModel.list[i].esi_amount = parseFloat($scope.empSalaryListModel.list[i].esi_amount).toFixed(2);
                        esi = esi + parseFloat($scope.empSalaryListModel.list[i].esi_amount);
                        advance_amt = advance_amt + parseFloat($scope.empSalaryListModel.list[i].adv_deduction_amt);
                        $scope.empSalaryListModel.list[i].pf_amount = parseFloat($scope.empSalaryListModel.list[i].pf_amount).toFixed(2);
                        pf = pf + parseFloat($scope.empSalaryListModel.list[i].pf_amount);
                        $scope.empSalaryListModel.list[i].deduction_amount = parseFloat($scope.empSalaryListModel.list[i].deduction_amount).toFixed(2);
                        dedamt = dedamt + parseFloat($scope.empSalaryListModel.list[i].deduction_amount);
                        $scope.empSalaryListModel.list[i].ot_amount = parseFloat($scope.empSalaryListModel.list[i].ot_amount).toFixed(2);
                        ot = ot + parseFloat($scope.empSalaryListModel.list[i].ot_amount);
                        $scope.empSalaryListModel.list[i].fixed_allowance_amount = parseFloat($scope.empSalaryListModel.list[i].fixed_allowance_amount).toFixed(2);
                        allow = allow + parseFloat($scope.empSalaryListModel.list[i].fixed_allowance_amount);
                        $scope.empSalaryListModel.list[i].total_amount = parseFloat($scope.empSalaryListModel.list[i].week_salary)
                                - parseFloat($scope.empSalaryListModel.list[i].deduction_amount) + parseFloat($scope.empSalaryListModel.list[i].ot_amount)
                                + parseFloat($scope.empSalaryListModel.list[i].fixed_allowance_amount);
                        $scope.empSalaryListModel.list[i].total_amount = parseFloat($scope.empSalaryListModel.list[i].total_amount).toFixed(2);
                        total = total + parseFloat($scope.empSalaryListModel.list[i].total_amount);
                        final_total = final_total + parseFloat($scope.empSalaryListModel.list[i].salay_amount);
                        $scope.empSalaryListModel.totalweeksalary = parseFloat(weeksalary).toFixed(2);
                        $scope.empSalaryListModel.totalesi = parseFloat(esi).toFixed(2);
                        $scope.empSalaryListModel.totaladv = parseFloat(advance_amt).toFixed(2);
                        $scope.empSalaryListModel.totalpfamt = parseFloat(pf).toFixed(2);
                        $scope.empSalaryListModel.totaldedamt = parseFloat(dedamt).toFixed(2);
                        $scope.empSalaryListModel.totalot = parseFloat(ot).toFixed(2);
                        $scope.empSalaryListModel.totalallowable = parseFloat(allow).toFixed(2);
                        $scope.empSalaryListModel.totalAmt = parseFloat(final_total).toFixed(2);
                        $scope.empSalaryListModel.totalqty = parseFloat(qty).toFixed(2);
                    }

                }
                $scope.empSalaryListModel.isLoadingProgress = false;
                $scope.empSalaryListModel.isSearchLoadingProgress = false;
            });
        };
        $scope.getTeamList = function ()
        {

            var getListParam = {};
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeTeamList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.empSalaryListModel.teamList = data.list;
                }
            });
        };
        //        $scope.getList();
        $scope.getTeamList();
        $scope.getAccountlist();
        $scope.dailyChange = function ()
        {
            $scope.searchFilter.weekdate = '';
            $scope.searchFilter.month = '';
        }
        $scope.weeklyChange = function ()
        {
            $scope.searchFilter.daily = '';
            $scope.searchFilter.month = '';
        }
        $scope.monthlyChange = function ()
        {
            $scope.searchFilter.daily = '';
            $scope.searchFilter.weekdate = '';
        }
    }]);




