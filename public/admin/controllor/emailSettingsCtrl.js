
app.controller('emailSettingsCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', '$stateParams', '$http', '$window', function($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, $stateParams, $http, $window) {

        $scope.emailSettingsModel = {
            "id": 0,
            "date_format": '',
            "isactive": 1,
            "currency": "",
            list: [],
            message: '',
            isLoadingProgress: false
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.email_settings_form != 'undefined' && typeof $scope.email_settings_form.$pristine != 'undefined' && !$scope.email_settings_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {
            if (typeof $scope.email_settings_form != 'undefined')
            {
                $scope.email_settings_form.$setPristine();
                $scope.getEmailSettingsInfo();
            }
        }
       
        $scope.getEmailSettingsInfo = function()
        {
            $scope.emailSettingsModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'emailsettings';
            getListParam.type = 'email';
            getListParam.tplname=$stateParams.id;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmailSettingsList(getListParam,configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.emailSettingsModel.list = data.list;
                    }
                }
                $scope.emailSettingsModel.isLoadingProgress = false;
            });

        };

        $scope.getEmailSettingsInfo( );
        $scope.updateEmailSettings = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var attributeParam = [];
                var headers = {};
                headers['screen-code'] = 'emailsettings';
                for (var i = 0; i < $scope.emailSettingsModel.list.length; i++)
                {
                    var attributeItem = {};
                    attributeItem.id = $scope.emailSettingsModel.list[i].id;
                    attributeItem.tplname = $scope.emailSettingsModel.list[i].tplname;
                    attributeItem.subject = $scope.emailSettingsModel.list[i].subject;
                    attributeItem.message = $scope.emailSettingsModel.list[i].message;
                    attributeParam.push(attributeItem);
                }
                adminService.editEmailSettings(attributeParam, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.emailsettings', {'reload': true});
                    }
                    $scope.isDataSavingProcess = false;
                });

            }
        }
    }]);

