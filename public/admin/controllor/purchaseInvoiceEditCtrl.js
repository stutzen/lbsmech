
app.controller('purchaseInvoiceEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "unit": "",
                    "mrp": "",
                    "sales_price": "",
                    "qty": "",
                    "rowtotal": "",
                    "isActive": "1",
                    "total": 0,
                    "customername": "",
                    "phone": "",
                    "addressId": "",
                    "subtotal": "0.00",
                    "taxAmt": 0,
                    "taxInfo": "",
                    "discountAmt": 0,
                    list: [],
                    invoice_list: [],
                    "billno": "",
                    "billdate": "",
                    "coupon": "",
                    "shipping": 0,
                    "discamount": 0,
                    "code": "",
                    "purchase_no": "",
                    "invoice_no": "",
                    "purchase_code": "",
                    "customerInfo": "",
                    "invoiceDetail": {},
                    "paymenttype": "CASH",
                    "couponcode": "",
                    "invoicePrefix": "",
                    "paymentTerm": "",
                    "taxPercentage": "",
                    "discountPercentage": "",
                    "totalDisPercentage": "0",
                    "totalTaxPercentage": "",
                    "discountMode": "",
                    "discountamount": "",
                    "taxamount": "",
                    "cardDetail": "",
                    "duedate": "",
                    "tax_id": '',
                    "paid_amount": "",
                    "productList": [],
                    "productId": '',
                    "isLoadingProgress": false,
                    "taxAmountDetail": {
                        "tax_amount": ''
                    },
                    "subtotalWithDiscount": '',
                    "prefixList": []
                }
        $scope.adminService = adminService;
        $scope.customAttributeList = [];
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.purchase_order_edit_form != 'undefined' && typeof $scope.purchase_order_edit_form.$pristine != 'undefined' && !$scope.purchase_order_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.purchase_order_edit_form.$setPristine();
            $scope.orderListModel.invoiceDetail = {};
            $scope.orderListModel.list = [];
            $scope.orderListModel.tax_id = "";
            $scope.getInvoiceInfo( );
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function() {

            $scope.orderListModel.user = '';
        }

        $scope.invoiceEditId = $stateParams.id;
        $scope.opened = false;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.dueDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        //$scope.dateOptions = {
          //  formatYear: 'yy',
            //startingDay: 1
       // };
        $scope.currentDate = new Date();

        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.dueDateOpen = true;
            }
        }

        $scope.senderEmail = '';
        $scope.receiver = [];
        $scope.ccEmail = '';
        $scope.showPopup = function(index) {
            $scope.showpopup = true;
            $scope.mailHeader = "Send a Receipt";
            $scope.senderEmail = $scope.orderListModel.customerInfo.email;
            $scope.receiver = [];
            var newRow = {
                "email": $scope.orderListModel.customerInfo.email
            };
            $scope.receiver.push(newRow);
            $scope.ccEmail = $scope.orderListModel.customerInfo.email;

            console.log("Sender Email ", $scope.senderEmail);
            console.log("Sender Email ", $scope.receiverEmail);
            console.log("Sender Email ", $scope.ccEmail);
            $scope.$broadcast("initPopupEvent");
        };

        $scope.addMoreReceiverEmail = function( )
        {
            var newRow = {
                "email": ""
            };
            $scope.receiver.push(newRow);
        }

        $scope.closePopup = function() {
            $scope.showpopup = false;
        }

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;

            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_purchase == '1' || data[i].is_purchase == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        $scope.$watch('orderListModel.customerInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.orderListModel.customerInfo = '';
            }
            else
            {
                var addrss = "";
                if (newVal.billing_address != undefined && newVal.billing_address != '')
                {
                    addrss += newVal.billing_address;
                }
                if (newVal.billing_city != undefined && newVal.billing_city != '')
                {
                    addrss += '\n' + newVal.billing_city;
                }
                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
                {
                    if (newVal.billing_city != undefined && newVal.billing_city != '')
                    {
                        addrss += '-' + newVal.billing_pincode;
                    }
                    else
                    {
                        addrss += newVal.billing_pincode;
                    }
                }

                if (newVal.billing_state != undefined && newVal.billing_state != '')
                {
                    addrss += '\n' + newVal.billing_state;
                }
                if (addrss == '' && newVal.id != undefined && $scope.orderListModel.customerInfo.id != undefined && $scope.orderListModel.customerInfo.id == newVal.id)
                {
                    $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
                }
                else
                {
                    $scope.orderListModel.customerInfo.shopping_address = addrss;
                }
            }
        });

        $scope.getUomList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.isLoadedTax = false;
        $scope.getTaxListInfo = function( )
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaxList(getListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.orderListModel.taxInfo = data;
                $scope.isLoadedTax = true;
            });
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isLoadedTax && $scope.isPrefixListLoaded)
            {
                $scope.updateInvoiceDetails();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateInvoiceDetails = function( )
        {
            $scope.orderListModel.id = $scope.orderListModel.invoiceDetail.id;
            $scope.orderListModel.customerInfo = {};
            $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
            $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_fname;
            $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
            $scope.orderListModel.customerInfo.phone = $scope.orderListModel.invoiceDetail.phone;
            $scope.orderListModel.customerInfo.email = $scope.orderListModel.invoiceDetail.email;
            $scope.orderListModel.subtotal = $scope.orderListModel.invoiceDetail.subtotal;
            $scope.orderListModel.taxAmt = $scope.orderListModel.invoiceDetail.tax_amount;
            $scope.orderListModel.taxAmountDetail.tax_amount = $scope.orderListModel.invoiceDetail.tax_amount;
            $scope.orderListModel.discountAmt = $scope.orderListModel.invoiceDetail.discount_amount;
            $scope.orderListModel.total = $scope.orderListModel.invoiceDetail.total_amount;
            $scope.orderListModel.paid_amount = $scope.orderListModel.invoiceDetail.paid_amount;
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.orderListModel.invoicePrefix = $scope.orderListModel.invoiceDetail.prefix;
           var idate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date,$scope.adminService.appConfig.date_format);
            $scope.orderListModel.billdate = idate;
            var duedate = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.duedate,$scope.adminService.appConfig.date_format);
            $scope.orderListModel.duedate = duedate;
            $scope.orderListModel.discountPercentage = $scope.orderListModel.invoiceDetail.discount_percentage + '';
            $scope.orderListModel.discountMode = $scope.orderListModel.invoiceDetail.discount_mode;
            $scope.orderListModel.tax_id = 'invoiced_tax_rate';
            //$scope.orderListModel.purchase_code = $scope.orderListModel.invoiceDetail.purchase_code;
            $scope.orderListModel.purchase_no = $scope.orderListModel.invoiceDetail.purchase_no;
            $scope.orderListModel.invoice_no = $scope.orderListModel.invoiceDetail.reference_no;
            $scope.customAttributeList = $scope.orderListModel.invoiceDetail.customattribute;
            var loop;

            for (loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
            {
                if ($scope.orderListModel.taxInfo[loop].id = $scope.orderListModel.invoiceDetail.tax_id)
                {
                    $scope.orderListModel.taxPercentage = $scope.orderListModel.invoiceDetail.tax_percentage;
                    break;
                }
            }

            for (loop = 0; loop < $scope.orderListModel.invoiceDetail.item.length; loop++)
            {
                var newRow =
                        {
                            "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                            "internal_productInfo": {
                                id : $scope.orderListModel.invoiceDetail.item[loop].internal_product_id,
                                name : $scope.orderListModel.invoiceDetail.item[loop].internal_product_name,
                            },
                            "name": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                            "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                            "sales_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                            "qty": $scope.orderListModel.invoiceDetail.item[loop].qty,
                            "rowtotal": $scope.orderListModel.invoiceDetail.item[loop].total_price,
                            "uom": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                            "uom_id": $scope.orderListModel.invoiceDetail.item[loop].uom_id,
                            "custom_opt1": $scope.orderListModel.invoiceDetail.item[loop].custom_opt1,
                            "custom_opt2": $scope.orderListModel.invoiceDetail.item[loop].custom_opt2,
                            "custom_opt3": $scope.orderListModel.invoiceDetail.item[loop].custom_opt3,
                            "custom_opt4": $scope.orderListModel.invoiceDetail.item[loop].custom_opt4,
                            "custom_opt5": $scope.orderListModel.invoiceDetail.item[loop].custom_opt5
                        };
                $scope.orderListModel.list.push(newRow);
                $scope.orderListModel.list[loop].productname = $scope.orderListModel.list[loop];
            }
            $scope.orderListModel.isLoadingProgress = false;
        }

        $scope.getProductList = function()
        {

            var getParam = {};

            getParam.is_active = 1;
            getParam.is_purchase = 1;
            getParam.localData = 1;
            if (typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
            {
                getParam.scopeId = $scope.selectedAssignFrom.id;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductList(getParam, configOption).then(function(response) {
                var data = response.data;
                $scope.orderListModel.productList = data.list;

            });
        }

        $scope.formatProductModel = function(model, index)
        {
            if (model != null)
            {
                $scope.updateProductInfo(model, index);
                return model.name;
            }
            return  '';
        }
         $scope.formatInternalProductModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        }

        $scope.updateProductInfo = function(model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].productname.name = model.name;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].sales_price = model.sales_price;
                if (model.qty != null && model.qty != '')
                {
                    $scope.orderListModel.list[index].qty = model.qty;
                }
                else
                {
                    $scope.orderListModel.list[index].qty = 1;
                }
                $scope.orderListModel.list[index].rowtotal = model.sales_price;
                $scope.orderListModel.list[index].discountPercentage = model.discountPercentage;
                $scope.orderListModel.list[index].taxPercentage = model.taxPercentage;
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                }
                $scope.orderListModel.list[index].uom_id = model.uom_id;
                $scope.updateInvoiceTotal();

                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
                $scope.createNewProduct(index)
                return;
            }
            else
            {
                //            var newRow = {
                //                "id": '',
                //                "productName": '',
                //                "sku": '',
                //                "sales_price": '',
                //                "qty": '',
                //                "rowtotal": '',
                //                "discountPercentage": '',
                //                "taxPercentage": '',
                //                "uom": '',
                //                "uom_id": ''
                //            }
                //            $scope.orderListModel.list.push(newRow);                        
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.initValidateProductDataAndAddNew = function(index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function() {
                        $scope.createNewProduct(index);
                    },
                    300);
        };

        $scope.formValidator = function()
        {
            //performace issue in form validator bug
            if ($scope.purchase_order_edit_form.$submitted)
            {
                $timeout(function() {
                    $scope.purchase_order_edit_form.$submitted = false;
                }, 0);
                $timeout(function() {
                    $scope.purchase_order_edit_form.$setSubmitted();
                }, 100);
            }
            else
            {
                $timeout(function() {
                    $scope.purchase_order_edit_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.createNewProduct = function(index)
        {
            var formDataError = false;
            if (index != -1)
            {
                var productContainer = window.document.getElementById('purchaseinvoice_edit_product_container');
                var errorCell = angular.element(productContainer).find('.has-error').length;
                if (errorCell > 0)
                {
                    formDataError = true;
                }
            }
            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1 && !$scope.deleteEvent)
                {

                    var newRow = {
                        "id": '',
                        "productName": '',
                        "sku": '',
                        "sales_price": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "taxPercentage": '',
                        "uom": '',
                        "uom_id": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": ''
                    }
                    $scope.orderListModel.list.push(newRow);
                }
                $scope.deleteEvent = false;
            }

        }
        $scope.addNewProduct = function()
        {
            var newRow = {
                "id": '',
                "productName": '',
                "sku": '',
                "sales_price": '',
                "qty": '',
                "rowtotal": '',
                "discountPercentage": '',
                "taxPercentage": '',
                "uom": '',
                "uomid": '',
                "custom_opt1": '',
                "custom_opt2": '',
                "custom_opt3": '',
                "custom_opt4": '',
                "custom_opt5": ''
            }
            $scope.orderListModel.list.push(newRow);
        }
        $scope.deleteEvent = false;
        $scope.deleteProduct = function(id)
        {
            var index = -1;
            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
                if (id == $scope.orderListModel.list[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.calculatetotal();
            $scope.calculateTaxAmt();
            $scope.deleteEvent = true;
        };

        $scope.taxList = [];
        $scope.calculatetotal = function()
        {
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            console.log("bfr loop", $scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                if ($scope.orderListModel.list[i].taxPercentage == null)
                {
                    $scope.orderListModel.list[i].taxPercentage = 0;
                }
                if ($scope.orderListModel.list[i].discountPercentage == null)
                {
                    $scope.orderListModel.list[i].discountPercentage = 0;
                }
            }
            $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.taxPercentage = '';
            for (var loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
            {
                if ($scope.orderListModel.tax_id == $scope.orderListModel.taxInfo[loop].id)
                {
                    $scope.orderListModel.taxPercentage = $scope.orderListModel.taxInfo[loop].tax_percentage;
                }
            }
            if ($scope.orderListModel.taxPercentage == '' || isNaN($scope.orderListModel.taxPercentage))
            {
                totTaxPercentage = 0;
            }
            totTaxPercentage = parseFloat($scope.orderListModel.taxPercentage);
            if ($scope.orderListModel.discountPercentage == '' || isNaN($scope.orderListModel.discountPercentage))
            {
                totDiscountPercentage = 0;
            }
            totDiscountPercentage = parseFloat($scope.orderListModel.discountPercentage);

            if (totDiscountPercentage > 0)
            {
                if ($scope.orderListModel.discountMode == '%')
                {

                    $scope.orderListModel.discountAmt = parseFloat((subTotal) * (totDiscountPercentage / 100)).toFixed(2);
                }
                else if ($scope.orderListModel.discountMode == 'amount')
                {
                    $scope.orderListModel.discountAmt = parseFloat(totDiscountPercentage).toFixed(2);
                }
            }
            else
            {
                $scope.orderListModel.discountAmt = '0.00';
            }
            if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
            {
                $scope.orderListModel.taxAmt = $scope.orderListModel.taxAmountDetail.tax_amount;
            }
            else if (totTaxPercentage > 0)
            {
                $scope.orderListModel.taxAmt = parseFloat((subTotal - $scope.orderListModel.discountAmt) * (totTaxPercentage / 100)).toFixed(2);
            }
            else
            {
                $scope.orderListModel.taxAmt = parseFloat(0).toFixed(2);
            }
            if ($scope.orderListModel.taxAmt == '' || isNaN($scope.orderListModel.taxAmt))
            {
                $scope.orderListModel.taxAmt = 0.00;
            }
            if ($scope.orderListModel.subtotal > 0)
            {
                $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                $scope.orderListModel.total = (parseFloat(subTotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
            }
            else
            {
                $scope.orderListModel.subtotalWithDiscount = 0.00;
                $scope.orderListModel.total = 0.00;
            }
            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
            $scope.updateRoundoff();
        }
        $scope.updateRoundoff = function()
        {
            $scope.orderListModel.round = Math.round($scope.orderListModel.total);
            $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
            $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
            $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
        }
        $scope.orderListModel.totalTax = 0;
        for (i = 0; i < $scope.taxList.length; i++)
        {
            $scope.orderListModel.totalTax += $scope.taxList[i].taxPercentage;
        }
        $scope.orderListModel.totalTaxPercentage = $scope.orderListModel.totalTax;

        $scope.$watch('orderListModel.subtotalWithDiscount', function(newVal, oldVal)
        {
            if (typeof newVal != 'undefined' && newVal != '' && newVal != null)
            {
                $scope.calculateTaxAmt(false);
            }
        });

        $scope.calculateTaxAmt = function(forceSave)
        {
            if (($scope.orderListModel.subtotalWithDiscount != 0 && $scope.orderListModel.subtotalWithDiscount != '' && $scope.orderListModel.subtotalWithDiscount >= 0) &&
                    ($scope.orderListModel.total != 0 && $scope.orderListModel.total != '' && $scope.orderListModel.total >= 0))
            {
                var getListParam = {};
                getListParam.amount = $scope.orderListModel.subtotalWithDiscount;
                getListParam.tax_id = $scope.orderListModel.tax_id;
                getListParam.invoice_id = $scope.orderListModel.id;
                getListParam.invoice_type = 'purchase';

                adminService.calculateTaxAmt(getListParam).then(function(response)
                {
                    var data = response.data;
                    $scope.orderListModel.taxAmountDetail = data;
                    $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmountDetail.tax_amount).toFixed(2);
                    $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                    $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                    $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                    $scope.updateRoundoff();

                    if (forceSave)
                    {
                        $scope.updateOrder(forceSave);
                    }
                    console.log('taxamtdetail');
                    console.log($scope.orderListModel.taxAmountDetail);
                });

            }
            else
            {
                $scope.orderListModel.taxAmountDetail = '';
                $scope.orderListModel.taxAmt = parseFloat(0).toFixed(2);
                if ($scope.orderListModel.subtotal > 0)
                {
                    $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                    $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                    $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                    $scope.updateRoundoff();
                }


                if (forceSave)
                {
                    $scope.updateOrder(forceSave);
                }
                console.log('taxamtdetail');
                console.log($scope.orderListModel.taxAmountDetail);
            }
        }
        $scope.$on("updateSelectedProductEvent", function(event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }
                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                    $scope.updateInvoiceTotal();
                }
                else
                {
                    var newRow =
                            {
                                "id": selectedItems.id,
                                "productName": selectedItems.name,
                                "sku": selectedItems.sku,
                                "sales_price": selectedItems.sales_price,
                                "qty": 1,
                                "rowtotal": selectedItems.sales_price,
                                "discountPercentage": selectedItems.discountPercentage,
                                "taxPercentage": selectedItems.taxPercentage,
                                "uom": selectedItems.uom,
                                "uom_id": selectedItems.uom_id
                            }
                    $scope.orderListModel.list.push(newRow);
                    console.log("upd 2");
                    $scope.updateInvoiceTotal();
                    return;
                }
            }
            else
            {
                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "sales_price": selectedItems.sales_price,
                    "qty": 1,
                    "rowtotal": selectedItems.sales_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom,
                    "uom_id": selectedItems.uom_id
                }
                $scope.orderListModel.list.push(newRow);
                console.log("upd 1");
                $scope.updateInvoiceTotal();
            }
        });


        $scope.getInvoiceInfo = function( )
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getPurchaseInvoiceDetail(getListParam, configOption).then(function(response)
            {
                var data = response.data.data;
                $scope.orderListModel.invoiceDetail = data;
                $scope.initUpdateDetail();
                $scope.getTaxListInfo( );
            });
        }

        $scope.$on('customSelectOptionChangeEvent', function(event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });

        $scope.updateCustomSelectValue = function(data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.$on('customfieldValueChangeEvent', function(event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });

        $scope.updateCustomFieldValue = function(data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }


        $scope.getInvoiceInfo( );
        $scope.getProductList( );
        $scope.updateProductInfo();
        $scope.updateOrder = function(forceSave)
        {
            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                if (i != $scope.orderListModel.list.length - 1)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != null &&
                                $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                        {
                            $scope.isSave = true;
                        }
                        else
                        {
                            $scope.isSave = false;
                        }
                    }
                    else
                    {
                        if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                        {
                            $scope.isSave = true;
                        }
                        else
                        {
                            $scope.isSave = false;
                        }
                    }
                }
            }

            if ($scope.isSave)
            {
                if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
                {
                    if ($scope.orderListModel.taxAmountDetail.tax_amount == $scope.orderListModel.taxAmt)
                    {
                        //$scope.modifyOrder();
                        $scope.getInvoiceNoListInfo(false);
                    }
                    else
                    {
//                            $scope.orderListModel.taxAmt = $scope.orderListModel.taxAmountDetail.tax_amount;
//                            $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
//                            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
//                            $scope.updateRoundoff();
//                            $scope.modifyOrder();
                        $scope.calculateTaxAmt(true);
                    }
                }
                else
                {
                    $scope.calculateTaxAmt(true);
                }
            }
            else
            {
                sweet.show('Oops...', 'Fill Product Detail.. ', 'error');
                $scope.isDataSavingProcess = false;
            }

        };

              $scope.isPrefixListLoaded = false;
        $scope.getPrefixList = function()
        {
            var getListParam = {};
            getListParam.setting = 'purchase_invoice_prefix';
            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var perfix = data.list[0].value;
                        perfix = perfix.split('\n');
                        for (var i = 0; i < perfix.length; i++)
                        {
                            $scope.orderListModel.prefixList.push(perfix[i]);
                        }
                    }
                    $scope.isPrefixListLoaded = true;

                }
                
              //  $scope.initUpdateDetail();
            });

        };

        $scope.isNextAutoGeneratorNumberLoaded = false;
        $scope.getNextInvoiceno = function()
        {
            $scope.isNextAutoGeneratorNumberLoaded = true;
            var getListParam = {};
            getListParam.type = "purchase_invoice";
            getListParam.prefix = $scope.orderListModel.invoicePrefix;

            var headers = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            configOption.perfix = getListParam.prefix;
            adminService.getNextInvoiceNo(getListParam, configOption, headers).then(function(response)
            {
                var config = response.config.config;
                if (response.data.success == true && typeof config.perfix != 'undefined' && config.perfix == $scope.orderListModel.invoicePrefix)
                {
                    if (!$scope.orderListModel.is_next_autogenerator_num_changed)
                    {
                        $scope.orderListModel.purchase_no = response.data.no;
                    }
                }
                $scope.isNextAutoGeneratorNumberLoaded = false;

            });

        };

        $scope.modifyOrder = function()
        {
            var updateOrderParam = {};
            var headers = {};
            headers['screen-code'] = 'purchaseinvoice';
            //updateOrderParam.status = 'unpaid';                
            updateOrderParam.prefix = $scope.orderListModel.invoicePrefix;
            updateOrderParam.purchase_no = $scope.orderListModel.purchase_no;
            updateOrderParam.reference_no = $scope.orderListModel.invoice_no;
            updateOrderParam.purchase_code = $scope.orderListModel.purchase_code;
            if (typeof $scope.orderListModel.billdate == 'object')
            {
                $scope.orderListModel.billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, "yyyy-MM-dd");
            }
            updateOrderParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, "yyyy-MM-dd");
            if (typeof $scope.orderListModel.duedate == 'object')
            {
                $scope.orderListModel.duedate = utilityService.parseDateToStr($scope.orderListModel.duedate, "yyyy-MM-dd");
            }
            updateOrderParam.duedate = utilityService.changeDateToSqlFormat($scope.orderListModel.duedate, "yyyy-MM-dd");
            updateOrderParam.customer_address = $scope.orderListModel.customerInfo.shopping_address;
            updateOrderParam.subtotal = $scope.orderListModel.subtotal;
            if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
            {
                updateOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmountDetail.tax_amount)).toFixed(2);
            }
            else
            {
                updateOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
            }
            updateOrderParam.discount_amount = $scope.orderListModel.discountAmt;
            updateOrderParam.discount_percentage = $scope.orderListModel.discountPercentage;
            updateOrderParam.discount_mode = $scope.orderListModel.discountMode;
            updateOrderParam.total_amount = $scope.orderListModel.total;
            updateOrderParam.round_off = (parseFloat($scope.orderListModel.roundoff)).toFixed(2);
            updateOrderParam.tax_id = $scope.orderListModel.tax_id;

            updateOrderParam.paymentmethod = "",
                    updateOrderParam.notes = "",
                    updateOrderParam.item = [];
            updateOrderParam.item.date = utilityService.parseDateToStr($scope.orderListModel.billdate, "yyyy-MM-dd");
            updateOrderParam.item.outletId = 1;
            updateOrderParam.customer_id = $scope.orderListModel.customerInfo.id;
            updateOrderParam.item.orderDetails = [];
            for (var i = 0; i < $scope.orderListModel.list.length - 1; i++)
            {
                //if ($scope.orderListModel.list[i].sku != '')
                {
                    var ordereditems = {};
                    ordereditems.product_id = $scope.orderListModel.list[i].id;
                    if($scope.orderListModel.list[i].internal_productInfo != '' && $scope.orderListModel.list[i].internal_productInfo != null && $scope.orderListModel.list[i].internal_productInfo != undefined && $scope.orderListModel.list[i].internal_productInfo.id != undefined)
                    {    
                       ordereditems.internal_product_id = $scope.orderListModel.list[i].internal_productInfo.id;
                    }
                    else
                    {
                        ordereditems.internal_product_id = '';
                    }    
                    ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                    if (typeof $scope.orderListModel.list[i].productname != undefined && $scope.orderListModel.list[i].productname != null && $scope.orderListModel.list[i].productname != '')
                    {
                        if ($scope.orderListModel.list[i].productname.name != null && $scope.orderListModel.list[i].productname.name != '')
                        {
                            ordereditems.product_name = $scope.orderListModel.list[i].productname.name;
                        }
                        else
                        {
                            ordereditems.product_name = $scope.orderListModel.list[i].productname;
                        }
                    }
                    ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].sales_price);
                    ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty);
                    if (typeof $scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo != null && $scope.orderListModel.list[i].uomInfo != '')
                    {
                        if ($scope.orderListModel.list[i].uomInfo.name != null && $scope.orderListModel.list[i].uomInfo.name != '')
                        {
                            ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                        }
//                            else
//                            {
//                                ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo;
//                            }
                    }
                    ordereditems.uom_id = $scope.orderListModel.list[i].uom_id;
                    ordereditems.custom_opt1 = '';
                    ordereditems.custom_opt2 = '';
                    ordereditems.custom_opt3 = '';
                    ordereditems.custom_opt4 = '';
                    ordereditems.custom_opt5 = '';
                    if ($scope.orderListModel.list[i].custom_opt1 != '' && $scope.orderListModel.list[i].custom_opt1 != null)
                    {
                        ordereditems.custom_opt1 = $scope.orderListModel.list[i].custom_opt1;
                    }
                    if ($scope.orderListModel.list[i].custom_opt2 != '' && $scope.orderListModel.list[i].custom_opt2 != null)
                    {
                        ordereditems.custom_opt2 = $scope.orderListModel.list[i].custom_opt2;
                    }
                    if ($scope.orderListModel.list[i].custom_opt3 != '' && $scope.orderListModel.list[i].custom_opt3 != null)
                    {
                        ordereditems.custom_opt3 = $scope.orderListModel.list[i].custom_opt3;
                    }
                    if ($scope.orderListModel.list[i].custom_opt4 != '' && $scope.orderListModel.list[i].custom_opt4 != null)
                    {
                        ordereditems.custom_opt4 = $scope.orderListModel.list[i].custom_opt4;
                    }
                    if ($scope.orderListModel.list[i].custom_opt5 != '' && $scope.orderListModel.list[i].custom_opt5 != null)
                    {
                        ordereditems.custom_opt5 = $scope.orderListModel.list[i].custom_opt5;
                    }
                    ordereditems.total_price = $scope.orderListModel.list[i].sales_price * ordereditems.qty;
                    updateOrderParam.item.push(ordereditems);
                }
            }
            updateOrderParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var customattributeItem = {};

                if ($scope.customAttributeList[i].value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].value;
                }
                else if ($scope.customAttributeList[i].default_value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].default_value;
                }
                else
                {
                    customattributeItem.value = "";
                }

                customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                updateOrderParam.customattribute.push(customattributeItem);
            }
            adminService.editPurchaseInvoice(updateOrderParam, $stateParams.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.isDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.purchaseInvoice');
                }
                else
                {
                    $scope.isDataSavingProcess = false;
                }
            });
        }
        $scope.isInvoiceExists = false;
        $scope.getInvoiceNoListInfo = function(fromKey)
        {
            $scope.isInvoiceExists = false;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'purchaseinvoice';
            getListParam.customer_id = $scope.orderListModel.customerInfo.id;
            getListParam.reference_no = $scope.orderListModel.invoice_no;
            getListParam.purchase_invoice_id = $stateParams.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getPurchaseInvoiceNumber(getListParam, headers, configOption).then(function(response)
            {
                if (response.data.success === false)
                {
                    sweet.show("Oops", "Invoice number already exist", "error");
                    $scope.isInvoiceExists = true;
                    $scope.isDataSavingProcess = false;
                }
                else
                {
                    if (fromKey == false && $scope.isInvoiceExists == false)
                    {
                        $scope.modifyOrder();
                    }

                }

            });
        }
        $scope.event = null;
        $scope.keyHandler = function(event)
        {
            var fromKey = true;
            console.log("inside");
            if (event.keyCode == 40)
            {
                if ($scope.orderListModel.customerInfo != undefined && $scope.orderListModel.customerInfo != null && $scope.orderListModel.customerInfo != '' && $scope.orderListModel.customerInfo.id != undefined)
                {
                    if ($scope.orderListModel.invoice_no != undefined && $scope.orderListModel.invoice_no != null && $scope.orderListModel.invoice_no != '')
                    {
                        $scope.getInvoiceNoListInfo(fromKey);
                    }
                    else
                    {
                        sweet.show("Oops", "Please enter invoice number", "error");
                    }
                }
                else
                {
                    sweet.show("Oops", "Please select Customer", "error");
                }
            }
        }
        $scope.getPrefixList();
    }]);