app.controller('groupListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.groupModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            serverList: null,
            isLoadingProgress: true
        };

        $scope.searchFilterNameValue = ''
        $scope.pagePerCount = [50, 100];
        $scope.groupModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name:''
           
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getGroupListInfo, 300);
        }

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
               name:''
            };
            $scope.initTableFilter();
        }

        $scope.selectGroupId = '';
        $scope.showdeletePopup = false;
         $scope.isdeleteProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectGroupId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
             $scope.isdeleteProgress = false;
        };
        $scope.deleteGroupInfo = function( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectGroupId;
                var headers = {};
                headers['screen-code'] = 'customergroup';
                adminService.deleteCustomerGroup(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getGroupListInfo();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };
        $scope.getGroupListInfo = function() {

            var GroupListParam = {};
            var headers = {};
            headers['screen-code'] = 'customergroup';
            GroupListParam.name= $scope.searchFilter.name;
            GroupListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCustomerGroupList(GroupListParam,configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.groupModel.list = data.list;
                    $scope.groupModel.total = data.total;
                }
                $scope.groupModel.isLoadingProgress = false;
            });

        };

        $scope.getGroupListInfo();

    }]);




