

app.controller('companyListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $filter, Auth, $timeout) {

            $rootScope.getNavigationBlockMsg = null;
            $scope.companyModel = {

                  currentPage: 1,
                  total: 0,
                  limit: 4,
                  companyList: [],

                  isLoadingProgress: true

            };
            $scope.pagePerCount = [50, 100];
            $scope.companyModel.limit = $scope.pagePerCount[0];
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
            $scope.searchFilter = {
                  name: '',
                  phone: '',
                  email: '',
                  country_name: '',
                  state_name: '',
                  city_name: '',
                  country_id: '',
                  state_id: '',
                  city_id: ''
            };
            $scope.refreshScreen = function ()
            {
                  $scope.searchFilter = {
                        name: '',
                        phone: '',
                        email: '',
                        country_name: '',
                        state_name: '',
                        city_name: '',
                        country_id: '',
                        state_id: '',
                        city_id: ''
                  };
                  $scope.initTableFilter();
            }
            $scope.initTableFilterTimeoutPromise = null;

            $scope.initTableFilter = function ()
            {
                  if ($scope.initTableFilterTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.initTableFilterTimeoutPromise);
                  }
                  $scope.initTableFilterTimeoutPromise = $timeout($scope.getCompanyList, 300);
            }
            $scope.showPopup = function (id)
            {
                  $scope.selectCompanyId = id;
                  $scope.showdeletePopup = true;
            };

            $scope.closePopup = function ()
            {
                  $scope.showdeletePopup = false;
                  $scope.isdeleteProgress = false;
            };

            $scope.deleteCompanyInfo = function ()
            {
                  if ($scope.isdeleteProgress == false)
                  {
                        $scope.isdeleteProgress = true;

                        var getListParam = {};
                        getListParam.id = $scope.selectCompanyId;
                        var headers = {};
                        headers['screen-code'] = 'company';
                        adminService.deleteCompany(getListParam, getListParam.id, headers).then(function (response)
                        {
                              var data = response.data;
                              if (data.success == true)
                              {
                                    $scope.closePopup();
                                    $scope.getCompanyList();
                              }
                              $scope.isdeleteProgress = false;
                        });
                  }
            };
            $scope.getCompanyList = function () {

                  $scope.companyModel.isLoadingProgress = true;
                  var companyListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'company';
                  companyListParam.is_active = 1;
                  companyListParam.name = $scope.searchFilter.name;
                  companyListParam.country_id = $scope.searchFilter.country_id;
                  companyListParam.state_id = $scope.searchFilter.state_id;
                  companyListParam.city_id = $scope.searchFilter.city_id;
                  companyListParam.start = ($scope.companyModel.currentPage - 1) * $scope.companyModel.limit;
                  companyListParam.limit = $scope.companyModel.limit;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getCompanyList(companyListParam, configOption, headers).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              $scope.companyModel.companyList = data.list;
                              if ($scope.companyModel.companyList.length > 0)
                                {
                                    for (var i = 0; i < $scope.companyModel.companyList.length; i++)
                                    {
                                        $scope.companyModel.companyList[i].index = companyListParam.start + i + 1;
                                    }
                                }
                              $scope.companyModel.total = data.total;
                        }
                        $scope.companyModel.isLoadingProgress = false;

                  });
            };

            $scope.getcountryList = function () {

                  $scope.companyModel.isLoadingProgress = true;
                  var countryListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'country';

                  countryListParam.id = '';

                  countryListParam.is_active = 1;

                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getCountryList(countryListParam, configOption, headers).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              $scope.companyModel.countryList = data.list;
                        }
                        $scope.companyModel.isLoadingProgress = false;
                  });
            };
            $scope.getcountryList();

            $scope.getstateList = function () {

                  $scope.companyModel.isLoadingProgress = true;
                  var stateListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'state';

                  stateListParam.id = '';

                  stateListParam.is_active = 1;
//            stateListParam.country_id = $scope.companyModel.companyList.country_id;
//            stateListParam.country_name = $scope.companyModel.companyList.country_name;
                  for (var i = 0; i < $scope.companyModel.countryList.length; i++)
                  {
                        if ($scope.searchFilter.country_id == $scope.companyModel.countryList[i].id)
                        {
                              stateListParam.country_id = $scope.companyModel.countryList[i].id;
                        }
                  }
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getStateList(stateListParam, configOption, headers).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              $scope.companyModel.stateList = data.list;
                        }
                        $scope.companyModel.isLoadingProgress = false;
                  });

            };

            $scope.getcityList = function () {

                  $scope.companyModel.isLoadingProgress = true;
                  var cityListParam = {};
                  var headers = {};
                  headers['screen-code'] = 'city';

                  cityListParam.id = '';

                  cityListParam.is_active = 1;
//            cityListParam.country_id = $scope.companyModel.coutryList.country_id;
//            cityListParam.country_name = $scope.companyModel.countryList.country_name;
//            cityListParam.state_id = $scope.companyModel.stateList.state_id;
                  cityListParam.start = 0;
                  cityListParam.limit = 0;
                  for (var i = 0; i < $scope.companyModel.countryList.length; i++)
                  {
                        if ($scope.searchFilter.country_id == $scope.companyModel.countryList[i].id)
                        {
                              cityListParam.country_id = $scope.companyModel.countryList[i].id;
                        }
                  }
                  for (var i = 0; i < $scope.companyModel.stateList.length; i++)
                  {
                        if ($scope.searchFilter.state_id == $scope.companyModel.stateList[i].id)
                        {
                              cityListParam.state_id = $scope.companyModel.stateList[i].id;
                        }
                  }
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getCityList(cityListParam, configOption, headers).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              $scope.companyModel.cityList = data.list;
                        }
                        $scope.companyModel.isLoadingProgress = false;
                  });

            };

            $scope.CountryChange = function ()
            {
                  $scope.companyModel.state_id = '';
                  $scope.companyModel.city_id = '';
                  $scope.initTableFilter();
                  $scope.getstateList();

            }
            $scope.StateChange = function ()
            {
                  $scope.companyModel.city_id = '';
                  $scope.initTableFilter();
                  $scope.getcityList();

            }
            $scope.getCompanyList();
      }]);






