

app.controller('userSaveCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $scope.userAddModel = {
            userName: '',
            f_name: '',
            l_name: '',
            email: '',
            city: '',
            country: '',
            Id: '',
            state: '',
            gender: '',
            dob: '',
            user_type: '',
            address: '',
            post_code: '',
            password: '',
            repassword: '',
            mobile: '',
            showPassword: false
        };

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.user_form != 'undefined' && typeof $scope.user_form.$pristine != 'undefined' && !$scope.user_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.user_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;


        $scope.userRoleList = ['', 'ADMIN', 'OWNER', 'WORKFORCE'];


        $scope.createUser = function() {

            $scope.isDataSavingProcess = true;
            var createClientParam = {};

            createClientParam.Id = 0;
            createClientParam.f_name = $scope.userAddModel.Fname;
            createClientParam.l_name = $scope.userAddModel.Lname;
            createClientParam.userName = $scope.userAddModel.username;
            createClientParam.password = $scope.userAddModel.password;
            createClientParam.repassword = $scope.userAddModel.repassword;
            createClientParam.user_type = $scope.userAddModel.role;
            createClientParam.mobile = $scope.userAddModel.mobile;
            createClientParam.email = $scope.userAddModel.email;
            createClientParam.city = $scope.userAddModel.city;
            createClientParam.country = $scope.userAddModel.country;
            //createClientParam.state = $scope.userAddModel.state;
            //createClientParam.address = $scope.userAddModel.address;
            //createClientParam.dob = $scope.userAddModel.dob;
            createClientParam.isActive = 1;


            adminService.createUser(createClientParam).then(function(response) {
                if (response.data.success == true)
                {

                    $scope.formReset();
                    $state.go('app.userlist');
                }
                $scope.isDataSavingProcess = false;
            });
        };


    }]);


