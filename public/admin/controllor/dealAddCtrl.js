

app.controller('dealAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST) {

    $scope.dealAddModel = {
        id: '',
        productInfo: "",
        stage: '',
        contactInfo: '',
        isActive: true,
        stageList: [],
        nextFollowList: [],
        customer: {},
        employeeList: [],
        user: '',
        type: '',
        closedate: '',
        amount: '',
        notes: ''
    };

    $scope.validationFactory = ValidationFactory;


    $scope.getNavigationBlockMsg = function (pageReload)
    {
        if (typeof $scope.deal_form != 'undefined' && typeof $scope.deal_form.$pristine != 'undefined' && !$scope.deal_form.$pristine)
        {
            return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;
    $scope.dealAddModel.isLoadingProgress = false;
    $scope.formReset = function () {

        //$scope.deal_form.$setPristine();
        $scope.dealAddModel = {
            id: '',
            productInfo: "",
            stage: '',
            isActive: true,

            user: '',
            amount: '',
            closedate: '',
            nextdate: '',
            type: '',
            notes: ''
        };
        $scope.getContactList();
        $scope.getStageList();
        $scope.getEmployeeList();

    }
    $scope.currentDate = new Date();
    $scope.dateFormat = 'dd-MM-yyyy';
    $scope.closeDateOpen = false;
    $scope.nextDateOpen = false;
    $scope.openDate = function (index)
    {
        if (index == 0)
        {
            $scope.closeDateOpen = true;
        }
        if (index == 1)
        {
            $scope.nextDateOpen = true;
        }
    }
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $scope.showCustomerAddPopup = false;
    $scope.showCustomerPopup = function ()
    {
        $scope.showCustomerAddPopup = true;
        //            if (typeof $scope.dealAddModel.customer != undefined && $scope.dealAddModel.customer != '' && $scope.dealAddModel.customer != null)
        //            {
        $rootScope.$broadcast('customerInfo', $scope.dealAddModel.customer);
    //}
    }
    $scope.closeCustomerPopup = function ()
    {
        $scope.showCustomerAddPopup = false;
    }

    $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
    {
        if (typeof customerDetail === 'undefined')
            return;
        $scope.dealAddModel.customer = customerDetail;
    });


    //        $scope.getCustomerList = function (val)
    //        {
    //            var autosearchParam = {};
    //            autosearchParam.search = val;
    //            if ($scope.dealAddModel.type == 'existing')
    //            {
    //                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
    //                {
    //                    var data = responseData.data.list;
    //                    var hits = data;
    //                    if (hits.length > 10)
    //                    {
    //                        hits.splice(10, hits.length);
    //                    }
    //                    return hits;
    //                });
    //            }
    //        };

    //        $scope.formatCustomerModel = function (model)
    //        {
    //            if (model != null)
    //            {
    //                if (model.fname != undefined)
    //                {
    //                    return model.fname + '(' + model.phone + ',' + model.email + ')';
    //                } else if (model.fname != undefined && model.phone != undefined)
    //                {
    //                    return model.fname + '(' + model.phone + ')';
    //                } else
    //                {
    //                    return model.fname;
    //                }
    //            }
    //            return  '';
    //        };

    $scope.getStageList = function ()
    {
        var createParam = {};
        createParam.is_active = 1;
        $scope.dealAddModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getStageList(createParam, configOption).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.dealAddModel.stageList = data.list;
                $scope.dealAddModel.nextFollowList = data.list;
                $scope.dealAddModel.isLoadingProgress = false;
            }
        });
    };

    $scope.getContactList = function ()
    {

        if ($stateParams.id != '' && typeof $stateParams.id != 'undefined' && $stateParams.id != null)
        {
            var contactListParam = {};
            var headers = {};
            headers['screen-code'] = 'deal';
            contactListParam.id = $stateParams.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            $scope.dealAddModel.isLoadingProgress = true;
            adminService.getContactList(contactListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.contactInfo = data.list[0];
                }
                $scope.dealAddModel.isLoadingProgress = false;
            });
        }
    }
    $scope.getProductInfo = function (val) {
        var getGroupListParam = {};
        getGroupListParam.name = val;
        getGroupListParam.type = 'product';
        getGroupListParam.is_active = 1;
        return $httpService.get(APP_CONST.API.PRODUCT_LIST, getGroupListParam, false).then(function (responseData) {
            var data = responseData.data;
            var hits = data.list;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.formatProductModel = function (model) {

        if (model != null)
        {
            if (model.name != undefined && model.sku != undefined)
            {
                return model.name;
            } else if (model.name != undefined && model.hsn_code != undefined)
{
                return model.name;
            }
            return model.name;
        }
        return  '';
    };
    $scope.updateProductInfo = function(model)
    {
        if (model != null)
        {  
            $scope.dealAddModel.mrp_price = model.mrp_price;
        }     
    }
    $scope.resetAmount = function()
    {
        if($scope.dealAddModel.productInfo == null || $scope.dealAddModel.productInfo == 'undefined' || $scope.dealAddModel.productInfo == '')
        {
            $scope.dealAddModel.mrp_price = '';    
        }
    }
    $scope.createNotes = function (dealId)
    {
        if ($scope.isDataSavingProcess == false)
        {
            if ($scope.dealAddModel.notes != '')
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'deal';
                createParam.description = $scope.dealAddModel.notes;
                createParam.type = 'note';
                createParam.deal_id = dealId;
                createParam.comments = 'left a note';
                createParam.contact_id = $scope.dealAddModel.contactInfo.id;
                createParam.is_active = 1;
                createParam.date = $filter('date')($scope.currentDate, 'yyyy-MM-dd');
                createParam.user_id = $rootScope.userModel.id;
                createParam.user_name = $rootScope.userModel.f_name;
                createParam.log_activity_type = '';
                createParam.log_activity_sub_type = '';
                createParam.time = '';
                createParam.attachment = [];
                adminService.addDealActivity(createParam, headers).then(function (response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $scope.isDataSavingProcess = false;
                        $state.go('app.dealedit', {
                            'id': dealId
                        }, {
                            'reload': true
                        });
                    }

                });
            }
        //$state.go('app.dealedit', {'id': dealId}, {'reload': true});
        }

    };

    $scope.getEmployeeList = function ()
    {
        var createParam = {};
        createParam.is_active = 1;
        $scope.dealAddModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getEmployeeList(createParam, configOption).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.dealAddModel.employeeList = data.list;
                $scope.dealAddModel.isLoadingProgress = false;
            }
        });
    };

    $scope.createDeal = function () {
        if ($scope.isDataSavingProcess == false)
        {
            $scope.isDataSavingProcess = true;
            var createParam = {};
            var headers = {};
            headers['screen-code'] = 'deal';
            createParam.name = ''
            if ($scope.dealAddModel.productInfo != '' && $scope.dealAddModel.productInfo.id != '' && $scope.dealAddModel.productInfo.id != undefined && $scope.dealAddModel.productInfo.id != null) {
                createParam.name = $scope.dealAddModel.productInfo.name;
            } else
{
                createParam.name = $scope.dealAddModel.productInfo;
            }
            createParam.stage_id = $scope.dealAddModel.stage;
            for (var i = 0; i < $scope.dealAddModel.stageList.length; i++)
            {
                if ($scope.dealAddModel.stageList[i].id == $scope.dealAddModel.stage)
                {
                    createParam.stage_name = $scope.dealAddModel.stageList[i].name;
                }
            }
            createParam.next_follow_up_action_id = $scope.dealAddModel.next_follow_action;
            for (var i = 0; i < $scope.dealAddModel.nextFollowList.length; i++)
            {
                if ($scope.dealAddModel.nextFollowList[i].id == $scope.dealAddModel.next_follow_action)
                {
                    createParam.next_follow_up_action = $scope.dealAddModel.nextFollowList[i].name;
                }
            }
            createParam.type = $scope.dealAddModel.type;
            createParam.amount = $scope.dealAddModel.mrp_price;
            //createParam.next_follow_up_action = $scope.dealAddModel.next_follow_action;
            createParam.contact_id = $scope.dealAddModel.contactInfo.id;
            createParam.contact_name = $scope.dealAddModel.contactInfo.fname;
            createParam.is_active = 1;
            //                        if ($rootScope.userModel.rolename != 'superadmin')
            //                        {
            createParam.emp_id = $scope.dealAddModel.employee;
            createParam.deal_category_id = $scope.dealAddModel.lead_category;
            for (var i = 0; i < $scope.dealAddModel.employeeList.length; i++)
            {
                if ($scope.dealAddModel.employeeList[i].id == $scope.dealAddModel.employee)
                {
                    createParam.emp_name = $scope.dealAddModel.employeeList[i].f_name;
                }
            }

            //                        } else
            //                        {
            //                              createParam.user_id = $scope.dealAddModel.user;
            //                              for (var i = 0; i < $scope.dealAddModel.userList.length; i++)
            //                              {
            //                                    if ($scope.dealAddModel.userList[i].id == $scope.dealAddModel.user)
            //                                    {
            //                                          createParam.user_name = $scope.dealAddModel.userList[i].f_name;
            //                                    }
            //                              }
            //                        }
            createParam.colsed_date = '';
            createParam.next_follow_up = '';
            if ($scope.dealAddModel.closedate != '')
            {
                if ($scope.dealAddModel.closedate != null && typeof $scope.dealAddModel.closedate == 'object')
                {
                    createParam.colsed_date = utilityService.parseDateToStr($scope.dealAddModel.closedate, $scope.dateFormat);
                }
                createParam.colsed_date = utilityService.changeDateToSqlFormat(createParam.colsed_date, $scope.dateFormat);
            }
            if ($scope.dealAddModel.nextdate != '')
            {
                if ($scope.dealAddModel.nextdate != null && typeof $scope.dealAddModel.nextdate == 'object')
                {
                    createParam.next_follow_up = utilityService.parseDateToStr($scope.dealAddModel.nextdate, $scope.dateFormat);
                }
                createParam.next_follow_up = utilityService.changeDateToSqlFormat(createParam.next_follow_up, $scope.dateFormat);
            }
            adminService.addDeal(createParam, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.isDataSavingProcess = false;
                    if ($scope.dealAddModel.notes != '')
                    {
                        $scope.createNotes(response.data.id);
                    } else
{
                        console.log($stateParams.id);
                        $scope.formReset();
                        $state.go('app.dealedit', {
                            'id': response.data.id
                        }, {
                            'reload': true
                        });
                    }
                }

            });
        }

    };
    $scope.getLeadTypeList = function()
    {
        $scope.dealAddModel.isLoadingProgress = true;
        var leadtypeListParam = {};
        leadtypeListParam.is_active = 1;
        leadtypeListParam.type = 'deal';
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getLeadTypeList(leadtypeListParam, configOption).then(function(response)
        {
            if(response.data.success == true)
            {
                var data = response.data;
                $scope.dealAddModel.leadList = data.list;
            }
            $scope.dealAddModel.isLoadingProgress = false;
        });
            
    };
    $scope.getLeadTypeList();
    $scope.getContactList();
    $scope.getStageList();
    $scope.getEmployeeList();
}]);




