app.controller('workorderListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.workOrderModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            productStatus:'',
            userList: [],
            isLoadingProgress: true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.workOrderModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.searchFilter =
                {
                    id: '',
                    bomTaskInfo: {},
                    userId: '',
                    workType: '',
                    status: '',
                    productStatus:'',
                    fromDate: '',
                    toDate: ''
                };

        $scope.workdateOpen = false;
        //$scope.duedateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.fromdateOpen = true;
            }
            if (index == 1)
            {
                $scope.todateOpen = true;
            }

        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getworkList, 300);
        }

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter =
                    {
                        id: '',
                        userId: '',
                        bomTaskInfo: '',
                        workType: '',
                        status: '',
                        productStatus:'',
                        statusvalue: '',
                        fromDate: '',
                        toDate: ''
                    };
            $scope.noResults = false;
            $scope.initTableFilter();
        }
        $scope.selectInvoiceId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectInvoiceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteworkorder = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectInvoiceId;
                var headers = {};
                headers['screen-code'] = 'workorder';
                adminService.deleteWorkOrder(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getworkList();
                    }

                    $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.$watch('searchFilter.bomTaskInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                $scope.initTableFilter();
            }
        });

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };

        $scope.getworkList = function ()
        {
            $scope.workOrderModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'workorder';

            var configOption = adminService.handleOnlyErrorResponseConfig;
            if ($scope.searchFilter.bomTaskInfo != null && typeof $scope.searchFilter.bomTaskInfo != 'undefined' && typeof $scope.searchFilter.bomTaskInfo.id != 'undefined')
            {
                getListParam.bomId = $scope.searchFilter.bomTaskInfo.id;
            } else
            {
                getListParam.bomId = '';
            }
            if ($scope.searchFilter.fromDate != null && $scope.searchFilter.fromDate != '')
            {
                if ($scope.searchFilter.fromDate != null && typeof $scope.searchFilter.fromDate == 'object')
                {
                    getListParam.fromDate = utilityService.parseDateToStr($scope.searchFilter.fromDate, $scope.adminService.appConfig.date_format);
                }
                getListParam.fromDate = utilityService.changeDateToSqlFormat(getListParam.fromDate, $scope.adminService.appConfig.date_format);
                getListParam.toDate = getListParam.fromDate;
            }

            if ($scope.searchFilter.toDate != null && $scope.searchFilter.toDate != '' && $scope.searchFilter.toDate != null && $scope.searchFilter.toDate != '')
            {
                if ($scope.searchFilter.toDate != null && typeof $scope.searchFilter.toDate == 'object')
                {
                    getListParam.toDate = utilityService.parseDateToStr($scope.searchFilter.toDate, $scope.adminService.appConfig.date_format);
                }
                getListParam.toDate = utilityService.changeDateToSqlFormat(getListParam.toDate, $scope.adminService.appConfig.date_format);
                // getListParam.to_date = getListParam.to_date;
            }
            getListParam.userId = $scope.searchFilter.userId;
            getListParam.id = $scope.searchFilter.id;
            getListParam.workType = $scope.searchFilter.workType;
            getListParam.start = ($scope.workOrderModel.currentPage - 1) * $scope.workOrderModel.limit;
            getListParam.limit = $scope.workOrderModel.limit;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            getListParam.productStatus=$scope.searchFilter.productStatus;
            adminService.getWorkOrderList(getListParam, configOption, headers).then(function (response)
            {
                var data = response.data;
                if (data.success)
                {
                    $scope.workOrderModel.list = data.list;
                    if ($scope.workOrderModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.workOrderModel.list.length; i++)
                        {
                            $scope.workOrderModel.list[i].newdate = utilityService.parseStrToDate($scope.workOrderModel.list[i].deadline_date);
                            $scope.workOrderModel.list[i].deadline_date = utilityService.parseDateToStr($scope.workOrderModel.list[i].newdate, $rootScope.appConfig.date_format);
                        }
                    }
                    $scope.workOrderModel.total = data.total;
                }
                $scope.workOrderModel.isLoadingProgress = false;
            });
        };
        $scope.getTaskList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCTION_TASK_DETAIL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatTaskModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getUserList = function () {

            var getListParam = {};
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function (response) {
                var data = response.data;
                $scope.workOrderModel.userList = data.list;
                $scope.workOrderModel.userId = $rootScope.userModel.id + '';
            });
        };
        $scope.getUserList();
        $scope.getworkList();
    }]);






