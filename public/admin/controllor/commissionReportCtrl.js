


app.controller('commissionReportCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.commissionModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            country: '',
            city: '',
            id: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            userList: [],
            categoryList: [],
            commissionamt: 0,
            totalamt: 0,
            reportDate:'',
            agentName : ''
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.commissionModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
            belongto: '',
            fromdate: '',
            todate: '',
            category: ''
        };
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = 'dd-MM-yyyy';
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.getBelongtoUserList = function() {
            var userListParam = {};
            userListParam.id = "";
            userListParam.name = "";
            adminService.getUserList(userListParam).then(function(response) {
                var data = response.data;
                $scope.commissionModel.userList = data.data;
            });
        };
        $scope.getCategoryList = function() {
            var userListParam = {};
            userListParam.id = "";
            userListParam.name = "";
            adminService.getCategoryList(userListParam).then(function(response) {
                var data = response.data;
                $scope.commissionModel.categoryList = data.list;
            });
        };

        $scope.calculateCommission = function()
        {
            var totCommission = 0;
            var totamt = 0;
            if ($scope.commissionModel.list.length != 0)
            {
                for (var i = 0; i < $scope.commissionModel.list.length; i++)
                {
                    if ($scope.commissionModel.list[i].clientCommission != null)
                    {
                        totCommission += $scope.commissionModel.list[i].amount * $scope.commissionModel.list[i].clientCommission / 100;
                        $scope.commissionModel.commissionamt = totCommission;
                    }
                    totamt += $scope.commissionModel.list[i].amount;
                    $scope.commissionModel.totalamt = totamt;

                }
            }
            else
            {
                $scope.commissionModel.totalamt = 0;
                $scope.commissionModel.commissionamt = 0;
            }
        }

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = "";
            getListParam.start = 0;
            getListParam.limit = $scope.commissionModel.limit;
            getListParam.companyId = $rootScope.userModel.companyId;
            getListParam.userId = '';
            getListParam.clientId = '';
            getListParam.clientEmail = '';
            getListParam.categoryId = $scope.searchFilter.category;
            getListParam.isSettled = '';
            getListParam.fromDate = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
            getListParam.toDate = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');
            if (getListParam.fromDate === '' || getListParam.toDate === '')
            {
                if (getListParam.fromDate != '')
                {
                    getListParam.toDate = getListParam.fromDate;
                }
                if (getListParam.toDate != '')
                {
                    getListParam.fromDate = getListParam.toDate;
                }
            }
            if(getListParam.fromDate === getListParam.toDate)
            {
                $scope.commissionModel.reportDate =  getListParam.fromDate;
            }
            else
            {
                $scope.commissionModel.reportDate = $filter('date')($scope.searchFilter.fromdate,'yyyy-MM-dd') + ' - ' + $filter('date')($scope.searchFilter.todate,'yyyy-MM-dd');
            }
                getListParam.accountNumber = '';
            getListParam.custName = '';
            getListParam.amount = '';
            getListParam.customerOf = $scope.searchFilter.belongto;
            if(getListParam.customerOf != '')
            {
                for(var i = 0; i < $scope.commissionModel.userList.length ; i++)
                {
                    if(getListParam.customerOf ==  $scope.commissionModel.userList[i].userid)
                    {
                        $scope.commissionModel.agentName =  $scope.commissionModel.userList[i].username;
                        break;
                    }    
                }
            }
            $scope.commissionModel.isLoadingProgress = true;
            adminService.getPaymentList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.commissionModel.list = data;
                $scope.calculateCommission();
                $scope.commissionModel.total = response.data.total;
                $scope.commissionModel.isLoadingProgress = false;
            });
        };


        $scope.getBelongtoUserList();
        $scope.getCategoryList();
        $scope.getList();
    }]);








