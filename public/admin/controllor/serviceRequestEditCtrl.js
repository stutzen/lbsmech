




app.controller('serviceRequestEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'sweet', 'NgMap', function ($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, sweet, NgMap) {

        $scope.serviceModel = {
            "id": "",
            "name": "",
            "current_count": 0,
            "is_active": 1,
            "productname": "",
            "customerInfo": "",
            "empInfo": "",
            "machine_status": "",
            "problem": "",
            "date": "",
            "model_name": "",
            "status": "",
            "attachments": [],
            "start_time": "",
            "end_time": "",
            "service_charge": "0.00",
            "spare_charge": "0.00",
            "total_charge": "0.00",
            "currentPage": 1,
            "total": 0,
            "limit": 10
        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.edit_servicerequest_form !== 'undefined' && typeof $scope.edit_servicerequest_form.$pristine !== 'undefined' && !$scope.edit_servicerequest_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {
            $scope.edit_servicerequest_form.$setPristine();
            $scope.updateServiceRequestInfo();


        };
        $scope.clear = function ()
        {
            $scope.serviceModel.workdate = "";
            $scope.serviceModel.start_time = "";
            $scope.serviceModel.end_time = "";
            $scope.serviceModel.comment = "";
        }
        $scope.pagePerCount = [50, 100];
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.serviceModel.limit = $scope.pagePerCount[0];
        $scope.initTableFilterTimeoutPromise = null;

        //        $scope.initTableFilter = function ()
        //        {
        //            if ($scope.initTableFilterTimeoutPromise !== null)
        //            {
        //                $timeout.cancel($scope.initTableFilterTimeoutPromise);
        //            }
        //            $scope.initTableFilterTimeoutPromise = $timeout($scope.getServiceRequestInfo, 300);
        //        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.totalcharge = function () {
            var total = 0.00;
            if ($scope.serviceModel.spare_charge != '')
            {
                total = total + parseFloat($scope.serviceModel.spare_charge);
            }
            if ($scope.serviceModel.service_charge != '')
            {
                total = total + parseFloat($scope.serviceModel.service_charge);
            }
            if (total > 0)
            {
                $scope.serviceModel.total_charge = parseFloat(total).toFixed(2);
            } else
            {
                $scope.serviceModel.total_charge = '';
            }

        };
        $scope.actCenterLat = '';
        $scope.actCenterLng = '';
        $scope.newCenterLat = '';
        $scope.newCenterLng = '';
        $scope.showExistingLocationPopup = false;
        $scope.showMapPopup = false;
        $scope.showLocationPopup = function (index)
        {
            $scope.vm1.positions = [];
            var newPos = {};
            var location = $scope.serviceModel.workList[index].start_location.split(',');
            newPos.lat = parseFloat(location[0]);
            newPos.lng = parseFloat(location[1]);
            $scope.actCenterLat = newPos.lat;
            $scope.actCenterLng = newPos.lng;
            $scope.vm1.positions.push(newPos);
            $scope.showExistingLocationPopup = true;
        };
        $scope.closeLocationPopup = function ()
        {
            $scope.showExistingLocationPopup = false;
        };
         $scope.isLoadedvm1 = false;
        $scope.vm1 = this;
        $scope.isLoadedvm2 = false;
        $scope.vm2 = this;
        NgMap.getMap({
            id: 'vm1'
        }).then(function (map) {
            $scope.vm1.map = map;
            $scope.vm2.map = map;
            $scope.isLoadedvm1 = true;
            $scope.vm1.positions = [{
                    lat: 37.7699298,
                    lng: -122.4469157
                }];
            $scope.vm1.deleteMarkers = function () {
                $scope.vm1.positions = [];
            };
            $scope.vm1.showMarkers = function () {
                for (var key in $scope.vm1.map.markers) {
                    $scope.vm1.map.markers[key].setMap($scope.vm1.map);
                }
            };
            $scope.vm1.hideMarkers = function () {
                for (var key in $scope.vm1.map.markers) {
                    $scope.vm1.map.markers[key].setMap(null);
                }
            };
        });
         $scope.initUpdateMarkerTimeoutPromise = null;

        $scope.initUpdateMarkerDetail = function ()
        {
            if ($scope.initUpdateMarkerTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateMarkerTimeoutPromise);
            }
            if ($scope.isLoadedvm1 && $scope.isDealActivityLoaded && $scope.isLoadedvm2)
            {
                $scope.staticMap = {
                    location: $scope.vm2.address,
                    locationAbout: "Try an address, a city, a place, or even latitude and longitude.",
                    API: "AIzaSyB-n6hREweTj6hYzxj9-nm7T7tapVBzMlc",
                    APIAbout: "You don't always need an API Key for Static Maps, but it's easy to acquire one. Without a key you might receive an error image instead of a map. Follow the link to the API Console.",
                    zoom: 13,
                    minZoom: 0,
                    maxZoom: 22,
                    scaleAbout: "Scale will double the stated height and width. This is good for when you need a width or height larger than 640px.",
                    width: 600,
                    height: 300,
                    maxSize: 640,
                    sizeAbout: "Max is 640px or 1280px when scale 2x.",
                    markerColor: "red",
                    mapType: "roadmap",
                    format: "png",
                    markerSize: "mid",
                    gimmeAbout: "Treat this like a regular image. Pop it into an img tag or use as a background-image."
                };

                //$scope.updateMarkerDetail();
            } else
            {
                $scope.initUpdateMarkerTimeoutPromise = $timeout($scope.initUpdateMarkerDetail, 300);
            }
        }
        $scope.modifyServiceRequest = function () {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var modifyServiceRequestParam = {};
                var headers = {};
                headers['screen-code'] = 'servicerequest';
                modifyServiceRequestParam.customer_id = $scope.serviceModel.customerInfo.id;
                modifyServiceRequestParam.product_id = $scope.serviceModel.productname.product_id;
                modifyServiceRequestParam.serial_no = $scope.serviceModel.productname.serial_no;
                if ($scope.serviceModel.productname.invoice_item_id == 0)
                {
                    modifyServiceRequestParam.product_id = $scope.serviceModel.productname.prod_id;
                }
                modifyServiceRequestParam.product_name = $scope.serviceModel.productname.product_name;
                modifyServiceRequestParam.is_active = $scope.serviceModel.is_active;
                modifyServiceRequestParam.assigned_emp_id = $scope.serviceModel.empInfo.id;
                modifyServiceRequestParam.machine_status = $scope.serviceModel.machine_status;
                if (typeof $scope.serviceModel.date == 'object')
                {
                    $scope.serviceModel.date = utilityService.parseDateToStr($scope.serviceModel.date, 'yyyy-MM-dd');
                }
                modifyServiceRequestParam.date = utilityService.changeDateToSqlFormat($scope.serviceModel.date, 'yyyy-MM-dd');
                modifyServiceRequestParam.model = $scope.serviceModel.model_name;
                modifyServiceRequestParam.status = $scope.serviceModel.status;
                modifyServiceRequestParam.problem = $scope.serviceModel.problem;
                modifyServiceRequestParam.solution = $scope.serviceModel.solution;
                modifyServiceRequestParam.service_charge = $scope.serviceModel.service_charge;
                modifyServiceRequestParam.spare_charge = $scope.serviceModel.spare_charge;
                modifyServiceRequestParam.total_charge = $scope.serviceModel.total_charge;
                modifyServiceRequestParam.attachement = [];
                for (var i = 0; i < $scope.serviceModel.attachments.length; i++)
                {
                    var imageParam = {};
                    imageParam.id = $scope.serviceModel.attachments[i].id;
                    imageParam.url = $scope.serviceModel.attachments[i].url;
                    imageParam.ref_id = $scope.serviceModel.attachments[i].ref_id;
                    imageParam.type = $scope.serviceModel.attachments[i].type;
                    modifyServiceRequestParam.attachement.push(imageParam);
                }
                adminService.modifyServiceRequest(modifyServiceRequestParam, $stateParams.id, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.formReset();
                        $state.go('app.servicerequestEdit', {
                            'id': $stateParams.id
                        }, {
                            'reload': true
                        });
                    }
                });
            }
        };

        $scope.updateServiceRequestInfo = function ()
        {
            if ($scope.serviceModel.list != null)
            {
                $scope.serviceModel.customerInfo = {
                    id: $scope.serviceModel.list.customer_id,
                    fname: $scope.serviceModel.list.customer_name
                }

                $scope.serviceModel.empInfo = {
                    id: $scope.serviceModel.list.assigned_emp_id,
                    f_name: $scope.serviceModel.list.employee_name
                }

                $scope.serviceModel.productname = {
                    id: $scope.serviceModel.list.product_id,
                    product_name: $scope.serviceModel.list.product_name,
                    serial_no : $scope.serviceModel.list.serial_no
                }
                $scope.serviceModel.id = $scope.serviceModel.list.id;
                $scope.serviceModel.machine_status = $scope.serviceModel.list.machine_status + '';
                $scope.serviceModel.problem = $scope.serviceModel.list.problem;
                var newdate = utilityService.parseStrToDate($scope.serviceModel.list.date);
                $scope.serviceModel.date = newdate;
                $scope.serviceModel.model_name = $scope.serviceModel.list.model;
                $scope.serviceModel.status = $scope.serviceModel.list.status + '';
                $scope.serviceModel.is_active = $scope.serviceModel.list.is_active;
                $scope.serviceModel.solution = $scope.serviceModel.list.solution;
                $scope.serviceModel.attachments = $scope.serviceModel.list.attachment;
                for (var i = 0; i < $scope.serviceModel.attachments.length; i++)
                {
                    $scope.serviceModel.attachments[i].path = $scope.serviceModel.attachments[i].url;
                }
                if ($scope.serviceModel.list.service_charge != null && $scope.serviceModel.list.spare_charge != null && $scope.serviceModel.total_charge != null)
                {
                    $scope.serviceModel.service_charge = parseFloat($scope.serviceModel.list.service_charge).toFixed(2);
                    $scope.serviceModel.spare_charge = parseFloat($scope.serviceModel.list.spare_charge).toFixed(2);
                    $scope.serviceModel.total_charge = (parseFloat($scope.serviceModel.service_charge) + parseFloat($scope.serviceModel.list.spare_charge)).toFixed(2);
                } else
                {
                    $scope.serviceModel.service_charge = "";
                    $scope.serviceModel.spare_charge = "";
                    $scope.serviceModel.total_charge = "";
                }

                $scope.getworkLoglist();
                $scope.getspareRequestList();
            }
        };

        $scope.getServiceRequestInfo = function () {

            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getServiceRequestList(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.serviceModel.list = data.list[0];
                        $scope.showWorklogAddPopup = false;
                        $scope.updateServiceRequestInfo();

                    }

                });
            }
        };
        $scope.getServiceRequestInfo();
        $scope.showWorklogAddPopup = false;
        $scope.showWorkLogPopup = function ()
        {
            $scope.showWorklogAddPopup = true;
            ;
        }
        $scope.closeWorkLogPopup = function ()
        {
            $scope.showWorklogAddPopup = false;
        }
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
            if (index == 1)
            {
                $scope.workDateOpen = true;
            }
        }
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.getEmployeeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            if (autosearchParam.fname != '')
            {
                return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatEmployeeModel = function (model)
        {
            if (model != null)
            {
                if (model.f_name != undefined && model.id != undefined && model.emp_code != undefined)
                {
                    return model.f_name + '(' + model.id + ',' + model.emp_code + ')';
                } else if (model.f_name != undefined && model.id != undefined)
                {
                    return model.f_name + '(' + model.id + ')';
                }
            }
            return  '';
        };
        $scope.getspareRequestList = function ()
        {
            $scope.serviceModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'spareRequest';

            var configOption = adminService.handleOnlyErrorResponseConfig;
            getListParam.limit = $scope.serviceModel.limit;
            getListParam.service_request_id = $stateParams.id;
            getListParam.start = ($scope.serviceModel.currentPage - 1) * $scope.serviceModel.limit;
            getListParam.limit = $scope.serviceModel.limit;
            getListParam.is_active = 1;
            getListParam.type = 'spare_request';
            adminService.getSalesOrderList(getListParam, configOption, headers).then(function (response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.serviceModel.spareRequestList = data.list;
                    if ($scope.serviceModel.spareRequestList.length > 0)
                    {
                        for (var i = 0; i < $scope.serviceModel.spareRequestList.length; i++)
                        {
                            $scope.serviceModel.spareRequestList[i].newdate = utilityService.parseStrToDate($scope.serviceModel.spareRequestList[i].date);
                            $scope.serviceModel.spareRequestList[i].date = utilityService.parseDateToStr($scope.serviceModel.spareRequestList[i].newdate, $rootScope.appConfig.date_format);
                            $scope.serviceModel.spareRequestList[i].validuntildate = utilityService.parseStrToDate($scope.serviceModel.spareRequestList[i].validuntil);
                            $scope.serviceModel.spareRequestList[i].validuntil = utilityService.parseDateToStr($scope.serviceModel.spareRequestList[i].validuntildate, $rootScope.appConfig.date_format);
                            //                    $scope.serviceModel.spareRequestList[i].total_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            //                    $scope.serviceModel.spareRequestList[i].advance_amount = parseFloat(utilityService.changeCurrency($scope.invoiceModel.list[i].advance_amount, $rootScope.appConfig.thousand_seperator)).toFixed(2);

                        }
                    }
                    $scope.serviceModel.total = data.total;
                }
                $scope.serviceModel.isLoadingProgress = false;
            });
        };

        $scope.getProductList = function (val)
        {
            if ($scope.serviceModel.customerInfo != '' && $scope.serviceModel.customerInfo != 'undefined' && $scope.serviceModel.customerInfo != null && $scope.serviceModel.customerInfo.id != 'undefined')
            {
                var autoSearchParam = {};
                autoSearchParam.search = val;
                autoSearchParam.customer_id = $scope.serviceModel.customerInfo.id;
                return $httpService.get(APP_CONST.API.GET_CUSTOMER_INVOICE_ITEM_LIST, autoSearchParam, false).then(function (response)
                {
                    var data = response.data.list;
                    var hits = data;
                    if (hits.length > 0)
                    {
                        hits.splice(100, hits.length);
                    }
                    return hits;
                });
            } else
            {
                sweet.show('Oops...', 'Please select atleast one customer', 'error');
            }
        }
        $scope.formatProductModel = function (model)
        {
            if (model != null)
            {
                if (model.product_name != undefined && model.id != undefined)
                {
                    return model.product_name;
                }
            }
            return  '';
        };
        $scope.updateProductDetails = function ()
        {
            if ($scope.serviceModel.customerInfo == '' || $scope.serviceModel.customerInfo == 'undefined' || $scope.serviceModel.customerInfo == null)
            {
                $scope.serviceModel.productname = '';

            }
        }

        $scope.createworklog = function ()
        {
            var createWorklogParam = {};
            var headers = {};
            headers['screen-code'] = 'attendance';
            createWorklogParam.ref_id = $stateParams.id;
            createWorklogParam.ref_type = "servicerequest";
            createWorklogParam.emp_id = $scope.serviceModel.list.assigned_emp_id;
            //        createWorklogParam.start_time = $filter('date')($scope.serviceModel.start_time, 'yyyy-MM-dd hh:mm:ss');
            createWorklogParam.start_time = utilityService.parseDateToStr($scope.serviceModel.workdate, 'yyyy-MM-dd') + ' ' + $filter('date')($scope.serviceModel.start_time, 'HH:mm:ss');
            createWorklogParam.end_time = utilityService.parseDateToStr($scope.serviceModel.workdate, 'yyyy-MM-dd') + ' ' + $filter('date')($scope.serviceModel.end_time, 'HH:mm:ss');
            createWorklogParam.comments = $scope.serviceModel.comment;
            if (typeof $scope.serviceModel.date == 'object')
            {
                $scope.serviceModel.workdate = utilityService.parseDateToStr($scope.serviceModel.workdate, 'yyyy-MM-dd');
            }
            createWorklogParam.date = $scope.serviceModel.workdate;
            adminService.createWorkLog(createWorklogParam, createWorklogParam.ref_id, headers).then(function (response) {
                if (response.data.success === true)
                {
                    $scope.isDataSavingProcess = false;
                    $scope.showWorklogAddPopup = false;
                    $scope.getworkLoglist();
                }
            });
        }
        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function (index, attachindex)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = true;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }
        }
        $scope.closePopup = function (index)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = false;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }
        }

        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data, category, imgcaption, index)
        {
            $scope.uploadedFileQueue = data;
            if (typeof category != undefined && category != null && category != '')
            {
                $scope.uploadedFileQueue[index].category = category;
            }
            if (typeof imgcaption != undefined && imgcaption != null && imgcaption != '')
            {
                $scope.uploadedFileQueue[index].imgcaption = imgcaption;
            }
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
        }

        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.showUploadMoreFilePopup)
            {
                if ($scope.serviceModel.attachments.length > index)
                {
                    $scope.serviceModel.attachments.splice(index, 1);
                }
            }
        }


        $scope.deleteImage = function ($index)
        {
            $scope.serviceModel.attachments.splice($index, 1);
        }

        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                if ($scope.showUploadMoreFilePopup)
                {
                    $scope.closePopup('morefileupload');
                }
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    //               $scope.serviceModel.attachments.push($scope.uploadedFileQueue[i]);
                    var imageUpdateParam = {};
                    imageUpdateParam.id = 0;
                    imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                    imageUpdateParam.ref_id = $stateParams.id;
                    imageUpdateParam.type = 'servicerequest';
                    imageUpdateParam.comments = '';
                    imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
                    imageUpdateParam.path = $scope.uploadedFileQueue[i].signedUrl;
                    $scope.serviceModel.attachments.push(imageUpdateParam);
                    if (i == $scope.uploadedFileQueue.length - 1)
                    {
                        $scope.closePopup('morefileupload');
                    }
                }
                $scope.modifyServiceRequest();
            } else
            {
                $scope.closePopup('morefileupload');
            }
        };
        $scope.timeFormat = "";
        $scope.endtimeFormat = "";
        $scope.getworkLoglist = function ()
        {
            var worklogLisParam = {};
            var headers = {};
            headers['screen-code'] = 'servicerequest';
            worklogLisParam.ref_id = $stateParams.id;
            worklogLisParam.ref_type = "servicerequest";
            worklogLisParam.emp_id = $scope.serviceModel.list.assigned_emp_id;
            worklogLisParam.start = ($scope.serviceModel.currentPage - 1) * $scope.serviceModel.limit;
            worklogLisParam.limit = $scope.serviceModel.limit;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getWorklogList(worklogLisParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.serviceModel.workList = data;
                    for (var i = 0; i < $scope.serviceModel.workList.length; i++)
                    {
                        if ($scope.serviceModel.workList[i].start_time != '' && $scope.serviceModel.workList[i].end_time != '')
                        {
                            $scope.serviceModel.workList[i].starttime = $scope.serviceModel.workList[i].start_time.split(' ')[1];
                            $scope.serviceModel.workList[i].startTime = utilityService.convertTime24to12($scope.serviceModel.workList[i].starttime);
                            $scope.serviceModel.workList[i].endtime = $scope.serviceModel.workList[i].end_time.split(' ')[1];
                            $scope.serviceModel.workList[i].endTime = utilityService.convertTime24to12($scope.serviceModel.workList[i].endtime);
                            $scope.serviceModel.workList[i].startDate = $scope.serviceModel.workList[i].start_time.split(' ')[0];
                            if (moment($scope.serviceModel.workList[i].startDate).valueOf() > 0)
                            {
                                var newnextdate = utilityService.convertToUTCDate($scope.serviceModel.workList[i].startDate);
                                $scope.serviceModel.workList[i].start_date = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                            } else
                            {
                                $scope.serviceModel.workList[i].start_date = '';
                            }
                        }
                    }
                    $scope.serviceModel.total = data.total;
                }
            });
        };
        //$scope.getworkLoglist();


    }]);




