

app.controller('stageAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.stageModel = {
            id: '',
            name: '',
            isActive: true,
            level: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_stage_form != 'undefined' && typeof $scope.add_stage_form.$pristine != 'undefined' && !$scope.add_stage_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_stage_form.$setPristine();
            if (typeof $scope.add_stage_form != 'undefined')
            {
                $scope.stageModel.name = "";
                $scope.stageModel.level = "";
                $scope.stageModel.isActive = true;
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.createstage = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addStageParam = {};
            var headers = {};
            headers['screen-code'] = 'stage';
            addStageParam.id = 0;
            addStageParam.name = $scope.stageModel.name;
            addStageParam.level = $scope.stageModel.level;
            addStageParam.stage_flag = $scope.stageModel.stageflag;
            addStageParam.is_active = $scope.stageModel.isActive == true ? 1 : 0;

            adminService.addStage(addStageParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.stage');
                }
                $scope.isDataSavingProcess = false;
            });
        };

    }]);
