





app.controller('consolidatedacodeStatementCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.acodeModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            totaldebit: 0,
            totalcredit: 0,
            totalamt: 0,
            totalbalance: 0,
            openingBalance: null,
            accountList: [],
            accountName: '',
            isSearchLoadingProgress: false,
            openingCredit: '',
            openingDebit: '',
            openingAmount: ''
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.acodeModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            acode: ''

        };
        $scope.searchFilter.todate = $scope.currentDate;
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                acode: ''
            };
            $scope.acodeModel.list = [];
            $scope.searchFilter.todate = $scope.currentDate;
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.calculateTotal = function()
        {
            var totDebit = 0;
            var totCredit = 0;
            var totamt = 0;
            $scope.acodeModel.totaldebit = 0.00;
            $scope.acodeModel.totalcredit = 0.00;
            for (var i = 0; i < $scope.acodeModel.list.length; i++)
            {
                $scope.acodeModel.list[i].credit = (typeof $scope.acodeModel.list[i].credit == 'undefined' || $scope.acodeModel.list[i].credit == null || isNaN($scope.acodeModel.list[i].credit)) ? 0 : parseFloat($scope.acodeModel.list[i].credit);
                $scope.acodeModel.list[i].debit = (typeof $scope.acodeModel.list[i].debit == 'undefined' || $scope.acodeModel.list[i].debit == null || isNaN($scope.acodeModel.list[i].debit)) ? 0 : parseFloat($scope.acodeModel.list[i].debit);
                $scope.acodeModel.list[i].balance = parseFloat($scope.acodeModel.list[i].credit - $scope.acodeModel.list[i].debit);
                $scope.acodeModel.list[i].displayCredit = 0;
                $scope.acodeModel.list[i].displayDebit = 0;
                if ($scope.acodeModel.list[i].balance > 0)
                {
                    $scope.acodeModel.list[i].displayCredit = $scope.acodeModel.list[i].balance;
                    $scope.acodeModel.list[i].displayDebit = 0;
                }
                if ($scope.acodeModel.list[i].balance < 0)
                {
                    $scope.acodeModel.list[i].displayDebit = Math.abs($scope.acodeModel.list[i].balance);
                    $scope.acodeModel.list[i].displayCredit = 0;
                }
                totDebit += parseFloat($scope.acodeModel.list[i].displayDebit);
                totCredit += parseFloat($scope.acodeModel.list[i].displayCredit);
            }
            $scope.acodeModel.totaldebit = totDebit;
            $scope.acodeModel.totaldebit = parseFloat($scope.acodeModel.totaldebit).toFixed(2);
            $scope.acodeModel.totaldebit = utilityService.changeCurrency($scope.acodeModel.totaldebit, $rootScope.appConfig.thousand_seperator);
            $scope.acodeModel.totalcredit = totCredit;
            $scope.acodeModel.totalcredit = parseFloat($scope.acodeModel.totalcredit).toFixed(2);
            $scope.acodeModel.totalcredit = utilityService.changeCurrency($scope.acodeModel.totalcredit, $rootScope.appConfig.thousand_seperator);

        }

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'consolidatedacode';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);         //  getListParam.from_Date = getListParam.to_Date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
            }
            getListParam.acode = '';
            if ($scope.searchFilter.acode != null && $scope.searchFilter.acode != '' && $scope.searchFilter.acode != 'undefined')
            {
                getListParam.acode = $scope.searchFilter.acode.code;
            }

            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.acodeModel.isLoadingProgress = true;
            $scope.acodeModel.isSearchLoadingProgress = true;
            adminService.getAcodeStatement(getListParam, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.acodeModel.list = data.list;
                    if ($scope.acodeModel.list.length !== 0)
                    {

                        $scope.calculateTotal();
                        for (var i = 0; i < $scope.acodeModel.list.length; i++)
                        {
                            $scope.acodeModel.list[i].newdate = utilityService.parseStrToDate($scope.acodeModel.list[i].transaction_date);
                            $scope.acodeModel.list[i].transaction_date = utilityService.parseDateToStr($scope.acodeModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                            $scope.acodeModel.list[i].displayCredit = parseFloat($scope.acodeModel.list[i].displayCredit).toFixed(2);
                            $scope.acodeModel.list[i].displayCredit = utilityService.changeCurrency($scope.acodeModel.list[i].displayCredit, $rootScope.appConfig.thousand_seperator);
                            $scope.acodeModel.list[i].displayDebit = parseFloat($scope.acodeModel.list[i].displayDebit).toFixed(2);
                            $scope.acodeModel.list[i].displayDebit = utilityService.changeCurrency($scope.acodeModel.list[i].displayDebit, $rootScope.appConfig.thousand_seperator);
                        }
                    }
                    $scope.acodeModel.total = data.total;
                }
                $scope.acodeModel.isLoadingProgress = false;
                $scope.acodeModel.isSearchLoadingProgress = false;
            });

        };

        $scope.print = function()
        {
            $window.print();
        };
        $scope.getAcodeList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.code = val;
            //  autosearchParam.ref_type = 'customer';
            return $httpService.get(APP_CONST.API.ACODE_SEARCH, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatAcodeModel = function(model)
        {
            if (model != null)
            {
                return model.code;
            }
            return  '';
        };

//        $scope.$watch('searchFilter.acode', function(newVal, oldVal)
//        {
//            if (typeof newVal === 'undefined' || newVal == null || newVal === '')
//            {
//                $scope.initTableFilter();
//
//            }
//        });


        $scope.getList();
    }]);




