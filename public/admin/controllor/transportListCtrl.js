


app.controller('transportListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.transportModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            email: '',
            list: [],
            country: '',
            city: '',
            id: '',
            status: '',
            serverList: null,
            isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.transportModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
            email: '',
            username: '',
            phno: '',
            user_type: '',
            name: '',
            country: '',
            city: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }        
        
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                email: '',
                phno: '',
                name: '',
                city: ''
            };
            $scope.initTableFilter();
        }
        $scope.selectTransportId = '';
        $scope.showdeletePopup = false;
        
        $scope.isdeleteTransportLoadingProgress = false;
        

        $scope.showPopup = function(id)
        {
            $scope.selectTransportId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteTransportLoadingProgress = false;
        };

        $scope.deleteTransportInfo = function( )
        {
            if ($scope.isdeleteTransportLoadingProgress == false)
            {
                $scope.isdeleteTransportLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectTransportId;
                var headers = {};
                headers['screen-code'] = 'transport';
                adminService.deleteTransport(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteTransportLoadingProgress = false;
                });
            }
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'transport';           
            getListParam.name = $scope.searchFilter.name;
            getListParam.mobile_no = $scope.searchFilter.phno;
            getListParam.email = $scope.searchFilter.email;
            getListParam.country = '';
            getListParam.city_name = $scope.searchFilter.city;          
            getListParam.is_active = 1;
            getListParam.start = ($scope.transportModel.currentPage - 1) * $scope.transportModel.limit;
            getListParam.limit = $scope.transportModel.limit;
            $scope.transportModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getTransportList(getListParam,configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.transportModel.list = data.list;
                    $scope.transportModel.total = data.total;
                }
                $scope.transportModel.isLoadingProgress = false;
            });

        };
       
        $scope.getList();
    }]);








