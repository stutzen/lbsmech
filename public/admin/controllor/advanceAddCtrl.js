

app.controller('advanceAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, sweet) {

        $scope.advanceModel = {
            id: '',
            date: '',
            adv_amount: '',
            remark: "",
            employeeInfo: '',
            emp_id: '',
            emp_code: '',
            deduction_amt: '',
            balance_amt: '',
            is_active: 1,
            accountList: [],
            account_id: ''
        }

        $scope.validationFactory = ValidationFactory;
        $scope.adminService = adminService;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.advance_add_form != 'undefined' && typeof $scope.advance_add_form.$pristine != 'undefined' && !$scope.advance_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.advance_add_form.$setPristine();
            if (typeof $scope.advance_add_form != 'undefined')
            {
                $scope.advanceModel.id = "";
                $scope.advanceModel.date = "";
                $scope.advanceModel.adv_amount = '';
                $scope.advanceModel.remark = "";
                $scope.advanceModel.employeeInfo = "";
                $scope.advanceModel.emp_id = '';
                $scope.advanceModel.emp_code = '';
                $scope.advanceModel.deduction_amt = '';
                $scope.advanceModel.balance_amt = "";
                $scope.advanceModel.account_id = "";
            }
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.currentDate = new Date();

        $scope.advanceDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.advanceDateOpen = true;
            }
        }

        $scope.getEmployeelist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeeModel = function (model) {

            if (model !== null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };

        $scope.addAdvance = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            if ($scope.advanceModel.account_id != '' && $scope.advanceModel.account_id != 'undefined' && $scope.advanceModel.account_id != null)
            {
                for (var i = 0; i < $scope.advanceModel.accountList.length; i++)
                {
                    if ($scope.advanceModel.account_id == $scope.advanceModel.accountList[i].id)
                    {
                        var advanceAmount = parseFloat($scope.advanceModel.adv_amount);
                        $scope.advanceModel.accountList[i].balance = parseFloat($scope.advanceModel.accountList[i].balance);
                        if ($scope.advanceModel.accountList[i].balance < advanceAmount)
                        {
                            $scope.advanceModel.account_id = '';
                            sweet.show('Oops...', 'Advance Amount greater than account balance...', 'error');
                            return;
                        }
                    }
                }
            }
            $scope.isDataSavingProcess = true;
            var addAdvanceParam = {};
            var headers = {};
            headers['screen-code'] = 'advance';

            addAdvanceParam.emp_id = $scope.advanceModel.employeeInfo.id;
            addAdvanceParam.emp_code = $scope.advanceModel.employeeInfo.emp_code;
            addAdvanceParam.date = $filter('date')($scope.advanceModel.date, 'yyyy-MM-dd');
            addAdvanceParam.adv_amount = $scope.advanceModel.adv_amount;
            addAdvanceParam.deduction_amt = $scope.advanceModel.deduction_amt;
            addAdvanceParam.balance_amt = $scope.advanceModel.adv_amount;
            addAdvanceParam.is_active = $scope.advanceModel.is_active;
            addAdvanceParam.account_id = $scope.advanceModel.account_id;
            addAdvanceParam.remark = $scope.advanceModel.remark;

            adminService.addAdvance(addAdvanceParam, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.advanceList');
                }
                $scope.isDataSavingProcess = false;
            });

        };

        $scope.getAccountList = function () {

            var getListParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response) {
                var data = response.data;
                $scope.advanceModel.accountList = data.list;

            });
        };

        $scope.getAccountList();

    }]);
