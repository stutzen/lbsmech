app.controller('expensetrackerAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.expenseTrackerModel = {
            "id": 0,
            "date": "",
            "amount": "",
            "is_active": 1,
            "list": []
        }
//        $("#field1").focus();
//        $scope.moveFocus = function(nextId) {
//            $('#' + nextId).focus();
//        };
        $scope.totalQty = 0;

        $scope.expenseTrackerModel.orderdate = new Date();
        $scope.pagePerCount = [10, 20, 50, 100];
        $scope.expenseTrackerModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.expense_add_form != 'undefined' && typeof $scope.expense_add_form.$pristine != 'undefined' && !$scope.expense_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.expense_add_form.$setPristine();
            $scope.expenseTrackerModel = {
                "id": 0,
                "contractorInfo": {},
                "projectInfo": {},
                "plotInfo": {},
                "totalQty": "",
                "total": "",
                "taskInfo": {},
                "list": []
            }
            $scope.expenseTrackerModel.date = new Date();
            $scope.addTask();
            $scope.showEditButton[0] = false;
        }

        $scope.resetOrderCreate = function() {

            $scope.expenseTrackerModel.user = '';
        }


        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedTaskList = [];
        $scope.undeletedTaskList = [];
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.orderDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.expenseTrackerModel.date = new Date();
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.expenseDateOpen = true;
            }
        }

        $scope.showPopupFlg = false;
        $scope.closePopup = function()
        {
            $scope.showPopupFlg = false;
            $scope.formReset();
            $state.go('app.purchaseorderlist');
        };
        $scope.showUploadPhotoPopup = false;
        $scope.showPhotoUploadPopup = function($index) {
            $scope.taskIndex = $index;
            $scope.showUploadPhotoPopup = true;
            $scope.expenseTrackerModel.list[$scope.taskIndex].attachment = $scope.expenseTrackerModel.list[$index].attachment;
        };
        $scope.closePhotoUploadPopup = function() {

            $scope.showUploadPhotoPopup = false;
        };
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
        }
        ;
        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.expenseTrackerModel.list[$scope.taskIndex].attachment.length > index)
            {
                if (typeof $scope.expenseTrackerModel.list[$scope.taskIndex].attachment[index].id != 'undefined' && $scope.expenseTrackerModel.list[$scope.taskIndex].attachment[index].id != '')
                {
                    var imageUpdateParam = {};
                    imageUpdateParam.id = $scope.expenseTrackerModel.list[$scope.taskIndex].attachment[index].id;
                    var configData = {};
                    configData.imgIndex = index;
                    adminService.deleteImage(imageUpdateParam, configData).then(function(response) {

                        var config = response.config.config;
                        if (typeof config.imgIndex !== 'undefined')
                        {
                            var index = config.imgIndex;
                            $scope.expenseTrackerModel.list[$scope.taskIndex].attachment.splice(index, 1);
                            $scope.$broadcast('genericSuccessEvent', 'Image removed successfully.');
                        }

                    });
                }
                else
                {
                    $scope.expenseTrackerModel.list[$scope.taskIndex].attachment.splice(index, 1);
                    $scope.uploadedFileQueue.splice(index, 1);
                }
            }

        }
        $scope.saveImagePath = function($index)
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePhotoUploadPopup();
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    var imageUpdateParam = {};
                    imageUpdateParam.path = $scope.uploadedFileQueue[i].urlpath;
                    imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                    $scope.expenseTrackerModel.list[$scope.taskIndex].attachment.push(imageUpdateParam);
                    $scope.closePhotoUploadPopup();
                }
                $scope.isImageSavingProcess = false;
            }
        }



        $scope.initvalidateTaskDataAndAddNewPromise = null;
        $scope.showEditButton = [];
        $scope.hasTaskEditIsProgress = false;

        $scope.initValidateTaskDataAndAddNew = function(index)
        {

            if ($scope.initvalidateTaskDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateTaskDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateTaskDataAndAddNewPromise = $timeout(
                    function() {
                        $scope.validateTaskDataAndAddNew(index);
                    },
                    300);
        };

        $scope.showEditTask = function(index)
        {
            if (index > $scope.expenseTrackerModel.list.length)
            {
                return;
            }
            var editProgress = false;
            var editIndex = -1;
            for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
            {
                if (i == index)
                {
                    $scope.showEditButton[i] = false;
                    editProgress = true;
                    editIndex = i;
                }
                else
                {
                    $scope.showEditButton[i] = true;
                }
            }
            if (editProgress)
            {
                $scope.hasTaskEditIsProgress = true
            }
            else
            {
                $scope.hasTaskEditIsProgress = false;
            }
        };

        $scope.updateShowEditTaskOption = function()
        {
            var editProgress = false;
            for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
            {
                if ($scope.showEditButton[i] != undefined && !$scope.showEditButton[i])
                {
                    editProgress = true;
                }
            }
            if (editProgress)
            {
                $scope.hasTaskEditIsProgress = true
            }
            else
            {
                $scope.hasTaskEditIsProgress = false;
            }
        };


        $scope.editTask = function(index)
        {
            $scope.showDetails[index] = true;
            for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
            {
                if (i == index)
                {
                    $scope.showEditButton[index] = false;
                }
            }
            $scope.updateShowEditTaskOption();
            $timeout(function() {
                //angular.element("#task_"+$index+"_branch0_1").focus();
                //  console.log('msg in time out');
                $("#task_" + index + "_1").focus();
            }, 300);

        }

        $scope.validateTaskDataAndAddNew = function(index)
        {
            var formDataError = false;
            var taskContainer = window.document.getElementById('taskContainer_' + index);
            var errorCell = angular.element(taskContainer).find('.has-error').length;
            if (errorCell > 0)
            {
                formDataError = true;
            }
            var branchCountError = !$scope.checkCount(index);
            if (formDataError & branchCountError)
            {
                sweet.show('Oops...', 'Error in Task Data & Branch item count..', 'error');
                return;
            }
            else if (formDataError || branchCountError)
            {
                if (formDataError)
                {
                    sweet.show('Oops...', 'Error in task data', 'error');
                }
                else if (branchCountError)
                {
                    sweet.show('Oops...', 'Error in branch item count..', 'error');
                }

                return;
            }
            var taskLastRowIndex = $scope.expenseTrackerModel.list.length - 1;
            if (taskLastRowIndex == index)
            {
                $scope.addTask();

                var lastTaskRowIndex = $scope.expenseTrackerModel.list.length - 1;
                $timeout(function() {
                    //angular.element("#task_"+$index+"_branch0_1").focus();
                    //  console.log('msg in time out');
                    $("#task_" + lastTaskRowIndex + "_1").focus();
                }, 300);
            }
            else
            {
                $scope.showEditButton[index] = true;
                $scope.updateShowEditTaskOption();
            }
        }
        $scope.getAllowancelist = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.ALLOWANCE_MASTER_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatAllowanceModel = function(model) {


            if (model !== null && model != undefined)
            {
                return model.name;
            }
            return  '';
        };



        $scope.hasLastRow = function(index) {


            if (index == $scope.expenseTrackerModel.list.length - 1)
            {
                return true;
            }
            else
            {
                return  false;
            }
        };
        $scope.deleteTask = function(deleteTaskIndex) {

            if (deleteTaskIndex < $scope.expenseTrackerModel.list.length)
            {
                $scope.expenseTrackerModel.list.splice(deleteTaskIndex, 1);

                $scope.updateShowEditTaskOption();
                $scope.updateInvoiceTotal();
            }
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                $scope.getSupplierList();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                $scope.getSupplierList();
            }

        }
        $scope.calculatetotal = function()
        {
            var subTotal = 0;
            var subQty = 0;
            $scope.expenseTrackerModel.totalQty = 0;
            for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
            {
                if (typeof $scope.expenseTrackerModel.list[i].unitprice != 'undefined' && $scope.expenseTrackerModel.list[i].unitprice != null && $scope.expenseTrackerModel.list[i].unitprice != 0 && !isNaN($scope.expenseTrackerModel.list[i].unitprice) &&
                        typeof $scope.expenseTrackerModel.list[i].reqqty != '' && typeof $scope.expenseTrackerModel.list[i].reqqty != 'undefined' && $scope.expenseTrackerModel.list[i].reqqty != null && $scope.expenseTrackerModel.list[i].reqqty != 0 && !isNaN($scope.expenseTrackerModel.list[i].reqqty))
                {
                    $scope.expenseTrackerModel.list[i].rowtotal = $scope.expenseTrackerModel.list[i].unitprice * $scope.expenseTrackerModel.list[i].reqqty;
                    subTotal += parseFloat($scope.expenseTrackerModel.list[i].rowtotal, 10);
                    subQty += parseFloat($scope.expenseTrackerModel.list[i].reqqty, 10);
                }
                else
                {
                    $scope.expenseTrackerModel.list[i].rowtotal = 0;
                    subTotal += parseFloat($scope.expenseTrackerModel.list[i].rowtotal, 10);
                    $scope.expenseTrackerModel.list[i].reqqty = 0;

                    subQty += parseFloat($scope.expenseTrackerModel.list[i].reqqty, 10);
                }
            }
            $scope.expenseTrackerModel.totalQty = parseFloat(subQty, 10).toFixed(2);
            $scope.expenseTrackerModel.total = parseFloat(subTotal, 10).toFixed(2);
        }
        $scope.showTaskList = false;
        $scope.showTask = function()
        {
            $scope.showTaskList = !$scope.showTaskList;
        }

        $scope.addTask = function(index)
        {
            var newRow = {
                "allowance_id": '',
                "role_id": "",
                "allowance_name": '',
                "amount": '',
                "comments": '',
                "is_active": '',
                "date": '',
                "attachment": [],
            }
            $scope.expenseTrackerModel.list.push(newRow);
            //  $scope.showEditTask($scope.expenseTrackerModel.list.length - 1);
            $scope.updateInvoiceTotal();
        };

        $scope.taskRenderInitHandler = function(index)
        {
            $timeout(function()
            {
                $scope.updateBranchId(index);
            }, 300);
        }
        $scope.showDetails = [];
        $scope.toggleTaskBranch = function($index)
        {
            if ($scope.showDetails[$index])
            {
                $scope.hideDetails($index);
            }
            else
            {
                $scope.displayDetails($index);
            }
        }

        $scope.displayDetails = function($index)
        {
            $scope.showDetails[$index] = true;
        }
        $scope.hideDetails = function($index)
        {
            $scope.showDetails[$index] = false;
        }

        $scope.formValidator = function()
        {
            if ($scope.expense_add_form.$submitted)
            {
                $timeout(function() {
                    $scope.expense_add_form.$submitted = false;
                }, 0);
                $timeout(function() {
                    $scope.expense_add_form.$setSubmitted();
                }, 0);
            }
            else
            {
                $timeout(function() {
                    $scope.expense_add_form.$setSubmitted();
                }, 0);
            }
        }

        $scope.initCreatePurchaseOrderTimeoutPromise = null;
        $scope.initCreatePurchaseOrder = function()
        {
            if ($scope.initCreatePurchaseOrderTimeoutPromise != null)
            {

                $timeout.cancel($scope.initCreatePurchaseOrderTimeoutPromise);
            }

            $scope.formValidator();
            $scope.initCreatePurchaseOrderTimeoutPromise = $timeout($scope.createExpense, 300);
        }

        $scope.createExpense = function() {
            if (!$scope.isDataSavingProcess)
            {
                if ($scope.expenseTrackerModel.list.length != 0)
                {
                    $scope.isDataSavingProcess = true;
                    var createExpenseParam = [];
                    for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
                    {
                        var expenseParam = {};
                        expenseParam.allowance_id = $scope.expenseTrackerModel.list[i].allowanceInfo.id;
                        expenseParam.allowance_name = $scope.expenseTrackerModel.list[i].allowanceInfo.name;
                        expenseParam.amount = $scope.expenseTrackerModel.list[i].amount;
                        expenseParam.comments = $scope.expenseTrackerModel.list[i].comments;
                        expenseParam.is_active = 1;
                        expenseParam.date = utilityService.parseDateToStr($scope.expenseTrackerModel.date, 'yyyy-MM-dd');
                        expenseParam.attachment = [];
                        for (var k = 0; k < $scope.expenseTrackerModel.list[i].attachment.length; k++)
                        {
                            var mediaDetail = {};
                            mediaDetail.url = $scope.expenseTrackerModel.list[i].attachment[k].path;
                            expenseParam.attachment.push(mediaDetail);
                        }
                        createExpenseParam.push(expenseParam);
                    }
                    adminService.createExpenseTrackerList(createExpenseParam).then(function(response) {
                        if (response.data.success == true)
                        {
                            $scope.formReset();
                            $state.go('app.expensetracker');
                        }
                    });
                }
                else
                {
                    sweet.show('Oops...', 'Add Expense', 'error');
                }
            }
        };
        $scope.addTask();

    }]);




