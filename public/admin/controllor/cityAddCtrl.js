

app.controller('cityAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.cityModel = {
            id: '',
            name: '',
            isActive: true,
            code: '',
            country_name:'',
            country_id:'',
            state_name:'',
            state_id:'',
            country_list:[],
            state_list:[]
        }

        $scope.validationFactory = ValidationFactory;
         $scope.cityModel.isLoadingProgress = false;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_city_form != 'undefined' && typeof $scope.add_city_form.$pristine != 'undefined' && !$scope.add_city_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_city_form.$setPristine();
            if (typeof $scope.add_city_form != 'undefined')
            {
                $scope.cityModel.name = "";
                $scope.cityModel.code = "";
                $scope.cityModel.isActive = true;
                $scope.cityModel.country_id="";
                $scope.cityModel.state_id="";
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

//         $scope.initStateListTimeoutPromise = null;
//
//        $scope.initStateList = function(searchkey)
//        {
//            if ($scope.initStateListTimeoutPromise != null)
//            {
//                $timeout.cancel($scope.initStateListTimeoutPromise);
//            }
//            $scope.initStateListTimeoutPromise = $timeout(function() {
//                $scope.getstateList(searchkey,'state');
//            }, 300);
//
//        };
        $scope.createcity = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addCityParam = {};
            var headers = {};
            headers['screen-code'] = 'city';
            addCityParam.id = 0;
            addCityParam.name = $scope.cityModel.name;
            addCityParam.code = $scope.cityModel.code;
            addCityParam.is_active = $scope.cityModel.isActive == true ? 1 : 0;
            addCityParam.country_id = $scope.cityModel.country_id;
            addCityParam.state_id = $scope.cityModel.state_id;
          
             for (var i = 0; i < $scope.cityModel.country_list.length; i++)
            {
                if ($scope.cityModel.country_id == $scope.cityModel.country_list[i].id)
                {
                    addCityParam.country_name = $scope.cityModel.country_list[i].name;
                }
            }
             for (var i = 0; i < $scope.cityModel.state_list.length; i++)
            {
                if ($scope.cityModel.state_id == $scope.cityModel.state_list[i].id)
                {
                    addCityParam.state_name = $scope.cityModel.state_list[i].name;
                }
            }
            adminService.saveCity(addCityParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.city');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        
        $scope.getcountryList = function() {          

            $scope.cityModel.isLoadingProgress = true;
            var countryListParam = {};
            var headers = {};
            headers['screen-code'] = 'country';           
           
            countryListParam.id = '';
            
            countryListParam.is_active = 1;
           
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.cityModel.country_list = data.list;                   
                }
                $scope.cityModel.isLoadingProgress = false;
            });

        };
        $scope.getcountryList();
        
        $scope.getstateList = function() {          
           
            $scope.cityModel.isLoadingProgress = true;
            var stateListParam = {};
            var headers = {};
            headers['screen-code'] = 'state';           
           
            stateListParam.id = '';
            
            stateListParam.is_active = 1;
            stateListParam.country_id = $scope.cityModel.country_id;
            stateListParam.country_name = $scope.cityModel.country_name;
          
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.cityModel.state_list = data.list;                   
                 }
                $scope.cityModel.isLoadingProgress = false;
            });

        };
        
         
         $scope.CountryChange = function () 
        {
            $scope.cityModel.state_id = '';
            $scope.getstateList();
            
        }
        

       
    }]);








