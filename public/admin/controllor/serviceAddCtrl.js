

app.controller('serviceAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.serviceModel = {
            id: '',
            name: '',
            sku: '',
            uomName: "",
            uomList: [],
            description: '',
            salesPrice: '',
            isActive: true,
            isPurchase: true,
            isSale: true,
            hsncode:'',
            attachment:[]
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.service_add_form != 'undefined' && typeof $scope.service_add_form.$pristine != 'undefined' && !$scope.service_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.service_add_form.$setPristine();
            if (typeof $scope.service_add_form != 'undefined')
            {
                $scope.serviceModel.name = "";
                $scope.serviceModel.sku = "";
                $scope.serviceModel.salesPrice = '';
                $scope.serviceModel.description = "";
                $scope.serviceModel.uomName = "";
                $scope.serviceModel.isActive = true;
                $scope.serviceModel.isPurchase = true;
                $scope.serviceModel.isSale = true;
                $scope.serviceModel.hsncode = "";
            }
        }

        $scope.getCUOMNameList = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = ($scope.serviceModel.currentPage - 1) * $scope.serviceModel.limit;
            getListParam.limit = $scope.serviceModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUom(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.serviceModel.uomList = data.list;
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getCUOMNameList( );
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        /*
         *  It used to format the speciality model value in UI
         */
        $scope.addServiceInfo = function() {

                if ($scope.isDataSavingProcess)
                {
                    return;
                }

                $scope.isDataSavingProcess = true;
                var addProductParam = {};
                var headers = {};
                headers['screen-code'] = 'service';
                // addProductParam.id = 0;
                addProductParam.name = $scope.serviceModel.name;
                addProductParam.sku = $scope.serviceModel.sku;
                addProductParam.hsn_code = $scope.serviceModel.hsncode;
                addProductParam.sales_price = parseFloat($scope.serviceModel.salesPrice);
                addProductParam.description = $scope.serviceModel.description;
                addProductParam.type = "Service";
                //   addProductParam.uom = $scope.serviceModel.uomName;

                addProductParam.is_active = $scope.serviceModel.isActive == true ? 1 : 0;
                addProductParam.is_purchase = $scope.serviceModel.isPurchase == true ? 1 : 0;
                addProductParam.is_sale = $scope.serviceModel.isSale == true ? 1 : 0;
                addProductParam.min_stock_qty = '';
                addProductParam.opening_stock = '';
                addProductParam.has_inventory = 0;
                addProductParam.attachment =[];
                if ($scope.serviceModel.uomName != "")
                {
                    for (var loopIndex = 0; loopIndex < $scope.serviceModel.uomList.length; loopIndex++)
                    {
                        if ($scope.serviceModel.uomName == $scope.serviceModel.uomList[loopIndex].name)
                        {
                            addProductParam.uom_id = $scope.serviceModel.uomList[loopIndex].id;
                            break;
                        }
                    }
                }
                else
                {
                    addProductParam.uom_id = "";
                }
                adminService.addService(addProductParam, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.serviceList');
                    }
                    $scope.isDataSavingProcess = false;
                });
        };

    }]);
