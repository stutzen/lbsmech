app.controller('inventoryStockCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$timeout', '$window', 'APP_CONST', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, $timeout, $window, APP_CONST) {

    $rootScope.getNavigationBlockMsg = null;
    $scope.inventoryStockModel = {
        scopeId: "",
        configId: 5,
        type: "",
        currentPage: 1,
        total: 0,
        limit: '',
        list: [],
        categorylist:[],
        status: '',
        start: 0,
        currencyFormat: '',
        serverList: null,
        isLoadingProgress: true
    };

    $scope.searchFilterNameValue = ''

    $scope.pagePerCount = [50, 100];
    $scope.inventoryStockModel.limit = $scope.pagePerCount[0];
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.searchFilter = {
        name: '',
        category_id:'',
        min_stock:''
    };

    $scope.clearFilters = function ()
    {
        $scope.searchFilter = {
            name: '',
            min_stock:'',
            category_name:''
        };
        //$scope.searchFilter.productInfo = {};
        //$scope.inventoryStockModel.list = [];
        $scope.initTableFilter();
    }
    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function ()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getInventoryStockListInfo, 300);
    }


    $scope.print = function ()
    {
        $window.print();
    };
    $scope.formatproductlistModel = function (model) {
        if (model != null)
        {
            return model.name;
        }
        return  '';
    };


    $scope.getproductlist = function (val) {

        var autosearchParam = {};
        autosearchParam.name = val;
        autosearchParam.type = "Product";
        return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData) {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.getcategorylist = function () {

        var autosearchParam = {};
        autosearchParam.name = '';
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getCategoryList(autosearchParam, configOption).then(function(response) {
            var data = response.data;
            $scope.inventoryStockModel.categorylist = data.list;
        });
    };
    $scope.getInventoryStockListInfo = function () {

        $scope.inventoryStockModel.isLoadingProgress = true;
        var materialListParam = {};
        var headers = {};
        headers['screen-code'] = 'inventorystock';
        materialListParam.id = '';
        materialListParam.product_name = $scope.searchFilter.name;
        materialListParam.category_id = $scope.searchFilter.category_name;
        materialListParam.type = $scope.searchFilter.min_stock;
        materialListParam.start = ($scope.inventoryStockModel.currentPage - 1) * $scope.inventoryStockModel.limit;
        materialListParam.limit = $scope.inventoryStockModel.limit;
        $scope.inventoryStockModel.start = materialListParam.start;
        adminService.getInventoryStockListInfo(materialListParam, headers).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.inventoryStockModel.list = data.list;
                for (var i = 0; i < $scope.inventoryStockModel.list.length; i++)
                {
                    $scope.inventoryStockModel.list[i].index = $scope.inventoryStockModel.start + i + 1;
                    
                        if(parseFloat($scope.inventoryStockModel.list[i].quantity) < parseFloat($scope.inventoryStockModel.list[i].min_stock_qty))
                        {
                            $scope.inventoryStockModel.list[i].isOrangeApplied = true;
                        }
                        else
                        {
                            $scope.inventoryStockModel.list[i].isOrangeApplied = false ;
                        }                     

                     
                }
                $scope.inventoryStockModel.total = data.total;
            }
            $scope.inventoryStockModel.isLoadingProgress = false;
        });

    };

    $scope.getInventoryStockListInfo();
    $scope.getcategorylist();

}]);




