


app.controller('clientListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'APP_CONST', 'ValidationFactory', function($scope, $rootScope, adminService, $httpService, $filter, Auth, $state, $timeout, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.clientModel = {
            currentPage: 1,
            id: '',
            total: 0,
            limit: 10,
            country: '',
            city: '',
            
            list: [],
            printList:[],
            status: '',
            serverList: null,
            isLoadingProgress: false,
            clientInfo: '',
            newAddedAmount: '',
            totalAmt: '',
            categoryList: []
        };

        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.clientModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            email: '',
            name: '',
            phone: '',
            category: '',
            city: '',
            familycode: '',
            customerOf:'',
            accno: ''
        };             
        $scope.print = function(){                  
             window.print();
        }

        $scope.showUpdateBalancePopup = false;
        $scope.showPopup = function() {

            $scope.showUpdateBalancePopup = true;
            $scope.clientModel.clientInfo = '';
            $scope.clientModel.newAddedAmount = '';

        };

        $scope.closePopup = function() {

            $scope.showUpdateBalancePopup = false;

        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getListPrint, 300);
        }

        $scope.formatClientModel = function(model) {

            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.getCategoryList = function() {
            var categoryListParam = {};
            categoryListParam.id = "";
            categoryListParam.name = "";
            categoryListParam.start = 0;
            categoryListParam.limit = 100;
            adminService.getCategoryList(categoryListParam).then(function(response) {
                var data = response.data.list;
                $scope.clientModel.categoryList = data;

            });

        };

        $scope.getClientList = function(val) {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.companyId = $rootScope.userModel.companyId;
            autosearchParam.mode = 1;
            autosearchParam.sort = 1;
            return $httpService.get(APP_CONST.API.GET_CLIENT_LIST, autosearchParam, false).then(function(responseData) {
                var data = responseData.data.data;
                var hits = data.list;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.update = function(isfilteropen)
        {

            $scope.searchFilter.accno = '';
            $scope.searchFilter.familycode = '';
            if (isfilteropen == false)
            {
                $scope.initTableFilter();
            }
        }

        $scope.getList = function(val) {

            var getListParam = {};
            getListParam.Id = "";
            getListParam.compId = $rootScope.userModel.companyId;
            getListParam.name = $scope.searchFilter.name;
            getListParam.phno = $scope.searchFilter.phone;
            getListParam.email = $scope.searchFilter.email;
            getListParam.country = '';
            getListParam.city = $scope.searchFilter.city;
            getListParam.categoryId = $scope.searchFilter.category;
            getListParam.state = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.start = ($scope.clientModel.currentPage - 1) * $scope.clientModel.limit;
            getListParam.limit = $scope.clientModel.limit;
            getListParam.familyCode = $scope.searchFilter.familycode;
            getListParam.customerOf = $scope.searchFilter.customerOf;
            getListParam.accountNumber = $scope.searchFilter.accno;
            $scope.clientModel.isLoadingProgress = true;
            adminService.getClientList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.clientModel.list = data;
                $scope.clientModel.total = response.data.total;
                $scope.clientModel.isLoadingProgress = false;
            });
        };
        
         $scope.getListPrint = function(val) {

            var getListParam = {};
            getListParam.Id = "";
            getListParam.compId = $rootScope.userModel.companyId;
            getListParam.name = $scope.searchFilter.name;
            getListParam.phno = $scope.searchFilter.phone;
            getListParam.email = $scope.searchFilter.email;
            getListParam.country = '';
            getListParam.city = $scope.searchFilter.city;
            getListParam.categoryId = $scope.searchFilter.category;
            getListParam.state = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.start = 0;
            //getListParam.limit = 0;commented because need to get the whole list, passing limit as 0 its response empty
            getListParam.familyCode = $scope.searchFilter.familycode;
            getListParam.accountNumber = $scope.searchFilter.accno;
            getListParam.customerOf = $scope.searchFilter.customerOf;
            $scope.clientModel.isLoadingProgress = true;
            adminService.getClientList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.clientModel.printList=data;              

                $scope.clientModel.isLoadingProgress = false;
            });

        };
        $scope.isupdateBalLoadingProgress = false;
        $scope.updateBalance = function() {

            var updateBalanceParam = {};
            updateBalanceParam.id = $scope.clientModel.clientInfo.id;
            updateBalanceParam.balanceAmount = $scope.clientModel.newAddedAmount;
            $scope.isupdateBalLoadingProgress = true;
            adminService.updateBalance(updateBalanceParam).then(function(response) {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.isupdateBalLoadingProgress = false;
                    $scope.closePopup();
                    $state.go('app.clientlist');
                    $scope.getList();
                }

            });

        };


       $scope.getList();
       $scope.getListPrint();
        $scope.getCategoryList();
    }]);








