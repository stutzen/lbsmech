


app.controller('dashboardListCtrl', ['$scope', '$rootScope', '$window', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function ($scope, $rootScope, $window, adminService, utilityService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.dashboardModel = {
            invoiceVsPaymentOption: {},
            invoiceVspurchasePaymentOption: {},
            incomeVsCategoryOption: {},
            expanseVsCategoryOption: {},
            incomeVsExpanseOption: {},
            invoicePurchasePaymentList: [],
            invoiceExpenseList: [],
            invoicePaymentList: {
                list: [],
                hasDataLoaded: false,
                isEmpty: false,
                hasDataLoadingProgress: false,
                chartData: [],
                date: []
            },
            invoicePurchasePaymentList: {
                list: [],
                hasDataLoaded: false,
                isEmpty: false,
                hasDataLoadingProgress: false,
                chartData: [],
                date: []
            },
            categoryIncomeSummary: {
                list: [],
                hasDataLoaded: false,
                isEmpty: false,
                hasDataLoadingProgress: false,
                chartData: []
            },
            categoryExpenseSummary: {
                list: [],
                hasDataLoaded: false,
                isEmpty: false,
                hasDataLoadingProgress: false,
                chartData: []
            },
            monthwiseSalesSummary: {
                list: [],
                hasDataLoaded: false,
                isEmpty: false,
                hasDataLoadingProgress: false,
                chartData: []
            },
            resentIncome: {
                list: [],
                isLoadingProgress: false
            },
            resentExpense: {
                list: [],
                isLoadingProgress: false
            },
            todayBalance: {
                list: [],
                isLoadingProgress: false
            },
            categoryExpenseSummaryList: [],
            isCategoryExpenseSumaryEmpty: false,
            isCategoryExpenseSumaryDataLoaded: false,
            activityList: [],
            countList: [],
            invoiceList: [],
            monthwiseSalesCount: '',
            receivableList: [],
            payableList: []
        };
        $scope.adminService = adminService;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.todayDate = new Date();
        $scope.monthcode = $filter('date')($scope.todayDate, 'MM');
        $scope.monthyear = $filter('date')($scope.todayDate, 'MMM yyyy');


        $scope.firstDay = moment().startOf('month');
        $scope.previousDay = new moment().subtract(6, 'months').date(1);

        $scope.startingMonthyear = $filter('date')(new Date($scope.previousDay), 'MMM yyyy');
        $scope.showPieChart = false;
        $scope.piechartSeries = [];
        $scope.drawChart = function (id)
        {
            if (id == 1)
            {
                var invoiceSeries = [];
                var paymentSeries = [];
                var date = [];
                var chartSeries = [];
                for (var i = 0; i < $scope.dashboardModel.invoicePaymentList.list.length; i++)
                {
                    invoiceSeries.push(parseInt($scope.dashboardModel.invoicePaymentList.list[i].invoice_amount));
                    paymentSeries.push(parseInt($scope.dashboardModel.invoicePaymentList.list[i].payment_amount));
                    date.push($scope.dashboardModel.invoicePaymentList.list[i].date);
                }
                chartSeries = [
                    {
                        name: 'Invoice',
                        data: invoiceSeries,
                        pointStart: 1,
                        color: '#00bfc7',
                        marker: {
                            lineWidth: 1,
                            lineColor: '#00bfc7'
                        }
                    },
                    {
                        name: 'Payment',
                        data: paymentSeries,
                        pointStart: 1,
                        color: '#b4becb',
                        marker: {
                            lineWidth: 1,
                            lineColor: '#b4becb'
                        }
                    }
                ]
                $scope.dashboardModel.invoicePaymentList.date = date;
                $scope.dashboardModel.invoicePaymentList.chartData = chartSeries;
                $timeout(function ()
                {
                    $scope.dashboardModel.invoicePaymentList.hasDataLoaded = true;
                }, 1000);

            }
            if (id == 2)
            {
                var invoiceSeries = [];
                var paymentSeries = [];
                var date = [];
                vchartSeries = [];
                for (var i = 0; i < $scope.dashboardModel.invoicePurchasePaymentList.list.length; i++)
                {
                    invoiceSeries.push(parseInt($scope.dashboardModel.invoicePurchasePaymentList.list[i].invoice_amount));
                    paymentSeries.push(parseInt($scope.dashboardModel.invoicePurchasePaymentList.list[i].payment_amount));
                    date.push($scope.dashboardModel.invoicePurchasePaymentList.list[i].date);
                }
                chartSeries = [
                    {
                        name: 'Invoice',
                        data: invoiceSeries,
                        pointStart: 1,
                        color: '#00bfc7'
                    },
                    {
                        name: 'Payment',
                        data: paymentSeries,
                        pointStart: 1,
                        color: '#b4becb'
                    }
                ]
                $scope.dashboardModel.invoicePurchasePaymentList.date = date;
                $scope.dashboardModel.invoicePurchasePaymentList.chartData = chartSeries;
                $timeout(function ()
                {
                    $scope.dashboardModel.invoicePurchasePaymentList.hasDataLoaded = true;
                }, 1000);
            }
            if (id == 3)
            {
                var incomeSeries = [];
                var expenseSeries = [];
                var month = [];
                for (i = 0; i < $scope.dashboardModel.invoiceExpenseList.length; i++)
                {
                    incomeSeries.push(parseFloat($scope.dashboardModel.invoiceExpenseList[i].Income));
                    expenseSeries.push(parseFloat($scope.dashboardModel.invoiceExpenseList[i].Expenses));
                    month.push($scope.dashboardModel.invoiceExpenseList[i].month);

                }
                $scope.dashboardModel.incomeVsExpanseOption = {
                    options: {
                        chart: {
                            type: 'column'
                        },
                        plotOptions: {
                            series: {
                                stacking: ''
                            }
                        },
                        xAxis: {
                            categories: month,
                            crosshair: true
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Amount'
                        }
                    },
                    series: [
                        {
                            name: 'Income',
                            data: incomeSeries,
                            pointStart: 0,
                            color: '#b3fff0'
                        },
                        {
                            name: 'Expense',
                            data: expenseSeries,
                            pointStart: 0,
                            color: '#ffc299'
                        }
                    ],
                    title: {
                        text: 'Income & Expense For Last 6 Months'
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    loading: false,
                    size: {}

                };
            }
            if (id == 4)
            {

                var chartSeries = [];

                for (var i = 0; i < $scope.dashboardModel.categoryIncomeSummary.list.length; i++)
                {
                    $scope.amountItem = 0;
                    $scope.nameItem = '';
                    $scope.amountItem = parseFloat($scope.dashboardModel.categoryIncomeSummary.list[i].amount);
                    $scope.nameItem = $scope.dashboardModel.categoryIncomeSummary.list[i].account_category;
                    chartSeries.push({"name": $scope.nameItem, "y": $scope.amountItem});
                }

                $scope.dashboardModel.categoryIncomeSummary.chartData = chartSeries;
                $timeout(function ()
                {
                    $scope.dashboardModel.categoryIncomeSummary.hasDataLoaded = true;
                }, 1000);
            }
            if (id == 5)
            {

                var chartSeries = [];

                for (var i = 0; i < $scope.dashboardModel.categoryExpenseSummary.list.length; i++)
                {
                    $scope.amountItem = 0;
                    $scope.nameItem = '';
                    $scope.amountItem = parseFloat($scope.dashboardModel.categoryExpenseSummary.list[i].amount);
                    $scope.nameItem = $scope.dashboardModel.categoryExpenseSummary.list[i].account_category;
                    chartSeries.push({"name": $scope.nameItem, "y": $scope.amountItem});
                }

                $scope.dashboardModel.categoryExpenseSummary.chartData = chartSeries;
                $timeout(function ()
                {
                    $scope.dashboardModel.categoryExpenseSummary.hasDataLoaded = true;
                }, 1000);
            }


        }

        $scope.drawSparkline = function ()
        {
            var values = [];
            for (var i = 0; i < $scope.dashboardModel.monthwiseSalesSummary.list.length; i++)
            {
                values.push($scope.dashboardModel.monthwiseSalesSummary.list[i].value);
            }
            var l = values.length;
            var pixel_width = parseInt((200 - l) / l);
            // Draw a sparkline for the #sparkline element
            $('#sparkline').sparkline(values, {
                type: "bar",
                "barWidth": pixel_width,
                "height": 150
            });

        }
        $scope.chartSeries = [{
                name: "Microsoft Internet Explorer",
                y: 56.33
            }, {
                name: "Chrome",
                y: 24.03,
                sliced: true,
                selected: true
            }, {
                name: "Firefox",
                y: 10.38}, {
                name: "Safari",
                y: 4.77
            }, {
                name: "Opera",
                y: 0.91
            }, {
                name: "Proprietary or Undetectable",
                y: 0.2
            }];

        $scope.dashboardModel.incomeVsCategoryOption = {
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Income Vs Expense'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                    data: $scope.chartSeries
                }]

        };
        $scope.print = function ()
        {
            $window.print('app.dashboardprint');
        }
        $scope.showinvoicepayment = false;
        $scope.showpurchaseinvoicepayment = false;
        $scope.showinvoiceexpense = false;
        $scope.showcategoryincome = false;
        $scope.showcategoryexpense = false;
        $scope.getInvoicePaymentList = function () {

            $scope.dashboardModel.invoicePaymentList.isDataProcessing = true;
            var getListParam = {};
            adminService.getInvoicePaymentList(getListParam, 90).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.invoicePaymentList.list = data.list;
                    $scope.dashboardModel.invoicePaymentList.isDataProcessing = false;
                    $scope.dashboardModel.invoicePaymentList.isEmpty = false;
                    $scope.drawChart(1);
                } else
                {
                    $scope.getInvoicePaymentJson();
                }
            });
        };

        $scope.getInvoicePaymentJson = function () {

            var getListParam = {};
            adminService.getInvoicePaymentJson(getListParam, 90).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.invoicePaymentList.list = data.list;
                    $scope.drawChart(1);
                    $scope.showinvoicepayment = true;
                    $scope.dashboardModel.invoicePaymentList.isDataProcessing = false;

                }
            });
        };
        $scope.getInvoicePurchasePaymentList = function () {

            $scope.dashboardModel.invoicePurchasePaymentList.isDataProcessing = true;
            var getListParam = {};
            adminService.getInvoicePurchasePaymentList(getListParam, 90).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.invoicePurchasePaymentList.list = data.list;
                    $scope.dashboardModel.invoicePurchasePaymentList.isDataProcessing = false;
                    $scope.dashboardModel.invoicePaymentList.isEmpty = false;
                    $scope.drawChart(2);
                } else
                {
                    $scope.getInvoicePurchasePaymentJson();
                }
            });
        };
        $scope.getInvoicePurchasePaymentJson = function () {

            var getListParam = {};
            adminService.getInvoicePurchasePaymentJson(getListParam, 90).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.invoicePurchasePaymentList.list = data.list;
                    $scope.drawChart(2);
                    $scope.dashboardModel.invoicePurchasePaymentList.isDataProcessing = false;
                }
            });
        };
        $scope.getInvoiceExpensePaymentList = function () {

            $scope.dashboardModel.isInvoiceExpenseLoadingProgress = true;
            var getListParam = {};
            adminService.getInvoiceExpensePaymentList(getListParam, 5).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.invoiceExpenseList = data.list;
                    $scope.dashboardModel.isInvoiceExpenseLoadingProgress = false;
                    $scope.drawChart(3);
                } else
                {

                    $scope.getInvoiceExpensePaymentJson();
                }
            });
        };
        $scope.getInvoiceExpensePaymentJson = function () {

            var getListParam = {};
            adminService.getInvoiceExpensePaymentJson(getListParam, 6).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.invoiceExpenseList = data.list;
                    $scope.drawChart(3);
                    $scope.showinvoiceexpense = true;
                    $scope.dashboardModel.isInvoiceExpenseLoadingProgress = false;

                }
            });
        };
        $scope.getCategoryIncomeSummaryList = function () {

            $scope.dashboardModel.categoryIncomeSummary.isDataProcessing = true;
            var getListParam = {};
            adminService.getCategoryIncomeSummaryList(getListParam, 180).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.categoryIncomeSummary.list = data.list;
                    $scope.dashboardModel.categoryIncomeSummary.isDataProcessing = false;
                    $scope.dashboardModel.categoryIncomeSummary.isEmpty = false;
                    $scope.drawChart(4);
                } else
                {
                    $scope.getCategoryIncomeSummaryJson();
                }
            });
        };
        $scope.getCategoryIncomeSummaryJson = function () {

            var getListParam = {};
            adminService.getCategoryIncomeSummaryJson(getListParam, 180).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.categoryIncomeSummary.list = data.list;
                    $scope.dashboardModel.categoryIncomeSummary.isDataProcessing = false;
                    $scope.dashboardModel.categoryIncomeSummary.isEmpty = true;
                    $scope.drawChart(4);
                }
            });
        };
        $scope.getCategoryExpenseSummaryList = function () {

            $scope.dashboardModel.categoryExpenseSummary.isDataProcessing = true;

            var getListParam = {};
            adminService.getCategoryExpenseSummaryList(getListParam, 180).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.categoryExpenseSummary.list = data.list;
                    $scope.dashboardModel.categoryExpenseSummary.isDataProcessing = false;
                    $scope.dashboardModel.categoryExpenseSummary.isEmpty = false;
                    $scope.drawChart(5);
                } else
                {
                    $scope.getCategoryExpenseSummaryJson();
                }
            });
        };
        $scope.getCategoryExpenseSummaryJson = function () {

            var getListParam = {};
            adminService.getCategoryExpenseSummaryJson(getListParam, 180).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.categoryExpenseSummary.list = data.list;
                    $scope.dashboardModel.categoryExpenseSummary.isDataProcessing = false;
                    $scope.dashboardModel.categoryExpenseSummary.isEmpty = true;

                    $scope.drawChart(5);

                }
            });
        };

        $scope.getActivityList = function () {

            var getListParam = {};
            getListParam.start = 0;
            getListParam.limit = 4;
            var headers = {};
            headers['screen-code'] = 'dashboard';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getActivityList(getListParam, configOption, headers).then(function (response) {
                var data = response.data;
                if (data.success && data.list.length != 0)
                {
                    $scope.dashboardModel.activityList = data.list;
                    for (var i = 0; i < $scope.dashboardModel.activityList.length; i++)
                    {
                        $scope.dashboardModel.activityList[i].newdate = utilityService.convertToUTCDate($scope.dashboardModel.activityList[i].date);
                        $scope.dashboardModel.activityList[i].date1 = utilityService.parseDateToStr($scope.dashboardModel.activityList[i].newdate, 'MMMM-dd-yyyy');
                        $scope.dashboardModel.activityList[i].date2 = utilityService.parseDateToStr($scope.dashboardModel.activityList[i].newdate, 'dd-MM-yyyy HH:MM:ss a');
                    }
                }
            });
        };
        $scope.getSalesPurchaseMonthlyCount = function () {

            var getListParam = {};

            adminService.getSalesPurchaseMonthlyCount(getListParam, $scope.monthcode).then(function (response) {
                var data = response.data;
                if (data.success && data.length != 0)
                {
                    $scope.dashboardModel.countList = data;
                    if ($scope.dashboardModel.countList.sales.total != null)
                    {
                        $scope.dashboardModel.countList.sales.total = utilityService.changeCurrency($scope.dashboardModel.countList.sales.total, $rootScope.appConfig.thousand_seperator);
                    } else
                    {
                        $scope.dashboardModel.countList.sales.total = '0.00';
                    }
                    if ($scope.dashboardModel.countList.purchase.total != null)
                    {
                        $scope.dashboardModel.countList.purchase.total = utilityService.changeCurrency($scope.dashboardModel.countList.purchase.total, $rootScope.appConfig.thousand_seperator);
                    } else
                    {
                        $scope.dashboardModel.countList.purchase.total = '0.00';
                    }
                    if ($scope.dashboardModel.countList.payment.total != null)
                    {
                        $scope.dashboardModel.countList.payment.total = utilityService.changeCurrency($scope.dashboardModel.countList.payment.total, $rootScope.appConfig.thousand_seperator);
                    } else
                    {
                        $scope.dashboardModel.countList.payment.total = '0.00';
                    }
                    if ($scope.dashboardModel.countList.purchasepayment.total != null)
                    {
                        $scope.dashboardModel.countList.purchasepayment.total = utilityService.changeCurrency($scope.dashboardModel.countList.purchasepayment.total, $rootScope.appConfig.thousand_seperator);
                    } else
                    {
                        $scope.dashboardModel.countList.purchasepayment.total = '0.00';
                    }
                }
            });
        };

        $scope.getinvoiceListInfo = function ()
        {
            var getListParam = {};
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.mode = 0;
            getListParam.start = 0;
            getListParam.limit = 5;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getInvoiceList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dashboardModel.invoiceList = data.list;
                    for (var i = 0; i < $scope.dashboardModel.invoiceList.length; i++)
                    {
                        $scope.dashboardModel.invoiceList[i].newdate = utilityService.convertToUTCDate($scope.dashboardModel.invoiceList[i].date);
                        $scope.dashboardModel.invoiceList[i].date = utilityService.parseDateToStr($scope.dashboardModel.invoiceList[i].newdate, $scope.adminService.appConfig.date_format);
                        $scope.dashboardModel.invoiceList[i].newduedate = utilityService.convertToUTCDate($scope.dashboardModel.invoiceList[i].duedate);
                        $scope.dashboardModel.invoiceList[i].duedate = utilityService.parseDateToStr($scope.dashboardModel.invoiceList[i].newduedate, $scope.adminService.appConfig.date_format);
                        $scope.dashboardModel.invoiceList[i].paid_amount = utilityService.changeCurrency($scope.dashboardModel.invoiceList[i].paid_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.dashboardModel.invoiceList[i].total_amount = utilityService.changeCurrency($scope.dashboardModel.invoiceList[i].total_amount, $rootScope.appConfig.thousand_seperator);
                    }
                }
            });
        };
        $scope.invoiceView = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {'id': id}, {'reload': true});
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {'id': id}, {'reload': true});
            } else
            {
                $state.go('app.invoiceView1', {'id': id}, {'reload': true});
            }
        };
        $scope.getdepositList = function ()
        {
            $scope.dashboardModel.resentIncome.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = '';
            getListParam.start = 0;
            getListParam.limit = 5;
            getListParam.is_active = 1;

            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDepositList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.dashboardModel.resentIncome.list = data;

                    for (var i = 0; i < $scope.dashboardModel.resentIncome.list.length; i++)
                    {
                        $scope.dashboardModel.resentIncome.list[i].newdate = utilityService.convertToUTCDate($scope.dashboardModel.resentIncome.list[i].transaction_date);
                        $scope.dashboardModel.resentIncome.list[i].transaction_date = utilityService.parseDateToStr($scope.dashboardModel.resentIncome.list[i].newdate, $scope.adminService.appConfig.date_format);
                        $scope.dashboardModel.resentIncome.list[i].amount = utilityService.changeCurrency($scope.dashboardModel.resentIncome.list[i].amount, $rootScope.appConfig.thousand_seperator);

                    }
                    $scope.dashboardModel.total = data.total;
                }
                $scope.dashboardModel.resentIncome.isLoadingProgress = false;
            });

        };

        $scope.getexpenseList = function ()
        {
            $scope.dashboardModel.resentIncome.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = '';
            getListParam.start = 0;
            getListParam.limit = 5;
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getTransexpenseList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.dashboardModel.resentExpense.list = data;

                    for (var i = 0; i < $scope.dashboardModel.resentExpense.list.length; i++)
                    {
                        $scope.dashboardModel.resentExpense.list[i].newdate = utilityService.convertToUTCDate($scope.dashboardModel.resentExpense.list[i].transaction_date);
                        $scope.dashboardModel.resentExpense.list[i].transaction_date = utilityService.parseDateToStr($scope.dashboardModel.resentExpense.list[i].newdate, $scope.adminService.appConfig.date_format);
                        $scope.dashboardModel.resentExpense.list[i].amount = utilityService.changeCurrency($scope.dashboardModel.resentExpense.list[i].amount, $rootScope.appConfig.thousand_seperator);

                    }

                    $scope.dashboardModel.total = data.total;
                }
                $scope.dashboardModel.resentIncome.isLoadingProgress = false;
            });

        };
        $scope.gettodayBalancelist = function ()
        {
            $scope.dashboardModel.todayBalance.isLoadingProgress = true;
            var getListParam = {};
            adminService.gettodayBalancelist(getListParam).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dashboardModel.todayBalance = data;

                    $scope.dashboardModel.todayBalance.balance = utilityService.changeCurrency($scope.dashboardModel.todayBalance.balance, $rootScope.appConfig.thousand_seperator);
                    $scope.dashboardModel.todayBalance.today_income = parseFloat($scope.dashboardModel.todayBalance.today_income).toFixed(2);
                    $scope.dashboardModel.todayBalance.today_expense = parseFloat($scope.dashboardModel.todayBalance.today_expense).toFixed(2);
                    $scope.dashboardModel.todayBalance.today_income = utilityService.changeCurrency($scope.dashboardModel.todayBalance.today_income, $rootScope.appConfig.thousand_seperator);
                    $scope.dashboardModel.todayBalance.today_expense = utilityService.changeCurrency($scope.dashboardModel.todayBalance.today_expense, $rootScope.appConfig.thousand_seperator);

                    $scope.dashboardModel.todayBalance.month_income = utilityService.changeCurrency($scope.dashboardModel.todayBalance.month_income, $rootScope.appConfig.thousand_seperator);
                    $scope.dashboardModel.todayBalance.month_expense = utilityService.changeCurrency($scope.dashboardModel.todayBalance.month_expense, $rootScope.appConfig.thousand_seperator);

                    $scope.dashboardModel.total = data.total;
                }
                $scope.dashboardModel.todayBalance.isLoadingProgress = false;
            });

        };

        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "invoice";

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                if (data.length != 0)
                {
                    $scope.customAttributeList = data;
                }
            });
        };

        $scope.getMonthwiseSalesList = function () {

            $scope.dashboardModel.monthwiseSalesSummary.isDataProcessing = true;

            var getListParam = {};
            getListParam.from_date = utilityService.parseDateToStr($scope.previousDay, 'dd-MM-yyyy');
            getListParam.to_date = utilityService.parseDateToStr($scope.firstDay, 'dd-MM-yyyy');
            adminService.getMonthwiseSalesList(getListParam).then(function (response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.dashboardModel.monthwiseSalesSummary.list = data.list;
                    $scope.dashboardModel.monthwiseSalesSummary.isDataProcessing = false;
                    $scope.dashboardModel.monthwiseSalesSummary.isEmpty = false;
                    $scope.drawSparkline();
                }
            });
        };

        $scope.getSalesAgeingDetailProcessing = false;
        $scope.getSalesAgeingDetail = function () {

            $scope.getSalesAgeingDetailProcessing = true;
            var getListParam = {};
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getSalesAgeingDetail(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                if (data.receivable_list.length != 0)
                {
                    $scope.dashboardModel.receivableList = data.receivable_list;
                }
                if (data.payable_list.length != 0)
                {
                    $scope.dashboardModel.payableList = data.payable_list;
                }
            });
            $scope.getSalesAgeingDetailProcessing = false;

        };
        $scope.getMonthwiseSalesCount = function () {
            var getListParam = {};
            adminService.getMonthwiseSalesCount(getListParam, $scope.monthcode).then(function (response) {
                var data = response.data;
                $scope.dashboardModel.monthwiseSalesCount = data;
                $scope.dashboardModel.monthwiseSalesCount.amount = utilityService.changeCurrency($scope.dashboardModel.monthwiseSalesCount.amount, $rootScope.appConfig.thousand_seperator);
            });
        };

        $scope.getinvoiceListInfo();
        $scope.getCustomAttributeList();
        $scope.getInvoiceExpensePaymentList();
        $scope.getInvoicePaymentList();
        $scope.getInvoicePurchasePaymentList();
        $scope.getCategoryIncomeSummaryList();
        $scope.getCategoryExpenseSummaryList();
        $scope.getActivityList();
        $scope.getSalesPurchaseMonthlyCount();
        $scope.getMonthwiseSalesList();
        $scope.getMonthwiseSalesCount();
        $scope.getdepositList();
        $scope.getexpenseList();
        $scope.gettodayBalancelist();
        $scope.getSalesAgeingDetail();
    }]);








