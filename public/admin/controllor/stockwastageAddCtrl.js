
app.controller('stockwastageAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.orderListModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "uom_id": "",
                    "qty": "",
                    "isActive": "1",
                    "taxInfo": "",
                    list: [],
                    "uomInfo": {},
                    "customerInfo": {},
                    "productList": [],
                    "productId": ''

                };

        $scope.onFocus = function(e) {
            $timeout(function() {
                $(e.target).trigger('input');
            });
        };

        $scope.showItems = false;
        $scope.showAreaPopup = function()
        {
            $scope.showItems = true;
        }

        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.order_add_form != 'undefined' && typeof $scope.order_add_form.$pristine != 'undefined' && !$scope.order_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.order_add_form.$setPristine();
            $scope.orderListModel.id = "";
            $scope.orderListModel.productname = "";
            $scope.orderListModel.qty = '';
            $scope.orderListModel.productList = "";
            $scope.orderListModel.productId = "";
            $scope.orderListModel.uom_id = "";
            $scope.orderListModel.sku = "";
            $scope.orderListModel.product_id = "";
            $scope.orderListModel.taxInfo = "";
            $scope.orderListModel.uomInfo = "";
            $scope.orderListModel.length = "";
            $scope.orderListModel.isActive = true;
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.getUomList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.isLoadedTax = false;
        $scope.getTaxListInfo = function( )
        {
            $scope.isLoadedTax = false;
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = 0;
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaxList(getListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.orderListModel.taxInfo = data;
                $scope.orderListModel.tax_id = $scope.orderListModel.taxInfo[0].id + '';
                $scope.isLoadedTax = true;
            });
        };
        $scope.deleteProduct = function(id)
        {
            var index = -1;
            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
                if (id == $scope.orderListModel.list[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.updateStockwastageTotal();
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateStockwastageTotal = function()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                $scope.getCustomerList();
            }
            if ($scope.isLoadedTax == true && $stateParams.id != "")
            {
                $scope.updateStockwastageDetails();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.getCustomerList, 300);
                //$scope.getCustomerList();
            }

        }
        $scope.formatProductModel = function(model, index)
        {
            if (model != null)
            {
                $scope.updateProductInfo(model, index);
                return model.name;
            }
            return  '';
        }
        $scope.updateProductInfo = function(model, index)
        {
            if ($scope.orderListModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.orderListModel.list[index].id = model.id;
                $scope.orderListModel.list[index].sku = model.sku;
                $scope.orderListModel.list[index].sales_price = model.sales_price;
                $scope.orderListModel.list[index].qty = 1;
                $scope.orderListModel.list[index].rowtotal = model.sales_price;
                $scope.orderListModel.list[index].discountPercentage = model.discountPercentage;
                $scope.orderListModel.list[index].taxPercentage = model.taxPercentage;
                $scope.orderListModel.list[index].uomInfo = {
                    "name": model.uom,
                    "id": model.uom_id
                }
                $scope.orderListModel.list[index].uomid = model.uom_id;
                $scope.updateStockwastageTotal();

                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.orderListModel.list.length);
                $scope.createNewProduct(index);
                return;
            }
            else
            {
                $scope.createNewProduct(-1);
            }
        }
        $scope.initvalidateProductDataAndAddNewPromise = null;

        $scope.initValidateProductDataAndAddNew = function(index)
        {

            if ($scope.initvalidateProductDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                    function() {
                        $scope.createNewProduct(index);
                    },
                    300);
        };

        $scope.formValidator = function()
        {
            //performace issue in form validator bug
            if ($scope.order_add_form.$submitted)
            {
                $timeout(function() {
                    $scope.order_add_form.$submitted = false;
                }, 0);
                $timeout(function() {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            }
            else
            {
                $timeout(function() {
                    $scope.order_add_form.$setSubmitted();
                }, 100);
            }
        }

        $scope.createNewProduct = function(index)
        {
            var formDataError = false;
            var productContainer = window.document.getElementById('invoice_product_container');
            var errorCell = angular.element(productContainer).find('.has-error').length;
            if (errorCell > 0)
            {
                formDataError = true;
            }
            if (!formDataError)
            {
                if (index == $scope.orderListModel.list.length - 1 && !$scope.deleteEvent)
                {

                    var newRow = {
                        "id": '',
                        "productName": '',
                        "sku": '',
                        "sales_price": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "taxPercentage": '',
                        "uom": '',
                        "uomid": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": ''
                    }
                    $scope.orderListModel.list.push(newRow);
                }
                $scope.deleteEvent = false;
            }

        }

        $scope.deleteEvent = false;
        $scope.deleteProduct = function(id)
        {
            var index = -1;
            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
                if (id == $scope.orderListModel.list[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.deleteEvent = true;
            // $scope.updateStockwastageTotal();
        };
        $scope.$on("updateSelectedProductEvent", function(event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }

                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    console.log("upd 4");
                    $scope.updateStockwastageTotal();
                }

                else
                {
                    var newRow = {
                        "id": selectedItems.id,
                        "productName": selectedItems.name,
                        "sku": selectedItems.sku,
                        "sales_price": selectedItems.sales_price,
                        "qty": 1,
                        "rowtotal": selectedItems.sales_price,
                        "discountPercentage": selectedItems.discountPercentage,
                        "taxPercentage": selectedItems.taxPercentage,
                        "uom": selectedItems.uom,
                        "uomid": selectedItems.uom_id
                    }
                    $scope.orderListModel.list.push(newRow);
                    console.log("upd 2");
                    $scope.updateStockwastageTotal();
                    return;
                }
            }
            else
            {
                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "sales_price": selectedItems.sales_price,
                    "qty": 1,
                    "rowtotal": selectedItems.sales_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom,
                    "uomid": selectedItems.uom_id
                }
                $scope.orderListModel.list.push(newRow);
                $scope.updateStockwastageTotal();
            }
        });
        $scope.getProductList = function()
        {
            $scope.orderListModel.isLoadingProgress = true;
            var getParam = {};

            getParam.is_active = 1;

            getParam.has_inventory = 1;
            getParam.localData = 1;

            if (typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
            {
                getParam.scopeId = $scope.selectedAssignFrom.id;
            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getProductList(getParam, configOption).then(function(response) {
                var data = response.data;
                $scope.orderListModel.productList = data.list;
                $scope.orderListModel.isLoadingProgress = false;
            });
        }

        $scope.checkId = function()
        {
            if (typeof $stateParams.id != 'undefined' && $stateParams.id != null && $stateParams.id != '')
            {
                $scope.getStockwastageInfo();
            }
        }
        $scope.getTaxListInfo();
        if (typeof $stateParams.id == 'undefined' || $stateParams.id == null || $stateParams.id == '')
        {
            $scope.updateProductInfo();
        }
        $scope.getProductList( );

        $scope.createOrder = function(val)
        {
            if (!$scope.isDataSavingProcess)
            {
                $scope.isDataSavingProcess = true;
                $scope.isSave = false;
                for (var i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (i != $scope.orderListModel.list.length - 1)
                    {
                        if ($rootScope.appConfig.uomdisplay)
                        {
                            if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                    $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != null &&
                                    $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null && $scope.orderListModel.list[i].qty > 0)
//                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
//                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                            {
                                $scope.isSave = true;
                            }
                            else
                            {
                                $scope.isSave = false;
                                 break;
                            }
                        }
                        else
                        {
                            if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                    $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null && $scope.orderListModel.list[i].qty > 0)
//                                $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
//                                $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                            {
                                $scope.isSave = true;
                            }
                            else
                            {
                                $scope.isSave = false;
                                 break;
                            }
                        }
                    }
                }
                if ($scope.isSave)
                {
                    var createOrderParam = {};
                    var headers = {};
                    headers['screen-code'] = 'stockwastage';
                    createOrderParam.product = [];
                    createOrderParam.product.orderDetails = [];
                    for (var i = 0; i < $scope.orderListModel.list.length; i++)
                    {
                        if ($scope.orderListModel.list[i].sku != "")
                        {
                            var ordereditems = {};
                            ordereditems.product_id = $scope.orderListModel.list[i].id;
                            ordereditems.comments = '';
                            ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                            if ($scope.orderListModel.list[i].productname.id != null && $scope.orderListModel.list[i].productname.id != '')
                            {
                                ordereditems.product_id = $scope.orderListModel.list[i].productname.id;
                            }
                            else
                            {
                                ordereditems.product_id = '';
                            }
                            ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty);
                            if ($scope.orderListModel.list[i].uomInfo.id != null && $scope.orderListModel.list[i].uomInfo.id != '')
                            {
                                ordereditems.uom_id = $scope.orderListModel.list[i].uomInfo.id;
                            }
                            else
                            {
                                ordereditems.uom_id = $scope.orderListModel.list[i].uomInfo.id;
                            }
                            //ordereditems.uom_id = $scope.orderListModel.list[i].uomid;
                            createOrderParam.product.push(ordereditems);
                        }
                    }
                    adminService.addStockwastage(createOrderParam, headers).then(function(response) {
                        if (response.data.success == true)
                        {

                            $scope.formReset();
                            $state.go('app.stockwastage');
                        }
                        $scope.isDataSavingProcess = false;
                    });
                }
                else
                {
                    sweet.show('Oops...', 'Fill Product Detail..', 'error');
                    $scope.isDataSavingProcess = false;
                }
            }
        }
    }]);




