




app.controller('qualityCtrlPointsAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', 'sweet', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, sweet, $stateParams, $timeout, ValidationFactory) {

            $scope.qualityModel = {
                  "title": "",
                  "is_active": 1,
                  "operation": "",
                  "empInfo": "",
                  "comments": "",
                  "instruction": "",
                  "productInfo": ""
            };
            $scope.validationFactory = ValidationFactory;
            $scope.adminService = adminService;
            $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
            $scope.getNavigationBlockMsg = function (pageReload)
            {
                  if (typeof $scope.create_qcp_form !== 'undefined' && typeof $scope.create_qcp_form.$pristine !== 'undefined' && !$scope.create_qcp_form.$pristine)
                  {
                        return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
                  }

                  return "";
            };
            $scope.isDataSavingProcess = false;
            $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
            $scope.formReset = function ()
            {
                  $scope.create_qcp_form.$setPristine();
                  $scope.qualityModel = {
                    "title": "",
                    "is_active": 1,
                    "operation": "",
                    "empInfo": "",
                    "comments": "",
                    "instruction": "",
                    "productInfo": ""
                };
            };
            $scope.getEmployeeList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.fname = val;
                  autosearchParam.is_active = 1;
                  if (autosearchParam.fname != '')
                  {
                        return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
                        {
                              var data = responseData.data.list;
                              var hits = data;
                              if (hits.length > 10)
                              {
                                    hits.splice(10, hits.length);
                              }
                              return hits;
                        });
                  }
            };

            $scope.formatEmployeeModel = function (model)
            {
                  if (model != null)
                  {
                        if (model.f_name != undefined && model.id != undefined && model.emp_code != undefined)
                        {
                              return model.f_name + '(' + model.id + ',' + model.emp_code + ')';
                        } else if (model.f_name != undefined && model.id != undefined)
                        {
                              return model.f_name + '(' + model.id + ')';
                        }
                  }
                  return  '';
            };

            $scope.getProductList = function (val)
            {
                var autoSearchParam = {};
                autoSearchParam.search = val;
                return $httpService.get(APP_CONST.API.PRODUCT_LIST, autoSearchParam, false).then(function (response)
                {
                      var data = response.data.list;
                      var hits = data;
                      if (hits.length > 10)
                      {
                            hits.splice(10, hits.length);
                      }
                      return hits;
                });
                  
            }
            $scope.formatProductModel = function (model)
            {
                  if (model != null)
                  {
                         return model.name;
                  }
                  return  '';
            };
            $scope.createQcp = function ()
            {
                $scope.isDataSavingProcess = true;
                 var QcpParam = {};
                 var headers = {};
                 headers['screen-code'] = 'qualitycontrolpoints';
                 QcpParam.title = $scope.qualityModel.title;
                 QcpParam.type = $scope.qualityModel.operation;
                 QcpParam.responsible_by = $scope.qualityModel.empInfo.id;
                 QcpParam.product_id = $scope.qualityModel.productInfo.id;
                 QcpParam.instructions = $scope.qualityModel.instructions;
                 QcpParam.comments = $scope.qualityModel.comments;
                 var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                 adminService.createQcp(QcpParam,configOption, headers).then(function (response) {
                       if (response.data.success === true)
                       {
                             $scope.formReset();
                             $state.go('app.qualitycontrolpoints');
                       }
                       $scope.isDataSavingProcess = false;
                 });
                  
            };
      }
]);




