app.controller('productiontaskCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', '$window', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, $window) {

    $rootScope.getNavigationBlockMsg = null;
    $scope.productionTaskModel = {
        currentPage: 1,
        total: 0,
        limit: 4,
        list: [],
        status: '',
        isLoadingProgress: true
    };
    $scope.validationFactory = ValidationFactory;
    $scope.pagePerCount = [50, 100];
    $scope.productionTaskModel.limit = $scope.pagePerCount[0];
    $scope.adminService = adminService;
    $scope.searchFilter =
    {
        productInfo: {},
        taskInfo: {},
        name: '',
        taskcode: '',
        qty: '',
        outputProducts: '',
        outputqty: '',
        outputupp: '',
        status : ''
    };

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }

    $scope.refreshScreen = function()
    {
        $scope.searchFilter =
        {
            productInfo: {},
            taskInfo: {},
            name: '',
            taskcode: '',
            status : ''
        };
        $scope.initTableFilter();
    }
    $scope.selectedtaskId = '';
    $scope.showdeletePopup = false;
    $scope.isdeleteProgress = false;
    $scope.showDetail = false;
    $scope.showPopup = function(id)
    {
        $scope.selectedtaskId = id;
        $scope.showdeletePopup = true;
    };

    $scope.closePopup = function()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
    };

    $scope.deleteProductionTask = function( )
    {
        if ($scope.isdeleteProgress == false)
        {
            $scope.isdeleteProgress = true;

            var getListParam = {};
            getListParam.id = $scope.selectedtaskId;
            var headers = {};
            headers['screen-code'] = 'billofmaterial';
            adminService.deleteProductionTask(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }

                $scope.isdeleteProgress = false;
            });

        }

    };

    $scope.$watch('searchFilter.productInfo', function(newVal, oldVal)
    {
        if (typeof newVal == 'undefined' || newVal == null || newVal == '')
        {
            $scope.initTableFilter();
        }
    });

    $scope.$watch('searchFilter.taskInfo', function(newVal, oldVal)
    {
        if (typeof newVal == 'undefined' || newVal == null || newVal == '')
        {
            $scope.initTableFilter();
        }
    });

    $scope.getCustomerList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        if (autosearchParam.name != '')
        {
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }
    };

    $scope.formatProductModel = function(model)
    {
        if (model != null)
        {
            return model.name;
        }
        return  '';
    };

    $scope.getProductList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };

    $scope.formatTaskModel = function(model)
    {
        if (model != null)
        {
            return model.name;
        }
        return  '';
    };

    $scope.getTaskList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        return $httpService.get(APP_CONST.API.PRODUCTION_TASK_LIST, autosearchParam, false).then(function(responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };

    $scope.getList = function()
    {
        $scope.productionTaskModel.isLoadingProgress = true;
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'billofmaterial';

        var configOption = adminService.handleOnlyErrorResponseConfig;
        if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
        {
            getListParam.output_pdt_id = $scope.searchFilter.productInfo.id;
        }
        else
        {
            getListParam.output_pdt_id = '';
        }
        if ($scope.searchFilter.taskInfo != null && typeof $scope.searchFilter.taskInfo != 'undefined' && typeof $scope.searchFilter.taskInfo.id != 'undefined')
        {
            getListParam.prev_bom_id = $scope.searchFilter.taskInfo.id;
        }
        else
        {
            getListParam.prev_bom_id = '';
        }
        getListParam.is_active = $scope.searchFilter.status === '' ? 1 : 0;
        getListParam.start = ($scope.productionTaskModel.currentPage - 1) * $scope.productionTaskModel.limit;
        getListParam.limit = $scope.productionTaskModel.limit;
        getListParam.name = $scope.searchFilter.name;
        getListParam.bom_code = $scope.searchFilter.taskcode;
        adminService.getProdTaskDetail(getListParam, configOption, headers).then(function(response)
        {
            var data = response.data;
            if (data.success)
            {
                $scope.productionTaskModel.list = data.list;
                $scope.productionTaskModel.total = data.total;
                    
                for (var i = 0; i < $scope.productionTaskModel.list.length; i++)
                {
                    $scope.productionTaskModel.list[i].production_price = parseFloat($scope.productionTaskModel.list[i].unit_production_price).toFixed(2);
                    $scope.productionTaskModel.list[i].unit_production_price = utilityService.changeCurrency($scope.productionTaskModel.list[i].production_price, $rootScope.appConfig.thousand_seperator);
                            
                }
            }
            $scope.productionTaskModel.isLoadingProgress = false;
        });
    };
    $scope.print = function()
    {
        $window.print();
    };
    $scope.getList();
}]);




