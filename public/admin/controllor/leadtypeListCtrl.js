

app.controller('leadtypeListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.leadModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            companyList: [],
           is_active:1,
            isLoadingProgress: true

        };
        $scope.pagePerCount = [50, 100];
        $scope.leadModel.limit = $scope.pagePerCount[0];
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
         $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                lead_name: '',
                type:''
            };
            $scope.initTableFilter();
        }
         $scope.searchFilter = {
                lead_name: '',
                type:''
            }
        
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getLeadtypeList, 300);
        };
        $scope.showPopup = function(id)
        {
            $scope.selectLeadId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteLeadInfo = function()
        {
            if($scope.isdeleteProgress == false)
            {
               $scope.isdeleteProgress = true;
               
                var getListParam = {};
                getListParam.id = $scope.selectLeadId;
                var headers = {};
                headers['screen-code'] = 'leadtype';
                adminService.deleteLeadType(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getLeadtypeList();
                    }
                     $scope.isdeleteProgress = false;
                });
            }
        };

        
        $scope.getLeadtypeList = function() {          

            $scope.leadModel.isLoadingProgress = true;
            var leadListParam = {};
            var headers = {};
            headers['screen-code'] = 'leadtype';
            leadListParam.name = $scope.searchFilter.lead_name;
            leadListParam.type = $scope.searchFilter.type;
            leadListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getLeadTypeList(leadListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.leadModel.leadList = data.list;
                    $scope.leadModel.total = data.total;
                }
                $scope.leadModel.isLoadingProgress = false;
              
            });

        };

        $scope.getLeadtypeList();
    }]);






