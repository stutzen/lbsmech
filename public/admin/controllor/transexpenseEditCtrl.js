




app.controller('transexpenseEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.transexpenseModel = {
            "date": "",
            "transexpense": "",
            "description": "",
            "amount": "",
            "id": "",
            "incomeCategoryInfo": "",
            "status": "",
            "isActive": true,
            isLoadingProgress: false



        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.transexpense_add_form !== 'undefined' && typeof $scope.transexpense_add_form.$pristine !== 'undefined' && !$scope.transexpense_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            $scope.transexpense_edit_form.$setPristine();
            if (typeof $scope.transexpense_edit_form !== 'undefined')
            {
                //$scope.transexpense_edit_form.$setPristine();
                $scope.updateTransexpenseInfo();
                $scope.transexpenseModel.date = "";
                $scope.transexpenseModel.particular = "";
                $scope.transexpenseModel.category = '';
                $scope.transexpenseModel.amount = "";
                $scope.transexpenseModel.created_by = "";
                $scope.transexpenseModel.isActive = true;
            }

        };
//        $scope.currentDate = new Date();
//       $scope.transexpenseModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTabletCreate = function() {

            $scope.transexpenseModel.user = '';

        };
        $scope.selectTransexpenseId = '';
        $scope.selectTransexpenseVoucher_number = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id, voucherno)
        {
            $scope.selectTransexpenseId = id;
            $scope.selectTransexpenseVoucher_number = voucherno;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deleteTransexpenseInfo = function( )
        {

            if ($scope.isdeleteLoadingProgress == false)
            {

                $scope.isdeleteLoadingProgress = true;

                if ($scope.selectTransexpenseVoucher_number === '' || $scope.selectTransexpenseVoucher_number == 0)
                {
                    var headers = {};
                    headers['screen-code'] = 'transexpense';

                    var getListParam = {};
                    getListParam.id = $scope.selectTransexpenseId;
                    adminService.deleteTransexpense(getListParam, getListParam.id, headers).then(function(response)
                    {
                        var data = response.data;
                        if (data.success === true)
                        {
                            $scope.closePopup();
                            //$scope.getList();
                            $state.go('app.transexpense', {'id': response.data.id}, {'reload': true});
                        }
                        $scope.isdeleteLoadingProgress = false;
                    });
                }
                else
                {
                    var getListParam = {};
                    getListParam.id = $scope.selectTransexpenseVoucher_number;
                    var headers = {};
                    headers['screen-code'] = 'transexpense';
                    adminService.deleteTransexpensePayment(getListParam, getListParam.id, headers).then(function(response)
                    {
                        var data = response.data;
                        if (data.success === true)
                        {
                            $scope.closePopup();
                            //$scope.getList();
                            $state.go('app.transexpense', {'id': response.data.id}, {'reload': true});
                        }
                        $scope.isdeleteLoadingProgress = false;
                    });
                }
            }

        };
        $scope.modifyTransexpense = function() {

            $scope.isDataSavingProcess = true;
            var modifyTransexpenseParam = {};
            var headers = {};
            headers['screen-code'] = 'transexpense';
            modifyTransexpenseParam.date = $scope.transexpenseModel.transexpense;
            modifyTransexpenseParam.description = $scope.transexpenseModel.description;
            modifyTransexpenseParam.amount = $scope.transexpenseModel.amount;
            modifyTransexpenseParam.isactive = 1;
            adminService.modifyTransexpense(modifyTransexpenseParam, $scope.transexpenseModel.id, headers).then(function(response) {
                if (response.data.success === true)
                {
                    $scope.isDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.transexpense');
                }
            });
        };

        $scope.updateTransexpenseInfo = function()
        {
            if ($scope.transexpenseModel.list !== null)
            {
                $scope.transexpenseModel.date = $scope.transexpenseModel.list.date;
                $scope.transexpenseModel.description = $scope.transexpenseModel.list.particulars;
                $scope.transexpenseModel.voucherno = $scope.transexpenseModel.list.voucher_number;
                $scope.transexpenseModel.amount = $scope.transexpenseModel.list.amount;
                $scope.transexpenseModel.id = $scope.transexpenseModel.list.id;
                var date = utilityService.parseStrToDate($scope.transexpenseModel.list.transaction_date);
                $scope.transexpenseModel.date = $filter('date')(date, $rootScope.appConfig.date_format);
                $scope.transexpenseModel.account = $scope.transexpenseModel.list.account;
                $scope.transexpenseModel.incomeCategoryInfo =
                        {
                            name: $scope.transexpenseModel.list.account_category
                        }
            }
            $scope.transexpenseModel.isLoadingProgress = false;
        };




        $scope.getTransexpenseInfo = function() {

            if (typeof $stateParams.id !== 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                $scope.transexpenseModel.isLoadingProgress = true;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getTransexpenseList(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.transexpenseModel.list = data.list[0];
                        $scope.updateTransexpenseInfo();
                    }
                    $scope.transexpenseModel.isLoadingProgress = false;
                });
            }
        };
        $scope.getAccountlist = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = ($scope.transexpenseModel.currentPage - 1) * $scope.transexpenseModel.limit;
            getListParam.limit = $scope.transexpenseModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.transexpenseModel.Accountlist = data.list;
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getIncomeCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.type = 'expense';
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatincomeCategoryModel = function(model) {

            if (model !== null)
            {

                return model.name;
            }
            return  '';
        };

        $scope.getTransexpenseInfo();
        $scope.getAccountlist();
        $scope.getIncomeCategoryList();


    }]);




