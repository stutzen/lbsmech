app.controller('serviceListCtrl', ['$scope', '$rootScope', 'adminService','utilityService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, utilityService ,$filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.serviceModel = {
            scopeId: "",
            configId: 5,
            type: "",
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            serverList: null,
            isLoadingProgress: true

        };
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.serviceModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: '',
            salesOrPurchase: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getServiceListInfo, 300);
        }

        $scope.selectServiceId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function(id)
        {
            $scope.selectServiceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteServiceInfo = function( )
        {
            if($scope.isdeleteProgress == false)
            {
               $scope.isdeleteProgress = true;
               
                var getListParam = {};
                getListParam.id = $scope.selectServiceId;
                var headers = {};
                headers['screen-code'] = 'service';
                adminService.deleteService(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getServiceListInfo();
                    }
                     $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: '',
                salesOrPurchase: ''
            };
            $scope.initTableFilter();
        }
        /*
         * Get market list
         */

        $scope.getServiceListInfo = function() {

            // ?name=&sku=&type=&start=&limit=

            $scope.serviceModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'service';
            //materialListParam.inventoryScopeId = $rootScope.userModel.scopeId;
            //materialListParam.configId = 5;
            materialListParam.type = "Service";
            materialListParam.name = $scope.searchFilter.name;
            //materialListParam.status = "initiated";
            materialListParam.id = '';
            if ($scope.searchFilter.salesOrPurchase == 1)
            {
                materialListParam.is_sale = 1;
            }
            else if ($scope.searchFilter.salesOrPurchase == 2)
            {
                materialListParam.is_purchase = 1;
            }
            materialListParam.is_active = 1;
            materialListParam.start = ($scope.serviceModel.currentPage - 1) * $scope.serviceModel.limit;
            materialListParam.limit = $scope.serviceModel.limit;

            adminService.getServiceList(materialListParam, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.serviceModel.list = data.list;
                    $scope.serviceModel.total = data.total;
                    for (var i = 0; i < $scope.serviceModel.list.length; i++)
                    {
                        $scope.serviceModel.list[i].sales_price = $scope.serviceModel.list[i].sales_price;
                        $scope.serviceModel.list[i].sales_price = utilityService.changeCurrency($scope.serviceModel.list[i].sales_price, $rootScope.appConfig.thousand_seperator);
                    }
                }
                $scope.serviceModel.isLoadingProgress = false;
            });

        };

        $scope.getServiceListInfo();

    }]);




