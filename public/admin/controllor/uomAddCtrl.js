



app.controller('uomAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', 'APP_CONST', 'StutzenHttpService', '$sce', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory, APP_CONST, StutzenHttpService, $sce) {

        $scope.uomAddModel = {
            id: '',
            uomname: '',
            note: '',
            limit: '',
            isActive: true,
            sample: ''
        };
        $scope.trustedInputHtml = $sce.trustAsHtml('<input type="text" class="form-control" name = "uomnote" data-ng-model = "uomAddModel.note" placeholder="" maxlength="35" />');
        $scope.myHTML =
                'I am an <code>HTML</code>string with ' +
                '<a href="#">links!</a> and other <em>stuff</em>' + '<br>' +
                '<input type="text"' +
                'class="form-control"' +
                ' name = "uomnote"' +
                'data-ng-model = "uomAddModel.note"' +
                'placeholder=""' +
                'maxlength="35"' +
                '/>';
        //$scope.uomAddModel.sample = '<p><b>Nisha</b></p>';

        $scope.pagePerCount = [50, 100];
        $scope.uomAddModel.limit = $scope.pagePerCount[0];

        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }


        $scope.validationFactory = ValidationFactory;


        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.uom_add_form != 'undefined' && typeof $scope.uom_add_form.$pristine != 'undefined' && !$scope.uom_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        //form reset code
        $scope.formReset = function() {

            $scope.uom_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetUomCreate = function() {

            $scope.uomAddModel.user = '';

        }


        $scope.createUom = function() {

            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createUomParam = {};
                var headers = {};
                headers['screen-code'] = 'uom';
                createUomParam.id = 0;
                createUomParam.name = $scope.uomAddModel.uomname;
                createUomParam.description = $scope.uomAddModel.note;
                // createUomParam.is_active = ($scope.uomAddModel.isActive==true?1:0);            
                createUomParam.is_active = 1;
                adminService.createUomList(createUomParam, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.uom');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }

        };
    }]);




