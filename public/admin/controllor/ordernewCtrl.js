




app.controller('ordernewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST) {

        $scope.orderListModel = {
            "id": 0,
            "productname": "",
            "sku": "",
            "unit": "",
            "mrp": "",
            "sales_price": "",
            "qty": "",
            "rowtotal": "",
            "isActive": "1",
            "total": "",
            "customername": "",
            "phone": "",
            "addressId": "",
            list: [],
            "billno": "",
            "billdate": "",
            "coupon": "",
            "shipping": 0,
            "discamount": 0,
            "code": "",
            "customerInfo": {},
            "paymenttype": "CASH",
            "couponcode": "",
            "invoicetotal": "",
            "totalDisPercentage": "0",
            "totalTaxPercentage": "",
            "payment": [
                {
                    'label': 'cash',
                    'key': 'cash',
                    'value': 'cash',
                    'isSelected': false
                },
                {
                    'label': 'credit',
                    'key': 'credit',
                    'value': 'credit',
                    'isSelected': false
                },
                {
                    'label': 'card',
                    'key': 'card',
                    'value': 'card',
                    'isSelected': false
                },
                {
                    'label': 'coupon',
                    'key': 'coupon',
                    'value': 'coupon',
                    'isSelected': false
                },
            ],
            "credit": "",
            "cash": "",
            "card": "",
            "discountamount": "",
            "taxamount": "",
            "cardDetail": ""


        }
        $scope.orderListModel.billdate = new Date();
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.order_add_form != 'undefined' && typeof $scope.order_add_form.$pristine != 'undefined' && !$scope.order_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.order_add_form.$setPristine();
            $scope.payment_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function() {

            $scope.orderListModel.user = '';
        }


        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showPaymentDetailPopup = false;
        $scope.showbillPopup = false;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = "dd-MMMM-yyyy"
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }

        $scope.showProdServPopup = false;
        $scope.showProductServiceAdd = function( )
        {
            $scope.showProdServPopup = true;
        }
        
        $scope.showPaymentPopup = function()
        {
            $scope.showPaymentDetailPopup = true;
            $scope.orderListModel.payment = [];
            $scope.orderListModel.payamt = [];
        }
        $scope.closePaymentPopup = function()
        {
            $scope.showPaymentDetailPopup = false;
        }

        $scope.showBillPopup = function()
        {
            $scope.showbillPopup = true;
        }
        $scope.closeBillPopup = function()
        {
            $scope.showbillPopup = false;
        }


        $scope.getCustomerList = function(val) 
        {
            var autosearchParam = {};
            autosearchParam.fname = val;

            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, autosearchParam, false).then(function(responseData) 
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                return model.fname;
            }
            return  '';
        };
        $scope.deleteProduct = function(id) {
            var index = -1;
            for (var i = 0; i < $scope.orderListModel.list.length; i++) {
                if (id == $scope.orderListModel.list[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.orderListModel.list.splice(index, 1);
            $scope.updateInvoiceTotal();
        };
        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.updateInvoiceTotal, 300);
            $scope.calculatetotal();
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                $scope.getCustomerList();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                $scope.getCustomerList();
            }

        }

        $scope.taxList = [];
        $scope.calculatetotal = function()
        {
            var subTotal = 0;
            var totDiscountPercentage = 0;
            var totTaxPercentage = 0;
            var totalTaxPercentage = 0;
            var totMapTax = 0;
            console.log("bfr loop",$scope.orderListModel.list.length);
            for (var i = 0; i < $scope.orderListModel.list.length; i++)
            {
                $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                if ($scope.orderListModel.list[i].taxPercentage == null)
                {
                    $scope.orderListModel.list[i].taxPercentage = 0;
                }
                if ($scope.orderListModel.list[i].discountPercentage == null)
                {
                    $scope.orderListModel.list[i].discountPercentage = 0;
                }
                totDiscountPercentage += parseFloat($scope.orderListModel.list[i].discountPercentage);
                totTaxPercentage = totTaxPercentage + parseFloat($scope.orderListModel.list[i].taxPercentage);
            }
            console.log("out of loop");
            totalTaxPercentage = totalTaxPercentage + totTaxPercentage + totMapTax;
            $scope.orderListModel.totalDisPercentage = totDiscountPercentage;
            $scope.orderListModel.totalTaxPercentage = totalTaxPercentage;
            $scope.orderListModel.taxamount = parseFloat(subTotal) * (totalTaxPercentage / 100);
            $scope.orderListModel.discountamount = parseFloat(subTotal) * (totDiscountPercentage / 100);
            $scope.orderListModel.invoicetotal = parseFloat(subTotal) + $scope.orderListModel.taxamount - $scope.orderListModel.discountamount;
            $scope.orderListModel.total = parseFloat(subTotal).toFixed(2);
            $scope.orderListModel.invoicetotal = parseFloat($scope.orderListModel.invoicetotal).toFixed(2);
        }
        $scope.orderListModel.totalTax = 0;
        for (i = 0; i < $scope.taxList.length; i++)
        {
            $scope.orderListModel.totalTax += $scope.taxList[i].taxPercentage;
        }
        console.log("tottax");
        console.log($scope.orderListModel.totalTax);
        $scope.orderListModel.totalTaxPercentage = $scope.orderListModel.totalTax;
        $scope.$on("updateSelectedProductEvent", function(event, selectedItems)
        {
            if (typeof selectedItems == 'undefined')
                return;
            if ($scope.orderListModel.list.length != 0)
            {
                var isDuplicateProduct = false;
                var duplicateProductIndex = -1;
                for (i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    if (selectedItems.id == $scope.orderListModel.list[i].id)
                    {
                        isDuplicateProduct = true;
                        duplicateProductIndex = i;
                    }

                }
                if (isDuplicateProduct)
                {
                    $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                    $scope.updateInvoiceTotal();
                }

                else
                {

                    var newRow = {
                        "id": selectedItems.id,
                        "productName": selectedItems.name,
                        "sku": selectedItems.sku,
                        "sales_price": selectedItems.sales_price,
                        "qty": 1,
                        "rowtotal": selectedItems.sales_price,
                        "discountPercentage": selectedItems.discountPercentage,
                        "taxPercentage": selectedItems.taxPercentage,
                        "uom": selectedItems.uom.name,
                        "uomid": selectedItems.uom.id,
                    }
                    $scope.orderListModel.list.push(newRow);
                    $scope.updateInvoiceTotal();
                    return;
                }
            }
            else
            {

                var newRow = {
                    "id": selectedItems.id,
                    "productName": selectedItems.name,
                    "sku": selectedItems.sku,
                    "sales_price": selectedItems.sales_price,
                    "qty": 1,
                    "rowtotal": selectedItems.sales_price,
                    "discountPercentage": selectedItems.discountPercentage,
                    "taxPercentage": selectedItems.taxPercentage,
                    "uom": selectedItems.uom.name,
                    "uomid": selectedItems.uom.id,
                }
                $scope.orderListModel.list.push(newRow);
                $scope.updateInvoiceTotal();
            }


        });
        $scope.createOrder = function() {
            var totalPayamt = 0;
            for (i = 0; i < 4; i++)
            {
                if ($scope.orderListModel.payamt[i] == '' && $scope.orderListModel.payamt[i] == null)
                {
                    $scope.orderListModel.payamt[i] = 0;
                }
                if (!isNaN($scope.orderListModel.payamt[i]))
                {
                    totalPayamt += parseFloat($scope.orderListModel.payamt[i]);
                }
                //  totalPayamt += parseFloat($scope.orderListModel.payamt[i]);

            }

            if (parseFloat($scope.orderListModel.invoicetotal) == totalPayamt)
            {
                $scope.isDataSavingProcess = true;
                var createOrderParam = {};
                createOrderParam.status = 'initiated';
                createOrderParam.cashType = '';
                createOrderParam.grandTotal = parseFloat($scope.orderListModel.invoicetotal);
                createOrderParam.date = utilityService.parseDateToStr($scope.orderListModel.billdate, 'yyyy-MM-dd HH:mm:ss');
                createOrderParam.discountAmount = parseFloat($scope.orderListModel.discountamount);
                createOrderParam.billPayments = [];
                for (var i = 0; i < $scope.orderListModel.payment.length; i++)
                {
                    var billpaymentDetail = {};
                    //billpaymentDetail.id = i;
                    billpaymentDetail.paymentMode = $scope.orderListModel.payment[i].key;
                    billpaymentDetail.amount = parseFloat($scope.orderListModel.payamt[i]);
                    createOrderParam.billPayments.push(billpaymentDetail);
                }
                createOrderParam.cardDetails = $scope.orderListModel.cardDetail;
                createOrderParam.taxAmount = parseFloat($scope.orderListModel.taxamount);
                createOrderParam.customerId = $scope.orderListModel.customerInfo.id;
                createOrderParam.noOfItems = $scope.orderListModel.list.length;
                createOrderParam.order = {};
                createOrderParam.order.date = utilityService.parseDateToStr($scope.orderListModel.billdate, 'yyyy-MM-dd HH:mm:ss');
                createOrderParam.order.outletId = 1;
                createOrderParam.order.status = 'initiated';
                createOrderParam.userId = $scope.orderListModel.customerInfo.id;
                createOrderParam.order.orderDetails = [];
                for (var i = 0; i < $scope.orderListModel.list.length; i++)
                {
                    var ordereditems = {};
                    // ordereditems.materialId = $scope.orderListModel.list[i].id;
                    ordereditems.sku = $scope.orderListModel.list[i].sku;
                    ordereditems.productName = $scope.orderListModel.list[i].productName;
                    ordereditems.grossPrice = parseFloat($scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty);
                    ordereditems.sales_price = parseFloat($scope.orderListModel.list[i].sales_price);
                    ordereditems.quantity = parseFloat($scope.orderListModel.list[i].qty);
                    ordereditems.uomValue = $scope.orderListModel.list[i].uom;
                    ordereditems.uomId = $scope.orderListModel.list[i].uomid;
                    ordereditems.discountPrice = (parseFloat($scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty) * $scope.orderListModel.list[i].discountPercentage) / 100;
                    ordereditems.isActive = true;
                    ordereditems.taxInfos = [];
                    createOrderParam.order.orderDetails.push(ordereditems);
                }
                var dat = new Date();
                var time = dat.getTime();
                console.log('test');
                console.log($rootScope.userModel.scopeId);
                createOrderParam.billNo = 'W' + '-' + $rootScope.userModel.scopeId + '-' + time;
                createOrderParam.userId = $scope.orderListModel.customerInfo.id;
                createOrderParam.outletId = 1;
                createOrderParam.totalAmount = parseFloat($scope.orderListModel.total);
                createOrderParam.paidAmount = 0;
                createOrderParam.orderId = '';

                adminService.createOrder(createOrderParam).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.closePaymentPopup();
                    }
                });
            }
            else
            {
                $rootScope.$broadcast('genericErrorEvent', 'Bill Amount Not Tallied');
            }


        };
    }]);




