





app.controller('serviceRequestListCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.serviceModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            isLoadingProgress: false
        };
        $scope.searchFilter = {
            id: '',
            customerInfo: {},
            productInfo: {},
            name: '',
            user_id: '',
            user_name: '',
            userInfo: '',
            service_status: '',
            machine_status: '',
            fromdate: '',
            todate: '',
            customerId: '',
            city: ''
        };
        $scope.validationFactory = ValidationFactory;
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                id: '',
                customerInfo: {},
                productInfo: {},
                name: '',
                user_id: '',
                user_name: '',
                userInfo: '',
                service_status: '',
                machine_status: '',
                fromdate: '',
                todate: '',
                customerId: '',
                city: ''
            };
            $scope.initTableFilter();
        }
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.serviceModel.limit = $scope.pagePerCount[0];

        $scope.validationFactory = ValidationFactory;

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };

        $scope.selectServiceId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteEmployeeLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectServiceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteEmployeeLoadingProgress = false;
        };
        $scope.toDateOpen = false;
        $scope.fromDateOpen = false;
        $scope.openDate = function(index) {
            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            if (index === 1)
            {
                $scope.toDateOpen = true;
            }
        };
        $scope.viewfilter = function()
        {
            $scope.isView = !$scope.isView;
        }
        $scope.deleteServiceRequestInfo = function( )
        {
            if ($scope.isdeleteEmployeeLoadingProgress == false)
            {
                $scope.isdeleteEmployeeLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectServiceId;
                var headers = {};
                headers['screen-code'] = 'servicerequest';
                adminService.deleteServiceRequest(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteEmployeeLoadingProgress = false;
                });
            }
        };

        $scope.getList = function() {

            $scope.serviceModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'servicerequest';
            getListParam.is_active = 1;
            getListParam.id = $scope.searchFilter.customerId;
            getListParam.name = $scope.searchFilter.name;
            getListParam.billing_city_name = $scope.searchFilter.city;
            if ($scope.searchFilter.empInfo != null && typeof $scope.searchFilter.empInfo != 'undefined' && typeof $scope.searchFilter.empInfo.id != 'undefined')
            {
                getListParam.assigned_emp_id = $scope.searchFilter.empInfo.id;
            }
            else
            {
                getListParam.assigned_emp_id = '';
            }
            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
            {
                getListParam.product_id = $scope.searchFilter.productInfo.id;
            }
            else
            {
                getListParam.product_id = '';
            }
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
            if ($scope.searchFilter.machine_status != null && $scope.searchFilter.machine_status != 'undefined' && $scope.searchFilter.machine_status != '')
            {
                getListParam.machine_status = $scope.searchFilter.machine_status;
            }
            else {
                getListParam.machine_status = '';
            }
            if ($scope.searchFilter.service_status != null && $scope.searchFilter.service_status != 'undefined' && $scope.searchFilter.service_status != '')
            {
                getListParam.status = $scope.searchFilter.service_status;
            }
            else {
                getListParam.status = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            getListParam.start = ($scope.serviceModel.currentPage - 1) * $scope.serviceModel.limit;
            getListParam.limit = $scope.serviceModel.limit;
            $scope.serviceModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getServiceRequestList(getListParam, configOption, headers).then(function(response) {

                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.serviceModel.list = data;
                    $scope.serviceModel.total = data.total;
                    if ($scope.serviceModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.serviceModel.list.length; i++)
                        {
                            var datevalue = moment($scope.serviceModel.list[i].date).valueOf();
                            if (datevalue > 0)
                            {
                                var nextdate = $scope.serviceModel.list[i].date.split(' ');
                                var newnextdate = utilityService.convertToUTCDate(nextdate[0]);
                                $scope.serviceModel.list[i].date = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                            } else
                                $scope.serviceModel.list[i].date = '';
                        }
                    }
                }
                $scope.serviceModel.isLoadingProgress = false;
            });

        };
        $scope.getEmployeeList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            if (autosearchParam.fname != '')
            {
                return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };
        $scope.formatEmployeeModel = function(model)
        {
            if (model != null)
            {
                if (model.f_name != undefined && model.id != undefined && model.emp_code != undefined)
                {
                    return model.f_name + '(' + model.id + ',' + model.emp_code + ')';
                }
                else if (model.f_name != undefined && model.id != undefined)
                {
                    return model.f_name + '(' + model.id + ')';
                }
            }
            return  '';
        };
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined && model.company != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ',' + model.company + ')';
                }
                else if (model.fname != undefined && model.phone != undefined && model.company != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.company + ')';
                }
            }
            return  '';
        };
        $scope.getProductList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            autosearchParam.is_sale = 1;
            if (typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
            {
                autosearchParam.scopeId = $scope.selectedAssignFrom.id;
            }

            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }
        $scope.formatProductModel = function(model)
        {
            if (model != null)
            {

                return model.name;
            }
            return  '';
        }
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'servicerequest_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "servicerequest_form-spare_customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
            if (data.fieldName == "servicerequest_form-spare_product_dropdown")
            {
                $scope.searchFilter.productInfo = data.value;
            }
            if (data.fieldName == "servicerequest_form-spare_employee_dropdown")
            {
                $scope.searchFilter.empInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }

        });


        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {

            $timeout(function() {
                $scope.init();
            }, 300);

        });

        $scope.init();
        //   $scope.getList();

    }]);




