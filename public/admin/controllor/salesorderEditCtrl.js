
app.controller('salesorderEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

            $scope.orderListModel =
                    {
                          "id": 0,
                          "productname": "",
                          "sku": "",
                          "unit": "",
                          "mrp": "",
                          "sales_price": "",
                          "qty": "",
                          "rowtotal": "",
                          "isActive": "1",
                          "total": 0,
                          "customername": "",
                          "phone": "",
                          "addressId": "",
                          "subtotal": 0,
                          "taxAmt": 0,
                          "taxInfo": "",
                          "discountAmt": 0,
                          list: [],
                          "billno": "",
                          "billdate": "",
                          "coupon": "",
                          "shipping": 0,
                          "discamount": 0,
                          "code": "",
                          "customerInfo": "",
                          "payeeInfo": '',
                          "consigneeInfo": '',
                          "invoiceDetail": {},
                          "paymenttype": "CASH",
                          "couponcode": "",
                          "invoicePrefix": "",
                          "paymentTerm": "",
                          "taxPercentage": "",
                          "discountPercentage": "",
                          "totalDisPercentage": "0",
                          "totalTaxPercentage": "",
                          "discountamount": "",
                          "taxamount": "",
                          "cardDetail": "",
                          "duedate": "",
                          "tax_id": "",
                          "advance_amount": "",
                          "balance_amount": "",
                          "roundoff": '0.00',
                          "round": '0.00',
                          "discountMode": "",
                          "deal_id": "",
                          "deal_name": "",
                          "isLoadingProgress": false,
                          "taxAmountDetail": {
                                "tax_amount": ''
                          },
                          "subtotalWithDiscount": '',
                          "expiry_date": '',
                          "status": '',
                          "type": '',
                          "service_request_id": '',
                          "serviceRequestDetail": '',
                          "companyInfo": {},
                          "prefixList": []
                    }
            $scope.adminService = adminService;
            $scope.customAttributeList = [];
            $scope.validationFactory = ValidationFactory;
            $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
            $scope.getNavigationBlockMsg = function ()
            {
                  if (typeof $scope.order_edit_form != 'undefined' && typeof $scope.order_edit_form.$pristine != 'undefined' && !$scope.order_edit_form.$pristine)
                  {
                        return $scope.NAVIGATION_BLOCKER_MSG;
                  }

                  return "";
            }
            $scope.isDataSavingProcess = false;
            $scope.formReset = function ()
            {
                  $scope.order_edit_form.$setPristine();
                  $scope.orderListModel.invoiceDetail = {};
                  $scope.orderListModel.list = [];
                  $scope.getInvoiceInfo( );
            }

            $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
            $scope.resetOrderCreate = function () {

                  $scope.orderListModel.user = '';
            }

            $scope.initTableFilterTimeoutPromise = null;
            $scope.deletedProductList = [];
            $scope.undeletedProductList = [];
            $scope.showbillPopup = false;
            $scope.initTableFilter = function ()
            {
                  if ($scope.initTableFilterTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.initTableFilterTimeoutPromise);
                  }
                  $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
            }
            $scope.maxDate = $scope.maxDate ? null : new Date();
            $scope.startDateOpen = false;
            $scope.dueDateOpen = false;
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.currencyFormat = $rootScope.appConfig.currency;
            $scope.dateOptions = {
                  formatYear: 'yy',
                  startingDay: 1
            };
            $scope.currentDate = new Date();
            $scope.quoteEditId = $stateParams.id;
            $scope.expiryDateOpen = false;
            $scope.openDate = function (index) {

                  if (index == 0)
                  {
                        $scope.startDateOpen = true;
                  } else if (index == 1)
                  {
                        $scope.dueDateOpen = true;
                  } else if (index == 2)
                  {
                        $scope.expiryDateOpen = true;
                  }
            }

            $scope.pageHeading = '';
            if ($rootScope.currentPage == 'spareRequest')
                  $scope.pageHeading = 'Spare Request';
            else
                  $scope.pageHeading = 'Sales Order';

            $scope.senderEmail = '';
            $scope.receiver = [];
            $scope.ccEmail = '';
            $scope.showPopup = function (index) {
                  $scope.showpopup = true;
                  $scope.mailHeader = "Send a Receipt";
                  $scope.senderEmail = $scope.orderListModel.customerInfo.email;
                  $scope.receiver = [];
                  var newRow = {
                        "email": $scope.orderListModel.customerInfo.email
                  };
                  $scope.receiver.push(newRow);
                  $scope.ccEmail = $scope.orderListModel.customerInfo.email;

                  console.log("Sender Email ", $scope.senderEmail);
                  console.log("Sender Email ", $scope.receiverEmail);
                  console.log("Sender Email ", $scope.ccEmail);
                  $scope.$broadcast("initPopupEvent");
            };

            $scope.addMoreReceiverEmail = function ( )
            {
                  var newRow = {
                        "email": ""
                  };
                  $scope.receiver.push(newRow);
            }

            $scope.closePopup = function () {
                  $scope.showpopup = false;
            }

            $scope.getCustomerList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.search = val;
                  autosearchParam.is_sale = 1;
                  return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                  {
                        var data = responseData.data.list;
                        var hits = [];
                        if (data.length > 0)
                        {
                              for (var i = 0; i < data.length; i++)
                              {
                                    if (data[i].is_sale == '1' || data[i].is_sale == 1)
                                    {
                                          hits.push(data[i]);
                                    }
                              }
                        }
                        if (hits.length > 10)
                        {
                              hits.splice(10, hits.length);
                        }
                        return hits;
                  });
            };

            $scope.formatCustomerModel = function (model)
            {
                  if (model != null)
                  {
                        var phoneNumber = '';
                        if (model.phone != undefined)
                        {
                              if (model.phone.indexOf(','))
                              {
                                    phoneNumber = model.phone.split(',')[0];
                              }
                        }
                        if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                        {

                              return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                        } else if (model.fname != undefined && phoneNumber != '')
                        {

                              return model.fname + '(' + phoneNumber + ')';
                        } else if (model.fname != undefined)
                        {
                              return model.fname;
                        }
                  }
                  return  '';
            };


            $scope.$watch('orderListModel.customerInfo', function (newVal, oldVal)
            {
                  if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
                  {
                        $scope.orderListModel.customerInfo = '';
                  } else
                  {
                        var addrss = "";
                        if (newVal.billing_address != undefined && newVal.billing_address != '')
                        {
                              addrss += newVal.billing_address;
                        }
                        if (newVal.billing_city != undefined && newVal.billing_city != '')
                        {
                              addrss += '\n' + newVal.billing_city;
                        }
                        if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
                        {
                              if (newVal.billing_city != undefined && newVal.billing_city != '')
                              {
                                    addrss += '-' + newVal.billing_pincode;
                              } else
                              {
                                    addrss += newVal.billing_pincode;
                              }
                        }

                        if (newVal.billing_state != undefined && newVal.billing_state != '')
                        {
                              addrss += '\n' + newVal.billing_state;
                        }
                        if (addrss == '' && newVal.id != undefined && $scope.orderListModel.customerInfo.id != undefined && $scope.orderListModel.customerInfo.id == newVal.id)
                        {
                              $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
                        } else
                        {
                              $scope.orderListModel.customerInfo.shopping_address = addrss;
                        }
                  }
            });
            $scope.getCompanyList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.name = val;
                  autosearchParam.is_active = 1;
                  return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function (responseData)
                  {
                        var data = responseData.data.list;
                        var hits = data;
                        if (hits.length > 10)
                        {
                              hits.splice(10, hits.length);
                        }
                        return hits;
                  });
            };
            $scope.formatCompanyModel = function (model)
            {
                  if (model != null)
                  {
                        return model.name;
                  }
                  return  '';
            };

            $scope.getUomList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.name = val;

                  return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function (responseData)
                  {
                        var data = responseData.data.list;
                        var hits = data;
                        if (hits.length > 10)
                        {
                              hits.splice(10, hits.length);
                        }
                        return hits;
                  });
            };

            $scope.formatUomModel = function (model)
            {
                  if (model != null)
                  {
                        return model.name;
                  }
                  return  '';
            };

            $scope.isLoadedTax = false;
            $scope.getTaxListInfo = function ( )
            {
                  $scope.isLoadedTax = false;
                  var getListParam = {};
                  getListParam.id = "";
                  getListParam.name = "";
                  getListParam.start = 0;
                  getListParam.is_active = 1;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getTaxList(getListParam, configOption).then(function (response)
                  {
                        var data = response.data.list;

                        $scope.orderListModel.taxInfo = data;
                        if (data.length > 0)
                        {
                              $scope.orderListModel.tax_id = $scope.orderListModel.taxInfo[0].id + '';
                        }
                        $scope.isLoadedTax = true;
                  });
            };

            $scope.totalcalculationTimeoutPromise != null
            $scope.updateInvoiceTotal = function ()
            {
                  if ($scope.totalcalculationTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.totalcalculationTimeoutPromise);
                  }
                  $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
            }

            $scope.initUpdateDetailTimeoutPromise = null;

            $scope.initUpdateDetail = function ()
            {

                  if ($scope.initUpdateDetailTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                  }

                  if ($scope.isLoadedTax)
                  {
                        $scope.updateInvoiceDetails();
                  } else
                  {
                        $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                  }
            }

            $scope.updateInvoiceDetails = function ( )
            {
                  $scope.dateFormat = $rootScope.appConfig.date_format;
                  $scope.orderListModel.customerInfo = {};
                  $scope.orderListModel.companyInfo = {};
                  $scope.orderListModel.customerInfo.id = $scope.orderListModel.invoiceDetail.customer_id;
                  $scope.orderListModel.customerInfo.fname = $scope.orderListModel.invoiceDetail.customer_fname;
                  $scope.orderListModel.customerInfo.shopping_address = $scope.orderListModel.invoiceDetail.customer_address;
                  $scope.orderListModel.customerInfo.phone = $scope.orderListModel.invoiceDetail.phone;
                  $scope.orderListModel.customerInfo.email = $scope.orderListModel.invoiceDetail.email;
                  $scope.orderListModel.subtotal = $scope.orderListModel.invoiceDetail.subtotal;
                  $scope.orderListModel.taxAmt = $scope.orderListModel.invoiceDetail.tax_amount;
                  $scope.orderListModel.taxAmountDetail.tax_amount = $scope.orderListModel.invoiceDetail.tax_amount;
                  $scope.orderListModel.discountAmt = $scope.orderListModel.invoiceDetail.discount_amount;
                  $scope.orderListModel.total = $scope.orderListModel.invoiceDetail.total_amount;
                  $scope.orderListModel.advance_amount = $scope.orderListModel.invoiceDetail.advance_amount;
                  $scope.orderListModel.balance_amount = parseFloat($scope.orderListModel.total - $scope.orderListModel.advance_amount).toFixed(2);
                  $scope.orderListModel.invoicePrefix = $scope.orderListModel.invoiceDetail.prefix;
                  $scope.orderListModel.invoiceCode = $scope.orderListModel.invoiceDetail.sales_order_code;
                  $scope.orderListModel.invoiceNo = $scope.orderListModel.invoiceDetail.sales_order_no;
                  $scope.orderListModel.companyInfo.id = $scope.orderListModel.invoiceDetail.company_id;
                  $scope.orderListModel.companyInfo.name = $scope.orderListModel.invoiceDetail.company_name;
                  var date = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.date, $scope.dateFormat);
                  $scope.orderListModel.billdate = date;
                  var date = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.validuntil, $scope.dateFormat);
                  $scope.orderListModel.duedate = date;
                  var date = utilityService.parseStrToDate($scope.orderListModel.invoiceDetail.expiry_date, $scope.dateFormat);
                  if (!isNaN(date) && date != undefined && date != null)
                  {
                        $scope.orderListModel.expiry_date = date;
                  } else
                  {
                        $scope.orderListModel.expiry_date = '';
                  }
                  $scope.orderListModel.discountPercentage = $scope.orderListModel.invoiceDetail.discount_percentage + '';
                  $scope.orderListModel.tax_id = $scope.orderListModel.invoiceDetail.tax_id + '';
                  $scope.orderListModel.deal_id = $scope.orderListModel.invoiceDetail.deal_id;
                  $scope.orderListModel.deal_name = $scope.orderListModel.invoiceDetail.deal_name;
                  $scope.orderListModel.discountMode = $scope.orderListModel.invoiceDetail.discount_mode;
                  $scope.orderListModel.status = $scope.orderListModel.invoiceDetail.status;
                  $scope.orderListModel.type = $scope.orderListModel.invoiceDetail.type;
                  $scope.orderListModel.serviceRequestDetail = '';
                  $scope.customAttributeList = $scope.orderListModel.invoiceDetail.customattribute;
                  var loop;

                  for (loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
                  {
                        if ($scope.orderListModel.taxInfo[loop].id == $scope.orderListModel.invoiceDetail.tax_id)
                        {
                              $scope.orderListModel.taxPercentage = $scope.orderListModel.invoiceDetail.tax_percentage;
                              break;
                        }
                  }

                  for (loop = 0; loop < $scope.orderListModel.invoiceDetail.item.length; loop++)
                  {
                        var newRow =
                                {
                                      "id": $scope.orderListModel.invoiceDetail.item[loop].product_id,
                                      "name": $scope.orderListModel.invoiceDetail.item[loop].product_name,
                                      "sku": $scope.orderListModel.invoiceDetail.item[loop].product_sku,
                                      "sales_price": $scope.orderListModel.invoiceDetail.item[loop].unit_price,
                                      "qty": $scope.orderListModel.invoiceDetail.item[loop].qty,
                                      "rowtotal": $scope.orderListModel.invoiceDetail.item[loop].total_price,
                                      "uom": $scope.orderListModel.invoiceDetail.item[loop].uom_name,
                                      "uom_id": $scope.orderListModel.invoiceDetail.item[loop].uom_id,
                                      "custom_opt1": $scope.orderListModel.invoiceDetail.item[loop].custom_opt1,
                                      "custom_opt2": $scope.orderListModel.invoiceDetail.item[loop].custom_opt2,
                                      "custom_opt3": $scope.orderListModel.invoiceDetail.item[loop].custom_opt3,
                                      "custom_opt4": $scope.orderListModel.invoiceDetail.item[loop].custom_opt4,
                                      "custom_opt5": $scope.orderListModel.invoiceDetail.item[loop].custom_opt5
                                };
                        $scope.orderListModel.list.push(newRow);
                        $scope.orderListModel.list[loop].productname = $scope.orderListModel.list[loop];
                  }

                  if ($scope.orderListModel.invoiceDetail.payee != '' && $scope.orderListModel.invoiceDetail.payee != 0)
                  {
                        $scope.orderListModel.payeeInfo = {};
                        $scope.orderListModel.payeeInfo.id = $scope.orderListModel.invoiceDetail.payee;
                        $scope.orderListModel.payeeInfo.fname = $scope.orderListModel.invoiceDetail.payee_name;
                  }

                  if ($scope.orderListModel.invoiceDetail.consignee != '' && $scope.orderListModel.invoiceDetail.consignee != 0)
                  {
                        $scope.orderListModel.consigneeInfo = {};
                        $scope.orderListModel.consigneeInfo.id = $scope.orderListModel.invoiceDetail.consignee;
                        $scope.orderListModel.consigneeInfo.fname = $scope.orderListModel.invoiceDetail.consignee_name;
                  }

                  //Insurance and Packaging Charge
                  $scope.orderListModel.insuranceAmt = parseFloat($scope.orderListModel.invoiceDetail.insurance_charge).toFixed(2);
                  $scope.orderListModel.insuranceCharge = parseFloat($scope.orderListModel.invoiceDetail.insurance_percentage) * 100;
                  $scope.orderListModel.packagingAmt = parseFloat($scope.orderListModel.invoiceDetail.packing_charge).toFixed(2);
                  $scope.orderListModel.packagingCharge = parseFloat($scope.orderListModel.invoiceDetail.packing_percentage) * 100;

                  $scope.orderListModel.service_request_id = $scope.orderListModel.invoiceDetail.service_request_id;
                  if ($scope.orderListModel.type == 'spare_request')
                  {
                        $scope.getServiceRequestInfo();
                  }

                  $scope.orderListModel.isLoadingProgress = false;
            }

            $scope.getServiceRequestInfo = function () {
                  if (typeof $scope.orderListModel.service_request_id != 'undefined')
                  {
                        var getListParam = {};
                        getListParam.id = $scope.orderListModel.service_request_id;
                        $scope.orderListModel.isLoadingProgress = true;
                        $scope.orderListModel.serviceRequestDetail = '';
                        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                        adminService.getServiceRequestList(getListParam, configOption).then(function (response) {
                              var data = response.data;
                              if (data.total !== 0)
                              {
                                    $scope.orderListModel.serviceRequestDetail = data.list[0];
                              }
                        });
                        $scope.orderListModel.isLoadingProgress = false;
                  }
            };

            $scope.showMachinePopup = false;
            $scope.openMachineInfoPopup = function ()
            {
                  $scope.showMachinePopup = true;
            };
            $scope.closeMachineInfoPopup = function ()
            {
                  $scope.showMachinePopup = false;
            };

            $scope.getProductList = function ()
            {
                  $scope.orderListModel.isLoadingProgress = true;
                  var getParam = {};

                  getParam.is_active = 1;
                  getParam.is_sale = 1;
                  if ($scope.orderListModel.invoiceDetail.type == 'spare_request')
                  {
                        getParam.categoryName = 'spare request';
                  }
                  getParam.localData = 1;
                  if (typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
                  {
                        getParam.scopeId = $scope.selectedAssignFrom.id;
                  }

                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getProductList(getParam, configOption).then(function (response) {
                        var data = response.data;
                        $scope.orderListModel.productList = data.list;
                        $scope.orderListModel.isLoadingProgress = false;
                  });
            }

            $scope.formatProductModel = function (model, index)
            {
                  if (model != null)
                  {
                        $scope.updateProductInfo(model, index);
                        return model.name;
                  }
                  return  '';
            }
            $scope.updateProductInfo = function (model, index)
            {
                  if ($scope.orderListModel.list.length != 0)
                  {
                        if (typeof model == 'undefined')
                              return;

                        $scope.orderListModel.list[index].id = model.id;
                        $scope.orderListModel.list[index].productname.name = model.name;
                        $scope.orderListModel.list[index].sku = model.sku;
                        $scope.orderListModel.list[index].sales_price = model.sales_price;
                        if (model.qty != null && model.qty != '')
                        {
                              $scope.orderListModel.list[index].qty = model.qty;
                        } else
                        {
                              $scope.orderListModel.list[index].qty = 1;
                        }
                        $scope.orderListModel.list[index].rowtotal = model.sales_price;
                        $scope.orderListModel.list[index].discountPercentage = model.discountPercentage;
                        $scope.orderListModel.list[index].taxPercentage = model.taxPercentage;
                        $scope.orderListModel.list[index].uomInfo = {
                              "name": model.uom,
                        }
                        $scope.orderListModel.list[index].uom_id = model.uom_id;
                        $scope.updateInvoiceTotal();

                        console.log("Index for item edit ", index);
                        console.log("List Length ", $scope.orderListModel.list.length);
                        $scope.createNewProduct(index);
                        return;
                  } else
                  {
                        //            var newRow = {
                        //                "id": '',
                        //                "productName": '',
                        //                "sku": '',
                        //                "sales_price": '',
                        //                "qty": '',
                        //                "rowtotal": '',
                        //                "discountPercentage": '',
                        //                "taxPercentage": '',
                        //                "uom": '',
                        //                "uom_id": ''
                        //            }
                        //            $scope.orderListModel.list.push(newRow);                        
                  }
            }
            $scope.initvalidateProductDataAndAddNewPromise = null;

            $scope.initValidateProductDataAndAddNew = function (index)
            {

                  if ($scope.initvalidateProductDataAndAddNewPromise != null)
                  {

                        $timeout.cancel($scope.initvalidateProductDataAndAddNewPromise);
                  }

                  $scope.formValidator();
                  $scope.initvalidateProductDataAndAddNewPromise = $timeout(
                          function () {
                                $scope.createNewProduct(index);
                          },
                          300);
            };

            $scope.formValidator = function ()
            {
                  //performace issue in form validator bug
                  if ($scope.order_edit_form.$submitted)
                  {
                        $timeout(function () {
                              $scope.order_edit_form.$submitted = false;
                        }, 0);
                        $timeout(function () {
                              $scope.order_edit_form.$setSubmitted();
                        }, 100);
                  } else
                  {
                        $timeout(function () {
                              $scope.order_edit_form.$setSubmitted();
                        }, 100);
                  }
            }

            $scope.createNewProduct = function (index)
            {
                  var formDataError = false;
                  if (index != -1)
                  {
                        var productContainer = window.document.getElementById('invoice_edit_product_container');
                        var errorCell = angular.element(productContainer).find('.has-error').length;
                        if (errorCell > 0)
                        {
                              formDataError = true;
                        }
                  }
                  if (!formDataError)
                  {
                        if (index == $scope.orderListModel.list.length - 1 && !$scope.deleteEvent)
                        {

                              var newRow = {
                                    "id": '',
                                    "productName": '',
                                    "sku": '',
                                    "sales_price": '',
                                    "qty": '',
                                    "rowtotal": '',
                                    "discountPercentage": '',
                                    "taxPercentage": '',
                                    "uom": '',
                                    "uom_id": '',
                                    "custom_opt1": '',
                                    "custom_opt2": '',
                                    "custom_opt3": '',
                                    "custom_opt4": '',
                                    "custom_opt5": ''
                              }
                              $scope.orderListModel.list.push(newRow);
                        }
                        $scope.deleteEvent = false;
                  }

            }
            $scope.addNewProduct = function ()
            {
                  var newRow = {
                        "id": '',
                        "productName": '',
                        "sku": '',
                        "sales_price": '',
                        "qty": '',
                        "rowtotal": '',
                        "discountPercentage": '',
                        "taxPercentage": '',
                        "uom": '',
                        "uomid": '',
                        "custom_opt1": '',
                        "custom_opt2": '',
                        "custom_opt3": '',
                        "custom_opt4": '',
                        "custom_opt5": ''
                  }
                  $scope.orderListModel.list.push(newRow);
            }
            $scope.deleteEvent = false;
            $scope.deleteProduct = function (id)
            {
                  var index = -1;
                  for (var i = 0; i < $scope.orderListModel.list.length; i++) {
                        if (id == $scope.orderListModel.list[i].id) {
                              index = i;
                              break;
                        }
                  }
                  $scope.orderListModel.list.splice(index, 1);
                  $scope.calculatetotal();
                  $scope.calculateTaxAmt(false);
                  $scope.deleteEvent = true;
            };

            $scope.taxList = [];
            $scope.calculatetotal = function ()
            {
                  var subTotal = 0;
                  var totDiscountPercentage = 0;
                  var totTaxPercentage = 0;
                  var totalTaxPercentage = 0;
                  var totMapTax = 0;
                  console.log("bfr loop", $scope.orderListModel.list.length);
                  for (var i = 0; i < $scope.orderListModel.list.length; i++)
                  {
                        $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].sales_price * $scope.orderListModel.list[i].qty;
                        subTotal += parseFloat($scope.orderListModel.list[i].rowtotal);
                        $scope.orderListModel.list[i].rowtotal = $scope.orderListModel.list[i].rowtotal.toFixed(2);
                        if ($scope.orderListModel.list[i].taxPercentage == null)
                        {
                              $scope.orderListModel.list[i].taxPercentage = 0;
                        }
                        if ($scope.orderListModel.list[i].discountPercentage == null)
                        {
                              $scope.orderListModel.list[i].discountPercentage = 0;
                        }
                  }
                  $scope.orderListModel.subtotal = parseFloat(subTotal).toFixed(2);
                  $scope.orderListModel.taxPercentage = '';
                  for (var loop = 0; loop < $scope.orderListModel.taxInfo.length; loop++)
                  {
                        if ($scope.orderListModel.tax_id == $scope.orderListModel.taxInfo[loop].id)
                        {
                              $scope.orderListModel.taxPercentage = $scope.orderListModel.taxInfo[loop].tax_percentage;
                        }
                  }
                  if ($scope.orderListModel.taxPercentage == '' || isNaN($scope.orderListModel.taxPercentage))
                  {
                        totTaxPercentage = 0;
                  }
                  totTaxPercentage = parseFloat($scope.orderListModel.taxPercentage);
                  if ($scope.orderListModel.discountPercentage == '' || isNaN($scope.orderListModel.discountPercentage))
                  {
                        totDiscountPercentage = 0;
                  }
                  totDiscountPercentage = parseFloat($scope.orderListModel.discountPercentage);

                  if (totDiscountPercentage > 0)
                  {
                        if ($scope.orderListModel.discountMode == '%')
                        {

                              $scope.orderListModel.discountAmt = parseFloat((subTotal) * (totDiscountPercentage / 100)).toFixed(2);
                        } else if ($scope.orderListModel.discountMode == 'amount')
                        {
                              $scope.orderListModel.discountAmt = parseFloat(totDiscountPercentage).toFixed(2);
                        }
                  } else
                  {
                        $scope.orderListModel.discountAmt = '0.00';
                  }
                  if (totTaxPercentage > 0 && typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
                  {
                        $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmountDetail.tax_amount).toFixed(2);
                  } else if (totTaxPercentage > 0)
                  {
                        $scope.orderListModel.taxAmt = parseFloat((subTotal - $scope.orderListModel.discountAmt) * (totTaxPercentage / 100)).toFixed(2);
                  } else
                  {
                        $scope.orderListModel.taxAmt = parseFloat(0).toFixed(2);
                  }
                  if ($scope.orderListModel.taxAmt == '' || isNaN($scope.orderListModel.taxAmt))
                  {
                        $scope.orderListModel.taxAmt = '0.00';
                  }
                  if ($scope.orderListModel.insuranceCharge == '' || isNaN($scope.orderListModel.insuranceCharge))
                  {
                        $scope.orderListModel.insuranceAmt = '0.00';
                  } else
                  {
                        $scope.orderListModel.insuranceAmt = parseFloat($scope.orderListModel.insuranceCharge).toFixed(2);
                  }
                  if ($scope.orderListModel.packagingCharge == '' || isNaN($scope.orderListModel.packagingCharge))
                  {
                        $scope.orderListModel.packagingAmt = '0.00';
                  } else
                  {
                        $scope.orderListModel.packagingAmt = parseFloat($scope.orderListModel.packagingCharge).toFixed(2);
                  }
                  if ($scope.orderListModel.subtotal > 0)
                  {
                        $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                        $scope.orderListModel.total = (parseFloat(subTotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt) + parseFloat($scope.orderListModel.insuranceAmt) + parseFloat($scope.orderListModel.packagingAmt)).toFixed(2);
                  } else
                  {
                        $scope.orderListModel.subtotalWithDiscount = 0.00;
                        $scope.orderListModel.total = 0.00;
                  }
                  $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                  $scope.updateRoundoff();
            }

            $scope.updateRoundoff = function ()
            {
                  $scope.orderListModel.round = Math.round($scope.orderListModel.total);
                  $scope.orderListModel.roundoff = $scope.orderListModel.round - parseFloat($scope.orderListModel.total);
                  $scope.orderListModel.roundoff = parseFloat($scope.orderListModel.roundoff).toFixed(2);
                  $scope.orderListModel.round = parseFloat($scope.orderListModel.round).toFixed(2);
            }
            $scope.orderListModel.totalTax = 0;
            for (i = 0; i < $scope.taxList.length; i++)
            {
                  $scope.orderListModel.totalTax += $scope.taxList[i].taxPercentage;
            }
            $scope.orderListModel.totalTaxPercentage = $scope.orderListModel.totalTax;


            $scope.$watch('orderListModel.subtotalWithDiscount', function (newVal, oldVal)
            {
                  if (typeof newVal != 'undefined' && newVal != '' && newVal != null)
                  {
                        $scope.calculateTaxAmt(false);
                  }
            });

            $scope.calculateTaxAmt = function (forceSave)
            {
                  if (($scope.orderListModel.subtotalWithDiscount != 0 && $scope.orderListModel.subtotalWithDiscount != '' && $scope.orderListModel.subtotalWithDiscount >= 0) &&
                          ($scope.orderListModel.total != 0 && $scope.orderListModel.total != '' && $scope.orderListModel.total >= 0))
                  {
                        var getListParam = {};
                        getListParam.amount = $scope.orderListModel.subtotalWithDiscount;
                        getListParam.tax_id = $scope.orderListModel.tax_id;

                        adminService.calculateTaxAmt(getListParam).then(function (response)
                        {
                              var data = response.data;
                              $scope.orderListModel.taxAmountDetail = data;
                              $scope.orderListModel.taxAmt = parseFloat($scope.orderListModel.taxAmountDetail.tax_amount).toFixed(2);
                              $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                              $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                              $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                              $scope.updateRoundoff();

                              if (forceSave)
                              {
                                    $scope.modifyOrder(forceSave);
                              }
                              console.log('taxamtdetail');
                              console.log($scope.orderListModel.taxAmountDetail);
                        });

                  } else
                  {
                        $scope.orderListModel.taxAmountDetail = '';
                        $scope.orderListModel.taxAmt = parseFloat(0).toFixed(2);
                        if ($scope.orderListModel.subtotal > 0)
                        {
                              $scope.orderListModel.subtotalWithDiscount = $scope.orderListModel.subtotal - $scope.orderListModel.discountAmt;
                              $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                              $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                              $scope.updateRoundoff();
                        }


                        if (forceSave)
                        {
                              $scope.modifyOrder(forceSave);
                        }
                        console.log('taxamtdetail');
                        console.log($scope.orderListModel.taxAmountDetail);
                  }
            }

            $scope.$on("updateSelectedProductEvent", function (event, selectedItems)
            {
                  if (typeof selectedItems == 'undefined')
                        return;
                  if ($scope.orderListModel.list.length != 0)
                  {
                        var isDuplicateProduct = false;
                        var duplicateProductIndex = -1;
                        for (i = 0; i < $scope.orderListModel.list.length; i++)
                        {
                              if (selectedItems.id == $scope.orderListModel.list[i].id)
                              {
                                    isDuplicateProduct = true;
                                    duplicateProductIndex = i;
                              }
                        }
                        if (isDuplicateProduct)
                        {
                              $scope.orderListModel.list[duplicateProductIndex].qty = $scope.orderListModel.list[duplicateProductIndex].qty + 1;
                              console.log("upd 4");
                              $scope.updateInvoiceTotal();
                        } else
                        {
                              var newRow =
                                      {
                                            "id": selectedItems.id,
                                            "productName": selectedItems.name,
                                            "sku": selectedItems.sku,
                                            "sales_price": selectedItems.sales_price,
                                            "qty": 1,
                                            "rowtotal": selectedItems.sales_price,
                                            "discountPercentage": selectedItems.discountPercentage,
                                            "taxPercentage": selectedItems.taxPercentage,
                                            "uom": selectedItems.uom,
                                            "uom_id": selectedItems.uom_id
                                      }
                              $scope.orderListModel.list.push(newRow);
                              console.log("upd 2");
                              $scope.updateInvoiceTotal();
                              return;
                        }
                  } else
                  {
                        var newRow = {
                              "id": selectedItems.id,
                              "productName": selectedItems.name,
                              "sku": selectedItems.sku,
                              "sales_price": selectedItems.sales_price,
                              "qty": 1,
                              "rowtotal": selectedItems.sales_price,
                              "discountPercentage": selectedItems.discountPercentage,
                              "taxPercentage": selectedItems.taxPercentage,
                              "uom": selectedItems.uom,
                              "uom_id": selectedItems.uom_id
                        }
                        $scope.orderListModel.list.push(newRow);
                        console.log("upd 1");
                        $scope.updateInvoiceTotal();
                  }
            });


            $scope.getInvoiceInfo = function ( )
            {
                  $scope.orderListModel.isLoadingProgress = true;
                  var getListParam = {};
                  getListParam.id = $stateParams.id;
                  getListParam.is_active = 1;
                  var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                  adminService.getSalesOrderDetail(getListParam, configOption).then(function (response)
                  {
                        var data = response.data.data;
                        $scope.orderListModel.invoiceDetail = data;
                        if ($scope.orderListModel.invoiceDetail.type == 'spare_request')
                        {
                              $scope.pageHeading = 'Spare Request';
                              $scope.currenPageName = 'spareRequest';
                        } else
                        {
                              $scope.currenPageName = $rootScope.currentPage;
                        }
                        $scope.getProductList( );
                        $scope.initUpdateDetail();
                        $scope.getTaxListInfo( );
                  });
            }

            $scope.getInvoiceInfo( );

            $scope.updateProductInfo();
            $scope.updateOrder = function (forceSave)
            {
                  if (!forceSave)
                  {
                        if ($scope.isDataSavingProcess)
                        {
                              return;
                        }
                  }
                  $scope.isDataSavingProcess = true;
                  $scope.isSave = false;
                  for (var i = 0; i < $scope.orderListModel.list.length; i++)
                  {
                        if (i != $scope.orderListModel.list.length - 1)
                        {
                              if ($rootScope.appConfig.uomdisplay)
                              {
                                    if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                            $scope.orderListModel.list[i].uomInfo != '' && $scope.orderListModel.list[i].uomInfo != null &&
                                            $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                                            $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
                                            $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                                    {
                                          $scope.isSave = true;
                                    } else
                                    {
                                          $scope.isSave = false;
                                    }
                              } else
                              {
                                    if ($scope.orderListModel.list[i].productname != '' && $scope.orderListModel.list[i].productname != null &&
                                            $scope.orderListModel.list[i].qty != '' && $scope.orderListModel.list[i].qty != null &&
                                            $scope.orderListModel.list[i].sales_price != '' && $scope.orderListModel.list[i].sales_price != null &&
                                            $scope.orderListModel.list[i].rowtotal != '' && $scope.orderListModel.list[i].rowtotal != null)
                                    {
                                          $scope.isSave = true;
                                    } else
                                    {
                                          $scope.isSave = false;
                                    }
                              }
                        }
                  }

                  if ($scope.isSave)
                  {
                        if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
                        {
                              if ($scope.orderListModel.taxAmountDetail.tax_amount == $scope.orderListModel.taxAmt)
                              {
                                    $scope.modifyOrder();
                              } else
                              {
                                    //                            $scope.orderListModel.taxAmt = $scope.orderListModel.taxAmountDetail.tax_amount;
                                    //                            $scope.orderListModel.total = (parseFloat($scope.orderListModel.subtotal) + parseFloat($scope.orderListModel.taxAmt) - parseFloat($scope.orderListModel.discountAmt)).toFixed(2);
                                    //                            $scope.orderListModel.balance_amount = (parseFloat($scope.orderListModel.total - $scope.orderListModel.paid_amount)).toFixed(2);
                                    //                            $scope.updateRoundoff();
                                    //                            $scope.modifyOrder();
                                    $scope.calculateTaxAmt(true);
                              }
                        } else
                        {
                              $scope.calculateTaxAmt(true);
                        }
                  } else
                  {
                        sweet.show('Oops...', 'Fill Product Detail.. ', 'error');
                        $scope.isDataSavingProcess = false;
                  }

            };

            $scope.updateCustomSelectValue = function (data)
            {
                  for (var i = 0; i < $scope.customAttributeList.length; i++)
                  {
                        if ($scope.customAttributeList[i].attribute_code == data.code)
                        {
                              $scope.customAttributeList[i].value = data.value;
                              break;
                        }
                  }
            }

            $scope.$on('customfieldValueChangeEvent', function (event, data)
            {
                  console.log(data);
                  $scope.updateCustomFieldValue(data);
            });

            $scope.updateCustomFieldValue = function (data)
            {
                  for (var i = 0; i < $scope.customAttributeList.length; i++)
                  {
                        if ($scope.customAttributeList[i].attribute_code == data.code)
                        {
                              $scope.customAttributeList[i].value = data.value;
                              break;
                        }
                  }
            }

            $scope.modifyOrder = function ()
            {
                  var updateOrderParam = {};
                  var headers = {};

                  if ($scope.currenPageName == 'spareRequest')
                        headers['screen-code'] = 'spareRequest';
                  else
                        headers['screen-code'] = 'salesorder';
                  updateOrderParam.status = $scope.orderListModel.status;
                  updateOrderParam.prefix = $scope.orderListModel.invoicePrefix;
                  if (typeof $scope.orderListModel.billdate == 'object')
                  {
                        $scope.orderListModel.billdate = utilityService.parseDateToStr($scope.orderListModel.billdate, 'yyyy-MM-dd');
                  }
                  updateOrderParam.date = utilityService.changeDateToSqlFormat($scope.orderListModel.billdate, 'yyyy-MM-dd');
                  if (typeof $scope.orderListModel.duedate == 'object')
                  {
                        $scope.orderListModel.duedate = utilityService.parseDateToStr($scope.orderListModel.duedate, 'yyyy-MM-dd');
                  }
                  updateOrderParam.validuntil = utilityService.changeDateToSqlFormat($scope.orderListModel.duedate, 'yyyy-MM-dd');
                  if (typeof $scope.orderListModel.expiry_date == 'object')
                  {
                        $scope.orderListModel.expiry_date = utilityService.parseDateToStr($scope.orderListModel.expiry_date, 'yyyy-MM-dd');
                  }
                  updateOrderParam.expiry_date = utilityService.changeDateToSqlFormat($scope.orderListModel.expiry_date, 'yyyy-MM-dd');

                  updateOrderParam.customer_address = $scope.orderListModel.customerInfo.shopping_address;
                  updateOrderParam.subtotal = $scope.orderListModel.subtotal;
                  updateOrderParam.deal_id = $scope.orderListModel.deal_id;
                  updateOrderParam.deal_name = $scope.orderListModel.deal_name;
                  if (typeof $scope.orderListModel.taxAmountDetail != undefined && $scope.orderListModel.taxAmountDetail != null && $scope.orderListModel.taxAmountDetail != '')
                  {
                        updateOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmountDetail.tax_amount)).toFixed(2);
                  } else
                  {
                        updateOrderParam.tax_amount = (parseFloat($scope.orderListModel.taxAmt)).toFixed(2);
                  }
                  updateOrderParam.discount_amount = $scope.orderListModel.discountAmt;
                  updateOrderParam.discount_percentage = $scope.orderListModel.discountPercentage;
                  updateOrderParam.discount_mode = $scope.orderListModel.discountMode;
                  updateOrderParam.total_amount = $scope.round;
                  updateOrderParam.round_off = $scope.orderListModel.roundoff;
                  updateOrderParam.tax_id = $scope.orderListModel.tax_id;
                  updateOrderParam.paymentmethod = "";
                  updateOrderParam.notes = "";
                  updateOrderParam.item = [];
                  updateOrderParam.item.date = utilityService.parseDateToStr($scope.orderListModel.billdate, "yyyy-MM-dd");
                  updateOrderParam.item.outletId = 1;
                  updateOrderParam.customer_id = $scope.orderListModel.customerInfo.id;

                  updateOrderParam.type = $scope.orderListModel.type;
                  updateOrderParam.payee = 0;
                  if ($scope.orderListModel.companyInfo != null && $scope.orderListModel.companyInfo != undefined)
                  {
                        updateOrderParam.company_id = $scope.orderListModel.companyInfo.id;
                  } else
                  {
                        updateOrderParam.company_id = '';
                  }
                  if ($scope.orderListModel.payeeInfo != null && $scope.orderListModel.payeeInfo.id != undefined)
                  {
                        updateOrderParam.payee = $scope.orderListModel.payeeInfo.id;
                  }
                  updateOrderParam.consignee = 0;
                  if ($scope.orderListModel.consigneeInfo != null && $scope.orderListModel.consigneeInfo.id != undefined)
                  {
                        updateOrderParam.consignee = $scope.orderListModel.consigneeInfo.id;
                  }

                  updateOrderParam.insurance_charge = (parseFloat($scope.orderListModel.insuranceAmt)).toFixed(2);
                  updateOrderParam.packing_charge = (parseFloat($scope.orderListModel.packagingAmt)).toFixed(2);

                  updateOrderParam.insurance_percentage = parseFloat($scope.orderListModel.insuranceCharge) / 100;
                  updateOrderParam.packing_percentage = parseFloat($scope.orderListModel.packagingCharge) / 100;

                  updateOrderParam.item.orderDetails = [];
                  for (var i = 0; i < $scope.orderListModel.list.length - 1; i++)
                  {
                        //                if ($scope.orderListModel.list[i].sku != '')
                        //                {
                        var ordereditems = {};
                        ordereditems.product_id = $scope.orderListModel.list[i].id;
                        ordereditems.product_sku = $scope.orderListModel.list[i].sku;
                        if (typeof $scope.orderListModel.list[i].productname != undefined && $scope.orderListModel.list[i].productname != null && $scope.orderListModel.list[i].productname != '')
                        {
                              if ($scope.orderListModel.list[i].productname.name != null && $scope.orderListModel.list[i].productname.name != '')
                              {
                                    ordereditems.product_name = $scope.orderListModel.list[i].productname.name;
                              } else
                              {
                                    ordereditems.product_name = $scope.orderListModel.list[i].productname;
                              }
                        }
                        ordereditems.unit_price = parseFloat($scope.orderListModel.list[i].sales_price);
                        ordereditems.qty = parseFloat($scope.orderListModel.list[i].qty);
                        if (typeof $scope.orderListModel.list[i].uomInfo != undefined && $scope.orderListModel.list[i].uomInfo != null && $scope.orderListModel.list[i].uomInfo != '')
                        {
                              if ($scope.orderListModel.list[i].uomInfo.name != null && $scope.orderListModel.list[i].uomInfo.name != '')
                              {
                                    ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo.name;
                              }
                              //                            else
                              //                            {
                              //                                ordereditems.uom_name = $scope.orderListModel.list[i].uomInfo;
                              //                            }
                        }
                        ordereditems.uom_id = $scope.orderListModel.list[i].uom_id;
                        ordereditems.custom_opt1 = '';
                        ordereditems.custom_opt2 = '';
                        ordereditems.custom_opt3 = '';
                        ordereditems.custom_opt4 = '';
                        ordereditems.custom_opt5 = '';
                        if ($scope.orderListModel.list[i].custom_opt1 != '' && $scope.orderListModel.list[i].custom_opt1 != null)
                        {
                              ordereditems.custom_opt1 = $scope.orderListModel.list[i].custom_opt1;
                        }
                        if ($scope.orderListModel.list[i].custom_opt2 != '' && $scope.orderListModel.list[i].custom_opt2 != null)
                        {
                              ordereditems.custom_opt2 = $scope.orderListModel.list[i].custom_opt2;
                        }
                        if ($scope.orderListModel.list[i].custom_opt3 != '' && $scope.orderListModel.list[i].custom_opt3 != null)
                        {
                              ordereditems.custom_opt3 = $scope.orderListModel.list[i].custom_opt3;
                        }
                        if ($scope.orderListModel.list[i].custom_opt4 != '' && $scope.orderListModel.list[i].custom_opt4 != null)
                        {
                              ordereditems.custom_opt4 = $scope.orderListModel.list[i].custom_opt4;
                        }
                        if ($scope.orderListModel.list[i].custom_opt5 != '' && $scope.orderListModel.list[i].custom_opt5 != null)
                        {
                              ordereditems.custom_opt5 = $scope.orderListModel.list[i].custom_opt5;
                        }
                        ordereditems.total_price = $scope.orderListModel.list[i].sales_price * ordereditems.qty;
                        updateOrderParam.item.push(ordereditems);
                  }
                  updateOrderParam.customattribute = [];
                  for (var i = 0; i < $scope.customAttributeList.length; i++)
                  {
                        var customattributeItem = {};

                        if ($scope.customAttributeList[i].value != undefined)
                        {
                              customattributeItem.value = $scope.customAttributeList[i].value;
                        } else if ($scope.customAttributeList[i].default_value != undefined)
                        {
                              customattributeItem.value = $scope.customAttributeList[i].default_value;
                        } else
                        {
                              customattributeItem.value = "";
                        }

                        customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                        customattributeItem.sno = $scope.customAttributeList[i].id;
                        updateOrderParam.customattribute.push(customattributeItem);
                  }

                  //}

                  adminService.editSalesOrder(updateOrderParam, $stateParams.id, headers).then(function (response)
                  {
                        if (response.data.success == true)
                        {
                              $scope.formReset();
                              $state.go('app.salesorder');
                        }
                        $scope.isDataSavingProcess = false;
                  });
            }
            $scope.getPrefixList = function ()
            {
                  var getListParam = {};
                  getListParam.setting = 'sales_order_prefix';
                  var headers = {};
                  var configOption = adminService.handleOnlyErrorResponseConfig;
                  adminService.getAppSettingsList(getListParam, configOption, headers).then(function (response)
                  {
                        if (response.data.success === true)
                        {
                              var data = response.data;
                              if (data.list.length > 0)
                              {
                                    var perfix = data.list[0].value;
                                    perfix = perfix.split('\n');
                                    for (var i = 0; i < perfix.length; i++)
                                    {
                                          $scope.orderListModel.prefixList.push(perfix[i]);
                                    }
                              }

                        }
                        $scope.isPrefixListLoaded = true;
                  });

            };
            $scope.getPrefixList();
      }]);