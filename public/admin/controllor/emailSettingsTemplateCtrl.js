
app.controller('emailSettingsTemplateCtrl', ['$scope',  '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', '$stateParams', '$http', '$window', function($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, $stateParams, $http, $window) {

        $scope.emailSettingsModel = {
            "id": 0,
            "date_format": '',
            "isactive": 1,
            "currency": "",
            list: [],
            message: '',
            isLoadingProgress: false,
            subject: '',
            tempDetail: {}
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.email_settings_form != 'undefined' && typeof $scope.email_settings_form.$pristine != 'undefined' && !$scope.email_settings_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {
            if (typeof $scope.email_settings_form != 'undefined')
            {
                $scope.email_settings_form.$setPristine();
                $scope.updateEmailTempDetail();
            }
        }

        $scope.updateEmailTempDetail = function()
        {
            if ($scope.emailSettingsModel.tempDetail != null)
            {
                $scope.emailSettingsModel.id = $scope.emailSettingsModel.tempDetail.id;
                $scope.emailSettingsModel.tplname = $scope.emailSettingsModel.tempDetail.tplname;
                $scope.emailSettingsModel.subject = $scope.emailSettingsModel.tempDetail.subject;
                $scope.emailSettingsModel.message = $scope.emailSettingsModel.tempDetail.message;
            }
        }

        $scope.getEmailSettingsInfo = function()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'emailsettings';
            //getListParam.type = 'email';
            getListParam.tplname = $stateParams.tplname;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmailSettingsList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.emailSettingsModel.tempDetail = data.list[0];
                        $scope.updateEmailTempDetail();
                    }
                }

            });

        };

        $scope.updateEmailSettings = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var attributeParam = [];
                var headers = {};
                headers['screen-code'] = 'emailsettings';
                var attributeItem = {};
                attributeItem.id = $scope.emailSettingsModel.id;
                attributeItem.tplname = $scope.emailSettingsModel.tplname;
                attributeItem.subject = $scope.emailSettingsModel.subject;
                attributeItem.message = $scope.emailSettingsModel.message;
                attributeParam.push(attributeItem);
                adminService.editEmailSettings(attributeParam, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.emailsettingstemp', {'tplname': $stateParams.tplname}, {'reload': true});
                    }
                    $scope.isDataSavingProcess = false;
                });

            }
        }
        $scope.getEmailSettingsInfo();
    }]);

