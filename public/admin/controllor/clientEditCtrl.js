

app.controller('clientEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST) {

        $scope.clientAddModel = {
            "id": "",
            "name": "",
            "email": "",
            "categoryInfo": {},
            "phoneNumber": "",
            "city": "",
            "state": "",
            "country": "",
            companyId: "",
            list: [],
            "postcode": "",
            "contactPersonName": "",
            "contactPersonRole": "",
            "contactPersonPhno": "",
            "address": "",
            "isactive": 1,
            categoryList: [],
            category: '',
            isLoadingProgress: false,
            "accno": '',
            "familycode": '',
            "belongto": '',
            "commission": ''

        };
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.client_edit_form != 'undefined' && typeof $scope.client_edit_form.$pristine != 'undefined' && !$scope.client_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {

            if (typeof $scope.client_edit_form != 'undefined')
            {
                $scope.client_edit_form.$setPristine();
                $scope.updateClientInfo();
            }

        }

        $scope.formatCategoryModel = function(model) {

            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.isCategoryLoaded = false;
        $scope.getCategoryList = function() {
            var categoryListParam = {};
            categoryListParam.id = "";
            categoryListParam.name = "";
            categoryListParam.start = 0;
            categoryListParam.limit = 100;
            adminService.getCategoryList(categoryListParam).then(function(response) {
                var data = response.data.list;
                $scope.clientAddModel.categoryList = data;
                $scope.isCategoryLoaded = true;
            });

        };

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isCategoryLoaded && $scope.belongtoUserLoaded)
            {
                $scope.updateClientInfo();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateClientInfo = function()
        {
            $scope.clientAddModel.id = $scope.clientAddModel.list.id;
            $scope.clientAddModel.name = $scope.clientAddModel.list.name;
            $scope.clientAddModel.phoneNumber = $scope.clientAddModel.list.phoneNumber;
            $scope.clientAddModel.category = $scope.clientAddModel.list.categoryId + '';
            $scope.clientAddModel.email = $scope.clientAddModel.list.email;
            $scope.clientAddModel.country = $scope.clientAddModel.list.country;
            $scope.clientAddModel.companyId = $scope.clientAddModel.list.companyId;
            $scope.clientAddModel.city = $scope.clientAddModel.list.city;
            $scope.clientAddModel.state = $scope.clientAddModel.list.state;
            $scope.clientAddModel.address = $scope.clientAddModel.list.address;
            $scope.clientAddModel.postcode = $scope.clientAddModel.list.postcode;
            $scope.clientAddModel.contactPersonName = $scope.clientAddModel.list.contactPersonName;
            $scope.clientAddModel.contactPersonPhno = $scope.clientAddModel.list.contactPersonPhno;
            $scope.clientAddModel.contactPersonRole = $scope.clientAddModel.list.contactPersonRole;
            $scope.clientAddModel.balanceAmount = $scope.clientAddModel.list.balanceAmount;
            $scope.clientAddModel.commission = $scope.clientAddModel.list.commission;
            $scope.clientAddModel.familycode = $scope.clientAddModel.list.familyCode;
            $scope.clientAddModel.belongto = $scope.clientAddModel.list.customerOf + '';
            $scope.clientAddModel.imagePath = $scope.clientAddModel.list.imagePath;
            $scope.clientAddModel.isActive = 1;
            $scope.clientAddModel.isLoadingProgress = false;
        }

        $scope.modifyClient = function() {

            $scope.isDataSavingProcess = true;
            var modifyClientParam = {};
            modifyClientParam.id = $scope.clientAddModel.id;
            modifyClientParam.name = $scope.clientAddModel.name;
            modifyClientParam.companyId = $scope.clientAddModel.companyId;
            modifyClientParam.ph_no = $scope.clientAddModel.phoneNumber;
            modifyClientParam.category_id = parseInt($scope.clientAddModel.category);
            for (i = 0; i < $scope.clientAddModel.categoryList.length; i++)
            {
                if ($scope.clientAddModel.category == $scope.clientAddModel.categoryList[i].id)
                {
                    modifyClientParam.category_name = $scope.clientAddModel.categoryList[i].name;
                    break;
                }
            }
            modifyClientParam.email = $scope.clientAddModel.email;
            modifyClientParam.city = $scope.clientAddModel.city;
            modifyClientParam.Country = $scope.clientAddModel.country;
            modifyClientParam.contactpersonrole = $scope.clientAddModel.contactPersonRole;
            modifyClientParam.address = $scope.clientAddModel.address;
            modifyClientParam.state = $scope.clientAddModel.state;
            modifyClientParam.balance_amount = $scope.clientAddModel.balanceAmount;
            modifyClientParam.family_code = $scope.clientAddModel.familycode;
            modifyClientParam.customer_of = $scope.clientAddModel.belongto;
            modifyClientParam.contactPersonPhno = $scope.clientAddModel.contactPersonPhno;
            modifyClientParam.contactpersonname = $scope.clientAddModel.contactPersonName;
            modifyClientParam.post_code = $scope.clientAddModel.postcode;
            modifyClientParam.isactive = 1;
            modifyClientParam.imagePath = $scope.clientAddModel.imagePath;
            modifyClientParam.commission = $scope.clientAddModel.commission;
            
            adminService.updateClient(modifyClientParam,$scope.clientAddModel.id).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.clientlist');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.belongtoUserLoaded = false;
        $scope.getBelongtoUserList = function() {
            var userListParam = {};
            userListParam.id = "";
            userListParam.name = "";
            userListParam.start = 0;
            userListParam.limit = 100;
            adminService.getUserList(userListParam).then(function(response) {
                var data = response.data.data;
                $scope.clientAddModel.userList = data;
                $scope.belongtoUserLoaded = true;
            });

        };


        $scope.getClient = function() {
            $scope.clientAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.Id = $stateParams.id;
            getListParam.name = '';
            getListParam.phno = '';
            getListParam.email = '';
            getListParam.country = '';
            getListParam.city = '';
            getListParam.state = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.start = 0;
            getListParam.limit = 10;

            adminService.getClientList(getListParam).then(function(response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.clientAddModel.list = data.list[0];
                }
                $scope.clientAddModel.total = data.total;
                $scope.getCategoryList();
                $scope.getBelongtoUserList();
                $scope.initUpdateDetail();

            });
        };
        $scope.getClient();

    }]);




