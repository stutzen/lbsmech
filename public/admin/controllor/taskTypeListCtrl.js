





app.controller('taskTypeListCtrl', ['$scope', '$rootScope', 'adminService','ValidationFactory', '$filter', 'Auth', '$state', '$timeout','$httpService','APP_CONST', function($scope, $rootScope, adminService,ValidationFactory, $filter, Auth, $state, $timeout,$httpService,APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.taskModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            isLoadingProgress: false,
            id: ''
        };


        $scope.searchFilter = {
            name: '',
            taskInfo: {},
        };
        $scope.validationFactory = ValidationFactory;
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
               name: '',
            };
            $scope.initTableFilter();
        }

        $scope.searchFilterValue = "";
        $scope.clearFilter = function()
        {
            $scope.searchFilter = {
                name: '',
                taskInfo: {},
            };
            $scope.initTableFilter();
        }
        
        $scope.$watch('searchFilter.taskInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == null || newVal == '')
            {
                $scope.initTableFilter();
            }
        });
        
        $scope.formatTaskModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.getTaskList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.EMP_TASK_TYPE_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.taskModel.limit = $scope.pagePerCount[0];

        $scope.validationFactory = ValidationFactory;
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getTaskTypeList, 300);
        }
        
        $scope.selectTaskId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectTaskId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.selectTaskId = '';
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deleteTaskTypeInfo = function( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectTaskId;
                var headers = {};
                headers['screen-code'] = 'empTaskType';
                adminService.deleteEmployeeTaskType(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getTaskTypeList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };

        $scope.getTaskTypeList = function() {

            $scope.taskModel.isLoadingProgress = true;
            var headers = {};
            headers['screen-code'] = 'empTaskType';
            var getListParam = {};
            if ($scope.searchFilter.taskInfo != null && typeof $scope.searchFilter.taskInfo != 'undefined' && typeof $scope.searchFilter.taskInfo.id != 'undefined')
            {
                getListParam.id = $scope.searchFilter.taskInfo.id;
            }
            else
            {
                getListParam.id = '';
            }
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeTaskTypeList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.taskModel.list = data;

                    $scope.taskModel.total = data.total;
                }
                $scope.taskModel.isLoadingProgress = false;

            });

        };

        $scope.getTaskTypeList();
    }]);




