
app.controller('dailyActivityUserReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', '$localStorageService', '$localStorage', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, $localStorageService, $localStorage) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.activityModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            productname: '',
            uom: '',
            quantity: '',
            amount: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            teamList: []
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.activityModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.toDateOpen1 = false;
        $scope.createDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0));
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.company_name = $rootScope.appConfig.app_tile;
        $scope.company_address = $rootScope.appConfig.pay_to_address;
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 1)
            {
                $scope.toDateOpen = true;
            } else if (index === 2)
            {
                $scope.fromDateOpen = true;
            } else if (index === 3)
            {
                $scope.toDateOpen1 = true;
            } else if (index === 4)
            {
                $scope.createDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            employeeInfo: '',
            teamid: '',
            city: '',
            created_date: ''
        };
        $scope.searchFilter.date = $scope.currentDate;
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                employeeInfo: '',
                teamid: '',
                city: '',
                created_date: ''
            };
            $scope.activityModel.list = [];
            $scope.searchFilter.date = $scope.currentDate;
        }

        $scope.print = function ()
        {
            $window.print();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.formatEmployeeModel = function (model) {

            if (model != null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };
        $scope.getEmployeelist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'dailyactivityreport';
            if ($scope.searchFilter.date != '' && $scope.searchFilter.date != null)
            {
                if ($scope.searchFilter.date != null && typeof $scope.searchFilter.date == 'object')
                {
                    getListParam.date = utilityService.parseDateToStr($scope.searchFilter.date, $scope.dateFormat);
                }
                getListParam.date = utilityService.changeDateToSqlFormat(getListParam.date, $scope.dateFormat);
            }
            if ($scope.searchFilter.employeeInfo != '' && $scope.searchFilter.employeeInfo != undefined && $scope.searchFilter.employeeInfo != null)
            {
                getListParam.emp_id = $scope.searchFilter.employeeInfo.id;
            } else
            {
                getListParam.emp_id = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.created_date != null && $scope.searchFilter.created_date != '')
            {
                if ($scope.searchFilter.created_date != null && typeof $scope.searchFilter.created_date == 'object')
                {
                    getListParam.updated_at = utilityService.parseDateToStr($scope.searchFilter.created_date, $scope.adminService.appConfig.date_format);
                }
                getListParam.updated_at = utilityService.changeDateToSqlFormat(getListParam.updated_at, $scope.adminService.appConfig.date_format);
            }
            getListParam.team_id = $scope.searchFilter.teamid;
            getListParam.city_name = $scope.searchFilter.city;
            $scope.activityModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getActivityUserReport(getListParam, configOption, headers).then(function (response)
            {
                var totalAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.activityModel.list = data;

                    for (var i = 0; i < $scope.activityModel.list.length; i++)
                    {
                        if ($scope.activityModel.list[i].punchIN != null && $scope.activityModel.list[i].punchIN != '')
                        {
                            $scope.activityModel.list[i].inTime = $scope.activityModel.list[i].punchIN.split(' ')[1];
                            $scope.activityModel.list[i].punchIN = $scope.activityModel.list[i].inTime;
                        }
                        if ($scope.activityModel.list[i].punchOut != null && $scope.activityModel.list[i].punchOut != '')
                        {
                            $scope.activityModel.list[i].outTime = $scope.activityModel.list[i].punchOut.split(' ')[1];
                            $scope.activityModel.list[i].punchOut = $scope.activityModel.list[i].outTime;
                        }
                        for (var j = 0; j < $scope.activityModel.list[i].daily_activity.length; j++)
                        {
                            if ($scope.activityModel.list[i].daily_activity[j].next_follow_up_date != null && $scope.activityModel.list[i].daily_activity[j].next_follow_up_date != '')
                            {
                                $scope.activityModel.list[i].daily_activity[j].amount = parseFloat($scope.activityModel.list[i].daily_activity[j].amount).toFixed(2);
                                $scope.activityModel.list[i].daily_activity[j].amount = utilityService.changeCurrency($scope.activityModel.list[i].daily_activity[j].amount, $rootScope.appConfig.thousand_seperator);
                                $scope.activityModel.list[i].daily_activity[j].newdate = utilityService.parseStrToDate($scope.activityModel.list[i].daily_activity[j].next_follow_up_date);
                                $scope.activityModel.list[i].daily_activity[j].next_follow_up_date = utilityService.parseDateToStr($scope.activityModel.list[i].daily_activity[j].newdate, $scope.adminService.appConfig.date_format);
                                $scope.activityModel.list[i].daily_activity[j].updatedate = utilityService.parseStrToDate($scope.activityModel.list[i].daily_activity[j].updated_at);
                                $scope.activityModel.list[i].daily_activity[j].updated_at = utilityService.parseDateToStr($scope.activityModel.list[i].daily_activity[j].updatedate, $scope.adminService.appConfig.date_format);
                                $scope.activityModel.list[i].daily_activity[j].createdate = utilityService.parseStrToDate($scope.activityModel.list[i].daily_activity[j].created_at);
                                $scope.activityModel.list[i].daily_activity[j].created_at = utilityService.parseDateToStr($scope.activityModel.list[i].daily_activity[j].createdate, $scope.adminService.appConfig.date_format);
                            }
                        }
                    }

                }
                $scope.activityModel.isLoadingProgress = false;
            });

        };
        $scope.getEmployeeTeam = function ()
        {
            var getListParam = {};
            getListParam.type = '';
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeTeamList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.activityModel.teamList = data;
                }
            });
        };
        $scope.getEmployeeTeam();
        $scope.getList();
    }]);







