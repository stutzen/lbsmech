
app.controller('dealCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', '$localStorageService', '$localStorage', 'dealFactory', function ($scope, $rootScope, $state, $stateParams, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, $localStorageService, $localStorage, dealFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.dealModel = {
            currentPage: 1,
            total: 0,
            limit: 50,
            list: [],
            stage: '',
            filterCustomerId: '',
            isLoadingProgress: true,
            stageList: [],
            leadList: []
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [10, 100];
        $scope.isView = false;
        $scope.dealModel.limit = $scope.pagePerCount[0];
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.adminService = adminService;
        $scope.dealFilter = dealFactory.get();
        dealFactory.set('');
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            if (index === 1)
            {
                $scope.toDateOpen = true;
            }
            if (index == 2)
            {
                $scope.dealfromDateOpen = true;
            }
            if (index == 3)
            {
                $scope.dealtoDateOpen = true;
            }
        };

        $scope.searchFilter =
                {
                    subject: '',
                    customerInfo: {},
                    stage: '',
                    fromdate: '',
                    todate: '',
                    user: {},
                    sortById: '',
                    sortBySubject: '',
                    sortByStage: '',
                    sortByLeads: '',
                    sortByNextFollowUpDate: '',
                    sortByAssignedTo: '',
                    sortByCreatedAt: '',
                    sortByCreatedBy: '',
                    sortBySegmentType: '',
                    sortByAmount: '',
                    deal_type: '',
                    companyInfo: '',
                    dealfromdate: '',
                    dealtodate: '',
                    city: {},
                    stage_flag: 'intermediate',
                    deal_id: '',
                    isactive: '1',
                    sourceInfo: {}
                };

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                subject: '',
                customerInfo: {},
                stage: '',
                fromdate: '',
                todate: '',
                user: {},
                sortById: '',
                sortBySubject: '',
                sortByStage: '',
                sortByLeads: '',
                sortByNextFollowUpDate: '',
                sortByAssignedTo: '',
                sortByCreatedAt: '',
                sortByCreatedBy: '',
                sortBySegmentType: '',
                sortByAmount: '',
                deal_type: '',
                companyInfo: '',
                dealfromdate: '',
                dealtodate: '',
                city: '',
                stage_flag: 'intermediate',
                deal_id: '',
                isactive: '1',
                sourceInfo: {}
            };
            $scope.initTableFilter();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getdealListInfo, 300);
        }
        $scope.customAttributeList = [];
        $scope.isdeleteProgress = false;
        $scope.selectDealId = '';
        $scope.showdeletePopup = false;
        $scope.showPopup = function (id)
        {
            $scope.selectDealId = id;
            $scope.showdeletePopup = true;
        };
        $scope.viewfilter = function ()
        {
            $scope.isView = !$scope.isView;
        }
        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteDealItem = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectDealId;
                var headers = {};
                headers['screen-code'] = 'deal';
                adminService.deleteDeal(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getdealListInfo();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };


        $scope.$watch('searchFilter.customerInfo', function (newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal == null || newVal === '')
            {
                //            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
                {
                    $scope.initTableFilter();
                }
            }
        });
        $scope.$watch('searchFilter.sourceInfo', function (newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal == null || newVal === '')
            {
                {
                    $scope.initTableFilter();
                }
            }
        });

        $scope.$watch('searchFilter.user', function (newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal == null || newVal === '')
            {
                $scope.initTableFilter();

            }
        });

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            //            if (autosearchParam.search != '')
            //            {
            return $httpService.get(APP_CONST.API.CONTACT_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
            //            }
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                } else
                {
                    return model.fname;
                }
            }
            return  '';
        };

        $scope.getStageList = function ()
        {
            $scope.dealModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStageList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealModel.stageList = data.list;
                }
            });
        };
        $scope.getLeadTypeList = function ()
        {
            $scope.dealModel.isLoadingProgress = true;
            var leadtypeListParam = {};
            leadtypeListParam.is_active = 1;
            leadtypeListParam.type = 'deal';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getLeadTypeList(leadtypeListParam, configOption).then(function (response)
            {
                if (response.data.success == true)
                {
                    var data = response.data;
                    $scope.dealModel.leadList = data.list;
                }
                $scope.dealModel.isLoadingProgress = false;
            });

        }


        $scope.items = [];

        // filter
        $scope.filterOptions = {
            filterText: "",
            useExternalFilter: true
        };

        // paging
        $scope.totalServerItems = 0;
        $scope.pagingOptions = {
            pageSizes: [25, 50, 100],
            pageSize: 25,
            currentPage: 1
        };

        // sort
        $scope.sortOptions = {
            fields: ["name", "stage_name"],
            directions: ["ASC", "DSC"]
        };

        // grid
        $scope.gridOptions = {
            data: "items",
            columnDefs: [
                {
                    field: "id",
                    displayName: "#",
                    width: "60"
                },
                {
                    field: "name",
                    displayName: "Subject",
                    pinnable: true
                },
                {
                    field: "stage_name",
                    displayName: "Stage",
                    width: "60"
                },
                {
                    field: "contact_name",
                    displayName: "Leads",
                    width: "40"
                },
                {
                    field: "next_follow_up",
                    displayName: "Next Follow-up date",
                    width: "40"
                },
                {
                    field: "emp_name",
                    displayName: "Assigned to",
                    width: "40"
                },
                {
                    field: "created_at",
                    displayName: "Created At",
                    width: "40"
                },
                {
                    field: "created_by_name",
                    displayName: "Created By",
                    width: "40"
                },
                {
                    field: "amount",
                    displayName: "Amount",
                    width: "40"
                }
            ],
            enablePaging: true,
            enablePinning: true,
            pagingOptions: $scope.pagingOptions,
            filterOptions: $scope.filterOptions,
            keepLastSelected: true,
            multiSelect: false,
            showColumnMenu: true,
            showFilter: true,
            showGroupPanel: true,
            showFooter: true,
            sortInfo: $scope.sortOptions,
            totalServerItems: "totalServerItems",
            useExternalSorting: true,
            i18n: "en"
        };

        //            $scope.getdealListInfo = function () {
        //                  setTimeout(function () {
        //                        var sb = [];
        //                        for (var i = 0; i < $scope.sortOptions.fields.length; i++) {
        //                              sb.push($scope.sortOptions.directions[i] === "DESC" ? "-" : "+");
        //                              sb.push($scope.sortOptions.fields[i]);
        //                        }
        //
        //                        var p = {
        //                              name: $scope.filterOptions.filterText,
        //                              pageNumber: $scope.pagingOptions.currentPage,
        //                              pageSize: $scope.pagingOptions.pageSize,
        //                              sortInfo: sb.join("")
        //                        };
        //
        //                        $http({
        //                              url: "/api/item",
        //                              method: "GET",
        //                              params: p
        //                        }).success(function (data, status, headers, config) {
        //                              $scope.totalServerItems = data.totalItems;
        //                              $scope.items = data.items;
        //                        }).error(function (data, status, headers, config) {
        //                              alert(JSON.stringify(data));
        //                        });
        //                  }, 100);
        //            };

        // watches
        $scope.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getdealListInfo();
            }
        }, true);

        $scope.$watch('filterOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getdealListInfo();
            }
        }, true);

        $scope.$watch('sortOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getdealListInfo();
            }
        }, true);

        $scope.sortBy = function (field)
        {
            if (field == 'id')
            {
                if ($scope.searchFilter.sortById == '')
                {
                    $scope.searchFilter.sortById = 1;
                } else if ($scope.searchFilter.sortById == 1)
                {
                    $scope.searchFilter.sortById = 2;
                } else if ($scope.searchFilter.sortById == 2)
                    $scope.searchFilter.sortById = '';
            }
            if (field == 'subject')
            {
                if ($scope.searchFilter.sortBySubject == '')
                {
                    $scope.searchFilter.sortBySubject = 1;
                } else if ($scope.searchFilter.sortBySubject == 1)
                {
                    $scope.searchFilter.sortBySubject = 2;
                } else if ($scope.searchFilter.sortBySubject == 2)
                    $scope.searchFilter.sortBySubject = '';
            }
            if (field == 'stage')
            {
                if ($scope.searchFilter.sortByStage == '')
                {
                    $scope.searchFilter.sortByStage = 1;
                } else if ($scope.searchFilter.sortByStage == 1)
                {
                    $scope.searchFilter.sortByStage = 2;
                } else if ($scope.searchFilter.sortByStage == 2)
                    $scope.searchFilter.sortByStage = '';
            }
            if (field == 'leads')
            {
                if ($scope.searchFilter.sortByLeads == '')
                {
                    $scope.searchFilter.sortByLeads = 1;
                } else if ($scope.searchFilter.sortByLeads == 1)
                {
                    $scope.searchFilter.sortByLeads = 2;
                } else if ($scope.searchFilter.sortByLeads == 2)
                    $scope.searchFilter.sortByLeads = '';
            }
            if (field == 'next_follow_up_date')
            {
                if ($scope.searchFilter.sortByNextFollowUpDate == '')
                {
                    $scope.searchFilter.sortByNextFollowUpDate = 1;
                } else if ($scope.searchFilter.sortByNextFollowUpDate == 1)
                {
                    $scope.searchFilter.sortByNextFollowUpDate = 2;
                } else if ($scope.searchFilter.sortByNextFollowUpDate == 2)
                    $scope.searchFilter.sortByNextFollowUpDate = '';
            }
            if (field == 'assigned_to')
            {
                if ($scope.searchFilter.sortByAssignedTo == '')
                {
                    $scope.searchFilter.sortByAssignedTo = 1;
                } else if ($scope.searchFilter.sortByAssignedTo == 1)
                {
                    $scope.searchFilter.sortByAssignedTo = 2;
                } else if ($scope.searchFilter.sortByAssignedTo == 2)
                    $scope.searchFilter.sortByAssignedTo = '';
            }
            if (field == 'created_at')
            {
                if ($scope.searchFilter.sortByCreatedAt == '')
                {
                    $scope.searchFilter.sortByCreatedAt = 1;
                } else if ($scope.searchFilter.sortByCreatedAt == 1)
                {
                    $scope.searchFilter.sortByCreatedAt = 2;
                } else if ($scope.searchFilter.sortByCreatedAt == 2)
                    $scope.searchFilter.sortByCreatedAt = '';
            }
            if (field == 'created_by')
            {
                if ($scope.searchFilter.sortByCreatedBy == '')
                {
                    $scope.searchFilter.sortByCreatedBy = 1;
                } else if ($scope.searchFilter.sortByCreatedBy == 1)
                {
                    $scope.searchFilter.sortByCreatedBy = 2;
                } else if ($scope.searchFilter.sortByCreatedBy == 2)
                    $scope.searchFilter.sortByCreatedBy = '';
            }
            if (field == 'segment_type')
            {
                if ($scope.searchFilter.sortBySegmentType == '')
                {
                    $scope.searchFilter.sortBySegmentType = 1;
                } else if ($scope.searchFilter.sortBySegmentType == 1)
                {
                    $scope.searchFilter.sortBySegmentType = 2;
                } else if ($scope.searchFilter.sortBySegmentType == 2)
                    $scope.searchFilter.sortBySegmentType = '';
            }
            if (field == 'amount')
            {
                if ($scope.searchFilter.sortByAmount == '')
                {
                    $scope.searchFilter.sortByAmount = 1;
                } else if ($scope.searchFilter.sortByAmount == 1)
                {
                    $scope.searchFilter.sortByAmount = 2;
                } else if ($scope.searchFilter.sortByAmount == 2)
                    $scope.searchFilter.sortByAmount = '';
            }
            $scope.getdealListInfo();
        }

        $scope.getdealListInfo = function ()
        {
            var sb = [];
            for (var i = 0; i < $scope.sortOptions.fields.length; i++) {
                sb.push($scope.sortOptions.directions[i] === "DESC" ? "-" : "+");
                sb.push($scope.sortOptions.fields[i]);
            }

            var p = {
                name: $scope.filterOptions.filterText,
                pageNumber: $scope.pagingOptions.currentPage,
                pageSize: $scope.pagingOptions.pageSize,
                sortInfo: sb.join("")
            };
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.dealModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'deal';
            getListParam.name = $scope.searchFilter.subject;
            if ($scope.searchFilter.employee != null && typeof $scope.searchFilter.employee != 'undefined' && typeof $scope.searchFilter.employee.id != 'undefined')
            {
                getListParam.emp_id = $scope.searchFilter.employee.id;
            } else
            {
                getListParam.emp_id = '';
            }
            if ($scope.searchFilter.companyInfo != null && typeof $scope.searchFilter.companyInfo != 'undefined' && typeof $scope.searchFilter.companyInfo.id != 'undefined')
            {
                getListParam.company_id = $scope.searchFilter.companyInfo.id;
            } else
            {
                getListParam.company_id = '';
            }
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.contact_id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.contact_id = '';
            }
            if ($scope.searchFilter.user != null && typeof $scope.searchFilter.user != 'undefined' && typeof $scope.searchFilter.user.id != 'undefined')
            {
                getListParam.created_by = $scope.searchFilter.user.id;
            } else
            {
                getListParam.created_by = '';
            }
            if ($scope.searchFilter.city != null && typeof $scope.searchFilter.city != 'undefined' && typeof $scope.searchFilter.city.id != 'undefined')
            {
                getListParam.city_id = $scope.searchFilter.city.id;
            } else
            {
                getListParam.city_id = '';
            }
            if ($scope.searchFilter.sourceInfo != null && typeof $scope.searchFilter.sourceInfo != 'undefined' && typeof $scope.searchFilter.sourceInfo.id != 'undefined')
            {
                getListParam.source_id = $scope.searchFilter.sourceInfo.id;
            } else
            {
                getListParam.source_id = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.date = utilityService.changeDateToSqlFormat(getListParam.date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.created_at = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.created_at = utilityService.changeDateToSqlFormat(getListParam.created_at, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.dealfromdate != null && $scope.searchFilter.dealfromdate != '')
            {
                if ($scope.searchFilter.dealfromdate != null && typeof $scope.searchFilter.dealfromdate == 'object')
                {
                    getListParam.fromDate = utilityService.parseDateToStr($scope.searchFilter.dealfromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.fromDate = utilityService.changeDateToSqlFormat(getListParam.fromDate, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.dealtodate != null && $scope.searchFilter.dealtodate != '')
            {
                if ($scope.searchFilter.dealtodate != null && typeof $scope.searchFilter.dealtodate == 'object')
                {
                    getListParam.toDate = utilityService.parseDateToStr($scope.searchFilter.dealtodate, $scope.adminService.appConfig.date_format);
                }
                getListParam.toDate = utilityService.changeDateToSqlFormat(getListParam.toDate, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.sortById != '')
            {
                getListParam.sortById = $scope.searchFilter.sortById;
            }
            if ($scope.searchFilter.sortBySubject != '')
            {
                getListParam.sortByName = $scope.searchFilter.sortBySubject;
            }
            if ($scope.searchFilter.sortByStage != '')
            {
                getListParam.sortByStage = $scope.searchFilter.sortByStage;
            }
            if ($scope.searchFilter.sortByLeads != '')
            {
                getListParam.sortByLeads = $scope.searchFilter.sortByLeads;
            }
            if ($scope.searchFilter.sortByNextFollowUpDate != '')
            {
                getListParam.sortByNfUpDate = $scope.searchFilter.sortByNextFollowUpDate;
            }
            if ($scope.searchFilter.sortByAssignedTo != '')
            {
                getListParam.sortByAssignedTo = $scope.searchFilter.sortByAssignedTo;
            }
            if ($scope.searchFilter.sortByCreatedAt != '')
            {
                getListParam.sortByCreatedAt = $scope.searchFilter.sortByCreatedAt;
            }
            if ($scope.searchFilter.sortByCreatedBy != '')
            {
                getListParam.sortByCreatedBy = $scope.searchFilter.sortByCreatedBy;
            }
            if ($scope.searchFilter.sortBySegmentType != '')
            {
                getListParam.sortDealCategory = $scope.searchFilter.sortBySegmentType;
            }
            if ($scope.searchFilter.sortByAmount != '')
            {
                getListParam.sortByAmount = $scope.searchFilter.sortByAmount;
            }
            getListParam.start = ($scope.dealModel.currentPage - 1) * $scope.dealModel.limit;
            getListParam.limit = $scope.dealModel.limit;
            getListParam.is_active = $scope.searchFilter.isactive;
            getListParam.mode = 2;
            getListParam.stage_id = $scope.searchFilter.stage;
            getListParam.deal_category_id = $scope.searchFilter.deal_type;
            getListParam.stage_flag = $scope.searchFilter.stage_flag;
            getListParam.id = $scope.searchFilter.deal_id;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDealList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealModel.list = data.list;
                    if ($scope.dealModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.dealModel.list.length; i++)
                        {
                            $scope.dealModel.list[i].index = getListParam.start + i + 1;
                            $scope.dealModel.list[i].amount = utilityService.changeCurrency($scope.dealModel.list[i].amount, $rootScope.appConfig.thousand_seperator);
                            $scope.dealModel.list[i].newdate = utilityService.parseStrToDate($scope.dealModel.list[i].updated_at);
                            $scope.dealModel.list[i].updated_at = utilityService.parseDateToStr($scope.dealModel.list[i].newdate, $rootScope.appConfig.date_format);
                            var datevalue = moment($scope.dealModel.list[i].next_follow_up).valueOf();
                            if (datevalue > 0)
                            {
                                var nextdate = $scope.dealModel.list[i].next_follow_up.split(' ');
                                var newnextdate = utilityService.convertToUTCDate(nextdate[0]);
                                $scope.dealModel.list[i].next_follow_up = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                            } else
                                $scope.dealModel.list[i].next_follow_up = '';
                            //$scope.items.push($scope.dealModel.list[i]);
                        }
                    }
                    //$scope.gridOptions.data = $scope.dealModel.list;
                    $scope.dealModel.total = data.total;
                    //$scope.totalServerItems = data.total;
                    $scope.updateLocalStorage();
                }
                $scope.dealModel.isLoadingProgress = false;
            });
        };
        $scope.updateLocalStorage = function ()
        {
            if ($scope.searchFilter.city != '')
            {
                $localStorage["deal_form-city_dropdown"] = $scope.searchFilter.city;
            }
            if ($scope.searchFilter.companyInfo != '')
            {
                $localStorage["deal_form-deal_company_dropdown"] = $scope.searchFilter.companyInfo;
            }
            if ($scope.searchFilter.sourceInfo != '')
            {
                $localStorage["deal_form-source_info_dropdown"] = $scope.searchFilter.sourceInfo;
            }
            if ($scope.searchFilter.customerInfo != '')
            {
                $localStorage["deal_form-deal_customer_dropdown"] = $scope.searchFilter.customerInfo;
            }
            if ($scope.searchFilter.employee != '')
            {
                $localStorage["deal_form-deal_user_dropdown"] = $scope.searchFilter.employee;
            }
            if ($scope.searchFilter.deal_type != '')
            {
                $localStorage["deal_form-dealdropdown"] = $scope.searchFilter.deal_type;
            }
            if ($scope.searchFilter.stage_flag != '')
            {
                $localStorage["deal_form-dealstagetypedropdown"] = $scope.searchFilter.stage_flag;
            }
            if ($scope.searchFilter.dealfromdate != '')
            {
                $localStorage["deal_form-fromdate"] = $scope.searchFilter.dealfromdate;
            }
            if ($scope.searchFilter.stage != '')
            {
                $localStorage["deal_form-stagedropdown"] = $scope.searchFilter.stage;
            }
            if ($scope.searchFilter.subject != '')
            {
                $localStorage["deal_form-subject"] = $scope.searchFilter.subject;
            }
            if ($scope.searchFilter.todate != '')
            {
                $localStorage["deal_form-todaydeal"] = $scope.searchFilter.todate;
            }
            if ($scope.searchFilter.user != '')
            {
                $localStorage["deal_form-user_dropdown"] = $scope.searchFilter.user;
            }
        }
        $scope.formatEmployeeModel = function (model)
        {
            if (model != null)
            {
                if (model.f_name != undefined)
                {
                    return model.f_name;
                }
            }
            return  '';
        };
        $scope.getEmployeeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });

        };
        $scope.formatCityModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined)
                {
                    return model.name;
                }
            }
            return  '';
        };
        $scope.getCityList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.CITY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });

        };
        $scope.formatUserModel = function (model)
        {
            if (model != null)
            {
                if (model.f_name != undefined)
                {
                    return model.f_name;
                }
            }
            return  '';
        };
        $scope.getUserList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.GET_USER_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });

        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'deal_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "deal_form-deal_customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
            if (data.fieldName == "deal_form-deal_user_dropdown")
            {
                $scope.searchFilter.employee = data.value;
            }
            if (data.fieldName == "deal_form-deal_company_dropdown")
            {
                $scope.searchFilter.companyInfo = data.value;
            }
            if (data.fieldName == "deal_form-user_dropdown")
            {
                $scope.searchFilter.user = data.value;
            }
            if (data.fieldName == "deal_form-city_dropdown")
            {
                $scope.searchFilter.city = data.value;
            }
            if (data.fieldName == "deal_form-source_info_dropdown")
            {
                $scope.searchFilter.sourceInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            }
        });


        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function () {
                    $scope.init();
                }, 300);
            }
        });
        $scope.getCompanyList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCompanyModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getSourceList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.SOURCE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatSourceModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null || typeof $scope.dealFilter.type == 'undefined')
        {
            $scope.init();
        } else
        {

            $scope.searchFilter.dealfromdate = $scope.dealFilter.fromDate;
            $scope.searchFilter.dealtodate = $scope.dealFilter.toDate;
            $scope.searchFilter.deal_id = $scope.dealFilter.id;
            if ($scope.dealFilter.deal_name != '' && $scope.dealFilter.deal_name != null && $scope.dealFilter.deal_name != undefined)
            {
                $scope.searchFilter.subject = $scope.dealFilter.deal_name;
            }
            if ($scope.dealFilter.stage_id != '' && $scope.dealFilter.stage_id != null && $scope.dealFilter.stage_id != undefined)
                $scope.searchFilter.stage = $scope.dealFilter.stage_id + '';

            if ($scope.dealFilter.emp_id != null && $scope.dealFilter.emp_id != undefined && $scope.dealFilter.emp_id != null)
            {
                $scope.searchFilter.employee = {
                    id: $scope.dealFilter.emp_id,
                    f_name: $scope.dealFilter.emp_name
                }
            }
            if ($scope.dealFilter.deal_type_id != null && $scope.dealFilter.deal_type_id != '' && $scope.dealFilter.deal_type_id != undefined)
            {
                $scope.searchFilter.deal_type = $scope.dealFilter.deal_type_id + '';
            }
            if ($scope.dealFilter.user_id != null && $scope.dealFilter.user_id != undefined && $scope.dealFilter.user_id != null)
            {
                $scope.searchFilter.user = {
                    id: $scope.dealFilter.user_id,
                    f_name: $scope.dealFilter.emp_name
                }
            }
            if ($scope.dealFilter.city_id != null && $scope.dealFilter.city_id != undefined && $scope.dealFilter.city_id != null)
            {
                $scope.searchFilter.city = {
                    id: $scope.dealFilter.city_id,
                    name: $scope.dealFilter.city_name
                }


            }
            $scope.searchFilter.stage_flag = 'all';
            $scope.initTableFilter();
        }
        // $scope.getUserList();
        $scope.getdealListInfo();
        $scope.getStageList();
        $scope.getLeadTypeList();
    }]);




