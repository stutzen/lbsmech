app.controller('conversionjournalListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', 'utilityService', 'ValidationFactory', function ($scope, $rootScope, adminService, $filter, Auth, $timeout, utilityService, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.journalModel = {

            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            isSearchLoadingProgress: false,
            isLoadingProgress: true

        };
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.journalModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            fromdate: '',
            todate: ''
        };
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: ''
            };
            $scope.initTableFilter();
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getJournalList, 300);
        }


        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function (id)
        {
            $scope.selectedId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };
        $scope.validationFactory = ValidationFactory;
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.deleteJournalInfo = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectedId;
                var headers = {};
                headers['screen-code'] = 'conversionjournal';
                adminService.deleteConversionJournal(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getJournalList();
                    }
                    $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: ''
            };
            $scope.initTableFilter();
        }

        $scope.getJournalList = function () {

            $scope.journalModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'conversionjournal';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.start = ($scope.journalModel.currentPage - 1) * $scope.journalModel.limit;
            getListParam.limit = $scope.journalModel.limit;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getConversionJournalList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.journalModel.list = data.list;
                    for (var i = 0; i < $scope.journalModel.list.length; i++)
                    {
                        $scope.journalModel.list[i].newdate = utilityService.parseStrToDate($scope.journalModel.list[i].date);
                        $scope.journalModel.list[i].date = utilityService.parseDateToStr($scope.journalModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                    }
                    $scope.journalModel.total = data.total;
                }
                $scope.journalModel.isLoadingProgress = false;
            });

        };

        $scope.getJournalList();

    }]);




