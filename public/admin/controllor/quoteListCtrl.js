app.controller('quoteListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', '$localStorageService', 'customerFactory', '$localStorage', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, $localStorageService, customerFactory, $localStorage) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.invoiceModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            isLoadingProgress: true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.invoiceModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.customerFilter = customerFactory.get();
        customerFactory.set('');
        $scope.searchFilter =
                {
                    id: '',
                    quoteId: '',
                    duedate: '',
                    invoicedate: '',
                    customerInfo: {},
                    employeeInfo: {},
                    status: '',
                    companyInfo: {},
                    fromdate: '',
                    todate: ''
                };

        $scope.invoicedateOpen = false;
        $scope.duedateOpen = false;
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.invoicedateOpen = true;
            } else if (index == 1)
            {
                $scope.duedateOpen = true;
            }
            else if (index == 2)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 3)
            {
                $scope.toDateOpen = true;
            }
        }
        $scope.isView = false;
        $scope.expandFilter = function()
        {
            $scope.isView = !$scope.isView;
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getinvoiceListInfo, 300);
        }

        $scope.refreshScreen = function()
        {
            $scope.searchFilter =
                    {
                        id: '',
                        quoteId: '',
                        duedate: '',
                        invoicedate: '',
                        customerInfo: {},
                        employeeInfo: {},
                        status: '',
                        companyInfo: '',
                        fromdate: '',
                        todate: ''
                    };
            $scope.initTableFilter();
        }
        $scope.selectInvoiceId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectInvoiceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };
        $scope.viewfilter = function()
        {
            $scope.isView = !$scope.isView;
        }
        $scope.deleteInvoiceItem = function( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectInvoiceId;
                var headers = {};
                headers['screen-code'] = 'salesestimate';
                adminService.deleteQuote(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getinvoiceListInfo();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };

        $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                $scope.initTableFilter();
            }
        });

        $scope.$watch('searchFilter.employeeInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                $scope.initTableFilter();
            }
        });
        $scope.$watch('searchFilter.companyInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                $scope.initTableFilter();
            }
        });
        if ($scope.customerFilter != '' && $scope.customerFilter != null && $scope.customerFilter != undefined)
        {
            $scope.searchFilter.customerInfo = $scope.customerFilter;
            $localStorage["quote_list_form-quote_customer_dropdown"] = $scope.searchFilter.customerInfo;
        }

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CONTACT_SEARCH_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function(model)
        {
            var phoneNumber = '';
            if (model.phone != undefined)
            {
                if (model.phone.indexOf(','))
                {
                    phoneNumber = model.phone.split(',')[0];
                }
            }
            if (model != null)
            {
                if (model.fname != undefined && phoneNumber != undefined && model.email != undefined)
                {
                    return model.fname + '(' + phoneNumber + ',' + model.email + ')';
                } else if (model.fname != undefined && phoneNumber != undefined)
                {
                    return model.fname + '(' + phoneNumber + ')';
                }
                else
                {
                    return model.fname;
                }
            }
            return  '';
        };

        $scope.getEmployeelist = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeeModel = function(model) {

            if (model != null && model != undefined && model != '')
            {
                return model.f_name;
            }
            return  '';
        };
        $scope.getCompanyList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCompanyModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getinvoiceListInfo = function()
        {
            $scope.invoiceModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesestimate';

            var configOption = adminService.handleOnlyErrorResponseConfig;

            getListParam.id = $scope.searchFilter.quoteId;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.contactid = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.contactid = '';
            }
            if ($scope.searchFilter.companyInfo != null && typeof $scope.searchFilter.companyInfo != 'undefined' && typeof $scope.searchFilter.companyInfo.id != 'undefined')
            {
                getListParam.company_id = $scope.searchFilter.companyInfo.id;
            } else
            {
                getListParam.company_id = '';
            }

            if ($scope.searchFilter.employeeInfo != null && typeof $scope.searchFilter.employeeInfo != 'undefined' && typeof $scope.searchFilter.employeeInfo.id != 'undefined')
            {
                getListParam.assigned_to = $scope.searchFilter.employeeInfo.id;
            } else
            {
                getListParam.assigned_to = '';
            }

            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '')
            {
                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
                {
                    getListParam.from_validdate = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_validdate = utilityService.changeDateToSqlFormat(getListParam.from_validdate, $scope.adminService.appConfig.date_format);
                getListParam.to_validdate = getListParam.from_validdate;
            }
            if ($scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
            {
                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
                getListParam.to_date = getListParam.from_date;
            }
            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '' && $scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
            {
                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
                {
                    getListParam.from_validdate = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_validdate = utilityService.changeDateToSqlFormat(getListParam.from_validdate, $scope.adminService.appConfig.date_format);
                getListParam.to_validdate = getListParam.from_validdate;
                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
                getListParam.to_date = getListParam.from_date;
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            getListParam.start = ($scope.invoiceModel.currentPage - 1) * $scope.invoiceModel.limit;
            getListParam.limit = $scope.invoiceModel.limit;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            adminService.getQuoteList(getListParam, configOption, headers).then(function(response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.invoiceModel.list = data.list;
                    if ($scope.invoiceModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.invoiceModel.list.length; i++)
                        {
                            $scope.invoiceModel.list[i].index = getListParam.start + i + 1;
                            $scope.invoiceModel.list[i].newdate = utilityService.parseStrToDate($scope.invoiceModel.list[i].date);
                            $scope.invoiceModel.list[i].date = utilityService.parseDateToStr($scope.invoiceModel.list[i].newdate, $rootScope.appConfig.date_format);
                            $scope.invoiceModel.list[i].validuntildate = utilityService.parseStrToDate($scope.invoiceModel.list[i].validuntil);
                            $scope.invoiceModel.list[i].validuntil = utilityService.parseDateToStr($scope.invoiceModel.list[i].validuntildate, $rootScope.appConfig.date_format);
                            $scope.invoiceModel.list[i].total_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.invoiceModel.list[i].advance_amount = parseFloat(utilityService.changeCurrency($scope.invoiceModel.list[i].advance_amount, $rootScope.appConfig.thousand_seperator)).toFixed(2);

                        }
                    }
                    $scope.invoiceModel.total = data.total;
                }
                $scope.invoiceModel.isLoadingProgress = false;
            });
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'quote_list_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('updateTypeaheadFieldValue', function(event, data) {
            if (data.fieldName == "quote_list_form-quote_customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
            if (data.fieldName == "quote_list_form-quote_company_dropdown")
            {
                $scope.searchFilter.companyInfo = data.value;
            }
            if (data.fieldName == "quote_list_form-assigned_to_dropdown")
            {
                $scope.searchFilter.employeeInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }

        });


        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {

            $timeout(function() {
                $scope.init();
            }, 300);

        });
        $scope.checkFilter = function ()
        {
            if ($scope.customerFilter != null && $scope.customerFilter != undefined && $scope.customerFilter != '')
            {
                
                $scope.getinvoiceListInfo();
            } else
            {
                $scope.init();
            }
        }
        $scope.checkFilter();
    }]);




