

app.controller('groupSendSmsCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$window', 'sweet', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $window,sweet) {
        
        $scope.validationFactory = ValidationFactory;
        $rootScope.getNavigationBlockMsg = null;
        $scope.groupModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            groupList :[],
            serverList: null,
            isLoadingProgress: true
        };
        $scope.updateGroupInfo = function(model,index)
        {
            $scope.groupModel.groupList[index].id = model.id;
            $scope.groupModel.groupList[index].name = model.name;
        };
        $scope.checkAvail = function (model)
        {
            if($scope.groupModel.groupList.length >1)
            {
                $scope.found = false ;
                for(var i =0; i<$scope.groupModel.groupList.length;i++)
                {
                    if(model.id == $scope.groupModel.groupList[i].id)
                    {
                       $scope.found = true;
                    }
                }
            }
            if($scope.found == true)
            {
                sweet.show('Oops...', 'Group already added..', 'error');
            }
            else 
            {
                return;
            }
        }
        $scope.deleteGroupInfo = function (index)
        {
            $scope.groupModel.groupList.splice(index, 1);
            
        }
        $scope.event = null;
        $scope.keyupHandler = function(event,index)
        {
            //event.preventDefault();
            if (event.keyCode == 13 || event.keyCode == 9)
            {
                event.preventDefault();
                var currentFocusField = angular.element(event.currentTarget);
                var nextFieldId = $(currentFocusField).attr('next-focus-id');
                var currentFieldId = $(currentFocusField).attr('id');
                var productIndex = $(currentFocusField).attr('product-index');
                if (nextFieldId == undefined || nextFieldId == '')
                {
                    return;
                }

                if (currentFieldId.indexOf('_') != -1)
                {
                    var filedIdSplitedValue = currentFieldId.split('_');
                    var existingBrunchCount = $scope.groupModel.groupList.length - 1;
                    if (existingBrunchCount == filedIdSplitedValue[filedIdSplitedValue.length - 1])
                    {
                        if($scope.groupModel.groupList[index].name =="")
                        {
                            return;
                        }
                        else{
                            $scope.addGroup();
                            $timeout(function() {
                                        $scope.focusNextField (nextFieldId);
                                    },
                                    300);
                        }
                        
                    }
                }


                $("#" + nextFieldId).focus();
            }
        }

        $scope.focusNextField = function(nextFieldId)
        {
            if (nextFieldId != '')
            {
                $("#" + nextFieldId).focus();
            }
        }
        $scope.addGroup = function() {
            var group={}
            group.id='';
            group.name='';
            $scope.groupModel.groupList.push(group);
        };
        $scope.getGroupList = function (val)
        {
            var getParam = {};
            getParam.is_active = 1;
            getParam.name = val;
            return $httpService.get(APP_CONST.API.CUSTOMER_GROUP_LIST, getParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }

        $scope.formatGroupModel = function (model,index)
        {
            if (model != null)
                {
                    $scope.updateGroupInfo(model,index);
                    return model.name;
                }
                return  '';
        }
        $scope.addGroup ();
        $scope.sendSms = function (){
            if($scope.groupModel.groupList[0].id != '' || $scope.groupModel.mobile !='' && $scope.groupModel.mobile != null && $scope.groupModel.mobile !='undefined' )
            {
                if($scope.groupModel.groupList[0].id != '')
                {
                    for (var i = 0; i < $scope.groupModel.groupList.length; i++)
                    {
                        $scope.customerGroupSms($scope.groupModel.groupList[i].id,"group");
                    }
                }
                if($scope.groupModel.mobile != '')
                {
                    var mobileno = $scope.groupModel.mobile.split(',');
                    for (var i = 0; i < mobileno.length; i++)
                    {
                        $scope.customerGroupSms(mobileno[i],"mobile");
                    }
                }
                
            }
            else
            {
                 sweet.show('Oops...', 'Enter atleast One Mobile No/Group...', 'error');
            }
        };
        $scope.customerGroupSms = function (id,type)
        {
            var getListParam = {};
            getListParam.group_id = '';
            if(type == "group")
            {
                getListParam.group_id = id;
            }
            getListParam.content = $scope.groupModel.content;
            getListParam.mobile_no = '';
            if(type == "mobile")
            {
                getListParam.mobile_no = id;
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.customerGroupSms(getListParam, configOption).then(function(response)
            {
                if (response.data.success === true)
                {
                    //$scope.formReset();
                    $state.go('app.group');
                }
                
            });
        }
    }]);
