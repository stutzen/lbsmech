

app.controller('acodeAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.acodeModel = {
            id: '',
            name: '',
            isActive: true,
            code: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_acode_form != 'undefined' && typeof $scope.add_acode_form.$pristine != 'undefined' && !$scope.add_acode_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_acode_form.$setPristine();
            if (typeof $scope.add_acode_form != 'undefined')
            {
                $scope.acodeModel.name = "";
                $scope.acodeModel.code = "";
                $scope.acodeModel.isActive = true;
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.createacode = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addAcodeParam = {};
            var headers = {};
            headers['screen-code'] = 'acodesettings';
            addAcodeParam.id = 0;
            addAcodeParam.name = $scope.acodeModel.name;
            addAcodeParam.code = $scope.acodeModel.code;
            addAcodeParam.is_active = $scope.acodeModel.isActive == true ? 1 : 0;

            adminService.saveAcode(addAcodeParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.acodesettings');
                }
                $scope.isDataSavingProcess = false;
            });
        };

    }]);






