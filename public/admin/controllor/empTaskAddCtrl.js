




app.controller('empTaskAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', 'sweet', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, sweet, $stateParams, $timeout, ValidationFactory) {

        $scope.taskModel = {
            task_name: '',
            is_active: true,
            comments: "",
//            description: ""
        };
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.task_add_form !== 'undefined' && typeof $scope.task_add_form.$pristine !== 'undefined' && !$scope.task_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            $scope.task_add_form.$setPristine();
            $scope.taskModel.task_name = "";
            $scope.taskModel.is_active = true;
            $scope.taskModel.comments = "";
//            $scope.taskModel.description = "";
        };

        $scope.createTaskType = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createTaskTypeParam = {};
                var headers = {};
                headers['screen-code'] = 'empTask';
                createTaskTypeParam.task_name = $scope.taskModel.task_name;
                createTaskTypeParam.comments = $scope.taskModel.comments;
//                createTaskTypeParam.description = $scope.taskModel.description;
                createTaskTypeParam.is_active = 1; 
                adminService.createEmployeeTaskList(createTaskTypeParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.empTask');
                    }
                    $scope.isDataSavingProcess = false;
                });
            } else
            {
                return;
            }

        };

    }
]);




