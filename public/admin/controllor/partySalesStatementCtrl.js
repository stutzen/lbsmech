





app.controller('partySalesStatementCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.partySalesStatementModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            totaldebit: 0,
            totalcredit: 0,
            totalamt: 0,
            totalbalance: 0,
            openingBalance: null,
            isSearchLoadingProgress: false,
            openingCredit: '',
            openingDebit: '',
            openingAmount: ''
        };

        $scope.pagePerCount = [50, 100];
        $scope.partySalesStatementModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.adminService = adminService;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };
       
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            customerInfo: {}

        };
         $scope.searchFilter.todate = $scope.currentDate;
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                customerInfo: {}
            };
            $scope.partySalesStatementModel.list = [];
            $scope.partySalesStatementModel.openingBalance = null;
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.calculateTotal = function()
        {
            var totDebit = 0;
            var totCredit = 0;
            var totamt = 0;
            for (var i = 0; i < $scope.partySalesStatementModel.list.length; i++)
            {
                totDebit += parseFloat($scope.partySalesStatementModel.list[i].debit);
                $scope.partySalesStatementModel.totaldebit = totDebit + parseFloat($scope.partySalesStatementModel.openingBalance.debit);
                $scope.partySalesStatementModel.totaldebit = parseFloat($scope.partySalesStatementModel.totaldebit).toFixed(2);
                $scope.partySalesStatementModel.totaldebit = utilityService.changeCurrency($scope.partySalesStatementModel.totaldebit, $rootScope.appConfig.thousand_seperator);

                totCredit += parseFloat($scope.partySalesStatementModel.list[i].credit);
                $scope.partySalesStatementModel.totalcredit = totCredit + parseFloat($scope.partySalesStatementModel.openingBalance.credit);
                $scope.partySalesStatementModel.totalcredit = parseFloat($scope.partySalesStatementModel.totalcredit).toFixed(2);
                $scope.partySalesStatementModel.totalcredit = utilityService.changeCurrency($scope.partySalesStatementModel.totalcredit, $rootScope.appConfig.thousand_seperator);

                totamt += parseFloat($scope.partySalesStatementModel.list[i].amount);
                $scope.partySalesStatementModel.totalamt = totamt + parseFloat($scope.partySalesStatementModel.totalbalance);

            }
        }

        $scope.updateBalanceAmount = function()
        {
            if ($scope.partySalesStatementModel.list.length >= 1)
            {
                var openingBalance = parseFloat($scope.partySalesStatementModel.openingBalance.credit) - parseFloat($scope.partySalesStatementModel.openingBalance.debit);
                for (var i = 0; i < $scope.partySalesStatementModel.list.length; i++)
                {
                    var credit = $scope.partySalesStatementModel.list[i].credit == null ? 0 : parseFloat($scope.partySalesStatementModel.list[i].credit);
                    var debit = $scope.partySalesStatementModel.list[i].debit == null ? 0 : parseFloat($scope.partySalesStatementModel.list[i].debit);
                    if (credit != 0)
                    {
                        openingBalance = openingBalance + credit;
                    }
                    else if (debit != 0)
                    {
                        openingBalance = openingBalance - debit;
                    }
                    $scope.partySalesStatementModel.list[i].balance = openingBalance;
                }

                $scope.partySalesStatementModel.totalbalance = openingBalance;
                $scope.partySalesStatementModel.totalbalance = parseFloat($scope.partySalesStatementModel.totalbalance).toFixed(2);
                $scope.partySalesStatementModel.totalbalance = utilityService.changeCurrency($scope.partySalesStatementModel.totalbalance, $rootScope.appConfig.thousand_seperator);


            }
            else
            {
                $scope.partySalesStatementModel.totalbalance = 0.00;
            }
        }

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'partysalesreport';
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.id = '';
            }
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            }
            else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            }
            else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }

            $scope.partySalesStatementModel.isLoadingProgress = true;
            $scope.partySalesStatementModel.isSearchLoadingProgress = true;
            getListParam.start = 0;
            getListParam.limit = 0;
            adminService.getPartySalesList(getListParam, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.partySalesStatementModel.list = data;
                    $scope.partySalesStatementModel.openingBalance = response.data.openBalance;
                    $scope.partySalesStatementModel.openingBalance.credit = parseFloat($scope.partySalesStatementModel.openingBalance.credit).toFixed(2);
                    $scope.partySalesStatementModel.openingBalance.debit = parseFloat($scope.partySalesStatementModel.openingBalance.debit).toFixed(2);
                    var openingBalance = parseFloat($scope.partySalesStatementModel.openingBalance.credit) - parseFloat($scope.partySalesStatementModel.openingBalance.debit);
                    if (openingBalance > 0)
                    {
                        $scope.partySalesStatementModel.openingBalance.credit = Math.abs(openingBalance.toFixed(2));
                        $scope.partySalesStatementModel.openingBalance.debit = 0;
                    }
                    else if (openingBalance < 0)
                    {
                        $scope.partySalesStatementModel.openingBalance.debit = Math.abs(openingBalance.toFixed(2));
                        $scope.partySalesStatementModel.openingBalance.credit = 0;
                    }
                    else if (openingBalance == 0)
                    {
                        $scope.partySalesStatementModel.openingBalance.debit = 0;
                        $scope.partySalesStatementModel.openingBalance.credit = 0;
                    }
                    $scope.partySalesStatementModel.openingBalance.balance = openingBalance;
                    $scope.partySalesStatementModel.openingBalance.balance = parseFloat($scope.partySalesStatementModel.openingBalance.balance).toFixed(2);
                    $scope.partySalesStatementModel.openingAmount = utilityService.changeCurrency($scope.partySalesStatementModel.openingBalance.balance, $rootScope.appConfig.thousand_seperator);
                    //$scope.partySalesStatementModel.openingBalance.balance = parseFloat($scope.partySalesStatementModel.openingBalance.balance).toFixed(2);
                    $scope.partySalesStatementModel.openingCredit = utilityService.changeCurrency($scope.partySalesStatementModel.openingBalance.credit.toFixed(2), $rootScope.appConfig.thousand_seperator);
                    $scope.partySalesStatementModel.openingDebit = utilityService.changeCurrency($scope.partySalesStatementModel.openingBalance.debit.toFixed(2), $rootScope.appConfig.thousand_seperator);

                    if ($scope.partySalesStatementModel.list.length != 0)
                    {
                        $scope.updateBalanceAmount();
                        $scope.calculateTotal();
                        for (var i = 0; i < $scope.partySalesStatementModel.list.length; i++)
                        {
                            $scope.partySalesStatementModel.list[i].newdate = utilityService.parseStrToDate($scope.partySalesStatementModel.list[i].date);
                            $scope.partySalesStatementModel.list[i].date = utilityService.parseDateToStr($scope.partySalesStatementModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                            // $scope.partySalesStatementModel.list[i].balance = parseFloat($scope.partySalesStatementModel.list[i].balance).toFixed(2);
                            $scope.partySalesStatementModel.list[i].balance = parseFloat($scope.partySalesStatementModel.list[i].balance).toFixed(2);
                            $scope.partySalesStatementModel.list[i].balance = utilityService.changeCurrency($scope.partySalesStatementModel.list[i].balance, $rootScope.appConfig.thousand_seperator);
                            $scope.partySalesStatementModel.list[i].debit = utilityService.changeCurrency($scope.partySalesStatementModel.list[i].debit, $rootScope.appConfig.thousand_seperator);
                            $scope.partySalesStatementModel.list[i].credit = utilityService.changeCurrency($scope.partySalesStatementModel.list[i].credit, $rootScope.appConfig.thousand_seperator);
                        }
                    }
                    else
                    {
                        $scope.partySalesStatementModel.totaldebit = $scope.partySalesStatementModel.openingDebit;
                        $scope.partySalesStatementModel.totalcredit = $scope.partySalesStatementModel.openingCredit;
                        $scope.partySalesStatementModel.totalbalance = $scope.partySalesStatementModel.openingBalance.balance;
                    }
                    $scope.partySalesStatementModel.total = data.total;
                }
                $scope.partySalesStatementModel.isLoadingProgress = false;
                $scope.partySalesStatementModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
//        $scope.formatcustomerModel = function(model) {
//
//            if (model !== null)
//            {
//                $scope.searchFilter.id = model.id;
//                return model.fname;
//            }
//            return  '';
//        };

        $scope.print = function()
        {
            $window.print();
        };

    }]);




