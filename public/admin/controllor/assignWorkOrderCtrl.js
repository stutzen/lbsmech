


app.controller('assignWorkOrderCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', '$httpService', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, $httpService, APP_CONST, $stateParams) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.workModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            isLoadingProgress: false,
            departmentList: [],
            taskGroupList: [],
            customerInfo: '',
            detailList: ''
        };

        $scope.pagePerCount = [50, 100];
        $scope.workModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            status: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                status: ''
            };
            $scope.initTableFilter();
        }
        $scope.getWorkorderPendingList = function()
        {
            var getListParam = {};
            getListParam.department_id = $scope.workModel.department;
            getListParam.is_active = 1;
//            getListParam.type = 'external';
//            getListParam.status = 'ready';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getWorkorderPendingList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.list.length > 0)
                {
                    $scope.workModel.taskGroupList = data.list;
                }

            });
        }
        $scope.deleteTaskGroup = function($index)
        {
            $scope.workModel.taskGroupList.splice($index, 1);
        }
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'manufacturingworkorder';
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            getListParam.start = ($scope.workModel.currentPage - 1) * $scope.workModel.limit;
            getListParam.limit = $scope.workModel.limit;
            $scope.workModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getWorkOrderDetailList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.workModel.list = data;
                    $scope.workModel.total = response.data.total;
                }
                $scope.workModel.isLoadingProgress = false;
            });

        };
        $scope.getDepartmentList = function()
        {
            var getListParam = {};
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDepartmentList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.list.length > 0)
                {
                    $scope.workModel.departmentList = data.list;
                }

            });
        }
        $scope.saveCustomer = function()
        {
            var customerParam = {};
            var headers = {};
            headers['screen-code'] = 'manufacturingworkorder';
            customerParam.order = [];
            if ($scope.workModel.customerInfo != '' && $scope.workModel.customerInfo != undefined && $scope.workModel.customerInfo != null)
            {
                customerParam.customer_id = $scope.workModel.customerInfo.id;
            }
            for (var i = 0; i < $scope.workModel.taskGroupList.length; i++)
            {
                customerParam.order.push($scope.workModel.taskGroupList[i].id);
            }
            adminService.saveWorkorderCustomer(customerParam, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.isDataSavingProcess = false;
                    //  $scope.formReset();
                    $state.go('app.manufacturingworkorder');
                }

            });
        }
        $scope.getSessionList = function() {

            var getListParam = {};
            if (typeof $stateParams.id != 'undefined')
            {
                getListParam.work_order_id = $stateParams.id;
                $scope.workModel.sessionList = [];
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getSessionListAll(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.workModel.sessionList = data.list;
                    }
                });
            }

        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.getList = function() {

            if (typeof $stateParams.id != undefined)
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                $scope.workModel.isLoadingProgress = true;
                var configOption = adminService.handleOnlyErrorResponseConfig;
                adminService.getWorkOrderDetailList(getListParam, configOption).then(function(response) {
                    if (response.data.success === true)
                    {
                        var data = response.data.list[0];
                        $scope.workModel.detailList = data;
                        $scope.workModel.total = response.data.total;
                    }
                    $scope.workModel.isLoadingProgress = false;
                });
            }
        }

        $scope.getSessionList();
        $scope.getDepartmentList();
        $scope.getList();
    }]);








