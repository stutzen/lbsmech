

app.controller('dealEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', '$googleMapService', 'NgMap', 'sweet',
    function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, $googleMapService, NgMap, sweet) {

        $scope.dealAddModel = {
            id: '',
            productInfo: {},
            stage: '',
            isActive: true,
            stageList: [],
            nextFollowList: [],
            customer: {},
            userList: [],
            user: '',
            type: '',
            closedate: '',
            nextdate: '',
            amount: '',
            activityList: [],
            notes: '',
            estimateList: [],
            orderList: [],
            customerList: [],
            attach: [],
            invoiceList: [],
            estimatecurrentPage: 1,
            estimatetotal: 0,
            estimatelimit: '',
            ordercurrentPage: 1,
            ordertotal: 0,
            orderlimit: '',
            activity_date: '',
            activity_time: '',
            log_type: '',
            log_subtype: '',
            invoicecurrentPage: 1,
            invoicetotal: 0,
            invoicelimit: '',
            "latitude": "APP_CONST.DEFAULT_GEO_POINT.LAT",
            "longitude": "APP_CONST.DEFAULT_GEO_POINT.LNG",
            "mapLocation": '',
            "followersList": [],
            sourceList: [],
            leadList: [],
            companyInfo: '',
            customer_name: '',
            mobile: '',
            contactList: [],
            source_id: '',
            sourceName: '',
            company_address: '',
            website: '',
            lead_category_type: '',
            lead_category_type_id: ''
        };
        $scope.dealQuestionModel = {
            established: '',
            printingactivities: '',
            machine: '',
            dailyproduction: '',
            ownCall: ''
        };

        $scope.dealAddModel.latitude = APP_CONST.DEFAULT_GEO_POINT.LAT;
        $scope.dealAddModel.longitude = APP_CONST.DEFAULT_GEO_POINT.LNG;
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.dealAddModel.estimatelimit = $scope.pagePerCount[0];
        $scope.dealAddModel.orderlimit = $scope.pagePerCount[0];
        $scope.dealAddModel.invoicelimit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.currentStep = 5;

        $scope.tabChange = function (value) {

            $scope.currentStep = value;
        }
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.deal_form != 'undefined' && typeof $scope.deal_form.$pristine != 'undefined' && !$scope.deal_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function (index) {
            if (index == 0)
            {
                $scope.deal_form.$setPristine();
                $scope.updateDealInfo();
            }
            if (index == 1)
            {
                $scope.deal_edit_form1.$setPristine();
                $scope.dealAddModel.notes = "";
                $scope.dealAddModel.attach = [];
            }
            if (index == 3)
            {

                $scope.deal_edit_form3.$setPristine();
                $scope.dealAddModel.log_subtype = "";
                $scope.dealAddModel.activity_time = new Date();
                $scope.dealAddModel.activity_date = new Date();
                $scope.dealAddModel.log_type = "";
                $scope.dealAddModel.latitude = "";
                $scope.dealAddModel.longitude = "";
            }
            if (index == 4)
            {
                $scope.question_form.$setPristine();
                $scope.updateDealInfo();
            }

        }
        $scope.currentDate = new Date();
        $scope.dealAddModel.activity_time = new Date();
        $scope.dealAddModel.activity_date = new Date();
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.closeDateOpen = false;
        $scope.nextDateOpen = false;
        $scope.activity_DateOpen = false;
        $scope.reasonDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.closeDateOpen = true;
            }
            if (index == 1)
            {
                $scope.activity_DateOpen = true;
            }
            if (index == 2)
            {
                $scope.nextDateOpen = true;
            }
            if (index == 3)
            {
                $scope.reasonDateOpen = true;
            }
        }
        $scope.isDealActivityLoaded = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.showCustomerAddPopup = false;
        //        $scope.showCustomerPopup = function ()
        //        {
        //            $scope.showCustomerAddPopup = true;
        //            if (typeof $scope.dealAddModel.customer != undefined && $scope.dealAddModel.customer != '' && $scope.dealAddModel.customer != null)
        //            {
        //
        //                $rootScope.$broadcast('customerInfo', $scope.dealAddModel.customer);
        //            }
        //        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
            $scope.noCustomer = '';
            $scope.selectedEstimate = '';
        }
        $scope.showReason = false;
        $scope.showReasonPopup = function ()
        {
            $scope.showReason = true;
        }
        $scope.closeReasonPopup = function ()
        {
            $scope.showReason = false;
        }
        $scope.showUploadFilePopup = false;
        $scope.showPopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            }

        }
        $scope.closePopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;

            }
        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
        }
        $scope.saveImagePath = function ()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {

                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                $scope.dealAddModel.attach = [];
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    var imageuploadparam = {};
                    imageuploadparam.path = $scope.uploadedFileQueue[i].urlpath;
                    imageuploadparam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                    $scope.dealAddModel.attach.push(imageuploadparam);

                }
                $scope.isImageSavingProcess = false;
                $scope.closePopup('fileupload');
            }

        }
        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.dealAddModel.customer = customerDetail;
        });

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isEmployeeLoaded && $scope.isStageLoaded && $scope.isDealTypeLoaded)
            {
                $scope.updateDealInfo();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $scope.leadEditInfo = false;
        $scope.leadEditDetail = function ()
        {
            $scope.leadEditInfo = true;
        }
        $scope.LeadInfoReset = function ()
        {
            $scope.leadEditInfo = false;
            $scope.getContactInfo();
        }

        $scope.dealStageLoading = false;
        $scope.updateDealInfo = function ()
        {
            if ($scope.dealDetail != null)
            {
                $scope.dealAddModel.id = parseInt($scope.dealDetail.id, 10);
                $scope.dealAddModel.productInfo =
                        {
                            name: $scope.dealDetail.name
                        };
                $scope.dealAddModel.stage = $scope.dealDetail.stage_id + '';
                $scope.dealAddModel.mrp_price = $scope.dealDetail.amount;
                $scope.dealAddModel.next_follow_action = $scope.dealDetail.next_follow_up_action_id + '';
                $scope.dealAddModel.lead_category = $scope.dealDetail.deal_category_id + '';
                $scope.dealAddModel.lead_category_type = $scope.dealDetail.lead_segment;
                $scope.dealAddModel.type = $scope.dealDetail.type;
                $scope.dealAddModel.customer =
                        {
                            id: $scope.dealDetail.contact_id,
                            fname: $scope.dealDetail.contact_name,
                            contact_number: $scope.dealDetail.contact_number
                        }
                var newclosedate = moment($scope.dealDetail.colsed_date).valueOf();
                console.log('Close date' + newclosedate);
                var newnextdate = moment($scope.dealDetail.next_follow_up).valueOf();
                console.log('Next date' + newnextdate);
                if (newclosedate > 0)
                {
                    console.log('close date type:' + typeof $scope.dealDetail.colsed_date);
                    $scope.dealAddModel.closedate = new Date($scope.dealDetail.colsed_date);
                } else
                {
                    $scope.dealAddModel.closedate = '';
                }
                if (newnextdate > 0)
                {
                    if ($scope.dealDetail.next_follow_up != null && $scope.dealDetail.next_follow_up != undefined && $scope.dealDetail.next_follow_up != '')
                    {
                        var nextdate = $scope.dealDetail.next_follow_up.split(' ');
                        $scope.dealAddModel.nextdate = new Date(nextdate[0]);
                    } else
                    {
                        $scope.dealAddModel.nextdate = '';
                    }
                } else
                    $scope.dealAddModel.nextdate = '';
                $scope.dealAddModel.employee = $scope.dealDetail.emp_id + '';
                $scope.dealAddModel.questionary_comments = $scope.dealDetail.questionary_comments;
                $scope.updateQuestion();
                $scope.getContactInfo();
            }
            $scope.dealStageLoading = true;
            $scope.dealAddModel.isLoadingProgress = false;
            $scope.isDataSavingProcess = false;
        }
        $scope.getContactInfo = function ()
        {
            $scope.isContactLoaded = false;
            $scope.dealAddModel.isLoadingProgress = true;
            var contactListParam = {};
            //  contactListParam.id = $scope.dealAddModel.customer.id;
            var headers = {};
            headers['screen-code'] = 'contact';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getContactDetails(contactListParam, $scope.dealAddModel.customer.id, configOption, headers).then(function (response) {

                var data = response.data;
                if (data.list.length > 0)
                {
                    $scope.dealAddModel.contactList = data.list[0];
                    $scope.isContactLoaded = true;
                    $scope.dealAddModel.isLoadingProgress = false;

                }
                $scope.initUpdateContactDetail();

            });

        };

        $scope.initUpdateContactTimeoutPromise = null;

        $scope.initUpdateContactDetail = function ()
        {

            if ($scope.initUpdateContactTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateContactTimeoutPromise);
            }

            if ($scope.isLeadTypeLoaded && $scope.isSourceLoaded)
            {
                $scope.updateContactInfo();
            } else
            {
                $scope.initUpdateContactTimeoutPromise = $timeout($scope.initUpdateContactDetail, 300);
            }
        }

        $scope.updateContactInfo = function ()
        {
            $scope.dealAddModel.is_active = $scope.dealAddModel.contactList.is_active;
            $scope.dealAddModel.customer_name = $scope.dealAddModel.contactList.fname;
            $scope.dealAddModel.company_address = $scope.dealAddModel.contactList.company_address;
            $scope.dealAddModel.website = $scope.dealAddModel.contactList.company_website;
            $scope.dealAddModel.mobile = $scope.dealAddModel.contactList.phone;
            $scope.dealAddModel.isLoadingProgress = false;
            //                  $scope.dealAddModel.id = $scope.dealAddModel.contactList.id;
            $scope.dealAddModel.companyInfo = {
                id: $scope.dealAddModel.contactList.company_id,
                name: $scope.dealAddModel.contactList.company_name
            }
            $scope.dealAddModel.source_id = $scope.dealAddModel.contactList.source_id + '';
            $scope.dealAddModel.sourceName = $scope.dealAddModel.contactList.source_name;
            $scope.dealAddModel.lead_category_type_id = $scope.dealAddModel.contactList.lead_type_id + '';
            $scope.dealAddModel.lead_category_type = $scope.dealAddModel.contactList.lead_type_name;
            $scope.leadEditInfo = false;
            $scope.leadEditButton = false;
            if ($scope.dealAddModel.contactList.contact_owner_id != $scope.dealDetail.emp_id)
            {
                $scope.leadEditButton = true;
            }

        }
        $scope.getCompanyList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCompanyModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.updateLeadInfo = function ()
        {
            $scope.isLeadSavingProcess = true;
            var addContactParam = {};
            addContactParam.id = $scope.dealAddModel.customer.id;
            addContactParam.fname = $scope.dealAddModel.customer_name;
            addContactParam.phone = $scope.dealAddModel.mobile;
            addContactParam.company_id = $scope.dealAddModel.companyInfo.id;
            addContactParam.source_id = $scope.dealAddModel.source_id;
            addContactParam.lead_type_id = $scope.dealAddModel.lead_category_type_id;
            addContactParam.is_active = 1;
            addContactParam.attachment = [];
            adminService.editContact(addContactParam, $scope.dealAddModel.customer.id).then(function (response)
            {
                if (response.data.success == true)
                {
                    $state.go('app.dealedit', {
                        'id': $stateParams.id
                    }, {
                        'reload': true
                    });
                    $scope.getContactInfo();
                }
                $scope.isLeadSavingProcess = false;
            });
        }
        $scope.updateQuestion = function ()
        {
            if ($scope.dealAddModel.questionary_comments != '' && $scope.dealAddModel.questionary_comments != 'undefined' && $scope.dealAddModel.questionary_comments != null)
            {
                var model = [];
                var j = 0;
                $scope.isDataSavingProcess = false;
                if ($scope.dealAddModel.questionary_comments.includes(","))
                {
                    model = $scope.dealAddModel.questionary_comments.split(',');
                    for (var i = 0; i < model.length; i++)
                    {
                        if (model[i].includes(":"))
                        {
                            var data = [];
                            data = model[i].split(':');
                            if (data[0] == 'concern_established')
                            {
                                $scope.dealQuestionModel.established = data[1].toLowerCase();
                            } else if (data[0] == 'post_printing_activities')
                            {
                                $scope.dealQuestionModel.printingactivities = data[1];
                            } else if (data[0] == 'available_machine_concern')
                            {
                                $scope.dealQuestionModel.machine = data[1].toLowerCase();
                            } else if (data[0] == 'daily_production')
                            {
                                $scope.dealQuestionModel.dailyproduction = data[1];
                            } else if (data[0] == 'own_services_call')
                            {
                                $scope.dealQuestionModel.ownCall = data[1];
                            }
                        }
                    }
                }
            } else
            {
                $scope.isDataSavingProcess = false;
                $scope.dealQuestionModel.established = '';
                $scope.dealQuestionModel.printingactivities = '';
                $scope.dealQuestionModel.machine = '';
                $scope.dealQuestionModel.dailyproduction = '';
                $scope.dealQuestionModel.ownCall = '';
            }
        }

        $scope.getStageList = function ()
        {
            $scope.isStageLoaded = false;
            var getListParam = {};
            getListParam.is_active = 1;
            var headers = {};
            headers['screen-code'] = 'stage';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStageList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.stageList = data.list;
                    $scope.dealAddModel.nextFollowList = data.list;
                    $scope.isStageLoaded = true;
                }
            });
        };
        $scope.generateInputJson = function ()
        {
            $scope.dealQuestionModel.inputJson = '';
            var mainString = '';
            mainString = mainString + 'concern_established:' + $scope.dealQuestionModel.established + ',';
            mainString = mainString + 'post_printing_activities:' + $scope.dealQuestionModel.printingactivities + ',';
            mainString = mainString + 'available_machine_concern:' + $scope.dealQuestionModel.machine + ',';
            mainString = mainString + 'daily_production:' + $scope.dealQuestionModel.dailyproduction + ',';
            mainString = mainString + 'own_services_call:' + $scope.dealQuestionModel.ownCall;
            $scope.dealQuestionModel.inputJson = mainString;
            $scope.isDataSavingProcess = false;
        };
        $scope.deleteActivity = function (id)
        {
            var getListParam = {};
            getListParam.id = id;
            var headers = {};
            headers['screen-code'] = 'deal';
            adminService.deleteActivityList(getListParam, getListParam.id, headers).then(function (response)
            {
                var data = response.data;
                if (data.success == true)
                {

                    $scope.getDealActivityList();
                }
                // $scope.isdeleteCategoryLoadingProgress = false;
            });
        }
        $scope.getDealActivityList = function ()
        {
            $scope.isDealActivityLoaded = false;
            var getListParam = {};
            getListParam.deal_id = $stateParams.id;
            getListParam.is_active = 1;
            getListParam.show = false;
            var headers = {};
            headers['screen-code'] = 'deal';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDealActivityList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.activityList = data.list;
                    for (var i = 0; i < $scope.dealAddModel.activityList.length; i++)
                    {
                        var newdate = new Date($scope.dealAddModel.activityList[i].created_at).toISOString();
                        $scope.dealAddModel.activityList[i].created_at_new = newdate;
                        $scope.dealAddModel.activityList[i].show = false;
                        if (typeof $scope.dealAddModel.activityList[i].attachment === 'string')
                        {
                            $scope.dealAddModel.activityList[i].attachment = [];
                        }
                        if ($scope.dealAddModel.activityList[i].attachment.length > 0)
                        {
                            for (var j = 0; j < $scope.dealAddModel.activityList[i].attachment.length; j++) {
                                $scope.dealAddModel.activityList[i].attachment[j].path = $scope.dealAddModel.activityList[i].attachment[j].url;
                            }
                        }
                    }
                    //                              if ($scope.isLoadedvm1)
                    //                              {
                    //                                    for (var i = 0; i < $scope.dealAddModel.activityList.length; i++)
                    //                                    {
                    //                                          if ($scope.dealAddModel.activityList[i].type == 'logActivity')
                    //                                          {
                    //                                                var newPos = {};
                    //                                                newPos.activityIndex = i;
                    //                                                newPos.lat = $scope.dealAddModel.activityList[i].latitude;
                    //                                                newPos.lat = $scope.dealAddModel.activityList[i].longitude;
                    //                                                $scope.vm1.positions.push(newPos);
                    //                                          }
                    //                                    }
                    //                              }
                    $scope.isDealActivityLoaded = true;
                }
                $scope.initUpdateMarkerDetail();
            });
        };
        //nisha fix timezone syed
        app.filter('timestampToISO', function () {
            return function (input) {
                input = new Date(input).toISOString();
                return input;
            };
        });
        $scope.getEmployeeList = function ()
        {
            $scope.isEmployeeLoaded = false;
            var getListParam = {};
            getListParam.is_active = 1;
            var headers = {};
            headers['screen-code'] = 'employee';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.dealAddModel.employeeList = data.list;
                    $scope.isEmployeeLoaded = true;
                }
            });
        };

        $scope.changeStatus = function (id)
        {
            // $scope.dealAddModel.stage = id;
            if (id != '')
            {
                for (var i = 0; i < $scope.dealAddModel.stageList.length; i++)
                {
                    if ($scope.dealAddModel.stageList[i].id == id)
                    {
                        $scope.dealAddModel.stageflag = $scope.dealAddModel.stageList[i].stage_flag;

                    }
                }
            }
            if ($scope.dealAddModel.stageflag.toLowerCase() == 'won' || $scope.dealAddModel.stageflag.toLowerCase() == 'loss')
            {
                $scope.showReason = true;
                $scope.dealAddModel.newStage = id;
                $scope.isReasonSavingProcess = false;

                if ($scope.dealAddModel.stageflag == 'won')
                {
                    $scope.dealAddModel.flagvalue = 'Won';
                }
                if ($scope.dealAddModel.stageflag == 'loss')
                {
                    $scope.dealAddModel.flagvalue = 'Loss';
                }
            } else
            {
                $scope.dealAddModel.stage = id;
                $scope.showReason = false;
                $scope.modifyDeal();
            }
        };
        $scope.changeStage = function (id)
        {
            if (id != '')
            {
                for (var i = 0; i < $scope.dealAddModel.stageList.length; i++)
                {
                    if ($scope.dealAddModel.stageList[i].id == id)
                    {
                        $scope.dealAddModel.stageflag = $scope.dealAddModel.stageList[i].stage_flag;

                    }
                }
            }
            if ($scope.dealAddModel.stageflag.toLowerCase() == 'won' || $scope.dealAddModel.stageflag.toLowerCase() == 'loss')
            {
                $scope.showReason = true;
                $scope.dealAddModel.newStage = id;
                $scope.isReasonSavingProcess = false;

                if ($scope.dealAddModel.stageflag == 'won')
                {
                    $scope.dealAddModel.flagvalue = 'Won';
                }
                if ($scope.dealAddModel.stageflag == 'loss')
                {
                    $scope.dealAddModel.flagvalue = 'Loss';
                }
            } else
            {
                $scope.dealAddModel.stage = id;
                $scope.showReason = false;
            }

        };
        $scope.progressIndicatorStatus = function (indicatorValue, id, currentIndex)
        {
            var retVal = false;
            var stage = '';
            var index = '';
            for (var i = 0; i < $scope.dealAddModel.stageList.length; i++)
            {
                if ($scope.dealAddModel.stageList[i].id == $scope.dealAddModel.stage)
                {
                    stage = parseInt($scope.dealAddModel.stageList[i].level);
                    index = i;
                }
            }
            if ($scope.dealStageLoading == true)
            {
                if (stage > indicatorValue)
                {
                    retVal = true;
                } else if (stage = indicatorValue)
                {
                    if (id == $scope.dealAddModel.stage)
                        retVal = true;
                    else
                    {
                        if (currentIndex <= index)
                            retVal = true;
                    }
                } else
                    retVal = false;
            }
            return retVal;

        };
        $scope.getProductInfo = function (val) {
            var getGroupListParam = {};
            getGroupListParam.name = val;
            getGroupListParam.type = 'product';
            getGroupListParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, getGroupListParam, false).then(function (responseData) {
                var data = responseData.data;
                var hits = data.list;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function (model) {

            if (model != null)
            {
                if (model.name != undefined && model.sku != undefined)
                {
                    return model.name;
                } else if (model.name != undefined && model.hsn_code != undefined)
                {
                    return model.name;
                }
                return model.name;
            }
            return  '';
        };
        $scope.updateProductInfo = function (model)
        {
            if (model != null)
            {
                $scope.dealAddModel.mrp_price = model.mrp_price;
            }
        }
        $scope.resetAmount = function ()
        {
            if ($scope.dealAddModel.productInfo == null || $scope.dealAddModel.productInfo == 'undefined' || $scope.dealAddModel.productInfo == '')
            {
                $scope.dealAddModel.mrp_price = '';
            }
        }

        $scope.modifyDeal = function () {
            if ($scope.dealAddModel.nextdate > $scope.dealAddModel.closedate)
            {
                sweet.show("Oops", "Next followup date shouldn't  exceed the deal close date", "error");
            } else
            {
                if ($scope.isDataSavingProcess == false)
                {
                    $scope.isDataSavingProcess = true;
                    $scope.generateInputJson();
                    var createParam = {};
                    var headers = {};
                    headers['screen-code'] = 'deal';
                    createParam.questionary_comments = $scope.dealQuestionModel.inputJson;
                    createParam.name = '';
                    if ($scope.dealAddModel.productInfo != '' && $scope.dealAddModel.productInfo.name != '' && $scope.dealAddModel.productInfo.name != undefined && $scope.dealAddModel.productInfo.name != null) {
                        createParam.name = $scope.dealAddModel.productInfo.name;
                    } else
                    {
                        createParam.name = $scope.dealAddModel.productInfo;
                    }
                    //                createParam.name = $scope.dealAddModel.name;
                    createParam.stage_id = $scope.dealAddModel.stage;
                    for (var i = 0; i < $scope.dealAddModel.stageList.length; i++)
                    {
                        if ($scope.dealAddModel.stageList[i].id == $scope.dealAddModel.stage)
                        {
                            createParam.stage_name = $scope.dealAddModel.stageList[i].name;
                        }
                    }
                    createParam.next_follow_up_action_id = $scope.dealAddModel.next_follow_action;
                    for (var i = 0; i < $scope.dealAddModel.nextFollowList.length; i++)
                    {
                        if ($scope.dealAddModel.nextFollowList[i].id == $scope.dealAddModel.next_follow_action)
                        {
                            createParam.next_follow_up_action = $scope.dealAddModel.nextFollowList[i].name;
                        }
                    }
                    createParam.type = $scope.dealAddModel.type;
                    createParam.deal_category_id = $scope.dealAddModel.lead_category;
                    createParam.amount = $scope.dealAddModel.mrp_price;
                    createParam.contact_id = $scope.dealAddModel.customer.id;
                    createParam.contact_name = $scope.dealAddModel.customer.fname;
                    //createParam.next_follow_up_action = $scope.dealAddModel.next_follow_action;
                    createParam.is_active = 1;
                    //                if ($rootScope.userModel.rolename != 'superadmin')
                    //                {
                    createParam.emp_id = $scope.dealAddModel.employee;
                    for (var i = 0; i < $scope.dealAddModel.employeeList.length; i++)
                    {
                        if ($scope.dealAddModel.employeeList[i].id == $scope.dealAddModel.employee)
                        {
                            createParam.emp_name = $scope.dealAddModel.employeeList[i].f_name;
                        }
                    }
                    //                } else
                    //{
                    //                        createParam.user_id = $scope.dealAddModel.user;
                    //                    for (var i = 0; i < $scope.dealAddModel.userList.length; i++)
                    //                    {
                    //                        if ($scope.dealAddModel.userList[i].id == $scope.dealAddModel.user)
                    //                        {
                    //                            createParam.user_name = $scope.dealAddModel.userList[i].f_name;
                    //                        }
                    //                    }
                    //                }
                    if ($scope.dealAddModel.closedate != '')
                    {
                        if ($scope.dealAddModel.closedate != null && typeof $scope.dealAddModel.closedate == 'object')
                        {
                            createParam.colsed_date = utilityService.parseDateToStr($scope.dealAddModel.closedate, $scope.dateFormat);
                            createParam.colsed_date = utilityService.changeDateToSqlFormat(createParam.colsed_date, $scope.dateFormat);
                        }

                    }
                    if ($scope.dealAddModel.nextdate != '')
                    {
                        if ($scope.dealAddModel.nextdate != null && typeof $scope.dealAddModel.nextdate == 'object')
                        {
                            createParam.next_follow_up = utilityService.parseDateToStr($scope.dealAddModel.nextdate, $scope.dateFormat);
                            createParam.next_follow_up = utilityService.changeDateToSqlFormat(createParam.next_follow_up, $scope.dateFormat);
                        }
                    }
                    adminService.editDeal(createParam, $stateParams.id, adminService.handleSuccessAndErrorResponse, headers).then(function (response) {
                        if (response.data.success == true)
                        {
                            $scope.formReset(0);
                            $state.go('app.dealedit', {
                                'id': $stateParams.id
                            }, {
                                'reload': true
                            });
                        }
                        $scope.isDataSavingProcess = false;
                    });
                }
            }

        };

        $scope.getdealListInfo = function ()
        {
            $scope.id = '';
            $scope.dealAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
//            getListParam.is_active = 1;
            var headers = {};
            headers['screen-code'] = 'deal';
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDealList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.dealDetail = data.list[0];
                        $scope.id = $stateParams.id;
                        $scope.getStageList();
                        $scope.getEmployeeList();
                        $scope.initUpdateDetail();
                        $scope.getDealActivityList();
                    }
                }

            });
        };

        $scope.createInvoice = function (id)
        {
            if ($scope.currentStep === 3)
            {
                console.log("Check for customer");
                $scope.createOrder();
                //$state.go('app.dealInvoiceAdd', {'deal_id': $stateParams.id});
            }
            if ($scope.currentStep === 4)
            {
                console.log("Redirecting to Invoice add");
                $state.go('app.dealInvoiceAdd', {
                    'id': id,
                    'deal_id': $stateParams.id
                });
            }
        };
        $scope.createTask = function ()
        {
            if ($scope.currentStep === 6)
            {
                console.log("Redirecting to TMS");
                $state.go('app.taskManagementAdd', {
                    'deal_id': $stateParams.id
                });
            }
        };

        $scope.createQuote = function ()
        {
            console.log("Redirecting to quote");
            $state.go('app.dealQuoteAdd', {
                'id': $stateParams.id,
                'cust_id': $scope.dealAddModel.customer.id
            });
        };

        $scope.noCustomer = '';
        $scope.selectedEstimate = '';

        $scope.createOrderFromEstimate = function (estimate_id)
        {
            $scope.selectedEstimate = estimate_id;
            $scope.createOrder();
        };

        $scope.createOrder = function ()
        {
            var contactId = $scope.dealAddModel.customer.id;
            var getListParam = {};
            getListParam.contact_id = contactId;
            getListParam.name = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.is_active = 1;
            $scope.showCustomerAddPopup = false;
            $scope.noCustomer = false;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCustomersList(getListParam, configOption).then(function (response) {
                var data = response.data;
                $scope.dealAddModel.customerList = [];
                $scope.dealAddModel.customerList = data.list;
                //                    for(var i=0; i<data.list.length; i++)
                //                    {
                //                        $scope.dealAddModel.customerList.push(data.list[i]);
                //                    }
                if ($scope.dealAddModel.customerList.length > 0)
                {
                    $scope.showCustomerAddPopup = true;
                    $scope.noCustomer = false;
                }
                if ($scope.dealAddModel.customerList.length === 0)
                {
                    $scope.showCustomerAddPopup = true;
                    $scope.noCustomer = true;
                }
            });
        };

        $scope.redirectToOrder = function (custId)
        {
            if ($scope.currentStep === 3)
            {
                console.log("Redirecting to invoice add with cutomer id");
                $state.go('app.dealInvoiceAdd', {
                    'deal_id': $stateParams.id,
                    'cust_id': custId
                });
            } else
            {
                if ($scope.selectedEstimate !== '' && typeof $scope.selectedEstimate !== 'undefined' && $scope.selectedEstimate !== null)
                {
                    console.log("Redirecting to sales estimate with customer id");
                    $state.go('app.dealsalesorderAdd', {
                        'id': $scope.selectedEstimate,
                        'deal_id': $stateParams.id,
                        'cust_id': custId
                    });
                }
                if ($scope.selectedEstimate === '' || typeof $scope.selectedEstimate === 'undefined' || $scope.selectedEstimate === null)
                {
                    console.log("Redirecting to sales order with customer id");
                    $state.go('app.dealsalesorderAdd', {
                        'deal_id': $stateParams.id,
                        'cust_id': custId
                    });
                }
            }
        };

        $scope.redirectToCustomer = function ()
        {
            console.log("Redirecting to customer add");
            $state.go('app.customeradd', {
                'id': $scope.dealAddModel.customer.id,
                'deal_id': $stateParams.id
            });
        };

        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.createNotes = function ()
        {
            if ($scope.isDataSavingProcess === false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'deal';
                createParam.description = $scope.dealAddModel.notes;
                createParam.type = 'note';
                createParam.deal_id = $stateParams.id;
                createParam.comments = 'left a note';
                createParam.contact_id = $scope.dealAddModel.customer.id;
                //createParam.customer_name = $scope.dealAddModel.customer.fname;
                createParam.is_active = 1;
                createParam.date = $filter('date')($scope.currentDate, 'yyyy-MM-dd');
//                createParam.user_id = $rootScope.userModel.id;
//                createParam.user_name = $rootScope.userModel.f_name;
                createParam.log_activity_type = '';
                createParam.log_activity_sub_type = '';
                createParam.time = '';
                createParam.attachment = [];
                var arr = [];
                for (var i = 0; i < $scope.dealAddModel.attach.length; i++)
                {
                    var att = {};
                    att.file_path = $scope.dealAddModel.attach[i].urlpath;
                    arr.push(att);
                }
                createParam.attachment = arr;
                adminService.addDealActivity(createParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset(1);
                        $state.go('app.dealedit', {
                            'id': $stateParams.id
                        }, {
                            'reload': true
                        });
                        // $state.go('app.invoiceView1', {'id': response.data.id}, {'reload': true});
                    }
                    $scope.isDataSavingProcess = false;
                });
            }

        };
        $scope.createReason = function ()
        {
            if ($scope.isReasonSavingProcess === false)
            {
                $scope.isReasonSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'deal';
                createParam.description = $scope.dealAddModel.flagvalue + ' ' + 'Reason' + ' ' + ':' + ' ' + $scope.dealAddModel.reason;
                createParam.type = 'note';
                createParam.deal_id = $stateParams.id;
                createParam.comments = 'left a note';
                createParam.contact_id = $scope.dealAddModel.customer.id;
                //createParam.customer_name = $scope.dealAddModel.customer.fname;
                createParam.is_active = 1;
                if (typeof $scope.dealAddModel.reasondate === 'object')
                {
                    $scope.dealAddModel.reasondate = utilityService.parseStrToDate($scope.dealAddModel.reasondate, $scope.adminService.appConfig.date_format);
                }
                createParam.date = utilityService.changeDateToSqlFormat($scope.dealAddModel.reasondate, $scope.adminService.appConfig.date_format);
                createParam.user_id = $rootScope.userModel.id;
                createParam.user_name = $rootScope.userModel.f_name;
                createParam.log_activity_type = '';
                createParam.log_activity_sub_type = '';
                createParam.time = '';
                createParam.attachment = [];
                adminService.addDealActivity(createParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.dealAddModel.stage = $scope.dealAddModel.newStage;
                        $scope.modifyDeal();

                        // $state.go('app.invoiceView1', {'id': response.data.id}, {'reload': true});
                    }
                    $scope.isReasonSavingProcess = false;
                });
            }

        };

        $scope.createLog = function ()
        {
            if ($scope.isDataSavingProcess === false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'deal';
                createParam.description = $scope.dealAddModel.notes;
                createParam.type = 'logActivity';
                createParam.log_activity_sub_type = '';
                createParam.comments = '';
                createParam.deal_id = $stateParams.id;
                createParam.log_activity_type = $scope.dealAddModel.log_type;
                createParam.contact_id = $scope.dealAddModel.customer.id;
                createParam.attachments = [];
                if (typeof $scope.dealAddModel.log_subtype !== 'undefined' && $scope.dealAddModel.log_subtype !== null && $scope.dealAddModel.log_type === 'call')
                {
                    createParam.log_activity_sub_type = $scope.dealAddModel.log_subtype;
                    createParam.comments = 'made a call';
                    createParam.description = $scope.dealAddModel.notes + ' -- Call outcome: ' + $scope.dealAddModel.log_subtype + '-- ';
                }
                if ($scope.dealAddModel.log_type === 'email')
                {
                    createParam.comments = 'sent an email';
                    //createParam.description = '';
                }
                if ($scope.dealAddModel.log_type === 'meeting')
                {
                    createParam.comments = 'arranged a meeting';
                    //createParam.description = '';
                }
                //createParam.customer_id = $scope.dealAddModel.customer.id;
                //createParam.customer_name = $scope.dealAddModel.customer.fname;
                createParam.latitude = $scope.dealAddModel.latitude;
                createParam.longitude = $scope.dealAddModel.longitude;
                //                                console.log($scope.map.center.latitude);
                //                                console.log($scope.map.center.longitude);
                createParam.is_active = 1;
                createParam.date = $filter('date')($scope.dealAddModel.activity_date, 'yyyy-MM-dd');
                createParam.time = $filter('date')($scope.dealAddModel.activity_time, 'shortTime');
                createParam.user_id = $rootScope.userModel.id;
                createParam.user_name = $rootScope.userModel.f_name;


                adminService.addDealActivity(createParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset(3);
                        $state.go('app.dealedit', {
                            'id': $stateParams.id
                        }, {
                            'reload': true
                        });
                        // $state.go('app.invoiceView1', {'id': response.data.id}, {'reload': true});
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.initEstimateTableFilterTimeoutPromise = null;

        $scope.initEstimateTableFilter = function ()
        {
            if ($scope.initEstimateTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initEstimateTableFilterTimeoutPromise);
            }
            $scope.initEstimateTableFilterTimeoutPromise = $timeout($scope.getestimateListInfo, 300);
        }

        $scope.getestimateListInfo = function ()
        {
            $scope.dealAddModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesestimate';

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            getListParam.id = '';
            getListParam.deal_id = $stateParams.id;
            getListParam.start = ($scope.dealAddModel.estimatecurrentPage - 1) * $scope.dealAddModel.estimatelimit;
            getListParam.limit = $scope.dealAddModel.estimatelimit;
            getListParam.is_active = 1;
            getListParam.status = '';
            adminService.getQuoteList(getListParam, configOption, headers).then(function (response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.dealAddModel.estimateList = data.list;
                    if ($scope.dealAddModel.estimateList.length > 0)
                    {
                        for (var i = 0; i < $scope.dealAddModel.estimateList.length; i++)
                        {
                            $scope.dealAddModel.estimateList[i].newdate = utilityService.parseStrToDate($scope.dealAddModel.estimateList[i].date);
                            $scope.dealAddModel.estimateList[i].date = utilityService.parseDateToStr($scope.dealAddModel.estimateList[i].newdate, $scope.adminService.appConfig.date_format);
                            $scope.dealAddModel.estimateList[i].validuntildate = utilityService.parseStrToDate($scope.dealAddModel.estimateList[i].validuntil);
                            $scope.dealAddModel.estimateList[i].validuntil = utilityService.parseDateToStr($scope.dealAddModel.estimateList[i].validuntildate, $scope.adminService.appConfig.date_format);
                            $scope.dealAddModel.estimateList[i].total_amount = utilityService.changeCurrency($scope.dealAddModel.estimateList[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.dealAddModel.estimateList[i].advance_amount = parseFloat(utilityService.changeCurrency($scope.dealAddModel.estimateList[i].advance_amount, $rootScope.appConfig.thousand_seperator)).toFixed(2);

                        }
                    }
                    $scope.dealAddModel.estimatetotal = data.total;
                }
                $scope.dealAddModel.isLoadingProgress = false;
            });
        };

        $scope.initOrderTableFilterTimeoutPromise = null;

        $scope.initOrderTableFilter = function ()
        {
            if ($scope.initOrderTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initOrderTableFilterTimeoutPromise);
            }
            $scope.initOrderTableFilterTimeoutPromise = $timeout($scope.getorderListInfo, 300);
        }

        $scope.getorderListInfo = function ()
        {
            $scope.dealAddModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesorder';

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            getListParam.id = '';
            getListParam.deal_id = $stateParams.id;
            getListParam.start = ($scope.dealAddModel.ordercurrentPage - 1) * $scope.dealAddModel.orderlimit;
            getListParam.limit = $scope.dealAddModel.orderlimit;
            getListParam.is_active = 1;
            getListParam.status = '';
            adminService.getSalesOrderList(getListParam, configOption, headers).then(function (response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.dealAddModel.orderList = data.list;
                    if ($scope.dealAddModel.orderList.length > 0)
                    {
                        for (var i = 0; i < $scope.dealAddModel.orderList.length; i++)
                        {
                            $scope.dealAddModel.orderList[i].newdate = utilityService.parseStrToDate($scope.dealAddModel.orderList[i].date);
                            $scope.dealAddModel.orderList[i].date = utilityService.parseDateToStr($scope.dealAddModel.orderList[i].newdate, $scope.adminService.appConfig.date_format);
                            $scope.dealAddModel.orderList[i].validuntildate = utilityService.parseStrToDate($scope.dealAddModel.orderList[i].validuntil);
                            $scope.dealAddModel.orderList[i].validuntil = utilityService.parseDateToStr($scope.dealAddModel.orderList[i].validuntildate, $scope.adminService.appConfig.date_format);
                            $scope.dealAddModel.orderList[i].total_amount = utilityService.changeCurrency($scope.dealAddModel.orderList[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.dealAddModel.orderList[i].advance_amount = parseFloat(utilityService.changeCurrency($scope.dealAddModel.orderList[i].advance_amount, $rootScope.appConfig.thousand_seperator)).toFixed(2);

                        }
                    }
                    $scope.dealAddModel.ordertotal = data.total;
                }
                $scope.dealAddModel.isLoadingProgress = false;
            });
        };

        $scope.initInvoiceTableFilterTimeoutPromise = null;

        $scope.initInvoiceTableFilter = function ()
        {
            if ($scope.initInvoiceTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initInvoiceTableFilterTimeoutPromise);
            }
            $scope.initInvoiceTableFilterTimeoutPromise = $timeout($scope.getinvoiceListInfo, 300);
        }

        $scope.getinvoiceListInfo = function ()
        {
            $scope.dealAddModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesinvoice';

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            getListParam.id = '';
            getListParam.deal_id = $stateParams.id;
            getListParam.start = ($scope.dealAddModel.invoicecurrentPage - 1) * $scope.dealAddModel.invoicelimit;
            getListParam.limit = $scope.dealAddModel.invoicelimit;
            getListParam.is_active = 1;
            getListParam.status = '';
            adminService.getInvoiceList(getListParam, configOption, headers).then(function (response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.dealAddModel.invoiceList = data.list;
                    if ($scope.dealAddModel.invoiceList.length > 0)
                    {
                        for (var i = 0; i < $scope.dealAddModel.invoiceList.length; i++)
                        {
                            $scope.dealAddModel.invoiceList[i].newdate = utilityService.parseStrToDate($scope.dealAddModel.invoiceList[i].date);
                            $scope.dealAddModel.invoiceList[i].date = utilityService.parseDateToStr($scope.dealAddModel.invoiceList[i].newdate, $scope.adminService.appConfig.date_format);
                            $scope.dealAddModel.invoiceList[i].newduedate = utilityService.parseStrToDate($scope.dealAddModel.invoiceList[i].duedate);
                            $scope.dealAddModel.invoiceList[i].duedate = utilityService.parseDateToStr($scope.dealAddModel.invoiceList[i].newduedate, $scope.adminService.appConfig.date_format);
                            $scope.dealAddModel.invoiceList[i].total_amount = utilityService.changeCurrency($scope.dealAddModel.invoiceList[i].total_amount, $rootScope.appConfig.thousand_seperator);


                        }
                    }
                    $scope.dealAddModel.invoicetotal = data.total;
                }
                $scope.dealAddModel.isLoadingProgress = false;
            });
        };

        $scope.actCenterLat = '';
        $scope.actCenterLng = '';
        $scope.newCenterLat = '';
        $scope.newCenterLng = '';
        $scope.showExistingLocationPopup = false;
        $scope.showMapPopup = false;
        $scope.showLocationPopup = function (type, index)
        {
            if (type == 'list')
            {
                $scope.vm1.positions = [];
                var newPos = {};
                newPos.lat = parseFloat($scope.dealAddModel.activityList[index].latitude);
                newPos.lng = parseFloat($scope.dealAddModel.activityList[index].longitude);
                $scope.actCenterLat = newPos.lat;
                $scope.actCenterLng = newPos.lng;
                $scope.vm1.positions.push(newPos);
                $scope.showExistingLocationPopup = true;
            } else if (type == 'new')
            { // New Activity 
                $scope.vm2.positions = [];
                var newPos = {};
                newPos.lat = $scope.dealAddModel.latitude;
                newPos.lng = $scope.dealAddModel.longitude;
                $scope.newCenterLat = newPos.lat;
                $scope.newCenterLng = newPos.lng;
                $scope.vm2.positions.push(newPos);
                $scope.showMapPopup = true;
            }

        };
        $scope.closeLocationPopup = function (type)
        {
            if (type == 'list')
            {
                $scope.showExistingLocationPopup = false;
            } else if (type == 'new')
            {
                $scope.showMapPopup = false;
                $scope.dealAddModel.latitude = APP_CONST.DEFAULT_GEO_POINT.LAT;
                $scope.dealAddModel.longitude = APP_CONST.DEFAULT_GEO_POINT.LNG;
                $scope.vm2.address = '';
            }
        };

        $scope.updateLocation = function ()
        {
            $scope.showMapPopup = false;
            //  $scope.vm2.address = '';
        };

        $scope.getTaskManagementList = function ()
        {
            $scope.dealAddModel.isLoadingProgress = true;

            var headers = {};
            headers['screen-code'] = 'taskManagement';
            var getListParam = {};
            getListParam.is_active = 1;
            getListParam.id = '';
            getListParam.ref_id = $stateParams.id;
            getListParam.ref_name = 'deal';
            getListParam.start = ($scope.dealAddModel.taskcurrentPage - 1) * $scope.dealAddModel.tasklimit;
            getListParam.limit = $scope.dealAddModel.tasklimit;
            var getArrayParam = {};
            getArrayParam.assign_to = [];
            getArrayParam.type = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaskMgtDetail(getArrayParam, configOption, getListParam.is_active, getListParam.id, getListParam.ref_id, getListParam.ref_name, getListParam.start, getListParam.limit, headers).then(function (response)
            {
                var data = response.data;
                if (data.success)
                {
                    $scope.dealAddModel.taskMngList = data.list;
                    $scope.dealAddModel.tasktotal = data.total;
                }
                $scope.dealAddModel.isLoadingProgress = false;
            });
        };


        $scope.isLoadedvm1 = false;
        $scope.vm1 = this;
        $scope.isLoadedvm2 = false;
        $scope.vm2 = this;
        NgMap.getMap({
            id: 'vm1'
        }).then(function (map) {
            $scope.vm1.map = map;
            $scope.vm2.map = map;
            $scope.isLoadedvm1 = true;
            $scope.vm1.positions = [{
                    lat: 37.7699298,
                    lng: -122.4469157
                }];
            $scope.vm1.deleteMarkers = function () {
                $scope.vm1.positions = [];
            };
            $scope.vm1.showMarkers = function () {
                for (var key in $scope.vm1.map.markers) {
                    $scope.vm1.map.markers[key].setMap($scope.vm1.map);
                }
            };
            $scope.vm1.hideMarkers = function () {
                for (var key in $scope.vm1.map.markers) {
                    $scope.vm1.map.markers[key].setMap(null);
                }
            };
        });

        NgMap.getMap({
            id: 'vm2'
        }).then(function (map) {
            $scope.vm2.map = map;
            //                  $scope.vm2.types = "['establishment']";
            $scope.isLoadedvm2 = true;
            $scope.vm2.positions = [
                {
                    lat: APP_CONST.DEFAULT_GEO_POINT.LAT,
                    lng: APP_CONST.DEFAULT_GEO_POINT.LNG
                }];

            $scope.vm2.dragEnd = function ()
            {
                console.log(map.getCenter());
                $scope.vm2.positions = [];
                var newPos = {};
                newPos.lat = $scope.vm2.map.getCenter().lat();
                newPos.lng = $scope.vm2.map.getCenter().lng();
                $scope.newCenterLat = newPos.lat;
                $scope.newCenterLng = newPos.lng;
                $scope.dealAddModel.latitude = newPos.lat;
                $scope.dealAddModel.longitude = newPos.lng;
                $scope.vm2.positions.push(newPos);
            }

            $scope.vm2.markerDragEnd = function ($event)
            {
                console.log($event.latLng.lat());
                console.log($event.latLng.lng());
                $scope.dealAddModel.latitude = $event.latLng.lat();
                $scope.dealAddModel.longitude = $event.latLng.lng();
            }

            $scope.vm2.placeChanged = function () {
                $scope.vm2.place = this.getPlace();
                //                        for (var key in $scope.vm1.map.markers) {
                //                              $scope.vm1.map.markers[key].setPosition($scope.vm2.place.geometry.location.lat, $scope.vm2.place.geometry.location.lng);
                //                        }
                $scope.vm2.positions = [];
                var newPos = {};
                newPos.lat = $scope.vm2.place.geometry.location.lat();
                newPos.lng = $scope.vm2.place.geometry.location.lng();
                $scope.newCenterLat = newPos.lat;
                $scope.newCenterLng = newPos.lng;
                $scope.dealAddModel.latitude = newPos.lat;
                $scope.dealAddModel.longitude = newPos.lng;
                $scope.vm2.positions.push(newPos);
                console.log('location', $scope.vm2.place.geometry.location);
                $scope.vm2.map.setCenter($scope.vm2.place.geometry.location);
            }
        });

        $scope.initUpdateMarkerTimeoutPromise = null;

        $scope.initUpdateMarkerDetail = function ()
        {
            if ($scope.initUpdateMarkerTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateMarkerTimeoutPromise);
            }
            if ($scope.isLoadedvm1 && $scope.isDealActivityLoaded && $scope.isLoadedvm2)
            {
                $scope.staticMap = {
                    location: $scope.vm2.address,
                    locationAbout: "Try an address, a city, a place, or even latitude and longitude.",
                    API: "AIzaSyB-n6hREweTj6hYzxj9-nm7T7tapVBzMlc",
                    APIAbout: "You don't always need an API Key for Static Maps, but it's easy to acquire one. Without a key you might receive an error image instead of a map. Follow the link to the API Console.",
                    zoom: 13,
                    minZoom: 0,
                    maxZoom: 22,
                    scaleAbout: "Scale will double the stated height and width. This is good for when you need a width or height larger than 640px.",
                    width: 600,
                    height: 300,
                    maxSize: 640,
                    sizeAbout: "Max is 640px or 1280px when scale 2x.",
                    markerColor: "red",
                    mapType: "roadmap",
                    format: "png",
                    markerSize: "mid",
                    gimmeAbout: "Treat this like a regular image. Pop it into an img tag or use as a background-image."
                };

                //$scope.updateMarkerDetail();
            } else
            {
                $scope.initUpdateMarkerTimeoutPromise = $timeout($scope.initUpdateMarkerDetail, 300);
            }
        }
        $scope.showOptions = false;
        $scope.openOptions = function (index)
        {
            for (var i = 0; i < $scope.dealAddModel.activityList.length; i++)
            {
                $scope.dealAddModel.activityList[i].show = false;
            }
            $scope.dealAddModel.activityList[index].show = true;
            $scope.showOptions = true;
        }

        $scope.closeOptions = function (index)
        {
            for (var i = 0; i < $scope.dealAddModel.activityList.length; i++)
            {
                $scope.dealAddModel.activityList[i].show = false;
            }
            $scope.dealAddModel.activityList[index].show = false;
            $scope.showOptions = false;
        }

        $scope.updateMarkerDetail = function ()
        {
            for (var i = 0; i < $scope.dealAddModel.activityList.length; i++)
            {
                if ($scope.dealAddModel.activityList[i].type == 'logActivity')
                {
                    var newPos = {};
                    newPos.activityIndex = i;
                    newPos.lat = parseFloat($scope.dealAddModel.activityList[i].latitude);
                    newPos.lng = parseFloat($scope.dealAddModel.activityList[i].longitude);
                    $scope.vm1.positions.push(newPos);
                }
            }
            for (var i = 0; i < $scope.vm1.map.markers.length; i++)
            {
                $scope.vm1.map.markers[i].setMap(null);
            }
            $scope.vm1.hideMarkers();
            console.log($scope.vm1.positions);
            console.log($scope.vm2.positions);
        }
        $scope.selectedImgList = [];
        $scope.scrollTo = function (image, $index, $parentIndex)
        {
            $scope.selectedImgList = $scope.dealAddModel.activityList[$parentIndex].attachment;
            $scope.showdeImgPopup = true;
            $scope.image = image;
        }
        $scope.closeImgPopup = function () {
            $scope.selectedImgList = [];
            $scope.showdeImgPopup = false;
        }



        $scope.addNewFollower = function ()
        {
            var addFollowerParam = {};
            var createFollowerParam = [];
            var existFollower = false;
            if ($scope.dealAddModel.employeeInfo.id != null && $scope.dealAddModel.employeeInfo.id != '' && $scope.dealAddModel.employeeInfo != undefined)
            {
                addFollowerParam.id = 0;
                addFollowerParam.deal_id = $stateParams.id;
                addFollowerParam.emp_id = $scope.dealAddModel.employeeInfo.id;
                createFollowerParam.push(addFollowerParam);
                for (var i = 0; i < $scope.dealAddModel.followersList.length; i++)
                {
                    if ($scope.dealAddModel.followersList[i].emp_id == addFollowerParam.emp_id)
                    {
                        sweet.show('Oops...', 'Follower name is already exist', 'error');
                        $scope.dealAddModel.employeeInfo = '';
                        existFollower = true;
                    }
                }
                if (existFollower == false)
                {
                    var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                    adminService.saveFollowers(createFollowerParam, configOption).then(function (response) {
                        if (response.data.success == true)
                        {
                            $scope.dealAddModel.employeeInfo = '';
                            $scope.getFollowerList();
                            $scope.isDataSavingProcess = false;
                        }
                    });
                }
            }
        };
        $scope.getEmployeeSearchList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeModel = function (model)
        {
            if (model != null)
            {
                if (model.f_name != undefined && model.team_name != undefined && model.emp_code != undefined)
                {
                    return model.f_name + '(' + model.team_name + ',' + model.emp_code + ')';
                } else if (model.f_name != undefined && model.team_name != undefined && model.company != undefined)
                {
                    return model.f_name + '(' + model.team_name + ')';
                }
                return model.f_name;
            }
            return  '';
        };

        $scope.getFollowerList = function ()
        {
            var followerListParam = {};
            followerListParam.is_active = 1;
            followerListParam.dealId = $stateParams.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDealFollowerList(followerListParam, configOption).then(function (response)
            {
                if (response.data.success == true)
                {
                    var data = response.data;
                    $scope.dealAddModel.followersList = data.list;
                }
                $scope.dealAddModel.isLoadingProgress = false;
            });
        }
        $scope.deleteFollower = function (id)
        {
            var getListParam = {};
            getListParam.id = id;
            adminService.deleteDealFollower(getListParam, getListParam.id).then(function (response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.getFollowerList();
                }
            });
        }
        $scope.isSourceLoaded = false;
        $scope.getSourceList = function () {
            var getSourceListParam = {};
            getSourceListParam.is_active = 1;
            $scope.dealAddModel.isLoadingProgress = true;
            $scope.isSourceLoaded = false;
            var headers = {};
            headers['screen-code'] = 'source';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getSourceList(getSourceListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    $scope.dealAddModel.sourceList = response.data.list;
                }
                $scope.isSourceLoaded = true;
                $scope.dealAddModel.isLoadingProgress = false;
            });
        };
        $scope.isLeadTypeLoaded = false;
        $scope.getLeadTypeList = function ()
        {
            $scope.dealAddModel.isLoadingProgress = true;
            var leadtypeListParam = {};
            leadtypeListParam.is_active = 1;
            leadtypeListParam.type = 'lead';
            $scope.isLeadTypeLoaded = false;
            var headers = {};
            headers['screen-code'] = 'leadType';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getLeadTypeList(leadtypeListParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    var data = response.data;
                    $scope.dealAddModel.leadList = data.list;
                }
                $scope.isLeadTypeLoaded = true;
                $scope.dealAddModel.isLoadingProgress = false;
            });

        }

        $scope.getDealTypeList = function ()
        {
            $scope.dealAddModel.isLoadingProgress = true;
            var leadtypeListParam = {};
            leadtypeListParam.is_active = 1;
            leadtypeListParam.type = 'deal';
            $scope.dealAddModel.dealTypeList = [];
            $scope.isDealTypeLoaded = false;
            var headers = {};
            headers['screen-code'] = 'leadType';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getLeadTypeList(leadtypeListParam, configOption, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    var data = response.data;
                    $scope.dealAddModel.dealTypeList = data.list;
                }
                $scope.isDealTypeLoaded = true;
                $scope.dealAddModel.isLoadingProgress = false;
            });

        };
        $scope.getSourceList();
        $scope.getLeadTypeList();
        $scope.getFollowerList();
        $scope.getDealTypeList();
        $scope.getTaskManagementList();
        $scope.getinvoiceListInfo();
        $scope.getorderListInfo();
        $scope.getestimateListInfo();
        $scope.getdealListInfo();

    }]);




