

app.controller('groupAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$window', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $window) {

        $scope.groupAddModel = {
            name: '',
            comments: '',
            currentPage: 1,
            total: 0,
            limit: 7,
            isLoadingProgress: false,
            customerList: [],
            groupList: [],
            stateList :[],
            cityList : []
        }

        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [20,50, 100];
        $scope.groupAddModel.limit = $scope.pagePerCount[0];

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.group_add_form != 'undefined' && typeof $scope.group_add_form.$pristine != 'undefined' && !$scope.group_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.searchFilter = {
            email: '',
            name: '',
            company_name: '',
            state: '',
            category: '',
            city: '',
            familycode: '',
            customerOf: '',
            accno: '',
            salesOrPurchase: '',
            customAttribute: []
        };
        $scope.customerFormReset = function()
        {
            $scope.searchFilter = {
                email: '',
                name: '',
                company_name: '',
                state: '',
                category: '',
                city: '',
                familycode: '',
                customerOf: '',
                accno: '',
                salesOrPurchase: '',
                customAttribute: []
            };
            $scope.getStateList();
            $scope.getCityList ();
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
                $scope.groupAddModel.name = "";
                $scope.groupAddModel.comments = "";
                $scope.groupAddModel.groupList = [];
            
        }
        $scope.isdeleteProgress = false;
        $scope.selectGroupId = '';
        $scope.showdeletePopup = false;
        $scope.showPopup = function(id)
        {
            $scope.selectGroupId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
           
        };
        $scope.deleteGroupCustomer = function(id)
        {
            $scope.selectGroupId = id;
            $scope.groupAddModel.groupList.splice($scope.selectGroupId,1);
            $scope.isdeleteProgress = true;
            $scope.closePopup();

        };
        $scope.initTableFilter = function()
        {
            if ($scope.initDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDetailTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getCustomerList, 300);
            // $scope.initTableFilterTimeoutPromise = $timeout($scope.getListPrint, 300);
        }
        $scope.selectAll = false;
        $scope.checkAllCustomer = function()
        {
            for (var i = 0; i < $scope.groupAddModel.customerList.length; i++)
            {
                if ($scope.selectAll == true)
                {
                    $scope.groupAddModel.customerList[i].customerChecked = true;
                } else
                {
                    $scope.groupAddModel.customerList[i].customerChecked = false;
                }
            }
        };
        $scope.getCustomerList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'customer';

            getListParam.fname = $scope.searchFilter.name;
            getListParam.company_name = $scope.searchFilter.company_name;
            getListParam.email = $scope.searchFilter.email;
            getListParam.state_id = $scope.searchFilter.state_id;
            getListParam.city_id = $scope.searchFilter.city_id;
            if ($scope.searchFilter.salesOrPurchase == 1)
            {
                getListParam.is_sale = 1;
            }
            else if ($scope.searchFilter.salesOrPurchase == 2)
            {
                getListParam.is_purchase = 1;
            }
            getListParam.is_active = 1;
            getListParam.start = ($scope.groupAddModel.currentPage - 1) * $scope.groupAddModel.limit;
            getListParam.limit = $scope.groupAddModel.limit;
            $scope.groupAddModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getCustomersList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.groupAddModel.customerList = data;
                    $scope.groupAddModel.total = response.data.total;
                    for (var i = 0; i < $scope.groupAddModel.customerList.length; i++)
                    {
                        $scope.groupAddModel.customerList[i].customerChecked = false;
                    }
                }
                $scope.groupAddModel.isLoadingProgress = false;
            });
        };
        $scope.getStateList = function() {          

            var stateListParam = {};
            var headers = {};
            headers['screen-code'] = 'state';
            stateListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.groupAddModel.stateList = data.list;
                }
            });

        };
        $scope.getCityList = function() {          
        var cityListParam = {};
        var headers = {};
        headers['screen-code'] = 'city';
        if($scope.searchFilter.state_id != '' && $scope.searchFilter.state_id != 'undefined' && $scope.searchFilter.state_id != null)
        {
            cityListParam.state_id = $scope.searchFilter.state_id;
        }
        cityListParam.is_active = 1;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getCityList(cityListParam, configOption, headers).then(function(response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.groupAddModel.cityList = data.list;
            }
        });

    };
        $scope.groupAddModel.groupList = [];
        $scope.groupCustomerAdd = function()
        {
            for (var i = 0; i < $scope.groupAddModel.customerList.length; i++)
            {
                if ($scope.groupAddModel.customerList[i].customerChecked == true)
                {
                    var id = $scope.groupAddModel.customerList[i].id;
                    $scope.checkGroupList(id);
                    if ($scope.groupList == false)
                    {
                        $scope.groupAddModel.groupList.push($scope.groupAddModel.customerList[i]);
                        $scope.groupAddModel.customerList[i].customerChecked = false;
                    }
                }

            }
        }
        $scope.groupList = false;
        $scope.checkGroupList = function(id)
        {
            $scope.groupList = false;
            if ($scope.groupAddModel.groupList.length > 0)
            {
                for (var j = 0; j < $scope.groupAddModel.groupList.length; j++)
                {
                    if (id == $scope.groupAddModel.groupList[j].id)
                    {
                        $scope.groupList = true;
                    }
                }
            }
            else
            {
                $scope.groupList = false;
            }

        };
        $scope.saveGroupInfo = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addGroupParam = {};
            var headers = {};
            headers['screen-code'] = 'customergroup';
            addGroupParam.name = $scope.groupAddModel.name;
            addGroupParam.comment = $scope.groupAddModel.comments;
            addGroupParam.detail = [];
           // var customer = [];
            for (var i = 0; i< $scope.groupAddModel.groupList.length; i++)
            {
                if($scope.groupAddModel.groupList.length > 0)
                {
                   var customerId = {
                        customer_id : $scope.groupAddModel.groupList[i].id
                    };
                    addGroupParam.detail.push(customerId);
                    
                }
                
            }
            //addGroupParam.detail.push(customer)
            //addGroupParam.is_active = 1;
            adminService.saveCustomerGroup(addGroupParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.group');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.stateChange = function () 
        {
            $scope.searchFilter.city_id = '';
            $scope.getCityList();
            
        }
        $scope.initTableFilter();
        $scope.getStateList();
        $scope.getCityList ();
    }]);
