
app.controller('amcAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.amcAddModel =
                {
                    "customerInfo": '',
                    "productname": '',
                    list: [],
                    itemList: [],
                    duation: '',
                    amount: '',
                    date: "",
                    productList: [],
                    serialno: ''
                };
        $scope.validationFactory = ValidationFactory;
        $scope.isDataSavingProcess = false;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_amc_form !== 'undefined' && typeof $scope.create_amc_form.$pristine !== 'undefined' && !$scope.create_amc_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {

            $scope.create_amc_form.$setPristine();
            $scope.amcAddModel = {
                "customerInfo": '',
                "productname": '',
                list: [],
                itemList: [],
                duation: '',
                amount: '',
                date: "",
                serialno: ""
            };
        }
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.dateOpen = false;
        $scope.amcAddModel.date = $scope.currentDate;
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.dateOpen = true;
            }
        }
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function(model)
        {
            if (model != null)
            {
                $scope.amcAddModel.productname.serialno = model.serial_no;
                return model.product_name;
                
            }
            return  '';

        }
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.getProductInfo = function(val)
        {
            if ($scope.amcAddModel.customerInfo != undefined && $scope.amcAddModel.customerInfo != '' && $scope.amcAddModel.customerInfo.id != undefined)
            {
                var autosearchParam = {};
                autosearchParam.search = val;
                autosearchParam.is_active = 1;
                autosearchParam.customer_id = $scope.amcAddModel.customerInfo.id;
                return $httpService.get(APP_CONST.API.GET_CUSTOMER_INVOICE_ITEM_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
            else
            {
                sweet.show('Oops...', 'Please select a customer', 'error');
            }
        };



//        $scope.getProductInfo = function()
//        {
//            $scope.amcAddModel.productList = [];
//            if ($scope.amcAddModel.customerInfo != undefined && $scope.amcAddModel.customerInfo != '' && $scope.amcAddModel.customerInfo.id != undefined)
//            {
//                var getParam = {};
//                getParam.customer_id =  $scope.amcAddModel.customerInfo.id;
//                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
//                adminService.getCustomerInvoiceItemList(getParam, configOption).then(function(response) {
//                    if (response.data.success === true)
//                    {
//                        var data = response.data;
//                        $scope.amcAddModel.productList = data.list;
//
//                    }
//
//                });
//            }
//            else
//            {
//                sweet.show('Oops...', 'Please select atleast one customer', 'error');
//            }
//
//        };
        $scope.createAmc = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'amc';
                createParam.customer_id = $scope.amcAddModel.customerInfo.id;
                createParam.product_id = $scope.amcAddModel.productname.product_id;
                if($scope.amcAddModel.productname.invoice_item_id == 0)
                {
                createParam.product_id = $scope.amcAddModel.productname.prod_id;
                }
                createParam.invoice_id = $scope.amcAddModel.productname.invoice_id;
                createParam.invoice_item_id = $scope.amcAddModel.productname.invoice_item_id;
                createParam.serial_no = $scope.amcAddModel.productname.serialno;
                createParam.duration = $scope.amcAddModel.duation;
                createParam.from_date = $filter('date')($scope.amcAddModel.date, 'yyyy-MM-dd');
                $scope.to_date = moment($scope.amcAddModel.date).add($scope.amcAddModel.duation, 'years');
                createParam.to_date = $filter('date')($scope.to_date._d, 'yyyy-MM-dd');
                createParam.amount = $scope.amcAddModel.amount;
                createParam.is_active = 1;
                adminService.saveAmc(createParam, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();

                        $state.go('app.amc');
                    }
                    $scope.isDataSavingProcess = false;

                });
            }


        };



    }]);