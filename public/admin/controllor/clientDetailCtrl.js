

app.controller('clientDetailCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.clientAddModel = {
            "id": '',
            "name": "",
            "email": "",
            "phoneNumber": "",
            "city": "",
            "state": "",
            "country": "",
            "category":"",
            "customerOf":"",
            companyId: "",
            list: [],
            "postcode": "",
            "address": "",
            "isactive": 1,
            currentPage: 1,
            total: 0,
            limit: 10,
            isLoadingProgress: false,
            creditdate: '',
            creditamount: '',
            creditcomments: '',
            debitdate: new Date(),
            debitamount: '',
            debitcomments: ''
        };
        $scope.currentDate = new Date();
        $scope.settledPaymentModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            isLoadingProgress: false,
            totSettledPayment: ''
        };
        $scope.unsettledPaymentModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            isLoadingProgress: false,
            totUnsettledPayment: ''
        };

        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.client_edit_form != 'undefined' && typeof $scope.client_edit_form.$pristine != 'undefined' && !$scope.client_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {

            if (typeof $scope.client_edit_form != 'undefined')
            {
                $scope.client_edit_form.$setPristine();
                $scope.updateClientInfo();
            }

        }

        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = 'dd-MM-yyyy';

        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }

        $scope.showcreditPopup = false;
        $scope.showCreditPopup = function() {

            $scope.showcreditPopup = true;
            $scope.fromDateOpen = false;
            $scope.clientAddModel.creditdate = '';
            $scope.clientAddModel.creditamount = '';
            $scope.clientAddModel.creditcomments = '';

        };

        $scope.closeCreditPopup = function() {

            $scope.showcreditPopup = false;

        };
        $scope.showdebitPopup = false;
        $scope.showDebitPopup = function() {

            $scope.showdebitPopup = true;
            $scope.clientAddModel.debitdate = '';
            $scope.clientAddModel.debitamount = '';
            $scope.clientAddModel.debitcomments = '';

        };

        $scope.closeDebitPopup = function() {

            $scope.showdebitPopup = false;

        };


        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            $scope.initUpdateDetailTimeoutPromise = $timeout($scope.updateClientInfo, 300);
        }

        $scope.updateClientInfo = function()
        {
            $scope.clientAddModel.id = $scope.clientAddModel.list.id;
            $scope.clientAddModel.name = $scope.clientAddModel.list.name;
            $scope.clientAddModel.phoneNumber = $scope.clientAddModel.list.phoneNumber;
            $scope.clientAddModel.email = $scope.clientAddModel.list.email;
            $scope.clientAddModel.country = $scope.clientAddModel.list.country;
            $scope.clientAddModel.companyId = $scope.clientAddModel.list.companyId;
            $scope.clientAddModel.city = $scope.clientAddModel.list.city;
            $scope.clientAddModel.state = $scope.clientAddModel.list.state;
            $scope.clientAddModel.address = $scope.clientAddModel.list.address;
            $scope.clientAddModel.postcode = $scope.clientAddModel.list.postcode;
            $scope.clientAddModel.balanceAmount = $scope.clientAddModel.list.balanceAmount;
            $scope.getUnsettledPaymentList();
            $scope.getSettledPaymentList();
        }


        $scope.getClient = function() {
            $scope.clientAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.Id = $stateParams.id;
            getListParam.name = '';
            getListParam.phno = '';
            getListParam.email = '';
            getListParam.country = '';
            getListParam.city = '';
            getListParam.state = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.start = 0;
            getListParam.limit = 10;

            adminService.getClientList(getListParam).then(function(response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.clientAddModel.list = data.list[0];
                }
                $scope.clientAddModel.total = data.total;
                $scope.clientAddModel.isLoadingProgress = false;

                $scope.initUpdateDetail();
            });
        };

        $scope.paymentdetail = {};
        $scope.getSettledPaymentList = function() {
            
            var getListParam = {};
            $scope.settledPaymentModel.isLoadingProgress = true;
            getListParam.id = "";
            getListParam.companyId = $rootScope.userModel.companyId;
            getListParam.userId = '';
            getListParam.clientId = $scope.clientAddModel.id;
            getListParam.categoryId = $scope.clientAddModel.category;
            getListParam.fromDate='';
            getListParam.toDate='';
            getListParam.accountNumber = '';
            getListParam.custName = '';
            getListParam.amount = '';
            getListParam.customerOf = $scope.clientAddModel.customerOf;
            getListParam.clientEmail = '';
            getListParam.isSettled = 1;
            getListParam.start = ($scope.settledPaymentModel.currentPage - 1) * $scope.settledPaymentModel.limit;
            getListParam.limit = $scope.settledPaymentModel.limit;
            $scope.settledPaymentModel.isLoadingProgress = true;
            
            adminService.getPaymentList(getListParam).then(function(response) {
                var data = response.data;
                $scope.settledPaymentModel.totSettledPayment = data.totalAmt;
                $scope.settledPaymentModel.list = data.list;
                $scope.settledPaymentModel.total = data.total;
                $scope.settledPaymentModel.isLoadingProgress = false;

            });
        };

        $scope.getUnsettledPaymentList = function() {

            var getListParam = {};
            $scope.unsettledPaymentModel.isLoadingProgress = true;
            getListParam.id = "";
            getListParam.companyId = $rootScope.userModel.companyId;
            getListParam.userId = '';
            getListParam.clientId = $scope.clientAddModel.id;
            getListParam.clientEmail = '';
            getListParam.isSettled = 0;
            getListParam.start = ($scope.unsettledPaymentModel.currentPage - 1) * $scope.unsettledPaymentModel.limit;
            getListParam.limit = $scope.unsettledPaymentModel.limit;
            $scope.unsettledPaymentModel.isLoadingProgress = true;
            adminService.getPaymentList(getListParam).then(function(response) {
                var data = response.data;
                $scope.unsettledPaymentModel.totUnsettledPayment = data.totalAmt;
                $scope.unsettledPaymentModel.list = data.list;
                $scope.unsettledPaymentModel.total = data.total;
                $scope.unsettledPaymentModel.isLoadingProgress = false;

            });
        };


        $scope.saveOpeningBalance = function(id) {

            var saveOpeningBalanceParam = {};
            $scope.isLoadingProgress = true;
                     
            saveOpeningBalanceParam.clientId = $scope.clientAddModel.id;
            $scope.clientAddModel.creditdate = $filter('date')($scope.clientAddModel.creditdate, 'yyyy-MM-dd');
            $scope.clientAddModel.debitdate = $filter('date')($scope.clientAddModel.debitdate, 'yyyy-MM-dd');
            if (id == 1)
            {
                saveOpeningBalanceParam.date = $scope.clientAddModel.creditdate;
                saveOpeningBalanceParam.amount = $scope.clientAddModel.creditamount;
                saveOpeningBalanceParam.particulars = $scope.clientAddModel.creditcomments;
                saveOpeningBalanceParam.type = 1;
                
                adminService.saveCreditOrDebitOpeningBalance(saveOpeningBalanceParam).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.closeCreditPopup();
                        $scope.closeDebitPopup();
                        $scope.getClient();
                    }
                    $scope.isLoadingProgress = false;

                });

            }
            else
            {
                saveOpeningBalanceParam.date = $scope.clientAddModel.debitdate;
                saveOpeningBalanceParam.amount = $scope.clientAddModel.debitamount;
                saveOpeningBalanceParam.particulars = $scope.clientAddModel.debitcomments;
                saveOpeningBalanceParam.type = 0;
                
                adminService.saveCreditOrDebitOpeningBalance(saveOpeningBalanceParam).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.closeDebitPopup();
                        $scope.getClient();
                    }
                    $scope.isLoadingProgress = false;

                });
            }

        };

        $scope.getClient();
    }]);




