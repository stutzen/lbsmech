


app.controller('allowanceListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.allowanceListModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.allowanceListModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
            name: ''
        }   ;
            $scope.initTableFilter();
       }

        $scope.selectCategoryId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteCategoryLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectCategoryId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteCategoryLoadingProgress = false;
        };

        $scope.deleteCategoryInfo = function( )
        {
            if($scope.isdeleteCategoryLoadingProgress == false)
            {
            $scope.isdeleteCategoryLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $scope.selectCategoryId;
            var headers = {};
            headers['screen-code'] = 'allowance';
            adminService.deleteAllowanceCategory(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }
                $scope.isdeleteCategoryLoadingProgress = false;
            });
        } 
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'allowance';
            getListParam.id = '';
            getListParam.is_active = 1;
            //getListParam.type = 'income';
            getListParam.name = $scope.searchFilter.name;
            getListParam.start = ($scope.allowanceListModel.currentPage - 1) * $scope.allowanceListModel.limit;
            getListParam.limit = $scope.allowanceListModel.limit;
            $scope.allowanceListModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAllowanceCategoryList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.allowanceListModel.list = data;
                    $scope.allowanceListModel.total = response.data.total;
                }
                $scope.allowanceListModel.isLoadingProgress = false;
            });

        };


        $scope.getList();
    }]);








