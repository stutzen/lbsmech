app.controller('salesorderListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', '$localStorage', 'customerFactory', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, $localStorage, customerFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.invoiceModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            isLoadingProgress: true
        };
        $scope.pageHeading = '';
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.invoiceModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.customerFilter = customerFactory.get();
        customerFactory.set('');
        $scope.searchFilter =
                {
                    id: '',
                    quoteId: '',
                    duedate: '',
                    invoicedate: '',
                    customerInfo: {},
                    status: '',
                    companyInfo: {},
                    invoiceId: '',
                    fromdate: '',
                    todate: ''
                };

        $scope.invoicedateOpen = false;
        $scope.duedateOpen = false;
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.invoicedateOpen = true;
            } else if (index == 1)
            {
                $scope.duedateOpen = true;
            } else if (index == 2)
            {
                $scope.fromDateOpen = true;
            } else if (index == 3)
            {
                $scope.toDateOpen = true;
            }
        }

        if ($rootScope.currentPage == 'spareRequest')
            $scope.pageHeading = 'Spare Request';
        else
            $scope.pageHeading = 'Sales Order';

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getinvoiceListInfo, 300);
        }

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter =
                    {
                        id: '',
                        quoteId: '',
                        duedate: '',
                        invoicedate: '',
                        customerInfo: {},
                        status: '',
                        companyInfo: '',
                        invoiceId: '',
                        fromdate: '',
                        todate: ''
                    };
            $scope.initTableFilter();
        }
        $scope.selectInvoiceId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectInvoiceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteInvoiceItem = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectInvoiceId;
                var headers = {};
                if ($rootScope.currentPage == 'spareRequest')
                    headers['screen-code'] = 'spareRequest';
                else
                    headers['screen-code'] = 'salesorder';
                adminService.deleteSalesOrder(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getinvoiceListInfo();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };

        $scope.$watch('searchFilter.customerInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                $scope.initTableFilter();
            }
        });
        $scope.$watch('searchFilter.companyInfo', function (newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal === '')
            {

                $scope.initTableFilter();

            }
        });

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };
        if ($scope.customerFilter != '' && $scope.customerFilter != null && $scope.customerFilter != undefined)
        {
            $scope.searchFilter.customerInfo = $scope.customerFilter;
            $localStorage["sales_order_list_form-order_customer_dropdown"] = $scope.searchFilter.customerInfo;
        }

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                } else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                } else
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.getCompanyList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCompanyModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.getinvoiceListInfo = function ()
        {
            $scope.invoiceModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            if ($rootScope.currentPage == 'spareRequest')
                headers['screen-code'] = 'spareRequest';
            else
                headers['screen-code'] = 'salesorder';

            var configOption = adminService.handleOnlyErrorResponseConfig;
            getListParam.id = $scope.searchFilter.invoiceId;
            getListParam.sales_order_code = $scope.searchFilter.quoteId;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customerid = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.customerid = '';
            }
            if ($scope.searchFilter.companyInfo != null && typeof $scope.searchFilter.companyInfo != 'undefined' && typeof $scope.searchFilter.companyInfo.id != 'undefined')
            {
                getListParam.company_id = $scope.searchFilter.companyInfo.id;
            } else
            {
                getListParam.company_id = '';
            }
//            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '')
//            {
//                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
//                {
//                    getListParam.from_validdate = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.adminService.appConfig.date_format);
//                }
//                getListParam.from_validdate = utilityService.changeDateToSqlFormat(getListParam.from_validdate, $scope.adminService.appConfig.date_format);
//                getListParam.to_validdate = getListParam.from_validdate;
//            }
//            if ($scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
//            {
//                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
//                }
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
//                getListParam.to_date = getListParam.from_date;
//            }
//            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '' && $scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
//            {
//                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
//                {
//                    getListParam.from_validdate = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.adminService.appConfig.date_format);
//                }
//                getListParam.from_validdate = utilityService.changeDateToSqlFormat(getListParam.from_validdate, $scope.adminService.appConfig.date_format);
//                getListParam.to_validdate = getListParam.from_validdate;
//                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
//                }
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
//                getListParam.to_date = getListParam.from_date;
//            }
            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '')
            {
                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
                {
                    getListParam.order_date = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.adminService.appConfig.date_format);
                }
                getListParam.order_date = utilityService.changeDateToSqlFormat(getListParam.order_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
            {
                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
                {
                    getListParam.valid_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
                }
                getListParam.valid_date = utilityService.changeDateToSqlFormat(getListParam.valid_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            getListParam.start = ($scope.invoiceModel.currentPage - 1) * $scope.invoiceModel.limit;
            getListParam.limit = $scope.invoiceModel.limit;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            if ($rootScope.currentPage == 'spareRequest')
                getListParam.type = 'spare_request';
            else
                getListParam.type = 'sales_order';
            adminService.getSalesOrderList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.invoiceModel.list = data.list;
                    if ($scope.invoiceModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.invoiceModel.list.length; i++)
                        {
                            $scope.invoiceModel.list[i].index = getListParam.start + i + 1;
                            $scope.invoiceModel.list[i].newdate = utilityService.parseStrToDate($scope.invoiceModel.list[i].date);
                            $scope.invoiceModel.list[i].date = utilityService.parseDateToStr($scope.invoiceModel.list[i].newdate, $rootScope.appConfig.date_format);
                            $scope.invoiceModel.list[i].validuntildate = utilityService.parseStrToDate($scope.invoiceModel.list[i].validuntil);
                            $scope.invoiceModel.list[i].validuntil = utilityService.parseDateToStr($scope.invoiceModel.list[i].validuntildate, $rootScope.appConfig.date_format);
                            $scope.invoiceModel.list[i].total_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.invoiceModel.list[i].advance_amount = parseFloat(utilityService.changeCurrency($scope.invoiceModel.list[i].advance_amount, $rootScope.appConfig.thousand_seperator)).toFixed(2);

                        }
                    }
                    $scope.invoiceModel.total = data.total;
                }
                $scope.invoiceModel.isLoadingProgress = false;
            });
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'sales_order_list_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "sales_order_list_form-order_customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
            if (data.fieldName == "sales_order_list_form-order_company_dropdown")
            {
                $scope.searchFilter.companyInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRenterCount++;
            }
            if ($scope.localStorageRenterCount >= $scope.localStorageCount)
            {
                console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
            }

        });


        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {

            if ($scope.localStorageFormName == formName)
            {
                $scope.localStorageRetrieveCount++;
            }
            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
            {
                $scope.initTableFilter();
            }
            console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.customerFilter == null || $scope.customerFilter == undefined || $scope.customerFilter == '')
            {
                $timeout(function () {
                    $scope.init();
                }, 300);
            }

        });
        $scope.checkFilter = function ()
        {
            if ($scope.customerFilter != null && $scope.customerFilter != undefined && $scope.customerFilter != '')
            {

                $scope.getinvoiceListInfo();
            } else
            {
                $scope.init();
            }
        }
        $scope.checkFilter();
    }]);




