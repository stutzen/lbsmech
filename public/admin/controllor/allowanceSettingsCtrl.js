


app.controller('allowanceSettingsCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory','$stateParams', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory,$stateParams) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.allowanceModel = {
        currentPage: 1,
        total: 0,
        limit: 10,
        email: '',
        list: [],
        period : '',
        isLoadingProgress: false,
        userList : [],
        allowanceList : []
    };
    $scope.pagePerCount = [50, 100];
    $scope.allowanceModel.limit = $scope.pagePerCount[0];
    $scope.validationFactory = ValidationFactory;
    $scope.formReset = function() {

        $scope.allowance_form.$setPristine();
        $scope.getallowanceList();

    }

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }
    
    $scope.selectEmployeeId = '';

    $scope.saveallowance = function()
    {
        var allowanceParam = [];
        var updateAllowance = [];
        for(var i= 0;i< $scope.allowanceModel.allowanceList.length;i++ )
        {    
            var allowance = {};
            allowance.id = $scope.allowanceModel.allowanceList[i].id;
            allowance.role_id = $scope.allowanceModel.allowanceList[i].role_id;
            allowance.allowance_id = $stateParams.id;
            allowance.amount = $scope.allowanceModel.allowanceList[i].amount;
            allowance.type = $scope.allowanceModel.allowanceList[i].type;
            updateAllowance.push(allowance);
        }
        allowanceParam = updateAllowance;
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.modifyAllowanceMapping(allowanceParam,configOption ).then(function (response) {
            var data = response.data;
            if (data.success == true)
            {
                    
                $state.go('app.allowanceList');
            }
        });
    }
//    $scope.getUserList = function ()
//    {            
//        var getListParam = {};  
//        getListParam.isActive = 1;
//        var configOption = adminService.handleOnlyErrorResponseConfig;
//        adminService.getUserList(getListParam, configOption).then(function (response) {
//            if (response.data.success === true)
//            {
//                var data = response.data.list;
//                $scope.allowanceModel.userList = data;                    
//            }                
//        });
//    };
    $scope.getallowanceList = function() {

        var getListParam = {};
        getListParam.id = '';
        
        getListParam.allowance_id = $stateParams.id;
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getAllowanceMappingList(getListParam, configOption).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.allowanceModel.allowanceList = data;
                for(var i = 0;i < $scope.allowanceModel.allowanceList.length;i++)
                {
                    if($scope.allowanceModel.allowanceList[i].amount == '' || $scope.allowanceModel.allowanceList[i].amount == null)
                    {    
                    $scope.allowanceModel.allowanceList[i].amount = '0.00';
                    }
                    if($scope.allowanceModel.allowanceList[i].type == '' || $scope.allowanceModel.allowanceList[i].type == null)
                    {    
                    $scope.allowanceModel.allowanceList[i].type = 'Daily';
                    }
                }    
            }
        });

    };
    $scope.getallowanceList(); 
}]);










