

app.controller('stageEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.stageModel = {
            id: '',
            name: '',
            isActive: true,
            list: {},
            level: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_stage_form != 'undefined' && typeof $scope.add_stage_form.$pristine != 'undefined' && !$scope.add_stage_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_stage_form.$setPristine();
            if (typeof $scope.add_stage_form != 'undefined')
            {
                $scope.stageModel.name = "";
                $scope.stageModel.isActive = true;
            }
            $scope.updateStageInfo();
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.modifystage = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addStageParam = {};
            var headers = {};
            headers['screen-code'] = 'stage';
            addStageParam.id = $stateParams.id;
            addStageParam.name = $scope.stageModel.name;
            addStageParam.level = $scope.stageModel.level;
            addStageParam.is_active = $scope.stageModel.isActive;
            addStageParam.stage_flag = $scope.stageModel.stageflag;
            adminService.editStage(addStageParam, $scope.stageModel.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.stage');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.updateStageInfo = function()
        {
            $scope.stageModel.is_active = $scope.stageModel.list.is_active;
            $scope.stageModel.name = $scope.stageModel.list.name;
            $scope.stageModel.isLoadingProgress = false;
            $scope.stageModel.id = $scope.stageModel.list.id;
            $scope.stageModel.level = parseInt($scope.stageModel.list.level,10);
            $scope.stageModel.stageflag = $scope.stageModel.list.stage_flag;
        }

        $scope.getStageInfo = function()
        {
            $scope.stageModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var productListParam = {};
                productListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getStageList(productListParam, configOption).then(function(response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.stageModel.list = data.list[0];

                    }
                    $scope.updateStageInfo();

                });
            }
        };
        $scope.getStageInfo();
    }]);
