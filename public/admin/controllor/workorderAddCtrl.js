
app.controller('workorderAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.workModel =
                {
                    "date": "",
                    "next_date": "",
                    "taskList": [],
                    "detail": [],
                    "inputProducts": [],
                    "outputProducts": [],
                    "userList": [],
                    "task_group_name": '',
                    "task_group_id": "",
                    "qtyToProduce": 1,
                    "userId": '',
                    "output_pdt_id": '',
                    "comments": '',
                    "work_order_type" : ''
                }
        $scope.workModel.date = new Date();
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.adminService = adminService;

        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.work_add_form !== 'undefined' && typeof $scope.work_add_form.$pristine !== 'undefined' && !$scope.work_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            $scope.work_add_form.$setPristine();

        };
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
        }
        $scope.addNewWork = function ()
        {
            var newRow = {
                output_pdt_qty: 1,
                output_pdt_id: '',
                product_for_id: '',
                output_pdt_uom_id: '',
                inputs: []
            };
            $scope.workModel.taskList.push(newRow);
        };
        $scope.showCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = true;
        }
        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
        }
        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.workModel.customerInfo = customerDetail;
            $scope.showCustomerAddPopup = false;
        });
        $scope.deleteProduct = function (index)
        {
            //            var index = -1;
            //            for (var i = 0; i < $scope.workModel.taskList.length; i++) {
            //                if (id == $scope.workModel.taskList[i].id) {
            //                    index = i;
            //                    break;
            //                }
            //            }
            $scope.workModel.taskList.splice(index, 1);
            $scope.workModel.outputProducts = [];
            $scope.workModel.inputProducts = [];
            for (var i = 0; i < $scope.workModel.taskList.length; i++)
            {
                var newoutputRow = {
                    output_pdt_id: $scope.workModel.taskList[i].output_pdt_id,
                    output_pdt_qty: parseFloat($scope.workModel.taskList[i].output_pdt_qty),
                    output_pdt_name: $scope.workModel.taskList[i].output_pdt_name
                };
                $scope.workModel.outputProducts.push(newoutputRow);
                for (var j = 0; j < $scope.workModel.taskList[i].input.length; j++)
                {
                    $scope.workModel.taskList[i].input[j].taskIndex = i;
                    var newinputRow = {
                        input_pdt_id: $scope.workModel.taskList[i].input[j].input_pdt_id,
                        input_pdt_name: $scope.workModel.taskList[i].input[j].input_pdt_name,
                        input_qty: parseFloat($scope.workModel.taskList[i].input[j].input_qty)
                    };
                    $scope.workModel.inputProducts.push(newinputRow);
                }
            }
            $scope.formatArray();
        };

        $scope.getTaskList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCTION_TASK_DETAIL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatTaskModel = function (model, index)
        {
            if (model != null)
            {
                $scope.updateTaskGroup(model);
                return model.name;
            }
            return  '';
        };

        $scope.updateTaskGroup = function (model)
        {
            $scope.workModel.task_group_name = model.task_group_name;
            $scope.workModel.task_group_id = model.task_group_id;
            $scope.workModel.qtyToProduce = 1;
            $scope.workModel.output_pdt_id = model.output_pdt_id;
            $scope.workModel.work_order_type = model.type;

        };

        $scope.getUserList = function () {

            var getListParam = {};
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function (response) {
                var data = response.data;
                $scope.workModel.userList = data.list;
                $scope.workModel.userId = $rootScope.userModel.id + '';
            });
        };

        $scope.getProdTaskDetail = function (index)
        {
            var getListParam = {};
            var headers = {};
            if ($scope.workModel.taskList[index].taskInfo.id != null && $scope.workModel.taskList[index].taskInfo.id != 'undefined' && $scope.workModel.taskList[index].taskInfo.id != '')
            {
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                getListParam.is_active = 1;
                getListParam.id = $scope.workModel.taskList[index].taskInfo.id;
                adminService.getProdTaskDetail(getListParam, configOption, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var taskdata = data.list[0];
                        taskdata.output_pdt_qty = 1;
                        //                        for (var i = 0; i < taskdata.input.length; i++)
                        //                        {
                        //                            taskdata.input[i].required_input_qty = taskdata.input[i].input_qty;
                        //                        }
                        $scope.updateTaskInfo(taskdata, index);
                    }
                });
            }
        };

        $scope.inputQtycalculationTimeoutPromise != null
        $scope.updateInputQty = function (index)
        {
            if ($scope.inputQtycalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.inputQtycalculationTimeoutPromise);
            }
            $scope.inputQtycalculationTimeoutPromise = $timeout($scope.calculateInputQty(index), 300);
        }

        $scope.calculateInputQty = function (index)
        {
            var output_pdt_qty = parseFloat($scope.workModel.taskList[index].output_pdt_qty);
            if ($scope.workModel.taskList[index].input.length > 0)
            {
                for (var i = 0; i < $scope.workModel.taskList[index].input.length; i++)
                {
                    $scope.workModel.taskList[index].input[i].input_qty = parseFloat($scope.workModel.taskList[index].input[i].required_input_qty) * output_pdt_qty;
                }
            }
            $scope.formatArray();
        };

        $scope.createorder = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createorderParam = {};
                var headers = {};
                headers['screen-code'] = 'workorder';
                createorderParam.customer_id = '';
                if (typeof $scope.workModel.date == 'object')
                {
                   var date = utilityService.parseDateToStr($scope.workModel.date, 'yyyy-MM-dd');
                }
                createorderParam.deadline_date = utilityService.changeDateToSqlFormat(date, 'yyyy-MM-dd');
                createorderParam.qty = $scope.workModel.qtyToProduce;
                createorderParam.bom_id = $scope.workModel.bomTaskInfo.id;
                createorderParam.user_id = $scope.workModel.userId;
                createorderParam.produced_qty = 0;
                createorderParam.task_group_id = $scope.workModel.task_group_id;
                createorderParam.status = "initiated";
                createorderParam.work_type = $scope.workModel.work_order_type;
                createorderParam.is_root = 1;
                createorderParam.root_id = 0;
                createorderParam.ref_id = 0;
                createorderParam.output_pdt_id = $scope.workModel.output_pdt_id;
                createorderParam.comments = $scope.workModel.comments;

                adminService.saveWorkOrder(createorderParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.workorder');
                    }
                    $scope.isDataSavingProcess = false;
                });
            } else
            {
                return;
            }

        };

        $scope.checkTaskInfo = function (index)
        {
            if ($scope.workModel.taskList[index].taskInfo == 'undefined' && ($scope.workModel.taskList[index].taskInfo.id == 'undefined' || $scope.workModel.taskList[index].taskInfo.id == '' || $scope.workModel.taskList[index].taskInfo.id == null))
            {
                $scope.workModel.taskList[index].id = '';
                $scope.workModel.taskList[index].name = '';
                $scope.workModel.taskList[index].output_pdt_id = '';
                $scope.workModel.taskList[index].product_for_name = '';
                $scope.workModel.taskList[index].output_pdt_name = '';
                $scope.workModel.taskList[index].output_pdt_uom = '';
                $scope.workModel.taskList[index].output_pdt_uom_id = '';
                $scope.workModel.taskList[index].output_pdt_qty = '';
                $scope.workModel.taskList[index].input = [];
                $scope.formatArray();
            }
        };

        $scope.updateTaskInfo = function (model, index)
        {
            if ($scope.workModel.taskList[index].length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.workModel.taskList[index].id = model.id;
                $scope.workModel.taskList[index].name = model.name;
                $scope.workModel.taskList[index].output_pdt_id = model.output_pdt_id;
                $scope.workModel.taskList[index].product_for_name = model.product_for_name;
                $scope.workModel.taskList[index].output_pdt_name = model.output_pdt_name;
                $scope.workModel.taskList[index].output_pdt_uom = model.output_pdt_uom;
                $scope.workModel.taskList[index].output_pdt_uom_id = model.output_pdt_uom_id;
                $scope.workModel.taskList[index].output_pdt_qty = 1;
                $scope.workModel.taskList[index].input = [];
                //                var newoutputRow = {
                //                    output_pdt_id: model.output_pdt_id,
                //                    output_pdt_qty: parseFloat(model.output_pdt_qty),
                //                    output_pdt_name: model.output_pdt_name
                //                };
                //                $scope.workModel.outputProducts.push(newoutputRow);
                for (var i = 0; i < model.input.length; i++)
                {
                    var newRow = {
                        config_input_id: model.input[i].id,
                        input_pdt_id: model.input[i].input_pdt_id,
                        input_pdt_name: model.input[i].input_pdt_name,
                        taskIndex: index,
                        input_pdt_uom: model.input[i].input_pdt_uom,
                        input_pdt_uom_id: model.input[i].input_pdt_uom_id,
                        required_input_qty: parseFloat(model.input[i].input_qty),
                        input_qty: parseFloat(model.input[i].input_qty)
                    };
                    $scope.workModel.taskList[index].input.push(newRow);
                    //                    var newinputRow = {
                    //                        input_pdt_id: model.input[i].input_pdt_id,
                    //                        input_pdt_name: model.input[i].input_pdt_name,
                    //                        input_qty: parseFloat(model.input[i].input_qty)
                    //                    };
                    //                    $scope.workModel.inputProducts.push(newinputRow);
                }
                $scope.formatArray();
                return;
            }
        }

        $scope.formatArray = function ()
        {
            var inputArray = [];
            var outputArray = [];

            var startInput = 0;
            var startOutput = 0;

            $scope.workModel.outputProducts = [];
            $scope.workModel.inputProducts = [];
            for (var i = 0; i < $scope.workModel.taskList.length; i++)
            {
                var newoutputRow = {
                    output_pdt_id: $scope.workModel.taskList[i].output_pdt_id,
                    output_pdt_qty: parseFloat($scope.workModel.taskList[i].output_pdt_qty),
                    output_pdt_name: $scope.workModel.taskList[i].output_pdt_name,
                    output_pdt_uom: $scope.workModel.taskList[i].output_pdt_uom
                };
                $scope.workModel.outputProducts.push(newoutputRow);
                for (var j = 0; j < $scope.workModel.taskList[i].input.length; j++)
                {
                    var newinputRow = {
                        input_pdt_id: $scope.workModel.taskList[i].input[j].input_pdt_id,
                        input_pdt_name: $scope.workModel.taskList[i].input[j].input_pdt_name,
                        input_qty: parseFloat($scope.workModel.taskList[i].input[j].input_qty),
                        input_pdt_uom: $scope.workModel.taskList[i].input[j].input_pdt_uom
                    };
                    $scope.workModel.inputProducts.push(newinputRow);
                }
            }

            if ($scope.workModel.inputProducts.length > 0)
            {
                inputArray.push($scope.workModel.inputProducts[0]);
                startInput = 1;
            }
            if ($scope.workModel.inputProducts.length > 1)
            {
                for (var i = startInput; i < $scope.workModel.inputProducts.length; i++)
                {
                    var found = false;
                    for (var j = 0; j < inputArray.length; j++)
                    {
                        if (inputArray[j].input_pdt_id == $scope.workModel.inputProducts[i].input_pdt_id)
                        {
                            inputArray[j].input_qty = parseFloat(inputArray[j].input_qty) + parseFloat($scope.workModel.inputProducts[i].input_qty)
                            found = true;
                        }
                    }
                    if (found == false)
                    {
                        inputArray.push($scope.workModel.inputProducts[i]);
                    }
                }
            }
            $scope.workModel.inputProducts = inputArray;
            console.log(inputArray);

            if ($scope.workModel.outputProducts.length > 0)
            {
                outputArray.push($scope.workModel.outputProducts[0]);
                startOutput = 1;
            }
            if ($scope.workModel.outputProducts.length > 1)
            {
                for (var i = startOutput; i < $scope.workModel.outputProducts.length; i++)
                {
                    var found = false;
                    for (var j = 0; j < outputArray.length; j++)
                    {
                        if (outputArray[j].output_pdt_id == $scope.workModel.outputProducts[i].output_pdt_id)
                        {
                            outputArray[j].output_pdt_qty = parseFloat(outputArray[j].output_pdt_qty) + parseFloat($scope.workModel.outputProducts[i].output_pdt_qty)
                            found = true;
                        }
                    }
                    if (found == false)
                    {
                        outputArray.push($scope.workModel.outputProducts[i]);
                    }
                }
            }
            $scope.workModel.outputProducts = outputArray;
            console.log(outputArray);
        };

        $scope.$watch('workModel.customerInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.workModel.customerInfo = '';
            } else if (typeof $stateParams.id == 'undefined' || $stateParams.id == null || $stateParams.id == '' || $stateParams.id == 0)
            {
                var addrss = "";

                if (newVal.billing_address != undefined && newVal.billing_address != '')
                {
                    addrss += newVal.billing_address;
                }
                if (newVal.billing_city != undefined && newVal.billing_city != '')
                {
                    addrss += '\n' + newVal.billing_city;
                }
                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
                {
                    if (newVal.billing_city != undefined && newVal.billing_city != '')
                    {
                        addrss += '-' + newVal.billing_pincode;
                    } else
                    {
                        addrss += newVal.billing_pincode;
                    }
                }

                if (newVal.billing_state != undefined && newVal.billing_state != '')
                {
                    addrss += '\n' + newVal.billing_state;
                }

                $scope.workModel.customerInfo.shopping_address = addrss;
            }
        });
        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.getCustomerInfo = function () {
            $scope.workModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.cust_id;
            getListParam.name = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.is_active = 1;
            adminService.getCustomerDetail(getListParam).then(function (response) {
                var data = response.data;
                if (data.total != 0)
                {
                    var customerInfo = data.data;
                    $scope.workModel.customerInfo = {};
                    $scope.workModel.customerInfo.id = customerInfo.id;
                    $scope.workModel.customerInfo.fname = customerInfo.fname;
                    $scope.workModel.customerInfo.billing_address = customerInfo.billing_address;
                    $scope.workModel.customerInfo.phone = customerInfo.phone;
                    $scope.workModel.customerInfo.email = customerInfo.email;
                    $scope.workModel.isLoadingProgress = false;
                }
            });
        };
        $scope.getUserList();
    }]);






