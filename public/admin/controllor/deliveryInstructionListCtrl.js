app.controller('deliveryInstructionListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.deliveryModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],            
            isLoadingProgress: true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.deliveryModel.limit = $scope.pagePerCount[0];
        $scope.adminService = adminService;
        $scope.searchFilter =
                {                    
                    transportInfo: {}                    
                };       

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.refreshScreen = function()
        {
            $scope.searchFilter =
                    {                        
                        transportInfo: {}                       
                    };
              $scope.initTableFilter();
        }
        $scope.selectedpaymentId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectedpaymentId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteAdvancePaymentItem = function( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectedpaymentId;
                var headers = {};
                headers['screen-code'] = 'deliveryinstruction';
                adminService.deleteDeliveryInstruction(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };
        
        $scope.getTransportList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.TRANSPORT_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatTransportModel = function (model)
        {
            if (model != null)
            {
                if (model.name != undefined && model.mobile_no != undefined && model.email != undefined)
                {
                    return model.name + '(' + model.mobile_no + ',' + model.email + ')';
                } else if (model.name != undefined && model.mobile_no != undefined)
                {
                    return model.name + '(' + model.mobile_no + ')';
                } else
                {
                    return model.name;
                }
            }
            return '';
        };
        
        $scope.getList = function()
        {
            $scope.deliveryModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'deliveryinstruction';

            var configOption = adminService.handleOnlyErrorResponseConfig;
            if($scope.searchFilter.transportInfo != 'undefined' && $scope.searchFilter.transportInfo != null && $scope.searchFilter.transportInfo.id != 'undefined')           
            {
                getListParam.transport_id = $scope.searchFilter.transportInfo.id;
            }
            else
                $scope.searchFilter.transportInfo = '';
            getListParam.start = ($scope.deliveryModel.currentPage - 1) * $scope.deliveryModel.limit;
            getListParam.limit = $scope.deliveryModel.limit;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            adminService.getDeliveryInstructionList(getListParam, configOption, headers).then(function(response)
            {

                var data = response.data;
                if (data.success)
                {
                    $scope.deliveryModel.list = data.list;
                    $scope.deliveryModel.total = data.total;
                }
                $scope.deliveryModel.isLoadingProgress = false;
            });
        };
        $scope.getList();
    }]);




