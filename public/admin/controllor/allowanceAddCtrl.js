

app.controller('allowanceAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $scope.allowanceAddModel = {
            id: '',
            name: "",
            amount: '',
            comments: '',
            isActive: true

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.category_form != 'undefined' && typeof $scope.category_form.$pristine != 'undefined' && !$scope.category_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {


            $scope.allowanceAddModel.name = '';
            $scope.allowanceAddModel.amount = '';
            $scope.allowanceAddModel.comments = '';
            $scope.category_form.$setPristine();

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.createAllowanceCategory = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var getListParam = {};
                var headers = {};
                headers['screen-code'] = 'allowance';                
                getListParam.name = $scope.allowanceAddModel.name;
                getListParam.amount = $scope.allowanceAddModel.amount;
                getListParam.comments = $scope.allowanceAddModel.comments;
  //              getListParam.type = 'income';

                adminService.addAllowanceCategory(getListParam, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.allowanceList');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

    }]);




