

app.controller('customerEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', '$window', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, $window) {

        $scope.customerEditModel = {
            "id": "",

            "name": "",
            "companyname": "",
            "email": "",
            "phone": "",
            "city": "",
            "state": "",
            "country": "",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "postcode": "",
            "iswelcomeEmail": '',
            showPassword: false,
            customerInfo: [],
            userList: [],
            categorylist: [],
            "isactive": 1,
            is_purchase: '',
            is_sale: '',
            isLoadingProgress: false,
            "img": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "billingCountryList": [],
            "billingStateList": [],
            "billingCityList": [],
            "shoppingCountryList": [],
            "shoppingStateList": [],
            "shoppingCityList": [],
            "contact_id": '',
            "acode": '',
            "dob": '',
            "special_date1": '',
            "special_date2": '',
            "special_date3": '',
            "special_date4": '',
            "special_date5": ''
        }


        $scope.customerDualEditModel = {
            "id": "",
            "customer_id": 0,
            "name": "",
            "companyname": "",
            "email": "",
            "phone": "",
            "city": "",
            "state": "",
            "country": "",
            "address": "",
            "tags": "",
            "currency": "",
            "group": "",
            "password": "",
            "confirmpassword": "",
            "postcode": "",
            "iswelcomeEmail": '',
            showPassword: false,
            customerInfo: [],
            userList: [],
            categorylist: [],
            "isactive": 1,
            is_purchase: '',
            is_sale: '',
            isLoadingProgress: false,
            "img": [],
            "billingaddr": '',
            "billingcity": '',
            "billingstate": '',
            "billingpostcode": '',
            "billingcountry": '',
            "shoppingaddr": '',
            "shoppingcity": '',
            "shoppingstate": '',
            "shoppingpostcode": '',
            "shoppingcountry": '',
            "isSameAddr": '',
            "billingCountryList": [],
            "billingStateList": [],
            "billingCityList": [],
            "shoppingCountryList": [],
            "shoppingStateList": [],
            "shoppingCityList": [],
            "contact_id": '',
            "acode": '',
            "dob": '',
            "special_date1": '',
            "special_date2": '',
            "special_date3": '',
            "special_date4": '',
            "special_date5": ''
        }

        $scope.customAttributeList = [];
        $scope.customAttributeBaseList = [];
        $scope.customAttributeDualSupportList = [];

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.modify_customer_form != 'undefined' && typeof $scope.modify_customer_form.$pristine != 'undefined' && !$scope.modify_customer_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            if (typeof $scope.modify_customer_form != 'undefined')
            {
                $scope.modify_customer_form.$setPristine();
                $scope.getCustomerInfo( );
            }
        }
        $scope.showUploadFilePopup = false;
        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            } else if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }


        }

        $scope.closePopup = function (value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;

            } else if (value == 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }

        }
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.isImageUploadComplete = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
            $scope.validateFileUploadStatus();
        }

        $scope.validateFileUploadStatus = function()
        {
            $scope.isImageSavingProcess = true;
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = false;
                $scope.isImageUploadComplete = true;
                }

        }
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.dobDateOpen = false;
        $scope.specialDate1 = false;
        $scope.specialDate2 = false;
        $scope.specialDate3 = false;
        $scope.specialDate4 = false;
        $scope.specialDate5 = false;
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.dobDateOpen = true;
            }
            if (index == 1)
            {
                $scope.specialDate1 = true;
            }
            if (index == 2)
            {
                $scope.specialDate2 = true;
            }
            if (index == 3)
            {
                $scope.specialDate3 = true;
            }
            if (index == 4)
            {
                $scope.specialDate4 = true;
            }
            if (index == 5)
            {
                $scope.specialDate5 = true;
            }

        }

        $scope.saveImagePath = function()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePopup('fileupload');
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.customerEditModel.img = $scope.uploadedFileQueue;
                $scope.closePopup('fileupload');
            }
        }

        $scope.updateShoppingAddr = function(string)
        {
            if ($scope.customerEditModel.isSameAddr == true)
            {
                $scope.customerEditModel.shoppingaddr = $scope.customerEditModel.billingaddr;
                $scope.customerEditModel.shoppingcity = $scope.customerEditModel.billingcity;
                $scope.customerEditModel.shoppingstate = $scope.customerEditModel.billingstate;
                $scope.customerEditModel.shoppingpostcode = $scope.customerEditModel.billingpostcode;
                $scope.customerEditModel.shoppingcountry = $scope.customerEditModel.billingcountry;

                $scope.customerDualEditModel.shoppingaddr = $scope.customerDualEditModel.billingaddr;
                $scope.customerDualEditModel.shoppingcity = $scope.customerDualEditModel.billingcity;
                $scope.customerDualEditModel.shoppingstate = $scope.customerDualEditModel.billingstate;
                $scope.customerDualEditModel.shoppingpostcode = $scope.customerDualEditModel.billingpostcode;
                $scope.customerDualEditModel.shoppingcountry = $scope.customerDualEditModel.billingcountry;
            } else if ($scope.customerEditModel.isSameAddr == false)
            {
                if (string != 'update')
                {
                    $scope.customerEditModel.shoppingaddr = '';
                    $scope.customerEditModel.shoppingcity = '';
                    $scope.customerEditModel.shoppingstate = '';
                    $scope.customerEditModel.shoppingpostcode = '';
                    $scope.customerEditModel.shoppingcountry = '';
                }
                $scope.customerDualEditModel.shoppingaddr = '';
                $scope.customerDualEditModel.shoppingcity = '';
                $scope.customerDualEditModel.shoppingstate = '';
                $scope.customerDualEditModel.shoppingpostcode = '';
                $scope.customerDualEditModel.shoppingcountry = '';

            }

        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.belongtoUserLoaded && $scope.customerDualInfoLoaded)
            {
                $scope.updateCustomerInfo( );
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateCustomerInfo = function ( )
        {
            $scope.customAttributeList = $scope.customerEditModel.customerInfo.customattribute;
            $scope.customerEditModel.contact_id = $scope.customerEditModel.customerInfo.contact_id;
            console.log('Customer DOB' + typeof $scope.customerEditModel.customerInfo.dob);
            $scope.customerEditModel.dob = new Date($scope.customerEditModel.customerInfo.dob);
            $scope.customerEditModel.billingaddr = $scope.customerEditModel.customerInfo.billing_address;
            if ($scope.customerEditModel.customerInfo.billing_city_id != 0)
            {
                $scope.customerEditModel.billingcity = $scope.customerEditModel.customerInfo.billing_city_id;
            } else
            {
                $scope.customerEditModel.billingcity = '';
            }
            if ($scope.customerEditModel.customerInfo.billing_state_id != 0)
            {
                $scope.customerEditModel.billingstate = $scope.customerEditModel.customerInfo.billing_state_id;
            } else
            {
                $scope.customerEditModel.billingstate = '';
            }
            if ($scope.customerEditModel.customerInfo.billing_country_id != 0)
            {
                $scope.customerEditModel.billingcountry = $scope.customerEditModel.customerInfo.billing_country_id;
            } else
            {
                $scope.customerEditModel.billingcountry = '';
            }
            $scope.customerEditModel.billingpostcode = $scope.customerEditModel.customerInfo.billing_pincode;
            $scope.customerEditModel.shoppingaddr = $scope.customerEditModel.customerInfo.shopping_address;
            if ($scope.customerEditModel.customerInfo.shopping_city_id != 0)
            {
                $scope.customerEditModel.shoppingcity = $scope.customerEditModel.customerInfo.shopping_city_id;
            } else
            {
                $scope.customerEditModel.shoppingcity = '';
            }
            if ($scope.customerEditModel.customerInfo.shopping_state_id != 0)
            {
                $scope.customerEditModel.shoppingstate = $scope.customerEditModel.customerInfo.shopping_state_id;
            } else
            {
                $scope.customerEditModel.shoppingstate = '';
            }
            if ($scope.customerEditModel.customerInfo.shopping_country_id != 0)
            {
                $scope.customerEditModel.shoppingcountry = $scope.customerEditModel.customerInfo.shopping_country_id;
            } else
            {
                $scope.customerEditModel.shoppingcountry = '';
            }

            $scope.customerEditModel.shoppingpostcode = $scope.customerEditModel.customerInfo.shopping_pincode;
            $scope.customerEditModel.acode = $scope.customerEditModel.customerInfo.acode;
            var datevalue1 = moment($scope.customerEditModel.customerInfo.special_date1).valueOf();
            if (datevalue1 > 0)
            {
                $scope.customerEditModel.special_date1 = new Date($scope.customerEditModel.customerInfo.special_date1);
            } else
                $scope.customerEditModel.special_date1 = '';
            var datevalue2 = moment($scope.customerEditModel.customerInfo.special_date2).valueOf();
            if (datevalue2 > 0)
            {
                $scope.customerEditModel.special_date2 = new Date($scope.customerEditModel.customerInfo.special_date2);
            } else
                $scope.customerEditModel.special_date2 = '';
            var datevalue3 = moment($scope.customerEditModel.customerInfo.special_date3).valueOf();
            if (datevalue3 > 0)
            {
                $scope.customerEditModel.special_date3 = new Date($scope.customerEditModel.customerInfo.special_date3);
            } else
                $scope.customerEditModel.special_date3 = '';
            var datevalue4 = moment($scope.customerEditModel.customerInfo.special_date4).valueOf();
            if (datevalue4 > 0)
            {
                $scope.customerEditModel.special_date4 = new Date($scope.customerEditModel.customerInfo.special_date4);
            } else
                $scope.customerEditModel.special_date4 = '';
            var datevalue5 = moment($scope.customerEditModel.customerInfo.special_date5).valueOf();
            if (datevalue5 > 0)
            {
                $scope.customerEditModel.special_date5 = new Date($scope.customerEditModel.customerInfo.special_date5);
            } else
                $scope.customerEditModel.special_date5 = '';

//            $scope.customerEditModel.special_date2 = new Date($scope.customerEditModel.customerInfo.special_date2);
//            $scope.customerEditModel.special_date3 = new Date($scope.customerEditModel.customerInfo.special_date3);
//            $scope.customerEditModel.special_date4 = new Date($scope.customerEditModel.customerInfo.special_date4);
//            $scope.customerEditModel.special_date5 = new Date($scope.customerEditModel.customerInfo.special_date5);
            $scope.customerEditModel.isLoadingProgress = false;
            if ($scope.customerEditModel.billingaddr == $scope.customerEditModel.shoppingaddr &&
                    $scope.customerEditModel.billingcity == $scope.customerEditModel.shoppingcity &&
                    $scope.customerEditModel.billingstate == $scope.customerEditModel.shoppingstate &&
                    $scope.customerEditModel.billingcountry == $scope.customerEditModel.shoppingcountry &&
                    $scope.customerEditModel.billingpostcode == $scope.customerEditModel.shoppingpostcode)
            {
                $scope.customerEditModel.isSameAddr = true;
            } else
            {
                $scope.customerEditModel.isSameAddr = false;
            }
            if ($scope.customerEditModel.customerInfo.img != '' && $scope.customerEditModel.customerInfo.img != null)
            {
                var fileItem = {}
                fileItem.urlpath = $scope.customerEditModel.customerInfo.img;
                $scope.customerEditModel.img.push(fileItem);
                console.log('imgtest');
                console.log($scope.customerEditModel.img);
            } else
            {
                $scope.customerEditModel.img = [];

            }
            if ($scope.customerDualEditModel.customerInfo != undefined && $scope.customerDualEditModel.customerInfo != null && $scope.customerDualEditModel.customerInfo != '')
            {
                $scope.customerDualEditModel.id = $scope.customerDualEditModel.customerInfo.id;
                $scope.customerDualEditModel.dob = $scope.customerDualEditModel.customerInfo.dob;
                $scope.customerDualEditModel.customer_id = $scope.customerDualEditModel.customerInfo.customer_id;
                $scope.customerDualEditModel.name = $scope.customerDualEditModel.customerInfo.fname;
                $scope.customerDualEditModel.companyname = $scope.customerDualEditModel.customerInfo.company;
                $scope.customerDualEditModel.contact_id = $scope.customerDualEditModel.customerInfo.contact_id;
                $scope.customerDualEditModel.billingaddr = $scope.customerDualEditModel.customerInfo.billing_address;
                if ($scope.customerDualEditModel.customerInfo.billing_city_id != 0)
                {
                    $scope.customerDualEditModel.billingcity = $scope.customerDualEditModel.customerInfo.billing_city_id;
                } else
                {
                    $scope.customerDualEditModel.billingcity = '';
                }
                if ($scope.customerDualEditModel.customerInfo.billing_state_id != 0)
                {
                    $scope.customerDualEditModel.billingstate = $scope.customerDualEditModel.customerInfo.billing_state_id;
                } else
                {
                    $scope.customerDualEditModel.billingstate = '';
                }
                if ($scope.customerDualEditModel.customerInfo.billing_country_id != 0)
                {
                    $scope.customerDualEditModel.billingcountry = $scope.customerDualEditModel.customerInfo.billing_country_id;
                } else
                {
                    $scope.customerDualEditModel.billingcountry = '';
                }
                $scope.customerDualEditModel.billingpostcode = $scope.customerDualEditModel.customerInfo.billing_pincode;
                $scope.customerDualEditModel.shoppingaddr = $scope.customerDualEditModel.customerInfo.shopping_address;
                if ($scope.customerDualEditModel.customerInfo.shopping_city_id != 0)
                {
                    $scope.customerDualEditModel.shoppingcity = $scope.customerDualEditModel.customerInfo.shopping_city_id;
                } else
                {
                    $scope.customerDualEditModel.shoppingcity = '';
                }
                if ($scope.customerDualEditModel.customerInfo.shopping_state_id != 0)
                {
                    $scope.customerDualEditModel.shoppingstate = $scope.customerDualEditModel.customerInfo.shopping_state_id;
                } else
                {
                    $scope.customerDualEditModel.shoppingstate = '';
                }
                if ($scope.customerDualEditModel.customerInfo.shopping_country_id != 0)
                {
                    $scope.customerDualEditModel.shoppingcountry = $scope.customerDualEditModel.customerInfo.shopping_country_id;
                } else
                {
                    $scope.customerDualEditModel.shoppingcountry = '';
                }
                $scope.customerDualEditModel.shoppingpostcode = $scope.customerDualEditModel.customerInfo.shopping_pincode;
                $scope.customerDualEditModel.acode = $scope.customerDualEditModel.customerInfo.acode;
                var datevalue1 = moment($scope.customerDualEditModel.customerInfo.special_date1).valueOf();
                if (datevalue1 > 0)
                {
                    $scope.customerDualEditModel.special_date1 = new Date($scope.customerDualEditModel.customerInfo.special_date1);
                } else
                    $scope.customerDualEditModel.special_date1 = '';
                var datevalue2 = moment($scope.customerDualEditModel.customerInfo.special_date2).valueOf();
                if (datevalue2 > 0)
                {
                    $scope.customerDualEditModel.special_date2 = new Date($scope.customerDualEditModel.customerInfo.special_date2);
                } else
                    $scope.customerDualEditModel.special_date2 = '';
                var datevalue3 = moment($scope.customerDualEditModel.customerInfo.special_date3).valueOf();
                if (datevalue3 > 0)
                {
                    $scope.customerDualEditModel.special_date3 = new Date($scope.customerDualEditModel.customerInfo.special_date3);
                } else
                    $scope.customerDualEditModel.special_date3 = '';
                var datevalue4 = moment($scope.customerDualEditModel.customerInfo.special_date4).valueOf();
                if (datevalue4 > 0)
                {
                    $scope.customerDualEditModel.special_date4 = new Date($scope.customerDualEditModel.customerInfo.special_date4);
                } else
                    $scope.customerDualEditModel.special_date4 = '';
                var datevalue5 = moment($scope.customerDualEditModel.customerInfo.special_date5).valueOf();
                if (datevalue5 > 0)
                {
                    $scope.customerDualEditModel.special_date5 = new Date($scope.customerDualEditModel.customerInfo.special_date5);
                } else
                    $scope.customerDualEditModel.special_date5 = '';
            }

            $scope.customAttributeBaseList = [];
            $scope.customAttributeDualSupportList = [];

            if ($rootScope.appConfig.dual_language_support)
            {
                for (var i = 0; i < $scope.customAttributeList.length; i++)
                {
                    var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                    if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                    {
                        $scope.customAttributeDualSupportList.push($scope.customAttributeList[i]);
                    } else
                    {
                        $scope.customAttributeBaseList.push($scope.customAttributeList[i]);
                    }
                }
            } else
            {
                $scope.customAttributeBaseList = $scope.customAttributeList;
            }




        }
        $scope.modifyCustomerDetails = function (imgSave) {
            var headers = {};
            headers['screen-code'] = 'customer';
            $scope.isDataSavingProcess = true;
            var modifyCustomerParam = {};
            modifyCustomerParam.id = $scope.customerEditModel.customerInfo.id;
            modifyCustomerParam.customer_no = $scope.customerEditModel.customerInfo.customer_no;
            modifyCustomerParam.contact_id = $scope.customerEditModel.customerInfo.contact_id;
            modifyCustomerParam.prefix = $scope.customerEditModel.customerInfo.prefix;
            modifyCustomerParam.fname = $scope.customerEditModel.customerInfo.fname;
            modifyCustomerParam.company = $scope.customerEditModel.customerInfo.company;
            modifyCustomerParam.phone = $scope.customerEditModel.customerInfo.phone;
            modifyCustomerParam.email = $scope.customerEditModel.customerInfo.email;
            modifyCustomerParam.city = $scope.customerEditModel.customerInfo.city;
            modifyCustomerParam.country = $scope.customerEditModel.customerInfo.country;
            modifyCustomerParam.address = $scope.customerEditModel.customerInfo.address;
            modifyCustomerParam.state = $scope.customerEditModel.customerInfo.state;
            modifyCustomerParam.zip = $scope.customerEditModel.customerInfo.zip;
            modifyCustomerParam.password = $scope.customerEditModel.password;
            modifyCustomerParam.iswelcomeEmail = ($scope.customerEditModel.iswelcomeEmail == true ? 1 : 0);
            modifyCustomerParam.is_sale = ($scope.customerEditModel.is_sale == true ? 1 : 0);
            modifyCustomerParam.is_purchase = ($scope.customerEditModel.is_purchase == true ? 1 : 0);
            modifyCustomerParam.isactive = 1;
            modifyCustomerParam.acode = $scope.customerEditModel.acode;
            if ($scope.customerEditModel.img.length > 0)
            {
                modifyCustomerParam.img = $scope.customerEditModel.img[0].urlpath;
            }
            else
            {
                modifyCustomerParam.img = '';
            }
            if (typeof $scope.customerEditModel.dob == 'object')
            {
                $scope.customerEditModel.dob = utilityService.parseDateToStr($scope.customerEditModel.dob, 'yyyy-MM-dd');
            }
            modifyCustomerParam.dob = utilityService.changeDateToSqlFormat($scope.customerEditModel.dob, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date1 == 'object')
            {
                $scope.customerEditModel.special_date1 = utilityService.parseDateToStr($scope.customerEditModel.special_date1, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date1 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date1, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date2 == 'object')
            {
                $scope.customerEditModel.special_date2 = utilityService.parseDateToStr($scope.customerEditModel.special_date2, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date2 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date2, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date3 == 'object')
            {
                $scope.customerEditModel.special_date3 = utilityService.parseDateToStr($scope.customerEditModel.special_date3, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date3 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date3, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date4 == 'object')
            {
                $scope.customerEditModel.special_date4 = utilityService.parseDateToStr($scope.customerEditModel.special_date4, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date4 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date4, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date5 == 'object')
            {
                $scope.customerEditModel.special_date5 = utilityService.parseDateToStr($scope.customerEditModel.special_date5, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date5 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date5, 'yyyy-MM-dd');
            modifyCustomerParam.billing_address = $scope.customerEditModel.billingaddr;
            modifyCustomerParam.billing_city_id = $scope.customerEditModel.billingcity;
            modifyCustomerParam.billing_city = '';
            for (var i = 0; i < $scope.customerEditModel.billingCityList.length; i++)
            {
                if ($scope.customerEditModel.billingCityList[i].id == $scope.customerEditModel.billingcity)
                {
                    modifyCustomerParam.billing_city = $scope.customerEditModel.billingCityList[i].name;
                }
            }

            modifyCustomerParam.billing_state_id = $scope.customerEditModel.billingstate;
            modifyCustomerParam.billing_state = "";
            for (var i = 0; i < $scope.customerEditModel.billingStateList.length; i++)
            {
                if ($scope.customerEditModel.billingStateList[i].id == $scope.customerEditModel.billingstate)
                {
                    modifyCustomerParam.billing_state = $scope.customerEditModel.billingStateList[i].name;
                }
            }
            modifyCustomerParam.billing_country_id = $scope.customerEditModel.billingcountry;
            modifyCustomerParam.billing_country = "";
            for (var i = 0; i < $scope.customerEditModel.billingCountryList.length; i++)
            {
                if ($scope.customerEditModel.billingCountryList[i].id == $scope.customerEditModel.billingcountry)
                {
                    modifyCustomerParam.billing_country = $scope.customerEditModel.billingCountryList[i].name;
                }
            }

            modifyCustomerParam.billing_pincode = $scope.customerEditModel.billingpostcode;
            modifyCustomerParam.shopping_address = $scope.customerEditModel.shoppingaddr;
            modifyCustomerParam.shopping_city_id = $scope.customerEditModel.shoppingcity;
            modifyCustomerParam.shopping_city = '';
            for (var i = 0; i < $scope.customerEditModel.shoppingCityList.length; i++)
            {
                if ($scope.customerEditModel.shoppingCityList[i].id == $scope.customerEditModel.shoppingcity)
                {
                    modifyCustomerParam.shopping_city = $scope.customerEditModel.shoppingCityList[i].name;
                }
            }
            modifyCustomerParam.shopping_state_id = $scope.customerEditModel.shoppingstate;
            modifyCustomerParam.shopping_state = "";
            for (var i = 0; i < $scope.customerEditModel.shoppingStateList.length; i++)
            {
                if ($scope.customerEditModel.shoppingStateList[i].id == $scope.customerEditModel.shoppingstate)
                {
                    modifyCustomerParam.shopping_state = $scope.customerEditModel.shoppingStateList[i].name;
                }
            }

            modifyCustomerParam.shopping_country_id = $scope.customerEditModel.shoppingcountry;
            modifyCustomerParam.shopping_country = "";
            for (var i = 0; i < $scope.customerEditModel.shoppingCountryList.length; i++)
            {
                if ($scope.customerEditModel.shoppingCountryList[i].id == $scope.customerEditModel.shoppingcountry)
                {
                    modifyCustomerParam.shopping_country = $scope.customerEditModel.shoppingCountryList[i].name;
                }
            }
            modifyCustomerParam.shopping_pincode = $scope.customerEditModel.shoppingpostcode;
            modifyCustomerParam.is_active = 1;
            modifyCustomerParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                {
                    continue;
                }

                var customattributeItem = {};

                if ($scope.customAttributeList[i].value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].value;
                } else if ($scope.customAttributeList[i].default_value != undefined)
                {
                    customattributeItem.value = $scope.customAttributeList[i].default_value;
                } else
                {
                    customattributeItem.value = "";
                }

                customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                modifyCustomerParam.customattribute.push(customattributeItem);
            }

            adminService.editCustomerInfo(modifyCustomerParam, $scope.customerEditModel.customerInfo.id, headers).then(function (response) {
                if (response.data.success == true)
                {
                    if ($scope.customerDualEditModel.customerInfo != undefined && $scope.customerDualEditModel.customerInfo != null
                            )
                    {
                        $scope.modifyCustomerDualInfoDetails(imgSave);
                    } else
                    {
                        $scope.formReset();
                        $state.go('app.customers');
                    }
                } else
                {
                    $scope.isDataSavingProcess = false;
                }
            });
        };

        $scope.modifyCustomerDualInfoDetails = function (imgSave) {
            var headers = {};
            headers['screen-code'] = 'customer';
            $scope.isDataSavingProcess = true;
            var modifyCustomerParam = {};
            modifyCustomerParam.id = $scope.customerEditModel.customerInfo.id;
            modifyCustomerParam.customer_id = $scope.customerDualEditModel.customer_id;
            modifyCustomerParam.contact_id = $scope.customerDualEditModel.contact_id;
            modifyCustomerParam.fname = $scope.customerDualEditModel.name;
            modifyCustomerParam.company = $scope.customerDualEditModel.companyname;
            modifyCustomerParam.phone = $scope.customerEditModel.customerInfo.phone;
            modifyCustomerParam.email = $scope.customerEditModel.customerInfo.email;
            modifyCustomerParam.city = $scope.customerEditModel.customerInfo.city;
            modifyCustomerParam.country = $scope.customerEditModel.customerInfo.country;
            modifyCustomerParam.address = $scope.customerEditModel.customerInfo.address;
            modifyCustomerParam.state = $scope.customerEditModel.customerInfo.state;
            modifyCustomerParam.zip = $scope.customerEditModel.customerInfo.zip;
            modifyCustomerParam.password = $scope.customerEditModel.password;
            if (typeof $scope.customerEditModel.dob == 'object')
            {
                $scope.customerEditModel.dob = utilityService.parseDateToStr($scope.customerEditModel.dob, 'yyyy-MM-dd');
            }
            modifyCustomerParam.dob = utilityService.changeDateToSqlFormat($scope.customerEditModel.dob, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date1 == 'object')
            {
                $scope.customerEditModel.special_date1 = utilityService.parseDateToStr($scope.customerEditModel.special_date1, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date1 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date1, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date2 == 'object')
            {
                $scope.customerEditModel.special_date2 = utilityService.parseDateToStr($scope.customerEditModel.special_date2, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date2 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date2, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date3 == 'object')
            {
                $scope.customerEditModel.special_date3 = utilityService.parseDateToStr($scope.customerEditModel.special_date3, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date3 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date3, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date4 == 'object')
            {
                $scope.customerEditModel.special_date4 = utilityService.parseDateToStr($scope.customerEditModel.special_date4, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date4 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date4, 'yyyy-MM-dd');
            if (typeof $scope.customerEditModel.special_date5 == 'object')
            {
                $scope.customerEditModel.special_date5 = utilityService.parseDateToStr($scope.customerEditModel.special_date5, 'yyyy-MM-dd');
            }
            modifyCustomerParam.special_date5 = utilityService.changeDateToSqlFormat($scope.customerEditModel.special_date5, 'yyyy-MM-dd');
            modifyCustomerParam.iswelcomeEmail = ($scope.customerEditModel.iswelcomeEmail == true ? 1 : 0);
            modifyCustomerParam.is_sale = ($scope.customerEditModel.is_sale == true ? 1 : 0);
            modifyCustomerParam.is_purchase = ($scope.customerEditModel.is_purchase == true ? 1 : 0);
            modifyCustomerParam.isactive = 1;
            modifyCustomerParam.acode = $scope.customerEditModel.acode;
            if ($scope.customerEditModel.img.length > 0)
            {
                modifyCustomerParam.img = $scope.customerEditModel.img[0].filepath;
            } else
            {
                modifyCustomerParam.img = '';
            }
            modifyCustomerParam.billing_address = $scope.customerDualEditModel.billingaddr;
            modifyCustomerParam.billing_city = $scope.customerDualEditModel.billingcity;
            modifyCustomerParam.billing_state = $scope.customerDualEditModel.billingstate;
            modifyCustomerParam.billing_country = $scope.customerDualEditModel.billingcountry;
            modifyCustomerParam.billing_pincode = $scope.customerDualEditModel.billingpostcode;
            modifyCustomerParam.shopping_address = $scope.customerDualEditModel.shoppingaddr;
            modifyCustomerParam.shopping_city = $scope.customerDualEditModel.shoppingcity;
            modifyCustomerParam.shopping_state = $scope.customerDualEditModel.shoppingstate;
            modifyCustomerParam.shopping_country = $scope.customerDualEditModel.shoppingcountry;
            modifyCustomerParam.shopping_pincode = $scope.customerDualEditModel.shoppingpostcode;
            modifyCustomerParam.is_active = 1;
            modifyCustomerParam.customattribute = [];
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {

                var attribute_prefix = $scope.customAttributeList[i].attribute_code.split('_')[0];
                if (attribute_prefix == $rootScope.appConfig.dual_language_label)
                {
                    var customattributeItem = {};

                    if ($scope.customAttributeList[i].value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].value;
                    } else if ($scope.customAttributeList[i].default_value != undefined)
                    {
                        customattributeItem.value = $scope.customAttributeList[i].default_value;
                    } else
                    {
                        customattributeItem.value = "";
                    }

                    customattributeItem.attribute_code = $scope.customAttributeList[i].attribute_code;
                    modifyCustomerParam.customattribute.push(customattributeItem);
                }

            }

            adminService.editCustomerDualInfo(modifyCustomerParam, $scope.customerDualEditModel.customerInfo.id, headers).then(function (response) {
                if (response.data.success == true)
                {
                    if (imgSave == 1)
                    {
                        $scope.formReset();
                        $state.go('app.customerEdit', {'id': $stateParams.id}, {'reload': true});
                        // $state.go('app.customerEdit',{'reload': true});
                        // $window.location.reload();
                    } else
                    {
                        $scope.formReset();
                        $state.go('app.customers');
                    }
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.belongtoUserLoaded = false;
        $scope.getBelongtoUserList = function () {
            var userListParam = {};
            userListParam.id = "";
            userListParam.name = "";
            userListParam.start = 0;
            userListParam.limit = 100;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(userListParam, configOption).then(function (response) {
                var data = response.data.data;
                $scope.customerEditModel.userList = data;
                $scope.belongtoUserLoaded = true;
            });
        };

        $scope.customerDualInfoLoaded = false;
        $scope.getCustomerDualInfo = function () {
            var getListParam = {};
            getListParam.customer_id = $stateParams.id;
            getListParam.name = '';
            getListParam.is_active = 1;
            $scope.customerDualEditModel.customerInfo = '';
            adminService.getCustomerDualInfoDetail(getListParam).then(function (response) {
                var data = response.data;
                if (typeof data.data != undefined)
                {
                    $scope.customerDualEditModel.customerInfo = data.data;
                }
                else
                {
                    $scope.customerDualEditModel.customerInfo = '';
                }
                $scope.customerDualInfoLoaded = true;
                $scope.initUpdateDetail();
            });
        };
        $scope.getCustomerInfo = function () {
            $scope.customerEditModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.name = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.is_active = 1;
            adminService.getCustomerDetail(getListParam).then(function (response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.customerEditModel.customerInfo = data.data;
                    $scope.customerEditModel.iswelcomeEmail = $scope.customerEditModel.customerInfo.iswelcomeEmail == 1 ? true : false;
                    $scope.customerEditModel.is_sale = $scope.customerEditModel.customerInfo.is_sale == 1 ? true : false;
                    $scope.customerEditModel.is_purchase = $scope.customerEditModel.customerInfo.is_purchase == 1 ? true : false;
                }
                $scope.customerEditModel.total = data.total;
                $scope.getBelongtoUserList();
                $scope.getCustomerDualInfo();
                $scope.initUpdateDetail();
            });
        };
        $scope.$on('customSelectOptionChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomSelectValue(data);
        });

        $scope.updateCustomSelectValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        }

        $scope.$on('customfieldValueChangeEvent', function (event, data)
        {
            console.log(data);
            $scope.updateCustomFieldValue(data);
        });

        $scope.updateCustomFieldValue = function (data)
        {
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.customAttributeList[i].attribute_code == data.code)
                {
                    $scope.customAttributeList[i].value = data.value;
                    break;
                }
            }
        };

        $scope.getCountryList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.code = "";
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;

                $timeout(function () {
                    $scope.customerEditModel.billingCountryList = data;
                    $scope.customerEditModel.shoppingCountryList = data;

                    $scope.customerDualEditModel.billingCountryList = data;
                    $scope.customerDualEditModel.shoppingCountryList = data;

                });
            });
        };


        $scope.initBillingAddressStateListTimeoutPromise = null;

        $scope.initBillingAddressStateList = function (searchkey)
        {
            if ($scope.initBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressStateListTimeoutPromise);
            }
            $scope.initBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'billingstate');
            }, 300);

        };

        $scope.initShippingAddressStateListTimeoutPromise = null;

        $scope.initShippingAddressStateList = function (searchkey)
        {
            if ($scope.initShippingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initShippingAddressStateListTimeoutPromise);
            }
            $scope.initShippingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'shippingstate');
            }, 300);

        };

        $scope.initDualBillingAddressStateListTimeoutPromise = null;

        $scope.initDualBillingAddressStateList = function (searchkey)
        {
            if ($scope.initDualBillingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualBillingAddressStateListTimeoutPromise);
            }
            $scope.initDualBillingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'dualbillingstate');
            }, 300);

        };

        $scope.initDualShippingAddressStateListTimeoutPromise = null;

        $scope.initDualShippingAddressStateList = function (searchkey)
        {
            if ($scope.initDualShippingAddressStateListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualShippingAddressStateListTimeoutPromise);
            }
            $scope.initDualShippingAddressStateListTimeoutPromise = $timeout(function () {
                $scope.getStateList(searchkey, 'dualshippingstate');
            }, 300);

        };



        $scope.getStateList = function (searchkey, fieldName) {

            console.log('searchkey  = ' + searchkey);
            console.log('fieldName  = ' + fieldName);
            
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_id = "";

            if (fieldName == 'billingstate')
            {
                getListParam.country_id = $scope.customerEditModel.billingcountry;
            } else if (fieldName == 'shippingstate')
            {
                getListParam.country_id = $scope.customerEditModel.shoppingcountry;
            } else if (fieldName == 'dualbillingstate')
            {
                getListParam.country_id = $scope.customerDualEditModel.shoppingcountry;
            } else if (fieldName == 'dualshippingstate')
            {
                getListParam.country_id = $scope.customerDualEditModel.shoppingcountry;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.fieldName = fieldName;
            configOption.searchkey = searchkey;

            adminService.getStateList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined'
                        && response.config.config.fieldName != undefined && response.config.config.searchkey != undefined)
                {

//                    if (data.length == 0)
//                    {
//                        data.push({"name": response.config.config.searchkey});
//                    }
                    $timeout(function () {
                        if (fieldName == 'billingstate')
                        {
                            $scope.customerEditModel.billingStateList = data;
                        } else if (fieldName == 'shippingstate')
                        {
                            $scope.customerEditModel.shoppingStateList = data;
                        } else if (fieldName == 'dualbillingstate')
                        {
                            $scope.customerDualEditModel.billingStateList = data;
                        } else if (fieldName == 'dualshippingstate')
                        {
                            $scope.customerDualEditModel.shoppingStateList = data;
                        }
                    }, 0);
                }

            });

        };


        $scope.initBillingAddressCityListTimeoutPromise = null;

        $scope.initBillingAddressCityList = function (searchkey)
        {
            if ($scope.initBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initBillingAddressCityListTimeoutPromise);
            }
            $scope.initBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'billingcity');
            }, 300);

        };
        $scope.initShippingAddressCityListTimeoutPromise = null;

        $scope.initShippingAddressCityList = function (searchkey)
        {
            if ($scope.initShippingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initShippingAddressCityListTimeoutPromise);
            }
            $scope.initShippingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'shippingcity');
            }, 300);

        };

        $scope.initDualBillingAddressCityListTimeoutPromise = null;

        $scope.initDualBillingAddressCityList = function (searchkey)
        {
            if ($scope.initDualBillingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualBillingAddressCityListTimeoutPromise);
            }
            $scope.initDualBillingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'dualbillingcity');
            }, 300);

        };
        $scope.initDualShippingAddressCityListTimeoutPromise = null;

        $scope.initDualShippingAddressCityList = function (searchkey)
        {
            if ($scope.initDualShippingAddressCityListTimeoutPromise != null)
            {
                $timeout.cancel($scope.initDualShippingAddressCityListTimeoutPromise);
            }
            $scope.initDualShippingAddressCityListTimeoutPromise = $timeout(function () {
                $scope.getCityList(searchkey, 'dualshippingcity');
            }, 300);

        };


        $scope.getCityList = function (searchkey, fieldName) {

            console.log('searchkey  = ' + searchkey);
            console.log('fieldName  = ' + fieldName);

            var getListParam = {};
            getListParam.id = "";
            getListParam.name = searchkey;
            getListParam.country_name = "";
            getListParam.state_name = "";
            getListParam.start = 0;
            getListParam.limit = 0;
            if (fieldName == 'billingcity')
            {
                getListParam.state_id = $scope.customerEditModel.billingstate;
                getListParam.country_id = $scope.customerEditModel.billingcountry;
            } else if (fieldName == 'shippingcity')
            {
                getListParam.state_id = $scope.customerEditModel.shoppingstate;
                getListParam.country_id = $scope.customerEditModel.shoppingcountry;
            } else if (fieldName == 'dualbillingcity')
            {
                getListParam.state_id = $scope.customerDualEditModel.billingstate;
                getListParam.country_id = $scope.customerDualEditModel.billingcountry;
            } else if (fieldName == 'dualshippingcity')
            {
                getListParam.state_id = $scope.customerDualEditModel.shoppingstate;
                getListParam.country_id = $scope.customerDualEditModel.shoppingcountry;
            }

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            configOption.fieldName = fieldName;
            configOption.searchkey = searchkey;

            adminService.getCityList(getListParam, configOption).then(function (response)
            {
                if (!response.data.success)
                {
                    return;
                }
                var data = response.data.list;
                if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined'
                        && response.config.config.fieldName != undefined && response.config.config.searchkey != undefined)
                {

//                    if (data.length == 0)
//                    {
//                        data.push({"name": response.config.config.searchkey});
//                    }
                    $timeout(function () {
                        if (fieldName == 'billingcity')
                        {
                            $scope.customerEditModel.billingCityList = data;
                        } else if (fieldName == 'shippingcity')
                        {
                            $scope.customerEditModel.shoppingCityList = data;
                        } else if (fieldName == 'dualbillingcity')
                        {
                            $scope.customerDualEditModel.billingCityList = data;
                        } else if (fieldName == 'dualshippingcity')
                        {
                            $scope.customerDualEditModel.shoppingCityList = data;
                        }
                    }, 0);

                }

            });

        };

        $scope.billingAddressCountryChange = function ()
        {
            $scope.customerEditModel.billingstate = '';
            $scope.customerEditModel.billingcity = '';

            $scope.customerEditModel.billingCityList = [];
            $scope.customerEditModel.billingStateList = [];

            $scope.updateShoppingAddr();
        }

        $scope.billingAddressStateChange = function ()
        {

            $scope.customerEditModel.billingcity = '';
            $scope.customerEditModel.billingCityList = [];

            $scope.updateShoppingAddr();

        }

        $scope.shippingAddressCountryChange = function ()
        {
            $scope.customerEditModel.shoppingstate = '';
            $scope.customerEditModel.shoppingcity = '';

            $scope.customerEditModel.shoppingCityList = [];
            $scope.customerEditModel.shoppingStateList = [];

            $scope.updateShoppingAddr('update');
        }

        $scope.shippingAddressStateChange = function ()
        {
            $scope.customerEditModel.shoppingcity = '';
            $scope.customerEditModel.shoppingCityList = [];

            $scope.updateShoppingAddr('update');
        }


        $scope.getCountryList();
        $scope.getCustomerInfo();
    }]);




