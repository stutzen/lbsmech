

app.controller('albumViewNewCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'sweet', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, sweet) {

        $scope.id = '';
        $scope.$on('initCommentEvent', function(event, value) {
            if ((value != null && value != '') || value == 0)
            {
                $scope.id = $scope.albumViewModel.imglist[value].id;
                $scope.getCommentlist($scope.id);
            }

        });

        $scope.albumViewModel = {
            "id": "",
            "name": "",
            "code": "",
            "type": "",
            "comments": "",
            "newcomments": "",
            "imglist": [],
            isLoadingProgress: false,
            "album_id": '',
            "list": []

        };
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.comment_form != 'undefined' && typeof $scope.comment_form.$pristine != 'undefined' && !$scope.comment_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.isDataSavingProcess = false;
        $scope.validationFactory = ValidationFactory;

        $scope.getCommentlist = function(id) {

            $scope.id = id;
            var getCommentListParam = {};
            getCommentListParam.album_asset_id = id;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getcommentlist(getCommentListParam, configOption).then(function(response) {
                var data = response.data;
                $scope.albumViewModel.commentList = data.list;
                $scope.albumViewModel.isLoadingProgress = false;
            });

        };

        $scope.saveComment = function() {

            if ($scope.albumViewModel.newcomments != '')
            {
                // $scope.isDataSavingProcess = true;
                var createParam = {};
                createParam.album_asset_id = $scope.id;
                createParam.album_id = $scope.albumViewModel.album_id;
                createParam.album_code = $scope.albumViewModel.code;
                createParam.created_by_name = '';
                createParam.updated_by_name = '';
                createParam.comments = $scope.albumViewModel.newcomments;
                adminService.saveComment(createParam).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.albumViewModel.newcomments = '';
                        $scope.getCommentlist($scope.id);
                       // $state.go('app.albumViewNew', {'id': $stateParams.id}, {'reload': true});
                    }
                    //  $scope.isDataSavingProcess = false;
                });
            }
            else
            {
                sweet.show('Oops...', 'Give Comments First..', 'error');
                // $scope.isDataSavingProcess = false;
            }
        };

        $scope.getAlbumAssetList = function() {
            var getShareListParam = {};
            getShareListParam.album_id = $scope.albumViewModel.album_id;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAlbumAssetList(getShareListParam, configOption).then(function(response) {
                var data = response.data.list;
                $scope.albumViewModel.imglist = data;
                if ($scope.albumViewModel.imglist.length > 0)
                {
                    for (var i = 0; i < $scope.albumViewModel.imglist.length; i++)
                    {
                        $scope.albumViewModel.imglist[i].path = $scope.albumViewModel.imglist[i].url;
                    }
                    $scope.getCommentlist($scope.albumViewModel.imglist[0].id);
                }
            });

        };

        $scope.getAlbum = function() {
            $scope.albumViewModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.id;
            adminService.viewAlbum(getListParam).then(function(response) {
                var data = response.data.data;
                $scope.albumViewModel.list = data;
                $scope.albumViewModel.album_id = $scope.albumViewModel.list.id;
                $scope.albumViewModel.name = $scope.albumViewModel.list.name;
                $scope.albumViewModel.code = $scope.albumViewModel.list.code;
                $scope.albumViewModel.type = $scope.albumViewModel.list.type;
                $scope.albumViewModel.comments = $scope.albumViewModel.list.comments;
//                $scope.albumViewModel.imglist = data.asset;
//                for (var i = 0; i < $scope.albumViewModel.imglist.length; i++)
//                {
//                    $scope.albumViewModel.imglist[i].path = $scope.albumViewModel.imglist[i].url;
//                }
//                $scope.getCommentlist($scope.albumViewModel.imglist[0].id);
                $scope.albumViewModel.total = data.total;
                $scope.getAlbumAssetList();

            });
        };

        $scope.getAlbum();
        // $scope.getCommentlist();

    }]);




