


app.controller('expenseListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, utilityService, $httpService, $filter, Auth, $state, $timeout, APP_CONST) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.expenseModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.expenseModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            customerInfo: {}
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_purchase = 1;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                return model.fname;
            }
            return  '';
        };

        $scope.$watch('searchFilter.customerInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '')
            {
                $scope.initTableFilter();
            }
        });

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = '';
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
            getListParam.start = ($scope.expenseModel.currentPage - 1) * $scope.expenseModel.limit;
            getListParam.limit = $scope.expenseModel.limit;
            $scope.expenseModel.isLoadingProgress = true;

            adminService.getExpenseList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.expenseModel.list = data;
                for (var i = 0; i < $scope.expenseModel.list.length; i++)
                {
                    $scope.expenseModel.list[i].newpaymentdate = utilityService.parseStrToDate($scope.expenseModel.list[i].payment_date);
                    $scope.expenseModel.list[i].payment_date = utilityService.parseDateToStr($scope.expenseModel.list[i].newpaymentdate, $scope.dateFormat);
                }
                $scope.expenseModel.total = response.data.total;
                $scope.expenseModel.isLoadingProgress = false;
            });

        };


        $scope.getList();
    }]);








