

app.controller('stateAddctrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.stateModel = {
            id: '',
            name: '',
            isActive: true,
            code: '',
            country_name:'',
            country_id:'',
            country_list:[]
        }

        $scope.validationFactory = ValidationFactory;
         $scope.stateModel.isLoadingProgress = false;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_state_form != 'undefined' && typeof $scope.add_state_form.$pristine != 'undefined' && !$scope.add_state_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_state_form.$setPristine();
            if (typeof $scope.add_state_form != 'undefined')
            {
                $scope.stateModel.name = "";
                $scope.stateModel.code = "";
                $scope.stateModel.isActive = true;
                $scope.stateModel.country_id="";
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.createstate = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addStateParam = {};
            var headers = {};
            headers['screen-code'] = 'state';
            addStateParam.id = 0;
            addStateParam.name = $scope.stateModel.name;
            addStateParam.code = $scope.stateModel.code;
            addStateParam.is_active = $scope.stateModel.isActive == true ? 1 : 0;
            addStateParam.country_id = $scope.stateModel.country_id;
             for (var i = 0; i < $scope.stateModel.country_list.length; i++)
            {
                if ($scope.stateModel.country_id == $scope.stateModel.country_list[i].id)
                {
                    addStateParam.country_name = $scope.stateModel.country_list[i].name;
                }
            }
            adminService.saveState(addStateParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.state');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        
        $scope.getcountryList = function() {          

            $scope.stateModel.isLoadingProgress = true;
            var countryListParam = {};
            var headers = {};
            headers['screen-code'] = 'country';           
           
            countryListParam.id = '';
            
            countryListParam.is_active = 1;
           
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.stateModel.country_list = data.list;                   
                }
                $scope.stateModel.isLoadingProgress = false;
            });

        };
        $scope.getcountryList();
        
         
    }]);






