
app.controller('followupReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.followupModel = {
        currentPage: 1,
        total: 0,
        total_Amount: 0,
        limit: 50,
        list: [],
        stageList:[],
        length: [],
        serverList: null,
        isLoadingProgress: false,
        isSearchLoadingProgress: false
    };
    $scope.showDetail = false;
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.followupModel.limit = $scope.pagePerCount[0];
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.validationFactory = ValidationFactory;
    $scope.openDate = function(index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        }
       

    };

   $scope.searchFilter = {
            stage: '',
            fromdate : $scope.currentDate,
            stage_flag: 'all',
            subject : ''
        };

    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            stage: '',
            fromdate : $scope.currentDate,
            stage_flag: 'all',
            subject : ''
        };
        $scope.followupModel.list = [];
    }
    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise !== null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    $scope.getStageList = function ()
        {
            $scope.followupModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStageList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.followupModel.stageList = data.list;
                }
            });
        };
   
    $scope.getList = function() {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'followupreport';
        getListParam.name = $scope.searchFilter.subject;
        if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.date = utilityService.changeDateToSqlFormat(getListParam.date, $scope.adminService.appConfig.date_format);
            }
        getListParam.stage_id = $scope.searchFilter.stage;
        getListParam.stage_flag = $scope.searchFilter.stage_flag;
        getListParam.is_active = 1;
        getListParam.mode = 0;
        getListParam.start = ($scope.followupModel.currentPage - 1) * $scope.followupModel.limit;
        getListParam.limit = $scope.followupModel.limit;
        $scope.followupModel.isLoadingProgress = true;
        $scope.followupModel.isSearchLoadingProgress = true;
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getDealList(getListParam, configOption, headers).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.followupModel.list = data;
                 for (var i = 0; i < $scope.followupModel.list.length; i++)
                    {
                        $scope.followupModel.list[i].index = getListParam.start + i + 1;
                        $scope.followupModel.list[i].amount = utilityService.changeCurrency($scope.followupModel.list[i].amount, $rootScope.appConfig.thousand_seperator);
                        $scope.followupModel.list[i].newdate = utilityService.parseStrToDate($scope.followupModel.list[i].updated_at);
                        $scope.followupModel.list[i].updated_at = utilityService.parseDateToStr($scope.followupModel.list[i].newdate, $rootScope.appConfig.date_format);
                        var datevalue = moment($scope.followupModel.list[i].next_follow_up).valueOf();
                        if (datevalue > 0)
                        {
                            var nextdate = $scope.followupModel.list[i].next_follow_up.split(' ');
                            var newnextdate = utilityService.convertToUTCDate(nextdate[0]);
                            $scope.followupModel.list[i].next_follow_up = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                        } else
                            $scope.followupModel.list[i].next_follow_up = '';
                        //$scope.items.push($scope.followupModel.list[i]);
                    }
            }
            $scope.followupModel.isLoadingProgress = false;
            $scope.followupModel.isSearchLoadingProgress = false;
        });

    };
    
    $scope.getStageList();
    $scope.getList();
}]);




