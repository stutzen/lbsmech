
app.controller('attendanceReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;
    $scope.attendanceModel = {
        currentPage: 1,
        total: 0,
        limit: 4,
        list: [],
        total_duration: '',
        serverList: null,
        isLoadingProgress: false,
        isSearchLoadingProgress: false,
        teamList : []
    };
    $scope.showDetail = false;
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.attendanceModel.limit = $scope.pagePerCount[0];
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
    $scope.validationFactory = ValidationFactory;


    $scope.searchFilter = {
        fromdate: '',
        todate: '',
        employeeInfo: '',
        teamid : ''

    };
    $scope.openDate = function (index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        } else if (index === 1)
{
            $scope.toDateOpen = true;
        }
    };
    $scope.clearFilters = function ()
    {
        $scope.showDetail = false;
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            employeeInfo: '',
            teamid : ''
        };
        $scope.attendanceModel.list = [];

    }
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: false
    };
    $scope.validateDateFilterData = function ()
    {
        var retVal = false;
        if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
        {
            retVal = true;
        }
        return retVal;
    };
    $scope.searchFilterValue = "";
    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
    $scope.getNavigationBlockMsg = function ()
    {
        if (typeof $scope.attendance_form != 'undefined' && typeof $scope.attendance_form.$pristine != 'undefined' && !$scope.attendance_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    }
    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function ()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getAttendanceList, 300);
    }
    $scope.getEmployeelist = function (val)
    {
        var autosearchParam = {};
        autosearchParam.fname = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };

    $scope.formatEmployeeModel = function (model) {

        if (model != null && model != undefined)
        {
            return model.f_name;
        }
        return  '';
    };

    $scope.getAttendanceList = function () {

        $scope.attendanceModel.isLoadingProgress = true;  
        var attendanceListParam = {};
        var headers = {};
        headers['screen-code'] = 'attendancereport';
        attendanceListParam.emp_id = '';
        if ($scope.showDetail)
        {
            attendanceListParam.show_all = 1;
        } else
{

            attendanceListParam.show_all = '';
        }
        if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                attendanceListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            attendanceListParam.to_date = utilityService.changeDateToSqlFormat(attendanceListParam.to_date, $scope.dateFormat);
            attendanceListParam.from_date = attendanceListParam.to_date;
        } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
{
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                attendanceListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            attendanceListParam.from_date = utilityService.changeDateToSqlFormat(attendanceListParam.from_date, $scope.dateFormat);
            attendanceListParam.to_date = attendanceListParam.from_date;
        } else
{
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                attendanceListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                attendanceListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            attendanceListParam.to_date = utilityService.changeDateToSqlFormat(attendanceListParam.to_date, $scope.dateFormat);
            attendanceListParam.from_date = utilityService.changeDateToSqlFormat(attendanceListParam.from_date, $scope.dateFormat);
        }
        if ($scope.searchFilter.employeeInfo != null && typeof $scope.searchFilter.employeeInfo != 'undefined' && typeof $scope.searchFilter.employeeInfo.id != 'undefined')
        {
            attendanceListParam.emp_id = $scope.searchFilter.employeeInfo.id;
        } else
{
            attendanceListParam.emp_id = '';
        }
        attendanceListParam.team_id = $scope.searchFilter.teamid;
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getAttendanceList(attendanceListParam, configOption, headers).then(function (response)
        {
            var data = response.data;
            $scope.attendanceModel.list = data.list;
            if($scope.showDetail == true)
            {
                for(var i=0; i<$scope.attendanceModel.list.length; i++)
                {
                    for(var j=0; j<$scope.attendanceModel.list[i].data.length; j++) 
                    {
                        $scope.attendanceModel.list[i].data[j].punchdate = $scope.attendanceModel.list[i].data[j].start_time.split(' ')[0];
                        $scope.attendanceModel.list[i].data[j].punchDate = $scope.attendanceModel.list[i].data[j].punchdate;
                        $scope.attendanceModel.list[i].data[j].punchtime = $scope.attendanceModel.list[i].data[j].start_time.split(' ')[1];
                        $scope.attendanceModel.list[i].data[j].punchTime = $scope.attendanceModel.list[i].data[j].punchtime;
//                        $scope.attendanceModel.list[i].data[j].punchout = $scope.attendanceModel.list[i].data[j].end_time.split(' ')[1];
//                        $scope.attendanceModel.list[i].data[j].punchOut = $scope.attendanceModel.list[i].data[j].punchout;
                        
                        if($scope.attendanceModel.list[i].data[j].end_time != null && $scope.attendanceModel.list[i].data[j].end_time != 'undefined' && $scope.attendanceModel.list[i].data[j].end_time != '')
                        {
                            $scope.attendanceModel.list[i].data[j].punchout = $scope.attendanceModel.list[i].data[j].end_time.split(' ')[1];
                            $scope.attendanceModel.list[i].data[j].punchOut = $scope.attendanceModel.list[i].data[j].punchout;
                        } else
                        {
                            $scope.attendanceModel.list[i].data[j].punchout = '-';
                            $scope.attendanceModel.list[i].data[j].punchOut = '-';
                            $scope.attendanceModel.list[i].data[j].duration = '-';
                        }
                    }
                } 
            }
           
            $scope.attendanceModel.isLoadingProgress = false;
        });

    };
        $scope.getEmployeeTeam = function ()
       {            
           var getListParam = {};  
           getListParam.type = '';
           getListParam.is_active = 1;
           var configOption = adminService.handleOnlyErrorResponseConfig;
           adminService.getEmployeeTeamList(getListParam, configOption).then(function (response) {
               if (response.data.success === true)
               {
                   var data = response.data.list;
                   $scope.attendanceModel.teamList = data;                    
               }                
           });
       };
       $scope.getEmployeeTeam();


}]);
















