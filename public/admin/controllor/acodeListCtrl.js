app.controller('acodeListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.acodeModel = {
            
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
           
            isLoadingProgress: true

        };
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.acodeModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: '',
            code: ''
            
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: '',
                code: ''
            };
            $scope.initTableFilter();
        }

        $scope.getList = function() {          

            $scope.acodeModel.isLoadingProgress = true;
            var acodeListParam = {};
            var headers = {};
            headers['screen-code'] = 'acodesettings';
            acodeListParam.name = $scope.searchFilter.name;
            acodeListParam.code = $scope.searchFilter.code;
            acodeListParam.id = '';
            
            acodeListParam.is_active = 1;
            acodeListParam.start = ($scope.acodeModel.currentPage - 1) * $scope.acodeModel.limit;
            acodeListParam.limit = $scope.acodeModel.limit;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAcodeList(acodeListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.acodeModel.list = data.list;
                    $scope.acodeModel.total = data.total;
                }
                $scope.acodeModel.isLoadingProgress = false;
            });

        };

        $scope.getList();

    }]);







