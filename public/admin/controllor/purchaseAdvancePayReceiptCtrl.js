app.controller('purchaseAdvancePayRecepiptCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory','customerFactory', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory,customerFactory) {

    $rootScope.getNavigationBlockMsg = null;
    $scope.advancePaymentModel = {
        currentPage: 1,
        total: 0,
        limit: 4,
        list: [],
        status: '',
        isLoadingProgress: true
    };
    $scope.validationFactory = ValidationFactory;
    $scope.pagePerCount = [50, 100];
    $scope.advancePaymentModel.limit = $scope.pagePerCount[0];
    $scope.adminService = adminService;
     $scope.customerFilter = customerFactory.get();
    customerFactory.set('');
    $scope.searchFilter =
    {
        customerInfo: {},
        status: '',
        from_date:'',
        to_date:''
    };

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function ()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.openDate = function(index) {

        if (index == 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index == 1)
        {
            $scope.toDateOpen = true;
        }

    };
    $scope.refreshScreen = function ()
    {
        $scope.searchFilter =
        {
            customerInfo: {},
            status: ''
        };
        $scope.initTableFilter();
    }
    $scope.selectedpaymentId = '';
    $scope.showdeletePopup = false;
    $scope.isdeleteProgress = false;
    $scope.showPopup = function (id)
    {
        $scope.selectedpaymentId = id;
        $scope.showdeletePopup = true;
    };

    $scope.closePopup = function ()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
    };

    $scope.deleteAdvancePaymentItem = function ( )
    {
        if ($scope.isdeleteProgress == false)
        {
            $scope.isdeleteProgress = true;

            var getListParam = {};
            getListParam.id = $scope.selectedpaymentId;
            //getListParam.voucher_type = 'advance_purchase';
            var headers = {};
            headers['screen-code'] = 'purchaseAdvPayReceipt';
            adminService.deleteAdvancepayment(getListParam, getListParam.id, headers).then(function (response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }

                $scope.isdeleteProgress = false;
            });

        }

    };

    $scope.$watch('searchFilter.customerInfo', function (newVal, oldVal)
    {
        if (typeof newVal == 'undefined' || newVal == '')
        {
            $scope.initTableFilter();
        }
    });

    $scope.getCustomerList = function (val)
    {
        var autosearchParam = {};
        autosearchParam.search = val;
        return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = [];
            if (data.length > 0)
            {
                for (var i = 0; i < data.length; i++)
                {
                    if (data[i].is_purchase == '1' || data[i].is_purchase == 1)
                    {
                        hits.push(data[i]);
                    }
                }
            }
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    if($scope.customerFilter != '' && $scope.customerFilter != null && $scope.customerFilter != undefined)
    {    
        $scope.searchFilter.customerInfo = $scope.customerFilter;
    }

    $scope.formatCustomerModel = function (model)
    {
        if (model != null)
        {
            if (model.fname != undefined && model.phone != undefined && model.email != undefined && model.company != undefined)
            {
                return model.fname + '(' + model.phone + ',' + model.email + ',' + model.company + ')';
            } else if (model.fname != undefined && model.phone != undefined && model.company != undefined)
{
                return model.fname + '(' + model.phone + ')';
            }
            else
            {
                return model.fname;
            }    
        }
        return  '';
    };

    $scope.getList = function ()
    {
        $scope.advancePaymentModel.isLoadingProgress = true;
        var getListParam = {};
        var headers = {};
        
        headers['screen-code'] = 'purchaseAdvPayReceipt';
        getListParam.type=2;
        var configOption = adminService.handleOnlyErrorResponseConfig;

        if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
        {
            getListParam.customer_id = $scope.searchFilter.customerInfo.id;
        } else
{
            getListParam.customer_id = '';
        }

        getListParam.start = ($scope.advancePaymentModel.currentPage - 1) * $scope.advancePaymentModel.limit;
        getListParam.limit = $scope.advancePaymentModel.limit;
        getListParam.is_active = 1;
        //getListParam.voucher_type = 'advance_purchase';
        getListParam.status = $scope.searchFilter.status;
        if ($scope.searchFilter.to_date != '' && ($scope.searchFilter.from_date == null || $scope.searchFilter.from_date == ''))
            {
                if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, 'yyyy-MM-dd');
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, 'yyyy-MM-dd');
                getListParam.from_date = getListParam.to_date;
            }
            else if ($scope.searchFilter.from_date != '' && ($scope.searchFilter.to_date == null || $scope.searchFilter.to_date == ''))
            {
                if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, 'yyyy-MM-dd');
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, 'yyyy-MM-dd');
                getListParam.to_date = getListParam.from_date;
            }
            else
            {
                if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, 'yyyy-MM-dd');
                }
                if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, 'yyyy-MM-dd');
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, 'yyyy-MM-dd');
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, 'yyyy-MM-dd');
            }
        adminService.getAdvancepaymentList(getListParam, configOption, headers).then(function (response)
        {

            var data = response.data;
            if (data.success == true)
            {
                $scope.advancePaymentModel.list = data.list;
                if ($scope.advancePaymentModel.list.length > 0)
                {
                    for (var i = 0; i < $scope.advancePaymentModel.list.length; i++)
                    {
                        $scope.advancePaymentModel.list[i].newdate = utilityService.parseStrToDate($scope.advancePaymentModel.list[i].date);
                        $scope.advancePaymentModel.list[i].date = utilityService.parseDateToStr($scope.advancePaymentModel.list[i].newdate, $rootScope.appConfig.date_format);
                        $scope.advancePaymentModel.list[i].balance_amount = utilityService.changeCurrency($scope.advancePaymentModel.list[i].balance_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.advancePaymentModel.list[i].amount = utilityService.changeCurrency($scope.advancePaymentModel.list[i].amount, $rootScope.appConfig.thousand_seperator);
                        $scope.advancePaymentModel.list[i].used_amount = parseFloat(utilityService.changeCurrency($scope.advancePaymentModel.list[i].used_amount, $rootScope.appConfig.thousand_seperator));
                    }
                }
                $scope.advancePaymentModel.total = data.total;
            }
            $scope.advancePaymentModel.isLoadingProgress = false;
        });
    };
    $scope.getList();
}]);




