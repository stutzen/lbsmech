app.controller('materialrequirementCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout','ValidationFactory','$window','utilityService', function($scope, $rootScope, adminService, $filter, Auth, $timeout,ValidationFactory,$window,utilityService) {

    $rootScope.getNavigationBlockMsg = null;
    $scope.materialModel = {
            
        currentPage: 1,
        total: 0,
        limit: 4,
        list: [],
        categorylist : [],
        isLoadingProgress: false

    };
    $scope.adminService = adminService;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.searchFilterNameValue = ''
    $scope.validationFactory = ValidationFactory;
    $scope.pagePerCount = [50, 100];
    $scope.materialModel.limit = $scope.pagePerCount[0];
    $scope.currentDate = new Date();
    $scope.searchFilter = {
        category_id: '',
        fromdate : '',
        todate : ''
    };
    $scope.formReset = function()
    {
        $scope.material_requirement_form.$setPristine();
        if (typeof $scope.material_requirement_form != 'undefined')
        {
            $scope.initTableFilter();
        }
    }
    $scope.searchFilter.todate = $scope.currentDate;

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getManufactureList, 300);
    }

        
    $scope.showdeletePopup = false;
    $scope.isdeleteProgress = false;

    $scope.showPopup = function(id)
    {
        $scope.selectStageId = id;
        $scope.showdeletePopup = true;
    };

    $scope.closePopup = function()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
    };

    $scope.deleteManufactureInfo = function( )
    {
        if($scope.isdeleteProgress == false)
        {
            $scope.isdeleteProgress = true;
               
            var getListParam = {};
            getListParam.id = $scope.selectStageId;
            var headers = {};
            headers['screen-code'] = 'materialrequirement';
            adminService.deleteStage(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getStageList();
                }
                $scope.isdeleteProgress = false;
            });
        }
    };
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.print = function()
    {
        $window.print();
    }
    $scope.openDate = function(index)
    {
        if(index == 0)
        {
            $scope.fromDateOpen = true;
        }   
        if(index == 1)
        {
            $scope.toDateOpen = true;
        }    
    }

    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            category_id: '',
            fromdate : '',
            todate : ''
        };
        $scope.materialModel.list = [];
    }
    $scope.getcategoryList = function()
    {
        var getListParam = {};
        getListParam.id = "";
        getListParam.category_id = "";
            
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getCategoryList(getListParam, configOption).then(function(response)
        {
            var data = response.data;
            $scope.materialModel.categorylist = data.list;
               
        });
    };
    $scope.getManufactureList = function() {          

        $scope.materialModel.isLoadingProgress = true;
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'materialrequirement';
        getListParam.name = $scope.searchFilter.name;
        getListParam.category_id = $scope.searchFilter.category_id;
        if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = getListParam.to_date;
        }
        else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
        {
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            getListParam.to_date = getListParam.from_date;
        }
        else
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
        }
        getListParam.is_active = 1;
        getListParam.start = ($scope.materialModel.currentPage - 1) * $scope.materialModel.limit;
        getListParam.limit = $scope.materialModel.limit;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getMaterialRequirementList(getListParam, configOption, headers).then(function(response)
        {
            if (response.data.success === true)
            {
                   
                var data = response.data;
                $scope.materialModel.list = data.list;
                for(var i = 0;i < $scope.materialModel.list.length; i++)
                {
                    $scope.materialModel.list[i].needtopurchase = 0;
                }    
                $scope.materialModel.total = data.total;
            }
            $scope.materialModel.isLoadingProgress = false;
        });

    };
    $scope.getcategoryList();

}]);




