





app.controller('accountCtrl', ['$scope', '$rootScope', 'adminService', 'APP_CONST', '$filter', 'Auth', '$state', '$timeout', '$httpService', function ($scope, $rootScope, adminService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.accountModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            account: '',
            description: '',
            balance: '',
            account_no: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.accountModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            id: '',
            start: '',
            account: '',
            bank_name: '',
            account_number: '',
            customerInfo: '',
            userInfo: ''

        };

        $scope.searchFilterValue = "";
        {

        }
        ;

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter.account = '';
            $scope.getList();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.selectAccountId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function (id)
        {
            $scope.selectAccountId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deleteAccountInfo = function ( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectAccountId;
                var headers = {};
                headers['screen-code'] = 'account';
                adminService.deleteAccount(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success === true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'account';
            getListParam.id = '';
            getListParam.account = $scope.searchFilter.account;
            getListParam.bank_name = $scope.searchFilter.bank_name;
            getListParam.account_number = $scope.searchFilter.account_number;
            getListParam.start = ($scope.accountModel.currentPage - 1) * $scope.accountModel.limit;
            getListParam.limit = $scope.accountModel.limit;
            $scope.accountModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.accountModel.list = data;
                    $scope.accountModel.total = data.total;
                }
                $scope.accountModel.isLoadingProgress = false;
            });

        };

        $scope.getList();
    }]);




