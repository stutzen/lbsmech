

app.controller('departmentEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.departmentModel = {
            id: '',
            departmentname: "",
            code: '',
            comments: '',
            isLoadingProgress: false

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.department_form != 'undefined' && typeof $scope.department_form.$pristine != 'undefined' && !$scope.department_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.department_form.$setPristine();
            $scope.updateDepartmentInfo();

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.updateDepartmentInfo = function()
        {
            if ($scope.departmentDetail != null)
            {
                $scope.departmentModel.departmentname = $scope.departmentDetail.name;
                $scope.departmentModel.code = $scope.departmentDetail.code;
                $scope.departmentModel.comments = $scope.departmentDetail.comments;
            }
            $scope.departmentModel.isLoadingProgress = false;
        }
        $scope.modifyDepartment = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'department';
                createParam.name = $scope.departmentModel.departmentname;
                createParam.code = $scope.departmentModel.code;
                createParam.workinhours = $scope.departmentModel.workinhours;
                createParam.comments = $scope.departmentModel.comments;
                adminService.modifyDepartment(createParam, $stateParams.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.department');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            $scope.departmentModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDepartmentList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.departmentDetail = data.list[0];
                    $scope.updateDepartmentInfo();
                }

            });

        };


        $scope.getList();

    }]);




