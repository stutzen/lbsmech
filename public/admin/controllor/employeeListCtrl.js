


app.controller('employeeListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.employeeModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            email: '',
            list: [],
            country: '',
            city: '',
            id: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            teamList : []
        };
        $scope.pagePerCount = [50, 100];
        $scope.employeeModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            fname:'',
            salarytype:'',
            employeetype:'',
            gender:'',
            teamid : '',
            emp_code : ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                 fname:'',
                 salarytype:'',
                 employeetype:'',
                 gender:'',
                 teamid : '',
                 emp_code : ''
            };
            $scope.initTableFilter();
        }
        $scope.selectEmployeeId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteEmployeeLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectEmployeeId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteCategoryLoadingProgress = false;
        };

        $scope.deleteEmployeeInfo = function( )
        {
            if ($scope.isdeleteEmployeeLoadingProgress == false)
            {
                $scope.isdeleteEmployeeLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectEmployeeId;
                var headers = {};
                headers['screen-code'] = 'employee';
                adminService.deleteEmployee(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteEmployeeLoadingProgress = false;
                });
            }
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'employee';
            getListParam.emp_code = $scope.searchFilter.emp_code;
            getListParam.fname = $scope.searchFilter.fname;
            getListParam.team_id = $scope.searchFilter.teamid;
            getListParam.salary_type = $scope.searchFilter.salarytype;
            getListParam.type_of_emp = $scope.searchFilter.employeetype;
            getListParam.gender = $scope.searchFilter.gender;
            getListParam.is_active = 1;
            getListParam.start = ($scope.employeeModel.currentPage - 1) * $scope.employeeModel.limit;
            getListParam.limit = $scope.employeeModel.limit;
            $scope.employeeModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeList(getListParam,configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeModel.list = data.list;
                    $scope.employeeModel.total = data.total;
                }
                $scope.employeeModel.isLoadingProgress = false;
            });

        };
        $scope.getList();
        $scope.getEmployeeTeam = function ()
       {            
           var getListParam = {};  
           var configOption = adminService.handleOnlyErrorResponseConfig;
           adminService.getEmployeeTeamList(getListParam, configOption).then(function (response) {
               if (response.data.success === true)
               {
                   var data = response.data.list;
                   $scope.employeeModel.teamList = data;                    
               }                
           });
       };
       $scope.getEmployeeTeam();
    }]);










