





app.controller('deviceCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.deviceModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            deviceCode: '',
            shortCode: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.deviceModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: '',
            devicecode: '',
            shortcode: ''

        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = '';
            getListParam.deviceCode = $scope.searchFilter.devicecode;
            getListParam.shortCode = $scope.searchFilter.shortcode;
            getListParam.name = $scope.searchFilter.name;
            getListParam.start = ($scope.deviceModel.currentPage - 1) * $scope.deviceModel.limit;
            getListParam.limit = $scope.deviceModel.limit;
            $scope.deviceModel.isLoadingProgress = true;
            adminService.getdeviceList(getListParam).then(function(response) {
                var data = response.data.data;
                $scope.deviceModel.list = data;
                $scope.deviceModel.total = data.total;
                $scope.deviceModel.isLoadingProgress = false;
            });

        };

        $scope.getList();
    }]);




