app.controller('areawisesalesCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.salesModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            stateList: [],
            cityList: [],
            countryList: [],
            country_name: '',
            state_name: '',
            city_name: '',
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.showDetail = false;
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.salesModel.limit = $scope.pagePerCount[0];

        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;

        $scope.searchFilter = {
            country_id: '',
            state_id: '',
            city_id: '',
            status: ''
        };

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                country_id: '',
                state_id: '',
                city_id: '',
                status: ''
            };
            $scope.initTableFilter();
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };

        $scope.getcountryList = function () {

            //$scope.salesModel.isLoadingProgress = true;
            $scope.salesModel.stateList = [];
            $scope.salesModel.cityList = [];
            var countryListParam = {};
            countryListParam.id = '';
            countryListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.salesModel.countryList = data.list;
                }
                //$scope.salesModel.isLoadingProgress = false;
            });

        };

        $scope.getstateList = function () {
            if ($scope.searchFilter.country_id != '' && $scope.searchFilter.country_id != 'undefined' && $scope.searchFilter.country_id != null)
            {
                //$scope.salesModel.isLoadingProgress = true;
                var stateListParam = {};
                stateListParam.id = '';
                stateListParam.country_id = $scope.searchFilter.country_id;
                stateListParam.is_active = 1;
                $scope.salesModel.stateList = [];
                $scope.salesModel.cityList = [];
                $scope.searchFilter.state_id = '';
                $scope.searchFilter.city_id = '';

                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getStateList(stateListParam, configOption).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data;
                        $scope.salesModel.stateList = data.list;                        
                    }
                    //$scope.salesModel.isLoadingProgress = false;
                });
            } else
            {
                $scope.salesModel.stateList = [];
                $scope.salesModel.cityList = [];
                $scope.searchFilter.state_id = '';
                $scope.searchFilter.city_id = '';
                $scope.salesModel.country_name = '';
                $scope.salesModel.state_name = '';
                $scope.salesModel.city_name = '';
            }
        };

        $scope.getcityList = function () {
            if ($scope.searchFilter.state_id != '' && $scope.searchFilter.state_id != 'undefined' && $scope.searchFilter.state_id != null
                    && $scope.searchFilter.country_id != '' && $scope.searchFilter.country_id != 'undefined' && $scope.searchFilter.country_id != null)
            {
                //$scope.salesModel.isLoadingProgress = true;
                var cityListParam = {};

                cityListParam.id = '';
                cityListParam.country_id = $scope.searchFilter.country_id;
                cityListParam.state_id = $scope.searchFilter.state_id;
                cityListParam.is_active = 1;
                $scope.salesModel.cityList = [];
                $scope.searchFilter.city_id = '';
                
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getCityList(cityListParam, configOption).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data;
                        $scope.salesModel.cityList = data.list;
                    }
                    //$scope.salesModel.isLoadingProgress = false;
                });
            } else
            {
                $scope.salesModel.cityList = [];
                $scope.searchFilter.city_id = '';
                $scope.salesModel.city_name = '';
                $scope.salesModel.state_name = '';
            }
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'areawisesalesreport';
            getListParam.name = '';
            getListParam.country_id = $scope.searchFilter.country_id;
            getListParam.state_id = $scope.searchFilter.state_id;
            getListParam.city_id = $scope.searchFilter.city_id;
            getListParam.payment_status = $scope.searchFilter.status;
            getListParam.start = 0;
            getListParam.limit = 0;
            getListParam.show_all = ($scope.showDetail == true) ? 1 : 0;
            $scope.salesModel.isLoadingProgress = true;
            $scope.salesModel.isSearchLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAreaWiseSalesList(getListParam, configOption, headers).then(function (response)
            {
                var totalAmt = 0.00;
                var totalQty = 0;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.salesModel.list = data;
                    for (var i = 0; i < $scope.salesModel.list.length; i++)
                    {
                        $scope.salesModel.list[i].newdate = utilityService.parseStrToDate($scope.salesModel.list[i].date);
                        $scope.salesModel.list[i].date = utilityService.parseDateToStr($scope.salesModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        totalAmt = totalAmt + parseFloat($scope.salesModel.list[i].total_amount);
                        totalQty = totalQty + parseFloat($scope.salesModel.list[i].total_qty);
                        $scope.salesModel.list[i].total_amount = utilityService.changeCurrency($scope.salesModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.salesModel.list[i].paid_amount = utilityService.changeCurrency($scope.salesModel.list[i].paid_amount, $rootScope.appConfig.thousand_seperator);
                    }
                    $scope.salesModel.total = data.total;
                    $scope.salesModel.total_Amount = totalAmt;
                    $scope.salesModel.total_qty = totalQty;
                    $scope.salesModel.total_Amount = parseFloat($scope.salesModel.total_Amount).toFixed(2);
                    $scope.salesModel.total_Amount = utilityService.changeCurrency($scope.salesModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    if ($scope.showDetail)
                    {
                        for (var i = 0; i < $scope.salesModel.list.length; i++)
                        {
                            if ($scope.salesModel.list[i].item != 'undefined' && $scope.salesModel.list[i].item.length > 0)
                            {
                                for (var j = 0; j < $scope.salesModel.list[i].item.length; j++)
                                {
                                    var rowtotal = (parseFloat($scope.salesModel.list[i].item[j].qty, 10)) * (parseInt($scope.salesModel.list[i].item[j].unit_price, 10));
                                    $scope.salesModel.list[i].item[j].rowtotal = rowtotal;
                                }
                            }
                        }
                    }
                }
                $scope.salesModel.isLoadingProgress = false;
                $scope.salesModel.isSearchLoadingProgress = false;
            });

        };

        $scope.print = function ()
        {
            for (var i = 0; i < $scope.salesModel.countryList.length; i++)
            {
                if ($scope.salesModel.countryList[i].id == $scope.searchFilter.country_id)
                {
                    $scope.salesModel.country_name = $scope.salesModel.countryList[i].name;
                }
            }
            for (var i = 0; i < $scope.salesModel.stateList.length; i++)
            {
                if ($scope.salesModel.stateList[i].id == $scope.searchFilter.state_id)
                {
                    $scope.salesModel.state_name = $scope.salesModel.stateList[i].name;
                }
            }
            for (var i = 0; i < $scope.salesModel.cityList.length; i++)
            {
                if ($scope.salesModel.cityList[i].id == $scope.searchFilter.city_id)
                {
                    $scope.salesModel.city_name = $scope.salesModel.cityList[i].name;
                }
            }
            $window.print();
        };

        $scope.getList();
        $scope.getcountryList();
    }]);




