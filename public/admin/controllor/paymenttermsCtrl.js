


app.controller('paymenttermsCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.paymenttermsModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            is_active: 1,
            status: '',
            total:'',
                    isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.paymenttermsModel.limit = $scope.pagePerCount[0];
        $scope.searchFilter = {
            name: '',
            id:''
        };
        $scope.searchFilterValue = "";
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                id:'',
                name: ''

            };
            $scope.initTableFilter();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectpaymenttermsId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectpaymenttermsId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };

        $scope.deletepaymenttermsInfo = function( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectpaymenttermsId;
                var headers = {};
                headers['screen-code'] = 'paymentterms';
                adminService.deletepaymentterms(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };

        $scope.getList = function()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'paymentterms';
            getListParam.id = $scope.searchFilter.id;
            getListParam.name = $scope.searchFilter.name;
            getListParam.is_active = 1;
//        getListParam.rolename = $scope.searchFilter.rolename;        
            getListParam.start = ($scope.paymenttermsModel.currentPage - 1) * $scope.paymenttermsModel.limit;
            getListParam.limit = $scope.paymenttermsModel.limit;
            $scope.paymenttermsModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getpaymenttermslist(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.paymenttermsModel.list = data.list;
                    for (var i = 0; i < $scope.paymenttermsModel.list.length; i++)
                    {
                      $scope.paymenttermsModel.list[i].is_default  = $scope.paymenttermsModel.list[i].is_default == 1 ? "YES":"NO";

                    }
                    $scope.paymenttermsModel.total = data.total;
                }
                $scope.paymenttermsModel.isLoadingProgress = false;
            });
        };
        $scope.getList();
    }]);








