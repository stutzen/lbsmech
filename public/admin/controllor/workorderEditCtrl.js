
app.controller('workorderEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', '$window', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet, $window) {

        $scope.workModel =
                {
                    "date": "",
                    "next_date": "",
                    "taskList": [],
                    "detail": [],
                    "teamId": '',
                    "userId": "",
                    "userList": [],
                    "inputProducts": [],
                    "outputProducts": [],
                    "qtyToProduce": '',
                    "qc_passed_qty": '',
                    "qc_failed_qty": '',
                    "total_qty": '',
                    "bomTaskInfo": {},
                    "produced_qty": '',
                    "task_group_id": '',
                    "status": '',
                    "work_type": '',
                    "is_parent": '',
                    "reference_id": '',
                    "output_pdt_id": '',
                    "comments": '',
                    "inputList": [],
                    "workOrderList": [],
                    "customerInfo": '',
                    "sessionList": [],
                    "output_pdt_name": '',
                    "wo_status": '',
                    "type": '',
                    "qc_final_output_id": ''
                }


        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.adminService = adminService;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.work_edit_form !== 'undefined' && typeof $scope.work_edit_form.$pristine !== 'undefined' && !$scope.work_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            $scope.work_edit_form.$setPristine();

        };
        $scope.validateTotalQty = function ()
        {
            var retVal = false;
            if (parseInt($scope.workModel.total_qty) == parseInt($scope.workModel.qc_passed_qty) + parseInt($scope.workModel.qc_failed_qty))
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.startDateOpen = false;
        $scope.woStartDateOpen = false;
        $scope.woEndDateOpen = false;
        $scope.isLoadedUserList = false;
        $scope.stateParamsData = $stateParams.id;
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.popup1 = {
            opened: false
        };
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
            if (index == 2)
            {
                $scope.woStartDateOpen = true;
            }
            if (index == 3)
            {
                $scope.woEndDateOpen = true;
            }
        };

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedUserList)
            {
                $scope.updateWorkInfo();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        };

        $scope.showCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = true;
        };

        $scope.closeCustomerPopup = function ()
        {
            $scope.showCustomerAddPopup = false;
        };
        $scope.$on("updateSavedCustomerDetail", function (event, customerDetail)
        {
            if (typeof customerDetail === 'undefined')
                return;
            $scope.workModel.customerInfo = customerDetail;
            $scope.showCustomerAddPopup = false;
        });

        $scope.getTaskList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCTION_TASK_DETAIL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatTaskModel = function (model, index)
        {
            if (model !== null)
            {
                $scope.updateTaskGroup(model);
                return model.name;
            }
            return  '';
        };

        $scope.updateTaskGroup = function (model)
        {
            $scope.workModel.task_group_name = model.task_group_name;
            $scope.workModel.task_group_id = model.task_group_id;
            //$scope.workModel.qtyToProduce = 1;
            $scope.workModel.output_pdt_id = model.output_pdt_id;

        };


        $scope.updateWorkInfo = function ()
        {
            if ($scope.workModel.list !== null)
            {
                $scope.workModel.date = new Date($scope.workModel.list.deadline_date);
                $scope.workModel.customerInfo = {
                    id: $scope.workModel.list.customer_id,
                    fname: $scope.workModel.list.customer_fname
                };
                $scope.workModel.qtyToProduce = $scope.workModel.list.qty;
                $scope.workModel.bomTaskInfo = {
                    id: $scope.workModel.list.bom_id,
                    output_pdt_id: $scope.workModel.list.output_pdt_id,
                    task_group_id: $scope.workModel.list.task_group_id,
                    task_group_name: $scope.workModel.list.taskGroupName,
                    name: $scope.workModel.list.bomName
                };
                $scope.workModel.userId = $scope.workModel.list.user_id + '';
                $scope.workModel.output_pdt_name = $scope.workModel.list.output_pdt_name;
                $scope.workModel.output_pdt_id = $scope.workModel.list.output_pdt_id;
                $scope.workModel.produced_qty = $scope.workModel.list.produced_qty;
                $scope.workModel.task_group_id = $scope.workModel.list.task_group_id;
                $scope.workModel.task_group_name = $scope.workModel.list.taskGroupName;
                $scope.workModel.status = $scope.workModel.list.status;
                $scope.workModel.qc_status = $scope.workModel.list.qc_status;
                $scope.workModel.wo_status = $scope.workModel.list.wo_status;
                if ($scope.workModel.status == 'initiated')
                {
                    $scope.qtyEdit = true;
                } else
                {
                    $scope.qtyEdit = false;
                }
                //$scope.workModel.work_type = $scope.workModel.list.work_type;
                $scope.workModel.is_parent = $scope.workModel.list.is_parent;
                $scope.workModel.reference_id = $scope.workModel.list.reference_id;

                $scope.workModel.output_pdt_id = $scope.workModel.list.output_pdt_id;
                $scope.workModel.isCompleted = $scope.workModel.list.isCompleted == 0 ? false : true;
                $scope.workModel.comments = $scope.workModel.list.comments;
            }
        };

        $scope.$watch('workModel.customerInfo', function (newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal === '' || newVal === null)
            {
                $scope.workModel.customerInfo = '';
            } else if (typeof $stateParams.id === 'undefined' || $stateParams.id === null || $stateParams.id === '' || $stateParams.id == 0)
            {
                var addrss = "";

                if (newVal.billing_address != undefined && newVal.billing_address != '')
                {
                    addrss += newVal.billing_address;
                }
                if (newVal.billing_city != undefined && newVal.billing_city != '')
                {
                    addrss += '\n' + newVal.billing_city;
                }
                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
                {
                    if (newVal.billing_city != undefined && newVal.billing_city != '')
                    {
                        addrss += '-' + newVal.billing_pincode;
                    } else
                    {
                        addrss += newVal.billing_pincode;
                    }
                }

                if (newVal.billing_state != undefined && newVal.billing_state != '')
                {
                    addrss += '\n' + newVal.billing_state;
                }

                $scope.workModel.customerInfo.shopping_address = addrss;
            }
        });

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.getCustomerInfo = function () {
            $scope.workModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.id = $stateParams.cust_id;
            getListParam.name = '';
            getListParam.mode = 1;
            getListParam.sort = 1;
            getListParam.is_active = 1;
            adminService.getCustomerDetail(getListParam).then(function (response) {
                var data = response.data;
                if (data.total != 0)
                {
                    var customerInfo = data.data;
                    $scope.workModel.customerInfo = {};
                    $scope.workModel.customerInfo.id = customerInfo.id;
                    $scope.workModel.customerInfo.fname = customerInfo.fname;
                    $scope.workModel.customerInfo.billing_address = customerInfo.billing_address;
                    $scope.workModel.customerInfo.phone = customerInfo.phone;
                    $scope.workModel.customerInfo.email = customerInfo.email;
                    $scope.workModel.isLoadingProgress = false;
                }
            });
        };
        $scope.getWorkInfo = function () {

            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                $scope.isLoadedList = false;
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getWorkOrderList(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.workModel.list = data.list[0];
                        $scope.initUpdateDetail();
                    }

                });
            }
        };

        $scope.modifyWork = function () {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var modifyProductionParam = {};
                var headers = {};
                headers['screen-code'] = 'workorder';
                if (typeof $scope.workModel.date == 'object')
                {
                    $scope.workModel.date = utilityService.parseDateToStr($scope.workModel.date, 'yyyy-MM-dd');
                }
                modifyProductionParam.deadline_date = utilityService.changeDateToSqlFormat($scope.workModel.date, 'yyyy-MM-dd');
                modifyProductionParam.date = utilityService.changeDateToSqlFormat($scope.workModel.date, 'yyyy-MM-dd');
                modifyProductionParam.qty = $scope.workModel.qtyToProduce;
                modifyProductionParam.bom_id = $scope.workModel.bomTaskInfo.id;
                modifyProductionParam.user_id = $scope.workModel.userId;
                modifyProductionParam.produced_qty = $scope.workModel.produced_qty;
                modifyProductionParam.task_group_id = $scope.workModel.task_group_id;
                modifyProductionParam.status = $scope.workModel.status;
                modifyProductionParam.work_type = $scope.workModel.work_type;
                modifyProductionParam.is_parent = '';
                modifyProductionParam.reference_id = '';
                modifyProductionParam.output_pdt_id = $scope.workModel.output_pdt_id;
                modifyProductionParam.comments = $scope.workModel.comments;
                modifyProductionParam.customer_id = '';
                if ($scope.workModel.customerInfo != '' && typeof $scope.workModel.customerInfo != 'undefined' && $scope.workModel.customerInfo != null && $scope.workModel.customerInfo.id != 'undefined')
                {
                    modifyProductionParam.customer_id = $scope.workModel.customerInfo.id;
                }
                adminService.editWorkOrder(modifyProductionParam, $stateParams.id, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.formReset();
                        $state.go('app.workorderEdit', {
                            'id': $stateParams.id
                        }, {
                            'reload': true
                        });
                    }
                });
            }
        };
        $scope.currentStep = 1;
//        $scope.sessionStep = 1;
        $scope.tabChange = function (value) {
            $scope.currentStep = value;
        }


        $scope.showWorkorder = false;

        $scope.selectedOrderDetail = '';
        $scope.showWorkorderPopup = function (index)
        {
            $scope.selectedOrderDetail = $scope.workModel.workOrderList[index];
            $scope.selectedOrderDetail.startDate = new Date();
            $scope.selectedOrderDetail.index = index;
            $scope.selectedOrderDetail.endDate = '';
            $scope.selectedOrderDetail.startTime = '';
            $scope.selectedOrderDetail.endTime = '';
            $scope.workModel.type = $scope.selectedOrderDetail.type;
            $scope.selectedOrderDetail.currentProducedQty = 1;
            $scope.selectedOrderDetail.customerInfo = {
                id: $scope.workModel.workOrderList[index].customer_id,
                fname: $scope.workModel.workOrderList[index].customerName
            };
            $scope.woStartDateOpen = false;
            $scope.woEndDateOpen = false;
            $scope.selectedOrderDetail.balanceToProduce = parseFloat($scope.selectedOrderDetail.original_production_qty) - parseFloat($scope.selectedOrderDetail.produced_qty);
            $scope.showWorkorder = true;
            if ($scope.selectedOrderDetail.status.toLowerCase() == 'completed')
            {
                $scope.sessionStep = 2;
                $scope.sessiontabChange(2);
                console.log($scope.sessionStep);
            } else
            {
                $scope.sessionStep = 1;
                $scope.sessiontabChange(1);
            }
            $scope.getSessionList();
        };

        $scope.sessiontabChange = function (value)
        {
            $scope.sessionStep = value;
        };

        $scope.closeWorkorderPopup = function ()
        {
            $scope.currentStep = 2;
            $scope.sessionStep = '';
            $scope.selectedOrderDetail = '';
            $scope.showWorkorder = false;
        };

        $scope.showWorkOrderCompleteButton = false;
        $scope.checkQuantity = function ()
        {
            $scope.showWorkOrderCompleteButton = false;
            if (parseFloat($scope.selectedOrderDetail.producedQty) === parseFloat($scope.selectedOrderDetail.balanceToProduce))
            {
                $scope.showWorkOrderCompleteButton = true;
            }
        };

        $scope.getUserList = function () {

            var getListParam = {};
            getListParam.is_active = 1;
            $scope.isLoadedUserList = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function (response) {
                var data = response.data;
                $scope.workModel.userList = data.list;
                $scope.workModel.userId = $rootScope.userModel.id + '';
                $scope.isLoadedUserList = true;
            });
        };

        $scope.isPlanDataSavingProcess = false;
        $scope.planOrder = function () {

            var planOrderParam = {};
            planOrderParam.id = $stateParams.id;
            $scope.isPlanDataSavingProcess = true;
            adminService.planWorkOrder(planOrderParam, $stateParams.id).then(function (response) {
                if (response.data.success === true)
                {
                    $scope.isPlanDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.workorderEdit', {
                        'id': $stateParams.id
                    }, {
                        'reload': true
                    });
                }
            });
        };
//        $scope.customerStatusUpdate = function()
//        {
//            var statusParam = {};
////        statusParam.status = 'inprogress';
//            statusParam.customer_id = $scope.selectedOrderDetail.customerInfo.id;
//            statusParam.status = $scope.selectedOrderDetail.status;
//            $scope.isPlanDataSavingProcess = true;
//            adminService.modifyWorkOrderStatus(statusParam, $scope.selectedOrderDetail.id).then(function(response) {
//                if (response.data.success === true)
//                {
//                    $scope.isPlanDataSavingProcess = false;
////                    $scope.formReset();
////                    $state.go('app.workorderEdit', {
////                        'id': $stateParams.id
////                    }, {
////                        'reload': true
////                    });
////                    $scope.closeWorkorderPopup();
//
//                    $scope.getWorkDetailList();
//                }
//            });
//        }

        $scope.customerWorkorderUpdate = function ()
        {
            var workorderParam = {};
            workorderParam.mo_id = $scope.selectedOrderDetail.mo_id;
            workorderParam.work_order_input_id = $scope.selectedOrderDetail.work_order_input_id;
            workorderParam.status = $scope.selectedOrderDetail.status;
            workorderParam.task_group_detail_id = $scope.selectedOrderDetail.task_group_detail_id;
            workorderParam.work_center_id = $scope.selectedOrderDetail.work_center_id;
            workorderParam.start_date = $scope.selectedOrderDetail.start_date;
            workorderParam.original_production_qty = $scope.selectedOrderDetail.original_production_qty;
            workorderParam.produced_qty = $scope.selectedOrderDetail.produced_qty;
            if ($scope.workModel.type == 'external')
            {
                workorderParam.customer_id = $scope.selectedOrderDetail.customerInfo.id;
            } else {
                workorderParam.customer_id = '';
            }
            workorderParam.type = $scope.workModel.type;
            workorderParam.amount = $scope.selectedOrderDetail.amount;
            workorderParam.sequence_no = $scope.selectedOrderDetail.sequence_no;
            workorderParam.is_active = $scope.selectedOrderDetail.is_active;
            workorderParam.comments = $scope.selectedOrderDetail.comments;
            adminService.modifyWorkOrderUpdate(workorderParam, $scope.selectedOrderDetail.id).then(function (response) {
                if (response.data.success === true)
                {
                    $scope.isPlanDataSavingProcess = false;
                    $scope.formReset();
//                    $state.go('app.workorderEdit', {
//                        'id': $stateParams.id
//                    }, {
//                        'reload': true
//                    });
                    //$scope.closeWorkorderPopup();

                    $scope.getWorkDetailList();
                }
            });

        };

        $scope.produceOrder = function () {

            var planOrderParam = {};
            planOrderParam.id = $stateParams.id;
            $scope.isPlanDataSavingProcess = true;
            adminService.produceWorkOrder(planOrderParam).then(function (response) {
                if (response.data.success === true)
                {
                    $scope.isPlanDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.workorderEdit', {
                        'id': $stateParams.id
                    }, {
                        'reload': true
                    });
                }
            });
        };

        $scope.isProductionDataSavingProcess = false;
        $scope.showQualityControlPopup = false;
        $scope.qualityCheck = function (index) {

            $scope.showQualityControlPopup = true;
            $scope.workModel.total_qty = $scope.workModel.workOrderList[index].original_production_qty;
            $scope.workModel.workorder_qc_id = $scope.workModel.workOrderList[index].id;
            if (index == $scope.workModel.workOrderList.length - 1)
            {
                $scope.workModel.qc_final_output_id = $scope.workModel.list.output_pdt_id;
            } else
            {
                $scope.workModel.qc_final_output_id = 0;
            }
        }
        $scope.checkQuality = function () {
            var qualityCheckParam = {};
            qualityCheckParam.ref_id = $scope.workModel.workorder_qc_id;
            qualityCheckParam.ref_type = "work_order";
            qualityCheckParam.product_id = $scope.workModel.qc_final_output_id;
            qualityCheckParam.passed_qty = $scope.workModel.qc_passed_qty;
            qualityCheckParam.failed_qty = $scope.workModel.qc_failed_qty;
            qualityCheckParam.tested_qty = $scope.workModel.total_qty;
            $scope.isDataSavingProcess = true;
            var headers = {};
            headers['screen-code'] = 'workorder';
            adminService.qualityCheck(qualityCheckParam, headers).then(function (response) {
                if (response.data.success === true)
                {
                    $scope.isDataSavingProcess = false;
                    $scope.showQualityControlPopup = false;
                    $scope.workModel.qc_passed_qty = '';
                    $scope.workModel.qc_failed_qty = '';
                    $scope.getWorkDetailList();
                    $scope.getWorkInfo();
                }
            });

        }
        $scope.mocompletecheck = function ()
        {

        }
        $scope.closeQualityCheckPopup = function ()
        {
            $scope.showQualityControlPopup = false;
        };
        $scope.markAsDone = function () {

            var planOrderParam = {};
            planOrderParam.id = $stateParams.id;
            $scope.isPlanDataSavingProcess = true;
            var headers = {};
            headers['screen-code'] = 'workorder';
            adminService.markAsDoneWorkOrder(planOrderParam, $stateParams.id, headers).then(function (response) {
                if (response.data.success === true)
                {
                    $scope.isDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.workorderEdit', {
                        'id': $stateParams.id
                    }, {
                        'reload': true
                    });
                }
            });
        };

        $scope.createWorkOrderProduction = function () {
            var starttime = moment($scope.selectedOrderDetail.startTime).format("h:mm:ss a");
            var sdate = moment($scope.selectedOrderDetail.startDate).format();
            var startdate = utilityService.parseStrToDate(sdate, 'yyyy-MM-dd');
            startdate = utilityService.changeDateToSqlFormat(startdate, 'yyyy-MM-dd');
            var endtime = moment($scope.selectedOrderDetail.endTime).format("h:mm:ss a");
            var edate = moment($scope.selectedOrderDetail.endDate).format();
            var enddate = utilityService.parseStrToDate(edate, 'yyyy-MM-dd');
            enddate = utilityService.changeDateToSqlFormat(enddate, 'yyyy-MM-dd');
            $scope.startdate = moment(startdate + " " + starttime);
            $scope.enddate = moment(enddate + " " + endtime);
            var newstartDate = moment($scope.startdate._d).valueOf();
            var newendDate = moment($scope.enddate._d).valueOf();
            //console.log($scope.startdate.format());
            var duration = moment.duration($scope.enddate.diff($scope.startdate));
            $scope.duration = duration._data.hours + ":" + duration._data.minutes + ":" + duration._data.seconds;
            if (newendDate <= newstartDate)
            {
                if (newendDate == newstartDate)
                {
                    sweet.show("Start Time and End Time Can't be Same");
                } else
                {
                    sweet.show("End Time Can't be Less than Start Time");
                }

            } else
            {
                var workOrderProductionParam = {};
                //      workOrderProductionParam.workOrderId = $stateParams.id;
                workOrderProductionParam.work_order_id = $scope.selectedOrderDetail.id;
                workOrderProductionParam.start_date = '';
                if (typeof $scope.selectedOrderDetail.startDate == 'object')
                {
                    $scope.selectedOrderDetail.startDate = utilityService.parseDateToStr($scope.selectedOrderDetail.startDate, 'yyyy-MM-dd');

                }
                $scope.selectedOrderDetail.start_date = utilityService.changeDateToSqlFormat($scope.selectedOrderDetail.startDate, 'yyyy-MM-dd');
                workOrderProductionParam.start_date = $scope.selectedOrderDetail.start_date + ' ' + ($filter('date')($scope.selectedOrderDetail.startTime, 'HH:mm:ss'));
                workOrderProductionParam.end_date = '';
                if (typeof $scope.selectedOrderDetail.endDate == 'object')
                {
                    $scope.selectedOrderDetail.endDate = utilityService.parseDateToStr($scope.selectedOrderDetail.endDate, 'yyyy-MM-dd');
                }
                $scope.selectedOrderDetail.end_date = utilityService.changeDateToSqlFormat($scope.selectedOrderDetail.endDate, 'yyyy-MM-dd');
                workOrderProductionParam.end_date = $scope.selectedOrderDetail.end_date + ' ' + ($filter('date')($scope.selectedOrderDetail.endTime, 'HH:mm:ss'));
                workOrderProductionParam.qty = $scope.selectedOrderDetail.currentProducedQty;
                workOrderProductionParam.customer_id = $scope.selectedOrderDetail.customerInfo.customer_id;

                if ($scope.selectedOrderDetail.type == 'external' && ($scope.selectedOrderDetail.customerInfo == null || $scope.selectedOrderDetail.customerInfo.customer_id == 'undefined' || $scope.selectedOrderDetail.customerInfo.customer_id == ''))
                {
                    sweet.show('Oops...', 'Please update customer', 'error');
                } else
                {
                    $scope.isProductionDataSavingProcess = true;
                    adminService.saveWorkOrderSession(workOrderProductionParam).then(function (response) {
                        if (response.data.success === true)
                        {
                            $scope.isProductionDataSavingProcess = false;
                            $scope.sessionStep = 1;
                            $scope.selectedOrderDetail.startDate = new Date();
                            $scope.selectedOrderDetail.endDate = '';
                            $scope.selectedOrderDetail.currentProducedQty = 1;
                            $scope.selectedOrderDetail.producedQty = 1;
                            $scope.closeWorkorderPopup();
                            $scope.getWorkDetailList();
                        }
                    });
                }
            }
        };

        $scope.getWorkInputList = function () {

            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                getListParam.moId = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getWorkOrderInputList(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.workModel.inputList = data.list;
                    }
                });
            }
        };
        $scope.getSessionList = function () {

            var getListParam = {};
            getListParam.work_order_id = $scope.selectedOrderDetail.id;
            $scope.workModel.sessionList = [];
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getSessionListAll(getListParam, configOption).then(function (response) {
                var data = response.data;
                if (data.total !== 0)
                {
                    $scope.workModel.sessionList = data.list;
                    for (var i = 0; i < $scope.workModel.sessionList.length; i++)
                    {
                        $scope.workModel.sessionList[i].startdate = utilityService.parseStrToDate($scope.workModel.sessionList[i].start_date);
                        $scope.workModel.sessionList[i].start_date = utilityService.parseDateToStr($scope.workModel.sessionList[i].startdate, $rootScope.appConfig.date_format);
                        $scope.workModel.sessionList[i].enddate = utilityService.parseStrToDate($scope.workModel.sessionList[i].end_date);
                        $scope.workModel.sessionList[i].end_date = utilityService.parseDateToStr($scope.workModel.sessionList[i].enddate, $rootScope.appConfig.date_format);
                    }
                }
            });

        };

        $scope.getWorkDetailList = function () {

            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                getListParam.moId = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getWorkOrderDetailList(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.workModel.workOrderList = data.list;
                        console.log("WORKLOG");
                        console.log($scope.workModel.workOrderList);
                    }
                });
            }
        };

        $scope.print = function (id)
        {

//            var to_date = utilityService.changeDateToSqlFormat(todate, $scope.dateFormat);
//            var date = from_date_old + "&" + to_date;
            //$state.go('app.customerReportPrint', {'date': from_date});
            var landingUrl = $window.location.protocol + "//" + $window.location.host + "/public/admin/#/requisitionPrint/" + id;
            $window.open(landingUrl, '_blank');
        };

        $scope.printWindow = function ()
        {
            window.print();
        };

        $scope.customerHide = false;
        $scope.checkCustomerType = function (type)
        {
            if ($scope.selectedOrderDetail.type == 'internal' && type == 'internal')
            {
                $scope.customerHide = true;
            } else if (type == 'external' && $scope.workModel.type == 'external')
            {
                $scope.customerHide = false;
            } else if (type == 'internal')
            {
                $scope.customerHide = true;
            } else {
                $scope.customerHide = false;
            }
        };

        $scope.tabChange(1);
        $scope.getWorkInfo();
        $scope.getWorkInputList();
        $scope.getWorkDetailList();
        $scope.getUserList();
    }]);








