

app.controller('workcenterEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.workcenterModel = {
            id: '',
            centername: "",
            code: '',
            comments: '',
            isLoadingProgress: false

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.workcenter_form != 'undefined' && typeof $scope.workcenter_form.$pristine != 'undefined' && !$scope.workcenter_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.workcenter_form.$setPristine();
            $scope.updateWorkcenterInfo();

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.updateWorkcenterInfo = function()
        {
            if ($scope.workcenterDetail != null)
            {
                $scope.workcenterModel.centername = $scope.workcenterDetail.name;
                $scope.workcenterModel.code = $scope.workcenterDetail.code;
                $scope.workcenterModel.comments = $scope.workcenterDetail.comments;
            }
            $scope.workcenterModel.isLoadingProgress = false;
        }
        $scope.modifyCenter = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'workcenter';
                createParam.name = $scope.workcenterModel.centername;
                createParam.code = $scope.workcenterModel.code;
                createParam.workinhours = $scope.workcenterModel.workinhours;
                createParam.comments = $scope.workcenterModel.comments;
                adminService.modifyWorkCenter(createParam, $stateParams.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.workcenter');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
            $scope.workcenterModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getWorkCenterList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.workcenterDetail = data.list[0];
                    $scope.updateWorkcenterInfo();
                }

            });

        };


        $scope.getList();

    }]);




