app.controller('employeeAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', function ($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state) {
        $scope.employeeAddModel = {
            "fname": "",
            "lname": "",
            "gender": "",
            "dob": '',
            "doj": '',
            "salarytype": '',
            "salary": '',
            "teamid": '',
            "email": "",
            "mobile": "",
            "city_id": "",
            "state_id": "",
            "country_id": "",
            "employeetype": "",
            "address": "",
            "country_list": [],
            "state_list": [],
            "city_list": [],
            "teamList": [],
            "empcode": '',
            "isUser": false,
            "maritalstatus": false,
            "employee": 'employee',
            "employeer": false,
            "isactive": true,
            "pfpercentage": '',
            "pfamount": '',
            "employeerpf": '',
            "employeeramount": '',
            "employeeresi": '',
            "employeeresiamount": '',
            "esipercentage": '',
            "esiamount": '',
            "relationship": '',
            "nomineename": '',
            "leavecount": '',
            "releving": '',
            "reason": '',
            "payrolltype": '',
            currentPage: 1,
            total: 0,
            bonus_percentage: ''
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_employee_form != 'undefined' && typeof $scope.create_employee_form.$pristine != 'undefined' && !$scope.create_employee_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.personalinformation = false;
        $scope.personal = function ()
        {
            $scope.personalinformation = true;
        }
        $scope.leave = false;
        $scope.attendance = function ()
        {
            if ($scope.employeeAddModel.salarytype == 'attendance')
            {
                $scope.leave = true;
            } else
            {
                $scope.leave = false;
            }
        }
        $scope.maritalstatus = false;
        $scope.checked = function ()
        {
            if ($scope.employeeAddModel.employee == true)
            {
                $scope.employeeAddModel.employeer = false;
            }
            if ($scope.employeeAddModel.employeer == true)
            {
                $scope.employeeAddModel.employee = false;
            }
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.currentDate = new Date();
        $scope.dateOptions = {
            maxDate: new Date(),
            showWeeks: false
        };

        $scope.formReset = function () {

            $scope.create_employee_form.$setPristine();
            $scope.employeeAddModel.fname = '';
            $scope.employeeAddModel.lname = '';
            $scope.employeeAddModel.dob = '';
            $scope.employeeAddModel.doj = '';
            $scope.employeeAddModel.gender = '';
            $scope.employeeAddModel.mobile = '';
            $scope.employeeAddModel.email = '';
            $scope.employeeAddModel.address = '';
            $scope.employeeAddModel.city_id = '';
            $scope.employeeAddModel.state_id = '';
            $scope.employeeAddModel.country_id = '';
            $scope.employeeAddModel.teamid = '';
            $scope.employeeAddModel.address = '';
            $scope.employeeAddModel.salary = '';
            $scope.employeeAddModel.employeetype = '';
            $scope.employeeAddModel.salarytype = '';
            $scope.employeeAddModel.isUser = false;
            $scope.employeeAddModel.empcode = '';
            $scope.employeeAddModel.maritalstatus = false;
            $scope.employeeAddModel.employee = 'employee';
            $scope.employeeAddModel.employeer = false;
            $scope.employeeAddModel.isactive = '';
            $scope.employeeAddModel.employeerpf = '';
            $scope.employeeAddModel.employeeresi = '';
            $scope.employeeAddModel.pfpercentage = '';
            $scope.employeeAddModel.esipercentage = '';
            $scope.employeeAddModel.relationship = '';
            $scope.employeeAddModel.releving = '';
            $scope.employeeAddModel.leavecount = '';
            $scope.employeeAddModel.reason = '';
            $scope.employeeAddModel.bonus_percentage = '';
            $scope.employeeAddModel.payroll = '';
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.currentDate = new Date();
        $scope.startDateOpen = false;
        $scope.DOJDateOpen = false;
        $scope.RelevingDateOpen = false;
        $scope.openDate = function (index) {

            if (index == 1)
            {
                $scope.startDateOpen = true;
            }
            if (index == 2)
            {
                $scope.DOJDateOpen = true;
            }
            if (index == 3)
            {
                $scope.RelevingDateOpen = true;
            }

        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createEmployee, 300);
        }

        $scope.createEmployee = function () {

            $scope.isDataSavingProcess = true;
            var createEmployeeParam = {};
            var headers = {};
            headers['screen-code'] = 'employee';
            createEmployeeParam.team_id = $scope.employeeAddModel.teamid;
            for (var i = 0; i < $scope.employeeAddModel.teamList.length; i++)
            {
                if ($scope.employeeAddModel.teamid == $scope.employeeAddModel.teamList[i].id)
                {
                    createEmployeeParam.team_name = $scope.employeeAddModel.teamList[i].name;
                }
            }
            createEmployeeParam.f_name = $scope.employeeAddModel.fname;
            createEmployeeParam.l_name = $scope.employeeAddModel.lname;
            createEmployeeParam.ph_no = $scope.employeeAddModel.mobile;
            createEmployeeParam.email = $scope.employeeAddModel.email;
            createEmployeeParam.salary_type = $scope.employeeAddModel.salarytype;
            createEmployeeParam.salary_amount = $scope.employeeAddModel.salary;
            createEmployeeParam.type_of_emp = $scope.employeeAddModel.employeetype;
            createEmployeeParam.address = $scope.employeeAddModel.address;
            createEmployeeParam.country_id = $scope.employeeAddModel.country_id;
            createEmployeeParam.state_id = $scope.employeeAddModel.state_id;
            createEmployeeParam.city_id = $scope.employeeAddModel.city_id;
            createEmployeeParam.bonus_percentage = $scope.employeeAddModel.bonus_percentage;
            for (var i = 0; i < $scope.employeeAddModel.country_list.length; i++)
            {
                if ($scope.employeeAddModel.country_id == $scope.employeeAddModel.country_list[i].id)
                {
                    createEmployeeParam.country = $scope.employeeAddModel.country_list[i].name;
                }
            }
            for (var i = 0; i < $scope.employeeAddModel.state_list.length; i++)
            {
                if ($scope.employeeAddModel.state_id == $scope.employeeAddModel.state_list[i].id)
                {
                    createEmployeeParam.state = $scope.employeeAddModel.state_list[i].name;
                }
            }
            for (var i = 0; i < $scope.employeeAddModel.city_list.length; i++)
            {
                if ($scope.employeeAddModel.city_id == $scope.employeeAddModel.city_list[i].id)
                {
                    createEmployeeParam.city = $scope.employeeAddModel.city_list[i].name;
                }
            }
            if ($scope.employeeAddModel.dob != null && $scope.employeeAddModel.dob != '')
            {
                createEmployeeParam.dob = utilityService.parseDateToStr($scope.employeeAddModel.dob, 'yyyy-MM-dd');
            } else
            {
                createEmployeeParam.dob = '';
            }
            createEmployeeParam.gender = $scope.employeeAddModel.gender;
            if ($scope.employeeAddModel.doj != null && $scope.employeeAddModel.doj != '')
            {
                createEmployeeParam.doj = utilityService.parseDateToStr($scope.employeeAddModel.doj, 'yyyy-MM-dd');
            } else
            {
                createEmployeeParam.doj = '';
            }
            if ($scope.employeeAddModel.releving != null && $scope.employeeAddModel.releving != '')
            {
                createEmployeeParam.reliving_date = utilityService.parseDateToStr($scope.employeeAddModel.releving, 'yyyy-MM-dd');
            } else
            {
                createEmployeeParam.reliving_date = '';
            }
            createEmployeeParam.reliving_reason = $scope.employeeAddModel.reason;
            createEmployeeParam.max_leave_count = $scope.employeeAddModel.leavecount;
            createEmployeeParam.is_active = $scope.employeeAddModel.isactive == true ? 1 : 0;
            createEmployeeParam.is_user = $scope.employeeAddModel.isUser == true ? 1 : 0;
            createEmployeeParam.marital_status = $scope.employeeAddModel.maritalstatus == true ? 'married' : 'unmarried';
            createEmployeeParam.emp_code = $scope.employeeAddModel.empcode;
            createEmployeeParam.relationship = $scope.employeeAddModel.relationship;
            createEmployeeParam.nominee_name = $scope.employeeAddModel.nomineename;
            createEmployeeParam.pf_percentage = $scope.employeeAddModel.pfpercentage;
            createEmployeeParam.Pf_amount = $scope.employeeAddModel.pfamount;
            createEmployeeParam.employer_pf_amount = $scope.employeeAddModel.employeeramount;
            createEmployeeParam.employer_pf_percentage = $scope.employeeAddModel.employeerpf;
            createEmployeeParam.esi_percentage = $scope.employeeAddModel.esipercentage;
            createEmployeeParam.esi_amount = $scope.employeeAddModel.esiamount;
            createEmployeeParam.employer_esi_percentage = $scope.employeeAddModel.employeeresi;
            createEmployeeParam.employer_esi_amount = $scope.employeeAddModel.employeeresiamount;
            createEmployeeParam.payroll_type = $scope.employeeAddModel.payrolltype;
            adminService.saveEmployee(createEmployeeParam, headers).then(function (response) {

                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.employee')
                }
                $scope.isDataSavingProcess = false;

            });
        };
        $scope.getcountryList = function () {

            $scope.employeeAddModel.isLoadingProgress = true;
            var countryListParam = {};
            var headers = {};
            headers['screen-code'] = 'country';

            countryListParam.id = '';

            countryListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(countryListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeAddModel.country_list = data.list;
                }
                $scope.employeeAddModel.isLoadingProgress = false;
            });

        };
        $scope.getcountryList();

        $scope.getstateList = function () {

            $scope.employeeAddModel.isLoadingProgress = true;
            var stateListParam = {};
            var headers = {};
            headers['screen-code'] = 'state';

            stateListParam.id = '';

            stateListParam.is_active = 1;
            stateListParam.country_id = $scope.employeeAddModel.country_id;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getStateList(stateListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeAddModel.state_list = data.list;
                }
                $scope.employeeAddModel.isLoadingProgress = false;
            });

        };

        $scope.getcityList = function () {

            $scope.employeeAddModel.isLoadingProgress = true;
            var cityListParam = {};
            var headers = {};
            headers['screen-code'] = 'city';

            cityListParam.id = '';

            cityListParam.is_active = 1;
            cityListParam.country_id = $scope.employeeAddModel.country_id;
            cityListParam.state_id = $scope.employeeAddModel.state_id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCityList(cityListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.employeeAddModel.city_list = data.list;
                }
                $scope.employeeAddModel.isLoadingProgress = false;
            });

        };

        $scope.CountryChange = function ()
        {
            $scope.employeeAddModel.state_id = '';
            $scope.employeeAddModel.city_id = '';
            $scope.getstateList();

        }
        $scope.StateChange = function ()
        {
            $scope.employeeAddModel.city_id = '';
            $scope.getcityList();

        }
        $scope.getEmployeeTeam = function ()
        {
            var getListParam = {};
            getListParam.type = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmployeeTeamList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.employeeAddModel.teamList = data;
                }
            });
        };
        $scope.getEmployeeTeam();
    }]);






