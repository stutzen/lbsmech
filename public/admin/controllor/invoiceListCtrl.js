app.controller('invoiceListCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', 'customerFactory', '$localStorage', function ($scope, $rootScope, $state, $stateParams, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, customerFactory, $localStorage) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.invoiceModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            status: '',
            filterCustomerId: '',
            isLoadingProgress: true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.invoiceModel.limit = $scope.pagePerCount[0];
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.adminService = adminService;
        $scope.customerFilter = customerFactory.get();
        customerFactory.set('');

        $scope.searchFilter =
                {
                    id: '',
                    invoiceId: '',
                    duedate: '',
                    invoicedate: '',
                    customerInfo: {},
                    status: '',
                    notes: '',
                    customAttribute: [],
                    companyInfo: {},
                    code: ''
                };

        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                invoicedate: '',
                duedate: '',
                customerInfo: {},
                invoiceId: '',
                status: '',
                customAttribute: [],
                companyInfo: '',
                code: ''
            };
            $scope.initTableFilter();
        }
        $scope.invoicedateOpen = false;
        $scope.duedateOpen = false;
        //    'dd-MM-yyyy';
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.invoicedateOpen = true;
            } else if (index == 1)
            {
                $scope.duedateOpen = true;
            }
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getinvoiceListInfo, 300);
        }
        $scope.customAttributeList = [];
        $scope.isdeleteProgress = false;
        $scope.selectInvoiceId = '';
        $scope.showdeletePopup = false;
        $scope.showPopup = function (id)
        {
            $scope.selectInvoiceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteInvoiceItem = function ( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var getListParam = {};
                getListParam.id = $scope.selectInvoiceId;
                var headers = {};
                headers['screen-code'] = 'salesinvoice';
                adminService.deleteInvoice(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getinvoiceListInfo();
                    }

                    $scope.isdeleteProgress = false;
                });

            }

        };
        $scope.print = function (id)
        {
            if ($rootScope.appConfig.invoice_print_template === 't1')
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            } else if ($rootScope.appConfig.invoice_print_template === 't2')
            {
                $state.go('app.invoiceView2', {
                    'id': id
                }, {
                    'reload': true
                });
            } else
            {
                $state.go('app.invoiceView1', {
                    'id': id
                }, {
                    'reload': true
                });
            }
        };
        $scope.$watch('searchFilter.customerInfo', function (newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal === '')
            {
                //            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
                {
                    $scope.initTableFilter();
                }
            }
        });
        $scope.$watch('searchFilter.companyInfo', function (newVal, oldVal)
        {
            if (typeof newVal === 'undefined' || newVal === '')
            {
                $scope.initTableFilter();

            }
        });
        $scope.length = 0;
        $scope.colspan = 13;
        $scope.pagespan = 9;
        $scope.getCustomAttributeList = function () {

            var getListParam = {};
            getListParam.id = "";
            getListParam.attributetype_id = "";
            getListParam.attribute_code = "";
            getListParam.attributetypecode = "invoice";
            getListParam.mode = "1";
            getListParam.is_show_in_list = "1";
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption).then(function (response)
            {
                var data = response.data.list;
                if (data.length != 0)
                {
                    $scope.customAttributeList = data;
                    for (var i = 0; i < $scope.customAttributeList.length; i++)
                    {
                        if ($scope.customAttributeList[i].input_type == 'dropdown')
                        {
                            $scope.optionList = [];
                            $scope.optionList = $scope.customAttributeList[i].option_value.split(',');
                            $scope.customAttributeList[i].option_value = $scope.optionList;
                        }
                    }
                    $scope.length = $scope.customAttributeList.length;
                    $scope.colspan = $scope.colspan + $scope.length;
                    $scope.pagespan = $scope.colspan - 2;
                }
            });
        };
        $scope.invoiceSummary = {};
        $scope.getInvoiceSummaryList = function () {

            var getListParam = {};

            adminService.getInvoiceSummaryList(getListParam).then(function (response)
            {
                var data = response.data;
                $scope.invoiceSummary = data;
                $scope.invoiceSummary.overdue.amount = (parseFloat($scope.invoiceSummary.overdue.amount)).toFixed(2);
                $scope.invoiceSummary.overdue.amount = utilityService.changeCurrency($scope.invoiceSummary.overdue.amount, $rootScope.appConfig.thousand_seperator);
                $scope.invoiceSummary.duedate.amount = (parseFloat($scope.invoiceSummary.duedate.amount)).toFixed(2);
                $scope.invoiceSummary.duedate.amount = utilityService.changeCurrency($scope.invoiceSummary.duedate.amount, $rootScope.appConfig.thousand_seperator);
                $scope.invoiceSummary.nextdate.amount = (parseFloat($scope.invoiceSummary.nextdate.amount)).toFixed(2);
                $scope.invoiceSummary.nextdate.amount = utilityService.changeCurrency($scope.invoiceSummary.nextdate.amount, $rootScope.appConfig.thousand_seperator);
            });
        };
        if ($scope.customerFilter != '' && $scope.customerFilter != null && $scope.customerFilter != undefined)
        {
            $scope.searchFilter.customerInfo = $scope.customerFilter;
            $localStorage["sales_invoice_form-invoice_customer_dropdown"] = $scope.searchFilter.customerInfo;
        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            if (autosearchParam.search != '')
            {
                return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined && model.company != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && model.phone != undefined && model.company != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.company + ')';
                } else
                {
                    return model.fname;
                }
            }
            return  '';
        };

        $scope.getCompanyList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCompanyModel = function (model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.update = function (isfilteropen)
        {
            $scope.searchFilter.customAttribute = [];
            if (isfilteropen == false)
            {
                $scope.initTableFilter();
            }
        }
        $scope.findCustomFilterVar = function ()
        {
            var j = 0;
            var data = '&';
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.searchFilter.customAttribute[i] != 'undefined' && $scope.searchFilter.customAttribute[i] != null && $scope.searchFilter.customAttribute[i] != '')
                {
                    var j = j + 1;
                    if (j > 1)
                    {
                        data = data + '&' + 'cus_attr_' + j + '=' + $scope.customAttributeList[i].attribute_code + '::' + $scope.searchFilter.customAttribute[i];
                    } else
                    {
                        data = data + 'cus_attr_' + j + '=' + $scope.customAttributeList[i].attribute_code + '::' + $scope.searchFilter.customAttribute[i];
                    }
                }
            }
            if (data != '&')
            {
                return data;
            }
        }

        $scope.getinvoiceListInfo = function ()
        {
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.invoiceModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'salesinvoice';
            getListParam.invoice_code = $scope.searchFilter.code;
            getListParam.id = $scope.searchFilter.invoiceId;
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            } else
            {
                getListParam.customer_id = '';
            }
            if ($scope.searchFilter.companyInfo != null && typeof $scope.searchFilter.companyInfo != 'undefined' && typeof $scope.searchFilter.companyInfo.id != 'undefined')
            {
                getListParam.company_id = $scope.searchFilter.companyInfo.id;
            } else
            {
                getListParam.company_id = '';
            }
            if ($scope.searchFilter.invoicedate != null && $scope.searchFilter.invoicedate != '')
            {
                if ($scope.searchFilter.invoicedate != null && typeof $scope.searchFilter.invoicedate == 'object')
                {
                    getListParam.invoice_date = utilityService.parseDateToStr($scope.searchFilter.invoicedate, $scope.adminService.appConfig.date_format);
                }
                getListParam.invoice_date = utilityService.changeDateToSqlFormat(getListParam.invoice_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.duedate != null && $scope.searchFilter.duedate != '')
            {
                if ($scope.searchFilter.duedate != null && typeof $scope.searchFilter.duedate == 'object')
                {
                    getListParam.due_date = utilityService.parseDateToStr($scope.searchFilter.duedate, $scope.adminService.appConfig.date_format);
                }
                getListParam.due_date = utilityService.changeDateToSqlFormat(getListParam.due_date, $scope.adminService.appConfig.date_format);
            }
            var j = 0;
            for (var i = 0; i < $scope.customAttributeList.length; i++)
            {
                if ($scope.searchFilter.customAttribute[i] != 'undefined' && $scope.searchFilter.customAttribute[i] != null && $scope.searchFilter.customAttribute[i] != '')
                {
                    var j = j + 1;
                    getListParam['cus_attr_' + j] = $scope.customAttributeList[i].attribute_code + '::' + $scope.searchFilter.customAttribute[i];
                }
            }
            getListParam.start = ($scope.invoiceModel.currentPage - 1) * $scope.invoiceModel.limit;
            getListParam.limit = $scope.invoiceModel.limit;
            getListParam.is_active = 1;
            getListParam.status = $scope.searchFilter.status;
            getListParam.notes = $scope.searchFilter.notes;
            getListParam.mode = 0;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getInvoiceList(getListParam, configOption, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.invoiceModel.list = data.list;
                    if ($scope.invoiceModel.list.length > 0)
                    {

                        for (var i = 0; i < $scope.invoiceModel.list.length; i++)
                        {
                            $scope.invoiceModel.list[i].index = getListParam.start + i + 1;
                            $scope.invoiceModel.list[i].newdate = utilityService.parseStrToDate($scope.invoiceModel.list[i].date);
                            $scope.invoiceModel.list[i].date = utilityService.parseDateToStr($scope.invoiceModel.list[i].newdate, $rootScope.appConfig.date_format);
                            if ($scope.invoiceModel.list[i].duedate == null || $scope.invoiceModel.list[i].duedate == '' || $scope.invoiceModel.list[i].duedate == "0000-00-00")
                            {
                                $scope.invoiceModel.list[i].duedate = ' - ';
                            } else
                            {
                                $scope.invoiceModel.list[i].newduedate = utilityService.parseStrToDate($scope.invoiceModel.list[i].duedate);
                                $scope.invoiceModel.list[i].duedate = utilityService.parseDateToStr($scope.invoiceModel.list[i].newduedate, $rootScope.appConfig.date_format);
                            }
                            $scope.invoiceModel.list[i].total_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.invoiceModel.list[i].paid_amount = utilityService.changeCurrency($scope.invoiceModel.list[i].paid_amount, $rootScope.appConfig.thousand_seperator);

                        }
                    }
                    $scope.invoiceModel.total = data.total;
                }
                $scope.invoiceModel.isLoadingProgress = false;
            });
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'sales_invoice_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function (event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('updateTypeaheadFieldValue', function (event, data) {

            if (data.fieldName == "sales_invoice_form-invoice_customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
            if (data.fieldName == "sales_invoice_form-invoice_company_dropdown")
            {
                $scope.searchFilter.companyInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function (event, formName) {
           
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

        });


        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function (event, formName) {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function ()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function () {
            if ($scope.customerFilter == null || $scope.customerFilter == undefined || $scope.customerFilter == '' || $scope.customerFilter == 'Object')
            {
                $timeout(function () {
                    $scope.init();
                }, 300);
            }

        });
        $scope.checkFilter = function ()
        {
            if ($scope.customerFilter != null && $scope.customerFilter != undefined && $scope.customerFilter != '')
            {
                
                $scope.getinvoiceListInfo();
            } else
            {
                $scope.init();
            }
        }
        $scope.checkFilter();
        $scope.getCustomAttributeList();
        $scope.getInvoiceSummaryList();
    }]);




