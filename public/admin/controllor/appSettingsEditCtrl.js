
app.controller('appSettingsEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', '$stateParams', '$http', '$window', function($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, $stateParams, $http, $window) {

        $scope.appSettingsEditModel = {
            "id": 0,
            "date_format": '',
            "timezone": '',
            "isactive": 1,
            "currency": "",
            list: [],
            isLoadingProgress: false,
            "uom": '',
            "uomlabel": '',
            "invoice_item_custom1": '',
            "invoice_item_custom2": '',
            "invoice_item_custom3": '',
            "invoice_item_custom4": '',
            "invoice_item_custom5": '',
            "invoice_item_custom1_label": '',
            "invoice_item_custom2_label": '',
            "invoice_item_custom3_label": '',
            "invoice_item_custom4_label": '',
            "invoice_item_custom5_label": '',
            "purchase_invoice_item_custom1": '',
            "purchase_invoice_item_custom2": '',
            "purchase_invoice_item_custom3": '',
            "purchase_invoice_item_custom4": '',
            "purchase_invoice_item_custom5": '',
            "purchase_invoice_item_custom1_label": '',
            "purchase_invoice_item_custom2_label": '',
            "purchase_invoice_item_custom3_label": '',
            "purchase_invoice_item_custom4_label": '',
            "purchase_invoice_item_custom5_label": '',
            "purchase_uom": '',
            "purchase_uom_label": '',
            "apptitle": '',
            "invoiceprefix": '',
            "paytoaddress": '',
            "signature": '',
            "sms_count": '',
            "bank_detail": '',
            "invoicefrom": '',
            "invoicecc": '',
            "invoiceprinttemplate": '',
            "thousand_seperator": 'dd,dd,dd,ddd',
            "dual_language_support": false,
            "dual_language_label": '',
            "type": '',
            "prefix": '',
            "special_date1_label": '',
            "special_date2_label": '',
            "special_date3_label": '',
            "special_date4_label": '',
            "special_date5_label": '',
            "customer_special_date1": '',
            "customer_special_date2": '',
            "customer_special_date3": '',
            "customer_special_date4": '',
            "customer_special_date5": ''
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.app_settings_form != 'undefined' && typeof $scope.app_settings_form.$pristine != 'undefined' && !$scope.app_settings_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function() {
            if (typeof $scope.app_settings_form != 'undefined')
            {
                $scope.app_settings_form.$setPristine();
                $scope.getappSettingsInfo();
            }
        }

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $scope.showUploadFilePopup = false;
        $scope.showBackgroundUploadPopup = false;

        $scope.showPopup = function(value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = true;
            }
            else if (value == 'background')
            {
                $scope.showBackgroundUploadPopup = true;
            }

        }

        $scope.closePopup = function(value) {

            if (value == 'fileupload')
            {
                $scope.showUploadFilePopup = false;

            }
            else if (value == 'background')
            {
                $scope.showBackgroundUploadPopup = false;
            }
        }

        $scope.closeLogoUpload = function()
        {
            $scope.isImageSavingProcess = true;
            $scope.closePopup('fileupload');
            $window.location.reload();
            $scope.isImageSavingProcess = false;
        }

        $scope.closeBackgroundUpload = function()
        {
            $scope.isImageSavingProcess = true;
            $scope.closePopup('background');
            $window.location.reload();
            $scope.isImageSavingProcess = false;
        }

        $scope.updateSettingsInfo = function( )
        {
            var loop;

            for (loop = 0; loop < $scope.appSettingsEditModel.list.length; loop++)
            {
                if ($scope.appSettingsEditModel.list[loop].setting == "date_format")
                {
                    $scope.appSettingsEditModel.date_format = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "currency")
                {
                    $scope.appSettingsEditModel.currency = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "thousand_seperator")
                {
                    $scope.appSettingsEditModel.thousand_seperator = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_uom")
                {
                    $scope.appSettingsEditModel.uom = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_uom_label")
                {
                    $scope.appSettingsEditModel.uomlabel = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom1")
                {
                    $scope.appSettingsEditModel.invoice_item_custom1 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom2")
                {
                    $scope.appSettingsEditModel.invoice_item_custom2 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom3")
                {
                    $scope.appSettingsEditModel.invoice_item_custom3 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom4")
                {
                    $scope.appSettingsEditModel.invoice_item_custom4 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom5")
                {
                    $scope.appSettingsEditModel.invoice_item_custom5 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom1_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom1_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom2_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom2_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom3_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom3_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom4_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom4_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_item_custom5_label")
                {
                    $scope.appSettingsEditModel.invoice_item_custom5_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom1_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom1_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom2_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom2_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom3_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom3_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom4_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom4_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom5_label")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom5_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom1")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom1 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom2")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom2 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom3")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom3 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom4")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom4 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_item_custom5")
                {
                    $scope.appSettingsEditModel.purchase_invoice_item_custom5 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_uom")
                {
                    $scope.appSettingsEditModel.purchase_uom = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "purchase_uom_label")
                {
                    $scope.appSettingsEditModel.purchase_uom_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "app_title")
                {
                    $scope.appSettingsEditModel.apptitle = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_prefix")
                {
                    $scope.appSettingsEditModel.invoiceprefix = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "pay_to_address")
                {
                    $scope.appSettingsEditModel.paytoaddress = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "signature")
                {
                    $scope.appSettingsEditModel.signature = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "sms_count")
                {
                    $scope.appSettingsEditModel.sms_count = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "bank_detail")
                {
                    $scope.appSettingsEditModel.bank_detail = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoicefrom")
                {
                    $scope.appSettingsEditModel.invoicefrom = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoicecc")
                {
                    $scope.appSettingsEditModel.invoicecc = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "invoice_print_template")
                {
                    $scope.appSettingsEditModel.invoiceprinttemplate = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "timezone")
                {
                    $scope.appSettingsEditModel.timezone = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "dual_language_support")
                {
                    $scope.appSettingsEditModel.dual_language_support = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "dual_language_label")
                {
                    $scope.appSettingsEditModel.dual_language_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if (($scope.appSettingsEditModel.list[loop].setting == "sales_estimate_prefix"
                        || $scope.appSettingsEditModel.list[loop].setting == "sales_order_prefix"
                        || $scope.appSettingsEditModel.list[loop].setting == "sales_invoice_prefix"
                        || $scope.appSettingsEditModel.list[loop].setting == "purchase_estimate_prefix"
                        || $scope.appSettingsEditModel.list[loop].setting == "purchase_invoice_prefix") && $scope.appSettingsEditModel.type != '')
                {
                    $scope.appSettingsEditModel.prefix = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date1")
                {
                    $scope.appSettingsEditModel.customer_special_date1 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date2")
                {
                    $scope.appSettingsEditModel.customer_special_date2 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date3")
                {
                    $scope.appSettingsEditModel.customer_special_date3 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date4")
                {
                    $scope.appSettingsEditModel.customer_special_date4 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date5")
                {
                    $scope.appSettingsEditModel.customer_special_date5 = $scope.appSettingsEditModel.list[loop].value == 1 ? true : false;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date1_label")
                {
                    $scope.appSettingsEditModel.special_date1_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date2_label")
                {
                    $scope.appSettingsEditModel.special_date2_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date3_label")
                {
                    $scope.appSettingsEditModel.special_date3_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date4_label")
                {
                    $scope.appSettingsEditModel.special_date4_label = $scope.appSettingsEditModel.list[loop].value;
                }
                else if ($scope.appSettingsEditModel.list[loop].setting == "customer_special_date5_label")
                {
                    $scope.appSettingsEditModel.special_date5_label = $scope.appSettingsEditModel.list[loop].value;
                }

            }
            $scope.appSettingsEditModel.isLoadingProgress = false;
        }

        $scope.getappSettingsInfo = function()
        {
            $scope.appSettingsEditModel.isLoadingProgress = true;
            $scope.appSettingsEditModel.prefix = '';
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'appsettings';
            if ($rootScope.currentPage == 'invoicesettings')
            {
                headers['screen-code'] = 'invoicesettings';
            }
            else if ($rootScope.currentPage == 'prefixsettings')
            {
                headers['screen-code'] = 'prefixsettings';
            }
            else if ($rootScope.currentPage == 'customersettings')
            {
                headers['screen-code'] = 'customersettings';
            }
            else
            {
                headers['screen-code'] = 'appsettings';
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            getListParam.setting = $scope.appSettingsEditModel.type;
            adminService.getAppSettingsList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.appSettingsEditModel.list = data.list;
                        $scope.updateSettingsInfo( );
                    }

                }
                $scope.appSettingsEditModel.isLoadingProgress = false;
            });

        };
//        $scope.isRequiredLabel1 = false;
//        $scope.isRequiredLabel2 = false;
//        $scope.isRequiredLabel3 = false;
//        $scope.isRequiredLabel4 = false;
//        $scope.isRequiredLabel5 = false;
//
//        $scope.changeRequired = function ()
//        {
//            if ($scope.appSettingsEditModel.customer_special_date1 == true)
//            {
//                $scope.isRequiredLabel1 = true;
//            } else
//            {
//                $scope.isRequiredLabel1 = false;
//            }
//        };

        $scope.getappSettingsInfo( );
        $scope.updateAppSettings = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var attributeParam = [];
                var headers = {};
                headers['screen-code'] = 'appsettings';
                if ($rootScope.currentPage == 'invoicesettings')
                {
                    headers['screen-code'] = 'invoicesettings';

                    var newRow = {
                        "setting": "invoice_item_uom",
                        "value": $scope.appSettingsEditModel.uom
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoice_item_uom_label",
                        "value": $scope.appSettingsEditModel.uomlabel
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoice_item_custom1",
                        "value": $scope.appSettingsEditModel.invoice_item_custom1
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom2",
                        "value": $scope.appSettingsEditModel.invoice_item_custom2
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom3",
                        "value": $scope.appSettingsEditModel.invoice_item_custom3
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom4",
                        "value": $scope.appSettingsEditModel.invoice_item_custom4
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom5",
                        "value": $scope.appSettingsEditModel.invoice_item_custom5
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoice_item_custom1_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom1_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom2_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom2_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom3_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom3_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom4_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom4_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_item_custom5_label",
                        "value": $scope.appSettingsEditModel.invoice_item_custom5_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom1",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom1
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom2",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom2
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom3",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom3
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom4",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom4
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom5",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom5
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom1_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom1_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom2_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom2_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom3_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom3_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom4_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom4_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_invoice_item_custom5_label",
                        "value": $scope.appSettingsEditModel.purchase_invoice_item_custom5_label
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_uom",
                        "value": $scope.appSettingsEditModel.purchase_uom
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "purchase_uom_label",
                        "value": $scope.appSettingsEditModel.purchase_uom_label
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoice_prefix",
                        "value": $scope.appSettingsEditModel.invoiceprefix
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "invoicefrom",
                        "value": $scope.appSettingsEditModel.invoicefrom
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoicecc",
                        "value": $scope.appSettingsEditModel.invoicecc
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "invoice_print_template",
                        "value": $scope.appSettingsEditModel.invoiceprinttemplate
                    }
                    attributeParam.push(newRow);
                }
                else if ($rootScope.currentPage == 'prefixsettings')
                {
                    headers['screen-code'] = 'prefixsettings';
                    var newRow = {
                        "setting": $scope.appSettingsEditModel.type,
                        "value": $scope.appSettingsEditModel.prefix
                    }
                    attributeParam.push(newRow);
                }
                else if ($rootScope.currentPage == 'customersettings')
                {
                    headers['screen-code'] = 'customersettings';
                    var newRow = {
                        "setting": "customer_special_date1",
                        "value": $scope.appSettingsEditModel.customer_special_date1
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_special_date2",
                        "value": $scope.appSettingsEditModel.customer_special_date2
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_special_date3",
                        "value": $scope.appSettingsEditModel.customer_special_date3
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_special_date4",
                        "value": $scope.appSettingsEditModel.customer_special_date4
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "customer_special_date5",
                        "value": $scope.appSettingsEditModel.customer_special_date5
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date1_label";
                    if ($scope.appSettingsEditModel.customer_special_date1 == true &&
                            $scope.appSettingsEditModel.special_date1_label == '')
                    {
                        newRow.value = 'Special Date1';
                    }
                    else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date1_label;
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date2_label";
                    if ($scope.appSettingsEditModel.customer_special_date2 == true &&
                            $scope.appSettingsEditModel.special_date2_label == '')
                    {
                        newRow.value = 'Special Date2';
                    }
                    else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date2_label;
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date3_label";
                    if ($scope.appSettingsEditModel.customer_special_date3 == true &&
                            $scope.appSettingsEditModel.special_date3_label == '')
                    {
                        newRow.value = 'Special Date3';
                    }
                    else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date3_label;
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date4_label";
                    if ($scope.appSettingsEditModel.customer_special_date4 == true &&
                            $scope.appSettingsEditModel.special_date4_label == '')
                    {
                        newRow.value = 'Special Date4';
                    }
                    else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date4_label;
                    }
                    attributeParam.push(newRow);
                    var newRow = {};
                    newRow.setting = "customer_special_date5_label";
                    if ($scope.appSettingsEditModel.customer_special_date5 == true &&
                            $scope.appSettingsEditModel.special_date5_label == '')
                    {
                        newRow.value = 'Special Date5';
                    }
                    else
                    {
                        newRow.value = $scope.appSettingsEditModel.special_date5_label;
                    }
                    attributeParam.push(newRow);

                }
                else
                {
                    headers['screen-code'] = 'appsettings';

                    var newRow = {
                        "setting": "date_format",
                        "value": $scope.appSettingsEditModel.date_format
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "currency",
                        "value": $scope.appSettingsEditModel.currency
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "thousand_seperator",
                        "value": $scope.appSettingsEditModel.thousand_seperator
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "app_title",
                        "value": $scope.appSettingsEditModel.apptitle
                    }

                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "pay_to_address",
                        "value": $scope.appSettingsEditModel.paytoaddress
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "signature",
                        "value": $scope.appSettingsEditModel.signature
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "sms_count",
                        "value": $scope.appSettingsEditModel.sms_count
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "bank_detail",
                        "value": $scope.appSettingsEditModel.bank_detail
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "timezone",
                        "value": $scope.appSettingsEditModel.timezone
                    }
                    attributeParam.push(newRow);

                    var newRow = {
                        "setting": "dual_language_support",
                        "value": $scope.appSettingsEditModel.dual_language_support
                    }
                    attributeParam.push(newRow);
                    var newRow = {
                        "setting": "dual_language_label",
                        "value": $scope.appSettingsEditModel.dual_language_label
                    }
                    attributeParam.push(newRow);

                }


                var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                adminService.editAppSettings(attributeParam, configOption, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {

                        $rootScope.appConfig.currency = $scope.appSettingsEditModel.currency;
                        $rootScope.appConfig.thousand_seperator = $scope.appSettingsEditModel.thousand_seperator;
                        $rootScope.appConfig.date_format = $scope.appSettingsEditModel.date_format;
                        $rootScope.appConfig.uom = $scope.appSettingsEditModel.uom;
                        $rootScope.appConfig.invoice_item_custom1 = $scope.appSettingsEditModel.invoice_item_custom1;
                        $rootScope.appConfig.invoice_item_custom2 = $scope.appSettingsEditModel.invoice_item_custom2;
                        $rootScope.appConfig.invoice_item_custom3 = $scope.appSettingsEditModel.invoice_item_custom3;
                        $rootScope.appConfig.invoice_item_custom4 = $scope.appSettingsEditModel.invoice_item_custom4;
                        $rootScope.appConfig.invoice_item_custom5 = $scope.appSettingsEditModel.invoice_item_custom5;
                        $rootScope.appConfig.invoice_item_custom1_label = $scope.appSettingsEditModel.invoice_item_custom1_label;
                        $rootScope.appConfig.invoice_item_custom2_label = $scope.appSettingsEditModel.invoice_item_custom2_label;
                        $rootScope.appConfig.invoice_item_custom3_label = $scope.appSettingsEditModel.invoice_item_custom3_label;
                        $rootScope.appConfig.invoice_item_custom4_label = $scope.appSettingsEditModel.invoice_item_custom4_label;
                        $rootScope.appConfig.invoice_item_custom5_label = $scope.appSettingsEditModel.invoice_item_custom5_label;
                        $rootScope.appConfig.purchase_invoice_item_custom1 = $scope.appSettingsEditModel.purchase_invoice_item_custom1;
                        $rootScope.appConfig.purchase_invoice_item_custom2 = $scope.appSettingsEditModel.purchase_invoice_item_custom2;
                        $rootScope.appConfig.purchase_invoice_item_custom3 = $scope.appSettingsEditModel.purchase_invoice_item_custom3;
                        $rootScope.appConfig.purchase_invoice_item_custom4 = $scope.appSettingsEditModel.purchase_invoice_item_custom4;
                        $rootScope.appConfig.purchase_invoice_item_custom5 = $scope.appSettingsEditModel.purchase_invoice_item_custom5;
                        $rootScope.appConfig.purchase_invoice_item_custom1_label = $scope.appSettingsEditModel.purchase_invoice_item_custom1_label;
                        $rootScope.appConfig.purchase_invoice_item_custom2_label = $scope.appSettingsEditModel.purchase_invoice_item_custom2_label;
                        $rootScope.appConfig.purchase_invoice_item_custom3_label = $scope.appSettingsEditModel.purchase_invoice_item_custom3_label;
                        $rootScope.appConfig.purchase_invoice_item_custom4_label = $scope.appSettingsEditModel.purchase_invoice_item_custom4_label;
                        $rootScope.appConfig.purchase_invoice_item_custom5_label = $scope.appSettingsEditModel.purchase_invoice_item_custom5_label;
                        $rootScope.appConfig.purchase_uom = $scope.appSettingsEditModel.purchase_uom;
                        $rootScope.appConfig.purchase_uom_label = $scope.appSettingsEditModel.purchase_uom_label;
                        $rootScope.appConfig.app_tile = $scope.appSettingsEditModel.apptitle;
                        $rootScope.appConfig.invoice_prefix = $scope.appSettingsEditModel.invoiceprefix;
                        $rootScope.appConfig.pay_to_address = $scope.appSettingsEditModel.paytoaddress;
                        $rootScope.appConfig.signature = $scope.appSettingsEditModel.signature;
                        $rootScope.appConfig.sms_count = $scope.appSettingsEditModel.sms_count;
                        $rootScope.appConfig.bank_detail = $scope.appSettingsEditModel.bank_detail;
                        $rootScope.appConfig.invoicefrom = $scope.appSettingsEditModel.invoicefrom;
                        $rootScope.appConfig.invoice_print_template = $scope.appSettingsEditModel.invoiceprinttemplate;
                        $rootScope.appConfig.invoicecc = $scope.appSettingsEditModel.invoicecc;
                        $rootScope.appConfig.timezone = $scope.appSettingsEditModel.timezone;
                        $rootScope.appConfig.dual_language_support = $scope.appSettingsEditModel.dual_language_support;
                        $rootScope.appConfig.dual_language_label = $scope.appSettingsEditModel.dual_language_label;
                        $rootScope.appConfig.customer_special_date1 = $scope.appSettingsEditModel.customer_special_date1;
                        $rootScope.appConfig.customer_special_date2 = $scope.appSettingsEditModel.customer_special_date2;
                        $rootScope.appConfig.customer_special_date3 = $scope.appSettingsEditModel.customer_special_date3;
                        $rootScope.appConfig.customer_special_date4 = $scope.appSettingsEditModel.customer_special_date4;
                        $rootScope.appConfig.customer_special_date5 = $scope.appSettingsEditModel.customer_special_date5;
                        $rootScope.appConfig.customer_special_date1_label = $scope.appSettingsEditModel.special_date1_label;
                        $rootScope.appConfig.customer_special_date2_label = $scope.appSettingsEditModel.special_date2_label;
                        $rootScope.appConfig.customer_special_date3_label = $scope.appSettingsEditModel.special_date3_label;
                        $rootScope.appConfig.customer_special_date4_label = $scope.appSettingsEditModel.special_date4_label;
                        $rootScope.appConfig.customer_special_date5_label = $scope.appSettingsEditModel.special_date5_label;
                        $scope.formReset();
                        if ($window.location.hash == "#/invoicesettings")
                        {
                            $state.go('app.invoicesettings', {'reload': true});
                        }
                        else if ($window.location.hash == "#/prefixsettings")
                        {
                            $state.go('app.prefixsettings', {'reload': true});
                        }
                        else if ($window.location.hash == "#/customersettings")
                        {
                            $state.go('app.customersettings', {'reload': true});
                        }
                        else
                        {
                            $state.go('app.appSettings', {'reload': true});
                        }
                    }
                    $scope.isDataSavingProcess = false;

                });
            }
        }
    }]);

