

app.controller('sourceEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.sourceModel = {
            id: '',
            name: '',
            isActive: true,
            list: {},
            comments: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_source_form != 'undefined' && typeof $scope.add_source_form.$pristine != 'undefined' && !$scope.add_source_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_source_form.$setPristine();
            if (typeof $scope.add_source_form != 'undefined')
            {
                $scope.sourceModel.name = "";
                $scope.sourceModel.isActive = true;
            }
            $scope.updateSourceInfo();
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.modifysource = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addSourceParam = {};
            var headers = {};
            headers['screen-code'] = 'source';
            addSourceParam.id = $stateParams.id;
            addSourceParam.name = $scope.sourceModel.name;
            addSourceParam.comments = $scope.sourceModel.comments;
            addSourceParam.is_active = $scope.sourceModel.isActive;

            adminService.editSource(addSourceParam, $scope.sourceModel.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.source');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.updateSourceInfo = function()
        {
            $scope.sourceModel.is_active = $scope.sourceModel.list.is_active;
            $scope.sourceModel.name = $scope.sourceModel.list.name;
            $scope.sourceModel.isLoadingProgress = false;
            $scope.sourceModel.id = $scope.sourceModel.list.id;
            $scope.sourceModel.comments =$scope.sourceModel.list.comments;
        }

        $scope.getSourceInfo = function()
        {
            $scope.sourceModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var productListParam = {};
                productListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getSourceList(productListParam, configOption).then(function(response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.sourceModel.list = data.list[0];

                    }
                    $scope.updateSourceInfo();

                });
            }
        };
        $scope.getSourceInfo();
    }]);
