app.controller('userRoleAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state) {

        $scope.userRoleAddModel = {
            "id": 0,
            "username": "",
            "rolename": "",
            "name": "",
            "comments": "",
            "isactive": 1,
            "userList": {},
            currentPage: 1,
            total: 0,
            showPassword: false,
            role: '',
            isLoadingProgress: false
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_user_role_form != 'undefined' && typeof $scope.create_user_role_form.$pristine != 'undefined' && !$scope.create_user_role_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function()
        {
            $scope.create_user_role_form.$setPristine();
            $scope.userRoleAddModel.name = '';
            //$scope.userRoleAddModel.username = '';
            //$scope.userRoleAddModel.rolename = '';
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createUser, 300);
        }

        $scope.getUserList = function() {

            var getListParam = {};
            getListParam.userId = '';
            $scope.userRoleAddModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function(response)
            {
                var data = response.data.data;
                $scope.userRoleAddModel.userList = data;
                $scope.userRoleAddModel.isLoadingProgress = false;
            });

        };

        $scope.getUserList();

        $scope.createUserrole = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createUserRoleParam = {};
                var headers = {};
                headers['screen-code'] = 'role';
                createUserRoleParam.id = 0;
                createUserRoleParam.name = $scope.userRoleAddModel.rolename;
                createUserRoleParam.comments = $scope.userRoleAddModel.comments;
                //createUserRoleParam.username = $scope.userRoleAddModel.username;
                //createUserRoleParam.rolename = $scope.userRoleAddModel.rolename;            
                createUserRoleParam.is_active = 1;

                adminService.addUserRole(createUserRoleParam, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.userrole')
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };
    }]);




