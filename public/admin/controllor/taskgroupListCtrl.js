app.controller('taskgroupListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.taskGroupModel = {
            
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
           
            isLoadingProgress: true

        };
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.taskGroupModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''            
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getTaskGroupList, 300);
        }

        
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function(id)
        {
            $scope.selectedId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteTaskGrpInfo = function( )
        {
            if($scope.isdeleteProgress == false)
            {
               $scope.isdeleteProgress = true;
               
                var getListParam = {};
                getListParam.id = $scope.selectedId;
                var headers = {};
                headers['screen-code'] = 'taskgroup';
                adminService.deleteTaskGroup(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getTaskGroupList();
                    }
                     $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: ''
            };
            $scope.initTableFilter();
        }

        $scope.getTaskGroupList= function() {          

            $scope.taskGroupModel.isLoadingProgress = true;
            var taskGrpListParam = {};
            var headers = {};
            headers['screen-code'] = 'taskgroup';
            taskGrpListParam.name = $scope.searchFilter.name;           
            taskGrpListParam.id = '';            
            taskGrpListParam.is_active = 1;
            taskGrpListParam.start = ($scope.taskGroupModel.currentPage - 1) * $scope.taskGroupModel.limit;
            taskGrpListParam.limit = $scope.taskGroupModel.limit;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaskGroupList(taskGrpListParam, configOption, headers).then(function(response)
            {               
                if (response.data.success === true)
                {                   
                    var data = response.data;
                    $scope.taskGroupModel.list = data.list;
                    $scope.taskGroupModel.total = data.total;                    
                }
                $scope.taskGroupModel.isLoadingProgress = false;
            });

        };

        $scope.getTaskGroupList();

    }]);




