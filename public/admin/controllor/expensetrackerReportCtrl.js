
app.controller('expensetrackerReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout','$localStorageService','$localStorage', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout,$localStorageService,$localStorage) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.trackerModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            productname: '',
            uom: '',
            quantity: '',
            amount: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.trackerModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0)); 
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.company_name = $rootScope.appConfig.app_tile;
        $scope.company_address = $rootScope.appConfig.pay_to_address;
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: ''
        };
        $scope.searchFilter.todate = $scope.currentDate;
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: ''
            };
            $scope.trackerModel.list = [];
            $scope.searchFilter.todate = $scope.currentDate;
        }

        $scope.print = function()
        {
            $window.print();
        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.show = function(index)
        {
//            for (var i = 0; i < $scope.trackerModel.list[index].data; i++)
//            {
//                if ($scope.trackerModel.list[index].data[i].flag == index)
//                {
                    if ($scope.showDetails[index])
                    {
                        $scope.showDetails[index] = false;
                    }
                    else
                        $scope.showDetails[index] = true;
//                }
//            }
        };

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'expensetrackerreport';
             getListParam.from_date = '';
            getListParam.to_date = '';
            if ($scope.searchFilter.todate != '' && $scope.searchFilter.todate != null)
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = getListParam.to_date;
            }
             if ($scope.searchFilter.fromdate != '' && $scope.searchFilter.fromdate != null)
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//                getListParam.to_date = getListParam.from_date;
            }
//            else
//            {
//                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
//                {
//                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
//                }
//                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
//                {
//                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
//                }
//                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
//                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
//            }
            getListParam.show_all = 1;
            $scope.trackerModel.isLoadingProgress = true;
            $scope.showDetails = [];
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getExpenseTrackerReport(getListParam,configOption,headers).then(function(response)
            {
                var totalAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.trackerModel.list = data;

                    for (var i = 0; i < $scope.trackerModel.list.length; i++)
                    {
                        totalAmt = totalAmt + parseFloat($scope.trackerModel.list[i].amount);
                        $scope.trackerModel.list[i].newdate = utilityService.parseStrToDate($scope.trackerModel.list[i].date);
                        $scope.trackerModel.list[i].date = utilityService.parseDateToStr($scope.trackerModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        $scope.trackerModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                    }
                    $scope.trackerModel.total_Amount = totalAmt;
                    $scope.trackerModel.total_Amount = parseFloat($scope.trackerModel.total_Amount).toFixed(2);
                    $scope.trackerModel.total_Amount = utilityService.changeCurrency($scope.trackerModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                }
                $scope.trackerModel.isLoadingProgress = false;
            });

        };
        $scope.redirect = function(id,date)
        {
            $localStorage.user_id = id;
            $localStorage.date = date;
        }
        

        $scope.getList();
    }]);




