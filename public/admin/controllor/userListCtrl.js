


app.controller('userListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.userModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            email: '',
            list: [],
            country: '',
            city: '',
            id: '',
            status: '',
            serverList: null,
            isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.userModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            email: '',
            username: '',
            phno: '',
            user_type: '',
            name: '',
            country: '',
            city: '',
            isactive: '1'
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.save_invite_form != 'undefined' && typeof $scope.save_invite_form.$pristine != 'undefined' && !$scope.save_invite_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function () {

            $scope.save_invite_form.$setPristine();
            $scope.userModel.email = '';
        }
        $scope.refreshScreen = function ()
        {
            $scope.searchFilter = {
                email: '',
                phno: '',
                name: '',
                city: '',
                isactive: '1'
            };
            $scope.initTableFilter();
        }
        $scope.selectUserId = '';
        $scope.showdeletePopup = false;
        $scope.showInvitePopup = false;
        $scope.isdeleteUserLoadingProgress = false;
        $scope.invitePopup = function ()
        {
            $scope.userModel.email = '';
            $scope.showInvitePopup = true;
        };

        $scope.closeInvitePopup = function ()
        {
            $scope.showInvitePopup = false;
        };

        $scope.showPopup = function (id)
        {
            $scope.selectUserId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteUserLoadingProgress = false;
        };

        $scope.deleteUserInfo = function ( )
        {
            if ($scope.isdeleteUserLoadingProgress == false)
            {
                $scope.isdeleteUserLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectUserId;
                var headers = {};
                headers['screen-code'] = 'users';
                adminService.deleteUser(getListParam, getListParam.id, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteUserLoadingProgress = false;
                });
            }
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'users';
            getListParam.userId = '';
            getListParam.companyId = $rootScope.userModel.companyId;
            getListParam.name = $scope.searchFilter.name;
            getListParam.phno = $scope.searchFilter.phno;
            getListParam.email = $scope.searchFilter.email;
            getListParam.country = '';
            getListParam.city = $scope.searchFilter.city;
            getListParam.userType = $scope.searchFilter.user_type;
            getListParam.userName = '';
            getListParam.isactive = $scope.searchFilter.isactive;
            getListParam.start = ($scope.userModel.currentPage - 1) * $scope.userModel.limit;
            getListParam.limit = $scope.userModel.limit;
            $scope.userModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getUserList(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.userModel.list = data.list;
                    $scope.userModel.total = data.total;
                }
                $scope.userModel.isLoadingProgress = false;
            });

        };
        $scope.saveUserInvite = function () {

            $scope.isDataSavingProcess = true;
            var userInviteParam = [];
            var userInfo = {};
            userInfo.email = $scope.userModel.email;
            userInviteParam.push(userInfo);
            adminService.createUserInvite(userInviteParam).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.showInvitePopup = false;
                    $scope.formReset();
                    $state.go('app.userlist')
                }
                $scope.isDataSavingProcess = false;

            });
        };
        $scope.getList();
    }]);








