


app.controller('manufacturingWorkorderListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.workModel = {
        currentPage: 1,
        total: 0,
        limit: 10,
        list: [],
        status: '',
        isLoadingProgress: false
    };

    $scope.pagePerCount = [50, 100];
    $scope.workModel.limit = $scope.pagePerCount[0];

    $scope.searchFilter = {
        status: 'inProgress'
    };

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }
    $scope.refreshScreen = function()
    {
        $scope.searchFilter = {
            status: 'inProgress'
        };
        $scope.initTableFilter();
    }
    
    $scope.getList = function() {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'manufacturingworkorder';
        getListParam.id = '';
        getListParam.is_active = 1;
        getListParam.status = $scope.searchFilter.status;
        getListParam.start = ($scope.workModel.currentPage - 1) * $scope.workModel.limit;
        getListParam.limit = $scope.workModel.limit;
        $scope.workModel.isLoadingProgress = true;
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getWorkOrderDetailList(getListParam, configOption, headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.workModel.list = data;
                $scope.workModel.total = response.data.total;
            }
            $scope.workModel.isLoadingProgress = false;
        });

    };


    $scope.getList();
}]);








