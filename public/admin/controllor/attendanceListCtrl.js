
app.controller('attendanceListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.attendanceModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            total_duration: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };

        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.attendanceModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;


        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            employeeInfo: ''

        };
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }
        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                employeeInfo: ''
            };
            $scope.attendanceModel.list = [];
        }
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            showWeeks: false
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.searchFilterValue = "";
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.attendance_form != 'undefined' && typeof $scope.attendance_form.$pristine != 'undefined' && !$scope.attendance_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getAttendanceList, 300);
        }

        $scope.getAttendanceList = function () {

            var attendanceListParam = {};
            var headers = {};
            headers['screen-code'] = 'attendance';
            attendanceListParam.user_id = '';

            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    attendanceListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                attendanceListParam.to_date = utilityService.changeDateToSqlFormat(attendanceListParam.to_date, $scope.dateFormat);
                attendanceListParam.from_date = attendanceListParam.to_date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    attendanceListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                attendanceListParam.from_date = utilityService.changeDateToSqlFormat(attendanceListParam.from_date, $scope.dateFormat);
                attendanceListParam.to_date = attendanceListParam.from_date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    attendanceListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    attendanceListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                attendanceListParam.to_date = utilityService.changeDateToSqlFormat(attendanceListParam.to_date, $scope.dateFormat);
                attendanceListParam.from_date = utilityService.changeDateToSqlFormat(attendanceListParam.from_date, $scope.dateFormat);
            }
            if ($scope.searchFilter.employeeInfo != undefined && $scope.searchFilter.employeeInfo != null && $scope.searchFilter.employeeInfo.id != null)
            {
                attendanceListParam.emp_id = $scope.searchFilter.employeeInfo.id;
            }
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getWorklogList(attendanceListParam, configOption, headers).then(function (response)
            {
                var data = response.data;
                $scope.attendanceModel.list = data.list;
                for (var i = 0; i < $scope.attendanceModel.list.length; i++)
                {
                    $scope.attendanceModel.list[i].showDetails = false;
                    if ($scope.attendanceModel.list[i].start_location != null)
                    {
                        if ($scope.attendanceModel.list[i].start_location.includes(','))
                        {
                            var location = $scope.attendanceModel.list[i].start_location.split(',');
                            $scope.attendanceModel.list[i].start_lat = location[0];
                            $scope.attendanceModel.list[i].start_lng = location[1];
                        }
                    }
                    if ($scope.attendanceModel.list[i].end_location != null)
                    {
                        if ($scope.attendanceModel.list[i].end_location.includes(','))
                        {
                            var location1 = $scope.attendanceModel.list[i].end_location.split(',');
                            $scope.attendanceModel.list[i].end_lat = location1[0];
                            $scope.attendanceModel.list[i].end_lng = location1[1];
                        }
                    }
                }
                // $scope.getLatLng();

            });
        };

        $scope.getLatLng = function ()
        {
            for (var i = 0; i < $scope.attendanceModel.list.length; i++)
            {
                var geoParam = {};
                geoParam.address = $scope.attendanceModel.list[i].start_location;
                geoParam.key = APP_CONST.GOOGLE_API_KEY;
                var configOption = adminService.handleOnlyErrorResponseConfig;
                return $httpService.get(APP_CONST.API.GEOCODE_SEARCH_URL, geoParam, false).then(function (responseData) {
                    var listData = responseData.result[0].geometry.location;
                    $scope.attendanceModel.list[i].start_lat = listData.lat;
                    $scope.attendanceModel.list[i].start_lng = listData.lng;
                });
            }
            $scope.attendanceModel.isLoadingProgress = false;
        };

        $scope.expand = function (index)
        {
            if ($scope.attendanceModel.list[index].showDetails)
            {
                $scope.attendanceModel.list[index].showDetails = false;
            } else
            {
                $scope.attendanceModel.list[index].showDetails = true;
            }
        }

        $scope.getEmployeeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeeModel = function (model) {

            if (model != null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };


    }]);
















