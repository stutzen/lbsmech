





app.controller('expensereportCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.expensereportModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            total_Amount: 0,
            list: [],
            user: '',
            id: '',
            date: '',
            particular: '',
            category: '',
            created_by: '',
            incomeCategoryInfo: '',
            amount: '',
            is_active: 1,
            status: '',
            userName: '',
//             sku: '',

            serverList: null,
            isSearchLoadingProgress: false,
            isLoadingProgress: false
        };


        $scope.searchFilter = {
            credited_by: '',
            customerid: '',
            limit: '',
            start: '',
            incomeCategoryInfo: '',
            fromdate: '',
            todate: ''
        };

        $scope.adminService = adminService;
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0)); 
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.searchFilter.fromdate = $scope.currentDate;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.validationFactory = ValidationFactory;

        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };
        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                credited_by: '',
                customerid: '',
                limit: '',
                start: '',
                incomeCategoryInfo: '',
                fromdate: '',
                todate: ''
            };
            $scope.expensereportModel.list = [];
            $scope.searchFilter.fromdate = $scope.currentDate;
            $scope.initTableFilter();
        }


        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !=null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };

        $scope.$watch('searchFilter.incomeCategoryInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });
        $scope.getList = function() {

            $scope.expensereportModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'expensereport';

               if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = getListParam.to_Date;
            }
            else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
                getListParam.to_Date = getListParam.from_Date;
            }
            else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
            }

            getListParam.id = '',
                    getListParam.account_id = '';
            getListParam.customer_id = '';
            getListParam.name = '';
            getListParam.created_by = $scope.searchFilter.credited_by;
            if ($scope.searchFilter.incomeCategoryInfo != null && typeof $scope.searchFilter.incomeCategoryInfo != 'undefined' && typeof $scope.searchFilter.incomeCategoryInfo.id != 'undefined')
            {
                getListParam.account_category = $scope.searchFilter.incomeCategoryInfo.name;
            }
            else
            {
                getListParam.account_category = '';
            }
            //  getListParam.start = ($scope.expensereportModel.currentPage - 1) * $scope.expensereportModel.limit;
            //  getListParam.limit = $scope.expensereportModel.limit;
            getListParam.is_active = 1;
            getListParam.mode = 1;
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.expensereportModel.isLoadingProgress = true;
            $scope.expensereportModel.isSearchLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getexpensereportList(getListParam, configOption, headers).then(function(response) {
                var totalAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.expensereportModel.list = data;
                    for (var i = 0; i < $scope.expensereportModel.list.length; i++)
                    {
                        $scope.expensereportModel.list[i].newdate = utilityService.parseStrToDate($scope.expensereportModel.list[i].transaction_date);
                        $scope.expensereportModel.list[i].transaction_date = utilityService.parseDateToStr($scope.expensereportModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        totalAmt = totalAmt + parseFloat($scope.expensereportModel.list[i].amount)
                        $scope.expensereportModel.list[i].amount = utilityService.changeCurrency($scope.expensereportModel.list[i].amount, $rootScope.appConfig.thousand_seperator);

                    }
                    $scope.expensereportModel.total = data.total;
                    $scope.expensereportModel.total_Amount = totalAmt;
                    $scope.expensereportModel.total_Amount = parseFloat($scope.expensereportModel.total_Amount).toFixed(2);
                    $scope.expensereportModel.total_Amount = utilityService.changeCurrency($scope.expensereportModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                }
                $scope.expensereportModel.isLoadingProgress = false;
                $scope.expensereportModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getIncomeCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.type = 'expense';
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatincomeCategoryModel = function(model) {

            if (model != null && model != undefined)
            {

                return model.name;
            }
            return  '';
        };
        $scope.getUserList = function() {

            var getListParam = {};
            getListParam.date = '';
            getListParam.particular = '';
            getListParam.category = '';
            getListParam.credited_by = '';
            getListParam.amount = '';
            getListParam.isactive = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function(response) {
                var data = response.data;
                $scope.expensereportModel.userlist = data.list;

            });

        };
        $scope.findUserName = function()
        {
            if ($scope.searchFilter.credited_by != null && $scope.searchFilter.credited_by != '')
            {
                for (var i = 0; i < $scope.expensereportModel.userlist.length; i++)
                {
                    if ($scope.searchFilter.credited_by == $scope.expensereportModel.userlist[i].id)
                    {
                        $scope.expensereportModel.userName = $scope.expensereportModel.userlist[i].f_name + '(' + $scope.expensereportModel.userlist[i].username + ')';
                    }
                }
            }
        }
        $scope.getListPrint = function(val)
        {
            var getListParam = {};
            getListParam.Id = "";
            getListParam.start = 0;
            $scope.expensereportModel.isLoadingProgress = true;
            adminService.getExpensereportList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.expensereportModel.printList = data;
                $scope.expensereportModel.isLoadingProgress = false;
            });
        };

        $scope.print = function()
        {
            $window.print();
        };
        // $scope.getList();
        $scope.getUserList();
        //$scope.getListPrint();
    }]);




