




app.controller('advancepaymentViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.advanceModel = {
            "advanceDetails": '',
            'list': ''
        };

        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.advancepayment_view !== 'undefined' && typeof $scope.advancepayment_view.$pristine !== 'undefined' && !$scope.advancepayment_view.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {
            $scope.transferlist_edit_form.$setPristine();
            $scope.updateTransferlistInfo();

        };
//        $scope.currentDate = new Date();
//       $scope.advanceModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTabletCreate = function () {

            $scope.advanceModel.user = '';

        };
        

        $scope.updateadvanceInfo = function ()
        {
            if ($scope.advanceModel.list !== null)
            {
                
                $scope.advanceModel.advanceDetails = $scope.advanceModel.list;
                $scope.advanceModel.isLoadingProgress = false;
            }
        };




        $scope.getadvanceInfo = function () {
            
            if (typeof $stateParams.id !== 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                getListParam.voucher_type  = 'advance_invoice';
                //getListParam.type=1;
                var headers = {};
                headers['screen-code'] = 'advancepayment';
                $scope.advanceModel.isLoadingProgress = true;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getAdvancepaymentList(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        data.list[0].date = utilityService.parseDateToStr(data.list[0].date, "dd/MM/yyyy");
                        $scope.advanceModel.list = data.list[0];
                        $scope.updateadvanceInfo();
                    }

                });
            }
        };



        $scope.getadvanceInfo();

    $scope.selectedpaymentId = '';
    $scope.showdeletePopup = false;
    $scope.isdeleteProgress = false;
    $scope.showPopup = function (id)
    {
        //$scope.showdeletePopup = false;
        $scope.selectedpaymentId = id;
//        for(var i=0; i<$scope.advanceModel.list.length;i++)
//        {
//            if($scope.selectedpaymentId==$scope.advanceModel.list[i].id)
//            {   
                    if(parseFloat($scope.advanceModel.advanceDetails.used_amount)==0)
                    {
                    
                        $scope.showdeletePopup = true;
                    
                    }
                    else if($scope.advanceModel.advanceDetails.used_amount !=0)
                    {
                        swal("oops!", "Delete not allowed", "error");
                        $scope.showdeletePopup = false;
                    }
                
           // }
        //}
    };
     $scope.deleteAdvancePaymentItem = function ( )
    {
        if ($scope.isdeleteProgress == false)
        {
            $scope.isdeleteProgress = true;

            var getListParam = {};
            getListParam.id = $scope.selectedpaymentId;
            //getListParam.voucher_type = 'advance_invoice'
            //getListParam.type=1;
            adminService.deleteAdvancepayment(getListParam, getListParam.id).then(function (response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $state.go('app.advancepayment');
                }

                $scope.isdeleteProgress = false;
            });
                        
        }

    };
    $scope.closePopup = function ()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
    };
    }]);




