app.controller('expensetrackerEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.expenseTrackerModel = {
            "id": 0,
            "date": "",
            "amount": "",
            "is_active": 1,
            "list": [],
            "attachments": []
        }
        $scope.totalQty = 0;

        $scope.expenseTrackerModel.orderdate = new Date();
        $scope.pagePerCount = [10, 20, 50, 100];
        $scope.expenseTrackerModel.limit = $scope.pagePerCount[0];
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.expense_add_form != 'undefined' && typeof $scope.expense_add_form.$pristine != 'undefined' && !$scope.expense_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {

            $scope.expense_add_form.$setPristine();
            if (typeof $scope.edit_company_form != 'undefined')
            {
                $scope.updateExpenseDetails();
            }

            $scope.showEditButton[0] = false;
        }

        $scope.resetOrderCreate = function () {

            $scope.expenseTrackerModel.user = '';
        }


        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedTaskList = [];
        $scope.undeletedTaskList = [];
        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.orderDateOpen = false;
        $scope.dateFormat = "dd-MM-yyyy"
        $scope.expenseTrackerModel.date = new Date();
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.openDate = function (index) {

            if (index == 0)
            {
                $scope.orderDateOpen = true;
            }
        }

        $scope.showPopupFlg = false;
        $scope.closePopup = function ()
        {
            $scope.showPopupFlg = false;
            $scope.formReset();
            $state.go('app.purchaseorderlist');
        };
        $scope.showUploadPhotoPopup = false;
        $scope.showPhotoUploadPopup = function ($index) {
            $scope.taskIndex = $index;
            $scope.showUploadPhotoPopup = true;
            $scope.expenseTrackerModel.list[$scope.taskIndex].media = $scope.expenseTrackerModel.list[$index].media;
        };
        $scope.closePhotoUploadPopup = function () {

            $scope.showUploadPhotoPopup = false;
        };
        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data)
        {
            $scope.uploadedFileQueue = data;
        }
        ;
        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.expenseTrackerModel.list[$scope.taskIndex].media.length > index)
            {
                if (typeof $scope.expenseTrackerModel.list[$scope.taskIndex].media[index].id != 'undefined' && $scope.expenseTrackerModel.list[$scope.taskIndex].media[index].id != '')
                {
                    var imageUpdateParam = {};
                    imageUpdateParam.id = $scope.expenseTrackerModel.list[$scope.taskIndex].media[index].id;
                    var configData = {};
                    configData.imgIndex = index;
                    adminService.deleteImage(imageUpdateParam, configData).then(function (response) {

                        var config = response.config.config;
                        if (typeof config.imgIndex !== 'undefined')
                        {
                            var index = config.imgIndex;
                            $scope.expenseTrackerModel.list[$scope.taskIndex].media.splice(index, 1);
                            $scope.$broadcast('genericSuccessEvent', 'Image removed successfully.');
                        }

                    });
                } else
                {
                    $scope.expenseTrackerModel.list[$scope.taskIndex].media.splice(index, 1);
                    $scope.uploadedFileQueue.splice(index, 1);
                }
            }

        }
        ;

        $scope.saveImagePath = function ($index)
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                $scope.closePhotoUploadPopup();
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    var imageUpdateParam = {};
                    imageUpdateParam.path = $scope.uploadedFileQueue[i].urlpath;
                    imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                    $scope.expenseTrackerModel.list[$scope.taskIndex].media.push(imageUpdateParam);
                    $scope.expenseTrackerModel.attachments.push(imageUpdateParam);
                    $scope.closePhotoUploadPopup();
                }
                $scope.isImageSavingProcess = false;
            }
        }

        $scope.initvalidateTaskDataAndAddNewPromise = null;
        $scope.showEditButton = [];
        $scope.hasTaskEditIsProgress = false;

        $scope.initValidateTaskDataAndAddNew = function (index)
        {

            if ($scope.initvalidateTaskDataAndAddNewPromise != null)
            {

                $timeout.cancel($scope.initvalidateTaskDataAndAddNewPromise);
            }

            $scope.formValidator();
            $scope.initvalidateTaskDataAndAddNewPromise = $timeout(
                    function () {
                        $scope.validateTaskDataAndAddNew(index);
                    },
                    300);
        };

        $scope.showEditTask = function (index)
        {
            if (index > $scope.expenseTrackerModel.list.length)
            {
                return;
            }
            var editProgress = false;
            var editIndex = -1;
            for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
            {
                if (i == index)
                {
                    $scope.showEditButton[i] = false;
                    editProgress = true;
                    editIndex = i;
                } else
                {
                    $scope.showEditButton[i] = true;
                }
            }
            if (editProgress)
            {
                $scope.hasTaskEditIsProgress = true
            } else
            {
                $scope.hasTaskEditIsProgress = false;
            }
        };

        $scope.updateShowEditTaskOption = function ()
        {
            var editProgress = false;
            for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
            {
                if ($scope.showEditButton[i] != undefined && !$scope.showEditButton[i])
                {
                    editProgress = true;
                }
            }
            if (editProgress)
            {
                $scope.hasTaskEditIsProgress = true
            } else
            {
                $scope.hasTaskEditIsProgress = false;
            }
        };

        $scope.editTask = function (index)
        {
            $scope.showDetails[index] = true;
            for (var i = 0; i < $scope.expenseTrackerModel.list.length; i++)
            {
                if (i == index)
                {
                    $scope.showEditButton[index] = false;
                }
            }
            $scope.updateShowEditTaskOption();
            $timeout(function () {
                //angular.element("#task_"+$index+"_branch0_1").focus();
                //  console.log('msg in time out');
                $("#task_" + index + "_1").focus();
            }, 300);

        }

        $scope.validateTaskDataAndAddNew = function (index)
        {
            var formDataError = false;
            var taskContainer = window.document.getElementById('taskContainer_' + index);
            var errorCell = angular.element(taskContainer).find('.has-error').length;
            if (errorCell > 0)
            {
                formDataError = true;
            }
            var branchCountError = !$scope.checkCount(index);
            if (formDataError & branchCountError)
            {
                sweet.show('Oops...', 'Error in Task Data & Branch item count..', 'error');
                return;
            } else if (formDataError || branchCountError)
            {
                if (formDataError)
                {
                    sweet.show('Oops...', 'Error in task data', 'error');
                } else if (branchCountError)
                {
                    sweet.show('Oops...', 'Error in branch item count..', 'error');
                }

                return;
            }
            var taskLastRowIndex = $scope.expenseTrackerModel.list.length - 1;
            if (taskLastRowIndex == index)
            {
                $scope.addTask();

                var lastTaskRowIndex = $scope.expenseTrackerModel.list.length - 1;
                $timeout(function () {
                    //angular.element("#task_"+$index+"_branch0_1").focus();
                    //  console.log('msg in time out');
                    $("#task_" + lastTaskRowIndex + "_1").focus();
                }, 300);
            } else
            {
                $scope.showEditButton[index] = true;
                $scope.updateShowEditTaskOption();
            }
        }
        $scope.getAllowancelist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.ALLOWANCE_MASTER_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatAllowanceModel = function (model) {


            if (model !== null && model != undefined)
            {
                return model.name;
            }
            return  '';
        };



        $scope.hasLastRow = function (index) {


            if (index == $scope.expenseTrackerModel.list.length - 1)
            {
                return true;
            } else
            {
                return  false;
            }
        };
        $scope.deleteTask = function (deleteTaskIndex) {

            if (deleteTaskIndex < $scope.expenseTrackerModel.list.length)
            {
                $scope.expenseTrackerModel.list.splice(deleteTaskIndex, 1);

                $scope.updateShowEditTaskOption();
                $scope.updateInvoiceTotal();
            }
        };
        $scope.showTaskList = false;
        $scope.showTask = function ()
        {
            $scope.showTaskList = !$scope.showTaskList;
        }

        $scope.addTask = function (index)
        {
            var newRow = {
                "allowance_id": '',
                "role_id": "",
                "allowance_name": '',
                "amount": '',
                "comments": '',
                "is_active": '',
                "date": '',
                "attachment": [],
                "media": []
            }
            $scope.expenseTrackerModel.list.push(newRow);
            //  $scope.showEditTask($scope.expenseTrackerModel.list.length - 1);            
        };

        $scope.taskRenderInitHandler = function (index)
        {
            $timeout(function ()
            {
                $scope.updateBranchId(index);
            }, 300);
        }
        $scope.showDetails = [];
        $scope.toggleTaskBranch = function ($index)
        {
            if ($scope.showDetails[$index])
            {
                $scope.hideDetails($index);
            } else
            {
                $scope.displayDetails($index);
            }
        }

        $scope.displayDetails = function ($index)
        {
            $scope.showDetails[$index] = true;
        }
        $scope.hideDetails = function ($index)
        {
            $scope.showDetails[$index] = false;
        }

        $scope.formValidator = function ()
        {
            //performace issue in form validator bug
            if ($scope.expense_add_form.$submitted)
            {
                $timeout(function () {
                    $scope.expense_add_form.$submitted = false;
                }, 0);
                $timeout(function () {
                    $scope.expense_add_form.$setSubmitted();
                }, 0);
            } else
            {
                $timeout(function () {
                    $scope.expense_add_form.$setSubmitted();
                }, 0);
            }
        }

        $scope.initCreatePurchaseOrderTimeoutPromise = null;
        $scope.initCreatePurchaseOrder = function ()
        {
            if ($scope.initCreatePurchaseOrderTimeoutPromise != null)
            {

                $timeout.cancel($scope.initCreatePurchaseOrderTimeoutPromise);
            }

            $scope.formValidator();
            $scope.initCreatePurchaseOrderTimeoutPromise = $timeout($scope.createExpense, 300);
        }
        $scope.updateExpenseDetails = function ()
        {

            $scope.expenseTrackerModel.allowanceInfo = {
                id: $scope.expenseTrackerModel.expenselist.allowance_id,
                name: $scope.expenseTrackerModel.expenselist.name
            }
            $scope.expenseTrackerModel.amount = $scope.expenseTrackerModel.expenselist.amount;
            $scope.expenseTrackerModel.date = $scope.expenseTrackerModel.expenselist.date;
            $scope.expenseTrackerModel.comments = $scope.expenseTrackerModel.expenselist.comments;
            $scope.expenseTrackerModel.status = $scope.expenseTrackerModel.expenselist.status;
            $scope.expenseTrackerModel.date = $scope.expenseTrackerModel.expenselist.date;
            $scope.expenseTrackerModel.attachments = $scope.expenseTrackerModel.expenselist.attachment;
            for (var i = 0; i < $scope.expenseTrackerModel.attachments.length; i++)
            {
                $scope.expenseTrackerModel.attachments[i].url = $scope.expenseTrackerModel.attachments[i].url;
            }

        }
        $scope.createExpense = function () {
            if (!$scope.isDataSavingProcess)
            {

                if ($scope.expenseTrackerModel.list.length != 0)
                {
                    $scope.isDataSavingProcess = true;
                    var createExpenseParam = {};
                    createExpenseParam.id = $stateParams.id;
                    createExpenseParam.allowance_id = $scope.expenseTrackerModel.allowanceInfo.id;
                    createExpenseParam.allowance_name = $scope.expenseTrackerModel.allowanceInfo.name;
                    createExpenseParam.amount = $scope.expenseTrackerModel.amount;
                    createExpenseParam.comments = $scope.expenseTrackerModel.expenselist.comments;
                    createExpenseParam.is_active = 1;
                    createExpenseParam.date = utilityService.changeDateToSqlFormat($scope.expenseTrackerModel.expenselist.date, $scope.dateFormat);
                    createExpenseParam.attachment = [];
                    for (var k = 0; k < $scope.expenseTrackerModel.attachments.length; k++)
                    {
                        var mediaDetail = {};
                        mediaDetail.url = $scope.expenseTrackerModel.attachments[k].url;
                        createExpenseParam.attachment.push(mediaDetail);
                    }
                    adminService.modifyExpenseTracker(createExpenseParam, createExpenseParam.id).then(function (response) {
                        if (response.data.success == true)
                        {
                            $scope.formReset();
                            $state.go('app.expensetracker');
                        }
                    });
                } else
                {
                    sweet.show('Oops...', 'Add Tasks', 'error');
                }
            }
        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.getList = function () {
            var getListParam = {};
            getListParam.start = ($scope.expenseTrackerModel.currentPage - 1) * $scope.expenseTrackerModel.limit;
            getListParam.limit = $scope.expenseTrackerModel.limit;
            getListParam.is_active = 1;
            getListParam.id = $stateParams.id;
            $scope.expenseTrackerModel.isLoadingProgress = true;
            var headers = {};
            headers['screen-code'] = 'expensetracker';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getExpenseTrackerList(getListParam, configOption, headers).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.expenseTrackerModel.expenselist = data[0];
                    $scope.expenseTrackerModel.expenselist.newdate = $scope.expenseTrackerModel.expenselist.date;
                    $scope.expenseTrackerModel.expenselist.date = utilityService.parseDateToStr($scope.expenseTrackerModel.expenselist.newdate, $scope.dateFormat);
                    $scope.expenseTrackerModel.total = data.total;
                    $scope.showCheckAddPopup = false;
                    $scope.expenseTrackerModel.selectall = '';
                    $scope.updateExpenseDetails();
                }
                $scope.expenseTrackerModel.isLoadingProgress = false;
            });
        };

        $scope.selectAll = function ()
        {
            $scope.showCheckAddPopup = true;
            if ($scope.expenseTrackerModel.selectall)
            {
                $scope.showCheckAddPopup = true;
                $scope.expenseTrackerModel.expenselist.is_select = true;

            } else
            {
                $scope.showCheckAddPopup = false;
                $scope.expenseTrackerModel.expenselist.is_select = false;
            }
        }

        $scope.showStatusAddPopup = false;
        $scope.showStatusPopup = function ()
        {
            $scope.showStatusAddPopup = true;
        }
        $scope.closeStatusPopup = function ()
        {
            $scope.showStatusAddPopup = false;
        }
        $scope.showPaidAddPopup = false;
        $scope.showPaidPopup = function ()
        {
            $scope.showPaidAddPopup = true;
            $scope.showStatusAddPopup = false;
        }
        $scope.closePaidPopup = function ()
        {
            $scope.showPaidAddPopup = false;
        }
        $scope.showCheckAddPopup = false;

        $scope.statusUpdate = function (status) {
            $scope.isDataSavingProcess = true;
            $scope.status = status;
            var statusParam = [];
            var savestatusParam = {};
            savestatusParam.id = $scope.expenseTrackerModel.expenselist.id;
            savestatusParam.status = $scope.status;
            statusParam.push(savestatusParam);
            adminService.statusSave(statusParam, $scope.status).then(function (response) {

                if (response.data.success == true)
                {
                    $scope.getList();
                    $scope.formReset();
                    $state.go('app.expensetrackerEdit');
                }
                $scope.formReset();
                $scope.isDataSavingProcess = false;
                $scope.showStatusAddPopup = false;
                $scope.showPaidAddPopup = false;
            });
        };

        $scope.getList();

        $timeout(function () {
            $("#purchaseorder_1").focus();
        }, 300);
        $scope.addTask();

    }]);




