
app.controller('attendanceListLocationCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'NgMap', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $stateParams, $timeout, NgMap) {

            $rootScope.getNavigationBlockMsg = null;
            $scope.attendanceModel = {
                  currentPage: 1,
                  total: 0,
                  limit: 4,
                  list: [],
                  total_duration: '',
                  serverList: null,
                  isLoadingProgress: false,
                  isSearchLoadingProgress: false
            };

            $scope.adminService = adminService;
            $scope.pagePerCount = [50, 100];
            $scope.attendanceModel.limit = $scope.pagePerCount[0];
            $scope.fromDateOpen = false;
            $scope.toDateOpen = false;
            $scope.dateFormat = $rootScope.appConfig.date_format;
            $scope.currentDate = new Date();
            $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
            $scope.validationFactory = ValidationFactory;


            $scope.searchFilter = {
                  fromdate: '',
                  todate: '',
                  employeeInfo: ''

            };
            $scope.openDate = function (index) {

                  if (index === 0)
                  {
                        $scope.fromDateOpen = true;
                  } else if (index === 1)
                  {
                        $scope.toDateOpen = true;
                  }
            };
            $scope.clearFilters = function ()
            {
                  $scope.searchFilter = {
                        fromdate: '',
                        todate: '',
                        employeeInfo: ''
                  };

            }
            $scope.dateOptions = {
                  formatYear: 'yy',
                  startingDay: 1,
                  showWeeks: false
            };
            $scope.validateDateFilterData = function ()
            {
                  var retVal = false;
                  if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
                  {
                        retVal = true;
                  }
                  return retVal;
            };
            $scope.searchFilterValue = "";
            $scope.latlng = '';
            $scope.isTypeLoaded = false;



            $scope.isLoadedvm1 = false;
           
            $scope.initUpdateMarkerTimeoutPromise = null;

            $scope.initUpdateMarkerDetail = function ()
            {
                  if ($scope.initUpdateMarkerTimeoutPromise != null)
                  {
                        $timeout.cancel($scope.initUpdateMarkerTimeoutPromise);
                  }
                  if ($scope.isLoadedvm1 && $scope.isTypeLoaded)
                  {
                        $scope.vm1.positions = [];
                        var newPos = {};
                        if ($scope.latlng.includes(','))
                        {
                              var latlng = $scope.latlng.split(',');
                              newPos.lat = parseFloat(latlng[0]);
                              newPos.lng = parseFloat(latlng[1]);
                              $scope.actCenterLat = newPos.lat;
                              $scope.actCenterLng = newPos.lng;
                        }
                        $scope.vm1.positions.push(newPos);


                  } else
                  {
                        $scope.initUpdateMarkerTimeoutPromise = $timeout($scope.initUpdateMarkerDetail, 300);
                  }
            };
            
             $scope.vm1 = this;

            NgMap.getMap({
                  id: 'vm1'
            }).then(function (map) {
                  $scope.vm1.map = map;
            
                  $scope.isLoadedvm1 = true;
                  $scope.vm1.positions = [{
                              lat: 37.7699298,
                              lng: -122.4469157
                        }];

            });

            $scope.checkLatLng = function ()
            {
                  if ($stateParams.value != 'undefined' && $stateParams.value != '')
                  {
                        $scope.isTypeLoaded = true;
                        $scope.latlng = $stateParams.value;
                        $scope.initUpdateMarkerDetail();
                  }
            }
            $scope.checkLatLng();


      }]);
















