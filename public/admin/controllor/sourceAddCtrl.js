

app.controller('sourceAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.sourceModel = {
            id: '',
            name: '',
            isActive: true,
            level: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_source_form != 'undefined' && typeof $scope.add_source_form.$pristine != 'undefined' && !$scope.add_source_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_source_form.$setPristine();
            if (typeof $scope.add_source_form != 'undefined')
            {
                $scope.sourceModel.name = "";
                $scope.sourceModel.comments = "";
                $scope.sourceModel.isActive = true;
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.createsource = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addSourceParam = {};
            var headers = {};
            headers['screen-code'] = 'source';
            addSourceParam.id = 0;
            addSourceParam.name = $scope.sourceModel.name;
            addSourceParam.comments = $scope.sourceModel.comments;
            addSourceParam.is_active = $scope.sourceModel.isActive == true ? 1 : 0;

            adminService.addSource(addSourceParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.source');
                }
                $scope.isDataSavingProcess = false;
            });
        };

    }]);
