app.controller('advancepaymentCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory','$state','customerFactory', function ($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory,$state,customerFactory) {

    $rootScope.getNavigationBlockMsg = null;
    $scope.advanceModel = {
        currentPage: 1,
        total: 0,
        limit: 4,
        list: [],
        status: '',
        isLoadingProgress: true
    };
    $scope.validationFactory = ValidationFactory;
    $scope.pagePerCount = [50, 100];
    $scope.advanceModel.limit = $scope.pagePerCount[0];
    $scope.adminService = adminService;
    $scope.customerFilter = customerFactory.get();
    customerFactory.set('');
    $scope.searchFilter =
    {
        customerInfo: {},
        status: '',
        from_date : '',
        to_date : ''
    };

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function ()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    }
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.openDate = function(index) {

        if (index == 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index == 1)
        {
            $scope.toDateOpen = true;
        }

    };
    $scope.refreshScreen = function ()
    {
        $scope.searchFilter =
        {
            customerInfo: {},
            status: '',
            from_date : '',
            to_date : ''
        };
        $scope.initTableFilter();
    }
    $scope.selectedpaymentId = '';
    $scope.showdeletePopup = false;
    $scope.isdeleteProgress = false;
    $scope.showPopup = function (id)
    {
        //$scope.showdeletePopup = false;
        $scope.selectedpaymentId = id;
        for(var i=0; i<$scope.advanceModel.list.length;i++)
        {
            if($scope.selectedpaymentId==$scope.advanceModel.list[i].id)
            {
        
                if($scope.advanceModel.list[i].voucher_type=='advance_invoice')
                {
                    if($scope.advanceModel.list[i].status=='used')
                    {
                    
                        swal("oops!", "Delete not allowed", "error");
                        $scope.showdeletePopup = false;
                    
                    }
                    else if($scope.advanceModel.list[i].status=='available')
                    {
                        $scope.showdeletePopup = true;
                    }
                }
                else
                {
                    $scope.showdeletePopup=true;
                }
            }
        //            else
        //            {
        //                $scope.showdeletePopup = false;
        //            }
         
        }
    // $scope.showdeletePopup = true;
           
    };

    $scope.closePopup = function ()
    {
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
    };

    $scope.deleteAdvancePaymentItem = function ( )
    {
        if ($scope.isdeleteProgress == false)
        {
            $scope.isdeleteProgress = true;

            var getListParam = {};
            getListParam.id = $scope.selectedpaymentId;
            //getListParam.voucher_type = 'advance_invoice';
           
            var headers = {};
            headers['screen-code'] = 'advancepayment';
            adminService.deleteAdvancepayment(getListParam, getListParam.id, headers).then(function (response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getList();
                }

                $scope.isdeleteProgress = false;
            });
                        
        }

    };

    $scope.$watch('searchFilter.customerInfo', function (newVal, oldVal)
    {
        if (typeof newVal == 'undefined' || newVal == '')
        {
            $scope.initTableFilter();
        }
    });

    $scope.getCustomerList = function (val)
    {
        var autosearchParam = {};
        autosearchParam.search = val;
        return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = [];
            if (data.length > 0)
            {
                for (var i = 0; i < data.length; i++)
                {
                    if (data[i].is_sale == '1' || data[i].is_sale == 1)
                    {
                        hits.push(data[i]);
                    }
                }
            }
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    if($scope.customerFilter != '' && $scope.customerFilter != null && $scope.customerFilter != undefined)
    {    
        $scope.searchFilter.customerInfo = $scope.customerFilter;
    }
    $scope.formatCustomerModel = function (model)
    {
        if (model != null)
        {
            if (model.fname != undefined && model.phone != undefined && model.email != undefined && model.company != undefined)
            {
                return model.fname + '(' + model.phone + ',' + model.email + ',' + model.company + ')';
            } else if (model.fname != undefined && model.phone != undefined && model.company != undefined)
{
                return model.fname + '(' + model.phone + ')';
            }
            else
            {
                return model.fname;
            }    
        }
        return  '';
    };

    $scope.getList = function ()
    {
        $scope.advanceModel.isLoadingProgress = true;
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'advancepayment';
        getListParam.type=1;
        //getListParam.from_date=$scope.searchFilter.from_date;
        //getListParam.to_date=$scope.searchFilter.to_date;
        var configOption = adminService.handleOnlyErrorResponseConfig;

        if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
        {
            getListParam.customer_id = $scope.searchFilter.customerInfo.id;
        } else
        {
            getListParam.customer_id = '';
        }
         if ($scope.searchFilter.to_date != '' && ($scope.searchFilter.from_date == null || $scope.searchFilter.from_date == ''))
            {
                if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, 'yyyy-MM-dd');
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, 'yyyy-MM-dd');
                getListParam.from_date = getListParam.to_date;
            }
            else if ($scope.searchFilter.from_date != '' && ($scope.searchFilter.to_date == null || $scope.searchFilter.to_date == ''))
            {
                if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, 'yyyy-MM-dd');
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, 'yyyy-MM-dd');
                getListParam.to_date = getListParam.from_date;
            }
            else
            {
                if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, 'yyyy-MM-dd');
                }
                if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, 'yyyy-MM-dd');
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, 'yyyy-MM-dd');
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, 'yyyy-MM-dd');
            }

        getListParam.start = ($scope.advanceModel.currentPage - 1) * $scope.advanceModel.limit;
        getListParam.limit = $scope.advanceModel.limit;
        getListParam.is_active = 1;
        //getListParam.voucher_type = 'advance_invoice';
        getListParam.status = $scope.searchFilter.status;
        adminService.getAdvancepaymentList(getListParam, configOption, headers).then(function (response)
        {

            var data = response.data;

            if (data.success == true)
            {
                $scope.advanceModel.list = data.list;
                if ($scope.advanceModel.list.length > 0)
                {
                    for (var i = 0; i < $scope.advanceModel.list.length; i++)
                    {
                        $scope.advanceModel.list[i].newdate = utilityService.parseStrToDate($scope.advanceModel.list[i].date);
                        $scope.advanceModel.list[i].date = utilityService.parseDateToStr($scope.advanceModel.list[i].newdate, $rootScope.appConfig.date_format);
                        $scope.advanceModel.list[i].total_amount = utilityService.changeCurrency($scope.advanceModel.list[i].total_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.advanceModel.list[i].advance_amount = parseFloat(utilityService.changeCurrency($scope.advanceModel.list[i].advance_amount, $rootScope.appConfig.thousand_seperator)).toFixed(2);

                    }
                }
                $scope.advanceModel.total = data.total;
            }
            $scope.advanceModel.isLoadingProgress = false;
        });
    };
    $scope.getList();
    $scope.viewDirect = function(id)
    {
        if ($rootScope.appConfig.invoice_print_template === 't1')
        {
            $state.go('app.invoiceView1', {
                'id': id
            }, {
                'reload': true
            });
        }
        else if ($rootScope.appConfig.invoice_print_template === 't2')
        {
            $state.go('app.invoiceView2', {
                'id': id
            }, {
                'reload': true
            });
        }
        else
        {
            $state.go('app.invoiceView1', {
                'id': id
            }, {
                'reload': true
            });
        }
    };
}]);




