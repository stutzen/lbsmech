

app.controller('productEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', '$window', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams, $window) {

        $scope.productModel = {
            id: '',
            name: '',
            sku: '',
            description: '',
            uomList: [],
            salesPrice: '',
            category_id: '',
            is_active: '',
            is_purchase: '',
            is_sale: '',
            uom: '',
            isLoadingProgress: false,
            minstock: '',
            openstock: '',
            hasinventory: '',
            hsncode: '',
            mrp_price: '',
            attachments: [],
            productdetail: [],
            type: '',
            width: '',
            thickness: '',
            diameter: '',
            af: '',
            square: '',
            prodlength: '',
            color: '',
            gauge: '',
            brand: '',
            pitch: ''
        }
        $scope.productDetail = {};
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.product_edit_form != 'undefined' && typeof $scope.product_edit_form.$pristine != 'undefined' && !$scope.product_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.product_edit_form.$setPristine();
            if (typeof $scope.product_edit_form != 'undefined')
            {
                $scope.productModel.name = "";
                $scope.productModel.sku = "";
                $scope.productModel.id = "";
                $scope.productModel.salesPrice = '';
                $scope.productModel.description = "";
                $scope.productModel.is_active = '';
                $scope.productModel.is_purchase = '';
                $scope.productModel.is_sale = '';
                $scope.productModel.has_inventory = '';
                $scope.productModel.hsncode = '';
                // $scope.productModel.uom = '';
                $scope.productModel.mrp_price = '';
                $scope.productModel.prodlength = '';
                $scope.productModel.color = '';
                $scope.productModel.gauge = '';
                $scope.productModel.brand = '';
                $scope.productModel.pitch = '';
            }
            $scope.updateProductInfo();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isUOMLoaded && $scope.isCategoryLoaded)
            {
                $scope.updateProductInfo();
                //$scope.productModel.isLoadingProgress = false;
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $scope.updateProductInfo = function()
        {
            //$scope.productModel = $scope.productModel.list;
            $scope.productModel.is_active = 1;
            //$scope.productModel.is_active = $scope.productModel.is_active == 1 ? true : false;
            $scope.productModel.is_sale = $scope.productDetail.is_sale == 1 ? true : false;
            $scope.productModel.is_purchase = $scope.productDetail.is_purchase == 1 ? true : false;
            $scope.productModel.openstock = $scope.productDetail.opening_stock;
            $scope.productModel.hasinventory = $scope.productDetail.has_inventory == 1 ? true : false;
            $scope.productModel.minstock = $scope.productDetail.min_stock_qty;
            $scope.productModel.name = $scope.productDetail.name;
            $scope.productModel.category_id = $scope.productDetail.category_id + '';
            $scope.productModel.sku = $scope.productDetail.sku;
            $scope.productModel.uom = $scope.productDetail.uom;
            $scope.productModel.sales_price = $scope.productDetail.sales_price;
            $scope.productModel.mrp_price = $scope.productDetail.mrp_price;
            $scope.productModel.description = $scope.productDetail.description;
            $scope.productModel.hsncode = $scope.productDetail.hsn_code;
            $scope.productModel.attachments = $scope.productDetail.attachment;
            for (var i = 0; i < $scope.productModel.attachments.length; i++)
            {
                $scope.productModel.attachments[i].path = $scope.productModel.attachments[i].url;
            }
            $scope.productModel.productdetail = $scope.productDetail.detail;
            for (var j = 0; j < $scope.productModel.productdetail.length; j++)
            {
                $scope.productModel.type = $scope.productModel.productdetail[j].type+'';
                $scope.productModel.width = $scope.productModel.productdetail[j].width;
                $scope.productModel.thickness = $scope.productModel.productdetail[j].thickners;
                $scope.productModel.diameter = $scope.productModel.productdetail[j].dia;
                $scope.productModel.af = $scope.productModel.productdetail[j].af;
                $scope.productModel.square = $scope.productModel.productdetail[j].square;
                $scope.productModel.prodlength = $scope.productModel.productdetail[j].length;
                $scope.productModel.color = $scope.productModel.productdetail[j].color;
                $scope.productModel.gauge = $scope.productModel.productdetail[j].gauge;
                $scope.productModel.brand = $scope.productModel.productdetail[j].brand;
                $scope.productModel.pitch = $scope.productModel.productdetail[j].pitch;
            }
            $scope.productModel.isLoadingProgress = false;
        }


        $scope.getProductInfo = function()
        {
            $scope.productModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var productListParam = {};
                productListParam.id = $stateParams.id;
                productListParam.start = 0;
                productListParam.limit = 1;
                $scope.productDetail = {};
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getProductList(productListParam, configOption).then(function(response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.productDetail = data.list[0];

                    }
                    $scope.getCUOMList();
                    $scope.getcategoryList();
                    $scope.initUpdateDetail();

                });
            }
        };
        $scope.isCategoryLoaded = false;
        $scope.getcategoryList = function()
        {
            var getListParam = {};
            getListParam.category_id = "";
            getListParam.name = "";
            getListParam.id = "";
            getListParam.start = ($scope.productModel.currentPage - 1) * $scope.productModel.limit;
            getListParam.limit = $scope.productModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.productModel.categorylist = data.list;
                $scope.isCategoryLoaded = true;
                $scope.isDataSavingProcess = false;
            });

        };
        $scope.getProductInfo();

        $scope.isUOMLoaded = false;
        $scope.getCUOMList = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.start = ($scope.productModel.currentPage - 1) * $scope.productModel.limit;
            getListParam.limit = $scope.productModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUom(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.productModel.uomList = data.list;
                $scope.isUOMLoaded = true;
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.getProductTypeList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'producttype';
            getListParam.is_active = 1;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getProductTypeList(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.productModel.productTypeList = data;
                }
            });

        };
        $scope.ShowAlert = function() {
            if ($scope.productModel.sales_price > $scope.productModel.mrp_price)
            {
                $window.alert("Sales Price must be less than MRP Price");

                $scope.productModel.sales_price = '';
                $scope.isDataSavingProcess = false;
            }
        }
        //$scope.getProductInfo();
        /*
         *  It used to format the speciality model value in UI
         */
        $scope.editProductInfo = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addProductParam = {};
            var detailProductParam = {};
            var headers = {};
            headers['screen-code'] = 'product';
            //     addProductParam.id = 0;
            addProductParam.name = $scope.productModel.name;
            addProductParam.sku = $scope.productModel.sku;
            addProductParam.sales_price = parseFloat($scope.productModel.sales_price);
            addProductParam.mrp_price = parseFloat($scope.productModel.mrp_price);
            addProductParam.description = $scope.productModel.description;
            addProductParam.category_id = $scope.productModel.category_id;
            //      addProductParam.uom = $scope.productModel.uom;
            addProductParam.type = "Product";
            if ($scope.productModel.uom != "")
            {
                for (var loopIndex = 0; loopIndex < $scope.productModel.uomList.length; loopIndex++)
                {
                    if ($scope.productModel.uom == $scope.productModel.uomList[loopIndex].name)
                    {
                        addProductParam.uom_id = $scope.productModel.uomList[loopIndex].id;
                        break;
                    }
                }
            } else
            {
                addProductParam.uom_id = "";
            }

            addProductParam.is_active = $scope.productModel.is_active ? 1 : 0;
            addProductParam.is_sale = $scope.productModel.is_sale ? 1 : 0;
            addProductParam.is_purchase = $scope.productModel.is_purchase ? 1 : 0;
            addProductParam.min_stock_qty = $scope.productModel.minstock;
            addProductParam.opening_stock = $scope.productModel.openstock;
            addProductParam.has_inventory = $scope.productModel.hasinventory == true ? 1 : 0;
            addProductParam.hsn_code = $scope.productModel.hsncode;
            addProductParam.attachment = [];
            addProductParam.detail = [];
            for (var i = 0; i < $scope.productModel.attachments.length; i++)
            {
                var imageParam = {};
                imageParam.id = $scope.productModel.attachments[i].id;
                imageParam.url = $scope.productModel.attachments[i].url;
                imageParam.ref_id = $scope.productModel.attachments[i].ref_id;
                imageParam.type = $scope.productModel.attachments[i].type;
                addProductParam.attachment.push(imageParam);
            }
            for (var j = 0; j < $scope.productModel.productdetail.length; j++)
            {
                detailProductParam.type = $scope.productModel.type;
                detailProductParam.width = $scope.productModel.width;
                detailProductParam.thickners = $scope.productModel.thickness;
                detailProductParam.dia = $scope.productModel.diameter;
                detailProductParam.af = $scope.productModel.af;
                detailProductParam.square = $scope.productModel.square;
                detailProductParam.length = $scope.productModel.prodlength;
                detailProductParam.color = $scope.productModel.color;
                detailProductParam.gauge = $scope.productModel.gauge;
                detailProductParam.brand = $scope.productModel.brand;
                detailProductParam.pitch = $scope.productModel.pitch;
                addProductParam.detail.push(detailProductParam);
            }
            adminService.editProduct(addProductParam, $stateParams.id, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.productList');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.showPhotoGalaryPopup = false;
        $scope.showPopup = function(index, attachindex)
        {
            if (index == 'morefileupload')
            {
                $scope.isImageSavingProcess = false;
                $scope.showUploadMoreFilePopup = true;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = true;
            }
        }
        $scope.closePopup = function(index)
        {
            if (index == 'morefileupload')
            {
                $scope.showUploadMoreFilePopup = false;
            } else if (index === 'photogalary')
            {
                $scope.showPhotoGalaryPopup = false;
            }
        }

        $scope.uploadedFileQueue = [];
        $scope.uploadedFileCount = 0;
        $scope.isImageSavingProcess = false;
        $scope.$on("updateFileUploadEvent", updateFileUploadEventHandler);
        function updateFileUploadEventHandler(event, data, category, imgcaption, index)
        {
            $scope.uploadedFileQueue = data;
            if (typeof category != undefined && category != null && category != '')
            {
                $scope.uploadedFileQueue[index].category = category;
            }
            if (typeof imgcaption != undefined && imgcaption != null && imgcaption != '')
            {
                $scope.uploadedFileQueue[index].imgcaption = imgcaption;
            }
            console.log('$scope.uploadedFileQueue');
            console.log($scope.uploadedFileQueue);
        }

        $scope.$on("deletedUploadFileEvent", deletedUploadFileEventHandler);
        function deletedUploadFileEventHandler(event, index)
        {
            if ($scope.showUploadMoreFilePopup)
            {
                if ($scope.productModel.attachments.length > index)
                {
                    $scope.productModel.attachments.splice(index, 1);
                }
            }
        }


        $scope.deleteImage = function($index)
        {
            $scope.productModel.attachments.splice($index, 1);
        }

        $scope.saveImagePath = function()
        {
            if ($scope.uploadedFileQueue.length == 0)
            {
                if ($scope.showUploadMoreFilePopup)
                {
                    $scope.closePopup('morefileupload');
                }
            }
            var hasUploadCompleted = true;
            for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
            {
                if (!$scope.uploadedFileQueue[i].isSuccess)
                {
                    hasUploadCompleted = false;
                    $scope.$broadcast('genericErrorEvent', 'Image upload is in progress.');
                    break;
                }
            }
            if (hasUploadCompleted && $scope.uploadedFileQueue.length > 0)
            {
                $scope.uploadedFileCount = $scope.uploadedFileQueue.length;
                $scope.isImageSavingProcess = true;
                for (var i = 0; i < $scope.uploadedFileQueue.length; i++)
                {
                    //               $scope.productModel.attachments.push($scope.uploadedFileQueue[i]);
                    var imageUpdateParam = {};
                    imageUpdateParam.id = 0;
                    imageUpdateParam.url = $scope.uploadedFileQueue[i].urlpath;
                    imageUpdateParam.ref_id = $stateParams.id;
                    imageUpdateParam.type = 'product';
                    imageUpdateParam.comments = '';
                    imageUpdateParam.signedUrl = $scope.uploadedFileQueue[i].signedUrl;
                    imageUpdateParam.path = $scope.uploadedFileQueue[i].urlpath;
                    imageUpdateParam.urlpath = $scope.uploadedFileQueue[i].urlpath;
                    $scope.productModel.attachments.push(imageUpdateParam);
                    if (i == $scope.uploadedFileQueue.length - 1)
                    {
                        $scope.closePopup('morefileupload');
                    }
                }
            } else
            {
                $scope.closePopup('morefileupload');
            }
        };
        $scope.getCUOMList();
        $scope.getcategoryList();
        $scope.getProductTypeList();
// $scope.getcategoryList();
    }]);



