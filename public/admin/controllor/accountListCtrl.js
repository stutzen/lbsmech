


app.controller('accountListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.accountModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            serverList: null,
            isLoadingProgress: false,
            withdrawamt: '',
            reason: '',
            category: '',
            type: ''
        };
        $scope.pagePerCount = [50, 100];
        $scope.accountModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = 'dd-MM-yyyy';
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        }
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            type: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.showSaveWithdrawPopup = false;
        $scope.showPopup = function() {

            $scope.showSaveWithdrawPopup = true;
            $scope.accountModel.reason = '';
            $scope.accountModel.withdrawamt = '';

        };

        $scope.closePopup = function() {

            $scope.showSaveWithdrawPopup = false;

        };

        $scope.issaveWithdrawLoadingProgress = false;
        $scope.saveWithdraw = function() {

            var saveWithdrawParam = {};
            saveWithdrawParam.id = 0;
            saveWithdrawParam.particulars = $scope.accountModel.reason;
            saveWithdrawParam.credit = parseFloat($scope.accountModel.withdrawamt).toFixed(2);

            $scope.issaveWithdrawLoadingProgress = true;
            adminService.saveWithdraw(saveWithdrawParam).then(function(response) {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.issaveWithdrawLoadingProgress = false;
                    $scope.closePopup();
                    $state.go('app.accountlist');
                    $scope.getList();
                }

            });

        };


        $scope.getList = function() {

            $scope.accountModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.fromDate = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
            getListParam.toDate = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');
            if (getListParam.fromDate === '' || getListParam.toDate === '')
            {
                if (getListParam.fromDate !== '')
                {
                    getListParam.toDate = getListParam.fromDate;
                }
                if (getListParam.toDate !== '')
                {
                    getListParam.fromDate = getListParam.toDate;
                }
            }
            getListParam.sourceType = $scope.searchFilter.type;
            getListParam.category = $scope.searchFilter.category;
            getListParam.start = ($scope.accountModel.currentPage - 1) * $scope.accountModel.limit;
            getListParam.limit = $scope.accountModel.limit;

            adminService.getAccountlist(getListParam).then(function(response) {
                var data = response.data;
                $scope.accountModel.list = data.list;
                $scope.accountModel.total = data.total;
                $scope.accountModel.isLoadingProgress = false;
            });

        };

        

        $scope.getList();
    }]);








