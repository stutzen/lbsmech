

app.controller('productTypeAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

        $scope.productTypeModel = {
            id: '',
            name: "",
            code: '',
            comments: ''
        };

        $scope.validationFactory = ValidationFactory;


        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.product_type_form != 'undefined' && typeof $scope.product_type_form.$pristine != 'undefined' && !$scope.product_type_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            $scope.product_type_form.$setPristine();
            $scope.productTypeModel.name = '';
            $scope.productTypeModel.comments = '';
            $scope.productTypeModel.code = '';
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.createProductType = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var getListParam = {};
                var headers = {};
                headers['screen-code'] = 'producttype';                
                getListParam.name = $scope.productTypeModel.name;
                getListParam.code = $scope.productTypeModel.code;
                getListParam.comments = $scope.productTypeModel.comments;
                adminService.addProductType(getListParam, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.producttype');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

    }]);




