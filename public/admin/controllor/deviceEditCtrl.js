




app.controller('deviceEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.deviceModel = {
            "name": "",
            "id": '',
            "devicecode": "",
            "shortcode": "",
            "comments": "",
            "isActive": true,
            "isDataSavingProcess": false



        }

        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.device_add_form != 'undefined' && typeof $scope.device_add_form.$pristine != 'undefined' && !$scope.device_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.device_add_form.$setPristine();
            $scope.updateDeviceInfo();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTabletCreate = function() {

            $scope.deviceModel.user = '';

        }
        $scope.modifyDevice = function() {

            $scope.isDataSavingProcess = true;
            var modifyDeviceParam = {};

            modifyDeviceParam.id = $scope.deviceDetail.id;
            modifyDeviceParam.name = $scope.deviceModel.name;
            modifyDeviceParam.device_code = $scope.deviceModel.devicecode;
            modifyDeviceParam.short_code = $scope.deviceModel.shortcode;
            modifyDeviceParam.comments = $scope.deviceModel.comments;
            
            adminService.modifyDevice(modifyDeviceParam,$scope.deviceDetail.id).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.isDataSavingProcess = false;
                    $scope.formReset();
                    $state.go('app.device');
                }
            });
        };

        $scope.updateDeviceInfo = function()
        {
            if ($scope.deviceDetail != null)
            {
                $scope.deviceModel.id = $scope.deviceDetail.id;
                $scope.deviceModel.name = $scope.deviceDetail.name;
                $scope.deviceModel.devicecode = $scope.deviceDetail.deviceCode;
                $scope.deviceModel.shortcode = $scope.deviceDetail.shortCode;

            }
        }



        $scope.getDeviceInfo = function() {

            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                getListParam.deviceCode = $stateParams.id;
                adminService.getdeviceList(getListParam).then(function(response) {
                    var data = response.data.data;
                    if (data.total != 0)
                    {
                        $scope.deviceDetail = data[0];
                        $scope.updateDeviceInfo();
                    }

                });
            }
        };

        $scope.getDeviceInfo();



    }]);




