app.controller('amcListCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$filter', 'Auth', '$timeout', '$httpService', 'APP_CONST', 'ValidationFactory', '$localStorage', 'customerFactory', function($scope, $rootScope, adminService, utilityService, $filter, Auth, $timeout, $httpService, APP_CONST, ValidationFactory, $localStorage, customerFactory) {
        $scope.amcListModel =
                {
                    currentPage: 1,
                    total: 0,
                    limit: 50,
                    list: [],
                    serverList: null,
                    isLoadingProgress: false
                };
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;
        $scope.customerFilter = customerFactory.get();
        customerFactory.set('');
        $scope.pagePerCount = [50, 100];
        $scope.amcListModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            customerInfo: '',
            status: '',
            amcId: '',
            fromdate: '',
            todate: ''
        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                customerInfo: '',
                amcId: '',
                status: '',
                fromdate: '',
                todate: ''
            };
            $scope.initTableFilter();
        }
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }
        }
        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.showdeletePopup = false;
        $scope.selectedamcId = '';
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectedamcId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };
        $scope.deleteAmcInfo = function( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectedamcId;
                var headers = {};
                headers['screen-code'] = 'amc';
                adminService.deleteAmc(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.getList();
                        $scope.closePopup();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };
        if ($scope.customerFilter != '' && $scope.customerFilter != null && $scope.customerFilter != undefined)
        {
            $scope.searchFilter.customerInfo = $scope.customerFilter;
            $localStorage["amc_list_form-amc_customer_dropdown"] = $scope.searchFilter.customerInfo;
        }
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };
        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'amc';
            getListParam.id = $scope.searchFilter.amcId;
            if ($scope.searchFilter.goodsDate != null && $scope.searchFilter.goodsDate != '')
            {
                if ($scope.searchFilter.goodsDate != null && typeof $scope.searchFilter.goodsDate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.goodsDate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
            if ($scope.searchFilter.companyInfo != null && typeof $scope.searchFilter.companyInfo != 'undefined' && typeof $scope.searchFilter.companyInfo.id != 'undefined')
            {
                getListParam.company_id = $scope.searchFilter.companyInfo.id;
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            getListParam.status = $scope.searchFilter.status;
            getListParam.is_active = 1;
            getListParam.start = ($scope.amcListModel.currentPage - 1) * $scope.amcListModel.limit;
            getListParam.limit = $scope.amcListModel.limit;
            $scope.amcListModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAmcList(getListParam, configOption, headers).then(function(response) {
                var data = response.data.list;
                $scope.amcListModel.list = data;
                if ($scope.amcListModel.list.length > 0)
                {
                    for (var i = 0; i < $scope.amcListModel.list.length; i++)
                    {
                        var datevalue = moment($scope.amcListModel.list[i].from_date).valueOf();
                        if (datevalue > 0)
                        {
                            var nextdate = $scope.amcListModel.list[i].from_date.split(' ');
                            var newnextdate = utilityService.convertToUTCDate(nextdate[0]);
                            $scope.amcListModel.list[i].from_date = utilityService.parseDateToStr(newnextdate, $rootScope.appConfig.date_format);
                        } else
                            $scope.amcListModel.list[i].from_date = '';
                        
                        $scope.amcListModel.list[i].newDate = utilityService.parseDateToStr($scope.amcListModel.list[i].to_date,$rootScope.appConfig.date_format);
                        $scope.amcListModel.list[i].to_date = $scope.amcListModel.list[i].newDate;
                    }
                }
                $scope.amcListModel.total = data.total;
                $scope.amcListModel.isLoadingProgress = false;
            });

        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'amc_list_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "amc_list_form-amc_customer_dropdown")
            {
                $scope.searchFilter.customerInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
            
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }

        });


        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);

        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
            if ($scope.customerFilter == null || $scope.customerFilter == undefined || $scope.customerFilter == '')
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            }

        });
        $scope.checkFilter = function ()
        {
            if ($scope.customerFilter != null && $scope.customerFilter != undefined && $scope.customerFilter != '')
            {
                
                $scope.getList();
            } else
            {
                $scope.init();
            }
        }
        $scope.checkFilter();
    }]);




