

app.controller('conversionjournalEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams','utilityService', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams,utilityService) {

    $scope.journalModel = {
        name: '',
        version: '',         
        list: [],
        taskGrplist: [],
        workcenterInfo : '',
        fromProducts : [],
        toProducts : []
    }

    $scope.validationFactory = ValidationFactory;
    $scope.adminService = adminService;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
    $scope.getNavigationBlockMsg = function()
    {
        if (typeof $scope.edit_journal_form != 'undefined' && typeof $scope.edit_journal_form.$pristine != 'undefined' && !$scope.edit_journal_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;
    $scope.formReset = function()
    {
        $scope.edit_journal_form.$setPristine();
        if (typeof $scope.edit_journal_form != 'undefined')
        {
            $scope.journalModel.date = "";
            $scope.journalModel.purpose = "";
            $scope.journalModel.fromProducts = [];
            $scope.journalModel.toProducts = [];
            $scope.updateTaskGroupDetail();
        }
    }
    $scope.getProductList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.formatFromProductModel = function(model,index) {

        if (model != null && model != undefined)
        {
            // $scope.updateFromProductInfo(model,index);
            return model.name;
        }
        return  '';
    };
    $scope.updatefromProductInfo = function(index)
    {
        if($scope.journalModel.fromProducts.length > 0)
        {  
            $scope.journalModel.fromProducts[index].fromqty = 1;
            
        }    
    }
    $scope.formatToProductModel = function(model) {

        if (model != null && model != undefined)
        {
            return model.name;
        }
        return  '';
    };
    $scope.updatetoProductInfo = function(index)
    {
        if($scope.journalModel.toProducts.length > 0)
        { 
            $scope.journalModel.toProducts[index].toqty = 1;
        }    
    }
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

    $scope.dateOpen = false;
    $scope.openDate = function(index)
    {
        if (index == 0)
        {
            $scope.dateOpen = true;
        }
    }
    $scope.modifyTaskGroup = function() {

        $scope.overallfromQty = 0;
        $scope.overalltoQty = 0;
        if ($scope.isDataSavingProcess)
        {
            return;
        }
        $scope.isDataSavingProcess = true;
        var addTaskGroupParam = {};
        var headers = {};
        headers['screen-code'] = 'conversionjournal';
        addTaskGroupParam.id = 0;
        addTaskGroupParam.date = $filter('date')($scope.journalModel.date, 'yyyy-MM-dd');
        addTaskGroupParam.description = $scope.journalModel.purpose;
        for (var i = 0; i < $scope.journalModel.fromProducts.length; i++)
        {
            $scope.overallfromQty += parseFloat($scope.journalModel.fromProducts[i].fromqty);
                
        }
        addTaskGroupParam.overall_from_qty = $scope.overallfromQty;
        for (var i = 0; i < $scope.journalModel.toProducts.length; i++)
        {
            $scope.overalltoQty += parseFloat($scope.journalModel.toProducts[i].toqty);
                
        }
        addTaskGroupParam.overall_to_qty = $scope.overalltoQty;
        addTaskGroupParam.is_active = 1;
        addTaskGroupParam.detail = [];
        for (var i = 0; i < $scope.journalModel.fromProducts.length; i++)
        {
            var fromParam = {};
            fromParam.from_pdt_id = $scope.journalModel.fromProducts[i].fromproductInfo.id;
            fromParam.from_qty = $scope.journalModel.fromProducts[i].fromqty;
            fromParam.from_uom_id = $scope.journalModel.fromProducts[i].fromproductInfo.uom_id;
            fromParam.to_pdt_id = 0;
            fromParam.to_qty = 0;
            fromParam.to_uom_id = 0;
            addTaskGroupParam.detail.push(fromParam);
        }
        for (var j = 0; j < $scope.journalModel.toProducts.length; j++)
        {
            var toParam = {};
            toParam.to_pdt_id = $scope.journalModel.toProducts[j].toproductInfo.id;
            toParam.to_qty = $scope.journalModel.toProducts[j].toqty;
            toParam.to_uom_id = $scope.journalModel.toProducts[j].toproductInfo.uom_id;
            toParam.from_pdt_id = 0;
            toParam.from_qty = 0;
            toParam.from_uom_id = 0;
            addTaskGroupParam.detail.push(toParam);
        }
        adminService.modifyConversionJournal(addTaskGroupParam,$stateParams.id, headers).then(function(response)
        {
            if (response.data.success == true)
            {
                $scope.formReset();
                $state.go('app.conversionjournal');
            }
            $scope.isDataSavingProcess = false;
        });
    };
    $scope.addFromProduct = function()
    {
        var fromProduct = {
            fromproductInfo : {},
            fromqty : ''
        }
        $scope.journalModel.fromProducts.push(fromProduct);
    }
    $scope.addToProduct = function()
    {
        var toProduct = {
            toproductInfo : {},
            toqty : ''
        }
        $scope.journalModel.toProducts.push(toProduct);
    }
    $scope.deleteFromProduct = function(index)
    {
        $scope.journalModel.fromProducts.splice(index, 1);
    };
    $scope.deleteToProduct = function(index)
    {
        $scope.journalModel.toProducts.splice(index, 1);
    };
        
    $scope.updateTaskGroupDetail = function()
    {
        if ($scope.journalModel.taskGrplist != null && $scope.journalModel.taskGrplist != 'undefined' && $scope.journalModel.taskGrplist != '')
        {
            var fromproductcount = 0;
            var toproductcount = 0;
            var date = utilityService.parseStrToDate($scope.journalModel.taskGrplist.date);
            $scope.journalModel.date = date;
            $scope.journalModel.purpose = $scope.journalModel.taskGrplist.description;
            for (var i = 0; i < $scope.journalModel.taskGrplist.detail.length; i++)
            {
                if($scope.journalModel.taskGrplist.detail[i].from_pdt_id != null && $scope.journalModel.taskGrplist.detail[i].from_pdt_id != 0)
                {
                    var fromproduct = {
                        "name": $scope.journalModel.taskGrplist.detail[i].from_pdt_name,
                        "from_product_id" : $scope.journalModel.taskGrplist.detail[i].from_pdt_id,
                        "fromqty" : $scope.journalModel.taskGrplist.detail[i].from_qty,
                        "uom_id": $scope.journalModel.taskGrplist.detail[i].from_uom_id,
                        "fromproductInfo" : {}
                    };
                    var fromindex = fromproductcount;
                    $scope.journalModel.fromProducts.push(fromproduct);
                    
                    $scope.journalModel.fromProducts[fromindex].fromproductInfo = { 
                        name : $scope.journalModel.fromProducts[fromindex].name,
                        id : $scope.journalModel.fromProducts[fromindex].from_product_id
                    };
                    ++fromproductcount;
                }
                if($scope.journalModel.taskGrplist.detail[i].to_pdt_id != null && $scope.journalModel.taskGrplist.detail[i].to_pdt_id != 0)
                {
                        
                    var toproduct = {
                        "toqty": $scope.journalModel.taskGrplist.detail[i].to_qty,
                        "uom_id" : $scope.journalModel.taskGrplist.detail[i].to_uom_id,
                        "name" : $scope.journalModel.taskGrplist.detail[i].to_pdt_name,
                        "to_product_id" : $scope.journalModel.taskGrplist.detail[i].to_pdt_id 
                    };
                    var toindex = toproductcount;
                    $scope.journalModel.toProducts.push(toproduct);
                    $scope.journalModel.toProducts[toindex].toproductInfo = {
                        name : $scope.journalModel.toProducts[toindex].name,
                        id : $scope.journalModel.toProducts[toindex].to_product_id
                    };
                    ++toproductcount;
                }
            } 
                
        }
    };
    $scope.getWorkCenterList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.WORK_CENTER_LIST, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.formatWorkCenterModel = function (model,index) {

        if (model !== null && model != undefined)
        {
            $scope.updateWorkInfo(model, index);
            return model.name;
        }
        return  '';
    };
    $scope.updateWorkInfo = function(model,index)
    {
        if ($scope.journalModel.list.length != 0)
        {
            if (typeof model == 'undefined')
                return;
            $scope.journalModel.list[index].workcenterInfo.name = model.name;
        }   
    }

    $scope.getTaskGroupList = function() {
        $scope.journalModel.isLoadingProgress = true;
        var taskGrpListParam = {};
        taskGrpListParam.id = $stateParams.id;
        taskGrpListParam.is_active = 1;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getConversionJournalList(taskGrpListParam, configOption).then(function(response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.journalModel.taskGrplist = data.list[0]; 
                $scope.journalModel.total = data.total;
                $scope.updateTaskGroupDetail();
            }
            $scope.journalModel.isLoadingProgress = false;
        });

    };
    $scope.onFocus = function (e) {
        $timeout(function () {
            $(e.target).trigger('input');
        });
    };
    $scope.keyupfromHandler = function (event)
    {
        if (event.keyCode == 9)
        {
            event.preventDefault();
            var currentFocusField = angular.element(event.currentTarget);
            var nextFieldId = $(currentFocusField).attr('next-focus-id');
            var currentFieldId = $(currentFocusField).attr('id');
            if (nextFieldId == undefined || nextFieldId == '')
            {
                return;
            }
            if(currentFieldId.indexOf('_name_') != -1)
            {
                var filedIdSplitedValue = nextFieldId.split('_');
                var existingBrunchCount = $scope.journalModel.fromProducts.length;
                if (existingBrunchCount > 0)
                {
                    $timeout(function () {
                        var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.journalModel.fromProducts.length) + '_1';
                        $("#" + newNextFieldId).focus();
                    }, 300);
                    return;
                }
            }    
            if (currentFieldId.indexOf('_qty_') != -1)
            {
                filedIdSplitedValue = nextFieldId.split('_');
                existingBrunchCount = $scope.journalModel.fromProducts.length;
                if (existingBrunchCount > 0)
                {
                    $scope.addFromProduct();
                    $timeout(function () {
                        var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.journalModel.fromProducts.length) + '_1';
                        $("#" + newNextFieldId).focus();
                    }, 300);
                    return;
                }
            }
            $timeout(function () {
                $("#" + nextFieldId).focus();
                window.document.getElementById(nextFieldId).setSelectionRange(0, window.document.getElementById(nextFieldId).value.length);
            }, 300);
        }
    }
    $scope.keyuptoHandler = function (event)
    {
        if (event.keyCode == 9)
        {
            event.preventDefault();
            var currentFocusField = angular.element(event.currentTarget);
            var nextFieldId = $(currentFocusField).attr('next-focus-id');
            var currentFieldId = $(currentFocusField).attr('id');
            if (nextFieldId == undefined || nextFieldId == '')
            {
                return;
            }
            if(currentFieldId.indexOf('_name_') != -1)
            {
                var filedIdSplitedValue = nextFieldId.split('_');
                var existingBrunchCount = $scope.journalModel.toProducts.length;
                if (existingBrunchCount > 0)
                {
                    $timeout(function () {
                        var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.journalModel.toProducts.length) + '_1';
                        $("#" + newNextFieldId).focus();
                    }, 300);
                    return;
                }
            }    
            if (currentFieldId.indexOf('_qty_') != -1)
            {
                filedIdSplitedValue = nextFieldId.split('_');
                existingBrunchCount = $scope.journalModel.fromProducts.length;
                if (existingBrunchCount > 0)
                {
                    $scope.addToProduct();
                    $timeout(function () {
                        var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.journalModel.toProducts.length) + '_1';
                        $("#" + newNextFieldId).focus();
                    }, 300);
                    return;
                }
            }
            $timeout(function () {
                $("#" + nextFieldId).focus();
                window.document.getElementById(nextFieldId).setSelectionRange(0, window.document.getElementById(nextFieldId).value.length);
            }, 300);
        }
    }
    
    $scope.getTaskGroupList();

}]);
