

app.controller('departmentAddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

    $scope.departmentModel = {
        id: '',
        departmentname: "",
        code: '',
        comments: ''
    };

    $scope.validationFactory = ValidationFactory;

    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
    $scope.getNavigationBlockMsg = function()
    {
        if (typeof $scope.department_form != 'undefined' && typeof $scope.department_form.$pristine != 'undefined' && !$scope.department_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;
    $scope.formReset = function() {
        $scope.department_form.$setPristine();
        $scope.departmentModel.centername = '';
        $scope.departmentModel.comments = '';
        $scope.departmentModel.code = '';
    }

    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



    $scope.createDepartment = function() {
        if ($scope.isDataSavingProcess == false)
        {
            $scope.isDataSavingProcess = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'department';                
            getListParam.name = $scope.departmentModel.departmentname;
            getListParam.code = $scope.departmentModel.code;
            getListParam.comments = $scope.departmentModel.comments;
            adminService.saveDepartment(getListParam, headers).then(function(response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.department');
                }
                $scope.isDataSavingProcess = false;
            });
        }
    };

}]);




