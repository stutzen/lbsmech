app.controller('paymenttermsAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', function($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state) {

        $scope.paymenttermsAddModel = {
            "id": 0,
            "name": "",
            "comments": "",
            "is_active": 1,
            "is_default": "",
            currentPage: 1,
            total: 0,
            isLoadingProgress: false
        }

        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_paymentterms_form != 'undefined' && typeof $scope.create_paymentterms_form.$pristine != 'undefined' && !$scope.create_paymentterms_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function()
        {
            if (typeof $scope.create_paymentterms_form != 'undefined')
            {

                $scope.create_paymentterms_form.$setPristine();
                $scope.paymenttermsAddModel = {
                    "id": 0,
                    "name": "",
                    "comments": "",
                    "is_active": 1,
                    "is_default": 1,
                    currentPage: 1,
                    total: 0

                }
            }
        }

        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createpaymentterms, 300);
        }

        $scope.getpaymenttermslist = function() {

            var getListParam = {};
            getListParam.selectpaymenttermsId = '';
            $scope.paymenttermsAddModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getpaymenttermslist(getListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.paymenttermsAddModel.list = data;
                $scope.paymenttermsAddModel.isLoadingProgress = false;
            });
        };
        $scope.getpaymenttermslist();
        $scope.createpaymentterms = function()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createPaymenttermsParam = {};
                var headers = {};
                headers['screen-code'] = 'paymentterms';
                createPaymenttermsParam.name = $scope.paymenttermsAddModel.name;
                createPaymenttermsParam.comments = $scope.paymenttermsAddModel.comments;
                createPaymenttermsParam.is_default = $scope.paymenttermsAddModel.is_default == true ? 1 : 0;
                //createPaymenttermsParam.rolename = $scope.paymenttermsAddModel.rolename;            
                createPaymenttermsParam.is_active = 1;
                adminService.addpaymentterms(createPaymenttermsParam, headers).then(function(response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.paymentterms')
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };
    }]);




