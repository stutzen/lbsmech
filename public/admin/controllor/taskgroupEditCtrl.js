

app.controller('taskgroupEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams','sweet', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams,sweet) {

    $scope.taskGroupModel = {
        name: '',
        version: '',         
        list: [],
        taskGrplist: [],
        workcenterInfo : '',
        departmentList : [],
        type : ''
    }

    $scope.validationFactory = ValidationFactory;

    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
    $scope.getNavigationBlockMsg = function()
    {
        if (typeof $scope.edit_taskgrp_form != 'undefined' && typeof $scope.edit_taskgrp_form.$pristine != 'undefined' && !$scope.edit_taskgrp_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;
    $scope.formReset = function()
    {
        $scope.edit_taskgrp_form.$setPristine();
        if (typeof $scope.edit_taskgrp_form != 'undefined')
        {
            $scope.taskGroupModel.name = "";
            $scope.taskGroupModel.version = "";
            $scope.taskGroupModel.isActive = true;
            $scope.taskGroupModel.list = [];
            $scope.updateTaskGroupDetail();
        }
    }
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

    $scope.dateOpen = false;
    $scope.openDate = function(index)
    {
        if (index == 0)
        {
            $scope.dateOpen = true;
        }
    }
    $scope.modifyTaskGroup = function() {

        if ($scope.isDataSavingProcess)
        {
            return;
        }

        $scope.isDataSavingProcess = true;
        var modifyTaskGroup = {};
        var headers = {};
        var count = 0;
        headers['screen-code'] = 'taskgroup';
        modifyTaskGroup.id = $stateParams.id;
        modifyTaskGroup.name = $scope.taskGroupModel.taskgroupname;
        modifyTaskGroup.code = $scope.taskGroupModel.version;
        modifyTaskGroup.type = $scope.taskGroupModel.type;
        modifyTaskGroup.is_active = 1;
        modifyTaskGroup.detail = [];
        for (var i = 0; i < $scope.taskGroupModel.list.length; i++)
        {
            var itemsParam = {};
            ++count;
            itemsParam.operation = $scope.taskGroupModel.list[i].operation;
            itemsParam.duration = $scope.taskGroupModel.list[i].duration;
            itemsParam.department_id = $scope.taskGroupModel.list[i].department;
            itemsParam.work_center_id = $scope.taskGroupModel.list[i].workcenterInfo.id;
            itemsParam.sequence_no = count;
            itemsParam.type = $scope.taskGroupModel.type;
            itemsParam.amount = $scope.taskGroupModel.list[i].amount;
            modifyTaskGroup.detail.push(itemsParam);
        }
        var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
        adminService.modifyTaskGroup(modifyTaskGroup, $stateParams.id, headers, configOption).then(function(response)
        {
            if (response.data.success == true)
            {
                $scope.formReset();
                $state.go('app.taskgroup');
            }
            $scope.isDataSavingProcess = false;
        });
    };

    $scope.addNewProduct = function()
    {
        var newRow = {
            "id": '',
            "operation": '',
            "duration": '',
            "name": '',
            "type" : 'internal',
            "amount" : '',
            "department" : ''
        }
        $scope.taskGroupModel.list.push(newRow);
    };

    $scope.deleteRow = function(index)
    {
        var index = -1;
        $scope.taskGroupModel.list.splice(index, 1);
    };

    $scope.updateTaskGroupDetail = function()
    {
        if ($scope.taskGroupModel.taskGrplist != null && $scope.taskGroupModel.taskGrplist != 'undefined' && $scope.taskGroupModel.taskGrplist != '')
        {
            $scope.taskGroupModel.taskgroupname = $scope.taskGroupModel.taskGrplist.name;
            $scope.taskGroupModel.version = $scope.taskGroupModel.taskGrplist.code;
            $scope.taskGroupModel.type = $scope.taskGroupModel.taskGrplist.type;
            for (var i = 0; i < $scope.taskGroupModel.taskGrplist.detail.length; i++)
            {
                var newRow = {
                    "operation": $scope.taskGroupModel.taskGrplist.detail[i].operation,
                    "department" : $scope.taskGroupModel.taskGrplist.detail[i].department_id + '',
                    "duration": $scope.taskGroupModel.taskGrplist.detail[i].duration,
                    "name": $scope.taskGroupModel.taskGrplist.detail[i].work_center_name,
                    "id" : $scope.taskGroupModel.taskGrplist.detail[i].work_center_id,
                    "type" : $scope.taskGroupModel.taskGrplist.detail[i].type,
                    "amount" : $scope.taskGroupModel.taskGrplist.detail[i].amount
                };
                $scope.taskGroupModel.list.push(newRow);
                if($scope.taskGroupModel.list[i].department == 0)
                {
                    $scope.taskGroupModel.list[i].department = '';
                }    
                $scope.taskGroupModel.list[i].workcenterInfo = $scope.taskGroupModel.list[i];
                console.log($scope.taskGroupModel.list);
            }
        }
    };
    $scope.getWorkCenterList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.WORK_CENTER_LIST, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.formatWorkCenterModel = function (model,index) {

        if (model !== null && model != undefined)
        {
            $scope.updateWorkInfo(model, index);
            return model.name;
        }
        return  '';
    };
    $scope.updateWorkInfo = function(model,index)
    {
        if ($scope.taskGroupModel.list.length != 0)
        {
            if (typeof model == 'undefined')
                return;
            $scope.taskGroupModel.list[index].workcenterInfo.name = model.name;
        }   
    }

    $scope.getTaskGroupList = function() {
        $scope.taskGroupModel.isLoadingProgress = true;
        var taskGrpListParam = {};
        taskGrpListParam.id = $stateParams.id;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getTaskGroupDetail(taskGrpListParam, configOption).then(function(response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.taskGroupModel.taskGrplist = data.list[0];
                for(var i = 0; i< $scope.taskGroupModel.taskGrplist.detail.length;i++)
                {
                    $scope.taskGroupModel.taskGrplist.detail[i].duration = $scope.taskGroupModel.taskGrplist.detail[i].duration.slice(1,5);
                }    
                $scope.taskGroupModel.total = data.total;
                $scope.updateTaskGroupDetail();
            }
            $scope.taskGroupModel.isLoadingProgress = false;
        });

    };
    $scope.taskgroupcheck = function()
    {
        if($scope.taskGroupModel.type == 'external' && $scope.taskGroupModel.list.length > 1)
        {
            sweet.show("In external select only one task group");
        }
        else
        {
            $scope.modifyTaskGroup();
        }    
    }
    $scope.getDepartmentList = function()
    {
        var getListParam = {};
        getListParam.is_active = 1;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getDepartmentList(getListParam, configOption).then(function(response) {
            var data = response.data;
            if (data.list.length > 0)
            {
                $scope.taskGroupModel.departmentList = data.list;
            }

        });
    }
    $scope.getDepartmentList();
    $scope.getTaskGroupList();

}]);
