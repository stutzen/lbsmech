
app.controller('inactiveDealReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.dealModel = {
        currentPage: 1,
        total: 0,
        total_Amount: 0,
        limit: 10,
        list: [],
        length: [],
        serverList: null,
        isLoadingProgress: false,
        isSearchLoadingProgress: false,
        stageList : []
    };
    $scope.showDetail = false;
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.dealModel.limit = $scope.pagePerCount[0];
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.validationFactory = ValidationFactory;
    $scope.openDate = function(index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index === 1)
        {
            $scope.toDateOpen = true;
        }

    };

    $scope.searchFilter = {
        days: '',
        stage : ''
    };

    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            days: '',
            stage : ''
        };
        $scope.dealModel.list = [];
    }
    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise !== null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    $scope.getList = function() {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'inactivitydeal';
        getListParam.days = $scope.searchFilter.days;
        getListParam.is_active = 1;
        getListParam.start = 0;
        getListParam.limit = 0;
        getListParam.stage_id = $scope.searchFilter.stage;
        $scope.dealModel.isLoadingProgress = true;
        $scope.dealModel.isSearchLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getInactiveDealReport(getListParam, configOption, headers).then(function(response)
        {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.dealModel.list = data;
                for (var i = 0; i < $scope.dealModel.list.length; i++)
                {
                    $scope.dealModel.list[i].newdate = utilityService.parseStrToDate($scope.dealModel.list[i].next_follow_up);
                    $scope.dealModel.list[i].next_follow_up = utilityService.parseDateToStr($scope.dealModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                    $scope.dealModel.list[i].newdate = utilityService.parseStrToDate($scope.dealModel.list[i].updated_at);
                    $scope.dealModel.list[i].updated_at = utilityService.parseDateToStr($scope.dealModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                }
            }
            $scope.dealModel.isLoadingProgress = false;
            $scope.dealModel.isSearchLoadingProgress = false;
        });

    };
    $scope.print = function()
    {
        $window.print();
    };
    $scope.getStageList = function ()
    {
        var createParam = {};
        createParam.is_active = 1;
        $scope.dealModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getStageList(createParam, configOption).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.dealModel.stageList = data.list;
                $scope.dealModel.isLoadingProgress = false;
            }
        });
    };
    $scope.getStageList();
}]);




