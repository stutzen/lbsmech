

app.controller('advanceEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.advanceModel = {
            id: '',
            date: '',
            adv_amount: '',
            remark: "",
            employeeInfo: '',
            emp_id: '',
            emp_code: '',
            deduction_amt: '',
            balance_amt: '',
            is_active: '',
            total_deduction_amt: '',
            deduction_amt: '',
            accountName: '',
            account_id: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.advance_edit_form != 'undefined' && typeof $scope.advance_edit_form.$pristine != 'undefined' && !$scope.advance_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.advance_edit_form.$setPristine();
            if (typeof $scope.advance_edit_form != 'undefined')
            {
                $scope.advanceModel.id = "";
                $scope.advanceModel.date = "";
                $scope.advanceModel.adv_amount = '';
                $scope.advanceModel.remark = "";
                $scope.advanceModel.employeeInfo = "";
                $scope.advanceModel.emp_id = '';
                $scope.advanceModel.emp_code = '';
                $scope.advanceModel.deduction_amt = '';
                $scope.advanceModel.balance_amt = "";
                $scope.advanceModel.deduction_amt = '';
                $scope.advanceModel.total_deduction_amt = '';
                $scope.advanceModel.accountName = '';
                $scope.advanceModel.account_id = '';
            }
            $scope.updateAdvanceInfo();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.adminService = adminService;
        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            $scope.initUpdateDetailTimeoutPromise = $timeout($scope.updateAdvanceInfo, 300);
        }

        $scope.currentDate = new Date();

        $scope.advanceDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.advanceDateOpen = true;
            }
        }

        $scope.updateAdvanceInfo = function ()
        {
            $scope.advanceModel.is_active = $scope.advanceModel.advanceDetail.is_active;
            $scope.advanceModel.id = $scope.advanceModel.advanceDetail.id;
            $scope.advanceModel.date = new Date($scope.advanceModel.advanceDetail.date);
            $scope.advanceModel.adv_amount = $scope.advanceModel.advanceDetail.adv_amount;
            $scope.advanceModel.balance_amt = $scope.advanceModel.advanceDetail.balance_amt;
            $scope.advanceModel.deduction_amt = $scope.advanceModel.advanceDetail.deduction_amt;
            $scope.advanceModel.total_deduction_amt = $scope.advanceModel.advanceDetail.total_deduction_amt;
            $scope.advanceModel.remark = $scope.advanceModel.advanceDetail.remark;
            $scope.advanceModel.employeeInfo = {
                id: $scope.advanceModel.advanceDetail.emp_id,
                f_name: $scope.advanceModel.advanceDetail.f_name,
                emp_code: $scope.advanceModel.advanceDetail.emp_code
            };
            $scope.advanceModel.emp_id = $scope.advanceModel.advanceDetail.emp_id;
            $scope.advanceModel.emp_code = $scope.advanceModel.advanceDetail.emp_code;
            $scope.advanceModel.accountName = $scope.advanceModel.advanceDetail.accountName;
            $scope.advanceModel.account_id = $scope.advanceModel.advanceDetail.account_id;
            $scope.advanceModel.isLoadingProgress = false;
        }

        $scope.getEmployeelist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeeModel = function (model) {

            if (model !== null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };

        $scope.getAdvanceInfo = function () {

            $scope.advanceModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var advanceListParam = {};
                advanceListParam.id = $stateParams.id;

                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getAdvanceList(advanceListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.advanceModel.advanceDetail = data.list[0];
                    }
                    $scope.initUpdateDetail();
                });
            }
        };

        $scope.editAdvance = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var modifyAdvanceParam = {};
            var headers = {};
            headers['screen-code'] = 'advance';

            modifyAdvanceParam.emp_id = $scope.advanceModel.employeeInfo.id;
            modifyAdvanceParam.emp_code = $scope.advanceModel.employeeInfo.emp_code;
            modifyAdvanceParam.date = $filter('date')($scope.advanceModel.date, 'yyyy-MM-dd');
            modifyAdvanceParam.adv_amount = $scope.advanceModel.adv_amount;
            modifyAdvanceParam.deduction_amt = $scope.advanceModel.deduction_amt;
            modifyAdvanceParam.balance_amt = $scope.advanceModel.adv_amount;
            modifyAdvanceParam.account_id = $scope.advanceModel.account_id;
            modifyAdvanceParam.is_active = $scope.advanceModel.is_active;
            modifyAdvanceParam.remark = $scope.advanceModel.remark;

            adminService.editAdvance(modifyAdvanceParam, $stateParams.id, headers).then(function (response) {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.advanceList');
                }
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getAdvanceInfo();
    }]);



