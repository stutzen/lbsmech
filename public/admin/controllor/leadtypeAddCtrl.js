

app.controller('leadtypeAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.leadModel = {
            name: '',
            comments:'',
            is_active:1,
            type:''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_lead_form != 'undefined' && typeof $scope.add_lead_form.$pristine != 'undefined' && !$scope.add_lead_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_lead_form.$setPristine();
            if (typeof $scope.add_lead_form != 'undefined')
            {
                $scope.leadModel.name = "";
                $scope.leadModel.comments = "";
                $scope.leadModel.is_active = 1;
                $scope.leadModel.type = "";
                
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.createLeadType = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addCompanyParam = {};
            var headers = {};
            headers['screen-code'] = 'leadtype';
            addCompanyParam.name =  $scope.leadModel.name;
            addCompanyParam.comments = $scope.leadModel.comments ;
            addCompanyParam.type = $scope.leadModel.type ;
            addCompanyParam.is_active = 1;
            adminService.createLeadTypeList(addCompanyParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.leadtype');
                }
                $scope.isDataSavingProcess = false;
            });
        };
         
    }]);





