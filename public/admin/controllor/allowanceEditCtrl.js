

app.controller('allowanceEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.allowanceEditModel = {
            id: '',
            name: "",
            amount: '',
            comments: '',
            isActive: true,
            isLoadingProgress: false

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.category_form != 'undefined' && typeof $scope.category_form.$pristine != 'undefined' && !$scope.category_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.category_form.$setPristine();
            $scope.updateCategoryInfo();

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;



        $scope.updateCategoryInfo = function()
        {
            if ($scope.allowanceCategoryDetail != null)
            {
                $scope.allowanceEditModel.name = $scope.allowanceCategoryDetail.name;
                $scope.allowanceEditModel.amount = $scope.allowanceCategoryDetail.amount;
                $scope.allowanceEditModel.comments = $scope.allowanceCategoryDetail.comments;
            }
            $scope.allowanceEditModel.isLoadingProgress = false;
        }
        $scope.updateAllowanceCategory = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'allowance';
                //createParam.seq_no = '';
                createParam.name = $scope.allowanceEditModel.name;
                createParam.amount = $scope.allowanceEditModel.amount;
                createParam.comments = $scope.allowanceEditModel.comments;
               // createParam.type = 'income';

                adminService.editAllowanceCategory(createParam, $stateParams.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.allowanceList');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getList = function() {

            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.is_active = 1;
           // getListParam.type = 'income';

            $scope.allowanceEditModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAllowanceCategoryList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.allowanceCategoryDetail = data.list[0];
                    $scope.updateCategoryInfo();
                }

            });

        };


        $scope.getList();

    }]);




