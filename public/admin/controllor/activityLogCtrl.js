





app.controller('activityLogCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.activityModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            account: '',
            description: '',
            balance: '',
            account_no: '',
            status: '',
            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.activityModel.limit = $scope.pagePerCount[0];

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };


        $scope.getList = function() {

            var getListParam = {};
//            getListParam.start = ($scope.activityModel.currentPage - 1) * $scope.activityModel.limit;
//            getListParam.limit = $scope.activityModel.limit;
            var headers = {};
            headers['screen-code'] = 'activitylog';
            if (window.location.hash = '#/activity-list')
            {
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            }
            else
            {
                var configOption = adminService.handleOnlyErrorResponseConfig;
            }
            adminService.getActivityList(getListParam, configOption, headers).then(function(response) {
                var data = response.data;
                if (data.success && data.list.length != 0)
                {
                    $scope.activityModel.list = data.list;
                    for (var i = 0; i < $scope.activityModel.list.length; i++)
                    {
                        $scope.activityModel.list[i].newdate = utilityService.parseStrToDate($scope.activityModel.list[i].date);
                        $scope.activityModel.list[i].date = utilityService.parseDateToStr($scope.activityModel.list[i].newdate, 'MMMM-dd-yyyy');
                    }

                }
            });

        };

        $scope.getList();
    }]);




