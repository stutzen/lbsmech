




app.controller('accountaddCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', '$httpService', 'APP_CONST', function ($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, $httpService, APP_CONST) {

        $scope.accountModel = {
            "account": "",
            "description": "",
            "balance": "",
            "contact_person": "",
            "contact_phone": "",
            "account_number": "",
            "website": "",
            "bank_name": "",
            "currency": "",
            "branch": "",
            "address": "",
            "notes": "",
            "sno": "",
            "token": "",
            "created_by": "",
            "updated_by": "",
            "created_at": "",
            "updated_at": "",
            "status": "",
            "isActive": true,
            "acode": ''
        };
        $scope.validationFactory = ValidationFactory;
        $scope.isDataSavingProcess = false;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.account_add_form !== 'undefined' && typeof $scope.account_add_form.$pristine !== 'undefined' && !$scope.account_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };

        $scope.formReset = function () {
            if (typeof $scope.create_account_form != 'undefined')
            {
                $scope.create_account_form.$setPristine();
                $scope.accountModel = {
                    "account": "",
                    "description": "",
                    "balance": "",
                    "contact_person": "",
                    "contact_phone": "",
                    "account_number": "",
                    "website": "",
                    "bank_name": "",
                    "currency": "",
                    "branch": "",
                    "address": "",
                    "notes": "",
                    "sno": "",
                    "token": "",
                    "created_by": "",
                    "updated_by": "",
                    "created_at": "",
                    "updated_at": "",
                    "status": "",
                    "isActive": true,
                    "acode": ''
                };
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetTabletCreate = function () {

            $scope.accountModel.user = '';

        };

        $scope.getAcodeList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.code = val;
            return $httpService.get(APP_CONST.API.ACODE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        
        $scope.formatAcodeModel = function (model)
        {
            if (model != null)
            {
                return model.code;
            }
            return  '';
        };

        $scope.createAccount = function () {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createAccountParam = {};
                var headers = {};
                headers['screen-code'] = 'account';
                createAccountParam.account = $scope.accountModel.account;
                createAccountParam.description = $scope.accountModel.description;
                createAccountParam.balance = $scope.accountModel.balance;
                createAccountParam.account_number = $scope.accountModel.account_number;
                createAccountParam.contact_person = $scope.accountModel.contact_person;
                createAccountParam.contact_phone = $scope.accountModel.contact_phone;
                createAccountParam.website = $scope.accountModel.website;
                createAccountParam.bank_name = '';
                createAccountParam.currecy = '';
                createAccountParam.branch = '';
                createAccountParam.address = '';
                createAccountParam.notes = '';
                createAccountParam.sno = '';
                createAccountParam.token = '';
                createAccountParam.created_by = '';
                createAccountParam.updated_by = '';
                createAccountParam.created_at = '';
                createAccountParam.updated_at = '';
                createAccountParam.acode = '';
                if (typeof $scope.accountModel.acode != undefined && $scope.accountModel.acode != null && $scope.accountModel.acode != '')
                {
                    createAccountParam.acode = $scope.accountModel.acode.code;
                }
                adminService.createAccount(createAccountParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.account');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };


    }]);




