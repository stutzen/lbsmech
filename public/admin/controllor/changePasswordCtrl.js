




app.controller('changePasswordCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout, ValidationFactory) {

    $scope.changePwdModel = {
        "id": 0,
        "userName":"",
        "password":"",
        "newpassword":"",
        "confirmpassword":""

    }

    $scope.validationFactory = ValidationFactory;

    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";


    $scope.getNavigationBlockMsg = function()
    {
        if (typeof $scope.change_password_form != 'undefined' && typeof $scope.change_password_form.$pristine != 'undefined' && !$scope.change_password_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;
    $scope.formReset = function() {

        $scope.change_password_form.$setPristine();

    }

    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;




    $scope.changePassword = function() {

        $scope.isDataSavingProcess = true;
        var changePasswordParam = {};

        changePasswordParam.username = $rootScope.userModel.username;
        changePasswordParam.password = $scope.changePwdModel.password;
        changePasswordParam.newPassword = $scope.changePwdModel.newpassword;

        adminService.changePassword(changePasswordParam).then(function(response) {
            if (response.data.success == true)
            {
               $scope.formReset();
               $state.go('app.changepassword', {}, {'reload': true});
            }
            $scope.isDataSavingProcess = false;
        });
    };



}]);




