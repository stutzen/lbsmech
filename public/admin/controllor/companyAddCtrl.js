

app.controller('companyAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout','$interval', 'APP_CONST', '$stateParams', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout,$interval, APP_CONST, $stateParams) {

    $scope.companyModel = {
        name: '',
        city: '',
        district: '',
        state: '',
        country: '',
        address: '',
        pin_code: '',
        website: '',
        is_active: 1,
        city_list: [],
        state_list: [],
        country_list: []
    }

    $scope.validationFactory = ValidationFactory;

    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
    $scope.getNavigationBlockMsg = function ()
    {
        if (typeof $scope.add_company_form != 'undefined' && typeof $scope.add_company_form.$pristine != 'undefined' && !$scope.add_company_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    }
    $scope.isDataSavingProcess = false;
    $scope.formReset = function ()
    {
        $scope.add_company_form.$setPristine();
        if (typeof $scope.add_company_form != 'undefined')
        {
            $scope.companyModel.name = "";
            $scope.companyModel.countryInfo = "";
            $scope.companyModel.stateInfo = "";
            $scope.companyModel.pin_code = "";
            $scope.companyModel.cityInfo = "";
            $scope.companyModel.website = "";
            $scope.companyModel.address = "";
            $scope.companyModel.is_active = 1;
            $scope.companyModel.district = "";

        }
    }
    $scope.fromContact = false;
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
    $timeout(function(){
        
    
        $rootScope.$on('companyInfo', function (event, companyDetail) {
            //        $state.go('app.companyAdd');
            //        $timeout(function() {
            $scope.fromContact = true;

            $scope.companyModel.name = '';
            if (typeof companyDetail === 'undefined')
                return;
            if (companyDetail.id != null && companyDetail.id == undefined)
            {

                $scope.companyModel.name = companyDetail;
                console.log('name');
                console.log($scope.companyModel.name);
            }
        }, 600);
        
    //    });
    });
    $scope.createCompany = function () {

        if ($scope.isDataSavingProcess)
        {
            return;
        }

        $scope.isDataSavingProcess = true;
        var addCompanyParam = {};
        var headers = {};
        headers['screen-code'] = 'company';
        addCompanyParam.name = $scope.companyModel.name;
        addCompanyParam.website = $scope.companyModel.website;
        addCompanyParam.pin_code = $scope.companyModel.pin_code;
        addCompanyParam.address = $scope.companyModel.address;
        addCompanyParam.is_active = 1;
        addCompanyParam.district = $scope.companyModel.district;
        addCompanyParam.country_id = $scope.companyModel.country_id;
        for (var i = 0; i < $scope.companyModel.country_list.length; i++)
        {
            if ($scope.companyModel.country_id == $scope.companyModel.country_list[i].id)
            {
                addCompanyParam.country_name = $scope.companyModel.country_list[i].name;
            }
        }
        addCompanyParam.state_id = $scope.companyModel.state_id;
        for (var i = 0; i < $scope.companyModel.state_list.length; i++)
        {
            if ($scope.companyModel.state_id == $scope.companyModel.state_list[i].id)
            {
                addCompanyParam.state_name = $scope.companyModel.state_list[i].name;
            }
        }
        addCompanyParam.city_id = $scope.companyModel.city_id;
        for (var i = 0; i < $scope.companyModel.city_list.length; i++)
        {
            if ($scope.companyModel.city_id == $scope.companyModel.city_list[i].id)
            {
                addCompanyParam.city_name = $scope.companyModel.city_list[i].name;
            }
        }
        adminService.addCompany(addCompanyParam, headers).then(function (response)
        {
            if (response.data.success == true)
            {
                $scope.formReset();
                if ($scope.fromContact == false)
                {
                    $state.go('app.company');
                }
                else
                {
                    if ($stateParams.id != '' && $stateParams.id != undefined)
                    {
                        $state.go('app.contactEdit', {
                            'id': $stateParams.id, 
                            'company_id': response.data.id
                        });
                    } else
{
                        $scope.companyInfo = addCompanyParam;
                        $scope.companyInfo.id = response.data.id;
                        $scope.companyInfo.name = addCompanyParam.name;
                        $scope.$broadcast('COMPANY_SELECT_EVENT', $scope.companyInfo);
                       $state.go('app.contactAdd', {
                            'company_id': $scope.companyInfo.id 
                            
                        });
                    }
                }
            }
            $scope.isDataSavingProcess = false;
        });
    };

    $scope.$on('COMPANY_SELECT_EVENT', function (e, data) {
        console.log('COMPANY_SELECT_EVENT');
        var savedCompany = data;
        $rootScope.$broadcast('updateSavedCompanyDetail', savedCompany);
    //        $state.go('app.contactAdd');
    });

    $scope.getcountryList = function () {

        $scope.companyModel.isLoadingProgress = true;
        var countryListParam = {};
        var headers = {};
        headers['screen-code'] = 'company';

        countryListParam.id = '';

        countryListParam.is_active = 1;

        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getCountryList(countryListParam, configOption, headers).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.companyModel.country_list = data.list;
            }
            $scope.companyModel.isLoadingProgress = false;
        });

    };
    $scope.getcountryList();

    $scope.getstateList = function () {

        $scope.companyModel.isLoadingProgress = true;
        var stateListParam = {};
        var headers = {};
        headers['screen-code'] = 'company';

        stateListParam.id = '';

        stateListParam.is_active = 1;
        stateListParam.country_id = $scope.companyModel.country_id;
        stateListParam.country_name = $scope.companyModel.country_name;

        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getStateList(stateListParam, configOption, headers).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.companyModel.state_list = data.list;
            }
            $scope.companyModel.isLoadingProgress = false;
        });

    };

    $scope.getcityList = function () {

        $scope.companyModel.isLoadingProgress = true;
        var cityListParam = {};
        var headers = {};
        headers['screen-code'] = 'company';

        cityListParam.id = '';

        cityListParam.is_active = 1;
        cityListParam.country_id = $scope.companyModel.country_id;
        cityListParam.country_name = $scope.companyModel.country_name;
        cityListParam.state_id = $scope.companyModel.state_id;
        cityListParam.start = 0;
        cityListParam.limit = 0;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getCityList(cityListParam, configOption, headers).then(function (response)
        {
            if (response.data.success === true)
            {
                var data = response.data;
                $scope.companyModel.city_list = data.list;
            }
            $scope.companyModel.isLoadingProgress = false;
        });

    };

    $scope.CountryChange = function ()
    {
        $scope.companyModel.state_id = '';
        $scope.companyModel.city_id = '';
        $scope.companyModel.city_list = [];
        $scope.getstateList();
    }
    $scope.StateChange = function ()
    {
        $scope.companyModel.city_id = '';
        $scope.getcityList();
    }
    
    $scope.checkPage=function()
    {
        if ($rootScope.currentPage == 'contactCompany')
        {
            $scope.fromContact = true;
        }
        else
        {
            $scope.fromContact = false;
        }
    }
    $scope.checkPage();
   
}]);





