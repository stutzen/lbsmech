
app.controller('attributeListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'APP_CONST', 'ValidationFactory', function($scope, $rootScope, adminService, $httpService, $filter, Auth, $state, $timeout, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.attributeModel = {
            currentPage: 1,
            id: '',
            total: 0,
            limit: 10,
            list: [],
            status: '',
            attributeTypeList: [],
            attribute_type: "",
            isLoadingProgress: false,
        };

        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.attributeModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            attributetype_id: '',
            attribute_code: ''
        };
        $scope.searchFilterValue = "";
        $scope.refreshScreen = function()
        {
            $scope.searchFilter.attributetype_id = '';
            $scope.searchFilter.attribute_code = '';
            $scope.getList();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectAttributeId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteCategoryLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectAttributeId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteCategoryLoadingProgress = false;
        };

        $scope.deleteAttributeInfo = function( )
        {
            if ($scope.isdeleteCategoryLoadingProgress == false)
            {
                $scope.isdeleteCategoryLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectAttributeId;
                var headers = {};
                headers['screen-code'] = 'attribute';
                adminService.deleteAttribute(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteCategoryLoadingProgress = false;
                });
            }
        };

        $scope.getcustomerList = function(val) {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.companyId = $rootScope.userModel.companyId;
            autosearchParam.mode = 1;
            autosearchParam.sort = 1;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, autosearchParam, false).then(function(responseData) {
                var data = responseData.data;
                var hits = data.list;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

//    $scope.updateAttributeType = function( )
//    {
//        if ($scope.isloadedAttType)
//        {   
//            var loopInner,loopOuter;
//            for ( loopOuter=0;loopOuter < $scope.attributeModel.list.length; loopOuter++)
//            {
//                for( loopInner = 0;loopInner < $scope.attributeModel.attributeTypeList.length; loopInner++ )
//                {   
//                    if ( $scope.attributeModel.attributeTypeList[loopInner].id == $scope.attributeModel.list[loopOuter].attributetype_id)
//                    {
//                        
//                    }
//                    attribute_type    
//                    $scope.attributeModel.isLoadingProgress = false;            
//                }   
//            }    
//        }   
//    }

        $scope.isloadedAttType = false;
        $scope.getAttributeTypeList = function()
        {
            var attrTypeListParam = {};
            $scope.isloadedAttType = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAttributeTypeList(attrTypeListParam, configOption).then(function(response)
            {
                var data = response.data.list;
                $scope.attributeModel.attributeTypeList = data;
                $scope.isloadedAttType = true;
            });
        };

        $scope.getList = function(val) {

            var getListParam = {};
            getListParam.id = "";
            var headers = {};
            headers['screen-code'] = 'attribute';
            getListParam.attributetype_id = $scope.searchFilter.attributetype_id;
            getListParam.attribute_code = $scope.searchFilter.attribute_code;
            getListParam.start = ($scope.attributeModel.currentPage - 1) * $scope.attributeModel.limit;
            getListParam.limit = $scope.attributeModel.limit;
            $scope.attributeModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAttributeList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.attributeModel.list = data;
                    $scope.attributeModel.total = response.data.total;
                }
                $scope.attributeModel.isLoadingProgress = false;
                //$scope.updateAttributeType ( ); 
            });
        };
        $scope.getAttributeTypeList( );
        $scope.getList();
    }]);








