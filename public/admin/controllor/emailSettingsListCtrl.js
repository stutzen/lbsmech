
app.controller('emailSettingsListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', '$stateParams', '$http', '$window', function($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, $stateParams, $http, $window) {

        $scope.emailModel = {
            id: '',
            "isactive": 1,
            list: [],
            currentPage: 1,
            limit:4,
            isLoadingProgress: false
        }
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.emailModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''
            
        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: ''
            };
            $scope.initTableFilter();
        }
        
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        
        $scope.validationFactory = ValidationFactory;

        $scope.getList = function()
        {
            $scope.emailModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'emailsettings';
            
            getListParam.type = 'email';
            getListParam.id='';
            getListParam.tplname = $scope.searchFilter.name;
            getListParam.start = ($scope.emailModel.currentPage - 1) * $scope.emailModel.limit;
            getListParam.limit = $scope.emailModel.limit;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getEmailSettingsList(getListParam,configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.emailModel.list = data.list;
                        $scope.emailModel.total = data.total;
                    }
                }
                $scope.emailModel.isLoadingProgress = false;
            });

        };

        $scope.getList( );
        
    }]);



