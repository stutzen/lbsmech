

app.controller('deductionSingleAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', 'sweet', 'utilityService', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, sweet, utilityService) {

        $scope.deductionModel = {
            id: '',
            date: '',
            adv_amount: '',
            remark: "",
            employeeInfo: '',
            emp_id: '',
            emp_code: '',
            deduction_amt: '',
            curr_deduction_amt:'',
            balance_amt: '',
            is_active: 1,
            advanceList: [],
            account_id: ''
        }

        $scope.validationFactory = ValidationFactory;
        $scope.adminService = adminService;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function ()
        {
            if (typeof $scope.deduction_add_form != 'undefined' && typeof $scope.deduction_add_form.$pristine != 'undefined' && !$scope.deduction_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function ()
        {
            $scope.deduction_add_form.$setPristine();
            if (typeof $scope.deduction_add_form != 'undefined')
            {
                $scope.deductionModel.id = "";
                $scope.deductionModel.date = "";
                $scope.deductionModel.adv_amount = '';
                $scope.deductionModel.employeeInfo = "";
                $scope.deductionModel.emp_id = '';
                $scope.deductionModel.curr_deduction_amt = '';
                $scope.deductionModel.balance_amt = '';
                $scope.deductionModel.deduction_amt = '';
                $scope.deductionModel.date = $scope.currentDate;
                $scope.deductionModel.advanceList = [];
            }
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.currentDate = new Date();
        $scope.deductionModel.date = $scope.currentDate;

        $scope.deductionDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.deductionDateOpen = true;
            }
        }

        $scope.getEmployeelist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active=1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeeModel = function (model) {

            if (model !== null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            $scope.initUpdateDetailTimeoutPromise = $timeout($scope.updateDeductionInfo, 300);
        }

        $scope.updateDeductionInfo = function ()
        {
            $scope.deductionModel.adv_amount = $scope.deductionDetail.adv_amount;
            $scope.deductionModel.balance_amt = parseFloat($scope.deductionDetail.balance_amt);
            $scope.deductionModel.curr_deduction_amt = parseFloat($scope.deductionDetail.deduction_amt);
            $scope.deductionModel.isLoadingProgress = false;
        }
        
        $scope.getAdvanceList = function ()
        {
            $scope.deductionModel.isLoadingProgress = true;
            var getAdvanceParam = {};
            getAdvanceParam.type = 1;
            getAdvanceParam.emp_id = $scope.deductionModel.employeeInfo.id;
            getAdvanceParam.is_active = '1';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getAdvanceList(getAdvanceParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.deductionDetail = data.list[0];
                        $scope.deductionDetail.deduction_amt = parseFloat($scope.deductionDetail.deduction_amt).toFixed(2);
                        $scope.initUpdateDetail();
                    }
                    else
                    {
                        $scope.deductionModel.adv_amount = '';
                        $scope.deductionModel.balance_amt = '';
                        $scope.deductionModel.deduction_amt = '';
                    }
                    
                });
        };

        $scope.addDeduction = function () {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addDeductionParam = [];
            var headers = {};
            headers['screen-code'] = 'advancededuction';
                if ($scope.deductionDetail.id != '')
                    {
                        var deductionAmount = parseFloat($scope.deductionModel.curr_deduction_amt);
                        $scope.deductionModel.balance_amt = parseFloat($scope.deductionDetail.balance_amt);
                        if ($scope.deductionDetail.balance_amt < deductionAmount)
                        {
                            $scope.deductionModel.id = '';
                            $scope.isDataSavingProcess = false;
                            sweet.show('Oops...', 'Deduction Amount greater than account balance...', 'error');
                            return;
                        }
                    }
                var advanceParam = {};
                advanceParam.emp_id = $scope.deductionModel.employeeInfo.id;
                advanceParam.emp_code = $scope.deductionModel.employeeInfo.emp_code;
                advanceParam.date = $filter('date')($scope.deductionModel.date, 'yyyy-MM-dd');
                advanceParam.advance_amt_id = $scope.deductionDetail.id;
                advanceParam.deduction_amt = $scope.deductionModel.curr_deduction_amt;
                advanceParam.is_active = 1;
                addDeductionParam.push(advanceParam);

            adminService.addDeduction(addDeductionParam, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.deductionList');
                }
                $scope.isDataSavingProcess = false;
            });
        };

//        $scope.getAdvanceList();

    }]);
