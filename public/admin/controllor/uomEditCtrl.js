



app.controller('uomEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'StutzenHttpService', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, StutzenHttpService) {

        $scope.uomAddModel = {
            id: '',
            uomname: '',
            note: '',
            is_active: ' ',
            isLoadingProgress: false
        };
        $scope.pagePerCount = [50, 100];
        $scope.uomAddModel.limit = $scope.pagePerCount[0];
        $scope.uomDetail = {};

        $scope.validationFactory = ValidationFactory;

        $scope.userRoleList = ['', 'ROLE_ADMIN', 'ROLE_WORKFORCE'];

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.uom_edit_form != 'undefined' && typeof $scope.uom_edit_form.$pristine != 'undefined' && !$scope.uom_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.uom_edit_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetUomEdit = function() {

            $scope.uomAddModel.user = '';

        }
// 3.display the data
        $scope.createUom = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var createUomParam = {};
                var headers = {};
                headers['screen-code'] = 'uom';
                createUomParam.id = $scope.uomAddModel.id;
                createUomParam.name = $scope.uomAddModel.uomname;
                createUomParam.description = $scope.uomAddModel.note;
                //createUomParam.is_active = ($scope.uomAddModel.is_active==true?1:0);
                createUomParam.is_active = 1;
                adminService.postUpdateUom(createUomParam, $scope.uomAddModel.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {

                        $scope.formReset();
                        $state.go('app.uom');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        //2.get the updated data and keep it in local
        $scope.updateUomInfo = function()
        {
            if ($scope.uomDetail != null)
            {
                $scope.uomAddModel.id = $scope.uomDetail.id;
                $scope.uomAddModel.uomname = $scope.uomDetail.name;
                $scope.uomAddModel.note = $scope.uomDetail.description;
                $scope.uomAddModel.is_active = ($scope.uomDetail.is_active == 1 ? true : false);
                $scope.uomAddModel.isLoadingProgress = false;
            }
        }

        //1.get the data from the list    
        $scope.getUomInfo = function() {

            $scope.uomAddModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                getListParam.limit = 1;
                getListParam.start = 0;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getUom(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.uomDetail = data.list[0];
                        $scope.updateUomInfo();
                    }

                });
            }
        };

        $scope.getUomInfo();

    }]);



