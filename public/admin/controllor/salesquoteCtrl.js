


app.controller('salesquoteCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'APP_CONST', 'ValidationFactory', function($scope, $rootScope, adminService, $httpService, $filter, Auth, $state, $timeout, APP_CONST, ValidationFactory) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.salesquoteModel = {
            currentPage: 1,
            id: '',
            total: 0,
            limit: 10,
            country: '',
            city: '',            
            list: [],
            printList:[],
            status: '',
            serverList: null,
            isLoadingProgress: false,
            customerInfo: '',
            newAddedAmount: '',
            totalAmt: '',
            categoryList: []
        };

        $scope.validationFactory = ValidationFactory;
        $scope.pagePerCount = [50, 100];
        $scope.salesquoteModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            email: '',
            name: '',
            phone: '',
            category: '',
            city: '',
            familycode: '',
            customerOf:'',
            accno: ''
        };             

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.getList = function(val) {

            var getListParam = {};
            getListParam.Id = "";
            getListParam.start = ($scope.salesquoteModel.currentPage - 1) * $scope.salesquoteModel.limit;
            getListParam.limit = $scope.salesquoteModel.limit;
            $scope.salesquoteModel.isLoadingProgress = true;
            adminService.getSalesquoteList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.salesquoteModel.list = data;
                $scope.salesquoteModel.total = response.data.total;
                $scope.salesquoteModel.isLoadingProgress = false;
            });
        };      
   
       $scope.getList();
    }]);








