

app.controller('itemHistoryReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.itemhistoryModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            total_Qty: 0,
            limit: 10,
            list: [],
            length: [],
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.itemhistoryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0));
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            customerInfo: {},
            serialno: '',
            cityInfo: {},
            fromdate: '',
            todate: ''

        };
//        $scope.searchFilter.todate = $scope.currentDate;

        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                customerInfo: '',
                serialno: '',
                cityInfo: {},
                fromdate: '',
                todate: ''
            };

            $scope.itemhistoryModel.list = [];
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };



        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'buyermachine';
            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
            {
                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
            }
            else
            {
                getListParam.customer_id = '';
            }
            if ($scope.searchFilter.cityInfo != null && typeof $scope.searchFilter.cityInfo != 'undefined' && typeof $scope.searchFilter.cityInfo.id != 'undefined')
            {
                getListParam.shopping_city_id = $scope.searchFilter.cityInfo.id;
            }
            else
            {
                getListParam.shopping_city_id = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            getListParam.serial_no = $scope.searchFilter.serialno;
            getListParam.start = 0;
            getListParam.limit = 0;
            getListParam.is_active = 1;
            $scope.itemhistoryModel.isLoadingProgress = true;
            $scope.itemhistoryModel.isSearchLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getItemHistoryReport(getListParam, configOption, headers).then(function(response)
            {
                var totalAmt = 0.00;
                var qty = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.itemhistoryModel.list = data;
                    for (var i = 0; i < $scope.itemhistoryModel.list.length; i++)
                    {
                        $scope.itemhistoryModel.list[i].newdate = utilityService.parseStrToDate($scope.itemhistoryModel.list[i].date);
                        $scope.itemhistoryModel.list[i].date = utilityService.parseDateToStr($scope.itemhistoryModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        totalAmt = totalAmt + parseFloat($scope.itemhistoryModel.list[i].price);
                        qty = qty + parseFloat($scope.itemhistoryModel.list[i].qty);
                    }
                    $scope.itemhistoryModel.total = data.total;
                    $scope.itemhistoryModel.total_Amount = totalAmt;
                    $scope.itemhistoryModel.total_Amount = parseFloat($scope.itemhistoryModel.total_Amount).toFixed(2);
                    $scope.itemhistoryModel.total_Amount = utilityService.changeCurrency($scope.itemhistoryModel.total_Amount, $rootScope.appConfig.thousand_seperator);
                    $scope.itemhistoryModel.total_Qty = qty;
                    $scope.itemhistoryModel.total_Qty = parseFloat($scope.itemhistoryModel.total_Qty).toFixed(2);
                    $scope.itemhistoryModel.total_Qty = utilityService.changeCurrency($scope.itemhistoryModel.total_Qty, $rootScope.appConfig.thousand_seperator);

                }
                $scope.itemhistoryModel.isLoadingProgress = false;
                $scope.itemhistoryModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
        $scope.getCityList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            return $httpService.get(APP_CONST.API.CITY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCityModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

//        $scope.print = function()
//        {
//            $window.print();
//        };

        $scope.print = function(div) {

            var docHead = document.head.outerHTML;
            var printContents = document.getElementById(div).outerHTML;
            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            var newWin = window.open("", "_blank", winAttr);
            var writeDoc = newWin.document;
            writeDoc.open();
            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            writeDoc.close();
            newWin.focus();

        }

        $scope.getList();
    }]);




