




app.controller('transexpenseAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.transexpenseModel = {
            "debit": "",
            "customerid": "",
            "account": "",
            "accountid": "",
            "particulars": "",
            "account_category": "",
            "name": "",
            "date": "",
            "amount": "",
            "created_by": "",
            "status": "",
            "length": "",
            "incomeCategoryInfo": "",
            "isActive": true
        };
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.transexpense_add_form !== 'undefined' && typeof $scope.transexpense_add_form.$pristine !== 'undefined' && !$scope.transexpense_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function()
        {
            $scope.create_transexpense_form.$setPristine();
            $scope.transexpenseModel.account = "";
            $scope.transexpenseModel.date = "";
            $scope.transexpenseModel.description = "";
            $scope.transexpenseModel.incomeCategoryInfo = '';
            $scope.transexpenseModel.amount = "";
            $scope.transexpenseModel.isActive = true;

        };
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };

//       $scope.currentDate = new Date();
//       $scope.transexpenseModel.date = $filter('date')($scope.currentDate, $scope.dateFormat);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.startDateOpen = false;

        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.startDateOpen = true;
            }

        };
        $scope.getAccountlist = function()
        {
            var getListParam = {};
            getListParam.id = "";
            getListParam.name = "";
            getListParam.account_id = $scope.transexpenseModel.accountid;
            getListParam.customer_id = $scope.transexpenseModel.customerid;
            getListParam.start = ($scope.transexpenseModel.currentPage - 1) * $scope.transexpenseModel.limit;
            getListParam.limit = $scope.transexpenseModel.limit;
            $scope.isDataSavingProcess = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.transexpenseModel.Accountlist = data.list;
                $scope.isDataSavingProcess = false;
            });
        };

        $scope.getIncomeCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.type = 'expense';
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatincomeCategoryModel = function(model) {

            if (model !== null)
            {

                return model.name;
            }
            return  '';
        };

        $scope.createTransexpense = function() {
            if($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createTransexpenseParam = {};
                var headers = {};
                headers['screen-code'] = 'transexpense';
                for (var i = 0; i < $scope.transexpenseModel.Accountlist.length; i++)
                {
                    if ($scope.transexpenseModel.account == $scope.transexpenseModel.Accountlist[i].id)
                    {
                        createTransexpenseParam.account = $scope.transexpenseModel.Accountlist[i].account;
                    }
                }
                createTransexpenseParam.account_id = $scope.transexpenseModel.account;
                createTransexpenseParam.transaction_date = $filter('date')($scope.transexpenseModel.date, 'yyyy-MM-dd');
                createTransexpenseParam.particulars = $scope.transexpenseModel.description;
                createTransexpenseParam.account_category = '';
                if ($scope.transexpenseModel.incomeCategoryInfo != undefined && $scope.transexpenseModel.incomeCategoryInfo.name != undefined)
                {
                    createTransexpenseParam.account_category = $scope.transexpenseModel.incomeCategoryInfo.name;
                }

                createTransexpenseParam.debit = $scope.transexpenseModel.amount;
                createTransexpenseParam.name = "",
                        createTransexpenseParam.is_active = 1,
                        createTransexpenseParam.customer_id = "",
                        createTransexpenseParam.payment_mode = "",
                        adminService.createTransexpense(createTransexpenseParam, headers).then(function(response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.transexpense');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getAccountlist();

    }]);




