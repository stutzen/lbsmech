

app.controller('categoryEditCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.categoryEditCtrl = {
            id: '',
            name: "",
            description: '',
            isActive: true,
            limit: 10,
            currentPage: 1,
            total: 0,
            imgs: [],
            isLoadingProgress: false

        };

        $scope.validationFactory = ValidationFactory;


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.category_edit_form != 'undefined' && typeof $scope.category_edit_form.$pristine != 'undefined' && !$scope.category_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            if (typeof $scope.category_edit_form != 'undefined')
            {
                $scope.category_edit_form.$setPristine();
                $scope.getCategory();
            }

        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;


        $scope.updateCategoryInfo = function()
        {
            $scope.categoryEditCtrl.id = $scope.categoryEditCtrl.list.id;
            $scope.categoryEditCtrl.name = $scope.categoryEditCtrl.list.name;
            $scope.categoryEditCtrl.description = $scope.categoryEditCtrl.list.description;
            $scope.categoryEditCtrl.isLoadingProgress = false;
        }

        $scope.modifyCategory = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                headers['screen-code'] = 'category';
                createParam.id =  $scope.categoryEditCtrl.id ;
                createParam.name = $scope.categoryEditCtrl.name;
                createParam.description = $scope.categoryEditCtrl.description;
                createParam.isActive = 1;

                adminService.updateCategory(createParam, createParam.id, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        //$scope.formReset();
                        $scope.category_edit_form.$setPristine();
                        $state.go('app.categorylist');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.getCategory = function() {

            var getListParam = {};
            getListParam.id = $stateParams.id;
            getListParam.name = "";
            getListParam.start = ($scope.categoryEditCtrl.currentPage - 1) * $scope.categoryEditCtrl.limit;
            getListParam.limit = $scope.categoryEditCtrl.limit;
            $scope.categoryEditCtrl.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCategoryList(getListParam, configOption).then(function(response) {
                var data = response.data.list;
                if (data.total != 0)
                {
                    $scope.categoryEditCtrl.list = data[0];
                }
                $scope.categoryEditCtrl.total = response.data.total;

                $scope.updateCategoryInfo();

            });

        };
        $scope.getCategory();

    }]);




