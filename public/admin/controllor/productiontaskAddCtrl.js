app.controller('productiontaskAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'sweet', '$httpService', 'APP_CONST', function ($scope, $rootScope, adminService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, sweet, $httpService, APP_CONST) {
        $scope.productionTaskModel = {
            inputProducts: [],
            productList: [],
            byProducts :[],
            taskList: [],
            outputqty: 1,
            outputupp: '',
            parentTask: '',
            taskcode: '',
            taskname: '',
            outputProducts: [],
            productFor: '',
            taskGroupList: [],
            taskGroupListDetail: [],
            taskgroup: ''
        }
         $scope.currentStep = 1;
         $scope.tabChange = function(value) {
            if(value == 2)
            {
                if($scope.productionTaskModel.byProducts.length == 0)
                {
                    $scope.addNewRow("byProduct");
                }
            }
            $scope.currentStep = value;
            
        }
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.create_production_task != 'undefined' && typeof $scope.create_production_task.$pristine != 'undefined' && !$scope.create_production_task.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.formReset = function () {

            $scope.create_production_task.$setPristine();
            $scope.productionTaskModel = {
                inputProducts: [],
                outputqty: 1,
                outputupp: '',
                parentTask: '',
                productFor: '',
                taskgroup: ''
            }
            $scope.getProductList();
            $scope.getTaskList();
            $scope.addNewRow();
        }

        $scope.addNewRow = function (value)
        {
            if(value == "product")
            {
               var newinput = {};
               newinput.name = '';
                newinput.productname = '';
                newinput.qty = '';
                newinput.taskgroupdetail = '';
               $scope.productionTaskModel.inputProducts.push(newinput); 
            }
            if(value == "byProduct")
            {
               var newinput = {};
               newinput.id = 0;
               newinput.name = '';
               newinput.isNew = true;
               newinput.input_qty = '';
               newinput.uom_id = '';
               $scope.productionTaskModel.byProducts.push(newinput); 
            }
        }

        $scope.deleteInputProduct = function (index,value)
        {
            if(value == 'byProduct')
            {
                $scope.productionTaskModel.byProducts.splice(index, 1);
            }
            if(value == 'product')
            {
                $scope.productionTaskModel.inputProducts.splice(index, 1);
            }
        }
        $scope.event = null;
        $scope.keyupHandler = function(event,type)
        {
            //event.preventDefault();
            if (event.keyCode == 13 || event.keyCode == 9)
            {
                if(type == "product")
                {
                    event.preventDefault();
                    var currentFocusField = angular.element(event.currentTarget);
                    var nextFieldId = $(currentFocusField).attr('next-focus-id');
                    var currentFieldId = $(currentFocusField).attr('id');
                    var productIndex = $(currentFocusField).attr('product-index');
                    if (nextFieldId == undefined || nextFieldId == '')
                    {
                        return;
                    }

                    if (currentFieldId.indexOf('_qty') != -1)
                    {
                        var filedIdSplitedValue = currentFieldId.split('_');
                        var existingBrunchCount = $scope.productionTaskModel.inputProducts.length - 1;
                        if (existingBrunchCount == filedIdSplitedValue[filedIdSplitedValue.length - 1])
                        {
                            $scope.addNewRow(type);
                            $timeout(function() {
                                            $scope.focusNextField (nextFieldId);
                                        },
                                        300);
                        }
                    }


                    $("#" + nextFieldId).focus();
                }
                if(type == "byProduct")
                {
                    event.preventDefault();
                    var currentFocusField = angular.element(event.currentTarget);
                    var nextFieldId = $(currentFocusField).attr('next-focus-id');
                    var currentFieldId = $(currentFocusField).attr('id');
                    var productIndex = $(currentFocusField).attr('product-index');
                    if (nextFieldId == undefined || nextFieldId == '')
                    {
                        return;
                    }

                    if (currentFieldId.indexOf('_qty') != -1)
                    {
                        var filedIdSplitedValue = currentFieldId.split('_');
                        var existingBrunchCount = $scope.productionTaskModel.byProducts.length - 1;
                        if (existingBrunchCount == filedIdSplitedValue[filedIdSplitedValue.length - 1])
                        {
                            $scope.addNewRow(type);
                            $timeout(function() {
                                            $scope.focusNextField (nextFieldId);
                                        },
                                        300);
                        }
                    }


                    $("#" + nextFieldId).focus();
                }
            }
           
            

        }

        $scope.focusNextField = function(nextFieldId)
        {
            if (nextFieldId != '')
            {
                $("#" + nextFieldId).focus();
            }
        }
        
        $scope.getProductList = function (val)
        {
            var getParam = {};
            getParam.is_active = 1;
            getParam.localData = 1;
            getParam.type = 'Product';
            getParam.name = val;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, getParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }

        $scope.formatProductModel = function (model, index,type)
        {
            if(type == "product")
            {
                if (model != null)
                {
                    $scope.updateProductInfo(model, index);
                    return model.name;
                }
                return  '';
            }
            if(type == "byProduct")
            {
                if (model != null)
                {
                    $scope.updateByproductInfo(model, index);
                    return model.name;
                }
                return  '';
            }
            
        }
        $scope.validateData = function()
        {

        }

        $scope.formatOutputProductModel = function (model, index)
        {
            
            if (model != null)
            {
                $scope.updateOutputProductInfo(model, index);
                return model.name;
            }
            return  '';
        }

        $scope.updateOutputProductInfo = function (model, index)
        {

            if (typeof model == 'undefined')
                return;
            if (model != undefined && model != null && model != '')
            {
                $scope.productionTaskModel.outputProducts = model;
                $scope.productionTaskModel.outputprodname = model.name;
                $scope.productionTaskModel.outputqty = 1;
            }

        }

        $scope.updateProductInfo = function (model, index)
        {
            if (model != null && model != undefined)
            {
                $scope.productionTaskModel.inputProducts[index].name = model.name;
                $scope.productionTaskModel.inputProducts[index].id = model.id;
            }
            
        }
        $scope.updateByproductInfo = function (model, index)
        {

            if (model != null && model != undefined)
            {
                $scope.productionTaskModel.byProducts[index].id = 0;
                $scope.productionTaskModel.byProducts[index].input_pdt_id = model.id;
                $scope.productionTaskModel.byProducts[index].name = model.name;
                $scope.productionTaskModel.byProducts[index].uom_id = model.uom_id;
            } 
        }
        $scope.getTaskList = function ()
        {
            var getParam = {};
            getParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductionTaskList(getParam, configOption).then(function (response) {

                var data = response.data;
                $scope.productionTaskModel.taskList = data.list;
            });
        }

        $scope.formatTaskModel = function (model, index)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        }

        $scope.formatForProductModel = function (model, index)
        {
            if (model != null)
            {
                $scope.updateForProductInfo(model, index);
                return model.name;
            }
            return  '';
        };

        $scope.updateForProductInfo = function (model, index)
        {

            if (typeof model == 'undefined')
                return;
            if (model != undefined && model != null && model != '')
            {
                $scope.productionTaskModel.productFor = model;
                $scope.productionTaskModel.productForName = model.name;
            }

        };

        $scope.saveProductionTask = function () {

            if ($scope.productionTaskModel.inputProducts.length > 0)
            {
                $scope.isDataSavingProcess = true;
                var createProdTaskParam = {};
                var headers = {};
                headers['screen-code'] = 'billofmaterial';
                createProdTaskParam.id = 0;
                createProdTaskParam.name = $scope.productionTaskModel.taskname;
                createProdTaskParam.bom_code = $scope.productionTaskModel.taskcode;
                if ($scope.productionTaskModel.parentTask != undefined && $scope.productionTaskModel.parentTask.id != '')
                {
                    createProdTaskParam.prev_bom_id = $scope.productionTaskModel.parentTask.id;
                } else
                {
                    createProdTaskParam.prev_bom_id = '';
                }
                createProdTaskParam.output_pdt_id = $scope.productionTaskModel.outputProducts.id;
                createProdTaskParam.product_for_id = $scope.productionTaskModel.productFor.id;
                createProdTaskParam.product_for_name = $scope.productionTaskModel.productFor.name;
                createProdTaskParam.unit_production_price = $scope.productionTaskModel.outputupp;
                createProdTaskParam.task_group_id = $scope.productionTaskModel.taskgroup;
                createProdTaskParam.comments = '';
                createProdTaskParam.detail = [];
                for (var i = 0; i < $scope.productionTaskModel.inputProducts.length; i++)
                {
                    var input = {};
                    input.id = 0;
                    input.name = $scope.productionTaskModel.inputProducts[i].name;
                    input.input_pdt_id = $scope.productionTaskModel.inputProducts[i].id;
                    input.input_qty = $scope.productionTaskModel.inputProducts[i].input_qty;
                    input.task_group_detail_id = 0;
                    createProdTaskParam.detail.push(input);
                }
                createProdTaskParam.productBom = [];
                for (var i = 0; i < $scope.productionTaskModel.byProducts.length; i++)
                {
                    var input = {};
                    input.product_id = $scope.productionTaskModel.byProducts[i].input_pdt_id;
                    input.qty = $scope.productionTaskModel.byProducts[i].input_qty;
                    input.uom_id = $scope.productionTaskModel.byProducts[i].uom_id;
                    createProdTaskParam.productBom.push(input);
                }
                adminService.addProductionTask(createProdTaskParam, headers).then(function (response) {

                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.billofmaterial');
                    }
                    $scope.isDataSavingProcess = false;

                });

            } else
            {
                sweet.show('Oops...', 'Select a Input Product...', 'error');
            }
        };

        $scope.getTaskGroupDetailList = function ()
        {
            for (var i = 0; i < $scope.productionTaskModel.inputProducts.length; i++)
            {
                $scope.productionTaskModel.inputProducts[i].taskgroupdetail = '';
            }
            var getParam = {};
            getParam.is_active = 1;
            getParam.id = $scope.productionTaskModel.taskgroup;
            $scope.productionTaskModel.operationList = [];
            if ($scope.productionTaskModel.taskgroup != '' && typeof $scope.productionTaskModel.taskgroup != 'undefined' && $scope.productionTaskModel.taskgroup != null)
            {
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getTaskGroupList(getParam, configOption).then(function (response) {
                    var data = response.data;
                    $scope.productionTaskModel.operationList = data.list[0].detail;
                    $scope.productionTaskModel.isLoadingProgress = false;
                });
            }
        };

        $scope.formatTaskGroupModel = function (model, index)
        {
            if (model != null)
            {

                return model.operation;
            }
            return  '';
        };


        $scope.getTaskGroupListAll = function ()
        {
            $scope.productionTaskModel.isLoadingProgress = true;
            var getParam = {};
            getParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getTaskGroupList(getParam, configOption).then(function (response) {
                var data = response.data;
                $scope.productionTaskModel.taskGroupList = data.list;
                $scope.productionTaskModel.isLoadingProgress = false;
            });
        };
        $scope.getTaskGroupListAll();
        $scope.getTaskGroupDetailList();
        $scope.addNewRow("product");
        $scope.tabChange(1);
        $scope.getTaskList();
    }]);




