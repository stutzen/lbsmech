





app.controller('transferlistCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.transferlistModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            from_date: '',
            to_date: '',
            account: '',
            comments: '',
            created_by: '',
            amount: '',
            is_active: 1,
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };
        $scope.searchFilter = {
            id: '',
            from_date: '',
            to_date: '',
            created_by: ''
        };
        $scope.validationFactory = ValidationFactory;
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                id: '',
                from_date: '',
                to_date: '',
                created_by: ''
            };
//            $scope.searchFilter.from_date = $scope.currentDate;
            $scope.initTableFilter();
        }
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.transferlistModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date();
      //  $scope.searchFilter.from_date = $scope.currentDate;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };



        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.selectTransferlistId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectTransferlistId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };
        $scope.deletetransferlistInfo = function( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeleteLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectTransferlistId;
                var headers = {};
                headers['screen-code'] = 'transfer';
                adminService.deletetransferlist(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };
        $scope.$watch('searchFilter.incomeCategoryInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });
        $scope.getList = function() {

            $scope.transferlistModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'transfer';
            // getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
            //getListParam.to_Date = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');

            getListParam.id = $scope.searchFilter.id;
            //getListParam.from_date = $scope.searchFilter.from_date;
            //getListParam.to_date = $scope.searchFilter.to_date;

            if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.adminService.appConfig.date_format);
            }

            if ($scope.searchFilter.from_date != null && $scope.searchFilter.from_date != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);

            }
            if ($scope.searchFilter.to_date != null && $scope.searchFilter.to_date != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);

            }

            getListParam.created_by = $scope.searchFilter.user;

            getListParam.start = ($scope.transferlistModel.currentPage - 1) * $scope.transferlistModel.limit;
            getListParam.limit = $scope.transferlistModel.limit;
            //getListParam.is_active = 1;
            $scope.transferlistModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.gettransferlist(getListParam, configOption, headers).then(function(response) {
//                var balanceamt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.transferlistModel.list = data;

                    for (var i = 0; i < $scope.transferlistModel.list.length; i++)
                    {
                        $scope.transferlistModel.list[i].newdate = utilityService.parseStrToDate($scope.transferlistModel.list[i].transaction_date);
                        $scope.transferlistModel.list[i].transaction_date = utilityService.parseDateToStr($scope.transferlistModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                    }

                    $scope.transferlistModel.total = data.total;
                }
                $scope.transferlistModel.isLoadingProgress = false;
            });

        };

//        $scope.getIncomeCategoryList = function(val)
//        {
//            var autosearchParam = {};
//            autosearchParam.name = val;
//            autosearchParam.type = 'income';
//            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
//            {
//                var data = responseData.data.list;
//                var hits = data;
//                if (hits.length > 10)
//                {
//                    hits.splice(10, hits.length);
//                }
//                return hits;
//            });
//        };
//        $scope.formatincomeCategoryModel = function(model) {
//
//            if (model !== null && model != undefined)
//            {
//
//                return model.name;
//            }
//            return  '';
//        };
//        $scope.getUserList = function() {
//
//            var getListParam = {};
//            getListParam.date = '';
//            getListParam.particular = '';
//            getListParam.category = '';
//            getListParam.credited_by = '';
//            getListParam.amount = '';
//            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
//            adminService.getUserList(getListParam, configOption).then(function(response) {
//                var data = response.data;
//                $scope.transferlistModel.userlist = data.list;
//
//            });
//
//        };
        $scope.getListPrint = function(val)
        {
            var headers = {};
            headers['screen-code'] = 'transfer';

            var getListParam = {};
            getListParam.Id = "";
            getListParam.start = 0;
            $scope.transferlistModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;

            adminService.gettransferlist(getListParam, configOption, headers).then(function(response) {

                if (response.data.success)
                {
                    var data = response.data.list;
                    $scope.transferlistModel.printList = data;
                }
                $scope.transferlistModel.isLoadingProgress = false;
            });
        };


        $scope.getList();
        //$scope.getUserList();
        // $scope.getListPrint();
    }]);




