





app.controller('balanceAdvanceReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.balanceAdvanceReport = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            employeeInfo: {},
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
            serverList: null,
            isLoadingProgress: false,
            totaldeduction: 0,
            totaladvance: 0,
            totalamt: 0,
            totalbalance: 0,
            openingBalance: null,
            teamList: [],
            accountName: '',
            isSearchLoadingProgress: false,
            openingAdvance: '',
            openingDeduction: '',
            openingAmount: ''
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.balanceAdvanceReport.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            employeeInfo: {}

        };
        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                employeeInfo: {}
            };
            $scope.balanceAdvanceReport.list = [];
            $scope.balanceAdvanceReport.openingBalance = null;
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        $scope.calculateTotal = function ()
        {
            var totDeduction = 0;
            var totAdvance = 0;
            var totamt = 0;
            for (var i = 0; i < $scope.balanceAdvanceReport.list.length; i++)
            {
                totDeduction += parseFloat($scope.balanceAdvanceReport.list[i].deduction_amount);
//                $scope.balanceAdvanceReport.totaldeduction = totDeduction;
//                $scope.balanceAdvanceReport.totaldeduction = parseFloat($scope.balanceAdvanceReport.totaldeduction).toFixed(2);
//                $scope.balanceAdvanceReport.totaldeduction = utilityService.changeCurrency($scope.balanceAdvanceReport.totaldeduction, $rootScope.appConfig.thousand_seperator);

                totAdvance += parseFloat($scope.balanceAdvanceReport.list[i].advance_amount);
//                $scope.balanceAdvanceReport.totaladvance = totAdvance;
//                $scope.balanceAdvanceReport.totaladvance = parseFloat($scope.balanceAdvanceReport.totaladvance).toFixed(2);
//                $scope.balanceAdvanceReport.totaladvance = utilityService.changeCurrency($scope.balanceAdvanceReport.totaladvance, $rootScope.appConfig.thousand_seperator);

//                totamt += parseFloat($scope.balanceAdvanceReport.list[i].amount);
//                $scope.balanceAdvanceReport.totalamt = totamt;

            }
            totAdvance += parseFloat($scope.balanceAdvanceReport.openingBalance.advance_amount);
            $scope.balanceAdvanceReport.totaladvance = totAdvance;
            $scope.balanceAdvanceReport.totaladvance = parseFloat($scope.balanceAdvanceReport.totaladvance).toFixed(2);
            $scope.balanceAdvanceReport.totaladvance = utilityService.changeCurrency($scope.balanceAdvanceReport.totaladvance, $rootScope.appConfig.thousand_seperator);

            totDeduction += parseFloat($scope.balanceAdvanceReport.openingBalance.deduction_amount);
            $scope.balanceAdvanceReport.totaldeduction = totDeduction;
            $scope.balanceAdvanceReport.totaldeduction = parseFloat($scope.balanceAdvanceReport.totaldeduction).toFixed(2);
            $scope.balanceAdvanceReport.totaldeduction = utilityService.changeCurrency($scope.balanceAdvanceReport.totaldeduction, $rootScope.appConfig.thousand_seperator);

        }

        $scope.updateBalanceAmount = function ()
        {
            if ($scope.balanceAdvanceReport.list.length >= 1)
            {
                var openingBalance = parseFloat($scope.balanceAdvanceReport.openingBalance.advance_amount) - parseFloat($scope.balanceAdvanceReport.openingBalance.deduction_amount);
                $scope.balanceAdvanceReport.list[0].balance = openingBalance;
                for (var i = 0; i < $scope.balanceAdvanceReport.list.length; i++)
                {
                    var advance_amount = $scope.balanceAdvanceReport.list[i].advance_amount == null ? 0 : parseFloat($scope.balanceAdvanceReport.list[i].advance_amount);
                    var deduction_amount = $scope.balanceAdvanceReport.list[i].deduction_amount == null ? 0 : parseFloat($scope.balanceAdvanceReport.list[i].deduction_amount);
                    if (advance_amount != 0)
                    {
                        openingBalance = openingBalance + advance_amount;
                    } else if (deduction_amount != 0)
                    {
                        openingBalance = openingBalance - deduction_amount;
                    }
                    $scope.balanceAdvanceReport.list[i].balance = openingBalance;
                }

                $scope.balanceAdvanceReport.totalbalance = openingBalance;
                $scope.balanceAdvanceReport.totalbalance = parseFloat($scope.balanceAdvanceReport.totalbalance).toFixed(2);
                $scope.balanceAdvanceReport.totalbalance = utilityService.changeCurrency($scope.balanceAdvanceReport.totalbalance, $rootScope.appConfig.thousand_seperator);


            } else
            {
                $scope.balanceAdvanceReport.totalbalance = 0.00;
            }
        }

        $scope.getEmployeelist = function (val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatEmployeeModel = function (model) {

            if (model != null && model != undefined)
            {
                return model.f_name;
            }
            return  '';
        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'balanceAdvanceReport';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            } else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }
//            if ($scope.searchFilter.todate !== '' && $scope.searchFilter.fromdate === '')
//            {
//                getListParam.to_date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//                getListParam.from_date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//            }
//            else if ($scope.searchFilter.fromdate !== '' && $scope.searchFilter.todate === '')
//            {
//                getListParam.to_date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//                getListParam.from_date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//            }
//            else
//            {
//                getListParam.to_date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//                getListParam.from_date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//            }
            if ($scope.searchFilter.employeeInfo != null && typeof $scope.searchFilter.employeeInfo != 'undefined' && typeof $scope.searchFilter.employeeInfo.id != 'undefined')
            {
                getListParam.emp_id = $scope.searchFilter.employeeInfo.id;
            } else
            {
                getListParam.emp_id = '';
            }
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.balanceAdvanceReport.isLoadingProgress = true;
            $scope.balanceAdvanceReport.isSearchLoadingProgress = true;
            adminService.getBalanceAdvanceReport(getListParam, headers).then(function (response)
            {
                if (response.data.list.length > 0)
                {
                    var data = response.data.list;
                    $scope.balanceAdvanceReport.list = data;
                    $scope.balanceAdvanceReport.openingBalance = response.data.openBalance;
                    $scope.balanceAdvanceReport.openingBalance.advance_amount = parseFloat($scope.balanceAdvanceReport.openingBalance.advance_amount).toFixed(2);
                    $scope.balanceAdvanceReport.openingBalance.deduction_amount = parseFloat($scope.balanceAdvanceReport.openingBalance.deduction_amount).toFixed(2);
                    var openingBalance = parseFloat($scope.balanceAdvanceReport.openingBalance.advance_amount) - parseFloat($scope.balanceAdvanceReport.openingBalance.deduction_amount);
                    $scope.balanceAdvanceReport.openingBalance.balance = openingBalance;
                    $scope.balanceAdvanceReport.openingBalance.balance = parseFloat($scope.balanceAdvanceReport.openingBalance.balance).toFixed(2);
                    $scope.balanceAdvanceReport.openingAdvance = utilityService.changeCurrency($scope.balanceAdvanceReport.openingBalance.advance_amount, $rootScope.appConfig.thousand_seperator);
                    $scope.balanceAdvanceReport.openingDeduction = utilityService.changeCurrency($scope.balanceAdvanceReport.openingBalance.deduction_amount, $rootScope.appConfig.thousand_seperator);

                    $scope.balanceAdvanceReport.openingAmount = utilityService.changeCurrency($scope.balanceAdvanceReport.openingBalance.balance, $rootScope.appConfig.thousand_seperator);

                    if ($scope.balanceAdvanceReport.list.length != 0)
                    {
                        $scope.updateBalanceAmount();
                        $scope.calculateTotal();
                        for (var i = 0; i < $scope.balanceAdvanceReport.list.length; i++)
                        {
                            $scope.balanceAdvanceReport.list[i].newdate = utilityService.parseStrToDate($scope.balanceAdvanceReport.list[i].transaction_date);
                            $scope.balanceAdvanceReport.list[i].transaction_date = utilityService.parseDateToStr($scope.balanceAdvanceReport.list[i].newdate, $scope.dateFormat);
                            $scope.balanceAdvanceReport.list[i].deduction_amount = utilityService.changeCurrency($scope.balanceAdvanceReport.list[i].deduction_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.balanceAdvanceReport.list[i].advance_amount = utilityService.changeCurrency($scope.balanceAdvanceReport.list[i].advance_amount, $rootScope.appConfig.thousand_seperator);
                            $scope.balanceAdvanceReport.list[i].balance = parseFloat($scope.balanceAdvanceReport.list[i].balance).toFixed(2);
                            $scope.balanceAdvanceReport.list[i].balance = utilityService.changeCurrency($scope.balanceAdvanceReport.list[i].balance, $rootScope.appConfig.thousand_seperator);

                        }
                    } else
                    {
                        $scope.balanceAdvanceReport.totaldeduction = 0.00;
                        $scope.balanceAdvanceReport.totaladvance = 0.00;
                        $scope.balanceAdvanceReport.totalbalance = 0.00;
                    }
                    $scope.balanceAdvanceReport.total = data.total;
                }
                $scope.balanceAdvanceReport.isLoadingProgress = false;
                $scope.balanceAdvanceReport.isSearchLoadingProgress = false;
            });

        };

        $scope.print = function ()
        {
            $window.print();
        };

        //$scope.getList();
    }]);




