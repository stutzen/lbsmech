

app.controller('countryAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

        $scope.countryModel = {
            id: '',
            name: '',
            isActive: true,
            code: ''
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_country_form != 'undefined' && typeof $scope.add_country_form.$pristine != 'undefined' && !$scope.add_country_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_country_form.$setPristine();
            if (typeof $scope.add_country_form != 'undefined')
            {
                $scope.countryModel.name = "";
                $scope.countryModel.code = "";
                $scope.countryModel.isActive = true;
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.createcountry = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }

            $scope.isDataSavingProcess = true;
            var addCountryParam = {};
            var headers = {};
            headers['screen-code'] = 'country';
            addCountryParam.id = 0;
            addCountryParam.name = $scope.countryModel.name;
            addCountryParam.code = $scope.countryModel.code;
            addCountryParam.is_active = $scope.countryModel.isActive == true ? 1 : 0;

            adminService.saveCountry(addCountryParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.country');
                }
                $scope.isDataSavingProcess = false;
            });
        };

    }]);



