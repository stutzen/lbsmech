
app.controller('itemissuedreportCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.itemissuedModel = {
        currentPage: 1,
        total: 0,
        limit: 10,    
        list: [],
        id: '',
        is_active: 1,
        isSearchLoadingProgress: false,
        isLoadingProgress: false
    };
    $scope.searchFilter = {
        limit: '',
        start: '',       
        fromdate: '',
        todate: '',
      
    };

    $scope.adminService = adminService;
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currencyFormat = $rootScope.appConfig.currency;
    $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0));
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
    $scope.searchFilter.todate = $scope.currentDate;

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.validationFactory = ValidationFactory;

    $scope.openDate = function(index) {
        if (index === 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index === 1)
        {
            $scope.toDateOpen = true;
        }
    };
    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            limit: '',
            start: '',           
            fromdate: '',
            todate: '',       
        };
        $scope.itemissuedModel.list = [];
        $scope.searchFilter.todate = $scope.currentDate;
        $scope.initTableFilter();
    }


    $scope.searchFilterValue = "";
    $scope.initTableFilterTimeoutPromise = null;
    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
   
   $scope.getList = function() {

        $scope.itemissuedModel.isLoadingProgress = true;
        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'itemwiseissuedreport';
        getListParam.is_active = 1;       
        getListParam.start = 0;
        getListParam.limit = 0;
        $scope.itemissuedModel.isLoadingProgress = true;
        $scope.itemissuedModel.isSearchLoadingProgress = true;
        if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
        {
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
            }
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.adminService.appConfig.date_format);
        }
        if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.adminService.appConfig.date_format);
        }
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getItemIssuedReport(getListParam, configOption, headers).then(function(response) {
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.itemissuedModel.list = data;
                for (var i = 0; i < $scope.itemissuedModel.list.length; i++)
                {
                    $scope.itemissuedModel.list[i].newdate = utilityService.parseStrToDate($scope.itemissuedModel.list[i].date);
                    $scope.itemissuedModel.list[i].date = utilityService.parseDateToStr($scope.itemissuedModel.list[i].newdate, $scope.adminService.appConfig.date_format);

                }
            }
            $scope.itemissuedModel.isLoadingProgress = false;
            $scope.itemissuedModel.isSearchLoadingProgress = false;
        });
    };   
    $scope.print = function()
    {
        $window.print();
    };
    $scope.getList();

}]);




