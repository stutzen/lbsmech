




app.controller('productionEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.productionModel = {
            "date": "",
            "emp_team_id": "",
            "emp_name": "",
            "emp_code": "",
            "amount": "",
            "emp_id": "",
            "is_present": "",
            "details": "",
            "allowance": [],
            "teamList": [],
            "empList": [],
            "teamId": '',
            "user_id": "",
            "user_name": "",
            "userInfo": "",
            "list": '',
            "allowance_amount": 0
        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;
        $scope.adminService = adminService;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.edit_production_form !== 'undefined' && typeof $scope.edit_production_form.$pristine !== 'undefined' && !$scope.edit_production_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function () {
            $scope.edit_production_form.$setPristine();
            $scope.updateProductionInfo();

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.isLoadedTeamList = false;
        $scope.isDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 1)
            {
                $scope.isDateOpen = true;
            }
        }

        $scope.setDate = function () {
            var date = moment(new Date()).add(-1, 'day');
            $scope.newdate = utilityService.parseStrToDate(date);
        };

        $scope.deleteTask = function (index)
        {
            $scope.productionModel.details.splice(index, 1);
            $scope.updateEmployeeSalary();
        };

        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedTeamList)
            {
                $scope.updateProductionInfo();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.getEmployeeTeam = function ()
        {
            var getListParam = {};
            var configOption = adminService.handleOnlyErrorResponseConfig;
            $scope.isLoadedTeamList = false;
            adminService.getEmployeeTeamList(getListParam, configOption).then(function (response) {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.productionModel.teamList = data;
                    $scope.isLoadedTeamList = true;
                }
            });
        };

        $scope.updateProductionInfo = function ()
        {
            if ($scope.productionModel.list != null)
            {
                $scope.productionModel.date = new Date($scope.productionModel.list.date);
                $scope.productionModel.emp_team_id = $scope.productionModel.list.emp_team_id + '';
                $scope.productionModel.emp_id = $scope.productionModel.list.emp_id;
                $scope.productionModel.emp_name = $scope.productionModel.list.emp_name;
                $scope.productionModel.emp_code = $scope.productionModel.list.emp_code;
                $scope.productionModel.emp_type = $scope.productionModel.list.emp_type;
                $scope.productionModel.amount = $scope.productionModel.list.amount;
                $scope.productionModel.salary_amount = $scope.productionModel.list.emp_salary;
                $scope.productionModel.is_present = $scope.productionModel.list.is_present == "1" ? true : false;
                $scope.productionModel.details = [];
                for (var i = 0; i < $scope.productionModel.list.details.length; i++)
                {
                    var detailsParam = {};
                    detailsParam.id = $scope.productionModel.list.details[i].id;
                    detailsParam.taskInfo = {
                        old: true,
                        id: $scope.productionModel.list.details[i].task_id,
                        name: $scope.productionModel.list.details[i].task_name,
                        type: $scope.productionModel.list.details[i].type,
                        duration: $scope.productionModel.list.details[i].duration,
                        output_pdt_id: $scope.productionModel.list.details[i].output_pdt_id,
                        product_for_name: $scope.productionModel.list.details[i].product_for_name,
                        output_pdt_name: $scope.productionModel.list.details[i].output_pdt_name,
                        output_pdt_uom: $scope.productionModel.list.details[i].output_pdt_uom,
                        production_qty: $scope.productionModel.list.details[i].production_qty,
                        production_unit_price: $scope.productionModel.list.details[i].production_unit_price,
                        amount: $scope.productionModel.list.details[i].amount,
                        inputs: []
                    };
//                    detailsParam.taskId = $scope.productionModel.list.details[i].task_id;
//                    detailsParam.name = $scope.productionModel.list.details[i].task_name;
                    detailsParam.id = $scope.productionModel.list.details[i].task_id;
                    detailsParam.type = $scope.productionModel.list.details[i].type;
                    detailsParam.duration = $scope.productionModel.list.details[i].duration;
                    detailsParam.output_pdt_id = $scope.productionModel.list.details[i].output_pdt_id;
                    detailsParam.product_for_name = $scope.productionModel.list.details[i].product_for_name;
                    detailsParam.output_pdt_uom = $scope.productionModel.list.details[i].output_pdt_uom;
                    detailsParam.production_qty = $scope.productionModel.list.details[i].production_qty;
                    detailsParam.production_unit_price = $scope.productionModel.list.details[i].production_unit_price;
                    detailsParam.task_amount = $scope.productionModel.list.details[i].amount;
                    detailsParam.inputs = [];
                    for (var j = 0; j < $scope.productionModel.list.details[i].inputs.length; j++)
                    {
                        var inputParam = {};
                        inputParam.id = $scope.productionModel.list.details[i].inputs[j].config_input_id;
                        inputParam.input_pdt_id = $scope.productionModel.list.details[i].inputs[j].input_pdt_id;
                        inputParam.task_config_id = $scope.productionModel.list.details[i].inputs[j].task_config_id;
                        inputParam.input_qty = $scope.productionModel.list.details[i].inputs[j].input_qty;
                        inputParam.input_pdt_name = $scope.productionModel.list.details[i].inputs[j].input_pdt_name;
                        inputParam.input_pdt_uom = $scope.productionModel.list.details[i].inputs[j].input_pdt_uom;
                        inputParam.required_input_qty = $scope.productionModel.list.details[i].inputs[j].production_input_qty;
                        inputParam.detailIndex = i;
                        detailsParam.inputs.push(inputParam);
                        detailsParam.taskInfo.inputs.push(inputParam);
                    }
                    $scope.productionModel.details.push(detailsParam);
                }
                for (var i = 0; i < $scope.productionModel.list.ot.length; i++)
                {
                    var allowanceParam = {};
                    allowanceParam.id = $scope.productionModel.list.ot[i].id;
                    allowanceParam.allowanceInfo = {
                        old: true,
                        id: $scope.productionModel.list.ot[i].allowance_master_id,
                        name: $scope.productionModel.list.ot[i].name,
                        amount: $scope.productionModel.list.ot[i].amount
                    };
                    allowanceParam.allowanceId = $scope.productionModel.list.ot[i].allowance_master_id;
                    allowanceParam.name = $scope.productionModel.list.ot[i].name;
                    allowanceParam.amount = $scope.productionModel.list.ot[i].amount;

                    $scope.productionModel.allowance.push(allowanceParam);
                }
                $scope.updateEmployeeSalary();
            }
        };

        $scope.changePresentStatus = function ()
        {
            if ($scope.productionModel.is_present == false)
            {
                $scope.productionModel.details = [];
                $scope.productionModel.amount = 0;
            } else
            {
                $scope.addNewTask();
            }
        };

        $scope.addNewTask = function ()
        {
            var type = $scope.productionModel.type;
            var newRow = {
                id: 0,
                taskId: '',
                name: '',
                type: type,
                duration: '1',
                output_pdt_id: '',
                product_for_name: '',
                production_qty: '',
                production_unit_price: '',
                task_amount: 0,
                inputs: []
            };
            $scope.productionModel.details.push(newRow);
        };

        $scope.getTaskList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.PRODUCTION_TASK_DETAIL, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatTaskModel = function (model, index)
        {
            if (model != null)
            {
                if (model.old == true)
                {
                    $scope.updateTaskInfo(model, index);
                }
                return model.name;
            }
            return  '';
        }

        $scope.getProdTaskDetail = function (index)
        {
            var getListParam = {};
            var headers = {};
            if ($scope.productionModel.details[index].taskInfo.id != null && $scope.productionModel.details[index].taskInfo.id != 'undefined' && $scope.productionModel.details[index].taskInfo.id != '')
            {
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                getListParam.is_active = 1;
                getListParam.id = $scope.productionModel.details[index].taskInfo.id;
                adminService.getProdTaskDetail(getListParam, configOption, headers).then(function (response)
                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        var taskdata = data.list[0];
                        var inputs = taskdata.input;
                        taskdata.inputs = inputs;
                        taskdata.type = $scope.productionModel.emp_type;
                        taskdata.duration = "1";
                        taskdata.production_unit_price = taskdata.unit_production_price;
                        taskdata.production_qty = 1;
                        for (var i = 0; i < taskdata.inputs.length; i++)
                        {
                            taskdata.inputs[i].required_input_qty = taskdata.inputs[i].input_qty;
                        }
                        $scope.updateTaskInfo(taskdata, index);
                    }
                });
            }
        };

        $scope.updateTaskInfo = function (model, index)
        {

            if (typeof model == 'undefined')
                return;

            $scope.productionModel.details[index].id = model.id;
            $scope.productionModel.details[index].name = model.name;
            $scope.productionModel.details[index].type = model.type.toLowerCase();
            $scope.productionModel.details[index].duration = model.duration;
            $scope.productionModel.details[index].task_amount = model.amount;
            $scope.productionModel.details[index].output_pdt_id = model.output_pdt_id;
            $scope.productionModel.details[index].product_for_name = model.product_for_name;
            $scope.productionModel.details[index].production_qty = model.production_qty;
            $scope.productionModel.details[index].output_pdt_name = model.output_pdt_name;
            $scope.productionModel.details[index].output_pdt_uom = model.output_pdt_uom;
            $scope.productionModel.details[index].production_unit_price = model.production_unit_price;
            $scope.productionModel.details[index].inputs = [];
            for (var i = 0; i < model.inputs.length; i++)
            {
                var newRow = {
                    config_input_id: model.inputs[i].id,
                    task_id: model.inputs[i].task_config_id,
                    input_pdt_id: model.inputs[i].input_pdt_id,
                    input_qty: model.inputs[i].input_qty,
                    detailIndex: index,
                    input_pdt_name: model.inputs[i].input_pdt_name,
                    input_pdt_uom: model.inputs[i].input_pdt_uom,
                    required_input_qty: model.inputs[i].required_input_qty
                };
                $scope.productionModel.details[index].inputs.push(newRow);
            }

            $scope.updateEmployeeSalary();

            return;

        }

        $scope.deleteAllowance = function (index)
        {
            $scope.productionModel.allowance.splice(index, 1);
            $scope.updateEmployeeSalary();
        };

        $scope.addNewAllowance = function ()
        {
            var newRow = {
                allowanceInfo: '',
                allowanceId: '',
                amount: '',
                name: ''
            };
            $scope.productionModel.allowance.push(newRow);
            $scope.calculatetotal();
        };

        $scope.getAllowanceList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.ALLOWANCE_MASTER_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatAllowanceModel = function (model, index)
        {
            if (model != null)
            {
                $scope.updateAllowanceInfo(model, index);
                return model.name;
            }
            return  '';
        }

        $scope.updateAllowanceInfo = function (model, index)
        {
            if ($scope.productionModel.allowance.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.productionModel.allowance[index].allowanceId = model.id;
                $scope.productionModel.allowance[index].name = model.name;
                $scope.productionModel.allowance[index].amount = model.amount;

                $scope.updateEmployeeSalary();
                return;
            }
        }

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateEmployeeSalary = function ()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.calculatetotal = function ()
        {
            var perdayamount = 0;
            var allowance_amount = 0;
            var empDuration = 0;
            $scope.productionModel.amount = 0;
            if ($scope.productionModel.is_present == true)
            {
                for (var i = 0; i < $scope.productionModel.details.length; i++)
                {
                    if ($scope.productionModel.details[i].type == 'attendance')
                    {
                        $scope.productionModel.details[i].task_amount = parseFloat(parseFloat($scope.productionModel.salary_amount) * parseFloat($scope.productionModel.details[i].duration));
                        empDuration = empDuration + parseFloat($scope.productionModel.details[i].duration);
                    } else if ($scope.productionModel.details[i].type == 'piece')
                    {
                        if ($scope.productionModel.details[i].production_unit_price == '' || $scope.productionModel.details[i].production_unit_price == '')
                            $scope.productionModel.details[i].production_unit_price = 0;
                        if ($scope.productionModel.details[i].production_qty == '' || $scope.productionModel.details[i].production_qty == '')
                            $scope.productionModel.details[i].production_qty = 0;
                        $scope.productionModel.details[i].task_amount = parseFloat(parseFloat($scope.productionModel.details[i].production_unit_price) * parseFloat($scope.productionModel.details[i].production_qty));
                    }
                    perdayamount = parseFloat(perdayamount) + parseFloat($scope.productionModel.details[i].task_amount);
                    $scope.productionModel.details[i].task_amount = $scope.productionModel.details[i].task_amount.toFixed(2);
                }
                for (var i = 0; i < $scope.productionModel.allowance.length; i++)
                {
                    if ($scope.productionModel.allowance[i].allowanceId != '' && $scope.productionModel.allowance[i].allowanceId != undefined)
                    {
                        allowance_amount = parseFloat(allowance_amount) + parseFloat($scope.productionModel.allowance[i].amount);
                    }
                }
                if ($scope.productionModel.emp_type == 'attendance')
                {
                    if (empDuration == 0 || empDuration == '' || empDuration == undefined || empDuration > 1)
                        empDuration = 1;
                    perdayamount = parseFloat(empDuration) * parseFloat($scope.productionModel.salary_amount);
                }
                $scope.productionModel.allowance_amount = allowance_amount.toFixed(2);
                $scope.productionModel.amount = parseFloat(perdayamount) + parseFloat(allowance_amount);
                $scope.productionModel.amount = $scope.productionModel.amount.toFixed(2);
            }
        };

        $scope.inputQtycalculationTimeoutPromise != null
        $scope.updateInputQty = function (index)
        {
            if ($scope.inputQtycalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.inputQtycalculationTimeoutPromise);
            }
            $scope.inputQtycalculationTimeoutPromise = $timeout($scope.calculateInputQty(index), 300);
        }

        $scope.calculateInputQty = function (index)
        {
            var production_qty = $scope.productionModel.details[index].production_qty;
            if ($scope.productionModel.details[index].inputs.length > 0)
            {
                for (var i = 0; i < $scope.productionModel.details[index].inputs.length; i++)
                {
                    $scope.productionModel.details[index].inputs[i].input_qty = parseFloat($scope.productionModel.details[index].inputs[i].required_input_qty * production_qty).toFixed(3);
                }
            }
        };

        $scope.modifyProduction = function () {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var modifyProductionParam = {};
                var headers = {};
                headers['screen-code'] = 'production';
                modifyProductionParam.date = '';
                if (typeof $scope.productionModel.date == 'object')
                {
                    $scope.productionModel.date = utilityService.parseDateToStr($scope.productionModel.date, $scope.adminService.appConfig.date_format);
                }
                modifyProductionParam.date = utilityService.changeDateToSqlFormat($scope.productionModel.date, $scope.adminService.appConfig.date_format);
                modifyProductionParam.emp_team_id = $scope.productionModel.emp_team_id;
                modifyProductionParam.emp_id = $scope.productionModel.emp_id;
                modifyProductionParam.amount = $scope.productionModel.amount;
                modifyProductionParam.is_present = $scope.productionModel.is_present;
                modifyProductionParam.details = [];
                modifyProductionParam.ot = [];
                for (var j = 0; j < $scope.productionModel.details.length; j++)
                {
                    var detailsParam = {};
                    detailsParam.task_id = $scope.productionModel.details[j].id;
                    detailsParam.output_pdt_id = $scope.productionModel.details[j].output_pdt_id;
                    detailsParam.type = $scope.productionModel.details[j].type;
                    detailsParam.duration = $scope.productionModel.details[j].duration;
                    detailsParam.production_qty = $scope.productionModel.details[j].production_qty;
                    detailsParam.production_unit_price = $scope.productionModel.details[j].production_unit_price;
                    detailsParam.amount = $scope.productionModel.details[j].task_amount;
                    detailsParam.comments = '';
                    detailsParam.inputs = [];
                    for (var loop = 0; loop < $scope.productionModel.details[j].inputs.length; loop++)
                    {
                        var inputParam = {};
                        inputParam.config_input_id = $scope.productionModel.details[j].inputs[loop].config_input_id;
                        inputParam.input_pdt_id = $scope.productionModel.details[j].inputs[loop].input_pdt_id;
                        inputParam.task_id = $scope.productionModel.details[j].id;
                        inputParam.input_qty = $scope.productionModel.details[j].inputs[loop].input_qty;
                        detailsParam.inputs.push(inputParam);
                    }
                    modifyProductionParam.details.push(detailsParam);
                }
                for (var i = 0; i < $scope.productionModel.allowance.length; i++)
                {
                    if ($scope.productionModel.allowance[i].allowanceId != '' && $scope.productionModel.allowance[i].allowanceId != undefined)
                    {
                        var allowanceParam = {};
                        allowanceParam.allowance_master_id = $scope.productionModel.allowance[i].allowanceId;
                        allowanceParam.amount = $scope.productionModel.allowance[i].amount;
                        modifyProductionParam.ot.push(allowanceParam);
                    }
                }
                adminService.editProduction(modifyProductionParam, $stateParams.id, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.formReset();
                        $state.go('app.production');
                    }
                });
            }
        };

        $scope.getProductionInfo = function () {

            if (typeof $stateParams.id != 'undefined')
            {
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getProductionDetail(getListParam, configOption).then(function (response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.productionModel.list = data.list[0];
                        $scope.initUpdateDetail();
                    }

                });
            }
        };

        $scope.getProductionInfo();
        $scope.getEmployeeTeam();
        $scope.setDate();
    }]);




