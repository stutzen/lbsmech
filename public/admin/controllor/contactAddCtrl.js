

app.controller('contactAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST','$stateParams','$window', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST,$stateParams,$window) {

    $scope.contactModel = {
        id: '',
        fname: '',
        lname:'',
        isActive: true,
        email:'',
        phone:'',
        company:'',
        pin_code:'',
        notes: '',
        companyList :[],
        source : '',
        sourceList:[],
        companyInfo: '',
        attachment: []
    }

    $scope.validationFactory = ValidationFactory;

//    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
//    $scope.getNavigationBlockMsg = function()
//    {
//        if (typeof $scope.add_lead_form != 'undefined' && typeof $scope.add_lead_form.$pristine != 'undefined' && !$scope.add_lead_form.$pristine)
//        {
//            return $scope.NAVIGATION_BLOCKER_MSG;
//        }
//
//        return "";
//    }
    $scope.isDataSavingProcess = false;
    $scope.formReset = function()
    {
        $scope.add_lead_form.$setPristine();
        if (typeof $scope.add_lead_form != 'undefined')
        {
            $scope.contactModel.fname = "";
            $scope.contactModel.lname = "";
            $scope.contactModel.pin_code = "";
            $scope.contactModel.email = "";
            $scope.contactModel.phone = "";
            $scope.contactModel.company = "";
            $scope.contactModel.notes = "";
            $scope.contactModel.companyInfo = "";
            $scope.contactModel.source = '';
            $scope.contactModel.lead = '';
            $scope.contactModel.empInfo = '';
            $scope.contactModel.isActive = true;
        }
    }
    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

    $scope.getSourceList = function() {
        var getSourceListParam = {};
        getSourceListParam.is_active = 1;
        $scope.contactModel.isLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getSourceList(getSourceListParam,configOption).then(function(response)
        {
            if (response.data.success === true)
            {
                $scope.contactModel.sourceList = response.data.list;                    
            }
            $scope.contactModel.isLoadingProgress = false;
        });
    };  
    
    $scope.createcontact = function() {

        if ($scope.isDataSavingProcess)
        {
            return;
        }

        $scope.isDataSavingProcess = true;
        var addContactParam = {};
        var headers = {};
        headers['screen-code'] = 'contact';
        addContactParam.id = 0;
        addContactParam.fname = $scope.contactModel.fname;
        addContactParam.lname = $scope.contactModel.lname;
        addContactParam.email = $scope.contactModel.email;
        addContactParam.phone = $scope.contactModel.phone;
        addContactParam.pin_code = $scope.contactModel.pin_code;
        addContactParam.company_id = $scope.contactModel.companyInfo.id;
        addContactParam.contact_owner_id = $scope.contactModel.empInfo.id;
        addContactParam.notes = $scope.contactModel.notes;
        addContactParam.lead_type_id = $scope.contactModel.lead;
        addContactParam.is_active = $scope.contactModel.isActive == true ? 1 : 0;
        addContactParam.source_id = $scope.contactModel.source;
        addContactParam.attachment = [];
        adminService.saveContact(addContactParam, headers).then(function(response)
        {
            if (response.data.success == true)
            {
                $scope.formReset();
                $window.localStorage.clear()
                $state.go('app.contactEdit' ,{
                    'id': response.data.id
                }, {
                    'reload': true
                });
            }
            $scope.isDataSavingProcess = false;
        });
    };
    $scope.getLeadTypeList = function()
    {
        $scope.contactModel.isLoadingProgress = true;
        var leadtypeListParam = {};
        leadtypeListParam.is_active = 1;
        leadtypeListParam.type = 'lead';
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getLeadTypeList(leadtypeListParam, configOption).then(function(response)
        {
            if(response.data.success == true)
            {
                var data = response.data;
                $scope.contactModel.leadList = data.list;
            }
            $scope.contactModel.isLoadingProgress = false;
        });
            
    }
    $scope.getCompanyList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function(responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.formatCompanyModel = function(model)
    {
        if (model != null)
        {        
            return model.name ;     
        }
        return  '';
    };
    $scope.showCompanyPopup = false;
    $scope.companyAdd = function()
    {
        //        $scope.showCompanyPopup = true;
        $state.go('app.contactCompanyAdd');
        //$rootScope.$broadcast('companyInfo', $scope.contactModel.companyInfo);                  
    }
    
    $scope.$on("updateSavedCompanyDetail", function (event, companyDetail)
    {
        if (typeof companyDetail === 'undefined')
            return;
        $scope.contactModel.companyInfo = companyDetail;
        $scope.closeCompanyPopup();
    });
      
    $scope.closeCompanyPopup = function()
    {
        $scope.showCompanyPopup = false;
    }
    
    $scope.getEmployeeList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.fname = val;
        autosearchParam.is_active = 1;
        if (autosearchParam.fname != '')
        {
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }
    };

    $scope.formatEmployeeModel = function(model)
    {
        if (model != null)
        {
            if (model.f_name != undefined && model.id != undefined && model.emp_code != undefined)
            {
                return model.f_name + '(' + model.id + ',' + model.emp_code + ')';
            }
            else if (model.f_name != undefined && model.id != undefined)
            {
                return model.f_name + '(' + model.id + ')';
            }
        }
        return  '';
    }; 
    $scope.getSourceList();
    $scope.getLeadTypeList();
    $scope.hasDependentDataLoaded = true;
    $scope.localStorageCount = 0;
    $scope.localStorageRenterCount = 0;
    $scope.localStorageRetrieveCount = 0;
    $scope.localStorageFormName = 'add_lead_form';


    $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

        if ($scope.localStorageFormName == data.formName)
        {
            $scope.localStorageCount = data.fieldCount;
        }
    });

    $scope.$on('updateTypeaheadFieldValue', function(event, data) {

        if (data.fieldName == 'add_lead_form-employee_dropdown')
        {
            $scope.contactModel.empInfo = data.value;
        }
        if (data.fieldName == 'add_lead_form-company_dropdown')
        {
            $scope.contactModel.companyInfo = data.value;
        }
            
    });

    $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {

        if ($scope.localStorageFormName == formName)
        {
            $scope.localStorageRenterCount++;
        }
        if ($scope.localStorageRenterCount >= $scope.localStorageCount)
        {
            console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
            $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
        }

    });


    $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {

        if ($scope.localStorageFormName == formName)
        {
            $scope.localStorageRetrieveCount++;
        }
        //            if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
        //            {
        //                $scope.initTableFilter();
        //            }
        console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
    });

    $scope.init = function()
    {
        $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

    };

    $scope.$on('onLocalStorageReadyEvent', function() {
        $timeout(function() {
            $scope.init();
        }, 300);
    });
    $scope.checkId = function()
    {
        if($stateParams.company_id != '' && $stateParams.company_id != undefined)
        {
            var id = $stateParams.company_id;
            $scope.getCompanyInfo(id);
            
        }    
    }
    $scope.getCompanyInfo = function(id)
    {
        $scope.companyLoaded = false;
        if (id != 'undefined' && id != '')
        {
            
            var companyListParam = {};
            companyListParam.id = $stateParams.company_id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCompanyList(companyListParam, configOption).then(function(response) {

                var data = response.data;
                if (data.list.length > 0)
                {
                    $scope.companyInfo = data.list[0];
                    $scope.companyLoaded = true;
                    $scope.updateContactInfo();
                }
            });
        }
    };
    $scope.updateContactInfo = function()
    {
        if($stateParams.company_id != '' && $stateParams.company_id != undefined)
        {
            $scope.contactModel.companyInfo ={
                name :  $scope.companyInfo.name,
                id : $scope.companyInfo.id              
            }
        } 
    }
     $scope.checkId();
    $scope.init();
    
}]);





