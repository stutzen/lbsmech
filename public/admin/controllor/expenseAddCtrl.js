
app.controller('expenseAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.expenseAddModel =
                {
                    "id": 0,
                    "customerInfo": {},
                    "inputType": "",
                    "list": [],
                    "taxList": []
                };


        $scope.onFocus = function(e) {
            $timeout(function() {
                $(e.target).trigger('input');
            });
        };
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.order_add_form != 'undefined' && typeof $scope.order_add_form.$pristine != 'undefined' && !$scope.order_add_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {

            $scope.order_add_form.$setPristine();
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.fname = val;
            autosearchParam.is_sale = 1;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.taxList;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                return model.fname;
            }
            return  '';
        };


        $scope.deleteTax = function(id)
        {
            var index = -1;
            for (var i = 0; i < $scope.expenseAddModel.taxList.length; i++) {
                if (id == $scope.expenseAddModel.taxList[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.expenseAddModel.taxList.splice(index, 1);
            $scope.updateInvoiceTotal();
        };

        $scope.totalcalculationTimeoutPromise != null
        $scope.updateInvoiceTotal = function()
        {
            if ($scope.totalcalculationTimeoutPromise != null)
            {
                $timeout.cancel($scope.totalcalculationTimeoutPromise);
            }
            $scope.totalcalculationTimeoutPromise = $timeout($scope.calculatetotal, 300);
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                $scope.getCustomerList();
            }
            if ($scope.isLoadedTax == true && $stateParams.id != "")
            {
                $scope.updateInvoiceDetails();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.getCustomerList, 300);
                //$scope.getCustomerList();
            }

        }

        $scope.formatTaxModel = function(model, index)
        {
            if (model != null)
            {
                $scope.updateTaxInfo(model, index);
                return model.tax_name;
            }
            return  '';
        }

        $scope.updateTaxInfo = function(model, index)
        {
            if ($scope.expenseAddModel.list.length != 0)
            {
                if (typeof model == 'undefined')
                    return;

                $scope.expenseAddModel.list[index].id = model.id;
                $scope.expenseAddModel.list[index].taxName = model.tax_name;
                $scope.expenseAddModel.list[index].tax_no = model.tax_no;
                $scope.expenseAddModel.list[index].tax_percentage = model.tax_percentage;
                $scope.expenseAddModel.list[index].taxAmount = model.tax_applicable_amt;
                $scope.expenseAddModel.list[index].expense_amt = 0.00;
                $scope.expenseAddModel.list[index].rowtotal = model.tax_applicable_amt;
                $scope.updateInvoiceTotal();

                if ((index + 1) == $scope.expenseAddModel.list.length)
                {
                    var newRow = {
                        "id": '',
                        "taxName": '',
                        "tax_no": '',
                        "tax_percentage": '',
                        "taxAmount": '',
                        "expense_amt": '',
                        "rowtotal": ''
                    }
                    $scope.expenseAddModel.list.push(newRow);
                }
                return;
            }
            else
            {
                var newRow = {
                    "id": '',
                    "taxName": '',
                    "tax_no": '',
                    "tax_percentage": '',
                    "taxAmount": '',
                    "expense_amt": '',
                    "rowtotal": ''
                }
                $scope.expenseAddModel.list.push(newRow);
            }
        }

        $scope.insertTax = function()
        {
            var newRow = {
                "id": '',
                "taxName": '',
                "tax_no": '',
                "tax_percentage": '',
                "taxAmount": '',
                "expense_amt": '',
                "rowtotal": ''
            }
            $scope.expenseAddModel.list.push(newRow);
        }


        $scope.taxList = [];
        $scope.calculatetotal = function()
        {
            var subTotal = 0;
            for (var i = 0; i < $scope.expenseAddModel.list.length; i++)
            {
                $scope.expenseAddModel.list[i].rowtotal = $scope.expenseAddModel.list[i].expense_amt + $scope.expenseAddModel.list[i].taxAmount;
                subTotal += parseFloat($scope.expenseAddModel.taxList[i].rowtotal);
                $scope.expenseAddModel.taxList[i].rowtotal = $scope.expenseAddModel.taxList[i].rowtotal.toFixed(2);
            }
            $scope.expenseAddModel.subtotal = parseFloat(subTotal).toFixed(2);

        }

//        $scope.getTaxList = function(val)
//        {
//            var autosearchParam = {};
//            autosearchParam.tax_name = val;
//
//            return $httpService.get(APP_CONST.API.TAX_LIST, autosearchParam, false).then(function(responseData)
//            {
//                var data = responseData.data.list;
//                var hits = data;
//                if (hits.length > 10)
//                {
//                    hits.splice(10, hits.length);
//                }
//                return hits;
//            });
//        };

        $scope.getTaxList = function() {

            var getListParam = {};
            getListParam.id = '';
            getListParam.name = '';
            
            adminService.getTaxList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.expenseAddModel.taxList = data;

            });

        };

        $scope.getCategoryList = function() {

            var getListParam = {};
            getListParam.id = '';
            getListParam.name = '';

            adminService.getCategoryList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.expenseAddModel.categoryList = data;

            });

        };

        $scope.createExpense = function(val)
        {
            $scope.isDataSavingProcess = true;
            if ($scope.expenseAddModel.taxList[0].name != '' && $scope.expenseAddModel.taxList[0].name != null &&
                    $scope.expenseAddModel.taxList[0].tax_percentage != '' && $scope.expenseAddModel.taxList[0].tax_percentage != null &&
                    $scope.expenseAddModel.taxList[0].expense_amt != '' && $scope.expenseAddModel.taxList[0].expense_amt != null &&
                    $scope.expenseAddModel.taxList[0].rowtotal != '' && $scope.expenseAddModel.taxList[0].rowtotal != null)
            {
                var createExpenseParam = {};
                createExpenseParam.attacement = '';
                createExpenseParam.amount = $scope.expenseAddModel.total;
                createExpenseParam.payment_type = '';
                createExpenseParam.payment_date = '';
                createExpenseParam.customer_id = $scope.expenseAddModel.customerInfo.id
                createExpenseParam.category_id = $scope.expenseAddModel.discountPercentage;
                createExpenseParam.tax_id = '';
                createExpenseParam.comments = '';
                createExpenseParam.tax_amount = '';
                createExpenseParam.refno = '';
                createExpenseParam.additional_notes = '';
                createExpenseParam.payment = [];
                for (var i = 0; i < $scope.expenseAddModel.taxList.length; i++)
                {
                    var taxItem = {};
                    taxItem.amount = $scope.expenseAddModel.taxList[i].expense_amt;
                    taxItem.tax_id = $scope.expenseAddModel.taxList[i].id;
                    taxItem.tax_amount = $scope.expenseAddModel.taxList[i].taxAmount;
                    taxItem.expense_name = $scope.expenseAddModel.taxList[i].taxAmount;
                }
                var duedate = utilityService.parseStrToDate($scope.expenseAddModel.duedate);
                createExpenseParam.duedate = $filter('date')(duedate, 'yyyy-MM-dd');

                adminService.addInvoice(createExpenseParam).then(function(response) {
                    if (response.data.success == true)
                    {
                        $scope.isDataSavingProcess = false;
                        $scope.formReset();
                        $state.go('app.expense');
                    }
                    else
                    {
                        $scope.isDataSavingProcess = false;
                    }
                });
            }
            else
            {
                sweet.show('Oops...', 'Choose Atleast One Tax .. ', 'error');
                $scope.isDataSavingProcess = false;
            }
        };
        $scope.getTaxList();
        $scope.getCategoryList();
        $scope.insertTax();

    }]);




