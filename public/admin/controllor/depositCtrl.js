





app.controller('depositCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.depositModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            date: '',
            particular: '',
            category: '',
            created_by: '',
            incomeCategoryInfo: '',
            amount: '',
            is_active: 1,
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };
        $scope.searchFilter = {
            accountid: '',
            customerid: '',
            name: '',
            start: '',
            fromdate: '',
            todate: '',
            depositInfo: '',
            userInfo: ''

        };
        $scope.validationFactory = ValidationFactory;
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                incomeCategoryInfo: '',
                user: ''
            };
          //  $scope.searchFilter.fromdate = $scope.currentDate;
            $scope.initTableFilter();
        }
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.depositModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.currentDate = new Date();
       // $scope.searchFilter.fromdate = $scope.currentDate;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };



        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.selectDepositId = '';
        $scope.selectDepositVoucher_number = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id, voucher_number)
        {
            $scope.selectDepositId = id;
            $scope.selectDepositVoucher_number = voucher_number;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;

        };

        $scope.deleteDepositInfo = function( )
        {

            if (!$scope.isdeleteLoadingProgress)
            {
                $scope.isdeleteLoadingProgress = true;
                if ($scope.selectDepositVoucher_number === '' || $scope.selectDepositVoucher_number == 0)
                {
                    var getListParam = {};
                    getListParam.id = $scope.selectDepositId;
                    var headers = {};
                    headers['screen-code'] = 'transdeposit';
                    adminService.deleteDeposit(getListParam, getListParam.id, headers).then(function(response)
                    {
                        var data = response.data;
                        if (data.success === true)
                        {
                            $scope.closePopup();
                            $scope.getList();
                        }
                        $scope.isdeleteLoadingProgress = false;
                    });
                }
                else
                {
                    var getListParam = {};
                    getListParam.id = $scope.selectDepositVoucher_number;
                    var headers = {};
                    headers['screen-code'] = 'transdeposit';
                    adminService.deleteVoucher(getListParam, getListParam.id, headers).then(function(response)
                    {
                        var data = response.data;
                        if (data.success === true)
                        {
                            $scope.closePopup();
                            $scope.getList();
                        }
                        $scope.isdeleteLoadingProgress = false;
                    });
                }
                ;
            }
        };
        $scope.$watch('searchFilter.incomeCategoryInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });
        $scope.getList = function() {

            $scope.depositModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'transdeposit';
            // getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, 'yyyy-MM-dd');
            //getListParam.to_Date = $filter('date')($scope.searchFilter.todate, 'yyyy-MM-dd');


            getListParam.from_Date = $scope.searchFilter.fromdate;
            getListParam.to_Date = $scope.searchFilter.todate;

            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.adminService.appConfig.date_format);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.adminService.appConfig.date_format);

            }
            getListParam.id = '';
            getListParam.account_id = '';
            getListParam.customer_id = '';
            getListParam.created_by = $scope.searchFilter.user;
            if ($scope.searchFilter.incomeCategoryInfo != null && typeof $scope.searchFilter.incomeCategoryInfo != 'undefined' && typeof $scope.searchFilter.incomeCategoryInfo.id != 'undefined')
            {
                getListParam.account_category = $scope.searchFilter.incomeCategoryInfo.name;
            }
            else
            {
                getListParam.account_category = '';
            }
            getListParam.name = '';
            getListParam.start = ($scope.depositModel.currentPage - 1) * $scope.depositModel.limit;
            getListParam.limit = $scope.depositModel.limit;
            getListParam.is_active = 1;
            $scope.depositModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getDepositList(getListParam, configOption, headers).then(function(response) {
//                var balanceamt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.depositModel.list = data;

                    for (var i = 0; i < $scope.depositModel.list.length; i++)
                    {
                        $scope.depositModel.list[i].newdate = utilityService.parseStrToDate($scope.depositModel.list[i].transaction_date);
                        $scope.depositModel.list[i].transaction_date = utilityService.parseDateToStr($scope.depositModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                    }

                    $scope.depositModel.total = data.total;
                }
                $scope.depositModel.isLoadingProgress = false;
            });

        };

        $scope.getIncomeCategoryList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = 'income';
            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatincomeCategoryModel = function(model) {

            if (model !== null && model != undefined)
            {

                return model.name;
            }
            return  '';
        };
        $scope.getUserList = function() {

            var getListParam = {};
            getListParam.date = '';
            getListParam.particular = '';
            getListParam.category = '';
            getListParam.credited_by = '';
            getListParam.amount = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserList(getListParam, configOption).then(function(response) {
                var data = response.data;
                $scope.depositModel.userlist = data.list;

            });

        };
        $scope.getListPrint = function(val)
        {
            var headers = {};
            headers['screen-code'] = 'salesinvoice';

            var getListParam = {};
            getListParam.Id = "";
            getListParam.start = 0;
            $scope.depositModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;

            adminService.getDepositList(getListParam, configOption, headers).then(function(response) {

                if (response.data.success)
                {
                    var data = response.data.list;
                    $scope.depositModel.printList = data;
                }
                $scope.depositModel.isLoadingProgress = false;
            });
        };


        $scope.getList();
        $scope.getUserList();
        //   $scope.getListPrint();
    }]);




