

app.controller('grnreportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.grnReportModel = {
        currentPage: 1,
        total: 0,
        total_Amount: 0,
        limit: 10,
        list: [],
        length: [],
        data: [],
        date: '',
        customer_id: '',
        status: '',
        serverList: null,
        isLoadingProgress: false,
        isSearchLoadingProgress: false
           
    };
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.grnReportModel.limit = $scope.pagePerCount[0];
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.validationFactory = ValidationFactory;
    $scope.openDate = function(index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index === 1)
        {
            $scope.toDateOpen = true;
        }

    };

    $scope.searchFilter = {
        fromdate: '',
        todate: '',
        customerInfo:''

    };
    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            customerInfo:''
        };
        $scope.grnReportModel.list = [];
    }
    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise !== null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    $scope.validateDateFilterData = function()
    {
        var retVal = false;
        if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
        {
            retVal = true;
        }
        return retVal;
    };

       
    $scope.getCustomerList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.search = val;
        if (autosearchParam.search != '')
        {
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        }
    };
    $scope.formatCustomerModel = function(model)
    {
        if (model != null)
        {
            if (model.fname != undefined && model.phone != undefined && model.email != undefined && model.company  != undefined)
            {
                return model.fname + '(' + model.phone + ',' + model.email + ',' + model.company + ')';
            }
            else if (model.fname != undefined && model.phone != undefined && model.company  != undefined)
            {
                return model.fname + '(' + model.phone + ',' + model.company + ')';
            }
        }
        return  '';
    };
     $scope.show = function(index)
    {

        if ($scope.showDetails[index])
        {
            $scope.showDetails[index] = false;
        }
        else
            $scope.showDetails[index] = true;

    };

    $scope.getList = function() {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'grnreport';
            
        if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
        {
            getListParam.customer_id = $scope.searchFilter.customerInfo.id;
        }
        else
        {
            getListParam.customer_id = '';
        }
        if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = getListParam.to_date;
        }
        else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
        {
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            getListParam.to_date = getListParam.from_date;
        }
        else
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
        }

        getListParam.start = 0;
        getListParam.limit = 0;
        getListParam.show_all=1;
         $scope.grnReportModel.isLoadingProgress = true;
        $scope.showDetails = [];
        $scope.grnReportModel.isSearchLoadingProgress = true;
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getGRNReport(getListParam, configOption,headers).then(function(response)
        {
                
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.grnReportModel.list = data;

                    for (var i = 0; i < $scope.grnReportModel.list.length; i++)
                    {   
                        $scope.grnReportModel.list[i].flag = i;
                        $scope.showDetails[i] = false;
                       
                        for (var j = 0; j < $scope.grnReportModel.list[i].data.length; j++)
                        {
                            $scope.grnReportModel.list[i].data[j].newdate = utilityService.parseStrToDate($scope.grnReportModel.list[i].data[j].date);
                            $scope.grnReportModel.list[i].data[j].date = utilityService.parseDateToStr($scope.grnReportModel.list[i].data[j].newdate, $scope.adminService.appConfig.date_format);
                            $scope.grnReportModel.list[i].data[j].flag = i;
                            
                        }
                    } 
                //$scope.grnReportModel.total = data.total;
            }
            $scope.grnReportModel.isLoadingProgress = false;
            $scope.grnReportModel.isSearchLoadingProgress = false;
        });

    };
   

    $scope.print = function()
    {
        $window.print();
    };
        
//        $scope.getList();
        
}]);




