app.controller('minstockListCtrl', ['$scope', '$rootScope', 'adminService', 'APP_CONST', 'ValidationFactory', '$httpService', 'utilityService', '$window', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, APP_CONST, ValidationFactory, $httpService, utilityService, $window, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.minstockModel = {
            scopeId: "",
            configId: 5,
            type: "",
            currentPage: 1,
            total: 0,
            productInfo: '',
            limit: 4,
            list: [],
            status: '',
            currencyFormat: '',
            serverList: null,
            isLoadingProgress: true
        };

        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.minstockModel.limit = $scope.pagePerCount[0];
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            id: ''


        };
        $scope.searchFilter.fromdate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };


        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                id: '',
                name: ''
            };
            $scope.minstockModel.productInfo = "";
            //$scope.minstockModel.list =[];
            $scope.initTableFilter();
        }

        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getMinstockListInfo, 300);
        }

        $scope.getMinstockListInfo = function() {

            $scope.minstockModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'minstock';
            //materialListParam.id = $scope.minstockModel.productInfo.id;
            if ($scope.minstockModel.productInfo != null && typeof $scope.minstockModel.productInfo != 'undefined' && typeof $scope.minstockModel.productInfo.id != 'undefined')
            {
                materialListParam.id = $scope.minstockModel.productInfo.id;
            }
            else
            {
                materialListParam.id = '';
            }
            materialListParam.start = ($scope.minstockModel.currentPage - 1) * $scope.minstockModel.limit;
            materialListParam.limit = $scope.minstockModel.limit;

            adminService.getMinstockList(materialListParam, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.minstockModel.list = data.list;
                    for (var i = 0; i < $scope.minstockModel.list.length; i++)
                    {
                        $scope.minstockModel.list[i].newdate = utilityService.parseStrToDate($scope.minstockModel.list[i].date);
                        $scope.minstockModel.list[i].date = utilityService.parseDateToStr($scope.minstockModel.list[i].newdate, $scope.dateFormat);
                    }
                    $scope.minstockModel.total = data.total;
                }
                $scope.minstockModel.isLoadingProgress = false;
            });

        };
        $scope.getProductList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.type = "Product";
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatproductModel = function(model) {

            if (model !== null)
            {
                $scope.searchFilter.id = model.id;
                return model.name;
            }
            return  '';
        };
        $scope.$watch('searchFilter.productInfo', function(newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });
        $scope.print = function()
        {
            $window.print();
        };

        $scope.getMinstockListInfo();
        $scope.getProductList();

    }]);




