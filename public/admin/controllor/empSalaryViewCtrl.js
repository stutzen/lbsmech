
app.controller('empSalaryViewCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', 'sweet','$stateParams', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, sweet,$stateParams) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.empSalaryModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            teamid: '',
            length: [],
            taskdetail: [],
            date: '',
            serverList: null,
            salarytype: '',
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            monthName: '',
            teamName: '',
            totalweeksalary: '0.00',
            totalesi: '0.00',
            totalpfamt: '0.00',
            totaldedamt: '0.00',
            totalot: '0.00',
            totalallowable: '0.00',
            totalAmt: '0.00',
            totalqty: '0.00'
        };
        $scope.showDetail = false;
        $scope.salarytype = '';
        $scope.teamname = '';
        $scope.fromdate = '';
        $scope.todate = '';
        $scope.month = '';
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.empSalaryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.isDataSavingProcess = false;
        $scope.searchFilterValue = "";
        $scope.initTableFilterTimeoutPromise = null;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
//        $scope.monthList = [{"id": '1', "name": 'January'}, {"id": '2', "name": 'February'}, {"id": '3', "name": 'March'},
//            {"id": '4', "name": 'April'}, {"id": '5', "name": 'May'}, {"id": '6', "name": 'June'}, {"id": '7', "name": 'July'},
//            {"id": '8', "name": 'August'}, {"id": '9', "name": 'September'}, {"id": '10', "name": 'October'},
//            {"id": '11', "name": 'November'}, {"id": '12', "name": 'December'}]
//        $scope.monthwise = false;
//        $scope.date = false;
//        $scope.attendance = function()
//        {
//            if ($scope.empSalaryModel.salarytype == 'attendance')
//            {
//                $scope.monthwise = true;
//                $scope.date = false;
//            }
//            if ($scope.empSalaryModel.salarytype == 'piece')
//            {
//                $scope.searchFilter.month = '';
//                $scope.monthwise = false;
//                $scope.date = true;
//            }
//        }

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empSalary';
            getListParam.preparation_id = $stateParams.id;
            $scope.empSalaryModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeSalaryDetail(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.empSalaryModel.list = data;
                    $scope.salarytype = $scope.empSalaryModel.list[0].salary_type;
                    $scope.teamname = $scope.empSalaryModel.list[0].team_name;
                    $scope.fromdate = $scope.empSalaryModel.list[0].from_date;
                    $scope.todate = $scope.empSalaryModel.list[0].to_date;
                    $scope.month = $scope.empSalaryModel.list[0].salary_month;
                    var weeksalary = 0;
                    var esi = 0;
                    var pf = 0;
                    var dedamt = 0;
                    var ot = 0;
                    var allow = 0;
                    var total = 0;
                    var qty = 0;
                    for (var i = 0; i < $scope.empSalaryModel.list.length; i++)
                    {
                        qty = qty + parseFloat($scope.empSalaryModel.list[i].overall_qty);
                        $scope.empSalaryModel.list[i].week_salary = parseFloat($scope.empSalaryModel.list[i].week_salary).toFixed(2);
                        weeksalary = weeksalary + parseFloat($scope.empSalaryModel.list[i].week_salary);
                        $scope.empSalaryModel.list[i].esi_amount = parseFloat($scope.empSalaryModel.list[i].esi_amount).toFixed(2);
                        esi = esi + parseFloat($scope.empSalaryModel.list[i].esi_amount);
                        $scope.empSalaryModel.list[i].pf_amount = parseFloat($scope.empSalaryModel.list[i].pf_amount).toFixed(2);
                        pf = pf + parseFloat($scope.empSalaryModel.list[i].pf_amount);
                        $scope.empSalaryModel.list[i].deduction_amount = parseFloat($scope.empSalaryModel.list[i].deduction_amount).toFixed(2);
                        dedamt = dedamt + parseFloat($scope.empSalaryModel.list[i].deduction_amount);
                        $scope.empSalaryModel.list[i].ot_amount = parseFloat($scope.empSalaryModel.list[i].ot_amount).toFixed(2);
                        ot = ot + parseFloat($scope.empSalaryModel.list[i].ot_amount);
                        $scope.empSalaryModel.list[i].fixed_allowance_amount = parseFloat($scope.empSalaryModel.list[i].fixed_allowance_amount).toFixed(2);
                        allow = allow + parseFloat($scope.empSalaryModel.list[i].fixed_allowance_amount);
                        $scope.empSalaryModel.list[i].total_amount = parseFloat($scope.empSalaryModel.list[i].week_salary)
                                - parseFloat($scope.empSalaryModel.list[i].deduction_amount) + parseFloat($scope.empSalaryModel.list[i].ot_amount)
                                + parseFloat($scope.empSalaryModel.list[i].fixed_allowance_amount);
                        $scope.empSalaryModel.list[i].total_amount = parseFloat($scope.empSalaryModel.list[i].total_amount).toFixed(2);
                        total = total + parseFloat($scope.empSalaryModel.list[i].total_amount);
                        $scope.empSalaryModel.totalweeksalary = parseFloat(weeksalary).toFixed(2);
                        $scope.empSalaryModel.totalesi = parseFloat(esi).toFixed(2);
                        $scope.empSalaryModel.totalpfamt = parseFloat(pf).toFixed(2);
                        $scope.empSalaryModel.totaldedamt = parseFloat(dedamt).toFixed(2);
                        $scope.empSalaryModel.totalot = parseFloat(ot).toFixed(2);
                        $scope.empSalaryModel.totalallowable = parseFloat(allow).toFixed(2);
                        $scope.empSalaryModel.totalAmt = parseFloat(total).toFixed(2);
                        $scope.empSalaryModel.totalqty = parseFloat(qty).toFixed(2);
                    }

                }
                $scope.empSalaryModel.isLoadingProgress = false;
            });
        };
        $scope.print = function()
        {
            $window.print();
        };
        $scope.getList();
    }]);







