app.controller('stockadjustmentCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$timeout', function ($scope, $rootScope, adminService, ValidationFactory, utilityService, $httpService, APP_CONST, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.stockadjustmentModel = {
            scopeId: "",
            configId: 5,
            type: "",
            from_date: "",
            currentPage: 1,
            total: 0,
            productInfo: '',
            product_id: '',
            id: '',
            limit: 4,
            list: [],
            status: '',
//        currencyFormat:'',
//        serverList: null,
            isLoadingProgress: true
        };

        $scope.adminService = adminService;

        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.stockadjustmentModel.limit = $scope.pagePerCount[0];
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;


        $scope.validationFactory = ValidationFactory;
        $scope.searchFilter = {
            from_date: '',
            to_date: '',
            id: '',
            product_id: ''

        };
        $scope.stockadjustmentModel.productInfo = "";
        $scope.searchFilter.from_date = $scope.currentDate;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };


        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                from_date: '',
                to_date: '',
                id: '',
                product_id: ''
            };
            $scope.stockadjustmentModel.list = [];
            $scope.stockadjustmentModel.productInfo = "";
            $scope.stockadjustment_form.$setPristine();
//            $scope.searchFilterValue = "";
//            $scope.initTableFilter();
            $scope.searchFilter.from_date = $scope.currentDate;
            $scope.initTableFilter();
        }

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getStockadjustmentListInfo, 300);
        }


        $scope.getProductList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatproductModel = function (model) {

            if (model !== null)
            {
                $scope.searchFilter.id = model.id;
                return model.name;
            }
            return  '';
        };
        $scope.$watch('searchFilter.productInfo', function (newVal, oldVal)
        {
            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
            {
                $scope.initTableFilter();
            }
        });

        $scope.getStockadjustmentListInfo = function () {

            // $scope.stockadjustmentModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'stockadjustment';
            materialListParam.type = 1;
            //materialListParam.type = " stock_adjust";
            //materialListParam.id = $scope.stockadjustmentModel.productInfo.id;
            materialListParam.id = '';
            materialListParam.from_date = $scope.searchFilter.from_date;
            materialListParam.to_date = $scope.searchFilter.to_date;
            if ($scope.searchFilter.to_date != '' && ($scope.searchFilter.from_date == null || $scope.searchFilter.from_date == ''))
            {
                if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                {
                    materialListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.dateFormat);

                    materialListParam.to_date = utilityService.changeDateToSqlFormat(materialListParam.to_date, $scope.dateFormat);
                    materialListParam.from_date = materialListParam.to_date;
                }
            } else if ($scope.searchFilter.from_date != '' && ($scope.searchFilter.to_date == null || $scope.searchFilter.to_date == ''))
            {
                if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                {
                    materialListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.dateFormat);

                    materialListParam.from_date = utilityService.changeDateToSqlFormat(materialListParam.from_date, $scope.dateFormat);
                    materialListParam.to_date = materialListParam.from_date;
                }
            } else if ($scope.searchFilter.to_date != null && $scope.searchFilter.to_date != '' && $scope.searchFilter.from_date != null && $scope.searchFilter.from_date != '')
            {
                if ($scope.searchFilter.to_date != null && typeof $scope.searchFilter.to_date == 'object')
                {
                    materialListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.to_date, $scope.dateFormat);
                }
                if ($scope.searchFilter.from_date != null && typeof $scope.searchFilter.from_date == 'object')
                {
                    materialListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.from_date, $scope.dateFormat);
                }
                materialListParam.to_date = utilityService.changeDateToSqlFormat(materialListParam.to_date, $scope.dateFormat);
                materialListParam.from_date = utilityService.changeDateToSqlFormat(materialListParam.from_date, $scope.dateFormat);
            }
            //materialListParam.product_id = $scope.stockadjustmentModel.productInfo.product_id;
            if ($scope.stockadjustmentModel.productInfo != null && typeof $scope.stockadjustmentModel.productInfo != 'undefined' && typeof $scope.stockadjustmentModel.productInfo.id != 'undefined')
            {
                materialListParam.product_id = $scope.stockadjustmentModel.productInfo.id;
            } else
            {
                materialListParam.product_id = '';
            }
            materialListParam.is_active = 1;
            // materialListParam.product_id = $scope.searchFilter.product_id;
            materialListParam.start = ($scope.stockadjustmentModel.currentPage - 1) * $scope.stockadjustmentModel.limit;
            materialListParam.limit = $scope.stockadjustmentModel.limit;
            $scope.stockadjustmentModel.isLoadingProgress = true;
            adminService.getStockadjustmentList(materialListParam, headers).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.stockadjustmentModel.list = data.list;
                    for (var i = 0; i < $scope.stockadjustmentModel.list.length; i++)
                    {
                        $scope.stockadjustmentModel.list[i].newdate = utilityService.parseStrToDate($scope.stockadjustmentModel.list[i].created_at);
                        $scope.stockadjustmentModel.list[i].created_at = utilityService.parseDateToStr($scope.stockadjustmentModel.list[i].newdate, $rootScope.appConfig.date_format);
                    }
                    $scope.stockadjustmentModel.total = data.total;
                }
                $scope.stockadjustmentModel.isLoadingProgress = false;
            });

        };

        $scope.getStockadjustmentListInfo();

    }]);




