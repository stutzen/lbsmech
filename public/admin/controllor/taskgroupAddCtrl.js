

app.controller('taskgroupAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST','sweet', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST,sweet) {

        $scope.taskGroupModel = {
            name: '',
            version: '',
            list: [],
            workCenter: [],
            departmentList : [],
            type : 'internal'
        }

        $scope.validationFactory = ValidationFactory;

        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.add_taskGrp_form != 'undefined' && typeof $scope.add_taskGrp_form.$pristine != 'undefined' && !$scope.add_taskGrp_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.add_taskGrp_form.$setPristine();
            if (typeof $scope.add_taskGrp_form != 'undefined')
            {
                $scope.taskGroupModel.name = "";
                $scope.taskGroupModel.version = "";
                $scope.taskGroupModel.isActive = true;
                $scope.taskGroupModel.list = [];
            }
        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.dateOpen = false;
        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.dateOpen = true;
            }
        };
        $scope.taskgroupcheck = function()
        {
            if($scope.taskGroupModel.type == 'external' && $scope.taskGroupModel.list.length > 1)
            {
                sweet.show("In external select only one task group");
            }
            else
            {
                $scope.createTaskGroup();
            }    
        }

        $scope.createTaskGroup = function() {

            if ($scope.isDataSavingProcess)
            {
                return;
            }
            $scope.isDataSavingProcess = true;
            var addTaskGroupParam = {};
            var headers = {};
            var count = 0;
            headers['screen-code'] = 'taskgroup';
            addTaskGroupParam.id = 0;
            addTaskGroupParam.name = $scope.taskGroupModel.taskgroupName;
            addTaskGroupParam.type = $scope.taskGroupModel.type;
            addTaskGroupParam.code = $scope.taskGroupModel.version;
            addTaskGroupParam.is_active = 1;
            addTaskGroupParam.detail = [];
            for (var i = 0; i < $scope.taskGroupModel.list.length; i++)
            {
                var itemsParam = {};
                ++count;
                itemsParam.operation = $scope.taskGroupModel.list[i].operation;
                itemsParam.duration = '';
                itemsParam.amount = '';
                if ($scope.taskGroupModel.list[i].workcenterInfo.id != '' && $scope.taskGroupModel.list[i].workcenterInfo != 'undefined')
                {
                    itemsParam.work_center_id = $scope.taskGroupModel.list[i].workcenterInfo.id;
                }
                else {
                    itemsParam.work_center_id = '';
                }
                itemsParam.department_id = $scope.taskGroupModel.list[i].department;
                itemsParam.sequence_no = count;
                itemsParam.type = $scope.taskGroupModel.type;
                if (itemsParam.type == 'internal')
                {
                    itemsParam.duration = $scope.taskGroupModel.list[i].duration;
                } else if (itemsParam.type == 'external')
                {
                    itemsParam.amount = $scope.taskGroupModel.list[i].amount;
                }
                addTaskGroupParam.detail.push(itemsParam);
            }
            adminService.createTaskGroupList(addTaskGroupParam, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.taskgroup');
                }
                $scope.isDataSavingProcess = false;
            });
        };
        $scope.addNewProduct = function()
        {
            var newRow = {
                "id": '',
                "operation": '',
                "workcenterInfo": '',
                "duration": '',
                "type": 'internal',
                "amount": '',
                "department" : ''
            }
            $scope.taskGroupModel.list.push(newRow);
        };
        $scope.addNewProduct();

        $scope.deleteRow = function(index)
        {
            var index = -1;
            $scope.taskGroupModel.list.splice(index, 1);
        };
        $scope.getWorkCenterList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.WORK_CENTER_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatWorkCenterModel = function(model) {

            if (model !== null && model != undefined)
            {
                return model.name;
            }
            return  '';
        };
        $scope.getDepartmentList = function()
        {
            var getListParam = {};
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getDepartmentList(getListParam, configOption).then(function(response) {
                var data = response.data;
                if (data.list.length > 0)
                {
                    $scope.taskGroupModel.departmentList = data.list;
                }

            });
        }
        $scope.getDepartmentList();
    }]);
