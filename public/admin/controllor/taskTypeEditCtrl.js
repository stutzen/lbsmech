




app.controller('taskTypeEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', 'APP_CONST', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', function($scope, $rootScope, adminService, $httpService, utilityService, APP_CONST, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory) {

        $scope.taskModel = {
            name: '',
            is_active: 1,
            comments: "",
            description: "",
            "isLoadingProgress": false
        };
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.validationFactory = ValidationFactory;

        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.task_edit_form !== 'undefined' && typeof $scope.task_edit_form.$pristine !== 'undefined' && !$scope.task_edit_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $scope.formReset = function() {
            $scope.task_edit_form.$setPristine();
            $scope.updateTaskInfo();

        };

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        
        $scope.modifyTaskType = function() {
            if ($scope.isDataSavingProcess == false)
            {

                $scope.isDataSavingProcess = true;
                var modifyTaskTypeParam = {};
                var headers = {};
                headers['screen-code'] = 'empTaskType';
		modifyTaskTypeParam.id = $scope.taskModel.id;
		modifyTaskTypeParam.name = $scope.taskModel.name;
		modifyTaskTypeParam.comments = $scope.taskModel.comments;
		modifyTaskTypeParam.description = $scope.taskModel.description;
		modifyTaskTypeParam.is_active = $scope.taskModel.is_active;
                adminService.modifyEmployeeTaskType(modifyTaskTypeParam, $scope.taskModel.id, headers).then(function(response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.empTaskType');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        };

        $scope.updateTaskInfo = function()
        {
            if ($scope.taskDetail != null)
            {                
                $scope.taskModel.id = $scope.taskDetail.id;
                $scope.taskModel.name = $scope.taskDetail.name;
                $scope.taskModel.comments = $scope.taskDetail.comments;
                $scope.taskModel.description = $scope.taskDetail.description;
                $scope.taskModel.is_active = $scope.taskDetail.is_active;
            }
			$scope.taskModel.isLoadingProgress = false;
        };

        $scope.getTaskTypeList = function() {

            if (typeof $stateParams.id != 'undefined')
            {
				$scope.taskModel.isLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getEmployeeTaskTypeList(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.taskDetail = data.list[0];
						$scope.updateTaskInfo();
                    }

                });
            }
        };

        

        $scope.getTaskTypeList();

    }]);




