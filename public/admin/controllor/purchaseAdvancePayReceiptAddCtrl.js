




app.controller('purchaseAdvancePayReceiptAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function ($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.paymentAddModel =
                {
                    "id": 0,
                    "account": '',
                    "date": '',
                    "list": [],
                    "comments": "",
                    "amount": '',
                    "account_id": '',
                    "payment_mode": '',
                    "paymentlist": [],
                    "purchaseorder_id": '',
                    "customerInfo": {},
                    "customer_id": '',
                    "categoryList": [],
                    "accountsList": [],
                    "category": '',
                    "purchaseorder_code": '',
                    "PurchaseOrderList": [],
                    "invoiceDetails": {}
                }


        $scope.adminService = adminService;
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.save_payment_form != 'undefined' && typeof $scope.save_payment_form.$pristine != 'undefined' && !$scope.save_payment_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.isDataSavingProcess = false;
        $scope.isPurchaseOrderLoading = false;

        $scope.formReset = function ( )
        {
            $scope.paymentAddModel.quote_id = '';
            $scope.paymentAddModel.customerInfo = '';
            $scope.paymentAddModel.account = "";
            $scope.paymentAddModel.invoicedate = "";
            $scope.paymentAddModel.payment_mode = '';
            $scope.paymentAddModel.paymentdefault = '';
            $scope.paymentAddModel.amount = "";
            $scope.paymentAddModel.date = "";
            $scope.paymentAddModel.category = '';
            $scope.paymentAddModel.account_id = '';
            $scope.paymentAddModel.invoiceDetails = {};
            $scope.paymentAddModel.PurchaseOrderList = [],
                    $scope.paymentAddModel.comments = "";

        }


        $scope.paymentDateOpen = false;
        $scope.openDate = function (index)
        {
            if (index == 0)
            {
                $scope.paymentDateOpen = true;
            }

        }

        $scope.getCustomerList = function (val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function (responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_purchase == '1' || data[i].is_purchase == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function (model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };

//
//        $scope.$watch('paymentAddModel.customerInfo', function(newVal, oldVal)
//        {
//            if (typeof newVal == 'undefined' || newVal == '' || newVal == null)
//            {
//                $scope.paymentAddModel.customerInfo = '';
//            } else
//            {
//                var addrss = "";
//
//                if (newVal.billing_address != undefined && newVal.billing_address != '')
//                {
//                    addrss += newVal.billing_address;
//                }
//                if (newVal.billing_city != undefined && newVal.billing_city != '')
//                {
//                    addrss += '\n' + newVal.billing_city;
//                }
//                if (newVal.billing_pincode != undefined && newVal.billing_pincode != '')
//                {
//                    if (newVal.billing_city != undefined && newVal.billing_city != '')
//                    {
//                        addrss += '-' + newVal.billing_pincode;
//                    } else
//                    {
//                        addrss += newVal.billing_pincode;
//                    }
//                }
//
//                if (newVal.billing_state != undefined && newVal.billing_state != '')
//                {
//                    addrss += '\n' + newVal.billing_state;
//                }
//
//                $scope.paymentAddModel.customerInfo.shopping_address = addrss;
//            }
//        });

        $scope.getPurchaseOrderList = function ()
        {
            var getListParam = {};
            if ($scope.paymentAddModel.customerInfo != null && typeof $scope.paymentAddModel.customerInfo != 'undefined' && typeof $scope.paymentAddModel.customerInfo.id != 'undefined')
            {
                $scope.isPurchaseOrderLoading = true;
                getListParam.customer_id = $scope.paymentAddModel.customerInfo.id;
//                getListParam.status = 'new';
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getPurchaseQuoteList(getListParam, configOption).then(function (response)
                {
                    if (response.data.success === true)
                    {
                        var data = response.data.list;
                        $scope.paymentAddModel.PurchaseOrderList = data;
                        if ($scope.paymentAddModel.PurchaseOrderList.length > 0)
                        {
                            $scope.paymentAddModel.purchaseorder_id = $scope.paymentAddModel.PurchaseOrderList[0].id;
                            $scope.getInvoiceDetails(0);
                        } else
                        {
                            $scope.paymentAddModel.invoiceDetails = '';
                            $scope.paymentAddModel.purchaseorder_id = '';
                        }
                        $scope.isPurchaseOrderLoading = false;
                    }
                });
            }
        };

        $scope.getInvoiceDetails = function (index)
        {
            $scope.paymentAddModel.invoiceDetails = $scope.paymentAddModel.PurchaseOrderList[index];
        };

        $scope.getIncomeCategoryList = function () {

            var getListParam = {};
            getListParam.id = '';
            getListParam.is_active = 1;
            getListParam.type = 'expense';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getIncomeCategoryList(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.paymentAddModel.categoryList = data;
            });

        };
        $scope.getAccountlist = function () {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getAccountlist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.paymentAddModel.accountsList = data;
            });

        };

        $scope.createAdvancePayment = function ()
        {
            var createAdvanceParam = {};

            if ($scope.paymentAddModel.customerInfo != null && typeof $scope.paymentAddModel.customerInfo != 'undefined' && typeof $scope.paymentAddModel.customerInfo.id != 'undefined')
            {
                createAdvanceParam.customer_id = $scope.paymentAddModel.customerInfo.id;
            }
            createAdvanceParam.comments = $scope.paymentAddModel.comments;
            if (typeof $scope.paymentAddModel.date == 'object')
            {
                $scope.paymentAddModel.date = utilityService.parseDateToStr($scope.paymentAddModel.date, 'yyyy-MM-dd');
            }
            createAdvanceParam.date = utilityService.changeDateToSqlFormat($scope.paymentAddModel.date, 'yyyy-MM-dd');
            createAdvanceParam.amount = $scope.paymentAddModel.amount;
            createAdvanceParam.payment_mode = $scope.paymentAddModel.payment_mode;
            createAdvanceParam.reference_no = '';
            createAdvanceParam.tra_category = $scope.paymentAddModel.category
            createAdvanceParam.voucher_type = "advance_purchase";
            createAdvanceParam.voucher_no = $scope.paymentAddModel.purchaseorder_id;
            createAdvanceParam.account_id = $scope.paymentAddModel.account_id;
            createAdvanceParam.cheque_no = $scope.paymentAddModel.cheque_no;
            for (var i = 0; i < $scope.paymentAddModel.accountsList.length; i++)
            {
                if ($scope.paymentAddModel.accountsList[i].id == $scope.paymentAddModel.account_id)
                {
                    createAdvanceParam.account = $scope.paymentAddModel.accountsList[i].account;
                }
            }
            createAdvanceParam.is_active = 1;
            var headers = {};
            headers['screen-code'] = 'purchaseAdvPayReceipt';
            adminService.addAdvancepayment(createAdvanceParam, headers).then(function (response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.purchaseAdvancePayReceipt');
                }
                $scope.isDataSavingProcess = false;
            });

        };
        $scope.getpaymenttermslist = function () {

            var getListParam = {};
            getListParam.id = '';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getpaymenttermslist(getListParam, configOption).then(function (response) {
                var data = response.data.list;
                $scope.paymentAddModel.paymentlist = data;
            });
        }

        $scope.getpaymenttermslist();
        $scope.getIncomeCategoryList();
        $scope.getAccountlist();
    }]);




