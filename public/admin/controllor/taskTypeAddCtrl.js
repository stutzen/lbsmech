




app.controller('taskTypeAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', 'sweet', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, sweet, $stateParams, $timeout, ValidationFactory) {

        $scope.taskModel = {
            name: '',
            is_active: true,
            comments: "",
            description: ""
        };
        $scope.validationFactory = ValidationFactory;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.task_add_form !== 'undefined' && typeof $scope.task_add_form.$pristine !== 'undefined' && !$scope.task_add_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            $scope.task_add_form.$setPristine();
            $scope.taskModel.name = "";
            $scope.taskModel.is_active = true;
            $scope.taskModel.comments = "";
            $scope.taskModel.description = "";
        };

        $scope.createTaskType = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createTaskTypeParam = {};
                var headers = {};
                headers['screen-code'] = 'empTaskType';
                createTaskTypeParam.name = $scope.taskModel.name;
                createTaskTypeParam.comments = $scope.taskModel.comments;
                createTaskTypeParam.description = $scope.taskModel.description;
                createTaskTypeParam.is_active = $scope.taskModel.is_active;  
                adminService.createEmployeeTaskTypeList(createTaskTypeParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.empTaskType');
                    }
                    $scope.isDataSavingProcess = false;
                });
            } else
            {
                return;
            }

        };

    }
]);




