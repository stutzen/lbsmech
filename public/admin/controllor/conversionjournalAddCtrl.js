

app.controller('conversionjournalAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST) {

    $scope.journalModel = {
        purpose: '',
        departmentList : [],
        fromProducts : [],
        toProducts : [],
        date : ''
    }

    $scope.validationFactory = ValidationFactory;

    $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
    $scope.getNavigationBlockMsg = function()
    {
        if (typeof $scope.add_taskGrp_form != 'undefined' && typeof $scope.add_taskGrp_form.$pristine != 'undefined' && !$scope.add_taskGrp_form.$pristine)
        {
            return $scope.NAVIGATION_BLOCKER_MSG;
        }

        return "";
    }
    $scope.validationFactory = ValidationFactory;
    $scope.adminService = adminService;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.journalModel.date = $scope.currentDate;
    $scope.isDataSavingProcess = false;
    $scope.formReset = function()
    {
        $scope.add_taskGrp_form.$setPristine();
        if (typeof $scope.add_taskGrp_form != 'undefined')
        {
            $scope.journalModel.name = "";
            $scope.journalModel.version = "";
            $scope.journalModel.isActive = true;
            $scope.journalModel.list = [];
        }
    };

    $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

    $scope.dateOpen = false;
    $scope.openDate = function(index)
    {
        if (index == 0)
        {
            $scope.dateOpen = true;
        }
    };
    $scope.isDateOpen = false;
    $scope.openDate = function(index)
    {
        if(index == 1)
        {
            $scope.isDateOpen = true;
        }    
    }
    
    $scope.addFromProduct = function()
    {
        var fromProduct = {
            fromproductInfo : {},
            fromqty : ''
        }
        $scope.journalModel.fromProducts.push(fromProduct);
    }
    $scope.addToProduct = function()
    {
        var toProduct = {
            toproductInfo : {},
            toqty : ''
        }
        $scope.journalModel.toProducts.push(toProduct);
    }
    $scope.addFromProduct();
    $scope.addToProduct();
    
    $scope.createJournal = function() {
        $scope.overallfromQty = 0;
        $scope.overalltoQty = 0;
        if ($scope.isDataSavingProcess)
        {
            return;
        }
        $scope.isDataSavingProcess = true;
        var addTaskGroupParam = {};
        var headers = {};
        headers['screen-code'] = 'conversionjournal';
        addTaskGroupParam.id = 0;
        addTaskGroupParam.date = $filter('date')($scope.journalModel.date, 'yyyy-MM-dd');
        addTaskGroupParam.description = $scope.journalModel.purpose;
        for (var i = 0; i < $scope.journalModel.fromProducts.length; i++)
        {
            $scope.overallfromQty += parseFloat($scope.journalModel.fromProducts[i].fromqty);
                
        }
        addTaskGroupParam.overall_from_qty = $scope.overallfromQty;
        for (var i = 0; i < $scope.journalModel.toProducts.length; i++)
        {
            $scope.overalltoQty += parseFloat($scope.journalModel.toProducts[i].toqty);
                
        }
        addTaskGroupParam.overall_to_qty = $scope.overalltoQty;
        addTaskGroupParam.is_active = 1;
        addTaskGroupParam.detail = [];
        for (var i = 0; i < $scope.journalModel.fromProducts.length; i++)
        {
            var fromParam = {};
            fromParam.from_pdt_id = $scope.journalModel.fromProducts[i].fromproductInfo.id;
            fromParam.from_qty = $scope.journalModel.fromProducts[i].fromqty;
            fromParam.from_uom_id = $scope.journalModel.fromProducts[i].fromproductInfo.uom_id;
            fromParam.to_pdt_id = 0;
            fromParam.to_qty = 0;
            fromParam.to_uom_id = 0;
            addTaskGroupParam.detail.push(fromParam);
        }
        for (var j = 0; j < $scope.journalModel.toProducts.length; j++)
        {
            var toParam = {};
            toParam.to_pdt_id = $scope.journalModel.toProducts[j].toproductInfo.id;
            toParam.to_qty = $scope.journalModel.toProducts[j].toqty;
            toParam.to_uom_id = $scope.journalModel.toProducts[j].toproductInfo.uom_id;
            toParam.from_pdt_id = 0;
            toParam.from_qty = 0;
            toParam.from_uom_id = 0;
            addTaskGroupParam.detail.push(toParam);
        }
        adminService.saveConversionJournal(addTaskGroupParam, headers).then(function(response)
        {
            if (response.data.success == true)
            {
                $scope.formReset();
                $state.go('app.conversionjournal');
            }
            $scope.isDataSavingProcess = false;
        });
    };
       
    $scope.deleteFromProduct = function(index)
    {
        $scope.journalModel.fromProducts.splice(index, 1);
    };
    $scope.deleteToProduct = function(index)
    {
        $scope.journalModel.toProducts.splice(index, 1);
    };
    $scope.getProductList = function(val)
    {
        var autosearchParam = {};
        autosearchParam.name = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.PRODUCT_LIST, autosearchParam, false).then(function(responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };
    $scope.formatProductModel = function(model) {

        if (model !== null && model != undefined)
        {
            return model.name;
        }
        return  '';
    };
    $scope.updatefromProductInfo = function(index)
    {
        $scope.journalModel.fromProducts[index].fromqty = 1;
    }
    $scope.updatetoProductInfo = function(index)
    {
        $scope.journalModel.toProducts[index].toqty = 1;
    }
    $scope.onFocus = function (e) {
        $timeout(function () {
            $(e.target).trigger('input');
        });
    };
    $scope.keyupfromHandler = function (event)
    {
        if (event.keyCode == 9)
        {
            event.preventDefault();
            var currentFocusField = angular.element(event.currentTarget);
            var nextFieldId = $(currentFocusField).attr('next-focus-id');
            var currentFieldId = $(currentFocusField).attr('id');
            if (nextFieldId == undefined || nextFieldId == '')
            {
                return;
            }
            if(currentFieldId.indexOf('_name_') != -1)
            {
                var filedIdSplitedValue = nextFieldId.split('_');
                var existingBrunchCount = $scope.journalModel.fromProducts.length;
                if (existingBrunchCount > 0)
                {
                    $timeout(function () {
                        var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.journalModel.fromProducts.length) + '_1';
                        $("#" + newNextFieldId).focus();
                    }, 300);
                    return;
                }
            }    
            if (currentFieldId.indexOf('_qty_') != -1)
            {
                filedIdSplitedValue = nextFieldId.split('_');
                existingBrunchCount = $scope.journalModel.fromProducts.length;
                if (existingBrunchCount > 0)
                {
                    $scope.addFromProduct();
                    $timeout(function () {
                        var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.journalModel.fromProducts.length) + '_1';
                        $("#" + newNextFieldId).focus();
                    }, 300);
                    return;
                }
            }
            $timeout(function () {
                $("#" + nextFieldId).focus();
                window.document.getElementById(nextFieldId).setSelectionRange(0, window.document.getElementById(nextFieldId).value.length);
            }, 300);
        }
    }
    $scope.keyuptoHandler = function (event)
    {
        if (event.keyCode == 9)
        {
            event.preventDefault();
            var currentFocusField = angular.element(event.currentTarget);
            var nextFieldId = $(currentFocusField).attr('next-focus-id');
            var currentFieldId = $(currentFocusField).attr('id');
            if (nextFieldId == undefined || nextFieldId == '')
            {
                return;
            }
            if(currentFieldId.indexOf('_name_') != -1)
            {
                var filedIdSplitedValue = nextFieldId.split('_');
                var existingBrunchCount = $scope.journalModel.toProducts.length;
                if (existingBrunchCount > 0)
                {
                    $timeout(function () {
                        var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.journalModel.toProducts.length) + '_1';
                        $("#" + newNextFieldId).focus();
                    }, 300);
                    return;
                }
            }    
            if (currentFieldId.indexOf('_qty_') != -1)
            {
                filedIdSplitedValue = nextFieldId.split('_');
                existingBrunchCount = $scope.journalModel.fromProducts.length;
                if (existingBrunchCount > 0)
                {
                    $scope.addToProduct();
                    $timeout(function () {
                        var newNextFieldId = filedIdSplitedValue[0] + '_' + filedIdSplitedValue[1] + '_' + ($scope.journalModel.toProducts.length) + '_1';
                        $("#" + newNextFieldId).focus();
                    }, 300);
                    return;
                }
            }
            $timeout(function () {
                $("#" + nextFieldId).focus();
                window.document.getElementById(nextFieldId).setSelectionRange(0, window.document.getElementById(nextFieldId).value.length);
            }, 300);
        }
    }
       
}]);
