app.controller('uomCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.uomModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            status: '',
            //             sku: '',

            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.uomModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            filteruomname: ''
        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                filteruomname: ''
            };
            $scope.initTableFilter();
        }
        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $scope.selectUOMId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectUOMId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteUOMInfo = function( )
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;
                
                var getListParam = {};
                getListParam.id = $scope.selectUOMId;
                var headers = {};
                headers['screen-code'] = 'uom';
                adminService.deleteUOM(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteProgress = false;
                    
                });
            }

        };

        $scope.getList = function()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'uom';
            getListParam.id = "";
            getListParam.is_active = 1;
            getListParam.name = $scope.searchFilter.filteruomname;
            getListParam.start = ($scope.uomModel.currentPage - 1) * $scope.uomModel.limit;
            getListParam.limit = $scope.uomModel.limit;
            $scope.uomModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getUom(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.uomModel.list = data.list;
                    $scope.uomModel.total = data.total;
                }
                $scope.uomModel.isLoadingProgress = false;
            });
        };

        $scope.getList();
    }]);




