




app.controller('taxGroupAddCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', '$filter', 'Auth', '$state', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, utilityService, $httpService, $filter, Auth, $state, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.taxGroupAddModel = {
            "tax_no": "",
            "tax_name": "",
            "tax_percentage": "",
            "tax_applicable_amt": "",
            "start_date": "",
            "end_date": "",
            "comments": "",
            "isActive": 1,
            "tax_type": '',
            "isdisplay": true
        }


        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.create_Taxlist_form != 'undefined' && typeof $scope.create_Taxlist_form.$pristine != 'undefined' && !$scope.create_Taxlist_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.tax = 'tax';
        $scope.mappedTax = {"belongTo": [$scope.tax], "items": []};


        $scope.startDateOpen = false;
        $scope.endDateOpen = false;
        $scope.openDate = function(index)
        {
            if (index == 1)
            {
                $scope.startDateOpen = true;
            }
            if (index == 2)
            {
                $scope.endDateOpen = true;
            }
        }


        $scope.formReset = function() {

            $scope.create_Taxlist_form.$setPristine();
            $scope.taxGroupAddModel.tax_name = '';
            $scope.taxGroupAddModel.tax_no = '';
            $scope.taxGroupAddModel.tax_percentage = '';
            $scope.taxGroupAddModel.tax_applicable_amt = '';
            $scope.taxGroupAddModel.start_date = '';
            $scope.taxGroupAddModel.end_date = '';
            $scope.taxGroupAddModel.comments = '';
            $scope.taxGroupAddModel.tax_type = '';
            $scope.taxGroupAddModel.tax = '';
            $scope.taxGroupAddModel.isdisplay = true;
            $scope.mappedTax = {"belongTo": [$scope.tax], "items": []};
        }
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.resetTaxlistCreate = function() {

            $scope.taxGroupAddModel.user = '';

        }

        $scope.validationFactory = ValidationFactory;

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.createTaxlist, 300);
        }

        $scope.taxGroupMapping = function()
        {
            if (typeof $scope.taxGroupAddModel.tax != undefined && $scope.taxGroupAddModel.tax != '' && $scope.taxGroupAddModel.tax != null && $scope.taxGroupAddModel.tax.id != '')
            {
                var count = 0;
                if ($scope.mappedTax.items.length > 0)
                {
                    for (var i = 0; i < $scope.mappedTax.items.length; i++)
                    {
                        if ($scope.taxGroupAddModel.tax.id == $scope.mappedTax.items[i].id)
                        {
                            count = count + 1;
                        }
                    }
                }
                if (count > 0)
                {
                    sweet.show('Oops...', 'This Tax is Already Mapped .. ', 'error');
                }
                else
                {
                    $scope.mappedTax.items.push($scope.taxGroupAddModel.tax);
                    angular.forEach($scope.mappedTax.items, function(item) {
                        item.belongTo = $scope.tax;
                    });
                    $scope.taxGroupAddModel.tax = '';
                }
            }
        }

        $scope.removeTaxMapping = function(id)
        {
            var index = -1;
            for (var i = 0; i < $scope.mappedTax.items.length; i++) {
                if (id == $scope.mappedTax.items[i].id) {
                    index = i;
                    break;
                }
            }
            $scope.mappedTax.items.splice(index, 1);
        };
        $scope.getTaxList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.tax_name = val;
            autosearchParam.is_group = 0;
            return $httpService.get(APP_CONST.API.TAX_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatTaxModel = function(model) {

            if (model !== null && model != undefined)
            {

                return model.tax_name;
            }
            return  '';
        };

        $scope.onMoved = function(list) {
            list.items = list.items.filter(function(item) {
                return !item.selected;
            });
        };

        $scope.onDragstart = function(list, event) {
            list.dragging = true;
            if (event.dataTransfer.setDragImage) {
                var img = new Image();
                img.src = '../img/ic_content_copy_black_24dp_2x.png';
                event.dataTransfer.setDragImage(img, 0, 0);
            }
        };

        $scope.onDrop = function(list, items, index) {
            angular.forEach(items, function(item) {
                item.selected = false;
            });
            list.items = list.items.slice(0, index)
                    .concat(items)
                    .concat(list.items.slice(index));
            return true;
        }

        $scope.getSelectedItemsIncluding = function(list, item) {
            item.selected = true;
            return list.items.filter(function(item) {
                return item.selected;

            });
        };


        $scope.createTaxGroup = function() {

            if (!$scope.isDataSavingProcess)
            {
                if ($scope.mappedTax.items.length > 0)
                {
                    $scope.isDataSavingProcess = true;
                    var createTaxlistParam = {};
                    var headers = {};
                    headers['screen-code'] = 'taxgroup';
                    createTaxlistParam.tax_name = $scope.taxGroupAddModel.tax_name;
                    createTaxlistParam.tax_percentage = $scope.taxGroupAddModel.tax_percentage;
                    createTaxlistParam.tax_no = $scope.taxGroupAddModel.tax_no;
                    createTaxlistParam.tax_type = $scope.taxGroupAddModel.tax_type;
                    createTaxlistParam.comments = $scope.taxGroupAddModel.comments;
                    createTaxlistParam.is_group = 1;
                    createTaxlistParam.group_tax_ids = '';
                    var groupTax = [];
                    for (var i = 0; i < $scope.mappedTax.items.length; i++)
                    {
                        $scope.mappedTax.items[i].id = $scope.mappedTax.items[i].id + '';
                        groupTax.push($scope.mappedTax.items[i].id);
                    }
                    createTaxlistParam.group_tax_ids = groupTax.toString();

                    createTaxlistParam.start_date = null;
                    createTaxlistParam.end_date = null;
                    if ($scope.taxGroupAddModel.start_date != null && $scope.taxGroupAddModel.start_date != "")
                    {
                        createTaxlistParam.start_date = utilityService.parseDateToStr($scope.taxGroupAddModel.start_date, 'yyyy-MM-dd');
                    }
                    if ($scope.taxGroupAddModel.end_date != null && $scope.taxGroupAddModel.end_date != "")
                    {
                        createTaxlistParam.end_date = utilityService.parseDateToStr($scope.taxGroupAddModel.end_date, 'yyyy-MM-dd');
                    }


                    createTaxlistParam.is_display = $scope.taxGroupAddModel.isdisplay == true ? 1 : 0;

                    adminService.addTax(createTaxlistParam, headers).then(function(response) {
                        if (response.data.success == true)
                        {
                            $scope.isDataSavingProcess = false;
                            $scope.formReset();
                            $state.go('app.taxgroup');
                        }
                    });
                }
                else
                {
                    sweet.show('Oops...', 'Tax Mapping Required .. ', 'error');
                }
            }
        };



    }]);




