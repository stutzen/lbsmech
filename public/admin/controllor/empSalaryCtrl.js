





app.controller('empSalaryCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.salaryModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
        teamid :'',
            length: [],
            teamList: [],
            date: '',
            serverList: null,
        salarytype :'',
            isLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.salaryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }

        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                teamid: '',
            salarytype :''
            };
            $scope.initTableFilter();
        }

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            teamid: '',
        salarytype :'',
        payroll_type: ''

        };
        // $scope.searchFilter.todate = $scope.currentDate;

        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                teamid: '',
            salarytype :'',
            payroll_type: ''
            };
            $scope.salaryModel.list = [];
            //  $scope.searchFilter.todate = $scope.currentDate;

        }


        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.typechange = function()
        {
            //$scope.searchFilter.teamid = '';
            $scope.initTableFilter();
        };
        $scope.team = function()
        {
            if ($scope.searchFilter.teamid != null && $scope.searchFilter.teamid != '')
            {
                for (var i = 0; i < $scope.salaryModel.teamList.length; i++)
                {
                    if ($scope.searchFilter.teamid == $scope.salaryModel.teamList[i].id)
                    {
                        $scope.salaryModel.teamid = $scope.salaryModel.teamList[i].name;
                    }
                }
            }
            $scope.initTableFilter();
        }
        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empSalary';
            getListParam.team_id = $scope.searchFilter.teamid;
            getListParam.salary_type = $scope.searchFilter.salarytype;
            getListParam.payroll_type = $scope.searchFilter.payroll_type;
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }
            getListParam.start = ($scope.salaryModel.currentPage - 1) * $scope.salaryModel.limit;
            getListParam.limit = $scope.salaryModel.limit;
            $scope.salaryModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;

            adminService.getEmployee(getListParam, configOption, headers).then(function(response)
            {

                var data = response.data;
                $scope.salaryModel.list = data.list;
                $scope.salaryModel.total = data.total;
                $scope.salaryModel.isLoadingProgress = false;
            });

        };
    $scope.getTeamList = function ()
        {

            var getListParam = {};
            getListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getEmployeeTeamList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.salaryModel.teamList = data.list;
                }
            });
        };

        $scope.print = function()
        {
            $window.print();
        };

        $scope.getList();
        $scope.getTeamList();
    }]);





