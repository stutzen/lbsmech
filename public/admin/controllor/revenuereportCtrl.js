





app.controller('revenuereportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.revenueModel = {
        currentPage: 1,
        total: 0,
        total_Amount: 0,
        limit: 10,
        list: [],
        length: [],
        isLoadingProgress: false,
        isSearchLoadingProgress: false
    };
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.revenueModel.limit = $scope.pagePerCount[0];
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0)); 
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.company_name = $rootScope.appConfig.app_tile;
    $scope.company_address = $rootScope.appConfig.pay_to_address;
    $scope.validationFactory = ValidationFactory;
    $scope.openDate = function(index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        }
        else if (index === 1)
        {
            $scope.toDateOpen = true;
        }

    };

    $scope.searchFilter = {
        fromdate: '',
        todate: ''
    };
    $scope.searchFilter.todate = $scope.currentDate;
    $scope.clearFilters = function()
    {
        $scope.searchFilter = {
            fromdate: '',
            todate: ''
        };
        $scope.revenueModel.list = [];
        $scope.searchFilter.todate = $scope.currentDate;
    }

    $scope.print = function()
    {
        $window.print();
    };

    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    $scope.validateDateFilterData = function()
    {
        var retVal = false;
        if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
        {
            retVal = true;
        }
        return retVal;
    };

    $scope.show = function(index)
    {
        if ($scope.showDetails[index])
        {
            $scope.showDetails[index] = false;
        }
        else
        {   
            $scope.showDetails[index] = true;
        }
    };

    $scope.getList = function() {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'revenuereport';
        if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
           // getListParam.from_date = getListParam.to_date;
        }
        else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
        {
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            getListParam.to_date = getListParam.from_date;
        }
        else
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
        }
        getListParam.start = 0;
        getListParam.limit = 0;
        $scope.revenueModel.isLoadingProgress = true;
        $scope.showDetails = [];
        var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
        adminService.getUserCountList(getListParam,configOption, headers).then(function(response)
        {
            var totalAmt = 0.00;
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.revenueModel.list = data;

                for (var i = 0; i < $scope.revenueModel.list.length; i++)
                {
                    totalAmt = totalAmt + parseFloat($scope.revenueModel.list[i].overallamount);
                    $scope.revenueModel.list[i].total_price = utilityService.changeCurrency($scope.revenueModel.list[i].overallamount, $rootScope.appConfig.thousand_seperator);
                    $scope.revenueModel.list[i].flag = i;
                    $scope.showDetails[i] = false;
                    for (var j = 0; j < $scope.revenueModel.list[i].stage.length; j++)
                    {
                        //                            $scope.revenueModel.list[i].data[j].newdate = utilityService.parseStrToDate($scope.revenueModel.list[i].data[j].date);
                        //                            $scope.revenueModel.list[i].data[j].date = utilityService.parseDateToStr($scope.revenueModel.list[i].data[j].newdate, $scope.adminService.appConfig.date_format);
                        $scope.revenueModel.list[i].stage[j].flag = i;
                    }
                }
                $scope.revenueModel.total_Amount = totalAmt;
                $scope.revenueModel.total_Amount = parseFloat($scope.revenueModel.total_Amount).toFixed(2);
                $scope.revenueModel.total_Amount = utilityService.changeCurrency($scope.revenueModel.total_Amount, $rootScope.appConfig.thousand_seperator);

            }
            $scope.revenueModel.isLoadingProgress = false;
        });

    };

    $scope.getList();
}]);




