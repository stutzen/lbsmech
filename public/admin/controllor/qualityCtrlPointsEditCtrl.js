




app.controller('qualityCtrlPointsEditCtrl', ['$scope', '$rootScope', 'adminService', 'utilityService', '$httpService', 'APP_CONST', '$filter', 'Auth', '$state', 'sweet', '$stateParams', '$timeout', 'ValidationFactory', function ($scope, $rootScope, adminService, utilityService, $httpService, APP_CONST, $filter, Auth, $state, sweet, $stateParams, $timeout, ValidationFactory) {

            $scope.qualityModel = {
                  "title": "",
                  "is_active": 1,
                  "operation": "",
                  "empInfo": "",
                  "comments": "",
                  "instructions": "",
                  "productInfo": ""
            };
            $scope.validationFactory = ValidationFactory;
            $scope.adminService = adminService;
            $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
            $scope.getNavigationBlockMsg = function (pageReload)
            {
                  if (typeof $scope.edit_qcp_form !== 'undefined' && typeof $scope.edit_qcp_form.$pristine !== 'undefined' && !$scope.edit_qcp_form.$pristine)
                  {
                        return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
                  }

                  return "";
            };
            $scope.isDataSavingProcess = false;
            $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
            $scope.formReset = function ()
            {
                  $scope.updateQcpInfo();
            };
            $scope.updateQcpInfo = function()
            {
                if ($scope.qualityModel.list !== null)
                {
                    $scope.qualityModel.title = $scope.qualityModel.list.title;
                    $scope.qualityModel.operation = $scope.qualityModel.list.type;
                    $scope.qualityModel.comments = $scope.qualityModel.list.comments;
                    $scope.qualityModel.instructions = $scope.qualityModel.list.instructions;
                    $scope.qualityModel.empInfo = {
                                            id:$scope.qualityModel.list.responsible_by,
                                            f_name : $scope.qualityModel.list.respone_name
                    };
                    $scope.qualityModel.productInfo = {
                                            id:$scope.qualityModel.list.product_id,
                                            name : $scope.qualityModel.list.product_name
                    };
                    
                }
                $scope.qualityModel.isLoadingProgress = false;
            };
            $scope.getEmployeeList = function (val)
            {
                  var autosearchParam = {};
                  autosearchParam.fname = val;
                  autosearchParam.is_active = 1;
                  if (autosearchParam.fname != '')
                  {
                        return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
                        {
                              var data = responseData.data.list;
                              var hits = data;
                              if (hits.length > 10)
                              {
                                    hits.splice(10, hits.length);
                              }
                              return hits;
                        });
                  }
            };

            $scope.formatEmployeeModel = function (model)
            {
                  if (model != null)
                  {
                        if (model.f_name != undefined && model.id != undefined && model.emp_code != undefined)
                        {
                              return model.f_name + '(' + model.id + ',' + model.emp_code + ')';
                        } else if (model.f_name != undefined && model.id != undefined)
                        {
                              return model.f_name + '(' + model.id + ')';
                        }
                  }
                  return  '';
            };

            $scope.getProductList = function (val)
            {
                var autoSearchParam = {};
                autoSearchParam.search = val;
                return $httpService.get(APP_CONST.API.PRODUCT_LIST, autoSearchParam, false).then(function (response)
                {
                      var data = response.data.list;
                      var hits = data;
                      if (hits.length > 10)
                      {
                            hits.splice(10, hits.length);
                      }
                      return hits;
                });
                  
            }
            $scope.formatProductModel = function (model)
            {
                  if (model != null)
                  {
                         return model.name;
                  }
                  return  '';
            };
            $scope.modifyQcp = function ()
            {
                $scope.isDataSavingProcess = true;
                 var QcpParam = {};
                 var headers = {};
                 headers['screen-code'] = 'qualitycontrolpoints';
                 QcpParam.title = $scope.qualityModel.title;
                 QcpParam.type = $scope.qualityModel.operation;
                 QcpParam.responsible_by = $scope.qualityModel.empInfo.id;
                 QcpParam.product_id = $scope.qualityModel.productInfo.id;
                 QcpParam.instructions = $scope.qualityModel.instructions;
                 QcpParam.comments = $scope.qualityModel.comments;
                 adminService.modifyQcp(QcpParam,$stateParams.id, headers).then(function (response) {
                       if (response.data.success === true)
                       {
                             $scope.formReset();
                             $state.go('app.qualitycontrolpoints');
                       }
                       $scope.isDataSavingProcess = false;
                 });
                  
            };
            $scope.getAccountInfo = function() {
                $scope.qualityModel.isLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getQcpList(getListParam, configOption).then(function(response) {
                    var data = response.data;
                    if (data.total !== 0)
                    {
                        $scope.qualityModel.list = data.list[0];
                        $scope.updateQcpInfo();
                    }

                });
            
        };
        $scope.getAccountInfo(); 
      }
]);




