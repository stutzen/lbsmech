





app.controller('taxsummaryCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.taxsummaryModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            sno: '',
            name: '',
            amount: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.taxsummaryModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: ''

        };

        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: ''
            };
            $scope.taxsummaryModel.list = [];
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate !== '' || $scope.searchFilter.fromdate !== '')
            {
                retVal = true;
            }
            return retVal;
        };
        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'taxsummaryreport';
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = getListParam.to_Date;
            }
            else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
                getListParam.to_Date = getListParam.from_Date;
            }
            else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_Date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_Date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_Date = utilityService.changeDateToSqlFormat(getListParam.to_Date, $scope.dateFormat);
                getListParam.from_Date = utilityService.changeDateToSqlFormat(getListParam.from_Date, $scope.dateFormat);
            }
//            if ($scope.searchFilter.todate !== '' && $scope.searchFilter.fromdate === '')
//            {
//                getListParam.to_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//                getListParam.from_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//            }
//            else if ($scope.searchFilter.fromdate !== '' && $scope.searchFilter.todate === '')
//            {
//                getListParam.to_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//                getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//            }
//            else
//            {
//                getListParam.to_Date = $filter('date')($scope.searchFilter.todate, $scope.dateFormat);
//                getListParam.from_Date = $filter('date')($scope.searchFilter.fromdate, $scope.dateFormat);
//            }
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.taxsummaryModel.isLoadingProgress = true;
            $scope.taxsummaryModel.isSearchLoadingProgress = true;
            adminService.gettaxsummarylist(getListParam, headers).then(function(response)
            {
                var totalAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.taxsummaryModel.list = data;

                    for (var i = 0; i < $scope.taxsummaryModel.list.length; i++)
                    {
                        if ($scope.taxsummaryModel.list[i].amount == null)
                        {
                            $scope.taxsummaryModel.list[i].amount = 0.00;
                        }
                        totalAmt = totalAmt + parseFloat($scope.taxsummaryModel.list[i].amount);
                        $scope.taxsummaryModel.list[i].amount = utilityService.changeCurrency($scope.taxsummaryModel.list[i].amount, $rootScope.appConfig.thousand_seperator);

                    }
                    $scope.taxsummaryModel.total = data.total;
                    $scope.taxsummaryModel.total_Amount = totalAmt;
                    $scope.taxsummaryModel.total_Amount = parseFloat($scope.taxsummaryModel.total_Amount).toFixed(2);
                    $scope.taxsummaryModel.total_Amount = utilityService.changeCurrency($scope.taxsummaryModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                    $scope.taxsummaryModel.isLoadingProgress = false;
                    $scope.taxsummaryModel.isSearchLoadingProgress = false;
                }
            });

        };

        $scope.print = function()
        {
            $window.print();
        };

        $scope.validateAmount = function(amount)
        {
            if (amount > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        //$scope.getList();
    }]);




