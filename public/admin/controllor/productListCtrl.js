app.controller('productListCtrl', ['$scope', '$rootScope', 'adminService','utilityService','$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService,utilityService, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.productModel = {
            scopeId: "",
            configId: 5,
            type: "",
            currentPage: 1,
            mrp_price:'',
            sales_price:'',
            total: 0,
            limit: 4,
            list: [],
            status: '',
            currencyFormat: '',
            serverList: null,
            isLoadingProgress: true
        };

        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.productModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: '',
            sku:'',
            salesOrPurchase: ''
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getProductListInfo, 300);
        }

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: '',
                 sku:'',
                salesOrPurchase: ''
            };
            $scope.initTableFilter();
        }

        $scope.selectProductId = '';
        $scope.showdeletePopup = false;
         $scope.isdeleteProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectProductId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
             $scope.isdeleteProgress = false;
        };

        $scope.deleteProductInfo = function( )
        {
            if($scope.isdeleteProgress == false)
            {
               $scope.isdeleteProgress = true;
               
            var getListParam = {};
            getListParam.id = $scope.selectProductId;
            var headers = {};
            headers['screen-code'] = 'product';
            adminService.deleteProduct(getListParam, getListParam.id, headers).then(function(response)
            {
                var data = response.data;
                if (data.success == true)
                {
                    $scope.closePopup();
                    $scope.getProductListInfo();
                }
                     $scope.isdeleteProgress = false;
            });
            }
        };

        /*
         * Get market list
         */

        $scope.getProductListInfo = function() {

            // ?name=&sku=&type=&start=&limit=
            $scope.productModel.isLoadingProgress = true;
            var materialListParam = {};
            var headers = {};
            headers['screen-code'] = 'product';
            //materialListParam.inventoryScopeId = $rootScope.userModel.scopeId;
            //materialListParam.configId = 5;
            materialListParam.type = "Product";
            materialListParam.sku = $scope.searchFilter.sku;
            //materialListParam.status = "initiated";
            materialListParam.id = '';
            materialListParam.name = $scope.searchFilter.name;
            if ($scope.searchFilter.salesOrPurchase == 1)
            {
                materialListParam.is_sale = 1;
            }
            else if ($scope.searchFilter.salesOrPurchase == 2)
            {
                materialListParam.is_purchase = 1;
            }
            materialListParam.is_active = 1;
            materialListParam.start = ($scope.productModel.currentPage - 1) * $scope.productModel.limit;
            materialListParam.limit = $scope.productModel.limit;
            
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getProductList(materialListParam,configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.productModel.list = data.list;
                    $scope.productModel.total = data.total;
                    for (var i = 0; i < $scope.productModel.list.length; i++)
                    {
                        $scope.productModel.list[i].mrp_price = utilityService.changeCurrency($scope.productModel.list[i].mrp_price, $rootScope.appConfig.thousand_seperator);
                        $scope.productModel.list[i].sales_price = utilityService.changeCurrency($scope.productModel.list[i].sales_price, $rootScope.appConfig.thousand_seperator);
                    }
                }
                $scope.productModel.isLoadingProgress = false;
            });

        };

        $scope.getProductListInfo();

    }]);




