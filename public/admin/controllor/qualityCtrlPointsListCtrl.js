





app.controller('qualityCtrlPointsListCtrl', ['$scope', '$rootScope', 'adminService', 'ValidationFactory', 'utilityService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, ValidationFactory, utilityService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.qualityModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            isLoadingProgress: false
        };
        $scope.searchFilter = {
            productInfo: {},
            title: '',
            type: '',
        };
        $scope.validationFactory = ValidationFactory;
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                productInfo: {},
                title: '',
                type: '',
            };
            $scope.initTableFilter();
        }
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.qualityModel.limit = $scope.pagePerCount[0];

        $scope.validationFactory = ValidationFactory;

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };

        $scope.selectQcpId = '';
        $scope.showdeletePopup = false;
        $scope.isdeleteLoadingProgress = false;
        $scope.showPopup = function(id)
        {
            $scope.selectQcpId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteLoadingProgress = false;
        };
        $scope.deleteQcpInfo = function( )
        {
            if ($scope.isdeleteLoadingProgress == false)
            {
                $scope.isdeletLoadingProgress = true;
                var getListParam = {};
                getListParam.id = $scope.selectQcpId;
                var headers = {};
                headers['screen-code'] = 'qualitycontrolpoints';
                adminService.deleteQcp(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteLoadingProgress = false;
                });
            }
        };

        $scope.getList = function() {

            $scope.qualityModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'qualitycontrolpoints';
            getListParam.is_active = 1;
            getListParam.title = $scope.searchFilter.title;
            getListParam.type = $scope.searchFilter.type;
            if ($scope.searchFilter.productInfo != null && typeof $scope.searchFilter.productInfo != 'undefined' && typeof $scope.searchFilter.productInfo.id != 'undefined')
            {
                getListParam.product_id = $scope.searchFilter.productInfo.id;
            }
            else
            {
                getListParam.product_id = '';
            }
            getListParam.start = ($scope.qualityModel.currentPage - 1) * $scope.qualityModel.limit;
            getListParam.limit = $scope.qualityModel.limit;
            $scope.qualityModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getQcpList(getListParam, configOption, headers).then(function(response) {

                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.qualityModel.list = data;
                    $scope.qualityModel.total = data.total;
                }
                $scope.qualityModel.isLoadingProgress = false;
            });

        };
         $scope.getProductList = function (val)
        {
            var autoSearchParam = {};
            autoSearchParam.search = val;
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, autoSearchParam, false).then(function (response)
            {
                  var data = response.data.list;
                  var hits = data;
                  if (hits.length > 10)
                  {
                        hits.splice(10, hits.length);
                  }
                  return hits;
            });

        }
        $scope.formatProductModel = function (model)
        {
              if (model != null)
              {
                     return model.name;
              }
              return  '';
        };

       $scope.getList();

    }]);




