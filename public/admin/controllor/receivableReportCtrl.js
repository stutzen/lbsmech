





app.controller('receivableReportCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.receivableReportModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            date: '',
            invoice_id: '',
            customer_id: '',
            amount: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false
        };
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.receivableReportModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date(new Date().setHours(0, 0, 0, 0)); 
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat);
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function(index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            customerInfo: {},
            overdue: false

        };
        $scope.searchFilter.fromdate = $scope.currentDate;

        $scope.clearFilters = function()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                customerInfo: {},
                overdue: false

            };

            $scope.receivableReportModel.list = [];
        }

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };


        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'receivablereport';
            getListParam.name = '';
//            if ($scope.searchFilter.customerInfo != null && typeof $scope.searchFilter.customerInfo != 'undefined' && typeof $scope.searchFilter.customerInfo.id != 'undefined')
//            {
//                getListParam.customer_id = $scope.searchFilter.customerInfo.id;
//            }
//            else
//            {
//                getListParam.customer_id = '';
//            }
            getListParam.customer_id = '';
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;
            getListParam.overdue = $scope.searchFilter.overdue == true ? 1 : 0;
            if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = getListParam.to_date;
            }
            else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
                getListParam.to_date = getListParam.from_date;
            }
            else
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
                }
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
                }
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            }

            getListParam.is_active = $scope.searchFilter.is_active;
            getListParam.from_duedate = '';
            getListParam.to_duedate = '';
            getListParam.discount_persentage = '';
            getListParam.status = 'partiallyPaid';
            getListParam.start = 0;
            getListParam.limit = 0;
            $scope.receivableReportModel.isLoadingProgress = true;
            $scope.receivableReportModel.isSearchLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.receivableList(getListParam, configOption, headers).then(function(response)
            {
                var totalAmt = 0.00;
                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.receivableReportModel.list = data;
                    for (var i = 0; i < $scope.receivableReportModel.list.length; i++)
                    {
                        $scope.receivableReportModel.list[i].newdate = utilityService.parseStrToDate($scope.receivableReportModel.list[i].date);
                        $scope.receivableReportModel.list[i].date = utilityService.parseDateToStr($scope.receivableReportModel.list[i].newdate, $scope.adminService.appConfig.date_format);
                        totalAmt = totalAmt + parseFloat($scope.receivableReportModel.list[i].balance);
                        $scope.receivableReportModel.list[i].balance = utilityService.changeCurrency($scope.receivableReportModel.list[i].balance, $rootScope.appConfig.thousand_seperator);
                    }
                    $scope.receivableReportModel.total = data.total;
                    $scope.receivableReportModel.total_Amount = totalAmt;
                    $scope.receivableReportModel.total_Amount = parseFloat($scope.receivableReportModel.total_Amount).toFixed(2);
                    $scope.receivableReportModel.total_Amount = utilityService.changeCurrency($scope.receivableReportModel.total_Amount, $rootScope.appConfig.thousand_seperator);

                }
                $scope.receivableReportModel.isLoadingProgress = false;
                $scope.receivableReportModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                if (model.fname != undefined && model.phone != undefined && model.email != undefined)
                {
                    return model.fname + '(' + model.phone + ',' + model.email + ')';
                }
                else if (model.fname != undefined && model.phone != undefined)
                {
                    return model.fname + '(' + model.phone + ')';
                }
            }
            return  '';
        };
//        $scope.formatcustomerModel = function(model) {
//
//            if (model !== null)
//            {
//                $scope.searchFilter.id = model.id;
//                return model.fname;
//            }
//            return  '';
//        };

        $scope.print = function()
        {
            $window.print();
        };

        $scope.getList();
    }]);




