
app.controller('itemizedsalesCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout) {

    $rootScope.getNavigationBlockMsg = null;

    $scope.advanceModel = {
        currentPage: 1,
        total: 0,
        total_Balance: 0,
        total_Deduction: 0,
        total_Advance: 0,
        limit: 10,
        list: [],
        bal_list : [],
        length: [],
        productname: '',
        uom: '',
        quantity: '',
        employeeInfo: {},
        amount: '',
        serverList: null,
        isLoadingProgress: false,
        isSearchLoadingProgress: false
    };
    $scope.adminService = adminService;
    $scope.pagePerCount = [50, 100];
    $scope.advanceModel.limit = $scope.pagePerCount[0];
    $scope.fromDateOpen = false;
    $scope.toDateOpen = false;
    $scope.dateFormat = $rootScope.appConfig.date_format;
    $scope.currentDate = new Date();
    $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
    $scope.validationFactory = ValidationFactory;
    $scope.openDate = function (index) {

        if (index === 0)
        {
            $scope.fromDateOpen = true;
        } else if (index === 1)
{
            $scope.toDateOpen = true;
        }

    };

    $scope.searchFilter = {
        fromdate: '',
        todate: '',
        employeeInfo: {}
    };
    $scope.clearFilters = function ()
    {
        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            employeeInfo: {}
        };
        $scope.advanceModel.list = [];
    }

    $scope.print = function ()
    {
        $window.print();
    };

    $scope.searchFilterValue = "";

    $scope.initTableFilterTimeoutPromise = null;

    $scope.initTableFilter = function ()
    {
        if ($scope.initTableFilterTimeoutPromise != null)
        {
            $timeout.cancel($scope.initTableFilterTimeoutPromise);
        }
        $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
    };
    $scope.validateDateFilterData = function ()
    {
        var retVal = false;
        if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
        {
            retVal = true;
        }
        return retVal;
    };

    $scope.getEmployeelist = function (val)
    {
        var autosearchParam = {};
        autosearchParam.fname = val;
        autosearchParam.is_active = 1;
        return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, autosearchParam, false).then(function (responseData)
        {
            var data = responseData.data.list;
            var hits = data;
            if (hits.length > 10)
            {
                hits.splice(10, hits.length);
            }
            return hits;
        });
    };

    $scope.formatEmployeeModel = function (model) {

        if (model != null && model != undefined)
        {
            return model.f_name;
        }
        return  '';
    };

    $scope.show = function (index,id)
    {
        if ($scope.showDetails[index])
        {
            $scope.showDetails[index] = false;
            
        } else
{
            $scope.showDetails[index] = true;
            $scope.getBalanceList(index,id);
        }
    };


    $scope.getList = function (index) {

        var getListParam = {};
        var headers = {};
        headers['screen-code'] = 'advancereport';
        $scope.showDetails = [];
        getListParam.show_all = 1;
        if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = getListParam.to_date;
        } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
{
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            getListParam.to_date = getListParam.from_date;
        } else
{
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
        }
        if ($scope.searchFilter.employeeInfo != null && typeof $scope.searchFilter.employeeInfo != 'undefined' && typeof $scope.searchFilter.employeeInfo.id != 'undefined')
        {
            getListParam.emp_id = $scope.searchFilter.employeeInfo.id;
        } else
{
            getListParam.emp_id = '';
        }
        getListParam.start = 0;
        getListParam.limit = 0;
            
        $scope.advanceModel.isLoadingProgress = true;
        var configOption = adminService.handleOnlyErrorResponseConfig;
        adminService.getAdvanceReport(getListParam, configOption, headers).then(function (response)
        {
            var totalAdvance = 0.00;
            var totalDeduction = 0.00;
            var totalBalance = 0.00;
            if (response.data.success === true)
            {
                var data = response.data.list;
                $scope.advanceModel.list = data;

                for (var i = 0; i < $scope.advanceModel.list.length; i++)
                {
                    
                    totalAdvance = totalAdvance + parseFloat($scope.advanceModel.list[i].advance_amount);
                    $scope.advanceModel.list[i].advance_amount = utilityService.changeCurrency($scope.advanceModel.list[i].advance_amount, $rootScope.appConfig.thousand_seperator);
                    totalDeduction = totalDeduction + parseFloat($scope.advanceModel.list[i].deduction_amount);
                    $scope.advanceModel.list[i].deduction_amount = utilityService.changeCurrency($scope.advanceModel.list[i].deduction_amount, $rootScope.appConfig.thousand_seperator);
                    totalBalance = totalBalance + parseFloat($scope.advanceModel.list[i].balance_amount);
                    $scope.advanceModel.list[i].balance_amount = utilityService.changeCurrency($scope.advanceModel.list[i].balance_amount, $rootScope.appConfig.thousand_seperator);
                    $scope.advanceModel.list[i].bal_list = i;
                    $scope.showDetails[i] = false;
//                    
//                    $scope.advanceModel.list[i].flag = i;
//                   // $scope.showDetails[i] = false;
//                    for (var j = 0; j < $scope.advanceModel.list[i].advance_detail.length; j++)
//                    {
//                        $scope.advanceModel.list[i].advance_detail[j].newdate = utilityService.parseStrToDate($scope.advanceModel.list[i].advance_detail[j].date);
//                        $scope.advanceModel.list[i].advance_detail[j].date = utilityService.parseDateToStr($scope.advanceModel.list[i].advance_detail[j].newdate, $scope.adminService.appConfig.date_format);
//                        $scope.advanceModel.list[i].advance_detail[j].flag = i;
//                    }
                }
                $scope.advanceModel.total_Advance = totalAdvance;
                $scope.advanceModel.total_Advance = parseFloat($scope.advanceModel.total_Advance).toFixed(2);
                $scope.advanceModel.total_Advance = utilityService.changeCurrency($scope.advanceModel.total_Advance, $rootScope.appConfig.thousand_seperator);
                $scope.advanceModel.total_Deduction = totalDeduction;
                $scope.advanceModel.total_Deduction = parseFloat($scope.advanceModel.total_Deduction).toFixed(2);
                $scope.advanceModel.total_Deduction = utilityService.changeCurrency($scope.advanceModel.total_Deduction, $rootScope.appConfig.thousand_seperator);
                $scope.advanceModel.total_Balance = totalAdvance - totalDeduction;
                $scope.advanceModel.total_Balance = parseFloat($scope.advanceModel.total_Balance).toFixed(2);
                $scope.advanceModel.total_Balance = utilityService.changeCurrency($scope.advanceModel.total_Balance, $rootScope.appConfig.thousand_seperator);
                    

            }
            $scope.advanceModel.isLoadingProgress = false;
        });

    };
    $scope.updateBalanceAmount = function (index)
    {
        if ($scope.advanceModel.list[index].bal_list.length >= 1)
        {
            var openingBalance = parseFloat($scope.advanceModel.list[index].bal_list.openingBalance.advance_amount) - parseFloat($scope.advanceModel.list[index].bal_list.openingBalance.deduction_amount);
           
            for (var i = 0; i < $scope.advanceModel.list[index].bal_list.length; i++)
            {
                $scope.advanceModel.list[index].bal_list[i].balance = openingBalance;
                var advance_amount = $scope.advanceModel.list[index].bal_list[i].advance_amount == null ? 0 : parseFloat($scope.advanceModel.list[index].bal_list[i].advance_amount);
                var deduction_amount = $scope.advanceModel.list[index].bal_list[i].deduction_amount == null ? 0 : parseFloat($scope.advanceModel.list[index].bal_list[i].deduction_amount);
                if (advance_amount != 0)
                {
                    openingBalance = openingBalance + advance_amount;
                } else if (deduction_amount != 0)
{
                    openingBalance = openingBalance - deduction_amount;
                }
                $scope.advanceModel.list[index].bal_list[i].balance = openingBalance;
            }

            $scope.advanceModel.totalbalance = openingBalance;
            $scope.advanceModel.totalbalance = parseFloat($scope.advanceModel.totalbalance).toFixed(2);
            $scope.advanceModel.totalbalance = utilityService.changeCurrency($scope.advanceModel.totalbalance, $rootScope.appConfig.thousand_seperator);


        } else
{
            $scope.advanceModel.totalbalance = 0.00;
        }
    }
    //   $scope.showDetails = false;
    $scope.getBalanceList = function (index,id) {

        var getListParam = {};
        var headers = {};
       
      ///$scope.showDetails = [];
        headers['screen-code'] = 'advancereport';
         
     
        getListParam.emp_id = id;
        if ($scope.searchFilter.todate != '' && ($scope.searchFilter.fromdate == null || $scope.searchFilter.fromdate == ''))
        {
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = getListParam.to_date;
        } else if ($scope.searchFilter.fromdate != '' && ($scope.searchFilter.todate == null || $scope.searchFilter.todate == ''))
{
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
            getListParam.to_date = getListParam.from_date;
        } else
{
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }
            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);
        }
           
       
            
        getListParam.start = 0;
        getListParam.limit = 0;
        adminService.getBalanceAdvanceReport(getListParam, headers).then(function (response)
        {
            if (response.data.list.length > 0)
            {
                var data = response.data.list;
                $scope.advanceModel.list[index].bal_list = data;
                $scope.advanceModel.list[index].bal_list.openingBalance = response.data.openBalance;
                $scope.advanceModel.list[index].bal_list.openingBalance.advance_amount = parseFloat($scope.advanceModel.list[index].bal_list.openingBalance.advance_amount).toFixed(2);
                $scope.advanceModel.list[index].bal_list.openingBalance.deduction_amount = parseFloat($scope.advanceModel.list[index].bal_list.openingBalance.deduction_amount).toFixed(2);
                var openingBalance = parseFloat($scope.advanceModel.list[index].bal_list.openingBalance.advance_amount) - parseFloat($scope.advanceModel.list[index].bal_list.openingBalance.deduction_amount);
                $scope.advanceModel.list[index].bal_list.openingBalance.balance = openingBalance;
                $scope.advanceModel.list[index].bal_list.openingBalance.balance = parseFloat($scope.advanceModel.list[index].bal_list.openingBalance.balance).toFixed(2);
                $scope.advanceModel.list[index].bal_list.openingAdvance = utilityService.changeCurrency($scope.advanceModel.list[index].bal_list.openingBalance.advance_amount, $rootScope.appConfig.thousand_seperator);
                $scope.advanceModel.list[index].bal_list.openingDeduction = utilityService.changeCurrency($scope.advanceModel.list[index].bal_list.openingBalance.deduction_amount, $rootScope.appConfig.thousand_seperator);

                $scope.advanceModel.list[index].bal_list.openingAmount = utilityService.changeCurrency($scope.advanceModel.list[index].bal_list.openingBalance.balance, $rootScope.appConfig.thousand_seperator);
                if ($scope.advanceModel.list[index].bal_list.length != 0)
                {
                    $scope.updateBalanceAmount(index);
                  
                       
                    for (var i = 0; i < $scope.advanceModel.list[index].bal_list.length; i++)
                    {
                        $scope.advanceModel.list[index].bal_list[i].newdate = utilityService.parseStrToDate($scope.advanceModel.list[index].bal_list[i].date);
                        $scope.advanceModel.list[index].bal_list[i].date = utilityService.parseDateToStr($scope.advanceModel.list[index].bal_list[i].newdate, $scope.dateFormat);
                        $scope.advanceModel.list[index].bal_list[i].deduction_amount = utilityService.changeCurrency($scope.advanceModel.list[index].bal_list[i].deduction_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.advanceModel.list[index].bal_list[i].advance_amount = utilityService.changeCurrency($scope.advanceModel.list[index].bal_list[i].advance_amount, $rootScope.appConfig.thousand_seperator);
                        $scope.advanceModel.list[index].bal_list[i].balance = parseFloat($scope.advanceModel.list[index].bal_list[i].balance).toFixed(2);
                        $scope.advanceModel.list[index].bal_list[i].balance = utilityService.changeCurrency($scope.advanceModel.list[index].bal_list[i].balance, $rootScope.appConfig.thousand_seperator);
                      //  $scope.advanceModel.list.bal_list[i].flag = i;
                      $scope.showDetails[index] = true;
                    }
                }
            }  

        });

    };
//   $scope.getBalanceList();
}]);







