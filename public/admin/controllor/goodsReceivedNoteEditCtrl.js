
app.controller('goodsReceivedNoteEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.goodsReceivedNoteModel =
                {
                    "id": 0,
                    "productname": "",
                    "sku": "",
                    "qty": "",
                    "billdate": "",
                    "comments": "",
                    "pack_size": "",
                    "isActive": "1",
                    "customername": "",
                    list: [],
                    grnList: [],
                    "customerInfo": {},
                    "productList": [],
                    "productId": '',
                    "isLoadingProgress": false,
                }
        $scope.adminService = adminService;
        $scope.customAttributeList = [];
        $scope.validationFactory = ValidationFactory;
        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.order_edit_form != 'undefined' && typeof $scope.order_edit_form.$pristine != 'undefined' && !$scope.order_edit_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $scope.formReset = function()
        {
            $scope.order_edit_form.$setPristine();
            $scope.goodsReceivedNoteModel.list = [];
        }

        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.resetOrderCreate = function() {

            $scope.goodsReceivedNoteModel.user = '';
        }

        $scope.opened = false;
        $scope.initTableFilterTimeoutPromise = null;
        $scope.deletedProductList = [];
        $scope.undeletedProductList = [];
        $scope.showbillPopup = false;
        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.maxDate = $scope.maxDate ? null : new Date();
        $scope.startDateOpen = false;
        $scope.dueDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.currentDate = new Date();

        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.startDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.dueDateOpen = true;
            }
        }



        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };



        $scope.getUomList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;

            return $httpService.get(APP_CONST.API.UOM, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };

        $scope.formatUomModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };

        $scope.isLoaded = false;
        $scope.getProductList = function()
        {
            var getParam = {};
            $scope.isLoaded = false;
            getParam.is_active = 1;
            getParam.is_sale = 1;
            getParam.type = 'Product';
            getParam.localData = 1;
            if (typeof $scope.selectedAssignFrom != 'undefined' && typeof $scope.selectedAssignFrom.id != 'undefined')
            {
                getParam.scopeId = $scope.selectedAssignFrom.id;
            }
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getProductList(getParam, configOption).then(function(response) {
                var data = response.data;
                $scope.goodsReceivedNoteModel.productList = data.list;
                $scope.isLoaded = true;
            });
        }

        $scope.formatProductModel = function(model, index)
        {
            if (model != null)
            {
                $scope.updateProductInfo(model, index);
                return model.name;
            }
            return  '';
        }
        $scope.updateProductInfo = function(model, index)
        {
            if ($scope.goodsReceivedNoteModel.list.length != 0)
            {
                $scope.goodsReceivedNoteModel.list[index].id = model.id;
                $scope.goodsReceivedNoteModel.list[index].productname.name = model.name;
                $scope.goodsReceivedNoteModel.list[index].sku = model.sku;
//                $scope.goodsReceivedNoteModel.list[index].sales_price = model.sales_price;
                $scope.goodsReceivedNoteModel.list[index].qty = model.qty;
                $scope.goodsReceivedNoteModel.list[index].pack_size = model.pack_size;
                $scope.goodsReceivedNoteModel.list[index].comments = model.comments;
                $scope.goodsReceivedNoteModel.list[index].uomInfo = {
                    "name": model.uom,
                }
                $scope.goodsReceivedNoteModel.list[index].uom_id = model.uom_id;


                console.log("Index for item edit ", index);
                console.log("List Length ", $scope.goodsReceivedNoteModel.list.length);
                return;
            }

        }


        $scope.updateOrder = function(forceSave)
        {

            if (!forceSave)
            {
                if ($scope.isDataSavingProcess)
                {
                    return;
                }
            }
            $scope.isDataSavingProcess = true;
            $scope.isSave = false;
            for (var i = 0; i < $scope.goodsReceivedNoteModel.list.length; i++)
            {
                if (i != $scope.goodsReceivedNoteModel.list.length)
                {
                    if ($rootScope.appConfig.uomdisplay)
                    {
                        if ($scope.goodsReceivedNoteModel.list[i].productname != '' && $scope.goodsReceivedNoteModel.list[i].productname != null &&
                                $scope.goodsReceivedNoteModel.list[i].uomInfo != '' && $scope.goodsReceivedNoteModel.list[i].uomInfo != null &&
                                $scope.goodsReceivedNoteModel.list[i].qty != '' && $scope.goodsReceivedNoteModel.list[i].qty != null &&
                                $scope.goodsReceivedNoteModel.list[i].pack_size != '' && $scope.goodsReceivedNoteModel.list[i].pack_size != null)
                        {
                            $scope.isSave = true;
                        }
                        else
                        {
                            $scope.isSave = false;
                        }
                    }
                    else
                    {
                        if ($scope.goodsReceivedNoteModel.list[i].productname != '' && $scope.goodsReceivedNoteModel.list[i].productname != null &&
                                $scope.goodsReceivedNoteModel.list[i].qty != '' && $scope.goodsReceivedNoteModel.list[i].qty != null &&
                                $scope.goodsReceivedNoteModel.list[i].pack_size != '' && $scope.goodsReceivedNoteModel.list[i].pack_size != null)
                        {
                            $scope.isSave = true;
                        }
                        else
                        {
                            $scope.isSave = false;
                        }
                    }
                }
            }

            if ($scope.isSave)
            {
                $scope.modifyOrder();
            }
            else
            {
                sweet.show('Oops...', 'Fill Product Detail.. ', 'error');
                $scope.isDataSavingProcess = false;
            }

        };

        $scope.modifyOrder = function()
        {
//            var updateOrderParam = [];
            var headers = {};
            headers['screen-code'] = 'goodsReceivedNote';

            for (var i = 0; i < $scope.goodsReceivedNoteModel.list.length ; i++)
            {
                var ordereditems = {};
                ordereditems.id = $stateParams.id;
                if (typeof $scope.goodsReceivedNoteModel.billdate == 'object')
                {
                    $scope.goodsReceivedNoteModel.billdate = utilityService.parseDateToStr($scope.goodsReceivedNoteModel.billdate, 'yyyy-MM-dd');
                }
                ordereditems.date = utilityService.changeDateToSqlFormat($scope.goodsReceivedNoteModel.billdate, 'yyyy-MM-dd');
                ordereditems.customer_id = $scope.goodsReceivedNoteModel.customerInfo.id;
                ordereditems.product_id = $scope.goodsReceivedNoteModel.list[i].id;
                if ($scope.goodsReceivedNoteModel.list[i].productname.name != undefined && $scope.goodsReceivedNoteModel.list[i].productname.name != null && $scope.goodsReceivedNoteModel.list[i].productname.name != '')
                {
                    ordereditems.product_name = $scope.goodsReceivedNoteModel.list[i].productname.name;
                }
                else
                {
                    ordereditems.product_name = $scope.goodsReceivedNoteModel.list[i].productname;
                }
                ordereditems.qty = parseFloat($scope.goodsReceivedNoteModel.list[i].qty);
                ordereditems.pack_size = parseFloat($scope.goodsReceivedNoteModel.list[i].pack_size);
                ordereditems.comments = $scope.goodsReceivedNoteModel.list[i].comments;
                if (typeof $scope.goodsReceivedNoteModel.list[i].uomInfo != undefined && $scope.goodsReceivedNoteModel.list[i].uomInfo != null && $scope.goodsReceivedNoteModel.list[i].uomInfo != '')
                {
                    if ($scope.goodsReceivedNoteModel.list[i].uomInfo.name != null && $scope.goodsReceivedNoteModel.list[i].uomInfo.name != '')
                    {
                        ordereditems.uom_name = $scope.goodsReceivedNoteModel.list[i].uomInfo.name;
                    }

                }
                ordereditems.uom_id = $scope.goodsReceivedNoteModel.list[i].uom_id;
//                updateOrderParam.push(ordereditems);
            }
            adminService.editGRNInfo(ordereditems, $stateParams.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {

                    $scope.formReset();
                    $state.go('app.goodsReceivedNoteList');
                }
                $scope.isDataSavingProcess = false;
            });
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }

            if ($scope.isLoaded)
            {
                $scope.updateGRNDetails();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.updateGRNDetails = function( )
        {
            $scope.goodsReceivedNoteModel.customerInfo = {};
            for (var i = 0; i < $scope.goodsReceivedNoteModel.grnList.length; i++)
            {
                $scope.goodsReceivedNoteModel.id = $scope.goodsReceivedNoteModel.grnList[0].id;
                $scope.goodsReceivedNoteModel.customerInfo.id = $scope.goodsReceivedNoteModel.grnList[0].customer_id;
                var idate = utilityService.parseStrToDate($scope.goodsReceivedNoteModel.grnList[0].date);
                $scope.goodsReceivedNoteModel.billdate = idate;
                $scope.goodsReceivedNoteModel.customerInfo =
                        {
                            id: $scope.goodsReceivedNoteModel.grnList[0].customer_id,
                            fname : $scope.goodsReceivedNoteModel.grnList[0].customer_name
                        };
                var newRow =
                        {
                            "id": $scope.goodsReceivedNoteModel.grnList[i].product_id,
                            "name": $scope.goodsReceivedNoteModel.grnList[i].product_name,
                            "sku": $scope.goodsReceivedNoteModel.grnList[i].product_sku,
                            "qty": parseFloat($scope.goodsReceivedNoteModel.grnList[i].qty).toFixed(2),
                            "pack_size": parseFloat($scope.goodsReceivedNoteModel.grnList[i].pack_size).toFixed(2),
                            "comments": $scope.goodsReceivedNoteModel.grnList[i].comments,
                            "uom": $scope.goodsReceivedNoteModel.grnList[i].uom,
                            "uom_id": $scope.goodsReceivedNoteModel.grnList[i].uom_id,
                        };
                $scope.goodsReceivedNoteModel.list.push(newRow);
                $scope.goodsReceivedNoteModel.list[i].productname = $scope.goodsReceivedNoteModel.list[i];
                console.log('GRNList');
                console.log($scope.goodsReceivedNoteModel.list);
            }
            $scope.goodsReceivedNoteModel.isLoadingProgress = false;
        }

        $scope.getGRNInfo = function() {
            var getListParam = {};
            getListParam.id = $stateParams.id;
            $scope.goodsReceivedNoteModel.isLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.grnListAll(getListParam, configOption).then(function(response) {
                var data = response.data.list;
                $scope.goodsReceivedNoteModel.grnList = data;
                $scope.goodsReceivedNoteModel.total = response.data.total;
                $scope.initUpdateDetail();
            });
        };
        $scope.getGRNInfo();
        $scope.getProductList( );
        $scope.updateProductInfo();
    }]);