

app.controller('stateEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', 'ValidationFactory', '$state', '$localStorageService', '$timeout', 'APP_CONST', '$stateParams', function($scope, $rootScope, adminService, $httpService, $filter, Auth, ValidationFactory, $state, $localStorageService, $timeout, APP_CONST, $stateParams) {

        $scope.stateModel = {
            id: '',
            name: '',
            isActive: true,
            list: {},
            code: '',
            country_id:'',
            country_name:'',
            country_list:[]
          //  isLoadingProgress: false

        }

        $scope.validationFactory = ValidationFactory;
       


        $scope.NAVIGATION_BLOCKER_MSG = "Form has unsaved data !";
        $scope.getNavigationBlockMsg = function()
        {
            if (typeof $scope.edit_state_form != 'undefined' && typeof $scope.edit_state_form.$pristine != 'undefined' && !$scope.edit_state_form.$pristine)
            {
                return $scope.NAVIGATION_BLOCKER_MSG;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
         $scope.formReset = function() {
            if (typeof $scope.edit_state_form != 'undefined')
            {
                $scope.edit_state_form.$setPristine();
                $scope.updateStateInfo();
            }

        }
        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isLoadedCountryList)
            {
                $scope.updateStateInfo();
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;

        $scope.modifystate = function() {

            if ($scope.isDataSavingProcess==false)
            {
            $scope.isDataSavingProcess = true;
            var addStateParam = {};
            var headers = {};
            headers['screen-code'] = 'state';
            addStateParam.id = $stateParams.id;
            addStateParam.name = $scope.stateModel.name;
            addStateParam.code = $scope.stateModel.code;
            addStateParam.is_active = $scope.stateModel.isActive;
            addStateParam.country_id = $scope.stateModel.country_id;
             for (var i = 0; i < $scope.stateModel.country_list.length; i++)
            {
                if ($scope.stateModel.country_id == $scope.stateModel.country_list[i].id)
                {
                    addStateParam.country_name = $scope.stateModel.country_list[i].name;
                }
            }
            adminService.editState(addStateParam, $scope.stateModel.id, headers).then(function(response)
            {
                if (response.data.success == true)
                {
                    $scope.formReset();
                    $state.go('app.state');
                }
                $scope.isDataSavingProcess = false;
            });
            }
        };

        $scope.updateStateInfo = function()
        {
            $scope.stateModel.is_active = $scope.stateModel.list.is_active;
            $scope.stateModel.name = $scope.stateModel.list.name;
            
            $scope.stateModel.id = $scope.stateModel.list.id;
            $scope.stateModel.code = $scope.stateModel.list.code;
            $scope.stateModel.country_id = $scope.stateModel.list.country_id;
            $scope.stateModel.isLoadingProgress = false;
        }
        $scope.getStateInfo = function()
        {
            $scope.stateModel.isLoadingProgress = true;
            if (typeof $stateParams.id != 'undefined')
            {
                var stateListParam = {};
                stateListParam.id = $stateParams.id;
                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
                adminService.getStateList(stateListParam, configOption).then(function(response) {

                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.stateModel.list = data.list[0];

                    }
                    $scope.initUpdateDetail();

                });
            }
        };
        $scope.getStateInfo();
        
         $scope.isLoadedCountryList = false;
        $scope.getcountryList = function()
        {
            var getListParam = {};
            getListParam.id = '';
            $scope.isLoadedCountryList = false;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getCountryList(getListParam, configOption).then(function(response)
            {
                var data = response.data;
                $scope.stateModel.country_list = data.list;
                $scope.isLoadedCountryList = true;
            });
        };
        $scope.getcountryList();
    }]);






