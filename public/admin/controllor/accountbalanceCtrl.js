





app.controller('accountbalanceCtrl', ['$scope', '$rootScope', 'adminService', 'APP_CONST', '$window', '$filter', 'Auth', '$state', '$timeout', '$httpService', function($scope, $rootScope, adminService, APP_CONST, $window, $filter, Auth, $state, $timeout, $httpService) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.accountbalanceModel = {
            currentPage: 1,
            total: 0,
            limit: 10,
            list: [],
            id: '',
            account: '',
            description: '',
            balance: '',
            account_no: '',
            status: '',
//             sku: '',

            serverList: null,
            isLoadingProgress: false
        };

        $scope.pagePerCount = [50, 100];
        $scope.accountbalanceModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            id: '',
            start: '',
            account: '',
            balance: '',
            bank_name: '',
            account_number: '',
            customerInfo: '',
            userInfo: ''

        };

        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise !== null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter.account = '';
            $scope.initTableFilter();
        }

        $scope.getList = function() {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'accountbalance';
            getListParam.id = '';
            getListParam.account = $scope.searchFilter.account;
            getListParam.bank_name = $scope.searchFilter.bank_name;
            getListParam.account_number = $scope.searchFilter.account_number;
            getListParam.start = ($scope.accountbalanceModel.currentPage - 1) * $scope.accountbalanceModel.limit;
            getListParam.limit = $scope.accountbalanceModel.limit;
            $scope.accountbalanceModel.isLoadingProgress = true;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption, headers).then(function(response) {
                if (response.data.success === true)
                {
                    var balanceamt = 0.00;
                    var data = response.data.list;
                    $scope.accountbalanceModel.list = data;

                    for (var i = 0; i < $scope.accountbalanceModel.list.length; i++)
                    {

                        balanceamt = balanceamt + parseFloat($scope.accountbalanceModel.list[i].balance)

                    }
                    $scope.accountbalanceModel.balance = balanceamt;
                    var data = response.data.list;
                    $scope.accountbalanceModel.list = data;
                    $scope.accountbalanceModel.total = data.total;
                }
                $scope.accountbalanceModel.isLoadingProgress = false;
            });

        };


        $scope.getList();
    }]);




