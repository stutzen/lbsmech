

app.controller('screenAccessEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', function ($scope, $rootScope, adminService, $httpService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST) {

        $scope.screenAccessModel = {
            "id": "",
            "name": "",
            "isactive": 1,
            is_create: false,
            is_modify: false,
            is_view: false,
            is_delete: false,
            userScreenList: [],
            screenAccessDetail: {},
            userAccessInfo: [],
            isLoadingProgress: false,
            createall: false,
            modifyall: false,
            viewall: false,
            deleteall: false,
            userDetail: {}
        };
        $scope.validationFactory = ValidationFactory;
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.modify_screen_access_form != 'undefined' && typeof $scope.modify_screen_access_form.$pristine != 'undefined' && !$scope.modify_screen_access_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            if (typeof $scope.modify_screen_access_form != 'undefined')
            {
                $scope.modify_screen_access_form.$setPristine();
                $scope.screenAccessModel.createall = false;
                $scope.screenAccessModel.modifyall = false;
                $scope.screenAccessModel.viewall = false;
                $scope.screenAccessModel.deleteall = false;
                $scope.getScreenAccess( );
                $scope.getScreenAccessList();
            }
        }

        $scope.initUpdateDetailTimeoutPromise = null;
        $scope.initUpdateDetail = function ()
        {
            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            if ($scope.isRoleDetail == true && $scope.isLoadedScreenDetail == true)
            {
                $scope.fillScreenAccess();
            } else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
            }
        }

        $scope.getUserRoleList = function ()
        {
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'role';
            getListParam.id = $stateParams.id;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getUserRoleList(getListParam, configOption, headers).then(function (response)
            {
                var data = response.data;
                if (data.list.length != 0)
                {
                    $scope.screenAccessModel.userDetail = data.list[0];
                    $scope.roleName = $scope.screenAccessModel.userDetail.name;
                }

            });
        };
        $scope.isLoadedScreenDetail = false;
        $scope.isRoleDetail = false;
        $scope.getScreenAccess = function ( )
        {
            var getListParam = {};
            $scope.isLoadedScreenDetail = false;
            var headers = {};
            headers['screen-code'] = 'role';
            getListParam.role_id = $stateParams.id;

            ///ibilling/public/userroleacess/listAll
            adminService.getUserAccessDetail(getListParam, headers).then(function (response)
            {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.screenAccessModel.screenAccessDetail = data.list;
                    $scope.isLoadedScreenDetail = true;
                } else
                {
                    $scope.screenAccessModel.isLoadingProgress = false;
                }
            });
        }

        $scope.selectCreateAll = function ()
        {
            if ($scope.screenAccessModel.createall)
            {
                for (var i = 0; i < $scope.screenAccessModel.userScreenList.length; i++)
                {
                    $scope.screenAccessModel.userScreenList[i].is_create = true;
                }
            } else
            {
                for (var i = 0; i < $scope.screenAccessModel.userScreenList.length; i++)
                {
                    $scope.screenAccessModel.userScreenList[i].is_create = false;
                }
            }
        }
        $scope.selectModifyAll = function ()
        {
            if ($scope.screenAccessModel.modifyall)
            {
                for (var i = 0; i < $scope.screenAccessModel.userScreenList.length; i++)
                {
                    $scope.screenAccessModel.userScreenList[i].is_modify = true;
                }
            } else
            {
                for (var i = 0; i < $scope.screenAccessModel.userScreenList.length; i++)
                {
                    $scope.screenAccessModel.userScreenList[i].is_modify = false;
                }
            }
        }
        $scope.selectViewAll = function ()
        {
            if ($scope.screenAccessModel.viewall)
            {
                for (var i = 0; i < $scope.screenAccessModel.userScreenList.length; i++)
                {
                    $scope.screenAccessModel.userScreenList[i].is_view = true;
                }
            } else
            {
                for (var i = 0; i < $scope.screenAccessModel.userScreenList.length; i++)
                {
                    $scope.screenAccessModel.userScreenList[i].is_view = false;
                }
            }
        }
        $scope.selectDeleteAll = function ()
        {
            if ($scope.screenAccessModel.deleteall)
            {
                for (var i = 0; i < $scope.screenAccessModel.userScreenList.length; i++)
                {
                    $scope.screenAccessModel.userScreenList[i].is_delete = true;
                }
            } else
            {
                for (var i = 0; i < $scope.screenAccessModel.userScreenList.length; i++)
                {
                    $scope.screenAccessModel.userScreenList[i].is_delete = false;
                }
            }
        }

        $scope.fillScreenAccess = function ( )
        {
            var loop, innerLoop, fillStatus = false;

            for (loop = 0; loop < $scope.screenAccessModel.userScreenList.length; loop++)
            {
                fillStatus = false;
                for (innerLoop = 0; innerLoop < $scope.screenAccessModel.screenAccessDetail.length; innerLoop++)
                {
                    if ($scope.screenAccessModel.userScreenList[loop].id == $scope.screenAccessModel.screenAccessDetail[innerLoop].screen_id)
                    {
                        $scope.screenAccessModel.userScreenList[loop].id = $scope.screenAccessModel.userScreenList[loop].id;
                        $scope.screenAccessModel.userScreenList[loop].is_create = ($scope.screenAccessModel.screenAccessDetail[innerLoop].is_create == 1 ? true : false);
                        $scope.screenAccessModel.userScreenList[loop].is_modify = ($scope.screenAccessModel.screenAccessDetail[innerLoop].is_modify == 1 ? true : false);
                        $scope.screenAccessModel.userScreenList[loop].is_view = ($scope.screenAccessModel.screenAccessDetail[innerLoop].is_view == 1 ? true : false);
                        $scope.screenAccessModel.userScreenList[loop].is_delete = ($scope.screenAccessModel.screenAccessDetail[innerLoop].is_delete == 1 ? true : false);
                        $scope.screenAccessModel.userScreenList[loop].is_visible_to = $scope.screenAccessModel.screenAccessDetail[innerLoop].is_visible_to;
                        fillStatus = true;
                    }
                }
                if (fillStatus == false)
                {
                    $scope.screenAccessModel.userScreenList[loop].id = $scope.screenAccessModel.userScreenList[loop].id;
                    $scope.screenAccessModel.userScreenList[loop].is_create = false;
                    $scope.screenAccessModel.userScreenList[loop].is_modify = false;
                    $scope.screenAccessModel.userScreenList[loop].is_view = false;
                    $scope.screenAccessModel.userScreenList[loop].is_delete = false;
                    $scope.screenAccessModel.userScreenList[loop].is_visible_to = 'All';
                }
                //$scope.screenAccessModel.userScreenList[loop].push(newRow);                                                
            }
            $scope.screenAccessModel.isLoadingProgress = false;
        }

        $scope.modifyUserScreenAccess = function ( )
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var updateParam = [];
                var headers = {};
                headers['screen-code'] = 'role';
                var loop;
                for (loop = 0; loop < $scope.screenAccessModel.userScreenList.length; loop++)
                {
                    var newRow =
                            {
                                "screen_name": '',
                                "role_id": $stateParams.id,
                                "screen_id": $scope.screenAccessModel.userScreenList[loop].id,
                                "is_create": ($scope.screenAccessModel.userScreenList[loop].is_create == true ? 1 : 0),
                                "is_modify": ($scope.screenAccessModel.userScreenList[loop].is_modify == true ? 1 : 0),
                                "is_view": ($scope.screenAccessModel.userScreenList[loop].is_view == true ? 1 : 0),
                                "is_delete": ($scope.screenAccessModel.userScreenList[loop].is_delete == true ? 1 : 0),
                                "is_visible_to": $scope.screenAccessModel.userScreenList[loop].is_visible_to
                            }
                    console.log("Data Push", newRow);
                    updateParam.push(newRow);
                }
                adminService.editUserScreenAccess(updateParam, headers).then(function (response)
                {
                    if (response.data.success == true)
                    {
                        $scope.formReset();
                        $state.go('app.userrole');
                    }
                    $scope.isDataSavingProcess = false;
                });
            }
        }

        $scope.getScreenAccessList = function ( )
        {
            $scope.isRoleDetail = false;
            $scope.screenAccessModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'role';
            //getListParam.id = $stateParams.id;

            //screen/listAll
            adminService.getUserScreenAccessList(getListParam, headers).then(function (response) {
                var data = response.data;
                if (data.total != 0)
                {
                    $scope.screenAccessModel.userScreenList = data.list;
                    $scope.initUpdateDetail();
                    $scope.isRoleDetail = true;
                }
                $scope.screenAccessModel.isLoadingProgress = false;
            });
        }
        $scope.getScreenAccessList( );
        $scope.getScreenAccess( );
        $scope.getUserRoleList();
    }]);




