





app.controller('bonusReportAddCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'APP_CONST', '$window', 'ValidationFactory', 'utilityService', '$filter', 'Auth', '$state', '$timeout', 'sweet', function ($scope, $rootScope, adminService, $httpService, APP_CONST, $window, ValidationFactory, utilityService, $filter, Auth, $state, $timeout, sweet) {

        $rootScope.getNavigationBlockMsg = null;

        $scope.bonusReportAddModel = {
            currentPage: 1,
            total: 0,
            total_Amount: 0,
            limit: 10,
            list: [],
            length: [],
            TeamList: [],
            date: '',
            emp_id: '',
            emp_code: '',
            emp_name: '',
            emp_team_id: '',
            bonus_percentage: '',
            bonus_amount: '',
            salary_amount: '',
            present_count: '',
            absent_count: '',
            serverList: null,
            isLoadingProgress: false,
            isSearchLoadingProgress: false,
            bonustotal: 0,
            account: '',
            accountList: []
        };

        $scope.currencyFormat = $rootScope.appConfig.currency;
        $scope.selectAll = false;
        $scope.adminService = adminService;
        $scope.pagePerCount = [50, 100];
        $scope.bonusReportAddModel.limit = $scope.pagePerCount[0];
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.currentDate = new Date();
        $scope.todaynewDate = $filter('date')($scope.currentDate, $scope.dateFormat)
        $scope.validationFactory = ValidationFactory;
        $scope.openDate = function (index) {

            if (index === 0)
            {
                $scope.fromDateOpen = true;
            } else if (index === 1)
            {
                $scope.toDateOpen = true;
            }

        };

        $scope.searchFilter = {
            fromdate: '',
            todate: '',
            teamInfo: '',
        };
        $scope.searchFilter.todate = $scope.currentDate;

        $scope.clearFilters = function ()
        {
            $scope.searchFilter = {
                fromdate: '',
                todate: '',
                teamInfo: ''

            };
            $scope.bonusReportAddModel.list = [];
            $scope.searchFilter.todate = $scope.currentDate;

        }


        $scope.searchFilterValue = "";

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function ()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        };
        $scope.validateDateFilterData = function ()
        {
            var retVal = false;
            if ($scope.searchFilter.todate != '' || $scope.searchFilter.fromdate != '')
            {
                retVal = true;
            }
            return retVal;
        };

        /*$scope.$watch('searchFilter.teamInfo', function(newVal, oldVal)
         {
         if (typeof newVal == 'undefined' || newVal == '')
         {
         {
         $scope.initTableFilter();
         }
         }
         });*/
        $scope.getNavigationBlockMsg = function (pageReload)
        {
            if (typeof $scope.bonus_form != 'undefined' && typeof $scope.bonus_form.$pristine != 'undefined' && !$scope.bonus_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        };
        $scope.isDataSavingProcess = false;
        $rootScope.getNavigationBlockMsg = $scope.getNavigationBlockMsg;
        $scope.formReset = function ()
        {
            $scope.bonus_form.$setPristine();
            $scope.bonusReportAddModel.emp_id = "";
            $scope.bonusReportAddModel.emp_code = "";
            $scope.bonusReportAddModel.emp_name = "";
            $scope.bonusReportAddModel.emp_team_id = "";
            $scope.bonusReportAddModel.bonus_percentage = '';
            $scope.bonusReportAddModel.bonus_amount = '';
            $scope.bonusReportAddModel.salary_amount = "";
            $scope.bonusReportAddModel.present_count = "";
            $scope.bonusReportAddModel.absent_count = "";

        };

        $scope.showsalaryPopup = false;
        $scope.showAccount = false;
        $scope.showPopup = function ()
        {
            $scope.showAccount = false;
            $scope.showsalaryPopup = true;
        };

        $scope.closePopup = function ()
        {
            $scope.showsalaryPopup = false;
        };

        $scope.showaccountPopup = function ()
        {
            var isDataSelected = 0;
            $scope.showAccount = false;
            if ($scope.bonusReportAddModel.list.length > 0)
            {
                for (var i = 0; i < $scope.bonusReportAddModel.list.length; i++)
                {
                    if ($scope.bonusReportAddModel.list[i].isSelected == true)
                    {
                        isDataSelected = isDataSelected + 1;
                    }
                }
            }
            if (isDataSelected == 0)
            {
                sweet.show('Oops...', 'Select Employee Entry', 'error');
                $scope.isDataSavingProcess = false;
                $scope.showAccount = false;
            } else
            {
                $scope.showAccount = true;
            }
        };

        $scope.showAccountListPopup = function ()
        {
            $scope.showAccount = true;
        };

        $scope.closeaccountPopup = function ()
        {
            $scope.showAccount = false;
            $scope.bonusReportAddModel.account = '';
        };

        $scope.checkbalance = function ()
        {
            if ($scope.bonusReportAddModel.account != '')
            {
                for (var i = 0; i < $scope.bonusReportAddModel.accountList.length > 0; i++)
                {
                    if ($scope.bonusReportAddModel.account == $scope.bonusReportAddModel.accountList[i].id)
                    {
                        $scope.bonusReportAddModel.accountbalance = $scope.bonusReportAddModel.accountList[i].balance;
                    }
                }
                var balance = parseFloat($scope.bonusReportAddModel.accountbalance);
                var salary_amount = parseFloat($scope.bonusReportAddModel.bonustotal);
                if (balance < salary_amount)
                {
                    sweet.show('Oops...', 'Posting amount is greater than balance amount', 'error');
                }
            }
        }

        $scope.getAccountlist = function ()
        {
            var getListParam = {};
            getListParam.is_active = 1;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getAccountlist(getListParam, configOption).then(function (response)
            {
                var data = response.data;
                $scope.bonusReportAddModel.accountList = data.list;
            });
        };

        $scope.checkAllData = function ()
        {
            for (var i = 0; i < $scope.bonusReportAddModel.list.length; i++)
            {
                if ($scope.selectAll == true)
                {
                    $scope.bonusReportAddModel.list[i].isSelected = true;
                } else
                {
                    $scope.bonusReportAddModel.list[i].isSelected = false;
                }
            }
            $scope.calculateTotal();
        };

        $scope.calculateTotal = function ()
        {
            $scope.bonusReportAddModel.bonustotal = 0;
            if ($scope.bonusReportAddModel.list.length > 0)
            {
                for (var i = 0; i < $scope.bonusReportAddModel.list.length; i++)
                {
                    if ($scope.bonusReportAddModel.list[i].isSelected == true)
                    {
                        $scope.bonusReportAddModel.bonustotal = parseFloat($scope.bonusReportAddModel.bonustotal) + parseFloat($scope.bonusReportAddModel.list[i].bonus_amount);

                    }
                }
            }

        };

        $scope.saveBonus = function ()
        {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var headers = {};
                headers['screen-code'] = 'empBonus';
                var createBonusParam = {};
                createBonusParam.account_id = parseInt($scope.bonusReportAddModel.account, 10);
                createBonusParam.list = [];
                for (var i = 0; i < $scope.bonusReportAddModel.list.length; i++)
                {
                    var saveBonusParam = {};
                    saveBonusParam.from_date = $scope.searchFilter.fromdate;
                    saveBonusParam.to_date = $scope.searchFilter.todate;
                    saveBonusParam.emp_id = $scope.bonusReportAddModel.list[i].emp_id;
                    saveBonusParam.emp_code = $scope.bonusReportAddModel.list[i].emp_code;
                    saveBonusParam.emp_name = $scope.bonusReportAddModel.list[i].emp_name;
                    saveBonusParam.emp_team_id = $scope.searchFilter.teamInfo;
                    saveBonusParam.bonus_percentage = $scope.bonusReportAddModel.list[i].bonus_percentage;
                    saveBonusParam.bonus_amount = $scope.bonusReportAddModel.list[i].bonus_amount;
                    saveBonusParam.salary_amount = $scope.bonusReportAddModel.list[i].salary_amount;
                    saveBonusParam.present_count = $scope.bonusReportAddModel.list[i].present_count;
                    saveBonusParam.absent_count = $scope.bonusReportAddModel.list[i].absent_count;
                    createBonusParam.list.push(saveBonusParam);
                }
                adminService.saveBonus(createBonusParam, headers).then(function (response) {
                    if (response.data.success === true)
                    {
                        $scope.formReset();
                        $state.go('app.bonusReportList');
                    }
                    $scope.isDataSavingProcess = false;
                });
            } else
            {
                return;
            }

        };

        $scope.getList = function () {

            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'empSalary';

            getListParam.team_id = $scope.searchFilter.teamInfo;
            getListParam.from_date = $scope.searchFilter.fromdate;
            getListParam.to_date = $scope.searchFilter.todate;

            if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
            {
                getListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.dateFormat);
            }
            if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
            {
                getListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.dateFormat);
            }

            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                getListParam.from_date = utilityService.changeDateToSqlFormat(getListParam.from_date, $scope.dateFormat);

            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                getListParam.to_date = utilityService.changeDateToSqlFormat(getListParam.to_date, $scope.dateFormat);
            }

            $scope.bonusReportAddModel.isLoadingProgress = true;
            $scope.bonusReportAddModel.isSearchLoadingProgress = true;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeBonusList(getListParam, configOption, headers).then(function (response)
            {

                if (response.data.success === true)
                {
                    var data = response.data.list;
                    $scope.bonusReportAddModel.list = data;
                    $scope.bonusReportAddModel.total = data.total;
                    for (var i = 0; i < $scope.bonusReportAddModel.list.length; i++)
                    {
                        $scope.bonusReportAddModel.list[i].isSelected = false;
                    }

                } else
                {
                    $scope.bonusReportAddModel.list = [];
                }
                $scope.bonusReportAddModel.isLoadingProgress = false;
                $scope.bonusReportAddModel.isSearchLoadingProgress = false;
            });

        };
        $scope.getTeamList = function ()
        {
            $scope.bonusReportAddModel.isLoadingProgress = true;
            var getListParam = {};
            getListParam.is_active = 1;

            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getEmployeeTeamList(getListParam, configOption).then(function (response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.bonusReportAddModel.TeamList = data.list;
                    $scope.bonusReportAddModel.isLoadingProgress = false;
                }
            });
        };

        $scope.print = function ()
        {
            $window.print();
        };

        //$scope.getList();
        $scope.getAccountlist();
        $scope.getTeamList();
    }]);




