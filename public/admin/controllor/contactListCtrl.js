app.controller('contactListCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', 'APP_CONST', '$httpService', 'utilityService', function($scope, $rootScope, adminService, $filter, Auth, $timeout, APP_CONST, $httpService, utilityService) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.contactModel = {
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
            isLoadingProgress: true

        };
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.contactModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            leadname: '',
            phone: '',
            email: '',
            lead: '',
            companyInfo: '',
            leadId: '',
            fromdate: '',
            todate: '',
            isactive:1
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.fromDateOpen = false;
        $scope.toDateOpen = false;
        $scope.openDate = function(index)
        {
            if (index == 0)
            {
                $scope.fromDateOpen = true;
            }
            else if (index == 1)
            {
                $scope.toDateOpen = true;
            }
        }
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function(id)
        {
            $scope.selectContactId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteContactInfo = function()
        {
            if ($scope.isdeleteProgress == false)
            {
                $scope.isdeleteProgress = true;

                var contactListParam = {};
                contactListParam.id = $scope.selectContactId;
                var headers = {};
                headers['screen-code'] = 'contact';
                var configOption = adminService.handleBothSuccessAndErrorResponseConfig;
                adminService.deleteContact(contactListParam, contactListParam.id, configOption, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                    $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: '',
                phone: '',
                email: '',
                companyInfo: '',
                fromdate: '',
                todate: '',
                isactive:1
            };
            $scope.initTableFilter();
        }
        $scope.getLeadTypeList = function()
        {
            $scope.contactModel.isLoadingProgress = true;
            var leadtypeListParam = {};
            leadtypeListParam.is_active = 1;
            leadtypeListParam.type = 'lead';
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getLeadTypeList(leadtypeListParam, configOption).then(function(response)
            {
                if (response.data.success == true)
                {
                    var data = response.data;
                    $scope.contactModel.leadList = data.list;
                }
                $scope.contactModel.isLoadingProgress = false;
            });

        }
        $scope.getList = function() {

            $scope.contactModel.isLoadingProgress = true;
            var contactListParam = {};
            var headers = {};
            headers['screen-code'] = 'contact';
            contactListParam.id = $scope.searchFilter.leadId;
            contactListParam.name = $scope.searchFilter.leadname;
            contactListParam.phone = $scope.searchFilter.phone;
            contactListParam.email = $scope.searchFilter.email;
            contactListParam.lead_type_id = $scope.searchFilter.lead;
            if ($scope.searchFilter.companyInfo != null && typeof $scope.searchFilter.companyInfo != 'undefined' && typeof $scope.searchFilter.companyInfo.id != 'undefined')
            {
                contactListParam.company_id = $scope.searchFilter.companyInfo.id;
            } else
            {
                contactListParam.company_id = '';
            }
            if ($scope.searchFilter.fromdate != null && $scope.searchFilter.fromdate != '')
            {
                if ($scope.searchFilter.fromdate != null && typeof $scope.searchFilter.fromdate == 'object')
                {
                    contactListParam.from_date = utilityService.parseDateToStr($scope.searchFilter.fromdate, $scope.adminService.appConfig.date_format);
                }
                contactListParam.from_date = utilityService.changeDateToSqlFormat(contactListParam.from_date, $scope.adminService.appConfig.date_format);
            }
            if ($scope.searchFilter.todate != null && $scope.searchFilter.todate != '')
            {
                if ($scope.searchFilter.todate != null && typeof $scope.searchFilter.todate == 'object')
                {
                    contactListParam.to_date = utilityService.parseDateToStr($scope.searchFilter.todate, $scope.adminService.appConfig.date_format);
                }
                contactListParam.to_date = utilityService.changeDateToSqlFormat(contactListParam.to_date, $scope.adminService.appConfig.date_format);
            }
            contactListParam.is_active = $scope.searchFilter.isactive;
            contactListParam.start = ($scope.contactModel.currentPage - 1) * $scope.contactModel.limit;
            contactListParam.limit = $scope.contactModel.limit;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getContactList(contactListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.contactModel.list = data.list;
                    if ($scope.contactModel.list.length > 0)
                    {
                        for (var i = 0; i < $scope.contactModel.list.length; i++)
                        {
                            $scope.contactModel.list[i].index = contactListParam.start + i + 1;
                        }
                    }
                    $scope.contactModel.total = data.total;
                }
                $scope.contactModel.isLoadingProgress = false;

            });

        };
        $scope.getCompanyList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.name = val;
            autosearchParam.is_active = 1;
            return $httpService.get(APP_CONST.API.COMPANY_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = data;
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatCompanyModel = function(model)
        {
            if (model != null)
            {
                return model.name;
            }
            return  '';
        };
        $scope.hasDependentDataLoaded = true;
        $scope.localStorageCount = 0;
        $scope.localStorageRenterCount = 0;
        $scope.localStorageRetrieveCount = 0;
        $scope.localStorageFormName = 'contact_list_form';


        $scope.$on('onLocalStorageFormRenterCompleteEvent', function(event, data) {

            if ($scope.localStorageFormName == data.formName)
            {
                $scope.localStorageCount = data.fieldCount;
            }
        });

        $scope.$on('updateTypeaheadFieldValue', function(event, data) {

            if (data.fieldName == "contact_list_form-contact_company_dropdown")
            {
                $scope.searchFilter.companyInfo = data.value;
            }
        });

        $scope.$on('onLocalStorageFieldRenterCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRenterCount++;
                }
                if ($scope.localStorageRenterCount >= $scope.localStorageCount)
                {
                    console.log('initLocalStorageFieldRetrieveEvent = ' + $scope.localStorageRenterCount);
                    $scope.$emit('initLocalStorageFieldRetrieveEvent', $scope.localStorageFormName);
                }
            }
        });


        $scope.$on('onLocalStorageFieldRetrieveCompleteEvent', function(event, formName) {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                if ($scope.localStorageFormName == formName)
                {
                    $scope.localStorageRetrieveCount++;
                }
                if ($scope.localStorageRetrieveCount >= $scope.localStorageCount && $scope.hasDependentDataLoaded)
                {
                    $scope.initTableFilter();
                }
                console.log('onLocalStorageFieldRetrieveCompleteEvent = ' + $scope.localStorageRetrieveCount);
            }
        });

        $scope.init = function()
        {
            $scope.$emit('initLocalStorageEvent', 'Message in initLocalStorageEvent.');

        };

        $scope.$on('onLocalStorageReadyEvent', function() {
            if ($scope.dealFilter == '' || typeof $scope.dealFilter == 'undefined' || $scope.dealFilter == null)
            {
                $timeout(function() {
                    $scope.init();
                }, 300);
            }
        });

        //   $scope.getList();
        $scope.getLeadTypeList();
    }]);






