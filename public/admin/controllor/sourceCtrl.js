app.controller('sourceCtrl', ['$scope', '$rootScope', 'adminService', '$filter', 'Auth', '$timeout', function($scope, $rootScope, adminService, $filter, Auth, $timeout) {

        $rootScope.getNavigationBlockMsg = null;
        $scope.sourceModel = {
            
            currentPage: 1,
            total: 0,
            limit: 4,
            list: [],
           
            isLoadingProgress: true

        };
        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.sourceModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''
            
        };

        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }

        
        $scope.showdeletePopup = false;
        $scope.isdeleteProgress = false;

        $scope.showPopup = function(id)
        {
            $scope.selectSourceId = id;
            $scope.showdeletePopup = true;
        };

        $scope.closePopup = function()
        {
            $scope.showdeletePopup = false;
            $scope.isdeleteProgress = false;
        };

        $scope.deleteSourceInfo = function( )
        {
            if($scope.isdeleteProgress == false)
            {
               $scope.isdeleteProgress = true;
               
                var getListParam = {};
                getListParam.id = $scope.selectSourceId;
                var headers = {};
                headers['screen-code'] = 'source';
                adminService.deleteSource(getListParam, getListParam.id, headers).then(function(response)
                {
                    var data = response.data;
                    if (data.success == true)
                    {
                        $scope.closePopup();
                        $scope.getList();
                    }
                     $scope.isdeleteProgress = false;
                });
            }
        };

        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: ''
            };
            $scope.initTableFilter();
        }

        $scope.getList = function() {          

            $scope.sourceModel.isLoadingProgress = true;
            var sourceListParam = {};
            var headers = {};
            headers['screen-code'] = 'source';
            sourceListParam.name = $scope.searchFilter.name;
           
            sourceListParam.id = '';
            
            sourceListParam.is_active = 1;
            sourceListParam.start = ($scope.sourceModel.currentPage - 1) * $scope.sourceModel.limit;
            sourceListParam.limit = $scope.sourceModel.limit;
            var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
            adminService.getSourceList(sourceListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)
                {
                    var data = response.data;
                    $scope.sourceModel.list = data.list;
                    $scope.sourceModel.total = data.total;
                        }
                $scope.sourceModel.isLoadingProgress = false;
            });

        };

        $scope.getList();

    }]);




