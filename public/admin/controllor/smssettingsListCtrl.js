
app.controller('smssettingsListCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$timeout', '$filter', 'Auth', 'ValidationFactory', '$state', 'APP_CONST', '$stateParams', '$http', '$window', function($scope, $rootScope, adminService, $httpService, utilityService, $timeout, $filter, Auth, ValidationFactory, $state, APP_CONST, $stateParams, $http, $window) {

        $scope.smsModel = {
            id: '',
            "isactive": 1,
            list: [],
            total: 0,
            limit: 4,
            isLoadingProgress: false
        }

        $scope.searchFilterNameValue = ''

        $scope.pagePerCount = [50, 100];
        $scope.smsModel.limit = $scope.pagePerCount[0];

        $scope.searchFilter = {
            name: ''
            
        };
        $scope.refreshScreen = function()
        {
            $scope.searchFilter = {
                name: ''
            };
            $scope.initTableFilter();
        }
        
        $scope.initTableFilterTimeoutPromise = null;

        $scope.initTableFilter = function()
        {
            if ($scope.initTableFilterTimeoutPromise != null)
            {
                $timeout.cancel($scope.initTableFilterTimeoutPromise);
            }
            $scope.initTableFilterTimeoutPromise = $timeout($scope.getList, 300);
        }
        $scope.validationFactory = ValidationFactory;
        $scope.isDataSavingProcess = false;


        $scope.getList = function()
        {
            $scope.smsModel.isLoadingProgress = true;
            var getListParam = {};
            var headers = {};
            headers['screen-code'] = 'smssettings';
            getListParam.type = 'sms';
            getListParam.id='';
            getListParam.tplname = $scope.searchFilter.name;
            getListParam.start = ($scope.smsModel.currentPage - 1) * $scope.smsModel.limit;
            getListParam.limit = $scope.smsModel.limit;
            var configOption = adminService.handleOnlyErrorResponseConfig;
            adminService.getSmsSettingsList(getListParam, configOption, headers).then(function(response)
            {
                if (response.data.success === true)

                {
                    var data = response.data;
                    if (data.list.length > 0)
                    {
                        $scope.smsModel.list = data.list;
                        $scope.smsModel.total = data.total;
                    }
                }
                $scope.smsModel.isLoadingProgress = false;
            });
        };


        $scope.getList();
    }]);



