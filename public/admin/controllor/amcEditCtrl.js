
app.controller('amcEditCtrl', ['$scope', '$rootScope', 'adminService', '$httpService', 'utilityService', '$filter', 'Auth', '$state', '$stateParams', '$timeout', 'ValidationFactory', 'APP_CONST', 'sweet', function($scope, $rootScope, adminService, $httpService, utilityService, $filter, Auth, $state, $stateParams, $timeout, ValidationFactory, APP_CONST, sweet) {

        $scope.amcEditModel =
                {
                    "customerInfo": '',
                    "productname": '',
                    list: [],
                    itemList: [],
                    duation: '',
                    amount: '',
                    date: "",
                    productList: [],
                    serialno: '',
                    amcdate : ''
                };
        $scope.validationFactory = ValidationFactory;
        $scope.isDataSavingProcess = false;
        $scope.amcEditModel.isLoadingProgress = false;
        $scope.getNavigationBlockMsg = function(pageReload)
        {
            if (typeof $scope.edit_amc_form != 'undefined' && typeof $scope.edit_amc_form.$pristine != 'undefined' && !$scope.edit_amc_form.$pristine)
            {
                return pageReload ? $rootScope.APP_CONST.BLOCKER_MSG.PAGE_RELOAD : $rootScope.APP_CONST.BLOCKER_MSG.PAGE_NAVIGATE;
            }

            return "";
        }
        $scope.formReset = function() {

            if (typeof $scope.edit_amc_form != 'undefined')
            {
                $scope.edit_amc_form.$setPristine();
                $scope.updateAmcInfo();
            }

        };
        $scope.initUpdateDetailTimeoutPromise = null;

        $scope.initUpdateDetail = function()
        {

            if ($scope.initUpdateDetailTimeoutPromise != null)
            {
                $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
            }
            else
            {
                $scope.initUpdateDetailTimeoutPromise = $timeout($scope.updateAmcInfo, 300);
            }
        };
        $scope.adminService = adminService;
        $scope.dateFormat = $rootScope.appConfig.date_format;
        $scope.stateParamId = $stateParams.id;
        $scope.currentDate = new Date();
        $scope.dateOpen = false;
        // $scope.amcListModel.date = $scope.currentDate;
        $scope.openDate = function(index) {

            if (index == 0)
            {
                $scope.dateOpen = true;
            }
        }
        $scope.updateAmcInfo = function() {
            $scope.amcEditModel.customerInfo = {
                id: $scope.amcEditModel.list.customer_id,
                fname: $scope.amcEditModel.list.customer_name
            }
            $scope.amcEditModel.productname = {
                id: $scope.amcEditModel.list.invoice_item_id,
                product_id: $scope.amcEditModel.list.product_id,
                product_name: $scope.amcEditModel.list.product_name,
                invoice_id: $scope.amcEditModel.list.invoice_id,
                serial_no: $scope.amcEditModel.list.serial_no
            }
            // $scope.amcEditModel.serialno = $scope.amcEditModel.list.serial_no;
            $scope.amcEditModel.duation = $scope.amcEditModel.list.duration;
            $scope.amcEditModel.amount = $scope.amcEditModel.list.amount;
            var from_date = utilityService.parseStrToDate($scope.amcEditModel.list.from_date);
            $scope.amcEditModel.date = from_date;
            $scope.amcEditModel.newdate = utilityService.parseStrToDate($scope.amcEditModel.list.from_date);
            $scope.amcEditModel.amcdate = utilityService.parseDateToStr($scope.amcEditModel.newdate, $rootScope.appConfig.date_format); 
            $scope.getProductInfo($scope.amcEditModel.list.customer_id);
            $scope.amcEditModel.status = $scope.amcEditModel.list.status;
        };
        $scope.getCustomerList = function(val)
        {
            var autosearchParam = {};
            autosearchParam.search = val;
            autosearchParam.is_sale = 1;
            return $httpService.get(APP_CONST.API.CUSTOMERS_SEARCH_LIST, autosearchParam, false).then(function(responseData)
            {
                var data = responseData.data.list;
                var hits = [];
                if (data.length > 0)
                {
                    for (var i = 0; i < data.length; i++)
                    {
                        if (data[i].is_sale == '1' || data[i].is_sale == 1)
                        {
                            hits.push(data[i]);
                        }
                    }
                }
                if (hits.length > 10)
                {
                    hits.splice(10, hits.length);
                }
                return hits;
            });
        };
        $scope.formatProductModel = function(model)
        {
            if (model != null)
            {
                $scope.amcEditModel.productname.serialno = model.serial_no;
                return model.product_name;
            }
            return  '';
        }
        $scope.formatCustomerModel = function(model)
        {
            if (model != null)
            {
                var phoneNumber = '';
                if (model.phone != undefined)
                {
                    if (model.phone.indexOf(','))
                    {
                        phoneNumber = model.phone.split(',')[0];
                    }
                }
                if (model.fname != undefined && phoneNumber != '' && model.email != undefined && model.company != undefined)
                {

                    return model.fname + '(' + phoneNumber + ',' + model.email + ',' + model.company + ')';
                } else if (model.fname != undefined && phoneNumber != '')
                {

                    return model.fname + '(' + phoneNumber + ')';
                } else if (model.fname != undefined)
                {
                    return model.fname;
                }
            }
            return  '';
        };

        $scope.getProductInfo = function(val)
        {
            if ($scope.amcEditModel.customerInfo != undefined && $scope.amcEditModel.customerInfo != '' && $scope.amcEditModel.customerInfo.id != undefined)
            {
                var autosearchParam = {};
                 autosearchParam.search = val;
                autosearchParam.is_active = 1;
                autosearchParam.customer_id = $scope.amcEditModel.customerInfo.id;
                return $httpService.get(APP_CONST.API.GET_CUSTOMER_INVOICE_ITEM_LIST, autosearchParam, false).then(function(responseData)
                {
                    var data = responseData.data.list;
                    var hits = data;
                    if (hits.length > 10)
                    {
                        hits.splice(10, hits.length);
                    }
                    return hits;
                });
            }
            else
            {
                sweet.show('Oops...', 'Please select a customer', 'error');
            }
        };


//        $scope.getProductInfo = function()
//        {
//            $scope.amcEditModel.productList = [];
//            if ($scope.amcEditModel.customerInfo != undefined && $scope.amcEditModel.customerInfo != '' && $scope.amcEditModel.customerInfo.id != undefined)
//            {
//                var getParam = {};
//                getParam.customer_id =  $scope.amcEditModel.customerInfo.id;
//                var configOption = adminService.ignoreBothSuccessAndErrorResponseConfig;
//                adminService.getCustomerInvoiceItemList(getParam, configOption).then(function(response) {
//                    if (response.data.success === true)
//                    {
//                        var data = response.data;
//                        $scope.amcEditModel.productList = data.list;
//
//                    }
//
//                });
//            }
//
//        };
        $scope.getAmcDetails = function() {

            var getListParam = {};
            getListParam.id = $stateParams.id;
            $scope.amcEditModel.isLoadingProgress = true;
            adminService.getAmcList(getListParam).then(function(response) {
                var data = response.data.list;
                $scope.amcEditModel.list = data[0];
                $scope.initUpdateDetail();

            });

        };
        $scope.modifyAmc = function() {
            if ($scope.isDataSavingProcess == false)
            {
                $scope.isDataSavingProcess = true;
                var createParam = {};
                var headers = {};
                var id = $scope.amcEditModel.list.id;
                headers['screen-code'] = 'amc';
                createParam.customer_id = $scope.amcEditModel.customerInfo.id;
                createParam.product_id = $scope.amcEditModel.productname.product_id;
                if($scope.amcEditModel.productname.invoice_item_id == 0)
                {
                  createParam.product_id = $scope.amcEditModel.productname.prod_id;  
                }
                createParam.invoice_id = $scope.amcEditModel.productname.invoice_id;
                createParam.Invoice_item_id = $scope.amcEditModel.productname.invoice_item_id;
                createParam.serial_no = $scope.amcEditModel.productname.serial_no;
                createParam.duration = $scope.amcEditModel.duation;
                createParam.from_date = $filter('date')($scope.amcEditModel.date, 'yyyy-MM-dd');
                $scope.to_date = moment($scope.amcEditModel.date).add($scope.amcEditModel.duation, 'years');
                createParam.to_date = $filter('date')($scope.to_date._d, 'yyyy-MM-dd');
                createParam.amount = $scope.amcEditModel.amount;
                var config = adminService.handleSuccessAndErrorResponse;
                adminService.editAmc(createParam, id, config, headers).then(function(response) {
                    if (response.data.success == true)
                    {
                        $state.go('app.amc');
                    }
                    $scope.isDataSavingProcess = false;
                });




            }
        };
             
        $scope.getAmcDetails();

    }]);