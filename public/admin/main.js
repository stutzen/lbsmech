'use strict';
/* Controllers */

angular.module('app')
        .controller('AppCtrl', ['$scope', '$rootScope', '$translate', '$localStorage', '$window', '$location', '$state', '$timeout', '$cookies', 'Auth', 'StutzenHttpService', 'APP_CONST', "$httpService", "adminService", "$http", function ($scope, $rootScope, $translate, $localStorage, $window, $location, $state, $timeout, $cookies, Auth, StutzenHttpService, APP_CONST, httpService, adminService, $http) {

                $rootScope.userModel = {
                    userName: '',
                    role_id: '',
                    role: [''],
                    business_list: []
                };
                $rootScope.appDomain = $window.location.host;
                $rootScope.screenAccessDetail = {
                    "customer": {
                        'is_view': false
                    },
                    "servicerequest": {
                        'is_view': false
                    },
                    "expense": {
                        'is_view': false
                    },
                    "purchaseestimate": {
                        'is_view': false
                    },
                    "purchaseinvoice": {
                        'is_view': false
                    },
                    "salesestimate": {
                        'is_view': false
                    },
                    "salesinvoice": {
                        'is_view': false
                    },
                    "salesorder": {
                        'is_view': false
                    },
                    "advancepayment": {
                        'is_view': false
                    },
                    "product": {
                        'is_view': false
                    },
                    "service": {
                        'is_view': false
                    },
                    "uom": {
                        'is_view': false
                    },
                    "users": {
                        'is_view': false
                    },
                    "role": {
                        'is_view': false
                    },
                    "category": {
                        'is_view': false
                    },
                    "incomecategory": {
                        'is_view': false
                    },
                    "expensecategory": {
                        'is_view': false
                    },
                    "attribute": {
                        'is_view': false
                    },
                    "appsettings": {
                        'is_view': false
                    },
                    "changepassword": {
                        'is_view': false
                    },
                    "purchaseheader": {
                        'is_view': false
                    },
                    "salesheader": {
                        'is_view': false
                    },
                    "produtserviceheader": {
                        'is_view': false
                    },
                    "settingsheader": {
                        'is_view': false
                    },
                    "photographyheader": {
                        'is_view': false
                    },
                    "crmheader": {
                        'is_view': false
                    },
                    "transport": {
                        'is_view': false
                    },
                    "deliveryinstruction": {
                        'is_view': false
                    },
                    "humanResourceheader": {
                        'is_view': false
                    },
                    "manufacturingheader": {
                        'is_view': false
                    }
                    //                    "location": {
                    //                        'is_view': false
                    //                    },
                    ////                    "location1": {
                    ////                        'is_view': false
                    ////                    }

                };
                $rootScope.screenStateCode = {
                    "customer": "app.customers",
                    "purchaseestimate": "app.purchaseQuote",
                    "purchaseinvoice": "app.purchaseInvoice",
                    "salesestimate": "app.quote",
                    "salesorder": "app.salesorder",
                    "advancepayment": "app.advancepayment",
                    "salesinvoice": "app.invoice",
                    "product": "app.productList",
                    "service": "app.serviceList",
                    "uom": "app.uom",
                    "users": "app.userlist",
                    "role": "app.userrole",
                    "tax": "app.taxlist",
                    "incomecategory": "app.incomecategory",
                    "expensecategory": "app.expensecategory",
                    "attribute": "app.attribute",
                    "appSettings": "app.appSettings",
                    "changepassword": "app.changepassword",
                    "dashboard": "app.dashboard",
                    "account": "app.account",
                    "accountbalance": "app.accountbalance",
                    "transactionreport": "app.transaction",
                    "salesreport": "app.sales",
                    "itemsalesreport": "app.itemizedsales",
                    "paymentreport": "app.paymentreport",
                    "receivablereport": "app.receivablereport",
                    "payablereport": "app.payablereport",
                    "partysalesreport": "app.partysalesstatement",
                    "partypurchasereport": "app.partypurchasestatement",
                    "invoicesettings": "app.invoicesettings",
                    "smssettings": "app.smssettings",
                    "emailsettings": "app.emailsettings",
                    "taxreport": "app.tax",
                    "followupreport": "app.followup",
                    "externalwiseproductreport": "app.externalwiseproduct",
                    "receivedqtyreport": "app.receivedqty",
                    "expectedqtyreport": "app.expectedqty",
                    "taxsummaryreport": "app.taxsummary",
                    "transdeposit": "app.deposit",
                    "transexpense": "app.transexpense",
                    "accountstatement": "app.accountstatement",
                    "stockadjustment": "app.stockadjustment",
                    "stockwastage": "app.stockwastage",
                    "minstock": "app.minstock",
                    "inventorystock": "app.inventorystock",
                    "stocktrace": "app.stocktracelist",
                    "activity": "app.activitylist",
                    "activitylog": "app.activitylog",
                    "category": "app.categorylist",
                    "transfer": "app.transferlist",
                    "paymentterms": "app.paymentterms",
                    "databasebackup": "app.databasebackup",
                    "taxgroup": "app.taxgroup",
                    "expensereport": "app.expensereport",
                    "albumlist": "app.albumlist",
                    "deal": "app.deal",
                    "company": "app.company",
                    "stage": "app.stage",
                    "contact": "app.contact",
                    "country": "app.country",
                    "state": "app.state",
                    "city": "app.city",
                    "transport": "app.transport",
                    "deliveryinstruction": "app.deliveryinstruction",
                    "prefixsettings": "app.prefixsettings",
                    "location": "app.location",
                    "attendance": "app.attendance",
                    "employee": "app.employee",
                    "team": "app.employeeteamlist",
                    "production": "app.production",
                    "billofmaterial": "app.billofmaterial",
                    "customergroup": "app.group",
                    "anniversaryreport": "app.anniversaryreport",
                    "areawisesalesreport": "app.areawisesales",
                    "customersettings": 'app.customersettings',
                    "allowance": 'app.allowanceList',
                    "empSalary": 'app.empSalaryList',
                    "advance": "app.advanceList",
                    "advancededuction": "app.deductionList",
                    "acodesettings": 'app.acodesettings',
                    "bonus": 'app.bonus',
                    "empBonus": 'app.bonusReportList',
                    "empPFStatement": 'app.empPFList',
                    "esireport": 'app.esireport',
                    "taskManagement": "app.taskManagement",
                    "advancereport": "app.advancereport",
                    "empTaskType": 'app.empTaskType',
                    "balanceAdvanceReport": "app.balanceAdvanceReport",
                    "workorder": "app.workorder",
                    "grnreport": "app.grnreport",
                    "goodsReceivedNote": "app.goodsReceivedNoteList",
                    "purchaseAdvPayReceipt": "app.purchaseAdvancePayReceipt",
                    "itempurchasereport": "app.itemizedPurchase",
                    "productionReport": "app.productionreport",
                    "stockReport": "app.stockReport",
                    "source": "app.source",
                    "empTask": 'app.empTask',
                    "servicerequest": "app.servicerequest",
                    "leadtype": "app.leadtype",
                    "attendancereport": "app.attendancereport",
                    "spareRequest": "app.spareRequest",
                    "amc": "app.amc",
                    "expensetracker": "app.expensetracker",
                    "expensetrackerreport": "app.expensetrackerreport",
                    "crmdashboard": "app.crmdashboard",
                    "dailyactivityreport": "app.dailyactivityreport",
                    "leadwithoutdeal": "app.leadwithoutdeal",
                    "inactivitydeal": "app.inactivitydeal",
                    "expentrackercategory": "app.expentrackercategory",
                    "materialrequirement": "app.materialrequirement",
                    "consolidatedacode": "app.consolidatedacode",
                    "creditnote": "app.creditnote",
                    "debitnote": "app.debitnote",
                    "workcenter": "app.workcenter",
                    "taskgroup": "app.taskgroup",
                    "salesorderpending": "app.salesorderpending",
                    "issuescreen": "app.issuescreen",
                    "customerbasedreport": "app.customerbasedreport",
                    "department": "app.department",
                    "manufacturingworkorder": "app.manufacturingworkorder",
                    "producttype": "app.producttype",
                    "moworkorder": "app.moworkorder",
                    "revenuereport": "app.revenuereport",
                    "buyermachine": "app.buyermachine",
                    "qualitycontrolpoints": "app.qualitycontrolpoints",
                    "conversionjournal": "app.conversionjournal",
                    "mobasedreport": "app.mobasedreport",
                    "mointernalreceivedreport": "app.mointernalreceivedreport",
                    "itemwiseissuedreport": "app.itemwiseissuedreport",
                    "manufacturingorderwisereport": "app.manufacturingorderwiserport",
                    "expectedrecdqty": "app.expectedrecdqty"                    
                };

                $rootScope.appConfig = {
                    navMenuExpand: true,
                    date_format: "yyyy/MM/dd",
                    currency: "Rs",
                    thousand_seperator: '',
                    uomdisplay: '',
                    uomlabel: '',
                    invoice_item_custom1: '',
                    invoice_item_custom2: '',
                    invoice_item_custom3: '',
                    invoice_item_custom4: '',
                    invoice_item_custom5: '',
                    invoice_item_custom1_label: '',
                    invoice_item_custom2_label: '',
                    invoice_item_custom3_label: '',
                    invoice_item_custom4_label: '',
                    invoice_item_custom5_label: '',
                    purchase_invoice_item_custom1: '',
                    purchase_invoice_item_custom2: '',
                    purchase_invoice_item_custom3: '',
                    purchase_invoice_item_custom4: '',
                    purchase_invoice_item_custom5: '',
                    purchase_invoice_item_custom1_label: '',
                    purchase_invoice_item_custom2_label: '',
                    purchase_invoice_item_custom3_label: '',
                    purchase_invoice_item_custom4_label: '',
                    purchase_invoice_item_custom5_label: '',
                    purchase_uom: '',
                    purchase_uom_label: '',
                    app_tile: '',
                    invoice_prefix: '',
                    pay_to_address: '',
                    signature: '',
                    sms_count: '',
                    bank_detail: '',
                    invoicefrom: '',
                    invoicecc: '',
                    invoice_print_template: '',
                    timezone: ''
                };
                $rootScope.CONTEXT_ROOT = "/";
                /*
                 *  It used to take APP_CONST value in UI
                 *
                 */
                $rootScope.APP_CONST = angular.copy(APP_CONST);
                // config
                $rootScope.app = {
                    roleSelectPopup: false,
                    selectedRoleIndex: -1,
                    selectedRole: '',
                    selectedClinicId: '',
                    selectedClinicName: ''
                }

                $rootScope.getNavigationBlockMsg = null;
                 $rootScope.cookieDisconnectError = '';
                window.onbeforeunload = function (event) {

                    var message = null;
                    if ($rootScope.getNavigationBlockMsg != null)
                    {
                        message = $rootScope.getNavigationBlockMsg(true);
                    }
                    if (message != null && message.length > 0)
                    {
                        return message;
                    }
                }
                window.addEventListener('online', onLineIndicatorHandler);
                window.addEventListener('offline', onLineIndicatorHandler);
                function onLineIndicatorHandler($event, response)
                {
                    if ($event.type !== undefined && $event.type.toLowerCase() === 'offline')
                    {
                        $timeout(function () {
                            $scope.serverDisconnectError = "Disconnected from the server";
                        });
                    } else
                    {
                        $timeout(function () {
                            $scope.serverDisconnectError = "";
                        });
                    }
                }
                ;

                $scope.$on('$destroy', function () {
                    delete window.onbeforeunload;
                    angular.element(window).off('online', onLineIndicatorHandler);
                    angular.element(window).off('offline', onLineIndicatorHandler);
                });
                $scope.getContainerClass = function ()
                {
                    return $rootScope.containerClass;
                }

                $scope.print = function ()
                {
                    window.print();
                }

                $scope.expandHandler = function ()
                {
                    $timeout(function () {
                        $rootScope.appConfig.navMenuExpand = !$rootScope.appConfig.navMenuExpand;
                    }, 0);
                }

                $rootScope.refer = "";
                $rootScope.cloudImageAccessKey = {};
                $rootScope.isLogined = false;
                $rootScope.isUserModelUpdated = false;
                $rootScope.cloudImagePrefixUrl = APP_CONST.CLOUD_IMAGE_PREFIX_URL;
                $rootScope.autoSearchList = [];
                $rootScope.autoSearchLimit = 5;
                $rootScope.searchMapLocation = "";
                $rootScope.searchKeyword = "";
                $scope.cookieAcception = typeof $cookies.get('cookie-acception') == 'undefined' ? false : true;
                $scope.signupRedirectPopup = false;
                $scope.signinRedirectPopup = false;
                $scope.serverDisconnectError = "";
                $scope.actionMsgSuccess = "";
                $scope.actionMsgWarning = "";
                $scope.actionMsgError = "";
                $scope.errorMsgAliveTime = 5000;
                $scope.successMsgAliveTime = 5000;
                $scope.warningMsgAliveTime = 5000;
                $scope.autoSearchServerSyncDelayTime = 200;
                $scope.autoCompleteFilterList = '';
                $scope.mapSearchOptions = {
                    //country: 'uk',
                    //types: '(cities)'
                };
                //            $scope.search = {
                //
                //                autosuggestlist : []
                //
                //            };
                $scope.searchMapLocationGecodeDetail = null;
                $scope.searchMapLocationDetail = null;
                //$scope.searchMapLocation = "";
                $scope.autoSearchDetail = null;
                $scope.aliveAutoSearch = false;
                $scope.autoSearchList = [];
                $scope.defaultState = '';
                $rootScope.isScreenDataLoaded = false;
                $scope.autoSearchTimeoutPromise = null;
                $scope.autoSearchList1 = [{
                        name: 'Global',
                        searchType: 'Company'
                    }, {
                        name: 'Rosy',
                        searchType: 'Staff'
                    }];
                //            $scope.tags = [ 'bootstrap', 'list', 'angular' ];
                //            $scope.allTags = [ 'bootstrap', 'list', 'angular', 'directive', 'edit', 'label', 'modal', 'close', 'button', 'grid', 'javascript', 'html', 'badge', 'dropdown'];
                //
                $scope.$on('updateUserInfoEvent', updateUserInfoEventHandler);
                $scope.$on('updateScreenAccessInfoEvent', updateScreenAccessInfoHandler);
                $scope.$on('updatePredefinedScreenAccessInfoEvent', updatePredefinedScreenAccessInfoHandler);
                $scope.$on('updateBusinessListInfoEvent', updateBusinessListInfoHandler);
                $scope.$on('updateAppSettings', updateAppSettingHandler);
                $scope.$on('httpGenericErrorEvent', genericHttpErrorHandler);
                $scope.$on('httpGenericResponseSuccessEvent', genericHttpResponseHandler);
                $scope.$on('genericErrorEvent', genericErrorHandler);
                $scope.$on('genericSuccessEvent', genericSuccessHandler);
                //temp fix for search keyword update.
                //Started temp fix to update the map location.
                $scope.keywoardChange = function (value) {
                    $rootScope.$broadcast('updateSearchKeywordEvent', value);
                }

                $scope.beyondChange = function (value) {
                    $rootScope.$broadcast('updateSearchBeyondEvent', value);
                }

                //$scope.$on('updateSearchKeywordEvent', updateSearchKeywordEventHandler);
                //$scope.$on('updateSearchBeyondEvent', updateSearchBeyondEventHandler);
                // $scope.$on('updateGeocodeEvent', updateGeocodeEventHandler);

                function updateSearchKeywordEventHandler($event, data)
                {
                    $scope.keyword = data;
                }

                function updateSearchBeyondEventHandler($event, data)
                {
                    $scope.homeSearchBeyond = data;
                }

                $scope.clearPreviousError = function ()
                {
                    $scope.actionMsgError = '';
                    $scope.actionMsgSuccess = '';
                    $scope.actionMsgWarning = '';
                }

                $scope.changeMsgSuccessAction = function ()
                {
                    $scope.actionMsgSuccess = '';
                }

                $scope.changeMsgErrorAction = function ()
                {
                    $scope.actionMsgError = '';
                }

                $scope.changeMsgWarningAction = function ()
                {
                    $scope.actionMsgWarning = '';
                }

                $scope.setErrorMessage = function (msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgError = msg;
                    $timeout(function () {
                        $scope.actionMsgError = '';
                    }, $scope.errorMsgAliveTime);
                };
                $scope.setSuccessMessage = function (msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgSuccess = msg;
                    $timeout(function () {
                        $scope.actionMsgSuccess = '';
                    }, $scope.successMsgAliveTime);
                };
                $scope.setWarningMessage = function (msg)
                {
                    if (msg != '') {
                        $scope.clearPreviousError();
                    }
                    $scope.actionMsgWarning = msg;
                    $timeout(function () {
                        $scope.actionMsgWarning = '';
                    }, $scope.warningMsgAliveTime);
                };
                $scope.logout = function ()
                {
                    httpService.get(APP_CONST.LOGOUT_API_URL, {}, true).then(function (response) {
                        $scope.setSuccessMessage("You have logout successfully.");
                        var cookieOption = {
                            path: "/"
                        };
                        $cookies.remove("api-token", cookieOption);
                        $timeout(function () {
                            $window.location.href = APP_CONST.SSO_LOGOUT_URL;

                        }, 100);
                    });
                }

                $scope.updateUserType = function (userType) {

                    $scope.homeSearchUserType = userType;
                }

                $scope.initUpdateDetailTimeoutPromise = null;
                $scope.initCheckEditAccess = function (state, screencode)
                {
                    if ($scope.initUpdateDetailTimeoutPromise != null)
                    {
                        $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                    }
                    if ($rootScope.isScreenDataLoaded)
                    {
                        $scope.checkEditAccess();
                    } else
                    {
                        $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initCheckEditAccess(state, screencode), 300);
                    }
                };

                $scope.checkEditAccess = function (state, params, screencode)
                {
                    if ($rootScope.screenAccessDetail[screencode].is_modify || $rootScope.screenAccessDetail[screencode].is_view)
                    {
                        params = params.replace(new RegExp('*', 'g'), '\"');
                        var paramsObj = {};
                        paramsObj = JSON.parse('' + params + '');
                        var fullState = '\'' + state + '\'' + ',' + params;
                        $state.go(fullState);
                    }
                };

                function updateUserInfoEventHandler($event, data)
                {

                    if (data.success == true)
                    {
                        $rootScope.isLogined = true;
                        $rootScope.userModel = data.data;
                        $scope.selectDefaultLoginRole();
                        console.log("user");
                    } else
                    {
                        $rootScope.isLogined = false;
                        $rootScope.userModel = {
                            userName: '',
                            role: ['']
                        };
                    }
                    $rootScope.isUserModelUpdated = true;
                }

                $scope.firstScreenCode = false;
                function updatePredefinedScreenAccessInfoHandler($event)
                {
                    $http({
                        method: 'GET',
                        url: APP_CONST.API.PREDEFINED_USER_ACCESS_INFO
                    }).success(function (responseData, status, headers, config) {
                        if (typeof responseData != 'undefined')
                            $rootScope.$broadcast('updateScreenAccessInfoEvent', responseData[$rootScope.userModel.id]);
                    })
                }

                function updateScreenAccessInfoHandler($event, data)
                {
                    var screenAccessInfo = data;
                    var loop;
                    for (loop = 0; loop < screenAccessInfo.length; loop++)
                    {
                        var screenAccess = screenAccessInfo[loop];
                        var screencode = screenAccess.screen_code;
                        if ($rootScope.screenAccessDetail[screencode] != 'undefined')
                        {
                            if ($rootScope.userModel.rolename == 'superadmin')
                            {
                                $rootScope.screenAccessDetail[screencode] = {};
                                $rootScope.screenAccessDetail[screencode].is_create = true;
                                $rootScope.screenAccessDetail[screencode].is_modify = true;
                                $rootScope.screenAccessDetail[screencode].is_view = true;
                                $rootScope.screenAccessDetail[screencode].is_visible_to = 'All';
                                if ($scope.defaultState == '' && !$scope.firstScreenCode && $rootScope.screenAccessDetail[screencode].is_view == true)
                                {
                                    $scope.firstScreenCode = true;
                                    $scope.defaultState = $rootScope.screenStateCode[screencode];
                                }
                                $rootScope.screenAccessDetail[screencode].is_delete = true;
                                $rootScope.screenAccessDetail[screencode].section_sqno = screenAccessInfo[loop].section_sqno;
                            } else
                            {
                                $rootScope.screenAccessDetail[screencode] = {};
                                $rootScope.screenAccessDetail[screencode].is_create = (screenAccessInfo[loop].is_create == 1 ? true : false);
                                $rootScope.screenAccessDetail[screencode].is_modify = (screenAccessInfo[loop].is_modify == 1 ? true : false);
                                $rootScope.screenAccessDetail[screencode].is_view = (screenAccessInfo[loop].is_view == 1 ? true : false);
                                $rootScope.screenAccessDetail[screencode].is_visible_to = screenAccessInfo[loop].is_visible_to;
                                if ($scope.defaultState == '' && !$scope.firstScreenCode && $rootScope.screenAccessDetail[screencode].is_view)
                                {
                                    $scope.firstScreenCode = true;
                                    $scope.defaultState = $rootScope.screenStateCode[screencode];
                                }
                                $rootScope.screenAccessDetail[screencode].is_delete = (screenAccessInfo[loop].is_delete == 1 ? true : false);
                                $rootScope.screenAccessDetail[screencode].section_sqno = screenAccessInfo[loop].section_sqno;
                            }
                        }
                        $rootScope.isScreenDataLoaded = true;
                    }
                    var screenCode = ['purchaseestimate', 'purchaseinvoice', 'salesestimate', 'salesinvoice', 'amc', 'salesorder', 'advancepayment', 'deliveryinstruction',
                        'product', 'service', 'uom', 'users', 'role', 'tax', 'taxgroup', 'incomecategory', 'expensecategory', 'attribute', 'appsettings', 'invoicesettings', 'smssettings',
                        'emailsettings', 'country', 'state', 'city', 'transport', 'prefixsettings', 'billofmaterial', 'customergroup', 'account', 'accountbalance', 'transdeposit', 'transexpense', 'salesreport', 'itemsalesreport', 'receivablereport',
                        'payablereport', 'partysalesreport', 'partypurchasereport', 'taxreport', 'followupreport', 'externalwiseproductreport', 'receivedqtyreport', 'expectedqtyreport', 'taxsummaryreport', 'accountstatement', 'stockadjustment', 'stockwastage',
                        'minstock', 'inventorystock', 'stocktrace', 'databasebackup', 'activitylog', 'albumlist', 'stage', 'deal', 'company', 'contact', 'acodesettings', 'bonus', 'customersettings',
                        'location', 'employee', 'team', 'production', 'attendance', 'anniversaryreport', 'areawisesalesreport', 'allowance', 'empSalary', 'empBonus', 'advance', 'advancededuction', 'empPFStatement', 'esireport',
                        'taskManagement', 'empTaskType', 'empTask', 'balanceAdvanceReport', 'advancereport', 'workorder', 'grnreport', 'goodsReceivedNote', 'purchaseAdvPayReceipt', 'itempurchasereport', 'productionReport', 'stockReport', 'source',
                        'servicerequest', 'leadtype', 'attendancereport', 'spareRequest', 'expensetracker', 'expensetrackerreport', 'crmdashboard', 'leadwithoutdeal', 'inactivitydeal', 'dailyactivityreport', 'expentrackercategory', 'materialrequirement', 'consolidatedacode', 'creditnote',
                        'debitnote', 'workcenter', 'taskgroup', 'salesorderpending', 'issuescreen', 'customerbasedreport', 'department', 'manufacturingworkorder', 'producttype', 'moworkorder', 'revenuereport', 'buyermachine', 'qualitycontrolpoints', 'conversionjournal', 'mobasedreport', 'mointernalreceivedreport', 
                        'itemwiseissuedreport', 'manufacturingorderwisereport','expectedrecdqty','suggestions','helpmanual','reportbug'];

                    for (var i = 0; i < screenCode.length; i++)
                    {
                        if ($rootScope.screenAccessDetail[screenCode[i]] == 'undefined' || $rootScope.screenAccessDetail[screenCode[i]] == '' || $rootScope.screenAccessDetail[screenCode[i]] == null)
                        {
                            $rootScope.screenAccessDetail[screenCode[i]] = {};
                            $rootScope.screenAccessDetail[screenCode[i]].is_create = false;
                            $rootScope.screenAccessDetail[screenCode[i]].is_modify = false;
                            $rootScope.screenAccessDetail[screenCode[i]].is_view = false;
                        }
                    }
                    if ($rootScope.screenAccessDetail.purchaseestimate.is_view || $rootScope.screenAccessDetail.purchaseinvoice.is_view || $rootScope.screenAccessDetail.purchaseAdvPayReceipt.is_view)
                    {
                        $rootScope.screenAccessDetail.purchaseheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.salesestimate.is_view || $rootScope.screenAccessDetail.salesinvoice.is_view || $rootScope.screenAccessDetail.salesorder.is_view || $rootScope.screenAccessDetail.amc.is_view
                            || $rootScope.screenAccessDetail.advancepayment.is_view || $rootScope.screenAccessDetail.goodsReceivedNote.is_view || $rootScope.screenAccessDetail.deliveryinstruction.is_view
                            || $rootScope.screenAccessDetail.spareRequest.is_view || $rootScope.screenAccessDetail.buyermachine.is_view)
                    {
                        $rootScope.screenAccessDetail.salesheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.product.is_view || $rootScope.screenAccessDetail.service.is_view || $rootScope.screenAccessDetail.uom.is_view)
                    {
                        $rootScope.screenAccessDetail.produtserviceheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.users.is_view || $rootScope.screenAccessDetail.role.is_view || $rootScope.screenAccessDetail.tax.is_view || $rootScope.screenAccessDetail.taxgroup.is_view
                            || $rootScope.screenAccessDetail.incomecategory.is_view || $rootScope.screenAccessDetail.expensecategory.is_view || $rootScope.screenAccessDetail.attribute.is_view
                            || $rootScope.screenAccessDetail.appsettings.is_view || $rootScope.screenAccessDetail.invoicesettings.is_view
                            || $rootScope.screenAccessDetail.smssettings.is_view || $rootScope.screenAccessDetail.emailsettings.is_view || $rootScope.screenAccessDetail.country.is_view || $rootScope.screenAccessDetail.state.is_view || $rootScope.screenAccessDetail.city.is_view
                            || $rootScope.screenAccessDetail.transport.is_view || $rootScope.screenAccessDetail.prefixsettings.is_view
                            || $rootScope.screenAccessDetail.billofmaterial.is_view || $rootScope.screenAccessDetail.customergroup.is_view || $rootScope.screenAccessDetail.empTaskType.is_view || $rootScope.screenAccessDetail.customersettings.is_view || $rootScope.screenAccessDetail.allowance.is_view || $rootScope.screenAccessDetail.acodesettings.is_view || $rootScope.screenAccessDetail.bonus.is_view
                            || $rootScope.screenAccessDetail.source.is_view || $rootScope.screenAccessDetail.empTask.is_view || $rootScope.screenAccessDetail.stage.is_view || $rootScope.screenAccessDetail.leadtype.is_view || $rootScope.screenAccessDetail.producttype.is_view
                            || $rootScope.screenAccessDetail.suggestions.is_view || $rootScope.screenAccessDetail.helpmanual.is_view || $rootScope.screenAccessDetail.reportbug.is_view)
                    {
                        $rootScope.screenAccessDetail.settingsheader.is_view = true;
                    }
                    if ($rootScope.screenAccessDetail.account.is_view || $rootScope.screenAccessDetail.accountbalance.is_view)
                    {
                        $rootScope.screenAccessDetail.bankheader = true;
                    }
                    if ($rootScope.screenAccessDetail.transdeposit.is_view || $rootScope.screenAccessDetail.transexpense.is_view || $rootScope.screenAccessDetail.creditnote.is_view || $rootScope.screenAccessDetail.debitnote.is_view)
                    {
                        $rootScope.screenAccessDetail.transactionheader = true;
                    }
                    if ($rootScope.screenAccessDetail.salesreport.is_view || $rootScope.screenAccessDetail.itemsalesreport.is_view || $rootScope.screenAccessDetail.itempurchasereport.is_view || $rootScope.screenAccessDetail.productionReport.is_view || $rootScope.screenAccessDetail.stockReport.is_view
                            || $rootScope.screenAccessDetail.receivablereport.is_view || $rootScope.screenAccessDetail.payablereport.is_view ||
                            $rootScope.screenAccessDetail.partysalesreport.is_view || $rootScope.screenAccessDetail.partypurchasereport.is_view
                            || $rootScope.screenAccessDetail.empPFStatement.is_view || $rootScope.screenAccessDetail.taxreport.is_view || $rootScope.screenAccessDetail.followupreport.is_view || $rootScope.screenAccessDetail.externalwiseproductreport.is_view || $rootScope.screenAccessDetail.receivedqtyreport.is_view || $rootScope.screenAccessDetail.expectedqtyreport.is_view || $rootScope.screenAccessDetail.taxsummaryreport.is_view
                            || $rootScope.screenAccessDetail.accountstatement.is_view || $rootScope.screenAccessDetail.balanceAdvanceReport.is_view || $rootScope.screenAccessDetail.advancereport.is_view
                            || $rootScope.screenAccessDetail.anniversaryreport.is_view || $rootScope.screenAccessDetail.areawisesalesreport.is_view || $rootScope.screenAccessDetail.esireport.is_view
                            || $rootScope.screenAccessDetail.grnreport.is_view || $rootScope.screenAccessDetail.attendancereport.is_view || $rootScope.screenAccessDetail.expensetrackerreport.is_view || $rootScope.screenAccessDetail.leadwithoutdeal.is_view || $rootScope.screenAccessDetail.inactivitydeal.is_view || $rootScope.screenAccessDetail.dailyactivityreport.is_view || $rootScope.screenAccessDetail.expentrackercategory.is_view
                            || $rootScope.screenAccessDetail.consolidatedacode.is_view || $rootScope.screenAccessDetail.salesorderpending.is_view || $rootScope.screenAccessDetail.customerbasedreport.is_view || $rootScope.screenAccessDetail.materialrequirement.is_view
                            || $rootScope.screenAccessDetail.moworkorder.is_view || $rootScope.screenAccessDetail.revenuereport.is_view || $rootScope.screenAccessDetail.mobasedreport.is_view
                            || $rootScope.screenAccessDetail.mointernalreceivedreport.is_view || $rootScope.screenAccessDetail.itemwiseissuedreport.is_view || $rootScope.screenAccessDetail.manufacturingorderwisereport.is_view || $rootScope.screenAccessDetail.expectedrecdqty.is_view)
                    {
                        $rootScope.screenAccessDetail.reportheader = true;
                    }
                    if ($rootScope.screenAccessDetail.stockadjustment.is_view || $rootScope.screenAccessDetail.stockwastage.is_view ||
                            $rootScope.screenAccessDetail.minstock.is_view || $rootScope.screenAccessDetail.inventorystock.is_view
                            || $rootScope.screenAccessDetail.stocktrace.is_view)
                    {
                        $rootScope.screenAccessDetail.inventoryheader = true;
                    }
                    if ($rootScope.screenAccessDetail.databasebackup.is_view || $rootScope.screenAccessDetail.activitylog.is_view)
                    {
                        $rootScope.screenAccessDetail.utilityheader = true;
                    }
                    if ($rootScope.screenAccessDetail.albumlist.is_view)
                    {
                        $rootScope.screenAccessDetail.photographyheader = true;
                    }
                    if ($rootScope.screenAccessDetail.stage.is_view || $rootScope.screenAccessDetail.deal.is_view || $rootScope.screenAccessDetail.company.is_view || $rootScope.screenAccessDetail.contact.is_view || $rootScope.screenAccessDetail.crmdashboard.is_view)
                    {
                        $rootScope.screenAccessDetail.crmheader = true;
                    }
                    if ($rootScope.screenAccessDetail.location.is_view || $rootScope.screenAccessDetail.employee.is_view || $rootScope.screenAccessDetail.team.is_view
                            || $rootScope.screenAccessDetail.production.is_view || $rootScope.screenAccessDetail.attendance.is_view || $rootScope.screenAccessDetail.empSalary.is_view
                            || $rootScope.screenAccessDetail.empBonus.is_view || $rootScope.screenAccessDetail.advance.is_view || $rootScope.screenAccessDetail.advancededuction.is_view || $rootScope.screenAccessDetail.expensetracker.is_view)
                    {
                        $rootScope.screenAccessDetail.humanResourceheader = true;
                    }
                    if ($rootScope.screenAccessDetail.workcenter.is_view || $rootScope.screenAccessDetail.taskgroup.is_view || $rootScope.screenAccessDetail.issuescreen.is_view || $rootScope.screenAccessDetail.department.is_view
                            || $rootScope.screenAccessDetail.manufacturingworkorder.is_view || $rootScope.screenAccessDetail.workorder.is_view || $rootScope.screenAccessDetail.qualitycontrolpoints.is_view
                            || $rootScope.screenAccessDetail.conversionjournal.is_view)
                    {
                        $rootScope.screenAccessDetail.manufacturingheader = true;
                    }

                }
                $rootScope.displayUrl = '';
                function updateBusinessListInfoHandler($event, data)
                {
                    var businessList = data;
                    var sso_token = $cookies.get("sso-token");
                    for (var loop = 0; loop < businessList.length; loop++)
                    {
                        var url = businessList[loop].bu_name;
                        businessList[loop]['url'] = "http://" + businessList[loop].bu_name + '/public/authorization.php?sso-token=' + sso_token;
                    }
                    $rootScope.userModel.business_list = businessList;
                    for (var loop = 0; loop < businessList.length; loop++)
                    {
                        var url = businessList[loop].bu_name;
                        if (window.location.host == url)
                        {
                            businessList[loop].hide = true;
                            $rootScope.displayUrl = businessList[loop].bu_name;
                        } else
                        {
                            businessList[loop].hide = false;
                        }
                    }
                    $rootScope.userModel.business_list = businessList;
                }

                function updateAppSettingHandler($event, data)
                {
                    var loop;
                    if (data.total > 0)
                    {
                        for (loop = 0; loop < data.total; loop++)
                        {
                            if (data.list[loop].setting == "date_format")
                            {
                                $rootScope.appConfig.date_format = data.list[loop].value;

                            } else if (data.list[loop].setting == "currency")
                            {
                                $rootScope.appConfig.currency = data.list[loop].value;
                            } else if (data.list[loop].setting == "thousand_seperator")
                            {
                                $rootScope.appConfig.thousand_seperator = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_uom")
                            {
                                $rootScope.appConfig.uomdisplay = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_uom_label")
                            {
                                $rootScope.appConfig.uomlabel = (data.list[loop].value == null || data.list[loop].value == '') ? 'Measure' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom1")
                            {
                                $rootScope.appConfig.invoice_item_custom1 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom2")
                            {
                                $rootScope.appConfig.invoice_item_custom2 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom3")
                            {
                                $rootScope.appConfig.invoice_item_custom3 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom4")
                            {
                                $rootScope.appConfig.invoice_item_custom4 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom5")
                            {
                                $rootScope.appConfig.invoice_item_custom5 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "invoice_item_custom1_label")
                            {
                                $rootScope.appConfig.invoice_item_custom1_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 1' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom2_label")
                            {
                                $rootScope.appConfig.invoice_item_custom2_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 2' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom3_label")
                            {
                                $rootScope.appConfig.invoice_item_custom3_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 3' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom4_label")
                            {
                                $rootScope.appConfig.invoice_item_custom4_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 4' : data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_item_custom5_label")
                            {
                                $rootScope.appConfig.invoice_item_custom5_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 5' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom1")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom1 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom2")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom2 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom3")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom3 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom4")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom4 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom5")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom5 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom1_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom1_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 1' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom2_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom2_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 2' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom3_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom3_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 3' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom4_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom4_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 4' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_invoice_item_custom5_label")
                            {
                                $rootScope.appConfig.purchase_invoice_item_custom5_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 5' : data.list[loop].value;
                            } else if (data.list[loop].setting == "purchase_uom")
                            {
                                $rootScope.appConfig.purchase_uom = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "purchase_uom_label")
                            {
                                $rootScope.appConfig.purchase_uom_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Measure' : data.list[loop].value;
                            } else if (data.list[loop].setting == "app_tile")
                            {
                                $rootScope.appConfig.app_tile = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_prefix")
                            {
                                $rootScope.appConfig.invoice_prefix = data.list[loop].value;
                            } else if (data.list[loop].setting == "signature")
                            {
                                $rootScope.appConfig.signature = data.list[loop].value;
                            } else if (data.list[loop].setting == "sms_count")
                            {
                                $rootScope.appConfig.sms_count = data.list[loop].value;
                            } else if (data.list[loop].setting == "pay_to_address")
                            {
                                $rootScope.appConfig.pay_to_address = data.list[loop].value;
                            } else if (data.list[loop].setting == "bank_detail")
                            {
                                $rootScope.appConfig.bank_detail = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoicefrom")
                            {
                                $rootScope.appConfig.invoicefrom = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoicecc")
                            {
                                $rootScope.appConfig.invoicecc = data.list[loop].value;
                            } else if (data.list[loop].setting == "invoice_print_template")
                            {
                                $rootScope.appConfig.invoice_print_template = data.list[loop].value;
                            } else if (data.list[loop].setting == "dual_language_support")
                            {
                                $rootScope.appConfig.dual_language_support = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "dual_language_label")
                            {
                                $rootScope.appConfig.dual_language_label = data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date1")
                            {
                                $rootScope.appConfig.customer_special_date1 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date2")
                            {
                                $rootScope.appConfig.customer_special_date2 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date3")
                            {
                                $rootScope.appConfig.customer_special_date3 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date4")
                            {
                                $rootScope.appConfig.customer_special_date4 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date5")
                            {
                                $rootScope.appConfig.customer_special_date5 = data.list[loop].value == 1 ? true : false;
                            } else if (data.list[loop].setting == "customer_special_date1_label")
                            {
                                $rootScope.appConfig.customer_special_date1_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 1' : data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date2_label")
                            {
                                $rootScope.appConfig.customer_special_date2_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 2' : data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date3_label")
                            {
                                $rootScope.appConfig.customer_special_date3_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 3' : data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date4_label")
                            {
                                $rootScope.appConfig.customer_special_date4_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 4' : data.list[loop].value;
                            } else if (data.list[loop].setting == "customer_special_date5_label")
                            {
                                $rootScope.appConfig.customer_special_date5_label = (data.list[loop].value == null || data.list[loop].value == '') ? 'Option 5' : data.list[loop].value;
                            }
                        }
                        adminService.appConfig = $rootScope.appConfig;
                    }
                }

                function genericHttpErrorHandler($event, response)
                {
                    if (response.status == "404")
                    {
                        $scope.setErrorMessage("Request Not Acceptable.");
                    } else if (response.status == "406")
                    {
                        $scope.setErrorMessage("Request Not Acceptable.");
                    } else if (response.status == "500")
                    {
                        $scope.setErrorMessage("Internal Server Error.");
                    } else if (response.status == "503")
                    {
                        $scope.setErrorMessage("Service Unavailable");
                    } else
                    {
                        $scope.setErrorMessage(response.statusText + " (" + response.status + ")");
                    }


                }

                function genericHttpResponseHandler($event, response)
                {
                    if (typeof response.data != 'undefined')
                    {
                        if (typeof response.data.success != 'undefined')
                        {
                            if (response.data.success == true)
                            {
                                $scope.setSuccessMessage(response.data.message);
                            } else
                            {
                                $scope.setErrorMessage(response.data.message);
                            }

                        }
                    }
                }

                function genericErrorHandler($event, response)
                {
                    $scope.setErrorMessage(response);
                }
                function genericSuccessHandler($event, response)
                {
                    $scope.setSuccessMessage(response);
                }

                $scope.setCookie = function (key, value) {

                    var cookieOption = {
                        path: "/"
                    };
                    $cookies.put(key, value, cookieOption);
                };
                $scope.cookieAccept = function () {

                    $scope.setCookie('cookie-acception', true);
                    $scope.cookieAcception = typeof $cookies.get('cookie-acception') == 'undefined' ? false : true;
                };
                $scope.altSearch = function (userType) {

                    $scope.homeSearchUserType = userType;
                    $scope.search();
                }

                $scope.openPopup = function (value)
                {
                    if (value === 'register')
                    {
                        $scope.signupRedirectPopup = true;
                    } else if (value === 'signin')
                    {
                        $scope.signinRedirectPopup = true;
                    } else if (value === 'role')
                    {
                        if ($scope.app.roleSelectPopup)
                        {
                            $scope.app.roleSelectPopup = false;
                        } else
                        {
                            $scope.app.roleSelectPopup = true;
                        }
                    }
                }

                $scope.closePopup = function (value)
                {
                    if (value === 'register')
                    {
                        $scope.signupRedirectPopup = false;
                    } else if (value === 'signin')
                    {
                        $scope.signinRedirectPopup = false;
                    } else if (value === 'role')
                    {
                        $scope.app.roleSelectPopup = false;
                    }

                }

                $scope.selectDefaultLoginRole = function (value)
                {
                    $rootScope.app.selectedRole = $rootScope.userModel.rolename;
                    $rootScope.app.selectedRoleIndex = -1;
                    $rootScope.app.selectedClinicId = '';
                    $rootScope.app.selectedClinicName = '';
                    $scope.closePopup('role');
                }
                $scope.companyChangeHandler = function (index)
                {
                    if (index < $rootScope.userModel.adminCompId.length)
                    {
                        $rootScope.app.selectedRoleIndex = index;
                        $rootScope.app.selectedRole = APP_CONST.ROLE.ADMIN;
                        $rootScope.app.selectedClinicId = $rootScope.userModel.adminCompId[index].id;
                        $rootScope.app.selectedClinicName = $rootScope.userModel.adminCompId[index].name;
                    }

                    $scope.closePopup('role');
                }
                $scope.initUpdateDetailTimeoutPromise = null;
                $scope.initUpdateDetail = function ()
                {

                    if ($scope.initUpdateDetailTimeoutPromise != null)
                    {
                        $timeout.cancel($scope.initUpdateDetailTimeoutPromise);
                    }
                    if ($rootScope.isScreenDataLoaded)
                    {
                        if ($state.current.name == 'app')
                        {
                            $state.go($scope.defaultState);
                        }
                    } else
                    {
                        $scope.initUpdateDetailTimeoutPromise = $timeout($scope.initUpdateDetail, 300);
                    }
                }

                $scope.$on('$viewContentLoaded', function () {
                    console.log('page load completed');
                    $scope.initUpdateDetail();
                    $scope.$broadcast('mapInitEvent', 'Start init the map.');
                });

            }])
        .factory('Auth', function ($http, $cookies, $location, $window, $rootScope, APP_CONST) {

            var factory = {};
            //factory.USER_INFO_SYNC_API ="/user/userInfo";

            factory.userModel = null;
            factory.isLogined = function ()
            {
                if ($rootScope.isUserModelUpdated)
                    return $rootScope.isLogined;
                return false;
            }
            factory.checkCookie = function ()
            {
                if (typeof $cookies.get("api-token") == 'undefined')
                {
                    $rootScope.isLogined = false;
                    $rootScope.actionMsgError = "Unauthorized";
                    $rootScope.cookieDisconnectError = "Already You loggedoff";

                }
            }
            factory.getUserInfo = function () {

                var cookieData = null;
                if (typeof $cookies.get("api-token") != 'undefined')
                {
                    cookieData = $cookies.get("api-token");
                } else
                {
                    //$window.location.href = $window.location.protocol + "//" + $window.location.host + $rootScope.CONTEXT_ROOT + "/login";
                    $window.location.href = APP_CONST.SSO_LOGIN_URL;
                }
                return $http({
                    method: 'GET',
                    url: APP_CONST.USER_INFO_SYNC_API,
                    headers: {
                        'api-token': cookieData
                    }

                }).success(function (data, status, headers, config) {

                    if (typeof data.data != 'undefined')
                        factory.userModel = data.data;
                    $rootScope.$broadcast('updateUserInfoEvent', data);
                    return $http({
                        method: 'GET',
                        url: APP_CONST.APP_SETTINGS_LIST,
                        headers: {
                            'api-token': cookieData
                        }
                    }).success(function (data, status, headers, config) {
                        if (typeof data.list != 'undefined')
                            $rootScope.$broadcast('updateAppSettings', data);


                        $http({
                            method: 'GET',
                            url: APP_CONST.API.USER_ACCESS_INFO,
                            params: {
                                role_id: $rootScope.userModel.role_id
                            },
                            headers: {
                                'api-token': cookieData
                            }
                        }).success(function (data, status, headers, config) {

                            if (typeof data.list != 'undefined')
                            {
//                                if ($rootScope.userModel.id === 62 || $rootScope.userModel.id === 54 || $rootScope.userModel.id === 66)
//                                {
//                                    $rootScope.$broadcast('updatePredefinedScreenAccessInfoEvent');
//                                } else
//                                {
                                $rootScope.$broadcast('updateScreenAccessInfoEvent', data.list);
//                                }
                            }
                        }).error(function (data, status, headers, config) {

                        });
                        //Get busineess List 
                        $http({
                            method: 'GET',
                            url: APP_CONST.API.GET_USER_BUSINESS_LIST,
                            params: {},
                            headers: {
                                'api-token': cookieData
                            }
                        }).success(function (data, status, headers, config) {

                            if (typeof data.list != 'undefined')
                                $rootScope.$broadcast('updateBusinessListInfoEvent', data.list);
                        }).error(function (data, status, headers, config) {

                        });
                    }).error(function (data, status, headers, config) {

                    });
                }).error(function (data, status, headers, config) {

                });
            }

            factory.loginAsync = function ()
            {

                if (!factory.isLogined())
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + "/app/login/#/login-customer" + "?refer=" + encodeURIComponent($location.absUrl());
                    $window.location.href = landingUrl;
                }
            }

            factory.setUserModel = function (userData, referUrl)
            {
                if (userData != null)
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + $rootScope.CONTEXT_ROOT;
                } else
                {
                    var landingUrl = $window.location.protocol + "//" + $window.location.host + $rootScope.CONTEXT_ROOT + "/login";
                }
                var previousRole = "";
                if (factory.userModel != null)
                {
                    previousRole = factory.userModel.userType;
                }
                factory.userModel = userData;
                if (referUrl != "")
                {
                    landingUrl = decodeURIComponent(referUrl);
                }
                if (referUrl == "")
                {
                    if (factory.userModel != null)
                    {
                        var userRole = factory.userModel.role;
                        if (userRole[0] === APP_CONST.ROLE.STAFF)
                        {
                            landingUrl += APP_CONST.REDIRECT.STAFF_LOGIN_SUCCESS;
                        } else if (userRole[0] === APP_CONST.ROLE.APP_MANAGER)
                        {
                            landingUrl += APP_CONST.REDIRECT.APP_MANAGER_LOGIN_SUCCESS;
                        } else if (userRole[0] === APP_CONST.ROLE.ROOT_ADMIN)
                        {
                            landingUrl += APP_CONST.REDIRECT.ROOT_ADMIN_LOGIN_SUCCESS;
                        } else if (userRole[0] == "-1")
                        {
                            landingUrl += APP_CONST.REDIRECT.UNMAPPED_ROLE_REDIRECT;
                        }
                    }
                }

                $window.location.href = landingUrl;
            };
            factory.validatePageAccess = function () {
                var landingUrl = $window.location.protocol + "//" + $window.location.host;
                if ($rootScope.app.selectedRole === APP_CONST.ROLE.STAFF)
                {
                    landingUrl += APP_CONST.REDIRECT.STAFF_LOGIN_SUCCESS;
                } else if ($rootScope.app.selectedRole === APP_CONST.ROLE.APP_MANAGER)
                {
                    landingUrl += APP_CONST.REDIRECT.APP_MANAGER_LOGIN_SUCCESS;
                } else if ($rootScope.app.selectedRole === APP_CONST.ROLE.ROOT_ADMIN)
                {
                    landingUrl += APP_CONST.REDIRECT.ROOT_ADMIN_LOGIN_SUCCESS;
                } else if ($rootScope.app.selectedRole == "-1")
                {
                    landingUrl += APP_CONST.REDIRECT.UNMAPPED_ROLE_REDIRECT;
                }
                $window.location.href = landingUrl;
            };
            return factory;
        })
        .factory('StutzenHttpInterceptor', ['$q', '$injector', '$rootScope', '$window', '$location', function ($q, $injector, $rootScope, $window, $location) {
                var stutzenHttpInterceptor = {};
                stutzenHttpInterceptor.responseError = function (response) {

                    $rootScope.$broadcast('httpGenericErrorEvent', response);
                    return $q.reject(response);
                }

                stutzenHttpInterceptor.response = function (response) {


                    //Global Redirect for session out
                    var responseData = "" + response.data;
                    var searchKey = 'swf/login_check';
                    if (responseData.indexOf(searchKey) != -1)
                    {
                        $window.location.href = $window.location.protocol + "//" + $window.location.host + "/login";
                    }

                    if (typeof response.config != 'undefined' && typeof response.config.config != 'undefined')
                    {
                        var config = response.config.config;
                        if (config.handleResponse == true)
                        {
                            var hasSuccessResponse = false;
                            var hasFailureResponse = false;
                            if (typeof response.data != 'undefined')
                            {
                                if (typeof response.data.success != 'undefined')
                                {
                                    if (response.data.success == true)
                                    {
                                        hasSuccessResponse = true;
                                    } else
                                    {
                                        hasFailureResponse = true;
                                    }

                                }
                            }


                            if (config.has_success_alert != undefined && config.has_error_alert != undefined)
                            {
                                if (config.has_success_alert && hasSuccessResponse)
                                {
                                    $rootScope.$broadcast('httpGenericResponseSuccessEvent', response);
                                } else if (config.has_error_alert && hasFailureResponse)
                                {
                                    $rootScope.$broadcast('httpGenericResponseSuccessEvent', response);
                                }
                            } else
                            {
                                $rootScope.$broadcast('httpGenericResponseSuccessEvent', response);
                            }
                        }

                    }
                    return response;
                }



                return stutzenHttpInterceptor;
            }])

        .factory('StutzenHttpService', ['$http', function ($http) {
                var stutzenHttpService = {};
                stutzenHttpService.get = function (url, data, handleResponse, cookieData) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'GET',
                        url: url,
                        params: data,
                        headers: {
                            'api-token': cookieData
                        },
                        config: configOption
                    }).success(function (data, status, headers, config) {
                    }).error(function (data, status, headers, config) {

                    });
                }

                stutzenHttpService.post = function (url, data, handleResponse) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'POST',
                        url: url,
                        data: angular.toJson(data, 0),
                        config: configOption
                    }).success(function (data, status, headers, config) {
                    }).error(function (data, status, headers, config) {

                    });
                }
                stutzenHttpService.formPost = function (url, data, handleResponse) {
                    if (typeof handleResponse == 'undefined') {
                        handleResponse = false;
                    }
                    var configOption = {};
                    configOption.handleResponse = handleResponse;
                    return $http({
                        method: 'POST',
                        url: url,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'handleResponse': handleResponse
                        },
                        transformRequest: function (obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: data,
                        config: configOption

                    }).success(function (data, status, headers, config) {
                    }).error(function (data, status, headers, config) {

                    });
                }
                return stutzenHttpService;
            }])
        .factory('MapLoaderService', function ($timeout) {

            var mapLoaderService = {};
            mapLoaderService.map = null;
            mapLoaderService.markerList = [];
            mapLoaderService.init = function (domId, centerPoint, markers) {

                var latValue = 51.1464659;
                var lngValue = 0.875019;
                if (centerPoint != null)
                {
                    latValue = centerPoint.lagtitude;
                    lngValue = centerPoint.longitude;
                } else if (markers != null && markers.length)
                {
                    latValue = markers[0].lagtitude;
                    lngValue = markers[0].longitude;
                }

                mapLoaderService.map = new google.maps.Map(document.getElementById(domId), {
                    zoom: 10,
                    center: {
                        lat: latValue,
                        lng: lngValue
                    }
                });
                $timeout(function () {
                    mapLoaderService.loadMarkerPoint(markers);
                }, 500);
            }
            mapLoaderService.loadMarkerPoint = function (markers) {

                if (markers != null)
                {
                    for (var i = 0; i < markers.length; i++) {
                        var position = new google.maps.LatLng(markers[i]['lagtitude'], markers[i]['longitude']);
                        //bounds.extend(position);
                        var marker = new google.maps.Marker({
                            position: position,
                            map: mapLoaderService.map,
                            title: markers[i]['name']
                        });
                        mapLoaderService.markerList.push(marker);
                    }
                }

            }
            mapLoaderService.clearMarker = function () {

                if (mapLoaderService.markerList == null)
                    return;
                for (var i = 0; i < mapLoaderService.markerList.length; i++) {

                    mapLoaderService.markerList[i].setMap(null);
                }
            }

            mapLoaderService.createDragableMarkerPoint = function (markers) {

                mapLoaderService.clearMarker();
                var latValue = 51.1464659;
                var lngValue = 0.875019;
                if (markers != null)
                {
                    latValue = markers['lagtitude'];
                    lngValue = markers['longitude'];
                }
                var position = new google.maps.LatLng(latValue, lngValue);
                //bounds.extend(position);
                var marker = new google.maps.Marker({
                    position: position,
                    draggable: true,
                    map: mapLoaderService.map

                });
                mapLoaderService.map.panTo(marker.getPosition());
                google.maps.event.addListener(marker, 'dragend', function (event) {
                    //$('#position').val('* '+this.getPosition().lat()+','+this.getPosition().lng());
                    //saveData(map,event);
                });
                mapLoaderService.markerList.push(marker);
            }


            return mapLoaderService;
        })
        .config(function () {
            var styleEl = document.createElement('style'),
                    styleSheet,
                    rippleCSS,
                    rippleLightCSS,
                    rippleKeyframes,
                    rippleWebkitKeyframes;
            rippleCSS = [
                '-webkit-animation: ripple 800ms ease-out;',
                'animation: ripple 800ms ease-out;',
                'background-color: rgba(0, 0, 0, 0.16);',
                'border-radius: 100%;',
                'height: 10px;',
                'pointer-events: none;',
                'position: absolute;',
                'transform: scale(0);',
                'width: 10px;'
            ];
            rippleLightCSS = 'background-color: rgba(255, 255, 255, 0.32);';
            rippleKeyframes = [
                '@keyframes ripple {',
                'to {',
                'transform: scale(2);',
                'opacity: 0;',
                '}',
                '}'
            ];
            rippleWebkitKeyframes = [
                '@-webkit-keyframes ripple {',
                'to {',
                '-webkit-transform: scale(2);',
                'opacity: 0;',
                '}',
                '}'
            ];
            document.head.appendChild(styleEl);
            styleSheet = styleEl.sheet;
            styleSheet.insertRule('.ripple-effect {' + rippleCSS.join('') + '}', 0);
            styleSheet.insertRule('.ripple-light .ripple-effect {' + rippleLightCSS + '}', 0);
            if (CSSRule.WEBKIT_KEYFRAMES_RULE) { // WebKit                 styleSheet.insertRule(rippleWebkitKeyframes.join(''), 0);
            } else if (CSSRule.KEYFRAMES_RULE) { // W3C
                styleSheet.insertRule(rippleKeyframes.join(''), 0);
            }
        })
        .directive('ripple', function () {
            return {
                restrict: 'C',
                link: function (scope, element, attrs) {
                    element[0].style.position = 'relative';
                    element[0].style.overflow = 'hidden';
                    element[0].style.userSelect = 'none';
                    element[0].style.msUserSelect = 'none';
                    element[0].style.mozUserSelect = 'none';
                    element[0].style.webkitUserSelect = 'none';
                    function createRipple(evt) {
                        var ripple = angular.element('<span class="ripple-effect animate">'),
                                rect = element[0].getBoundingClientRect(),
                                radius = Math.max(rect.height, rect.width),
                                left = evt.pageX - rect.left - radius / 2 - document.body.scrollLeft,
                                top = evt.pageY - rect.top - radius / 2 - document.body.scrollTop;
                        ripple[0].style.width = ripple[0].style.height = radius + 'px';
                        ripple[0].style.left = left + 'px';
                        ripple[0].style.top = top + 'px';
                        ripple.on('animationend webkitAnimationEnd', function () {
                            this.remove();
                        });
                        element.append(ripple);
                    }

                    element.on('click', createRipple);
                }
            };
        })
        .directive('form', function () {
            return function (scope, elem, attrs) {
                elem.attr('autocomplete', 'off');
            };
        })
        .directive('elemReady', function ($parse) {
            return {
                restrict: 'A',
                link: function ($scope, elem, attrs) {
                    enableSelectBoxes();
                }
            }
        })
        .directive('elemfocusSelect', function ($parse) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    element.on('click', function () {
                        this.select();
                    });
                }
            };
        })
        .directive('staticInclude', function () {
            return {
                restrict: 'A',
                templateUrl: function (ele, attrs) {
                    return attrs.staticInclude;
                }
            };
        })
        .directive('googleMap', ['$googleMapService', '$timeout', function ($googleMapService, $timeout) {
                return {
                    restrict: 'A',
                    scope: {
                        option: '='
                    },
                    link: function (scope, element, attrs) {
                        $timeout(function () {
                            $googleMapService.initMap(attrs.id, null, null, scope.option);
                        }, 300);
                    }
                };
            }])
        //        .directive('elemFormField', function() {
        //            return {
        //                restrict: 'A',
        //                // just postLink
        //                link: function(scope, element, attrs, ngModelCtrl) {
        //                    // do nothing in case of no 'name' attribiute
        //                    if (!attrs.name) {
        //                        return;
        //                    }
        //                    // fix what should be fixed
        //                    ngModelCtrl.$name = attrs.name;
        //                },
        //                // ngModel's priority is 0
        //                priority: '-100',
        //                // we need it to fix it's behavior
        //                require: 'ngModel'
        //            };
        //        })
        //
        //        $("#myInputField").focus(function(){
        //            // Select input field contents
        //            this.select(); //        });
        .filter('trustedhtml', function ($sce) {
            return $sce.trustAsHtml;
        })
        .filter('offset', function () {
            return function (input, start) {
                start = parseInt(start, 10);
                if (typeof input != 'undefined' && typeof input.slice != 'undefined')
                    return input.slice(start);
                else
                    return input;
            };
        })
        .directive('hcPieChart', function () {
            return {
                restrict: 'E',
                template: '<div></div>',
                scope: {
                    title: '@',
                    data: '='
                },
                link: function (scope, element) {
                    Highcharts.chart(element[0], {
                        chart: {
                            type: 'pie'
                        },
                        title: {
                            text: scope.title
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                showInLegend: true,
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                }
                            }
                        },
                        series: [{
                                data: scope.data
                            }]
                    });
                }
            };
        })
        .filter('propsFilter', function () {
            return function (items, props) {
                var out = [];

                if (angular.isArray(items)) {
                    var keys = Object.keys(props);

                    items.forEach(function (item) {
                        var itemMatches = false;

                        for (var i = 0; i < keys.length; i++) {
                            var prop = keys[i];
                            var text = props[prop].toLowerCase();
                            if (item[prop] != undefined && item[prop].toLowerCase().indexOf(text) !== -1) {
                                itemMatches = true;
                                break;
                            }
                        }

                        if (itemMatches) {
                            out.push(item);
                        }
                    });
                } else {
                    // Let the output be the input untouched
                    out = items;
                }

                return out;
            };
        });




