app.service("adminService", function ($httpService, Auth, APP_CONST) {

    return  {
        handleOnlyErrorResponseConfig: {
            "has_success_alert": false,
            "has_error_alert": true
        },
        handleOnlySuccessResponseConfig: {
            "has_success_alert": true,
            "has_error_alert": false
        },
        handleBothSuccessAndErrorResponseConfig: {
            "has_success_alert": true,
            "has_error_alert": true
        },
        ignoreBothSuccessAndErrorResponseConfig: {
            "has_success_alert": false,
            "has_error_alert": false
        },
        appConfig: {
            'date_format': '',
            'invoice_prefix': ''
        },
        createUser: function (data, headers) {
            return $httpService.post(APP_CONST.API.USER_ADD, data, true, this.handleBothSuccessAndErrorResponseConfig, headers);
        },
        deleteUser: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.USER_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        createPayment: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.PAYMENT_SAVE, data, true, config, headers);
        },
        createPurchasePayment: function (data, headers) {
            return $httpService.post(APP_CONST.API.PURCHASE_PAYMENT_SAVE, data, true, this.handleBothSuccessAndErrorResponseConfig, headers);
        },
        getPaymentList: function (data, config) {
            return $httpService.get(APP_CONST.API.PAYMENT_LIST, data, true, config);
        },
        getPurchasePaymentList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_PAYMENT_LIST, data, true, config, headers);
        },
        editPayment: function (data, id) {
            return $httpService.put(APP_CONST.API.PAYMENT_UPDATE.replace('{{id}}', id), data, true);
        },
        editPurchasePayment: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PURCHASE_PAYMENT_UPDATE.replace('{{id}}', id), data, true, headers);
        },
        deletePayment: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.PAYMENT_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        deletePurchasePayment: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_PAYMENT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        createCategory: function (data, headers) {
            return $httpService.post(APP_CONST.API.CATEGORY_SAVE, data, true, null, headers);
        },
        modifyUser: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.USER_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        updateCategory: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CATEGORY_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        updatePayment: function (data, id) {
            return $httpService.put(APP_CONST.API.PAYMENT_EDIT.replace('{{id}}', id), data, true);
        },
        getUserList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_USER_LIST, data, true, config, headers);
        },
        getCategoryList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_LIST, data, true, config, null, headers);
        },
        changePassword: function (data) {
            return $httpService.post(APP_CONST.API.CHANGE_PASSWORD, data, true);
        },
        addProduct: function (data, headers) {
            return $httpService.post(APP_CONST.API.PRODUCT_ADD, data, true, null, headers);
        },
        getProductList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PRODUCT_LIST, data, true, config, headers);
        },
        editProduct: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PRODUCT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteProduct: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PRODUCT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addCompany: function (data, headers) {
            return $httpService.post(APP_CONST.API.COMPANY_ADD, data, true, null, headers);
        },
        editCompany: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.COMPANY_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getCompanyList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.COMPANY_LIST, data, true, config, headers);
        },
        deleteCompany: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.COMPANY_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addDeal: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEAL_ADD, data, true, null, headers);
        },
        getDealList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DEAL_LIST, data, true, config, headers);
        },
        editDeal: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DEAL_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteDeal: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DEAL_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getDealActivityList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DEAL_ACTIVITY_LIST, data, true, config, headers);
        },
        addDealActivity: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEAL_ACTIVITY_ADD, data, true, null, headers);
        },
        addStage: function (data, headers) {
            return $httpService.post(APP_CONST.API.STAGE_ADD, data, true, null, headers);
        },
        getStageList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.STAGE_LIST, data, true, config, headers);
        },
        editStage: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.STAGE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteStage: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.STAGE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addService: function (data, headers) {
            return $httpService.post(APP_CONST.API.SERVICE_ADD, data, true, null, headers);
        },
        getServiceList: function (data, headers) {
            return $httpService.get(APP_CONST.API.SERVICE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editService: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.SERVICE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteService: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.SERVICE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getUom: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.UOM, data, true, config, null, headers);
        },
        createUomList: function (data, headers) {
            return $httpService.post(APP_CONST.API.UOM_ADD, data, true, null, headers);
        },
        postUpdateUom: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.UOM_MODIFY.replace('{{id}}', id), data, true, null, headers);
        },
        deleteUOM: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.UOM_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addCustomer: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.CUSTOMERS_ADD, data, true, config, headers);
        },
        getCustomersList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CUSTOMERS_LIST, data, true, config, headers);
        },
        editCustomerInfo: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CUSTOMERS_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteCustomer: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.CUSTOMERS_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getCustomerDetail: function (data) {
            return $httpService.get(APP_CONST.API.CUSTOMERS_DETAIL, data, true, this.ignoreBothSuccessAndErrorResponseConfig);
        },
        addCustomerDualInfo: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.CUSTOMERS_DUALINFO_ADD, data, true, config, headers);
        },
        getCustomersDualInfoList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CUSTOMERS_DUALINFO_LIST, data, true, config, headers);
        },
        editCustomerDualInfo: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CUSTOMERS_DUALINFO_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getCustomerDualInfoDetail: function (data) {
            return $httpService.get(APP_CONST.API.CUSTOMERS_DUALINFO_DETAIL, data, true, this.ignoreBothSuccessAndErrorResponseConfig);
        },
        addAttribute: function (data, headers) {
            return $httpService.post(APP_CONST.API.ATTRIBUTE_ADD, data, true, null, headers);
        },
        getAttributeList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ATTRIBUTE_LIST, data, true, config, headers);
        },
        editAttribute: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ATTRIBUTE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getAttributeTypeList: function (data, config) {
            return $httpService.get(APP_CONST.API.ATTRIBUTE_TYPE_ID_LIST, data, true, config);
        },
        deleteAttribute: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ATTRIBUTE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getSalesquoteList: function (data) {
            return $httpService.get(APP_CONST.API.QUOTE_LIST, data, false);
        },
        addInvoice: function (data, headers) {
            return $httpService.post(APP_CONST.API.INVOICE_SAVE, data, true, null, headers);
        },
        getInvoiceList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.INVOICE_LIST, data, true, config, headers);
        },
        editInvoice: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.INVOICE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteInvoice: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.INVOICE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getInvoiceDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.INVOICE_DETAILS, data, true, config, headers);
        },
        addQuote: function (data, headers) {
            return $httpService.post(APP_CONST.API.QUOTE_SAVE, data, true, null, headers);
        },
        getQuoteList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.QUOTE_LIST, data, true, config, headers);
        },
        editQuote: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.QUOTE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteQuote: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.QUOTE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getQuoteDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.QUOTE_DETAILS, data, true, config, headers);
        },
        addSalesOrder: function (data, headers) {
            return $httpService.post(APP_CONST.API.SALESORDER_SAVE, data, true, null, headers);
        },
        getSalesOrderList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.SALESORDER_LIST, data, true, config, headers);
        },
        getEmployeeSalaryList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EMPLOYEE_SALARY_LIST, data, true, config, headers);
        },
        editSalesOrder: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.SALESORDER_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteSalesOrder: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.SALESORDER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getSalesOrderDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.SALESORDER_DETAILS, data, true, config, headers);
        },
        getTaxList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.TAX_LIST, data, true, config, headers);
        },
        getTaxReport: function (data, header) {
            return $httpService.get(APP_CONST.API.GET_TAX_LIST, data, true, this.handleOnlyErrorResponseConfig, header);
        },
        editTax: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TAX_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteTax: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.TAX_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addTax: function (data, headers) {
            return $httpService.post(APP_CONST.API.TAX_SAVE, data, true, null, headers);
        },
        editAppSettings: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.APP_SETTINGS_SAVE, data, true, config, headers);
        },
        getAppSettingsList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.APP_SETTINGS_LIST, data, true, config, headers);
        },
        addPurchaseInvoice: function (data, headers) {
            return $httpService.post(APP_CONST.API.PURCHASE_INVOICE_SAVE, data, true, null, headers);
        },
        getPurchaseInvoiceList: function (data, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_INVOICE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editPurchaseInvoice: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PURCHASE_INVOICE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deletePurchaseInvoice: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_INVOICE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getPurchaseInvoiceDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_INVOICE_DETAILS, data, true, config, headers);
        },
        getPurchaseInvoiceNumber: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_INVOICE_NUMBER, data, true, config, headers);
        },
        addPurchaseQuote: function (data, headers) {
            return $httpService.post(APP_CONST.API.PURCHASE_QUOTE_SAVE, data, true, null, headers);
        },
        getPurchaseQuoteList: function (data, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_QUOTE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editPurchaseQuote: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PURCHASE_QUOTE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deletePurchaseQuote: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_QUOTE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getPurchaseQuoteDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PURCHASE_QUOTE_DETAILS, data, true, config, headers);
        },
        getUserScreenAccessList: function (data, headers) {
            return $httpService.get(APP_CONST.API.SCREEN_ACCESS_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getUserAccessDetail: function (data, headers)
        {
            return $httpService.get(APP_CONST.API.USER_ACCESS_INFO, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editUserScreenAccess: function (data, headers) {
            return $httpService.post(APP_CONST.API.USER_SCREEN_ACCESS_MODIFY, data, true, null, headers);
        },
        getUserRoleList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.USER_ROLE_LIST, data, true, config, headers);
        },
        deleteUserRoleInfo: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.USER_ROLE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addUserRole: function (data, headers) {
            return $httpService.post(APP_CONST.API.USER_ROLE_SAVE, data, true, null, headers);
        },
        editUserRole: function (data, headers) {
            return $httpService.post(APP_CONST.API.USER_ROLE_UPDATE, data, true, null, headers);
        },
        getIncomeCategoryList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_INCOME_CATEGORY_LIST, data, true, config, headers);
        },
        addIncomeCategory: function (data, headers) {
            return $httpService.post(APP_CONST.API.INCOME_CATEGORY_SAVE, data, true, null, headers);
        },
        addAllowanceCategory: function (data, headers) {
            return $httpService.post(APP_CONST.API.ALLOWANCE_MASTER_SAVE, data, true, null, headers);
        },
        editIncomeCategory: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.INCOME_CATEGORY_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        getExpenseList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_EXPENSE_LIST, data, true, null, headers);
        },
        addExpense: function (data, headers) {
            return $httpService.post(APP_CONST.API.EXPENSE_SAVE, data, true, null, headers);
        },
        editExpense: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.EXPENSE_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        getInvoicePaymentList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_PAYMENT_LIST.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoicePurchasePaymentList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_PURCHASE_PAYMENT_LIST.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoiceExpensePaymentList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_EXPENSE_PAYMENT_LIST.replace('{{noofdays}}', noofdays), data, false);
        },
        getCategoryIncomeSummaryList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_INCOME_SUMMARY.replace('{{noofdays}}', noofdays), data, false);
        },
        getCategoryExpenseSummaryList: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_EXPENSE_SUMMARY.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoicePaymentJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_PAYMENT_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoicePurchasePaymentJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_PURCHASE_PAYMENT_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        getInvoiceExpensePaymentJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_EXPENSE_PAYMENT_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        getCategoryIncomeSummaryJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_INCOME_SUMMARY_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        getCategoryExpenseSummaryJson: function (data, noofdays) {
            return $httpService.get(APP_CONST.API.GET_CATEGORY_EXPENSE_SUMMARY_JSON.replace('{{noofdays}}', noofdays), data, false);
        },
        deleteCategory: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.CATEGORY_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteIncomeCategory: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.INCOME_CATEGORY_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getAllowanceCategoryList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ALLOWANCE_MASTER_LIST, data, true, config, headers);
        },
        deleteAllowanceCategory: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ALLOWANCE_MASTER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        editAllowanceCategory: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ALLOWANCE_MASTER_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        modifyUserRole: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.MODIFY_USER_ROLE.replace('{{id}}', id), data, true, this.handleBothSuccessAndErrorResponseConfig, headers);
        },
        gettransactionlist: function (data) {
            return $httpService.get(APP_CONST.API.GET_TRANSACTION_LIST, data, false);
        },
        getPartySalesList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_PARTY_SALES_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getPartyPurchaseList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_PARTY_PURCHASE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getItemizedSalesList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_ITEMIZED_SALES_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getProductionReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_PRODUCTION_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getItemizedPurchasedList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_ITEMIZED_PURCHASED_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getActivityList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_ACTIVITY_LIST, data, true, config, headers);
        },
        getAccountlist: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_ACCOUNT_LIST, data, true, config, headers);
        },
        createAccount: function (data, headers) {
            return $httpService.post(APP_CONST.API.ACCOUNT_ADD, data, true, null, headers);
        },
        deleteAccount: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ACCOUNT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        modifyAccount: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ACCOUNT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getSmsSettingsList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_SMS_SETTINGS_LIST, data, true, config, headers);
        },
        editSmsSettings: function (data, headers) {
            return $httpService.put(APP_CONST.API.EDIT_SMS_SETTINGS, data, true, null, headers);
        },
        getEmailSettingsList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_EMAIL_SETTINGS_LIST, data, true, config, headers);
        },
        editEmailSettings: function (data, headers) {
            return $httpService.put(APP_CONST.API.EDIT_EMAIL_SETTINGS, data, true, null, headers);
        },
        gettaxsummarylist: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_TAXSUMMARY_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getDepositList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_DEPOSIT_LIST, data, true, config, headers);
        },
        createDeposit: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEPOSIT_ADD, data, true, null, headers);
        },
        modifyDeposit: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DEPOSIT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteDeposit: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DEPOSIT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteVoucher: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.VOUCHER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getAcodeStatementList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_ACODE_STATEMENT_LIST, data, true, config, headers);
        },
        getAccountStatementList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_ACCOUNTSTATEMENT_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getBalanceAdvanceReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.EMP_BALANCE_ADVANCE, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getTransexpenseList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_TRANSEXPENSE_LIST, data, true, config, headers);
        },
        createTransexpense: function (data, headers) {
            return $httpService.post(APP_CONST.API.TRANSEXPENSE_ADD, data, true, null, headers);
        },
        modifYTransexpense: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TRANSEXPENSE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteTransexpense: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.TRANSEXPENSE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteTransexpensePayment: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PURCHASE_PAYMENT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getInvoiceSummaryList: function (data) {
            return $httpService.get(APP_CONST.API.GET_INVOICE_SUMMARY_LIST, data, false);
        },
        getInventoryStockListInfo: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_INVENTORY_STOCK_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getstocktracelist: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_STOCKTRACE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getStockadjustmentList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_STOCKADJUSTMENT_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        addStockadjustment: function (data, headers) {
            return $httpService.post(APP_CONST.API.STOCKADJUSTMENT_ADD, data, true, null, headers);
        },
        getStockwastageList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_STOCKWASTAGE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        addStockwastage: function (data, headers) {
            return $httpService.post(APP_CONST.API.STOCKWASTAGE_ADD, data, true, null, headers);
        },
        getMinstockList: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_MINSTOCK_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        receivableList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.RECEIVABLE_LIST, data, true, config, headers);
        },
        payableList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PAYABLE_LIST, data, true, config, headers);
        },
        getSalesPurchaseMonthlyCount: function (data, monthcode) {
            return $httpService.get(APP_CONST.API.GET_SALES_PURCHASE_MONTHLY_SUMMARY.replace('{{monthcode}}', monthcode), data, false, null, null);
        },
        getMonthwiseSalesList: function (data) {
            return $httpService.get(APP_CONST.API.MONTHWISE_SALES_REPORT, data, false);
        },
        gettransferlist: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_TRANSFER_LIST, data, true, config, headers);
        },
        createtransferlist: function (data, headers) {
            return $httpService.post(APP_CONST.API.TRANSFERLIST_ADD, data, true, null, headers);
        },
        modifytransferlist: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TRANSFERLIST_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deletetransferlist: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.TRANSFERLIST_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getpaymenttermslist: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_PAYMENTTERMS_LIST, data, true, config, headers);
        },
        addpaymentterms: function (data, headers) {
            return $httpService.post(APP_CONST.API.PAYMENTTERMS_ADD, data, true, null, headers);
        },
        modifypaymentterms: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PAYMENTTERMS_EDIT.replace('{{id}}', id), data, true, this.handleBothSuccessAndErrorResponseConfig, headers);
        },
        deletepaymentterms: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PAYMENTTERMS_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getexpensereportList: function (data, header) {
            return $httpService.get(APP_CONST.API.GET_EXPENSEREPORT_LIST, data, true, this.handleOnlyErrorResponseConfig, header);
        },
        getMonthwiseSalesCount: function (data, monthcode) {
            return $httpService.get(APP_CONST.API.GET_MONTHWISE_SALES_COUNT.replace('{{monthcode}}', monthcode), data, false, null, null);
        },
        getTaxGroupDetail: function (data, headers) {
            return $httpService.get(APP_CONST.API.GET_TAXGROUP_DETAIL, data, false, null, headers);
        },
        calculateTaxAmt: function (data) {
            return $httpService.get(APP_CONST.API.GET_CALCULATE_TAX, data, false);
        },
        gettodayBalancelist: function (data) {
            return $httpService.get(APP_CONST.API.GET_TODAYBALANCE_LIST, data, false);
        },
        getalbumlist: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_LIST, data, true, config, headers);
        },
        createAlbum: function (data, headers) {
            return $httpService.post(APP_CONST.API.ALBUM_ADD, data, true, null, headers);
        },
        modifyAlbum: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ALBUM_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        viewAlbum: function (data) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_VIEW, data, false);
        },
        deletealbumlist: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ALBUM_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getimagelist: function (data) {
            return $httpService.get(APP_CONST.API.GET_IMAGE_LIST, data, true, null);
        },
        updateImageSortby: function (data, configData) {
            return $httpService.put(APP_CONST.API.UPDATE_IMAGE_SORTBY, data, true, configData);
        },
        getcommentlist: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_COMMENT_LIST, data, false, config);
        },
        addcommentlist: function (data) {
            return $httpService.post(APP_CONST.API.COMMENTLIST_ADD, data, true, null);
        },
        getSharelist: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_SHARE_LIST, data, false, config);
        },
        addSharelist: function (data) {
            return $httpService.post(APP_CONST.API.SHARELIST_ADD, data, false, null);
        },
        saveAsset: function (data) {
            return $httpService.post(APP_CONST.API.ASSET_ADD, data, true, null);
        },
        saveComment: function (data) {
            return $httpService.post(APP_CONST.API.SAVE_COMMENT, data, true, null);
        },
        deleteshare: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.SHARELIST_DELETE.replace('{{id}}', id), data, false, null, headers);
        },
        createUserInvite: function (data) {
            return $httpService.post(APP_CONST.API.SAVE_USERINVITE, data, true, null);
        },
        createSendMail: function (data) {
            return $httpService.post(APP_CONST.API.CREATE_SENDMAIL, data, true, null);
        },
        getAlbumAssetList: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_ALBUM_ASSET_LIST, data, false, config);
        },
        getCountryList: function (data, config) {
            return $httpService.get(APP_CONST.API.COUNTRY_LIST, data, false, config);
        },
        saveCountry: function (data, headers) {
            return $httpService.post(APP_CONST.API.COUNTRY_ADD, data, true, null, headers);
        },
        editCountry: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.COUNTRY_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteCountry: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.COUNTRY_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        getStateList: function (data, config) {
            return $httpService.get(APP_CONST.API.STATE_LIST, data, false, config);
        },
        saveState: function (data, headers) {
            return $httpService.post(APP_CONST.API.STATE_ADD, data, true, null, headers);
        },
        editState: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.STATE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteState: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.STATE_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        getCityList: function (data, config) {
            return $httpService.get(APP_CONST.API.CITY_LIST, data, false, config);
        },
        saveCity: function (data, headers) {
            return $httpService.post(APP_CONST.API.CITY_ADD, data, true, null, headers);
        },
        editCity: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CITY_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteCity: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.CITY_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        getContactList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CONTACT_LIST, data, false, config, headers);
        },
        saveContact: function (data, headers) {
            return $httpService.post(APP_CONST.API.CONTACT_ADD, data, true, null, headers);
        },
        editContact: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CONTACT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteContact: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.CONTACT_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        addAdvancepayment: function (data, headers) {
            return $httpService.post(APP_CONST.API.ADVANCEPAYMENT_ADD, data, true, null, headers);
        },
        getAdvancepaymentList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ADVANCEPAYMENT_LIST, data, true, config, headers);
        },
        editAdvancepayment: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ADVANCEPAYMENT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteAdvancepayment: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ADVANCEPAYMENT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        createTransport: function (data, headers) {
            return $httpService.post(APP_CONST.API.TRANSPORT_ADD, data, true, null, headers);
        },
        deleteTransport: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.TRANSPORT_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        modifyTransport: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TRANSPORT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getTransportList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.TRANSPORT_LIST, data, true, config, headers);
        },
        createDeliveryInstruction: function (data, headers) {
            return $httpService.post(APP_CONST.API.DELIVERYINSTRUCTION_ADD, data, true, null, headers);
        },
        deleteDeliveryInstruction: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.DELIVERYINSTRUCTION_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        modifyDeliveryInstruction: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DELIVERYINSTRUCTION_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getDeliveryInstructionList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DELIVERYINSTRUCTION_LIST, data, true, config, headers);
        },
        getDeliveryInstructionDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DELIVERYINSTRUCTION_DETAIL, data, true, config, headers);
        },
        addDeliveryInstructionInvoiceDetail: function (data, headers) {
            return $httpService.post(APP_CONST.API.DELIVERYINSTRUCTION_DETAIL_ADD, data, true, null, headers);
        },
        getDeliveryInstructionDetailInvoiceList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DELIVERYINSTRUCTION_DETAIL_LIST, data, true, config, headers);
        },
        getUnMappedDIInvoiceList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.UNMAPPED_DI_INVOICE, data, true, config, headers);
        },
        deleteDeliveryInstructionInvoiceDetail: function (data, headers) {
            return $httpService.post(APP_CONST.API.DELIVERYINSTRUCTION_INVOICEDETAIL_DELETE, data, true, null, headers);
        },
        getNextInvoiceNo: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.NEXT_INVOICE_NO, data, false, config, headers);
        },
        getAreaWiseSalesList: function (data, headers) {
            return $httpService.get(APP_CONST.API.AREAWISE_SALES_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getEmployeeTeamList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EMP_TEAM_LIST, data, true, config, headers);
        },
        createEmployeeTeam: function (data, headers) {
            return $httpService.post(APP_CONST.API.EMP_TEAM_ADD, data, true, null, headers);
        },
        modifyEmployeeTeam: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.EMP_TEAM_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteEmployeeTeam: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.EMP_TEAM_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getEmployeeTaskTypeList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EMP_TASK_TYPE_LIST, data, true, config, headers);
        },
        createEmployeeTaskTypeList: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.EMP_TASK_TYPE_ADD, data, true, config, headers);
        },
        deleteEmployeeTaskType: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.EMP_TASK_TYPE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        modifyEmployeeTaskType: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.EMP_TASK_TYPE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getEmployeeList: function (data, config) {
            return $httpService.get(APP_CONST.API.EMPLOYEE_LIST, data, false, config);
        },
        saveEmployee: function (data, headers) {
            return $httpService.post(APP_CONST.API.EMPLOYEE_ADD, data, true, null, headers);
        },
        editEmployee: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.EMPLOYEE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteEmployee: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.EMPLOYEE_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        addProductionTask: function (data, headers) {
            return $httpService.post(APP_CONST.API.PRODUCTION_TASK_ADD, data, true, null, headers);
        },
        getProductionTaskList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PRODUCTION_TASK_LIST, data, true, config, headers);
        },
        editProductionTask: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PRODUCTION_TASK_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteProductionTask: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.PRODUCTION_TASK_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getProdTaskDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PRODUCTION_TASK_DETAIL, data, true, config, headers);
        },
        getSalesAgeingDetail: function (data, config) {
            return $httpService.get(APP_CONST.API.GET_SALES_AGEING_DETAIL, data, true, config);
        },
        addProduction: function (data, headers) {
            return $httpService.post(APP_CONST.API.PRODUCTION_ADD, data, true, null, headers);
        },
        getProductionList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PRODUCTION_LIST, data, true, config, headers);
        },
        editProduction: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.PRODUCTION_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        getProductionDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PRODUCTION_DETAIL, data, true, config, headers);
        },
        getCustomerAnniversaryReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CUSTOMER_ANNIVERSARY_REPORT, data, false, config, headers);
        },
        sendSms: function (data, type, headers) {
            return $httpService.post(APP_CONST.API.SEND_SMS.replace('{{type}}', type), data, true, null, headers);
        },
        addAdvance: function (data, headers) {
            return $httpService.post(APP_CONST.API.ADVANCE_ADD, data, true, null, headers);
        },
        getAdvanceList: function (data, headers) {
            return $httpService.get(APP_CONST.API.ADVANCE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editAdvance: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ADVANCE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteAdvance: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ADVANCE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        addDeduction: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEDUCTION_ADD, data, true, null, headers);
        },
        getDeductionList: function (data, headers) {
            return $httpService.get(APP_CONST.API.DEDUCTION_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editDeduction: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DEDUCTION_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteDeduction: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DEDUCTION_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getEmployeeBonusList: function (data, config) {
            return $httpService.get(APP_CONST.API.BONUS_ADD, data, false, config);
        },
        getBonusList: function (data, config) {
            return $httpService.get(APP_CONST.API.BONUS_LIST, data, false, config);
        },
        saveBonus: function (data, headers) {
            return $httpService.post(APP_CONST.API.BONUS_SAVE, data, true, null, headers);
        },
        editBonus: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.BONUS_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteBonus: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.BONUS_DELETE.replace('{{id}}', id), data, true, config, headers);
        },
        addEmployeeAllowance: function (data, headers) {
            return $httpService.post(APP_CONST.API.EMP_ALLOWANCE_ADD, data, true, null, headers);
        },
        getEmployeeAllowanceList: function (data, headers) {
            return $httpService.get(APP_CONST.API.EMP_ALLOWANCE_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getAttendanceList: function (data, headers) {
            return $httpService.get(APP_CONST.API.TOTAL_WORKING_TIME, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        editEmployeeAllowance: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.EMP_ALLOWANCE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteEmployeeAllowance: function (data, headers) {
            return $httpService.post(APP_CONST.API.EMP_ALLOWANCE_DELETE, data, true, null, headers);
        },
        saveSalary: function (data, account_id, headers) {
            var api_url = APP_CONST.API.SAVE_SALARY;
            api_url = api_url.replace('{{account_id}}', account_id);
            return $httpService.post(api_url, data, true, null, headers);
        },
        getEmployee: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EMP_SALARY, data, true, config, headers);
        },
        getEsiReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ESI_REPORT, data, true, config, headers);
        },
        getEmployeeSalaryDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EMP_SALARY_DETAIL, data, true, config, headers);
        },
        getDuplicateDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.NO_PRODUCTION, data, true, config, headers);
        },
        getAdvanceReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ADVANCE_REPORT, data, true, config, headers);
        },
        getWorkOrderDetail: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.WORK_ORDER_DETAIL, data, true, config, headers);
        },
        saveWorkOrder: function (data, headers) {
            return $httpService.post(APP_CONST.API.WORK_ORDER_SAVE, data, true, null, headers);
        },
        editWorkOrder: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.WORK_ORDER_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteWorkOrder: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.WORK_ORDER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getWorkOrderList: function (data, headers) {
            return $httpService.get(APP_CONST.API.WORK_ORDER_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        planWorkOrder: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.WORK_ORDER_PLAN.replace('{{id}}', id), data, true, null, headers);
        },
        produceWorkOrder: function (data, headers) {
            return $httpService.get(APP_CONST.API.WORK_ORDER_PRODUCE, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getWorkOrderInputList: function (data, headers) {
            return $httpService.get(APP_CONST.API.WORK_ORDER_INPUT_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getWorkOrderDetailList: function (data, headers) {
            return $httpService.get(APP_CONST.API.WORK_ORDER_DETAIL_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getCustomerInvoiceItemList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_CUSTOMER_INVOICE_ITEM_LIST, data, true, config, headers);
        },
        saveAmc: function (data, headers) {
            return $httpService.post(APP_CONST.API.AMC_SAVE, data, true, null, headers);
        },
        editAmc: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.AMC_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteAmc: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.AMC_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getAmcList: function (data, headers) {
            return $httpService.get(APP_CONST.API.AMC_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getMachineInfo: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_MACHINE_INFO, data, true, config, headers);
        },
        grnAdd: function (data, headers) {
            return $httpService.post(APP_CONST.API.GRN_SAVE, data, true, null, headers);
        },
        grnListAll: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GRN_List_ALL, data, true, config, headers);
        },
        deleteGRNInfo: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.GRN_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        editGRNInfo: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.GRN_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        getGRNReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GRN_REPORT, data, true, config, headers);
        },
        getEmployeePFList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PF_REPORT, data, true, config, headers);
        },
        getTaskMgtDetail: function (data, config, is_active, id, ref_id, ref_name, start, limit, headers) {
            var api_url = APP_CONST.API.TASK_MGT_DETAIL;
            api_url = api_url.replace('{{is_active}}', is_active);
            api_url = api_url.replace('{{id}}', id);
            api_url = api_url.replace('{{ref_id}}', ref_id);
            api_url = api_url.replace('{{ref_name}}', ref_name);
            api_url = api_url.replace('{{start}}', start);
            api_url = api_url.replace('{{limit}}', limit);
            return $httpService.post(api_url, data, true, config, headers);
        },
        getTaskMgtlist: function (data, configOption, is_active, low, medium, high, initiated, inprogress, completed, closed, task_name, date, estimated_date, start, limit, headers) {
            var api_url = APP_CONST.API.TASK_MGT_LIST;
            api_url = api_url.replace('{{is_active}}', is_active);
            api_url = api_url.replace('{{low}}', low);
            api_url = api_url.replace('{{medium}}', medium);
            api_url = api_url.replace('{{high}}', high);
            api_url = api_url.replace('{{initiated}}', initiated);
            api_url = api_url.replace('{{inprogress}}', inprogress);
            api_url = api_url.replace('{{completed}}', completed);
            api_url = api_url.replace('{{closed}}', closed);
            api_url = api_url.replace('{{task_name}}', task_name);
            api_url = api_url.replace('{{date}}', date);
            api_url = api_url.replace('{{estimated_date}}', estimated_date);
            api_url = api_url.replace('{{start}}', start);
            api_url = api_url.replace('{{limit}}', limit);
            return $httpService.post(api_url, data, true, configOption, headers);
        },
        saveTaskMgt: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.SAVE_TASK_MGT, data, true, config, headers);
        },
        editTaskMgt: function (data, id, config, headers) {
            return $httpService.put(APP_CONST.API.EDIT_TASK_MGT.replace('{{id}}', id), data, true, config, headers);
        },
        deleteTaskMgt: function (data, id, config, headers) {
            return $httpService.delete(APP_CONST.API.DELETE_TASK_MGT.replace('{{id}}', id), data, true, config, headers);
        },
        getTaskActivityCommentsList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.TASK_ACTIVITY_COMMENTS_LIST, data, false, config, headers);
        },
        saveTaskActivityComments: function (data, headers) {
            return $httpService.post(APP_CONST.API.SAVE_TASK_COMMENTS, data, true, null, headers);
        },
        getEmployeeTaskList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.TASK_LIST, data, true, config, headers);
        },
        createEmployeeTaskList: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.TASK_ADD, data, true, config, headers);
        },
        deleteEmployeeTask: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.TASK_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        modifyEmployeeTask: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TASK_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        addSource: function (data, headers) {
            return $httpService.post(APP_CONST.API.SOURCE_ADD, data, true, null, headers);
        },
        getSourceList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.SOURCE_LIST, data, true, config, headers);
        },
        editSource: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.SOURCE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteSource: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.SOURCE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getGeoCodeList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GEOCODE_SEARCH_URL, data, true, config, headers);
        },
        getServiceRequestList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.SERVICE_REQUEST_LISTALL, data, true, config, headers);
        },
        createServiceRequestList: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.SERVICE_REQUEST_SAVE, data, true, config, headers);
        },
        modifyServiceRequest: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.SERVICE_REQUEST_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteServiceRequest: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.SERVICE_REQUEST_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteActivityList: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DEAL_ACTIVITY_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        createWorkLog: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.WORKLOG_SAVE, data, true, config, headers);
        },
        getWorklogList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.WORKLOG_LIST, data, true, config, headers);
        },
        getLeadTypeList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.LEAD_TYPE_LIST, data, true, config, headers);
        },
        createLeadTypeList: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.LEAD_TYPE_SAVE, data, true, config, headers);
        },
        modifyLeadType: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.LEAD_TYPE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteLeadType: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.LEAD_TYPE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getAttendanceDetailsList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_ATTENDANCE_DEATILS_LIST, data, true, config, headers);
        },
        getCustomerInvoiceItemList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.GET_CUSTOMER_INVOICE_ITEM_LIST, data, true, config, headers);
        },
        getExpenseTrackerList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EXPENSE_TRACKER_LIST, data, true, config, headers);
        },
        createExpenseTrackerList: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.EXPENSE_TRACKER_ADD, data, true, config, headers);
        },
        modifyExpenseTracker: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.EXPENSE_TRACKER_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteExpenseTracker: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.EXPENSE_TRACKER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        statusSave: function (data, checkNo, status) {
            var api_url = APP_CONST.API.EXPENSE_TRACKER_STATUS_UPDATE;
//            api_url = api_url.replace('{{no}}', checkNo);
            api_url = api_url.replace('{{status}}', status);
            return $httpService.post(api_url, data, true);
        },
        getAllowanceMappingList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.ALLOWANCE_MAPPING, data, true, config, headers);
        },
        modifyAllowanceMapping: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.ALLOWANCE_MAPPING_UPDATE, data, true, config, headers);
        },
        getExpenseTrackerReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EXPENSE_TRACKER_REPORT, data, true, config, headers);
        },
        saveFollowers: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEAL_FOLLOWER_SAVE, data, true, null, headers);
        },
        getDealFollowerList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DEAL_FOLLOWER_LISTALL, data, true, config, headers);
        },
        deleteDealFollower: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DEAL_FOLLOWER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getUserCountList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.USERBASED_COUNT, data, true, config, headers);
        },
        getStageCountList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.STAGEWISE_COUNT, data, true, config, headers);
        },
        getCityCountList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DEAL_CITYLIST_COUNT, data, true, config, headers);
        },
        getDealTypeCountList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DEAL_TYPEBASED_COUNT, data, true, config, headers);
        },
        getProductBasedCountList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.PRODUCT_BASED_COUNT, data, true, config, headers);
        },
        getActivityUserReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.DAILY_ACTIVITY_USER_REPORT, data, true, config, headers);
        },
        getInactiveDealReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.INACTIVE_DEAL_REPORT, data, true, config, headers);
        },
        getExternalwiseProductReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EXTERNAL_WISE_PRODUCT_REPORT, data, true, config, headers);
        },
        getReceivedQtyReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.RECEIVED_QTY_REPORT, data, true, config, headers);
        },
        getExpectedQtyReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EXPECTED_QTY_REPORT, data, true, config, headers);
        },
        getLeadWithoutDealReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.LEAD_WITHOUT_DEAL, data, true, config, headers);
        },
        getExpenseTrackerUserBasedReport: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.EXPENSETRACKER_USERBASED, data, true, config, headers);
        },
        getDealCreatorsList: function (data, config, headers)
        {
            return $httpService.get(APP_CONST.API.DEAL_CREATORS_LIST, data, true, config, headers);
        },
        getContactDetails: function (data, id, config, headers)
        {
            return $httpService.get(APP_CONST.API.CONTACT_DETAIL.replace('{{id}}', id), data, true, config, headers);
        },
        getAcodeStatement: function (data, headers) {
            return $httpService.get(APP_CONST.API.ACODE_STATEMENT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        saveWorkCenter: function (data, headers) {
            return $httpService.post(APP_CONST.API.WORK_CENTER_ADD, data, true, null, headers);
        },
        modifyWorkCenter: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.WORK_CENTER_MODIFY.replace('{{id}}', id), data, true, null, headers);
        },
        deleteWorkCenter: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.WORK_CENTER_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getWorkCenterList: function (data, headers) {
            return $httpService.get(APP_CONST.API.WORK_CENTER_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getTaskGroupList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.TASKGROUP_LISTALL, data, true, config, headers);
        },
        createTaskGroupList: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.TASKGROUP_SAVE, data, true, config, headers);
        },
        modifyTaskGroup: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.TASKGROUP_UPDATE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteTaskGroup: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.TASKGROUP_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getTaskGroupDetail: function (data) {
            return $httpService.get(APP_CONST.API.TASKGROUP_DETAIL, data, true, this.ignoreBothSuccessAndErrorResponseConfig);
        },
        saveWorkOrderSession: function (data) {
            return $httpService.post(APP_CONST.API.WORK_ORDER_SESSION_SAVE, data, true, null);
        },
        getSessionListAll: function (data) {
            return $httpService.get(APP_CONST.API.WORK_ORDER_SESSION_LISTALL, data, true, this.ignoreBothSuccessAndErrorResponseConfig);
        },
        modifyWorkOrderStatus: function (data, id) {
            return $httpService.put(APP_CONST.API.WORK_ORDER_STATUS_UPDATE.replace('{{id}}', id), data, true, null);
        },
        markAsDoneWorkOrder: function (data, id) {
            return $httpService.put(APP_CONST.API.WORK_ORDER_MARK_AS_DONE.replace('{{id}}', id), data, true, null);
        },
        qualityCheck: function (data, header) {
            return $httpService.post(APP_CONST.API.QUALITY_CHECK, data, true, this.handleOnlyErrorResponseConfig, header);
        },
        getPendingSalesOrderReport: function (data, header) {
            return $httpService.get(APP_CONST.API.SALES_ORDER_PENDING, data, true, this.handleOnlyErrorResponseConfig, header);
        },
        saveDepartment: function (data, headers) {
            return $httpService.post(APP_CONST.API.DEPARTMENT_ADD, data, true, null, headers);
        },
        modifyDepartment: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.DEPARTMENT_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteDepartment: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DEPARTMENT_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getDepartmentList: function (data, headers) {
            return $httpService.get(APP_CONST.API.DEPARTMENT_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getWorkorderPendingList: function (data, headers) {
            return $httpService.get(APP_CONST.API.WORK_ORDER_PENDING_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        saveWorkorderCustomer: function (data, headers) {
            return $httpService.post(APP_CONST.API.WORK_ORDER_CUSTOMER_SAVE, data, true, null, headers);
        },
        getMaterialRequirementList: function (data, headers) {
            return $httpService.get(APP_CONST.API.MATERIAL_REQUIREMENT_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        modifyWorkOrderUpdate: function (data, id) {
            return $httpService.put(APP_CONST.API.WORK_ORDER_UPDATE.replace('{{id}}', id), data, true, null);
        },
        addProductType: function (data, headers) {
            return $httpService.post(APP_CONST.API.SAVE_PRODUCT_TYPE, data, true, null, headers);
        },
        getProductTypeList: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.LIST_PRODUCT_TYPE, data, true, config, headers);
        },
        editProductType: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.EDIT_PRODUCT_TYPE.replace('{{id}}', id), data, true, null, headers);
        },
        deleteProductType: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.DELETE_PRODUCT_TYPE.replace('{{id}}', id), data, true, null, headers);
        },
        saveIssueQty: function (data, headers) {
            return $httpService.post(APP_CONST.API.SAVE_ISSUE_QTY, data, true, null, headers);
        },
        getCustomerBasedReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.CUSTOMER_BASED_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        checkSerialNo: function (data, config, headers) {
            return $httpService.get(APP_CONST.API.CHECK_SERIAL_NO, data, true, config, headers);
        },
        getItemHistoryReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.ITEM_HISTORY_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getQcpList: function (data) {
            return $httpService.get(APP_CONST.API.QCP_LIST, data, true, this.ignoreBothSuccessAndErrorResponseConfig);
        },
        createQcp: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.QCP_SAVE, data, true, config, headers);
        },
        modifyQcp: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.QCP_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteQcp: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.QCP_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        saveConversionJournal: function (data, headers) {
            return $httpService.post(APP_CONST.API.CONVERSION_JOURNAL_SAVE, data, true, null, headers);
        },
        modifyConversionJournal: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.CONVERSION_JOURNAL_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteConversionJournal: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.CONVERSION_JOURNAL_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getConversionJournalList: function (data, headers) {
            return $httpService.get(APP_CONST.API.CONVERSION_JOURNAL_LIST, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getManufacturingBasedReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.MO_BASED_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getMOInternalReceivedReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.MO_INTERNAL_RECEIVED_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getMOIssuedReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.MO_ISSUED_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getItemIssuedReport: function (data, headers) {
            return $httpService.get(APP_CONST.API.ITEMWISE_ISSUED_REPORT, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getExpectedAndReceivedQtyList: function (data, headers) {
            return $httpService.get(APP_CONST.API.EXPECTED_RECEIVED_QTY, data, true, this.handleOnlyErrorResponseConfig, headers);
        },
        getIssueList: function (data, config) {
            return $httpService.get(APP_CONST.API.ISSUE_LIST, data, true, config);
        },
        createIssue: function (data, config, headers) {
            return $httpService.post(APP_CONST.API.ISSUE_SAVE, data, true, config, headers);
        },
        modifyIssue: function (data, id, headers) {
            return $httpService.put(APP_CONST.API.ISSUE_EDIT.replace('{{id}}', id), data, true, null, headers);
        },
        deleteIssue: function (data, id, headers) {
            return $httpService.delete(APP_CONST.API.ISSUE_DELETE.replace('{{id}}', id), data, true, null, headers);
        },
        getIssueDetail: function (data) {
            return $httpService.get(APP_CONST.API.ISSUE_DETAIL_LIST, data, true, this.ignoreBothSuccessAndErrorResponseConfig);
        }

    }

});


