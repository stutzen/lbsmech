
app.constant('APP_CONST', {
    USER_INFO_SYNC_API: "/public/auth/userInfo",
    LOGOUT_API_URL: "/public/auth/logout",
    //    SSO_LOGOUT_URL: "http://lbssso/public/logout",
    //    SSO_LOGIN_URL: "http://lbssso/public/login",
    //    SSO_ADDBUSINESS_URL: "http://lbssso/public/addbusiness",
    SSO_LOGOUT_URL: "http://sso.accozen.com/public/logout",
    SSO_LOGIN_URL: "http://sso.accozen.com/public/login",
    SSO_ADDBUSINESS_URL: "http://sso.accozen.com/public/addbusiness",
    APP_SETTINGS_LIST: "/public/appconfig/listAll",
    INVOICE_PRINT_URL: "/public/printHelper.php",
    EMP_SALARY_PRINT_URL: "/public/customReport/view",
    INVOICE_DOWNLOAD: "/public/invoice/pdfPreview",
    DELIVARY_PRINT: "/public/printHelper.php",
    PAY_SLIP_PRINT: "/public/printHelper.php",
    INVOICE_PAY_SLIP_PRINT: "/public/printHelper.php",
    ADVANCE_PRINT: "/public/printHelper.php",
    ORDER_PRINT: "/public/printHelper.php",
    PAYMENT_RECEIPT: "/public/printHelper.php",
    ESI_PRINT: "/public/printHelper.php",
    PF_PRINT: "/public/printHelper.php",
    ADVANCE_REPORT: "/public/printHelper.php",
    ADVANCE_PAYMENT_RECEIPT: "/public/printHelper.php",
    API: {
        GET_USER_LIST: '/public/user/listingAll',
        GET_CATEGORY_LIST: '/public/category/listAll',
        GET_USER_BUSINESS_LIST: '/public/user/businessList',
        USER_ADD: '/public/user/save',
        CATEGORY_SAVE: '/public/category/save',
        USER_EDIT: '/public/user/update/{{id}}',
        CATEGORY_EDIT: '/public/category/modify/{{id}}',
        USER_DELETE: "/public/user/delete/{{id}}",
        CHANGE_PASSWORD: '/public/user/changePwd',
        PRODUCT_ADD: "/public/product/save",
        PRODUCT_LIST: "/public/product/listAll",
        PRODUCT_EDIT: "/public/product/modify/{{id}}",
        PRODUCT_DELETE: "/public/product/delete/{{id}}",
        DEAL_DELETE: "/public/deal/delete/{{id}}",
        DEAL_ADD: "/public/deal/save",
        DEAL_LIST: "/public/deal/listAll",
        DEAL_EDIT: "/public/deal/update/{{id}}",
        COMPANY_LIST: "/public/company/listAll",
        COMPANY_ADD: "/public/company/save",
        COMPANY_EDIT: "/public/company/update/{{id}}",
        COMPANY_DELETE: "/public/company/delete/{{id}}",
        STAGE_DELETE: "/public/dealstage/delete/{{id}}",
        STAGE_ADD: "/public/dealstage/save",
        STAGE_LIST: "/public/dealstage/listAll",
        STAGE_EDIT: "/public/dealstage/update/{{id}}",
        SERVICE_ADD: "/public/product/save",
        SERVICE_LIST: "/public/product/listAll",
        SERVICE_EDIT: "/public/product/modify/{{id}}",
        SERVICE_DELETE: "/public/product/delete/{{id}}",
        UOM: "/public/uom/listAll",
        UOM_ADD: "/public/uom/save",
        UOM_MODIFY: "/public/uom/modify/{{id}}",
        UOM_DELETE: "/public/uom/delete/{{id}}",
        CUSTOMERS_ADD: "/public/customer/save",
        CUSTOMERS_LIST: "/public/customer/listAll",
        CUSTOMERS_EDIT: "/public/customer/modify/{{id}}",
        CUSTOMERS_DELETE: "/public/customer/delete/{{id}}",
        CUSTOMERS_DETAIL: "/public/customer/detail",
        CUSTOMERS_DUALINFO_ADD: "/public/customerdualinfo/save",
        CUSTOMERS_DUALINFO_LIST: "/public/customerdualinfo/listAll",
        CUSTOMERS_DUALINFO_EDIT: "/public/customerdualinfo/modify/{{id}}",
        CUSTOMERS_DUALINFO_DETAIL: "/public/customerdualinfo/detail",
        COUNTRY_LIST: "/public/country/listAll",
        COUNTRY_ADD: "/public/country/save",
        COUNTRY_EDIT: "/public/country/update/{{id}}",
        COUNTRY_DELETE: "/public/country/delete/{{id}}",
        STATE_LIST: "/public/state/listAll",
        STATE_ADD: "/public/state/save",
        STATE_EDIT: "/public/state/update/{{id}}",
        STATE_DELETE: "/public/state/delete/{{id}}",
        CITY_LIST: "/public/city/listAll",
        CITY_ADD: "/public/city/save",
        CITY_EDIT: "/public/city/update/{{id}}",
        CITY_DELETE: "/public/city/delete/{{id}}",
        ATTRIBUTE_ADD: "/public/attribute/save",
        ATTRIBUTE_LIST: "/public/attribute/listAll",
        ATTRIBUTE_EDIT: "/public/attribute/modify/{{id}}",
        ATTRIBUTE_DELETE: "/public/attribute/delete/{{id}}",
        ATTRIBUTE_TYPE_ID_LIST: "/public/attributestype/listAll",
        NEXT_INVOICE_NO: "/public/general/nextInvoiceNo",
        INVOICE_LIST: "/public/invoice/listAll",
        INVOICE_DETAILS: "/public/invoice/detail",
        INVOICE_SAVE: "/public/invoice/save",
        INVOICE_UPDATE: "/public/invoice/modify/{{id}}",
        INVOICE_DELETE: "/public/invoice/delete/{{id}}",
        QUOTE_LIST: "/public/quote/listAll",
        QUOTE_DETAILS: "/public/quote/detail",
        QUOTE_SAVE: "/public/quote/save",
        QUOTE_UPDATE: "/public/quote/modify/{{id}}",
        QUOTE_DELETE: "/public/quote/delete/{{id}}",
        SALESORDER_LIST: "/public/salesorder/listAll",
        SALESORDER_DETAILS: "/public/salesorder/detail",
        SALESORDER_SAVE: "/public/salesorder/save",
        SALESORDER_UPDATE: "/public/salesorder/modify/{{id}}",
        SALESORDER_DELETE: "/public/salesorder/delete/{{id}}",
        TAX_LIST: "/public/tax/listAll",
        TAX_SAVE: "/public/tax/save",
        TAX_UPDATE: "/public/tax/modify/{{id}}",
        TAX_DELETE: "/public/tax/delete/{{id}}",
        PAYMENT_LIST: "/public/payment/listAll",
        PAYMENT_SAVE: "/public/payment/save",
        PAYMENT_UPDATE: "/public/payment/modify/{{id}}",
        PAYMENT_DELETE: "/public/payment/delete/{{id}}",
        PURCHASE_PAYMENT_UPDATE: '/public/purchasepayment/modify/{{id}}',
        PURCHASE_PAYMENT_DELETE: "/public/purchasepayment/delete/{{id}}",
        APP_SETTINGS_SAVE: "/public/appconfig/modify",
        APP_SETTINGS_LIST: "/public/appconfig/listAll",
        PURCHASE_INVOICE_LIST: "/public/purchaseinvoice/listAll",
        PURCHASE_INVOICE_DETAILS: "/public/purchaseinvoice/detail",
        PURCHASE_INVOICE_SAVE: "/public/purchaseinvoice/save",
        PURCHASE_INVOICE_UPDATE: "/public/purchaseinvoice/modify/{{id}}",
        PURCHASE_INVOICE_DELETE: "/public/purchaseinvoice/delete/{{id}}",
        PURCHASE_INVOICE_NUMBER: "/public/purchaseinvoice/invoiceDetail",
        PURCHASE_QUOTE_LIST: "/public/purchasequote/listAll",
        PURCHASE_QUOTE_DETAILS: "/public/purchasequote/detail",
        PURCHASE_QUOTE_SAVE: "/public/purchasequote/save",
        PURCHASE_QUOTE_UPDATE: "/public/purchasequote/modify/{{id}}",
        PURCHASE_QUOTE_DELETE: "/public/purchasequote/delete/{{id}}",
        PURCHASE_PAYMENT_SAVE: "/public/purchasepayment/save",
        SCREEN_ACCESS_LIST: "/public/screen/listAll",
        USER_ROLE_LIST: "/public/role/listAll",
        USER_SCREEN_ACCESS_MODIFY: "/public/roleacess/modify",
        USER_ROLE_SAVE: "/public/role/save",
        USER_ACCESS_INFO: "/public/roleacess/listAll",
        //USER_ACCESS_INFO: "service/user.json",
        USER_ROLE_UPDATE: "/public/roleacess/modify",
        USER_ROLE_DELETE: "/public/role/delete/{{id}}",
        PURCHASE_PAYMENT_LIST: '/public/purchasepayment/listAll',
        UPLOAD_IMAGE: '/public/uploadfile',
        UPLOAD_LOGO: '/public/uploadlogo',
        // UPLOAD_BACKGROUND: '/public/uploadbackground',
        GET_INCOME_CATEGORY_LIST: '/public/transationcategory/listAll',
        INCOME_CATEGORY_SAVE: '/public/transationcategory/save',
        INCOME_CATEGORY_UPDATE: '/public/transationcategory/update/{{id}}',
        GET_EXPENSE_LIST: '/public/expense/listAll',
        EXPENSE_SAVE: '/public/expense/save',
        EXPENSE_UPDATE: '/public/expense/update/{{id}}',
        GET_INVOICE_PAYMENT_LIST: '/public/dashboard/InvoiceVsPayment/{{noofdays}}',
        GET_INVOICE_PURCHASE_PAYMENT_LIST: '/public/dashboard/InvoicePurchaseVsPayment/{{noofdays}}',
        GET_INVOICE_EXPENSE_PAYMENT_LIST: '/public/dashboard/IncomeVsExpenses/{{noofdays}}',
        GET_CATEGORY_INCOME_SUMMARY: '/public/dashboard/categoryVsIncomeSummery/{{noofdays}}',
        GET_CATEGORY_EXPENSE_SUMMARY: '/public/dashboard/categoryVsExpenseSummery/{{noofdays}}',
        GET_SALES_PURCHASE_MONTHLY_SUMMARY: '/public/dashboard/SalesPurchaseMonthlyReport/{{monthcode}}',
        GET_INVOICE_PAYMENT_JSON: 'service/invoicevspayment.json',
        GET_INVOICE_PURCHASE_PAYMENT_JSON: 'service/invoicepurchasevspayment.json',
        GET_INVOICE_EXPENSE_PAYMENT_JSON: 'service/incomevsexpense.json',
        GET_CATEGORY_INCOME_SUMMARY_JSON: 'service/categoryvsincomesummary.json',
        GET_CATEGORY_EXPENSE_SUMMARY_JSON: 'service/categoryvsexpensesummary.json',
        CATEGORY_DELETE: '/public/category/delete/{{id}}',
        INCOME_CATEGORY_DELETE: '/public/transationcategory/delete/{{id}}',
        MODIFY_USER_ROLE: '/public/role/modify/{{id}}',
        GET_TRANSACTION_LIST: '/public/transaction/listAll',
        GET_PARTY_SALES_LIST: '/public/reports/PartyWiseSalesReport',
        GET_PARTY_PURCHASE_LIST: '/public/reports/PartyWisePurchaseReport',
        GET_ITEMIZED_SALES_LIST: '/public/reports/ItemWiseReport',
        //        GET_ITEMIZED_SALES_LIST: "/public/admin/service/itemizedsalesreport.json",
        GET_ACTIVITY_LIST: '/public/activitylog/listAll',
        GET_ACCOUNT_LIST: "/public/accounts/listAll",
        ACCOUNT_ADD: '/public/accounts/save',
        ACCOUNT_EDIT: '/public/accounts/update/{{id}}',
        ACCOUNT_DELETE: "/public/accounts/delete/{{id}}",
        GET_SMS_SETTINGS_LIST: '/public/emailTemplates/listAll',
        EDIT_SMS_SETTINGS: '/public/emailTemplates/update',
        GET_EMAIL_SETTINGS_LIST: '/public/emailTemplates/listAll',
        EDIT_EMAIL_SETTINGS: '/public/emailTemplates/update',
        GET_TAX_LIST: '/public/invoice/salesTaxReport',
        GET_TAXSUMMARY_LIST: '/public/invoice/salesTaxReportSummery',
        GET_DEPOSIT_LIST: '/public/transaction/listByIncome',
        DEPOSIT_ADD: '/public/transaction/save',
        DEPOSIT_EDIT: "/public/transaction/modify/{{id}}",
        DEPOSIT_DELETE: "/public/transaction/transactionIncomeDelete/{{id}}",
        VOUCHER_DELETE: "/public/payment/delete/{{id}}",
        GET_TRANSEXPENSE_LIST: '/public/transaction/listBYExpense',
        TRANSEXPENSE_ADD: '/public/transaction/save',
        TRANSEXPENSE_EDIT: "/public/transaction/modify/{{id}}",
        TRANSEXPENSE_DELETE: "/public/transaction/transactionExpenseDelete/{{id}}",
        GET_ACODE_STATEMENT_LIST: '/public/transaction/acodeStatement',
        GET_ACCOUNTSTATEMENT_LIST: '/public/transaction/accountStatement',
        GET_INVOICE_SUMMARY_LIST: '/public/invoice/summary',
        GET_INVENTORY_STOCK_LIST: '/public/stock/listAll',
        GET_STOCKTRACE_LIST: "/public/stock/InventoryAdjustment_list",
        GET_STOCKADJUSTMENT_LIST: '/public/stock/InventoryAdjustment_list',
        STOCKADJUSTMENT_ADD: "/public/stock/StockAdjustment",
        GET_STOCKWASTAGE_LIST: '/public/stock/InventoryAdjustment_list',
        STOCKWASTAGE_ADD: "/public/stock/StockWastage",
        GET_MINSTOCK_LIST: '/public/stock/MinStocklist',
        CUSTOMERS_SEARCH_LIST: '/public/customer/search',
        CONTACT_SEARCH_LIST: '/public/contact/search',
        TRANSPORT_SEARCH_LIST: '/public/transport/search',
        GET_SALE_PURCHASE_COUNT: '/public/dashboard/SalesPurchaseMonthlyReport/{{monthcode}}',
        RECEIVABLE_LIST: '/public/invoice/receivable_list',
        PAYABLE_LIST: '/public/purchaseinvoice/payable_list',
        MONTHWISE_SALES_REPORT: '/public/dashboard/salesMonthReport',
        GET_TRANSFER_LIST: '/public/transaction/transferList',
        TRANSFERLIST_ADD: '/public/transaction/transferSave',
        TRANSFERLIST_EDIT: "/public/transaction/transferList/{{id}}",
        TRANSFERLIST_DELETE: "/public/transaction/transferDelete/{{id}}",
        GET_PAYMENTTERMS_LIST: '/public/paymentTerm/listAll',
        PAYMENTTERMS_ADD: '/public/paymentTerm/save',
        PAYMENTTERMS_EDIT: "/public/paymentTerm/modify/{{id}}",
        PAYMENTTERMS_DELETE: "/public/paymentTerm/delete/{{id}}",
        GET_EXPENSEREPORT_LIST: '/public/transaction/listBYExpense',
        GET_MONTHWISE_SALES_COUNT: '/public/dashboard/salesMonthTotalReport/{{monthcode}}',
        GET_TAXGROUP_DETAIL: '/public/tax/groupdetail',
        GET_CALCULATE_TAX: '/public/tax/calculate',
        GET_TODAYBALANCE_LIST: '/public/dashboard/todayIncomeVsExpenses',
        CUSTOMER_DOWN: "/public/customer/customerdetails_exportcsv",
        PRODUCT_DOWN: "/public/product/product_csvexport",
        SERVICE_DOWN: "/public/product/product_csvexport",
        INVOICE_DOWN: "/public/invoice/invoicelist_csvexport",
        INVOICE_SALES_DOWN: "/public/invoice/invoice_csvexport",
        ACCOUNTSTATEMENT_DOWN: "/public/transaction/accountstatement_csvexport",
        ACODE_STATEMENT_DOWN: "/public/transaction/acodestatement_csvexport",
        EXPENSEREPORT_DOWN: "/public/transaction/ExpenseReport_csvexport",
        ITEMIZEDREPORT_DOWN: "/public/reports/itemizedreport_csvexport",
        PARTYPURCHASE_DOWN: "/public/reports/PartyWisePurchaseReport_csvexport",
        PARTYSALES_DOWN: "/public/reports/PartyWiseSalesReport_csvexport",
        PAYABLEREPORT_DOWN: "/public/purchaseinvoice/payablereport_csvexport",
        RECEIVABLEREPORT_DOWN: "/public/invoice/receivablereport_csvexport",
        TAXREPORT_DOWN: "/public/invoice/salesTaxReport_csvexport",
        TAXSUMMARYREPORT_DOWN: "/public/invoice/salesTaxReportSummery_csvexport",
        GET_ALBUM_LIST: "/public/album/listAll",
        ALBUM_ADD: '/public/album/save',
        ALBUM_EDIT: '/public/album/modify/{{id}}',
        ALBUM_DELETE: "/public/album/delete/{{id}}",
        GET_ALBUM_VIEW: "/public/album/detail",
        GET_IMAGE_LIST: "/public/admin/service/imageUpload.json",
        ASSET_ADD: "/public/albumasset/save",
        GET_ALBUM_ASSET_LIST: '/public/albumasset/listAll',
        SAVE_COMMENT: "/public/albumassetcomments/save",
        GET_COMMENT_LIST: "/public/albumassetcomments/listAll",
        COMMENTLIST_ADD: "/public/albumassetcomments/save",
        GET_SHARE_LIST: "/public/albumpermission/listAll",
        SHARELIST_ADD: "/public/albumpermission/save",
        SHARELIST_DELETE: "/public/albumpermission/delete/{{id}}",
        SAVE_USERINVITE: "/public/user/userInvite",
        CREATE_SENDMAIL: "/public/admin/service/sendmail.json",
        DEAL_ACTIVITY_LIST: '/public/dealactivity/listAll',
        DEAL_ACTIVITY_ADD: "/public/dealactivity/save",
        DEAL_ACTIVITY_DELETE: "/public/dealactivity/delete/{{id}}",
        CONTACT_LIST: "/public/contact/listAll",
        CONTACT_ADD: "/public/contact/save",
        CONTACT_EDIT: "/public/contact/update/{{id}}",
        CONTACT_DELETE: "/public/contact/delete/{{id}}",
        ADVANCEPAYMENT_DELETE: "/public/advancepayment/delete/{{id}}",
        ADVANCEPAYMENT_ADD: "/public/advancepayment/save",
        ADVANCEPAYMENT_LIST: "/public/advancepayment/listAll",
        ADVANCEPAYMENT_EDIT: "/public/advancepayment/update/{{id}}",
        TRANSPORT_LIST: '/public/transport/listAll',
        TRANSPORT_ADD: '/public/transport/save',
        TRANSPORT_EDIT: '/public/transport/update/{{id}}',
        TRANSPORT_DELETE: "/public/transport/delete/{{id}}",
        DELIVERYINSTRUCTION_LIST: '/public/deliveryInstruction/listAll',
        DELIVERYINSTRUCTION_DETAIL: '/public/deliveryInstruction/detail',
        DELIVERYINSTRUCTION_ADD: '/public/deliveryInstruction/save',
        DELIVERYINSTRUCTION_EDIT: '/public/deliveryInstruction/update/{{id}}',
        DELIVERYINSTRUCTION_DELETE: "/public/deliveryInstruction/delete/{{id}}",
        DELIVERYINSTRUCTION_DETAIL_ADD: '/public/deliveryInstructionDetail/detailSave',
        DELIVERYINSTRUCTION_DETAIL_LIST: '/public/deliveryInstructionDetail/listAll',
        UNMAPPED_DI_INVOICE: '/public/deliveryInstruction/getUnMappedInvoice',
        DELIVERYINSTRUCTION_INVOICEDETAIL_DELETE: "/public/deliveryInstructionDetail/delete",
        ACODE_SEARCH: "/public/acode/search",
        ACODE_LIST: "/public/acode/listAll",
        AREAWISE_SALES_REPORT: "/public/salesorder/areaWiseSalesReport",
        EMP_TEAM_LIST: '/public/employeeteam/listAll',
        EMP_TEAM_ADD: '/public/employeeteam/save',
        EMP_TEAM_EDIT: '/public/employeeteam/update/{{id}}',
        EMP_TEAM_DELETE: "/public/employeeteam/delete/{{id}}",
        EMPLOYEE_LIST: "/public/employee/listAll",
        EMPLOYEE_ADD: "/public/employee/save",
        EMPLOYEE_EDIT: "/public/employee/update/{{id}}",
        EMPLOYEE_DELETE: "/public/employee/delete/{{id}}",
        PRODUCTION_TASK_DELETE: "/public/bom/delete/{{id}}",
        PRODUCTION_TASK_ADD: "/public/bom/save",
        PRODUCTION_TASK_LIST: "/public/bom/listAll",
        PRODUCTION_TASK_EDIT: "/public/bom/update/{{id}}",
        PRODUCTION_TASK_DETAIL: '/public/bom/detail',
        GET_SALES_AGEING_DETAIL: '/public/dashboard/AgeingReport',
        PRODUCTION_ADD: "/public/empProduction/save",
        PRODUCTION_LIST: "/public/empProduction/listAll",
        PRODUCTION_EDIT: "/public/empProduction/update/{{id}}",
        PRODUCTION_DETAIL: "/public/empProduction/detail",
        CUSTOMER_ANNIVERSARY_REPORT: "/public/customer/wishList",
        SEND_SMS: "/public/customer/sendSms?type={{type}}",
        ALLOWANCE_MASTER_SAVE: '/public/allowanceMaster/save',
        ALLOWANCE_MASTER_LIST: '/public/allowanceMaster/listAll',
        ALLOWANCE_MASTER_DELETE: '/public/allowanceMaster/delete/{{id}}',
        ALLOWANCE_MASTER_UPDATE: '/public/allowanceMaster/update/{{id}}',
        ADVANCE_ADD: "/public/employeeAdvance/save",
        ADVANCE_LIST: "/public/employeeAdvance/listAll",
        ADVANCE_EDIT: "/public/employeeAdvance/update/{{id}}",
        ADVANCE_DELETE: "/public/employeeAdvance/delete/{{id}}",
        DEDUCTION_ADD: "/public/employeeAdvDeduct/save",
        DEDUCTION_LIST: "/public/employeeAdvDeduct/listAll",
        DEDUCTION_EDIT: "/public/employeeAdvDeduct/update/{{id}}",
        DEDUCTION_DELETE: "/public/employeeAdvDeduct/delete/{{id}}",
        EMPLOYEE_SALARY_LIST: '/public/empProduction/empSalary',
        BONUS_LIST: '/public/employeeBonus/listAll',
        BONUS_SAVE: '/public/employeeBonus/save',
        BONUS_EDIT: '/public/employeeBonus/update/{{id}}',
        BONUS_DELETE: '/public/employeeBonus/delete/{{id}}',
        BONUS_ADD: '/public/employeeBonus/empBonus',
        EMP_ALLOWANCE_ADD: "/public/employeeAllowance/save",
        EMP_ALLOWANCE_LIST: "/public/employeeAllowance/listAll",
        EMP_ALLOWANCE_EDIT: "/public/employeeAllowance/update/{{id}}",
        EMP_ALLOWANCE_DELETE: "/public/employeeAllowance/delete",
        SAVE_SALARY: '/public/empProduction/empSalarySave?account_id={{account_id}}',
        EMP_SALARY: '/public/empsalary/listAll',
        ESI_REPORT: '/public/empESI/listAll',
        PF_REPORT: '/public/empPF/listAll',
        EMP_SALARY_DETAIL: '/public/empProduction/empSalaryDetail',
        NO_PRODUCTION: '/public/empProduction/noProdutionEnterList',
        ADVANCE_REPORT: '/public/employeeAdvance/EmployeeAdvanceReport',
        EMP_TASK_TYPE_LIST: "/public/empTaskType/listAll",
        EMP_TASK_TYPE_ADD: "/public/empTaskType/save",
        EMP_TASK_TYPE_DELETE: "/public/empTaskType/delete/{{id}}",
        EMP_TASK_TYPE_EDIT: "/public/empTaskType/update/{{id}}",
        EMP_BALANCE_ADVANCE: "/public/employeeAdvance/EmployeeBalanceReport",
        WORK_ORDER_SAVE: "/public/workOrder/save",
        WORK_ORDER_EDIT: "/public/workOrder/update/{{id}}",
        WORK_ORDER_DELETE: "/public/workOrder/delete/{{id}}",
        WORK_ORDER_DETAIL: "/public/workOrder/detail",
        WORK_ORDER_LIST: "/public/workOrder/listAll",
        WORK_ORDER_PLAN: "/public/workOrder/orderPlan/{{id}}",
        WORK_ORDER_INPUT_LIST: "/public/workOrder/inputList",
        WORK_ORDER_DETAIL_LIST: "/public/workOrder/detailList",
        WORK_ORDER_PRODUCE: "/public/workOrder/produced",
        GRN_REPORT: "/public/grn/GrnReport",
        GRN_SAVE: "/public/grn/save",
        GRN_List_ALL: "/public/grn/listAll",
        GRN_DELETE: "/public/grn/delete/{{id}}",
        GRN_UPDATE: "/public/grn/update/{{id}}",
        GET_ITEMIZED_PURCHASED_LIST: '/public/reports/ItemWisePurchaseReport',
        GET_PRODUCTION_REPORT: '/public/reports/ProductionReport',
        TASK_MGT_DETAIL: '/public/taskActivity/listAll?is_active={{is_active}}&id={{id}}&ref_id={{ref_id}}&ref_name={{ref_name}}&start={{start}}&limit={{limit}}',
        TASK_MGT_LIST: '/public/taskActivity/listAll?is_active={{is_active}}&low={{low}}&medium={{medium}}&high={{high}}&initiated={{initiated}}&inprogress={{inprogress}}&completed={{completed}}&closed={{closed}}&task_name={{task_name}}&date={{date}}&estimated_date={{estimated_date}}&start={{start}}&limit={{limit}}',
        SAVE_TASK_MGT: '/public/taskActivity/save',
        EDIT_TASK_MGT: '/public/taskActivity/modify/{{id}}',
        DELETE_TASK_MGT: '/public/taskActivity/delete/{{id}}',
        TASK_ACTIVITY_COMMENTS_LIST: '/public/taskActivityComments/listAll',
        SAVE_TASK_COMMENTS: '/public/taskActivityComments/save',
        TASK_ADD: "/public/task/save",
        TASK_EDIT: "/public/task/modify/{{id}}",
        TASK_DELETE: "/public/task/delete/{{id}}",
        TASK_LIST: "/public/task/listAll",
        SOURCE_DELETE: "/public/source/delete/{{id}}",
        SOURCE_ADD: "/public/source/save",
        SOURCE_LIST: "/public/source/listAll",
        SOURCE_EDIT: "/public/source/update/{{id}}",
        GEOCODE_SEARCH_URL: "https://maps.googleapis.com/maps/api/geocode/json",
        SERVICE_REQUEST_SAVE: "/public/serviceRequest/save",
        SERVICE_REQUEST_UPDATE: "/public/serviceRequest/update/{{id}}",
        SERVICE_REQUEST_LISTALL: "/public/serviceRequest/listAll",
        SERVICE_REQUEST_DELETE: "/public/serviceRequest/delete/{{id}}",
        WORKLOG_SAVE: "/public/attendance/attendanceSave",
        WORKLOG_LIST: "/public/attendance/listAll",
        TOTAL_WORKING_TIME: "/public/attendance/totalWorkingReport",
        GET_ATTENDANCE_DEATILS_LIST: "/public/attendance/attendancedetails",
        LEAD_TYPE_SAVE: "/public/leadType/save",
        LEAD_TYPE_EDIT: "/public/leadType/update/{{id}}",
        LEAD_TYPE_LIST: "/public/leadType/listAll",
        LEAD_TYPE_DELETE: "/public/leadType/delete/{{id}}",
        AMC_SAVE: "/public/amc/save",
        AMC_EDIT: "/public/amc/update/{{id}}",
        AMC_LIST: "/public/amc/listAll",
        AMC_DELETE: "/public/amc/delete/{{id}}",
        GET_CUSTOMER_INVOICE_ITEM_LIST: "/public/invoice/customerItemList",
        GET_MACHINE_INFO: "/invoice/machineInfo",
        EXPENSE_TRACKER_ADD: "/public/ExpenseTracker/save",
        EXPENSE_TRACKER_EDIT: "/public/ExpenseTracker/update/{{id}}",
        EXPENSE_TRACKER_LIST: "/public/ExpenseTracker/listAll",
        EXPENSE_TRACKER_DELETE: "/public/ExpenseTracker/delete/{{id}}",
        EXPENSE_TRACKER_STATUS_UPDATE: "/public/ExpenseTracker/StatusUpdate",
        ALLOWANCE_MAPPING: "/public/allowanceMapping/listAll",
        ALLOWANCE_MAPPING_UPDATE: "/public/allowanceMapping/update",
        EXPENSE_TRACKER_REPORT: "/public/ExpenseTracker/Report",
        DEAL_FOLLOWER_SAVE: "/public/dealFollowers/save",
        DEAL_FOLLOWER_LISTALL: "/public/dealFollowers/listAll",
        DEAL_FOLLOWER_DELETE: "/public/dealFollowers/delete/{{id}}",
        USERBASED_COUNT: "/public/reports/userBasedReport",
        STAGEWISE_COUNT: "/public/reports/stageWiseReport",
        DEAL_CITYLIST_COUNT: "/public/reports/dealCityBasedReport",
        DEAL_TYPEBASED_COUNT: "/public/reports/dealTypeBasedReport",
        PRODUCT_BASED_COUNT: "/public/reports/dealNameBasedReport",
        DAILY_ACTIVITY_USER_REPORT: "/public/reports/dailyActivity",
        INACTIVE_DEAL_REPORT: "/public/reports/inactiveDeals",
        LEAD_WITHOUT_DEAL: "/public/reports/dealWithoutLead",
        EXPENSETRACKER_USERBASED: "/public/ExpenseTracker/userBasedReport",
        DEAL_CREATORS_LIST: "/public/reports/createdDealReport",
        CONTACT_DETAIL: "/public/contact/detail/{{id}}",
        ACODE_STATEMENT: "/public/acode/acodeReport",
        PREDEFINED_USER_ACCESS_INFO: "service/predefinedUserAccess.json",
        WORK_CENTER_ADD: "/public/workCenter/save",
        WORK_CENTER_LIST: "/public/workCenter/listAll",
        WORK_CENTER_DELETE: "/public/workCenter/delete/{{id}}",
        WORK_CENTER_MODIFY: "/public/workCenter/update/{{id}}",
        TASKGROUP_SAVE: "/public/taskGroup/save",
        TASKGROUP_UPDATE: "/public/taskGroup/update/{{id}}",
        TASKGROUP_LISTALL: "/public/taskGroup/listAll",
        TASKGROUP_DELETE: "/public/taskGroup/delete/{{id}}",
        TASKGROUP_DETAIL: "/public/taskGroup/detail",
        WORK_ORDER_SESSION_SAVE: "/public/workOrder/sessionSave",
        WORK_ORDER_SESSION_LISTALL: "/public/workOrder/sessionList",
        WORK_ORDER_STATUS_UPDATE: "/public/workOrder/orderStatusUpdate/{{id}}",
        WORK_ORDER_MARK_AS_DONE: "/public/workOrder/markAsDone/{{id}}",
        SALES_ORDER_PENDING: "/public/reports/salesOrderPendingReport",
        DEPARTMENT_ADD: "/public/department/save",
        DEPARTMENT_EDIT: "/public/department/update/{{id}}",
        DEPARTMENT_DELETE: "/public/department/delete/{{id}}",
        DEPARTMENT_LIST: "/public/department/listAll",
        WORK_ORDER_PENDING_LIST: "/public/workOrder/OrderPendingList",
        WORK_ORDER_CUSTOMER_SAVE: "/public/workOrder/customerAssignOrder",
        MATERIAL_REQUIREMENT_REPORT: "/public/reports/MaterialRecuritmentReport",
        WORK_ORDER_UPDATE: "/public/workOrder/workOrderUpdate/{{id}}",
        SAVE_ISSUE_QTY: "/public/workOrder/reservingQty",
        SAVE_PRODUCT_TYPE: "/public/productType/save",
        DELETE_PRODUCT_TYPE: "/public/productType/delete/{{id}}",
        EDIT_PRODUCT_TYPE: "/public/productType/update/{{id}}",
        LIST_PRODUCT_TYPE: "/public/productType/listAll",
        CUSTOMER_BASED_REPORT: "/public/reports/customerReport",
        CHECK_SERIAL_NO: "/public/invoice/serialNo",
        ITEM_HISTORY_REPORT : "/public/invoice/serialNoSearch",
        CONVERSION_JOURNAL_SAVE : "/public/conversionJournal/save",
        CONVERSION_JOURNAL_EDIT : "/public/conversionJournal/update/{{id}}",
        CONVERSION_JOURNAL_LIST : "/public/conversionJournal/listAll",
        CONVERSION_JOURNAL_DELETE : "/public/conversionJournal/delete/{{id}}",
        QCP_SAVE : "/qcp/save",
        QCP_EDIT : "/qcp/update/{{id}}",
        QCP_LIST : "/qcp/listAll",
        QCP_DELETE : "/qcp/delete/{{id}}",
        QUALITY_CHECK :"/public/qcp/qualityCheck",
        EXTERNAL_WISE_PRODUCT_REPORT : "/public/grnReports/externalType",
        RECEIVED_QTY_REPORT : "/public/grnReports/receivedQtyReport",
        EXPECTED_QTY_REPORT : "/public/grnReports/expectedQtyReport",
        MO_BASED_REPORT : "/public/grnReports/internalMoReport",
        MO_INTERNAL_RECEIVED_REPORT : "/grnReports/InternalReceivedBasedMoReport",
        INTERNAL_EXTERNAL_MO_PRINT : "/public/workOrder/moPrint",
        MO_ISSUED_REPORT:"/public/issue/listAll",
        ITEMWISE_ISSUED_REPORT:"/public/grnReports/MoItemWiseIssueReport",
        ISSUE_SAVE:"/public/issue/save",
        ISSUE_EDIT:"/public/issue/update/{{id}}",
        ISSUE_LIST:"/public/issue/listAll",
        ISSUE_DELETE:"/public/issue/delete/{{id}}",
        ISSUE_DETAIL_LIST:"/public/issue/detail"
    },
    BLOCKER_MSG: {
        PAGE_NAVIGATE: "You will lose unsaved changes if you leave this page",
        PAGE_RELOAD: "You will lose unsaved changes if you reload this page"

    },
    GOOGLE_API_KEY: "AIzaSyB-n6hREweTj6hYzxj9-nm7T7tapVBzMlc",
    DEFAULT_GEO_POINT: {
        LAT: 0,
        LNG: 0
    }
});