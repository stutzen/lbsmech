'use strict';

angular.module('app', [
      'ngAnimate',
      'ngCookies',
      'ngSanitize',
      'ngTouch',
      'ngStorage',
      'ui.router',
      'ui.bootstrap',
      'oc.lazyLoad',
      'pascalprecht.translate',
      'angularValidator',
      'angularLocalstorage',
      'hSweetAlert',
      'highcharts-ng',
      'dndLists',
      'textAngular',
      'chieffancypants.loadingBar',
      'ui.select',
      'uiGmapgoogle-maps',
      'ngMap',
    'ngMapAutocomplete'
]);
