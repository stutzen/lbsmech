'use strict';
/**
 * Config for the router
 */
angular.module('app')
        .run(
                ['$rootScope', '$state', '$stateParams', '$location', 'Auth',
                    function($rootScope, $state, $stateParams, $location, Auth) {
                        $rootScope.$state = $state;
                        $rootScope.$stateParams = $stateParams;
                        $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {

                            if (typeof $location.search().refer != 'undefined')
                            {
                                $rootScope.refer = $location.search().refer;
                            } else
                            {
                                $rootScope.refer = "";
                            }
                            //
                            if (!$rootScope.isUserModelUpdated) {

                                event.preventDefault();
                                if (!$rootScope.isUserModelUpdatedProcessing)
                                {
                                    $rootScope.isUserModelUpdatedProcessing = true;
                                    Auth.getUserInfo().then(function(response) {
                                        $state.go(toState, toParams);
                                    });
                                }
                                return;
                            }

                            if ($rootScope.getNavigationBlockMsg != null && $rootScope.getNavigationBlockMsg() != "")
                            {
                                if (!confirm($rootScope.getNavigationBlockMsg(false))) {
                                    event.preventDefault(); // user clicks cancel, wants to stay on page
                                } else {
                                    //No Script need to redirect another page
                                }

                            }


                        });
                        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                            Auth.checkCookie();
                            if ($rootScope.isLogined)
                            {
                                if (typeof toState.accessRole != 'undefined')
                                {
                                    var avaAccessRole = toState.accessRole;
                                    if (avaAccessRole.indexOf($rootScope.app.selectedRole) == -1)
                                    {
                                        Auth.validatePageAccess();
                                    }
                                }
                            }
                            $rootScope.hideHeader = false;
                            if (toState.name == 'attendanceLocation' || toState.name == 'requisitionPrint')
                            {
                                $rootScope.hideHeader = true;
                            } else
                            {
                                $rootScope.hideHeader = false;
                            }
                            if (typeof toState.containerClass != "undefined")
                            {
                                $rootScope.containerClass = toState.containerClass;
                            } else
                            {
                                $rootScope.containerClass = '';
                            }
                            //Current Page Update
                            if (typeof toState.pageName != "undefined" && $rootScope.isLogined)
                            {
                                $rootScope.currentPage = toState.pageName;
                            } else
                            {
                                $rootScope.currentPage = '';
                                $rootScope.actionMsgError = "Unauthorized";
                            }
                        });
                    }
                ]
                )
        .config(
                ['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG', '$provide', 'COMPONENT_CONFIG',
                    function($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG, $provide, COMPONENT_CONFIG) {

                        $urlRouterProvider.otherwise(function($injector, $location) {
                            var $state = $injector.get("$state");
                            $state.go('app');
                        });
                        $stateProvider
                                .state('app', {
                                    url: '',
                                    pageName: 'dashboard',
                                    containerClass: '',
                                    templateUrl: 'tpl/dashboard.html',
                                    resolve: load(['service/adminService.js', 'moment'])
                                })
                                .state('app.dashboard', {
                                    url: '/dashboard',
                                    pageName: 'dashboard',
                                    containerClass: 'czn-dashboard',
                                    templateUrl: 'tpl/dashboard-list.html',
                                    resolve: load(['controllor/dashboardListCtrl.js', 'sparklines', 'hc-area-chart'])
                                })
                                .state('app.userlist', {
                                    url: '/user-list',
                                    pageName: 'user',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-list.html',
                                    resolve: load(['controllor/userListCtrl.js'])
                                })
                                .state('app.userrole', {
                                    url: '/role-list',
                                    pageName: 'userrole',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-role.html',
                                    resolve: load(['controllor/userRoleCtrl.js'])
                                })
                                .state('app.userroleadd', {
                                    url: '/role-add',
                                    pageName: 'userrole-add',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-role-add.html',
                                    resolve: load(['controllor/userRoleAddCtrl.js'])
                                })
                                .state('app.userroleedit', {
                                    url: '/user-role-edit/:id',
                                    pageName: 'userrole-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-role-edit.html',
                                    resolve: load(['controllor/userRoleEditCtrl.js'])
                                })
                                .state('app.categorylist', {
                                    url: '/category-list',
                                    pageName: 'category',
                                    containerClass: '',
                                    templateUrl: 'tpl/category-list.html',
                                    resolve: load(['controllor/categoryListCtrl.js'])
                                })
                                .state('app.clientlist', {
                                    url: '/client-list',
                                    pageName: 'client',
                                    containerClass: '',
                                    templateUrl: 'tpl/client-list.html',
                                    resolve: load(['controllor/clientListCtrl.js'])
                                })
                                .state('app.paymentlist', {
                                    url: '/payment-list',
                                    pageName: 'payment',
                                    containerClass: 'payment-list-print',
                                    templateUrl: 'tpl/payment-list.html',
                                    resolve: load(['controllor/paymentListCtrl.js'])
                                })
                                .state('app.useradd', {
                                    url: '/user-save',
                                    pageName: 'user',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-add.html',
                                    resolve: load(['controllor/userAddCtrl.js'])
                                })
                                .state('app.paymentadd', {
                                    url: '/payment-save',
                                    pageName: 'payment',
                                    containerClass: '',
                                    templateUrl: 'tpl/paymentadd.html',
                                    resolve: load(['controllor/paymentAddCtrl.js'])
                                })
                                .state('app.clientadd', {
                                    url: '/client-save',
                                    pageName: 'client',
                                    templateUrl: 'tpl/clientadd.html',
                                    resolve: load(['controllor/clientAddCtrl.js'])
                                })
                                .state('app.categoryadd', {
                                    url: '/category-save',
                                    pageName: 'category',
                                    containerClass: '',
                                    templateUrl: 'tpl/category-add.html',
                                    resolve: load(['controllor/categoryAddCtrl.js'])
                                })
                                .state('app.userEdit', {
                                    url: '/user-edit/:userid',
                                    pageName: 'user',
                                    containerClass: '',
                                    templateUrl: 'tpl/user-edit.html',
                                    resolve: load(['controllor/userEditCtrl.js'])
                                })
                                .state('app.clientEdit', {
                                    url: '/client-edit/:id',
                                    pageName: 'client',
                                    containerClass: '',
                                    templateUrl: 'tpl/client-edit.html',
                                    resolve: load(['controllor/clientEditCtrl.js'])

                                })
                                .state('app.categoryEdit', {
                                    url: '/category-edit/:id',
                                    pageName: 'category',
                                    containerClass: '',
                                    templateUrl: 'tpl/category-edit.html',
                                    resolve: load(['controllor/categoryEditCtrl.js'])

                                })
                                .state('app.paymentEdit', {
                                    url: '/payment-edit/:id',
                                    pageName: 'payment',
                                    containerClass: '',
                                    templateUrl: 'tpl/payment-edit.html',
                                    resolve: load(['controllor/paymentEditCtrl.js'])
                                })
                                .state('app.payment-settlement', {
                                    url: '/payment-settlement',
                                    pageName: 'paymentsettle',
                                    containerClass: '',
                                    templateUrl: 'tpl/payment-settlement.html',
                                    resolve: load(['controllor/paymentSettlementCtrl.js'])
                                })
                                .state('settlement-print', {
                                    url: '/settlement-print/:id',
                                    pageName: 'settlement-print',
                                    containerClass: '',
                                    templateUrl: 'tpl/payment-settlement-print.html',
                                    resolve: load(['service/adminService.js', 'controllor/paymentSettlementPrintCtrl.js'])
                                })
                                .state('app.changepassword', {
                                    url: '/changepassword',
                                    pageName: 'changepassword',
                                    containerClass: '',
                                    templateUrl: 'tpl/changePassword.html',
                                    resolve: load(['controllor/changePasswordCtrl.js', 'controllor/userEditCtrl.js'])
                                })
                                .state('app.deal', {
                                    url: '/deal-list',
                                    pageName: 'deal-list',
                                    containerClass: '',
                                    templateUrl: 'tpl/deal.html',
                                    resolve: load(['controllor/dealCtrl.js'])
                                })
                                .state('app.dealadd', {
                                    url: '/deal-save/:id',
                                    pageName: 'deal-save',
                                    containerClass: '',
                                    templateUrl: 'tpl/dealadd.html',
                                    resolve: load(['controllor/dealAddCtrl.js', 'controllor/newCustomerAddCtrl.js'])
                                })
                                .state('app.dealedit', {
                                    url: '/deal-modify/:id',
                                    pageName: 'deal-modify',
                                    containerClass: '',
                                    templateUrl: 'tpl/dealedit.html',
                                    resolve: load(['controllor/dealEditCtrl.js', 'controllor/newCustomerAddCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.company', {
                                    url: '/Company-List',
                                    pageName: 'company',
                                    containerClass: '',
                                    templateUrl: 'tpl/company.html',
                                    resolve: load(['controllor/companyListCtrl.js'])
                                })
                                .state('app.companyAdd', {
                                    url: '/Company-List/:id',
                                    pageName: 'company',
                                    containerClass: '',
                                    templateUrl: 'tpl/companyadd.html',
                                    resolve: load(['controllor/companyAddCtrl.js'])
                                })
                                .state('app.companyEdit', {
                                    url: '/Company-modify/:id',
                                    pageName: 'company',
                                    containerClass: '',
                                    templateUrl: 'tpl/companyedit.html',
                                    resolve: load(['controllor/companyEditCtrl.js'])
                                })
                                .state('app.contactCompanyAdd', {
                                    url: '/contact-company-save/:id',
                                    pageName: 'contactCompany',
                                    containerClass: '',
                                    templateUrl: 'tpl/companyadd.html',
                                    resolve: load(['controllor/companyAddCtrl.js', 'controllor/contactAddCtrl.js'])
                                })
                                .state('app.leadtype', {
                                    url: '/Leadtype-List',
                                    pageName: 'leadtype',
                                    containerClass: '',
                                    templateUrl: 'tpl/leadtype.html',
                                    resolve: load(['controllor/leadtypeListCtrl.js'])
                                })
                                .state('app.leadtypeAdd', {
                                    url: '/leadtype-List-save/:id',
                                    pageName: 'leadtype',
                                    containerClass: '',
                                    templateUrl: 'tpl/leadtypeadd.html',
                                    resolve: load(['controllor/leadtypeAddCtrl.js'])
                                })
                                .state('app.leadtypeEdit', {
                                    url: '/leadtype-Modify-save/:id',
                                    pageName: 'leadtype',
                                    containerClass: '',
                                    templateUrl: 'tpl/leadtypeedit.html',
                                    resolve: load(['controllor/leadtypeEditCtrl.js'])
                                })
                                .state('app.stage', {
                                    url: '/crm-stage-list',
                                    pageName: 'stage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stage.html',
                                    resolve: load(['controllor/stageCtrl.js'])
                                })
                                .state('app.stageadd', {
                                    url: '/crm-stage-save',
                                    pageName: 'stage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stageadd.html',
                                    resolve: load(['controllor/stageAddCtrl.js'])
                                })
                                .state('app.stageedit', {
                                    url: '/crm-stage-modify/:id',
                                    pageName: 'stage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stageedit.html',
                                    resolve: load(['controllor/stageEditCtrl.js'])
                                })
                                .state('app.purchaseAdvancePayReceipt', {
                                    url: '/purchase-advance-payment-receipt-list',
                                    pageName: 'purchaseAdvancePayReceipt',
                                    containerClass: '',
                                    templateUrl: 'tpl/purchaseAdvancePayReceipt.html',
                                    resolve: load(['controllor/purchaseAdvancePayReceiptCtrl.js'])
                                })
                                .state('app.purchaseAdvancePayReceiptAdd', {
                                    url: '/purchase-advance-payment-receipt-add',
                                    pageName: 'purchaseAdvancePayReceiptAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/purchaseAdvancePayReceiptAdd.html',
                                    resolve: load(['controllor/purchaseAdvancePayReceiptAddCtrl.js'])
                                })
                                .state('app.purchaseAdvancePayReceiptView', {
                                    url: '/purchase-advance-payment-receipt-view/:id',
                                    pageName: 'purchaseAdvancePayReceiptView',
                                    containerClass: '',
                                    templateUrl: 'tpl/purchaseAdvancePayReceiptView.html',
                                    resolve: load(['controllor/purchaseAdvancePayReceiptViewCtrl.js'])
                                })
                                .state('app.advancepayment', {
                                    url: '/advancepayment-list',
                                    pageName: 'advancepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepayment.html',
                                    resolve: load(['controllor/advancepaymentCtrl.js'])
                                })
                                .state('app.advancepaymenAdd', {
                                    url: '/advancepayment-save',
                                    pageName: 'advancepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepaymentAdd.html',
                                    resolve: load(['controllor/advancepaymentAddCtrl.js'])
                                })
                                .state('app.advancepaymentEdit', {
                                    url: '/advancepayment-modify/:id',
                                    pageName: 'advancepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepaymentEdit.html',
                                    resolve: load(['controllor/advancepaymentEditCtrl.js'])
                                })
                                .state('app.advancepaymentView', {
                                    url: '/advancepayment-view/:id',
                                    pageName: 'advancepayment',
                                    containerClass: '',
                                    templateUrl: 'tpl/advancepaymentView.html',
                                    resolve: load(['controllor/advancepaymentViewCtrl.js'])
                                })
                                .state('app.screenAccess', {
                                    url: '/role-access/:id',
                                    pageName: 'screenaccess',
                                    containerClass: '',
                                    templateUrl: 'tpl/screen-access-edit.html',
                                    resolve: load(['controllor/screenAccessEditCtrl.js'])
                                })
                                .state('app.customersettings', {
                                    url: '/customersettings',
                                    pageName: 'customersettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/customersettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js'])
                                })
                                .state('app.appSettings', {
                                    url: '/appSettings',
                                    pageName: 'appSettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/appSettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js', 'ls-fileupload', 'angularLsFileUpload', 'ls-photo-gallery'])
                                })
                                .state('app.invoicesettings', {
                                    url: '/invoicesettings',
                                    pageName: 'invoicesettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/invoicesettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js'])
                                })
                                .state('app.prefixsettings', {
                                    url: '/prefixsettings',
                                    pageName: 'prefixsettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/prefixSettings.html',
                                    resolve: load(['controllor/appSettingsEditCtrl.js'])
                                })
                                .state('app.smssettingsedit', {
                                    url: '/smssettings/:id',
                                    pageName: 'smssettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/smssettings.html',
                                    resolve: load(['controllor/smsSettingsCtrl.js'])
                                })
                                .state('app.smssettings', {
                                    url: '/smssettings',
                                    pageName: 'smssettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/smssettingsList.html',
                                    resolve: load(['controllor/smssettingsListCtrl.js'])
                                })
                                .state('app.emailsettings', {
                                    url: '/emailsettings-list',
                                    pageName: 'emailsettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/emailSettingsList.html',
                                    resolve: load(['controllor/emailSettingsListCtrl.js'])
                                })
                                .state('app.clientdetail', {
                                    url: '/client-detail/:id',
                                    containerClass: '',
                                    templateUrl: 'tpl/clientdetail.html',
                                    resolve: load(['controllor/clientDetailCtrl.js'])

                                })
                                .state('app.accountlist', {
                                    url: '/account-list',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/account-list.html',
                                    resolve: load(['controllor/accountListCtrl.js'])
                                })
                                .state('app.accountaddd', {
                                    url: '/account-add',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/account-add.html',
                                    resolve: load(['controllor/accountAddCtrl.js'])
                                })
                                .state('app.device', {
                                    url: '/device-list',
                                    pageName: 'device',
                                    containerClass: '',
                                    templateUrl: 'tpl/device.html',
                                    resolve: load(['controllor/deviceCtrl.js'])
                                })
                                .state('app.deviceadd', {
                                    url: '/device-save',
                                    pageName: 'device',
                                    containerClass: '',
                                    templateUrl: 'tpl/deviceadd.html',
                                    resolve: load(['controllor/deviceAddCtrl.js'])
                                })
                                .state('app.deviceedit', {
                                    url: '/device-modify/:id',
                                    pageName: 'device',
                                    containerClass: '',
                                    templateUrl: 'tpl/deviceedit.html',
                                    resolve: load(['controllor/deviceEditCtrl.js'])
                                })
                                .state('app.commission', {
                                    url: '/commission-report',
                                    pageName: 'commission',
                                    containerClass: 'payment-list-print',
                                    templateUrl: 'tpl/commissionReport.html',
                                    resolve: load(['controllor/commissionReportCtrl.js'])
                                })
                                .state('app.attendancereport', {
                                    url: '/attendance-report',
                                    pageName: 'attendance',
                                    containerClass: '',
                                    templateUrl: 'tpl/attendanceReport.html',
                                    resolve: load(['controllor/attendanceReportCtrl.js'])
                                })
                                .state('app.attribute', {
                                    url: '/attribute-list',
                                    pageName: 'attribute',
                                    containerClass: '',
                                    templateUrl: 'tpl/attribute-list.html',
                                    resolve: load(['controllor/attributeListCtrl.js'])
                                })
                                .state('app.attributeadd', {
                                    url: '/attribute-add',
                                    pageName: 'attributeadd',
                                    containerClass: '',
                                    templateUrl: 'tpl/attribute-add.html',
                                    resolve: load(['controllor/attributeAddCtrl.js'])
                                })
                                .state('app.attributeedit', {
                                    url: '/attribute-Edit/:id',
                                    pageName: 'attributeedit',
                                    containerClass: '',
                                    templateUrl: 'tpl/attribute-edit.html',
                                    resolve: load(['controllor/attributeEditCtrl.js'])
                                })
                                .state('app.serviceAdd', {
                                    url: '/serviceAdd',
                                    pageName: 'serviceAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceAdd.html',
                                    resolve: load(['controllor/serviceAddCtrl.js'])
                                })
                                .state('app.serviceList', {
                                    url: '/serviceList',
                                    pageName: 'serviceList',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceList.html',
                                    resolve: load(['controllor/serviceListCtrl.js'])
                                })
                                .state('app.serviceEdit', {
                                    url: '/serviceEdit/:id',
                                    pageName: 'serviceEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceEdit.html',
                                    resolve: load(['controllor/serviceEditCtrl.js'])
                                })
                                .state('app.productAdd', {
                                    url: '/productAdd',
                                    pageName: 'productAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/productAdd.html',
                                    resolve: load(['controllor/productAddCtrl.js'])
                                })
                                .state('app.productList', {
                                    url: '/productList',
                                    pageName: 'productList',
                                    containerClass: '',
                                    templateUrl: 'tpl/productList.html',
                                    resolve: load(['controllor/productListCtrl.js'])
                                })
                                .state('app.productEdit', {
                                    url: '/productEdit/:id',
                                    pageName: 'productEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/productEdit.html',
                                    resolve: load(['controllor/productEditCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.customers', {
                                    url: '/customersList',
                                    pageName: 'customers',
                                    containerClass: '',
                                    templateUrl: 'tpl/customer-list.html',
                                    resolve: load(['controllor/customerListCtrl.js'])
                                })
                                .state('app.customeradd', {
                                    url: '/customeradd/:id/:deal_id',
                                    pageName: 'customeradd',
                                    containerClass: '',
                                    templateUrl: 'tpl/customeradd.html',
                                    resolve: load(['controllor/customerAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.customerEdit', {
                                    url: '/customerEdit/:id',
                                    pageName: 'customerEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/customer-edit.html',
                                    resolve: load(['controllor/customerEditCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.customerTimeline', {
                                    url: '/customerTimeline/:id',
                                    pageName: 'customerTimeline',
                                    containerClass: '',
                                    templateUrl: 'tpl/customerTimeline.html',
                                    resolve: load(['controllor/customerTimelineCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.uom', {
                                    url: '/uom',
                                    templateUrl: 'tpl/uom.html',
                                    resolve: load(['controllor/uomCtrl.js'])
                                })
                                .state('app.uomAdd', {
                                    url: '/uom-add',
                                    templateUrl: 'tpl/uomAdd.html',
                                    resolve: load(['controllor/uomAddCtrl.js'])
                                })
                                .state('app.uomEdit', {
                                    url: '/uom-edit/:id',
                                    templateUrl: 'tpl/uomEdit.html',
                                    resolve: load(['controllor/uomEditCtrl.js'])
                                })
                                .state('app.salesquote', {
                                    url: '/salesquote',
                                    pageName: 'salesquote',
                                    containerClass: '',
                                    templateUrl: 'tpl/salesquote.html',
                                    resolve: load(['controllor/salesquoteCtrl.js'])
                                })
                                .state('app.sampleadd', {
                                    url: '/sampleadd',
                                    pageName: 'sampleadd',
                                    containerClass: '',
                                    templateUrl: 'tpl/sampleadd.html',
                                    resolve: load(['controllor/sampleAddCtrl.js'])
                                })
                                .state('app.neworder', {
                                    url: '/new-order',
                                    pageName: "new-order",
                                    templateUrl: 'tpl/orders-new.html',
                                    resolve: load(['controllor/ordernewCtrl.js', 'controllor/productselectCtrl.js'])
                                })
                                .state('app.purchaseInvoice', {
                                    url: '/purchase-invoice',
                                    pageName: "purchase-invoice",
                                    templateUrl: 'tpl/purchase-invoice-list.html',
                                    resolve: load(['controllor/purchaseInvoiceListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseInvoiceAdd', {
                                    url: '/purchase-invoice-add/:id',
                                    pageName: "purchase-invoiceAdd",
                                    templateUrl: 'tpl/purchase-invoice-add.html',
                                    resolve: load(['controllor/purchaseInvoiceAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.grnpurchaseInvAdd', {
                                    url: '/grn-purchase-invoice-add/:id',
                                    pageName: "grn-purchase-invoiceAdd",
                                    templateUrl: 'tpl/purchase-invoice-add.html',
                                    resolve: load(['controllor/purchaseInvoiceAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.purchaseInvoiceEdit', {
                                    url: '/purchase-invoice-edit/:id',
                                    pageName: "purchase-invoiceEdit",
                                    templateUrl: 'tpl/purchase-invoice-edit.html',
                                    resolve: load(['controllor/purchaseInvoiceEditCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.purchaseInvoiceView', {
                                    url: '/purchase-invoice-view/:id',
                                    pageName: "purchase-invoiceView",
                                    templateUrl: 'tpl/purchase-invoice-view.html',
                                    resolve: load(['controllor/purchaseInvoiceViewCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoice', {
                                    url: '/sales-invoice',
                                    pageName: "invoice",
                                    templateUrl: 'tpl/invoiceList.html',
                                    resolve: load(['controllor/invoiceListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoiceAdd', {
                                    url: '/sales-invoice-add/:id',
                                    pageName: "invoiceAdd",
                                    templateUrl: 'tpl/invoiceAdd.html',
                                    resolve: load(['controllor/invoiceAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/newCustomerAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.serviceInvoiceAdd', {
                                    url: '/service-invoice-add/:service_id',
                                    pageName: "serviceInvoiceAdd",
                                    templateUrl: 'tpl/invoiceAdd.html',
                                    resolve: load(['controllor/invoiceAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/newCustomerAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.amcInvoiceAdd', {
                                    url: '/amc-invoice-add/:amc_id',
                                    pageName: "amcInvoiceAdd",
                                    templateUrl: 'tpl/invoiceAdd.html',
                                    resolve: load(['controllor/invoiceAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/newCustomerAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.dealInvoiceAdd', {
                                    url: '/deal-invoice-add/:id/:deal_id/:cust_id',
                                    pageName: "dealInvoiceAdd",
                                    templateUrl: 'tpl/invoiceAdd.html',
                                    resolve: load(['controllor/invoiceAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/newCustomerAddCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoiceEdit', {
                                    url: '/sales-invoice-edit/:id',
                                    pageName: "invoiceEdit",
                                    templateUrl: 'tpl/invoiceEdit.html',
                                    resolve: load(['controllor/invoiceEditCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js', , 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-date', 'custom-checkbox', 'custom-yes-or-no'])
                                })
                                .state('app.purchaseQuote', {
                                    url: '/purchase-order',
                                    pageName: "purchase-order",
                                    templateUrl: 'tpl/purchase-quote-list.html',
                                    resolve: load(['controllor/purchaseQuoteListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseQuoteAdd', {
                                    url: '/purchase-order-add/:id',
                                    pageName: "purchase-orderAdd",
                                    templateUrl: 'tpl/purchase-quote-add.html',
                                    resolve: load(['controllor/purchaseQuoteAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseQuoteEdit', {
                                    url: '/purchase-order-edit/:id',
                                    pageName: "purchase-orderEdit",
                                    templateUrl: 'tpl/purchase-quote-edit.html',
                                    resolve: load(['controllor/purchaseQuoteEditCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.purchaseQuoteView', {
                                    url: '/purchase-order-view/:id',
                                    pageName: "purchase-orderView",
                                    templateUrl: 'tpl/purchase-quote-view.html',
                                    resolve: load(['controllor/purchaseQuoteViewCtrl.js', 'controllor/purchaseProductListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.salesorder', {
                                    url: '/sales-order',
                                    pageName: "order",
                                    templateUrl: 'tpl/salesorderList.html',
                                    resolve: load(['controllor/salesorderListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.salesorderAdd', {
                                    url: '/sales-order-add/:id',
                                    pageName: "orderAdd",
                                    templateUrl: 'tpl/salesorderAdd.html',
                                    resolve: load(['controllor/salesorderAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.dealsalesorderAdd', {
                                    url: '/sales-order-add/:id/:deal_id/:cust_id',
                                    pageName: "orderAdd",
                                    templateUrl: 'tpl/salesorderAdd.html',
                                    resolve: load(['controllor/salesorderAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.salesorderEdit', {
                                    url: '/sales-order-edit/:id',
                                    pageName: "orderEdit",
                                    templateUrl: 'tpl/salesorderEdit.html',
                                    resolve: load(['controllor/salesorderEditCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.salesorderView', {
                                    url: '/sales-order-view/:id',
                                    pageName: "orderView",
                                    templateUrl: 'tpl/salesorderView.html',
                                    resolve: load(['controllor/salesorderViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.amc', {
                                    url: '/amc-list',
                                    pageName: "amcList",
                                    templateUrl: 'tpl/amcList.html',
                                    resolve: load(['controllor/amcListCtrl.js'])
                                })
                                .state('app.amcAdd', {
                                    url: '/amc-add/',
                                    pageName: "amcAdd",
                                    templateUrl: 'tpl/amcAdd.html',
                                    resolve: load(['controllor/amcAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.amcEdit', {
                                    url: '/amc-edit/:id',
                                    pageName: "amcEdit",
                                    templateUrl: 'tpl/amcEdit.html',
                                    resolve: load(['controllor/amcEditCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.quote', {
                                    url: '/sales-estimate',
                                    pageName: "estimate",
                                    templateUrl: 'tpl/quoteList.html',
                                    resolve: load(['controllor/quoteListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.quoteAdd', {
                                    url: '/sales-estimate-add/:id',
                                    pageName: "estimateAdd",
                                    templateUrl: 'tpl/quoteAdd.html',
                                    resolve: load(['controllor/quoteAddCtrl.js', 'controllor/newContactAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.dealQuoteAdd', {
                                    url: '/sales-estimate-add/:id/:cust_id',
                                    pageName: "orderAdd",
                                    templateUrl: 'tpl/quoteAdd.html',
                                    resolve: load(['controllor/quoteAddCtrl.js', 'controllor/newContactAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.quoteEdit', {
                                    url: '/sales-estimate-edit/:id',
                                    pageName: "estimateEdit",
                                    templateUrl: 'tpl/quoteEdit.html',
                                    resolve: load(['controllor/quoteEditCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoicePayList', {
                                    url: '/invoice-pay-list',
                                    pageName: "invoicePayList",
                                    templateUrl: 'tpl/invoice-pay-list.html',
                                    resolve: load(['controllor/invoicepayListCtrl.js'])
                                })
                                .state('app.invoicePayAdd', {
                                    url: '/invoice-pay-add',
                                    pageName: "invoicePayAdd",
                                    templateUrl: 'tpl/invoice-pay-add.html',
                                    resolve: load(['controllor/invoicepayAddCtrl.js'])
                                })
                                .state('app.invoiceView1', {
                                    url: '/sales-invoice-view-t1/:id',
                                    pageName: "invoiceView1",
                                    templateUrl: 'tpl/invoiceView1.html',
                                    resolve: load(['controllor/invoiceViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.invoiceView2', {
                                    url: '/sales-invoice-view-t2/:id',
                                    pageName: "invoiceView2",
                                    templateUrl: 'tpl/invoiceView2.html',
                                    resolve: load(['controllor/invoiceViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.quoteView', {
                                    url: '/sales-estimate-view/:id',
                                    pageName: "estimateView",
                                    templateUrl: 'tpl/quoteView.html',
                                    resolve: load(['controllor/quoteViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.trial', {
                                    url: '/trial-report',
                                    pageName: 'trial',
                                    containerClass: 'payment-list-print',
                                    templateUrl: 'tpl/trialReport.html',
                                    resolve: load(['controllor/trialReportCtrl.js'])
                                })
                                .state('app.expense', {
                                    url: '/expense-list',
                                    pageName: "expense",
                                    templateUrl: 'tpl/expenseList.html',
                                    resolve: load(['controllor/expenseListCtrl.js'])
                                })
                                .state('app.expenseadd', {
                                    url: '/expense-add',
                                    pageName: "expense",
                                    templateUrl: 'tpl/expenseAdd.html',
                                    resolve: load(['controllor/expenseAddCtrl.js'])
                                })
                                .state('app.expenseedit', {
                                    url: '/expense-edit/:id',
                                    pageName: "expense",
                                    templateUrl: 'tpl/expenseEdit.html',
                                    resolve: load(['controllor/expenseEditCtrl.js'])
                                })
                                .state('app.allowanceList', {
                                    url: '/allowanceList',
                                    pageName: "allowanceList",
                                    templateUrl: 'tpl/allowanceList.html',
                                    resolve: load(['controllor/allowanceListCtrl.js'])
                                })
                                .state('app.allowanceAdd', {
                                    url: '/allowanceAdd',
                                    pageName: "allowanceAdd",
                                    templateUrl: 'tpl/allowanceAdd.html',
                                    resolve: load(['controllor/allowanceAddCtrl.js'])
                                })
                                .state('app.allowanceEdit', {
                                    url: '/allowanceEdit/:id',
                                    pageName: "allowanceEdit",
                                    templateUrl: 'tpl/allowanceEdit.html',
                                    resolve: load(['controllor/allowanceEditCtrl.js'])
                                })
                                .state('app.allowancesettings', {
                                    url: '/allowance-settings/:id',
                                    pageName: "allowance-settings",
                                    templateUrl: 'tpl/allowanceSettings.html',
                                    resolve: load(['controllor/allowanceSettingsCtrl.js'])
                                })
                                .state('app.incomecategory', {
                                    url: '/incomecategory',
                                    pageName: "incomecategory",
                                    templateUrl: 'tpl/incomecategoryList.html',
                                    resolve: load(['controllor/incomecategoryListCtrl.js'])
                                })
                                .state('app.incomecategoryadd', {
                                    url: '/incomecategory-add',
                                    pageName: "incomecategory",
                                    templateUrl: 'tpl/incomecategoryAdd.html',
                                    resolve: load(['controllor/incomecategoryAddCtrl.js'])
                                })
                                .state('app.incomecategoryedit', {
                                    url: '/incomecategory-edit/:id',
                                    pageName: "incomecategory",
                                    templateUrl: 'tpl/incomecategoryEdit.html',
                                    resolve: load(['controllor/incomecategoryEditCtrl.js'])
                                })
                                .state('app.expensecategory', {
                                    url: '/expensecategory',
                                    pageName: "expensecategory",
                                    templateUrl: 'tpl/expensecategoryList.html',
                                    resolve: load(['controllor/expensecategoryListCtrl.js'])
                                })
                                .state('app.expensecategoryadd', {
                                    url: '/expensecategory-add',
                                    pageName: "expensecategory",
                                    templateUrl: 'tpl/expensecategoryAdd.html',
                                    resolve: load(['controllor/expensecategoryAddCtrl.js'])
                                })
                                .state('app.expensecategoryedit', {
                                    url: '/expensecategory-edit/:id',
                                    pageName: "expensecategory",
                                    templateUrl: 'tpl/expensecategoryEdit.html',
                                    resolve: load(['controllor/expensecategoryEditCtrl.js'])
                                })
                                .state('app.paymentprint', {
                                    url: '/payment-print/:id',
                                    pageName: "paymentprint",
                                    templateUrl: 'tpl/paymentprint.html',
                                    resolve: load(['controllor/paymentPrintCtrl.js'])
                                })
                                .state('app.transaction', {
                                    url: '/transaction-list',
                                    pageName: 'transaction',
                                    containerClass: '',
                                    templateUrl: 'tpl/transaction.html',
                                    resolve: load(['controllor/transactionCtrl.js'])
                                })
                                .state('app.sales', {
                                    url: '/sales-report',
                                    pageName: 'sales',
                                    containerClass: '',
                                    templateUrl: 'tpl/sales.html',
                                    resolve: load(['controllor/salesCtrl.js'])
                                })
                                .state('app.itemizedsales', {
                                    url: '/itemizedsales-report',
                                    pageName: 'itemizedsales',
                                    containerClass: '',
                                    templateUrl: 'tpl/itemizedsales.html',
                                    resolve: load(['controllor/itemizedsalesCtrl.js'])
                                })
                                .state('app.itemizedPurchase', {
                                    url: '/itemizedpurchase-report',
                                    pageName: 'itemizedPurchase',
                                    containerClass: '',
                                    templateUrl: 'tpl/itemizedPurchase.html',
                                    resolve: load(['controllor/itemizedPurchaseCtrl.js'])
                                })
                                .state('app.paymentreport', {
                                    url: '/payment-report',
                                    pageName: "paymentreport",
                                    templateUrl: 'tpl/paymentreport.html',
                                    resolve: load(['controllor/paymentReportCtrl.js'])
                                })
                                .state('app.receivablereport', {
                                    url: '/receivable-report',
                                    pageName: "receivablereport",
                                    templateUrl: 'tpl/receivablereport.html',
                                    resolve: load(['controllor/receivableReportCtrl.js'])
                                })
                                .state('app.payablereport', {
                                    url: '/payable-report',
                                    pageName: "payablereport",
                                    templateUrl: 'tpl/payablereport.html',
                                    resolve: load(['controllor/payableReportCtrl.js'])
                                })
                                .state('app.partysalesstatement', {
                                    url: '/partywise-sales-statement',
                                    pageName: "partysalesstatement",
                                    templateUrl: 'tpl/partysalesstatement.html',
                                    resolve: load(['controllor/partySalesStatementCtrl.js'])
                                })
                                .state('app.empPFList', {
                                    url: '/employee-pf-statement',
                                    pageName: "empPFList",
                                    templateUrl: 'tpl/empPFList.html',
                                    resolve: load(['controllor/empPFListCtrl.js'])
                                })
                                .state('app.partypurchasestatement', {
                                    url: '/party-purchase-statement',
                                    pageName: "partypurchasestatement",
                                    templateUrl: 'tpl/partypurchasestatement.html',
                                    resolve: load(['controllor/partyPurchasesStatementCtrl.js'])
                                })
                                .state('app.account', {
                                    url: '/account_list',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/account.html',
                                    resolve: load(['controllor/accountCtrl.js'])
                                })
                                .state('app.accountbalance', {
                                    url: '/accountbalance_list',
                                    pageName: 'accountbalance',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountbalance.html',
                                    resolve: load(['controllor/accountbalanceCtrl.js'])
                                })
                                .state('app.accountAdd', {
                                    url: '/account_add',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountAdd.html',
                                    resolve: load(['controllor/accountaddCtrl.js'])
                                })
                                .state('app.accountedit', {
                                    url: '/account-modify/:id',
                                    pageName: 'account',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountedit.html',
                                    resolve: load(['controllor/accountEditCtrl.js'])
                                })
                                .state('app.tax', {
                                    url: '/tax-list',
                                    pageName: 'tax',
                                    containerClass: '',
                                    templateUrl: 'tpl/tax.html',
                                    resolve: load(['controllor/taxCtrl.js'])
                                })
                                .state('app.followup', {
                                    url: '/todays-followup-report',
                                    pageName: 'followupreport',
                                    containerClass: '',
                                    templateUrl: 'tpl/followupreport.html',
                                    resolve: load(['controllor/followupReportCtrl.js'])
                                })
                                .state('app.taxsummary', {
                                    url: '/taxsummary-list',
                                    pageName: 'taxsummary',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxsummary.html',
                                    resolve: load(['controllor/taxsummaryCtrl.js'])
                                })
                                .state('app.deposit', {
                                    url: '/deposit-list',
                                    pageName: 'deposit',
                                    containerClass: '',
                                    templateUrl: 'tpl/deposit.html',
                                    resolve: load(['controllor/depositCtrl.js'])
                                })
                                .state('app.depositAdd', {
                                    url: '/deposit-save',
                                    pageName: 'deposit',
                                    templateUrl: 'tpl/depositAdd.html',
                                    resolve: load(['controllor/depositAddCtrl.js'])
                                })
                                .state('app.depositEdit', {
                                    url: '/deposit-modify/:id',
                                    pageName: 'deposit',
                                    containerClass: '',
                                    templateUrl: 'tpl/depositEdit.html',
                                    resolve: load(['controllor/depositEditCtrl.js'])
                                })
                                .state('app.transexpense', {
                                    url: '/transexpense_list',
                                    pageName: 'transexpense',
                                    containerClass: '',
                                    templateUrl: 'tpl/transexpense.html',
                                    resolve: load(['controllor/transexpenseCtrl.js'])
                                })
                                .state('app.transexpenseAdd', {
                                    url: '/transeexpense-save',
                                    pageName: 'transexpense',
                                    templateUrl: 'tpl/transexpenseAdd.html',
                                    resolve: load(['controllor/transexpenseAddCtrl.js'])
                                })
                                .state('app.transexpenseEdit', {
                                    url: '/transeexpense-modify/:id',
                                    pageName: 'transeexpense',
                                    containerClass: '',
                                    templateUrl: 'tpl/transexpenseEdit.html',
                                    resolve: load(['controllor/transexpenseEditCtrl.js'])
                                })
                                .state('app.accountstatement', {
                                    url: '/bank-statement',
                                    pageName: 'accountstatement',
                                    containerClass: '',
                                    templateUrl: 'tpl/accountstatement.html',
                                    resolve: load(['controllor/accountStatementCtrl.js'])
                                })
                                .state('app.balanceAdvanceReport', {
                                    url: '/balance-advance-report',
                                    pageName: 'balanceAdvanceReport',
                                    containerClass: '',
                                    templateUrl: 'tpl/balanceAdvanceReport.html',
                                    resolve: load(['controllor/balanceAdvanceReportCtrl.js'])
                                })
                                .state('app.acodestatement', {
                                    url: '/account-statement',
                                    pageName: 'acodestatement',
                                    containerClass: '',
                                    templateUrl: 'tpl/acodestatement.html',
                                    resolve: load(['controllor/acodeStatementCtrl.js'])
                                })
                                .state('app.activitylist', {
                                    url: '/activity-list',
                                    pageName: 'activitylist',
                                    containerClass: '',
                                    templateUrl: 'tpl/activity.html',
                                    resolve: load(['controllor/activityLogCtrl.js'])
                                })
                                .state('app.inventorystock', {
                                    url: '/inventorystock-list',
                                    pageName: 'inventorystock',
                                    containerClass: '',
                                    templateUrl: 'tpl/inventorystock.html',
                                    resolve: load(['controllor/inventoryStockCtrl.js'])
                                })
                                .state('app.stocktracelist', {
                                    url: '/stocktracelist',
                                    pageName: 'stocktrace',
                                    containerClass: '',
                                    templateUrl: 'tpl/stocktracelist.html',
                                    resolve: load(['controllor/stocktracelistCtrl.js'])
                                })
                                .state('app.stockadjustment', {
                                    url: '/stockadjustment-list',
                                    pageName: 'stockadjustment',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockadjustment.html',
                                    resolve: load(['controllor/stockadjustmentCtrl.js'])
                                })
                                .state('app.stockadjustmentAdd', {
                                    url: '/stockadjustment-save',
                                    pageName: 'stockadjustment',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockadjustmentAdd.html',
                                    resolve: load(['controllor/stockadjustmentAddCtrl.js'])
                                })
                                .state('app.stockwastage', {
                                    url: '/stockwastage-list',
                                    pageName: 'stockwastage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockwastage.html',
                                    resolve: load(['controllor/stockwastageCtrl.js'])
                                })
                                .state('app.stockwastageAdd', {
                                    url: '/stockwastage-save',
                                    pageName: 'stockwastage',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockwastageAdd.html',
                                    resolve: load(['controllor/stockwastageAddCtrl.js'])
                                })
                                .state('app.minstock', {
                                    url: '/minstock-list',
                                    pageName: 'minstock',
                                    containerClass: '',
                                    templateUrl: 'tpl/minstock.html',
                                    resolve: load(['controllor/minstockCtrl.js'])
                                })
                                .state('app.taxlist', {
                                    url: '/taxlist',
                                    pageName: 'taxlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxlist.html',
                                    resolve: load(['controllor/taxlistCtrl.js'])
                                })
                                .state('app.taxlistAdd', {
                                    url: '/tax-add',
                                    pageName: "taxlist",
                                    templateUrl: 'tpl/taxlistAdd.html',
                                    resolve: load(['controllor/taxlistAddCtrl.js'])
                                })
                                .state('app.taxlistEdit', {
                                    url: '/tax-modify/:id',
                                    pageName: 'taxlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxlistEdit.html',
                                    resolve: load(['controllor/taxlistEditCtrl.js'])
                                })
                                .state('app.transferlist', {
                                    url: '/journallist',
                                    pageName: 'journallist',
                                    containerClass: '',
                                    templateUrl: 'tpl/transferlist.html',
                                    resolve: load(['controllor/transferlistCtrl.js'])
                                })
                                .state('app.transferlistAdd', {
                                    url: '/journal-add',
                                    pageName: "journalAdd",
                                    templateUrl: 'tpl/transferlistAdd.html',
                                    resolve: load(['controllor/transferlistAddCtrl.js'])
                                })
                                .state('app.transferlistEdit', {
                                    url: '/journal-modify/:id',
                                    pageName: 'journalView',
                                    containerClass: '',
                                    templateUrl: 'tpl/transferlistEdit.html',
                                    resolve: load(['controllor/transferlistEditCtrl.js'])
                                })
                                .state('app.paymentterms', {
                                    url: '/paymentterms',
                                    pageName: 'paymentterms',
                                    containerClass: '',
                                    templateUrl: 'tpl/paymentterms.html',
                                    resolve: load(['controllor/paymenttermsCtrl.js'])
                                })
                                .state('app.paymenttermsAdd', {
                                    url: '/paymentterms-add',
                                    pageName: "paymentterms",
                                    templateUrl: 'tpl/paymenttermsAdd.html',
                                    resolve: load(['controllor/paymenttermsAddCtrl.js'])
                                })
                                .state('app.paymenttermsEdit', {
                                    url: '/paymentterms-modify/:id',
                                    pageName: 'paymentterms',
                                    containerClass: '',
                                    templateUrl: 'tpl/paymenttermsEdit.html',
                                    resolve: load(['controllor/paymenttermsEditCtrl.js'])
                                })
                                .state('app.activitylog', {
                                    url: '/activitylog',
                                    pageName: 'activitylog',
                                    containerClass: '',
                                    templateUrl: 'tpl/activitylog.html',
                                    resolve: load(['controllor/activityLogCtrl.js'])
                                })
                                .state('app.databasebackup', {
                                    url: '/databasebackup',
                                    pageName: 'databasebackup',
                                    containerClass: '',
                                    templateUrl: 'tpl/databasebackup.html'
                                })
                                .state('app.taxgroup', {
                                    url: '/taxgroup',
                                    pageName: 'taxgroup',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxgroup.html',
                                    resolve: load(['controllor/taxGroupCtrl.js'])
                                })
                                .state('app.taxgroupadd', {
                                    url: '/taxgroup-add',
                                    pageName: 'taxgroup',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxGroupAdd.html',
                                    resolve: load(['controllor/taxGroupAddCtrl.js'])
                                })
                                .state('app.taxgroupedit', {
                                    url: '/taxgroup-edit/:id',
                                    pageName: 'taxgroup',
                                    containerClass: '',
                                    templateUrl: 'tpl/taxGroupEdit.html',
                                    resolve: load(['controllor/taxGroupEditCtrl.js'])
                                })
                                .state('app.expensereport', {
                                    url: '/expensereport',
                                    pageName: 'expensereport',
                                    containerClass: '',
                                    templateUrl: 'tpl/expensereport.html',
                                    resolve: load(['controllor/expensereportCtrl.js'])
                                })
                                .state('app.emailsettingstemp', {
                                    url: '/emailsettings-template/:tplname',
                                    pageName: 'emailsettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/emailsettings-template.html',
                                    resolve: load(['controllor/emailSettingsTemplateCtrl.js'])
                                })
                                .state('app.emailsettingsedit', {
                                    url: '/emailsettings/:id',
                                    pageName: 'emailsettings',
                                    containerClass: '',
                                    templateUrl: 'tpl/emailsettings.html',
                                    resolve: load(['controllor/emailSettingsCtrl.js'])
                                })
                                .state('app.albumlist', {
                                    url: '/albumlist',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/albumlist.html',
                                    resolve: load(['controllor/albumlistCtrl.js', 'cloud-image-loader', 'photo-gallery'])
                                })
                                .state('app.albumAdd', {
                                    url: '/album-add',
                                    pageName: "albumlist",
                                    templateUrl: 'tpl/albumAdd.html',
                                    resolve: load(['controllor/albumAddCtrl.js'])
                                })
                                .state('app.albumEdit', {
                                    url: '/album-modify/:id',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/albumEdit.html',
                                    resolve: load(['controllor/albumEditCtrl.js'])
                                })
                                .state('app.albumView', {
                                    url: '/album-view/:id',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/albumView.html',
                                    resolve: load(['controllor/albumViewCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.albumViewNew', {
                                    url: '/albumNew-view-new/:id',
                                    pageName: 'albumlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/albumViewNew.html',
                                    resolve: load(['controllor/albumViewNewCtrl.js', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.country', {
                                    url: '/country-list',
                                    pageName: 'country',
                                    containerClass: '',
                                    templateUrl: 'tpl/countryList.html',
                                    resolve: load(['controllor/countryListCtrl.js'])
                                })
                                .state('app.countryAdd', {
                                    url: '/country-add',
                                    pageName: 'country',
                                    containerClass: '',
                                    templateUrl: 'tpl/countryAdd.html',
                                    resolve: load(['controllor/countryAddCtrl.js'])
                                })
                                .state('app.countryEdit', {
                                    url: '/country-edit/:id',
                                    pageName: 'country',
                                    containerClass: '',
                                    templateUrl: 'tpl/countryEdit.html',
                                    resolve: load(['controllor/countryEditCtrl.js'])
                                })
                                .state('app.state', {
                                    url: '/state-list',
                                    pageName: 'state',
                                    containerClass: '',
                                    templateUrl: 'tpl/stateList.html',
                                    resolve: load(['controllor/stateListCtrl.js'])
                                })
                                .state('app.stateAdd', {
                                    url: '/state-add',
                                    pageName: 'state',
                                    containerClass: '',
                                    templateUrl: 'tpl/stateAdd.html',
                                    resolve: load(['controllor/stateAddctrl.js'])
                                })
                                .state('app.stateEdit', {
                                    url: '/state-edit/:id',
                                    pageName: 'state',
                                    containerClass: '',
                                    templateUrl: 'tpl/stateEdit.html',
                                    resolve: load(['controllor/stateEditCtrl.js'])
                                })
                                .state('app.city', {
                                    url: '/city-list',
                                    pageName: 'city',
                                    containerClass: '',
                                    templateUrl: 'tpl/cityList.html',
                                    resolve: load(['controllor/cityListCtrl.js'])
                                })
                                .state('app.cityAdd', {
                                    url: '/city-add',
                                    pageName: 'city',
                                    containerClass: '',
                                    templateUrl: 'tpl/cityAdd.html',
                                    resolve: load(['controllor/cityAddCtrl.js'])
                                })
                                .state('app.cityEdit', {
                                    url: '/city-edit/:id',
                                    pageName: 'city',
                                    containerClass: '',
                                    templateUrl: 'tpl/cityEdit.html',
                                    resolve: load(['controllor/cityEditCtrl.js'])
                                })
                                .state('app.contact', {
                                    url: '/contact-list',
                                    pageName: 'contact',
                                    containerClass: '',
                                    templateUrl: 'tpl/contactList.html',
                                    resolve: load(['controllor/contactListCtrl.js'])
                                })
                                .state('app.contactAdd', {
                                    url: '/contact-add/:company_id',
                                    pageName: 'contact',
                                    containerClass: '',
                                    templateUrl: 'tpl/contactAdd.html',
                                    resolve: load(['controllor/contactAddCtrl.js', 'controllor/companyAddCtrl.js'])
                                })
                                .state('app.contactEdit', {
                                    url: '/contact-edit/:id/:company_id',
                                    pageName: 'contact',
                                    containerClass: '',
                                    templateUrl: 'tpl/contactEdit.html',
                                    resolve: load(['controllor/contactEditCtrl.js', 'controllor/companyAddCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.transportAdd', {
                                    url: '/transport-save',
                                    pageName: 'transport',
                                    containerClass: '',
                                    templateUrl: 'tpl/transportAdd.html',
                                    resolve: load(['controllor/transportAddCtrl.js'])
                                })
                                .state('app.transportEdit', {
                                    url: '/transport-edit/:id',
                                    pageName: 'transport',
                                    containerClass: '',
                                    templateUrl: 'tpl/transportEdit.html',
                                    resolve: load(['controllor/transportEditCtrl.js'])
                                })
                                .state('app.transport', {
                                    url: '/transport-list',
                                    pageName: 'transport',
                                    containerClass: '',
                                    templateUrl: 'tpl/transportList.html',
                                    resolve: load(['controllor/transportListCtrl.js'])
                                })
                                .state('app.deliveryInstructionAdd', {
                                    url: '/deliveryInstruction-save',
                                    pageName: 'deliveryInstruction',
                                    containerClass: '',
                                    templateUrl: 'tpl/deliveryInstructionAdd.html',
                                    resolve: load(['controllor/deliveryInstructionAddCtrl.js'])
                                })
                                .state('app.deliveryInstructionEdit', {
                                    url: '/deliveryInstruction-edit/:id',
                                    pageName: 'deliveryInstruction',
                                    containerClass: '',
                                    templateUrl: 'tpl/deliveryInstructionEdit.html',
                                    resolve: load(['controllor/deliveryInstructionEditCtrl.js'])
                                })
                                .state('app.deliveryInstruction', {
                                    url: '/deliveryInstruction-list',
                                    pageName: 'deliveryInstruction',
                                    containerClass: '',
                                    templateUrl: 'tpl/deliveryInstructionList.html',
                                    resolve: load(['controllor/deliveryInstructionListCtrl.js'])
                                })
                                .state('app.areawisesales', {
                                    url: '/areawisesales-report',
                                    pageName: 'areawisesales',
                                    containerClass: '',
                                    templateUrl: 'tpl/areawisesales.html',
                                    resolve: load(['controllor/areawisesalesCtrl.js'])
                                })
                                .state('app.bonusReportList', {
                                    url: '/bonus-report-list',
                                    pageName: 'bonusReportList',
                                    containerClass: '',
                                    templateUrl: 'tpl/bonusReportList.html',
                                    resolve: load(['controllor/bonusReportListCtrl.js'])
                                })
                                .state('app.bonusReportAdd', {
                                    url: '/bonus-report-add',
                                    pageName: 'bonusReportAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/bonusReportAdd.html',
                                    resolve: load(['controllor/bonusReportAddCtrl.js'])
                                })
                                .state('app.empSalary', {
                                    url: '/employee-salary-list',
                                    pageName: 'empSalaryList',
                                    containerClass: '',
                                    templateUrl: 'tpl/empSalaryList.html',
                                    resolve: load(['controllor/empSalaryListCtrl.js'])
                                })
                                .state('app.employee', {
                                    url: '/employee-list',
                                    pageName: 'employee',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeList.html',
                                    resolve: load(['controllor/employeeListCtrl.js'])
                                })
                                .state('app.employeeAdd', {
                                    url: '/employee-add',
                                    pageName: 'employee',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeAdd.html',
                                    resolve: load(['controllor/employeeAddCtrl.js'])
                                })
                                .state('app.employeeEdit', {
                                    url: '/employee-edit/:id',
                                    pageName: 'employee',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeEdit.html',
                                    resolve: load(['controllor/employeeEditCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                //                                .state('app.location1', {
                                //                                    url: '/location1',
                                //                                    pageName: 'location1',
                                //                                    containerClass: '',
                                //                                    templateUrl: 'tpl/location.html',
                                //                                    resolve: load(['controllor/ExampleController.js'])
                                //                                })
                                //                                .state('app.location', {
                                //                                    url: '/location',
                                //                                    pageName: 'location',
                                //                                    containerClass: '',
                                //                                    templateUrl: 'tpl/location_new.html',
                                //                                    resolve: load(['controllor/locationCtrl.js'])
                                //                                })
                                //                                .state('app.location2', {
                                //                                    url: '/location2',
                                //                                    pageName: 'location',
                                //                                    containerClass: '',
                                //                                    templateUrl: 'tpl/location_new2.html',
                                //                                    resolve: load(['controllor/locationCtrl2.js'])
                                //                                })
                                .state('app.billofmaterial', {
                                    url: '/bom-list',
                                    pageName: 'bom-list',
                                    containerClass: '',
                                    templateUrl: 'tpl/production-task.html',
                                    resolve: load(['controllor/productiontaskCtrl.js'])
                                })
                                .state('app.billofmaterialAdd', {
                                    url: '/bom-add',
                                    pageName: 'bom-add',
                                    containerClass: '',
                                    templateUrl: 'tpl/productiontask-add.html',
                                    resolve: load(['controllor/productiontaskAddCtrl.js'])
                                })
                                .state('app.billofmaterialEdit', {
                                    url: '/bom-edit/:id',
                                    pageName: 'bom-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/productiontask-edit.html',
                                    resolve: load(['controllor/productiontaskEditCtrl.js'])
                                })
                                .state('app.empTaskType', {
                                    url: '/employee-TaskType',
                                    pageName: 'empTaskType',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskTypeList.html',
                                    resolve: load(['controllor/taskTypeListCtrl.js'])
                                })
                                .state('app.empTaskTypeAdd', {
                                    url: '/employee-task-add',
                                    pageName: 'empTaskTypeAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskTypeAdd.html',
                                    resolve: load(['controllor/taskTypeAddCtrl.js'])
                                })
                                .state('app.empTaskTypeEdit', {
                                    url: '/employee-task-edit/:id',
                                    pageName: 'empTaskTypeEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskTypeEdit.html',
                                    resolve: load(['controllor/taskTypeEditCtrl.js'])
                                })
                                .state('app.employeeteamlist', {
                                    url: '/employeeteam-list',
                                    pageName: 'employeeteamlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeteamList.html',
                                    resolve: load(['controllor/employeeteamListCtrl.js'])
                                })
                                .state('app.employeeteamAdd', {
                                    url: '/employeeteam-add',
                                    pageName: "employeeteamAdd",
                                    templateUrl: 'tpl/employeeteamAdd.html',
                                    resolve: load(['controllor/employeeteamAddCtrl.js'])
                                })
                                .state('app.employeeteamEdit', {
                                    url: '/employeeteam-modify/:id',
                                    pageName: 'employeeteamEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/employeeteamEdit.html',
                                    resolve: load(['controllor/employeeteamEditCtrl.js'])
                                })
                                .state('app.production', {
                                    url: '/production-list',
                                    pageName: 'productionlist',
                                    containerClass: '',
                                    templateUrl: 'tpl/productionList.html',
                                    resolve: load(['controllor/productionListCtrl.js'])
                                })
                                .state('app.productionAdd', {
                                    url: '/production-add',
                                    pageName: "productionAdd",
                                    templateUrl: 'tpl/productionAdd.html',
                                    resolve: load(['controllor/productionAddCtrl.js'])
                                })
                                .state('app.productionEdit', {
                                    url: '/production-modify/:id',
                                    pageName: 'productionEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/productionEdit.html',
                                    resolve: load(['controllor/productionEditCtrl.js'])
                                })
                                .state('app.anniversaryreport', {
                                    url: '/dob-anniversary-report',
                                    pageName: 'anniversaryreport',
                                    containerClass: '',
                                    templateUrl: 'tpl/customeraniReport.html',
                                    resolve: load(['controllor/customeraniReportCtrl.js'])
                                })
                                .state('app.grnreport', {
                                    url: '/grnreport',
                                    pageName: 'grnreport',
                                    containerClass: '',
                                    templateUrl: 'tpl/grnreport.html',
                                    resolve: load(['controllor/grnreportCtrl.js'])
                                })
                                .state('app.externalwiseproduct', {
                                    url: '/externalwiseproduct',
                                    pageName: 'externalwiseproduct',
                                    containerClass: '',
                                    templateUrl: 'tpl/externalwiseProductReport.html',
                                    resolve: load(['controllor/externalwiseProductReportCtrl.js'])
                                })
                                .state('app.receivedqty', {
                                    url: '/received-qty',
                                    pageName: 'receivedqty',
                                    containerClass: '',
                                    templateUrl: 'tpl/receivedQtyReport.html',
                                    resolve: load(['controllor/receivedQtyReportCtrl.js'])
                                })
                                .state('app.expectedqty', {
                                    url: '/expected-qty',
                                    pageName: 'expectedqty',
                                    containerClass: '',
                                    templateUrl: 'tpl/expectedQtyReport.html',
                                    resolve: load(['controllor/expectedQtyReportCtrl.js'])
                                })
                                .state('app.goodsReceivedNoteList', {
                                    url: '/goods-received-note-list',
                                    pageName: "goodsReceivedNoteList",
                                    templateUrl: 'tpl/grnList.html',
                                    resolve: load(['controllor/grnListCtrl.js'])
                                })
                                .state('app.goodsReceivedNoteAdd', {
                                    url: '/goods-received-note-add',
                                    pageName: "goodsReceivedNoteAdd",
                                    templateUrl: 'tpl/grnAdd.html',
                                    resolve: load(['controllor/grnAddCtrl.js'])
                                })
                                .state('app.goodsReceivedNoteEdit', {
                                    url: '/goods-received-note-edit/:id',
                                    pageName: "goodsReceivedNoteEdit",
                                    templateUrl: 'tpl/grnEdit.html',
                                    resolve: load(['controllor/grnEditCtrl.js'])
                                })
                                .state('app.location', {
                                    url: '/location',
                                    pageName: 'location',
                                    containerClass: '',
                                    templateUrl: 'tpl/location.html',
                                    resolve: load(['controllor/locationCtrl.js'])
                                })
                                .state('app.attendance', {
                                    url: '/attendance',
                                    pageName: 'attendance',
                                    containerClass: '',
                                    templateUrl: 'tpl/attendanceList.html',
                                    resolve: load(['controllor/attendanceListCtrl.js', 'fileupload', 'photo-gallery', 'cloud-image-loader', 'photo-gallery'])
                                })
                                .state('attendanceLocation', {
                                    url: '/attendanceLocation/:value',
                                    pageName: 'attendanceLocation',
                                    containerClass: 'print-page-top',
                                    templateUrl: 'tpl/attendanceListLocation.html',
                                    resolve: load(['controllor/attendanceListLocationCtrl.js'])
                                })
                                .state('app.acodesettings', {
                                    url: '/acode-list',
                                    pageName: 'acodelist',
                                    containerClass: '',
                                    templateUrl: 'tpl/acodeList.html',
                                    resolve: load(['controllor/acodeListCtrl.js'])
                                })
                                .state('app.acodeAdd', {
                                    url: '/acode-add',
                                    pageName: "acodeadd",
                                    templateUrl: 'tpl/acodeAdd.html',
                                    resolve: load(['controllor/acodeAddCtrl.js'])
                                })
                                .state('app.acodeEdit', {
                                    url: '/acode-modify/:id',
                                    pageName: 'acodeedit',
                                    containerClass: '',
                                    templateUrl: 'tpl/acodeEdit.html',
                                    resolve: load(['controllor/acodeEditCtrl.js'])
                                })
                                .state('app.advanceAdd', {
                                    url: '/advanceAdd',
                                    pageName: 'advanceAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/advanceAdd.html',
                                    resolve: load(['controllor/advanceAddCtrl.js'])
                                })
                                .state('app.advanceList', {
                                    url: '/advanceList',
                                    pageName: 'advanceList',
                                    containerClass: '',
                                    templateUrl: 'tpl/advanceList.html',
                                    resolve: load(['controllor/advanceListCtrl.js'])
                                })
                                .state('app.advanceEdit', {
                                    url: '/advanceEdit/:id',
                                    pageName: 'advanceEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/advanceEdit.html',
                                    resolve: load(['controllor/advanceEditCtrl.js'])
                                })
                                .state('app.deductionSingleAdd', {
                                    url: '/deduction-add',
                                    pageName: 'deductionSingleAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/deductionSingleAdd.html',
                                    resolve: load(['controllor/deductionSingleAddCtrl.js'])
                                })
                                .state('app.deductionAdd', {
                                    url: '/deductionAdd',
                                    pageName: 'deductionAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/deductionAdd.html',
                                    resolve: load(['controllor/deductionAddCtrl.js'])
                                })
                                .state('app.deductionList', {
                                    url: '/deductionList',
                                    pageName: 'deductionList',
                                    containerClass: '',
                                    templateUrl: 'tpl/deductionList.html',
                                    resolve: load(['controllor/deductionListCtrl.js'])
                                })
                                .state('app.deductionEdit', {
                                    url: '/deductionEdit/:id',
                                    pageName: 'deductionEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/deductionEdit.html',
                                    resolve: load(['controllor/deductionEditCtrl.js'])
                                })
                                .state('app.bonus', {
                                    url: '/bonus-list',
                                    pageName: 'bonuslist',
                                    containerClass: '',
                                    templateUrl: 'tpl/bonusList.html',
                                    resolve: load(['controllor/bonusListCtrl.js'])
                                })
                                .state('app.bonusAdd', {
                                    url: '/bonus-add',
                                    pageName: "bonusadd",
                                    templateUrl: 'tpl/bonusAdd.html',
                                    resolve: load(['controllor/bonusAddCtrl.js'])
                                })
                                .state('app.bonusEdit', {
                                    url: '/bonus-modify/:id',
                                    pageName: 'bonusedit',
                                    containerClass: '',
                                    templateUrl: 'tpl/bonusEdit.html',
                                    resolve: load(['controllor/bonusEditCtrl.js'])
                                })
                                .state('app.empSalaryList', {
                                    url: '/salarypreparation',
                                    pageName: 'salaryreport',
                                    containerClass: '',
                                    templateUrl: 'tpl/empSalary.html',
                                    resolve: load(['controllor/empSalaryCtrl.js'])
                                })
                                .state('app.esireport', {
                                    url: '/esi-report',
                                    pageName: 'esireport',
                                    containerClass: '',
                                    templateUrl: 'tpl/esiReport.html',
                                    resolve: load(['controllor/esiReportCtrl.js'])
                                })
                                .state('app.empSalaryView', {
                                    url: '/salaryview/:id',
                                    pageName: 'salaryview',
                                    containerClass: '',
                                    templateUrl: 'tpl/empSalaryView.html',
                                    resolve: load(['controllor/empSalaryViewCtrl.js'])
                                })
                                .state('app.taskManagement', {
                                    url: '/taskManagementList',
                                    pageName: 'taskManagement',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskManagementList.html',
                                    resolve: load(['controllor/taskManagementListCtrl.js'])
                                })
                                .state('app.taskManagementAdd', {
                                    url: '/taskManagementAdd/:deal_id',
                                    pageName: 'taskManagementAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskManagementAdd.html',
                                    resolve: load(['controllor/taskManagementAddCtrl.js'])
                                })
                                .state('app.taskManagementEdit', {
                                    url: '/taskManagementEdit/:id',
                                    pageName: 'taskManagementEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskManagementEdit.html',
                                    resolve: load(['controllor/taskManagementEditCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.servicerequest', {
                                    url: '/servicerequestList',
                                    pageName: 'servicerequest',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceRequestList.html',
                                    resolve: load(['controllor/serviceRequestListCtrl.js'])
                                })
                                .state('app.servicerequestAdd', {
                                    url: '/servicerequestAdd/',
                                    pageName: 'servicerequestAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceRequestAdd.html',
                                    resolve: load(['controllor/serviceRequestAddCtrl.js'])
                                })
                                .state('app.servicerequestEdit', {
                                    url: '/servicerequestEdit/:id',
                                    pageName: 'servicerequestEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/serviceRequestEdit.html',
                                    resolve: load(['controllor/serviceRequestEditCtrl.js', 'cloud-image-loader', 'fileupload', 'angularFileUpload', 'photo-gallery'])
                                })
                                .state('app.empTask', {
                                    url: '/empTask-list',
                                    pageName: 'empTask',
                                    containerClass: '',
                                    templateUrl: 'tpl/empTaskList.html',
                                    resolve: load(['controllor/empTaskListCtrl.js'])
                                })
                                .state('app.empTaskAdd', {
                                    url: '/empTask-add',
                                    pageName: 'empTaskAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/empTaskAdd.html',
                                    resolve: load(['controllor/empTaskAddCtrl.js'])
                                })
                                .state('app.empTaskEdit', {
                                    url: '/empTask-edit/:id',
                                    pageName: 'empTaskEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/empTaskEdit.html',
                                    resolve: load(['controllor/empTaskEditCtrl.js'])
                                })

                                .state('app.advancereport', {
                                    url: '/advance-report',
                                    pageName: 'advancereport',
                                    containerClass: '',
                                    templateUrl: 'tpl/advanceReport.html',
                                    resolve: load(['controllor/advanceReportCtrl.js'])
                                })
                                .state('app.productionreport', {
                                    url: '/production-report',
                                    pageName: 'productionreport',
                                    containerClass: '',
                                    templateUrl: 'tpl/productionReport.html',
                                    resolve: load(['controllor/productionReportCtrl.js'])
                                })
                                .state('app.stockReport', {
                                    url: '/stock-report',
                                    pageName: 'stockReport',
                                    containerClass: '',
                                    templateUrl: 'tpl/stockReport.html',
                                    resolve: load(['controllor/stockReportCtrl.js'])
                                })
                                .state('app.workorder', {
                                    url: '/manufacturing-order',
                                    pageName: "manufacturing-order",
                                    containerClass: '',
                                    templateUrl: 'tpl/workorderList.html',
                                    resolve: load(['controllor/workorderListCtrl.js'])
                                })
                                .state('app.workorderAdd', {
                                    url: '/manufacturing-order-add',
                                    pageName: "manufacturing-order-add",
                                    containerClass: '',
                                    templateUrl: 'tpl/workorderAdd.html',
                                    resolve: load(['controllor/workorderAddCtrl.js', 'controllor/newCustomerAddCtrl.js'])
                                })
                                .state('app.workorderEdit', {
                                    url: '/manufacturing-order-edit/:id',
                                    pageName: "manufacturing-order-edit",
                                    containerClass: '',
                                    templateUrl: 'tpl/workorderEdit.html',
                                    resolve: load(['controllor/workorderEditCtrl.js', 'controllor/newCustomerAddCtrl.js'])
                                })
                                .state('app.workorderView', {
                                    url: '/manufacturing-order-view/:id',
                                    pageName: "manufacturing-order-view",
                                    containerClass: '',
                                    templateUrl: 'tpl/workorderView.html',
                                    resolve: load(['controllor/workorderViewCtrl.js'])
                                })
                                .state('app.source', {
                                    url: '/source-list',
                                    pageName: 'source',
                                    containerClass: '',
                                    templateUrl: 'tpl/source.html',
                                    resolve: load(['controllor/sourceCtrl.js'])
                                })
                                .state('app.sourceadd', {
                                    url: '/source-save',
                                    pageName: 'source',
                                    containerClass: '',
                                    templateUrl: 'tpl/sourceadd.html',
                                    resolve: load(['controllor/sourceAddCtrl.js'])
                                })
                                .state('app.sourceedit', {
                                    url: '/source-modify/:id',
                                    pageName: 'source',
                                    containerClass: '',
                                    templateUrl: 'tpl/sourceedit.html',
                                    resolve: load(['controllor/sourceEditCtrl.js'])
                                })
                                .state('app.spareRequest', {
                                    url: '/spare-request',
                                    pageName: "spareRequest",
                                    templateUrl: 'tpl/salesorderList.html',
                                    resolve: load(['controllor/salesorderListCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.spareRequestAdd', {
                                    url: '/spare-request-add/:service_id',
                                    pageName: "spareRequest",
                                    templateUrl: 'tpl/salesorderAdd.html',
                                    resolve: load(['controllor/salesorderAddCtrl.js', 'controllor/newCustomerAddCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.expensetracker', {
                                    url: '/expense-tracker',
                                    pageName: "expenseTracker",
                                    templateUrl: 'tpl/expensetrackerList.html',
                                    resolve: load(['controllor/expensetrackerListCtrl.js'])
                                })
                                .state('app.expensetrackerAdd', {
                                    url: '/expense-tracker-add',
                                    pageName: "expenseTrackerAdd",
                                    templateUrl: 'tpl/expensetrackerAdd.html',
                                    resolve: load(['controllor/expensetrackerAddCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.expensetrackerEdit', {
                                    url: '/expense-tracker-edit/:id',
                                    pageName: "expenseTrackerEdit",
                                    templateUrl: 'tpl/expensetrackerEdit.html',
                                    resolve: load(['controllor/expensetrackerEditCtrl.js', 'fileupload', 'angularFileUpload', 'photo-gallery', 'cloud-image-loader'])
                                })
                                .state('app.expensetrackerView', {
                                    url: '/expense-tracker-view',
                                    pageName: "expenseTrackerView",
                                    templateUrl: 'tpl/expensetrackerView.html',
                                    resolve: load(['controllor/expensetrackerViewCtrl.js'])
                                })
                                .state('app.expensetrackerreport', {
                                    url: '/expense-tracker-report',
                                    pageName: 'expensetracker-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/expensetrackerReport.html',
                                    resolve: load(['controllor/expensetrackerReportCtrl.js'])
                                })
                                .state('app.crmdashboard', {
                                    url: '/crm-dashboard',
                                    pageName: 'dashboard',
                                    containerClass: 'crm-Dashboard',
                                    templateUrl: 'tpl/crmDashboard.html',
                                    resolve: load(['controllor/crmDashboardCtrl.js'])
                                })
                                .state('app.dailyactivityreport', {
                                    url: '/daily-activity-report',
                                    pageName: 'daily-activity-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/dailyActivityUserReport.html',
                                    resolve: load(['controllor/dailyActivityUserReportCtrl.js'])
                                })
                                .state('app.leadwithoutdeal', {
                                    url: '/lead-report',
                                    pageName: 'lead-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/leadReport.html',
                                    resolve: load(['controllor/leadReportCtrl.js'])
                                })
                                .state('app.inactivitydeal', {
                                    url: '/inactive-deal-report',
                                    pageName: 'inactive-deal-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/inactiveDealReport.html',
                                    resolve: load(['controllor/inactiveDealReportCtrl.js'])
                                })
                                .state('app.expentrackercategory', {
                                    url: '/expense-tracker-categroy-report',
                                    pageName: 'expense-tracker-categroy-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/expensetrackercategoryReport.html',
                                    resolve: load(['controllor/expensetrackercategoryReportCtrl.js'])
                                })
                                .state('app.spareRequestEdit', {
                                    url: '/spare-request-edit/:id',
                                    pageName: "spareRequest",
                                    templateUrl: 'tpl/salesorderEdit.html',
                                    resolve: load(['controllor/salesorderEditCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js', 'custom-textinput', 'custom-selectoption', 'custom-multiselect', 'custom-checkbox', 'custom-yes-or-no', 'custom-date'])
                                })
                                .state('app.spareRequestView', {
                                    url: '/spare-request-view/:id',
                                    pageName: "spareRequest",
                                    templateUrl: 'tpl/salesorderView.html',
                                    resolve: load(['controllor/salesorderViewCtrl.js', 'controllor/productselectCtrl.js', 'controllor/sendMailCtrl.js'])
                                })
                                .state('app.materialrequirement', {
                                    url: '/materialrequirement',
                                    pageName: 'material-requirement',
                                    containerClass: '',
                                    templateUrl: 'tpl/materialrequirement.html',
                                    resolve: load(['controllor/materialrequirementCtrl.js'])
                                })
                                .state('app.materialrequirementAdd', {
                                    url: '/materialrequirement-add',
                                    pageName: 'material-requirement-add',
                                    containerClass: '',
                                    templateUrl: 'tpl/materialrequirementAdd.html',
                                    resolve: load(['controllor/materialrequirementAddCtrl.js'])
                                })
                                .state('app.materialrequirementEdit', {
                                    url: '/materialrequirement-edit',
                                    pageName: 'material-requirement-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/materialrequirementEdit.html',
                                    resolve: load(['controllor/materialrequirementEditCtrl.js'])
                                })
                                .state('app.consolidatedacode', {
                                    url: '/consolidated-acode/',
                                    pageName: "consolidated-acode",
                                    templateUrl: 'tpl/consolidatedacodestatement.html',
                                    resolve: load(['controllor/consolidatedacodeStatementCtrl.js'])
                                })
                                .state('app.creditnote', {
                                    url: '/creditnote-list',
                                    pageName: "creditnote",
                                    templateUrl: 'tpl/creditnoteList.html',
                                    resolve: load(['controllor/creditnoteListCtrl.js'])
                                })
                                .state('app.creditnoteadd', {
                                    url: '/creditnote-add',
                                    pageName: "creditnote-add",
                                    templateUrl: 'tpl/creditnoteAdd.html',
                                    resolve: load(['controllor/creditnoteAddCtrl.js'])
                                })
                                .state('app.creditnoteview', {
                                    url: '/creditnote-view/:id',
                                    pageName: "creditnote-view",
                                    templateUrl: 'tpl/creditnoteView.html',
                                    resolve: load(['controllor/creditnoteViewCtrl.js'])
                                })
                                .state('app.debitnote', {
                                    url: '/debitnote-list',
                                    pageName: "debitnote",
                                    templateUrl: 'tpl/debitnoteList.html',
                                    resolve: load(['controllor/debitnoteCtrl.js'])
                                })
                                .state('app.debitnoteadd', {
                                    url: '/debitnote-add',
                                    pageName: "debitnote-add",
                                    templateUrl: 'tpl/debitnoteAdd.html',
                                    resolve: load(['controllor/debitnoteAddCtrl.js'])
                                })
                                .state('app.debitnoteview', {
                                    url: '/debitnote-view/:id',
                                    pageName: "debitnote-view",
                                    templateUrl: 'tpl/debitnoteView.html',
                                    resolve: load(['controllor/debitnoteViewCtrl.js'])
                                })
                                .state('app.amcView', {
                                    url: '/amc-view/:id',
                                    pageName: "amcView",
                                    templateUrl: 'tpl/amcView.html',
                                    resolve: load(['controllor/amcEditCtrl.js'])
                                })
                                .state('app.workcenter', {
                                    url: '/workcenter-list',
                                    pageName: "workcenter-list",
                                    templateUrl: 'tpl/workcenterList.html',
                                    resolve: load(['controllor/workcenterListCtrl.js'])
                                })
                                .state('app.workcenterAdd', {
                                    url: '/workcenter-add',
                                    pageName: "workcenter-add",
                                    templateUrl: 'tpl/workcenterAdd.html',
                                    resolve: load(['controllor/workcenterAddCtrl.js'])
                                })
                                .state('app.workcenterEdit', {
                                    url: '/workcenter-edit/:id',
                                    pageName: "workcenterEdit",
                                    templateUrl: 'tpl/workcenterEdit.html',
                                    resolve: load(['controllor/workcenterEditCtrl.js'])
                                })
                                .state('app.taskgroup', {
                                    url: '/taskgroup',
                                    pageName: 'taskgroup',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskgroup.html',
                                    resolve: load(['controllor/taskgroupListCtrl.js'])
                                })
                                .state('app.taskgroupAdd', {
                                    url: '/taskgroup-add',
                                    pageName: 'taskgroupAdd',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskgroupAdd.html',
                                    resolve: load(['controllor/taskgroupAddCtrl.js'])
                                })
                                .state('app.taskgroupEdit', {
                                    url: '/taskgroup-edit/:id',
                                    pageName: 'taskgroupEdit',
                                    containerClass: '',
                                    templateUrl: 'tpl/taskgroupEdit.html',
                                    resolve: load(['controllor/taskgroupEditCtrl.js'])
                                })
                                .state('app.salesorderpending', {
                                    url: '/salesorderpending-report',
                                    pageName: 'salesorderpending-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/salesorderpendingreport.html',
                                    resolve: load(['controllor/salesorderpendingreportCtrl.js'])
                                })
                                .state('requisitionPrint', {
                                    url: '/requisitionPrint/:id',
                                    pageName: 'requisitionPrint',
                                    containerClass: '',
                                    templateUrl: 'tpl/requisitionPrint.html',
                                    resolve: load(['controllor/workorderEditCtrl.js'])
                                })
                                .state('app.issuescreen', {
                                    url: '/issuescreen-list',
                                    pageName: 'issuescreen-list',
                                    containerClass: '',
                                    templateUrl: 'tpl/issuescreenList.html',
                                    resolve: load(['controllor/issuescreenListCtrl.js'])
                                })
                                 .state('app.issuescreenAdd', {
                                    url: '/issuescreen-add',
                                    pageName: 'issuescreen-add',
                                    containerClass: '',
                                    templateUrl: 'tpl/issuescreenAdd.html',
                                    resolve: load(['controllor/issuescreenAddCtrl.js'])
                                })
                                 .state('app.issuescreenEdit', {
                                    url: '/issuescreen-edit/:id',
                                    pageName: 'issuescreen-edit',
                                    containerClass: '',
                                    templateUrl: 'tpl/issuescreenEdit.html',
                                    resolve: load(['controllor/issuescreenEditCtrl.js'])
                                })
                                .state('app.customerbasedreport', {
                                    url: '/customerbased-report',
                                    pageName: 'customerbased-report',
                                    containerClass: '',
                                    templateUrl: 'tpl/customerBasedReport.html',
                                    resolve: load(['controllor/customerBasedReportCtrl.js'])
                                })
                                .state('app.department', {
                                    url: '/department-list',
                                    pageName: "department-list",
                                    templateUrl: 'tpl/departmentList.html',
                                    resolve: load(['controllor/departmentListCtrl.js'])
                                })
                                .state('app.departmentAdd', {
                                    url: '/department-add',
                                    pageName: "department-add",
                                    templateUrl: 'tpl/departmentAdd.html',
                                    resolve: load(['controllor/departmentAddCtrl.js'])
                                })
                                .state('app.departmentEdit', {
                                    url: '/department-edit/:id',
                                    pageName: "department-edit",
                                    templateUrl: 'tpl/departmentEdit.html',
                                    resolve: load(['controllor/departmentEditCtrl.js'])
                                })
                                .state('app.manufacturingworkorder', {
                                    url: '/manufacturing-work-order',
                                    pageName: "manufacturing-work-order",
                                    containerClass: '',
                                    templateUrl: 'tpl/manufacturingWorkorderList.html',
                                    resolve: load(['controllor/manufacturingWorkorderListCtrl.js'])
                                })
                                .state('app.manufacturingworkorderView', {
                                    url: '/manufacturing-work-order-view/:id',
                                    pageName: "manufacturing-work-order-view",
                                    containerClass: '',
                                    templateUrl: 'tpl/manufacturingWorkorderView.html',
                                    resolve: load(['controllor/assignWorkOrderCtrl.js'])
                                })
                                .state('app.assignWorkOrder', {
                                    url: '/assign-work-order',
                                    pageName: "assign-work-order",
                                    containerClass: '',
                                    templateUrl: 'tpl/assignWorkOrder.html',
                                    resolve: load(['controllor/assignWorkOrderCtrl.js'])
                                })
                                .state('app.producttype', {
                                    url: '/product-type-list',
                                    pageName: "product-type-list",
                                    templateUrl: 'tpl/productTypeList.html',
                                    resolve: load(['controllor/productTypeListCtrl.js'])
                                })
                                .state('app.producttypeAdd', {
                                    url: '/product-type-add',
                                    pageName: "product-type-add",
                                    templateUrl: 'tpl/productTypeAdd.html',
                                    resolve: load(['controllor/productTypeAddCtrl.js'])
                                })
                                .state('app.producttypeEdit', {
                                    url: '/product-type-edit/:id',
                                    pageName: "product-type-edit",
                                    templateUrl: 'tpl/productTypeEdit.html',
                                    resolve: load(['controllor/productTypeEditCtrl.js'])
                                })
                                .state('app.moworkorder', {
                                    url: '/internal-work-order-report',
                                    pageName: "internal-work-order-report",
                                    containerClass: '',
                                    templateUrl: 'tpl/internalworkorderreport.html',
                                    resolve: load(['controllor/internalworkorderreportCtrl.js'])
                                })
                                .state('app.revenuereport', {
                                    url: '/revenue-report',
                                    pageName: "revenue-report",
                                    containerClass: '',
                                    templateUrl: 'tpl/revenuereport.html',
                                    resolve: load(['controllor/revenuereportCtrl.js'])
                                })
                                .state('app.buyermachine', {
                                    url: '/buyermachine-details',
                                    pageName: "buyermachine-details",
                                    templateUrl: 'tpl/itemhistoryreport.html',
                                    resolve: load(['controllor/itemHistoryReportCtrl.js'])
                                })
                                .state('app.qualitycontrolpoints', {
                                    url: '/quality-control-points',
                                    pageName: "quality-control-points",
                                    templateUrl: 'tpl/qualityControlPointsList.html',
                                    resolve: load(['controllor/qualityCtrlPointsListCtrl.js'])
                                })
                                .state('app.qualitycontrolpointsAdd', {
                                    url: '/quality-control-points-add',
                                    pageName: "quality-control-points-add",
                                    templateUrl: 'tpl/qualitycontrolPointsAdd.html',
                                    resolve: load(['controllor/qualityCtrlPointsAddCtrl.js'])
                                })
                                .state('app.qualitycontrolpointsEdit', {
                                    url: '/quality-control-points-edit/:id',
                                    pageName: "quality-control-points-edit",
                                    templateUrl: 'tpl/qualityControlPointsEdit.html',
                                    resolve: load(['controllor/qualityCtrlPointsEditCtrl.js'])
                                })
                                .state('app.conversionjournal', {
                                    url: '/conversion-journal',
                                    pageName: "conversion-journal",
                                    templateUrl: 'tpl/conversionjournal.html',
                                    resolve: load(['controllor/conversionjournalListCtrl.js'])
                                })
                                .state('app.conversionjournalAdd', {
                                    url: '/conversion-journal-add',
                                    pageName: "conversion-journal-add",
                                    templateUrl: 'tpl/conversionjournalAdd.html',
                                    resolve: load(['controllor/conversionjournalAddCtrl.js'])
                                })
                                .state('app.conversionjournalEdit', {
                                    url: '/conversion-journal-edit/:id',
                                    pageName: "conversion-journal-edit",
                                    templateUrl: 'tpl/conversionjournalEdit.html',
                                    resolve: load(['controllor/conversionjournalEditCtrl.js'])
                                })
                                .state('app.mobasedreport', {
                                    url: '/manufacuringorder-report/',
                                    pageName: "manufacuringorder-report",
                                    templateUrl: 'tpl/manufacuringorderbasedreport.html',
                                    resolve: load(['controllor/manufacturingorderbasedreportCtrl.js'])
                                })
                                .state('app.mointernalreceivedreport', {
                                    url: '/manufacuringorder-received-qty-report',
                                    pageName: "manufacuringorder-report",
                                    templateUrl: 'tpl/mointernalreceivedreport.html',
                                    resolve: load(['controllor/mointernalreceivedreportCtrl.js'])
                                })
                                  .state('app.itemwiseissuedreport', {
                                    url: '/itemwiseissued-details',
                                    pageName: "itemwiseissued-report",
                                    templateUrl: 'tpl/itemwiseissuedreport.html',
                                    resolve: load(['controllor/itemwiseissuedreportCtrl.js'])
                                })
                                 .state('app.manufacturingorderwisereport', {
                                    url: '/manufacturing-details',
                                    pageName: "manufacturing-report",
                                    templateUrl: 'tpl/manufacturingorderwisereport.html',
                                    resolve: load(['controllor/manufacturingorderwisereportCtrl.js'])
                                })
                        function load(srcs, callback) {
                            return {
                                deps: ['$ocLazyLoad', '$q',
                                    function($ocLazyLoad, $q) {
                                        var deferred = $q.defer();
                                        var promise = false;
                                        srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                                        if (!promise) {
                                            promise = deferred.promise;
                                        }
                                        angular.forEach(srcs, function(src) {
                                            promise = promise.then(function() {
                                                if (JQ_CONFIG[src]) {
                                                    return $ocLazyLoad.load(JQ_CONFIG[src]);
                                                }
                                                //lazy load the application component
                                                if (COMPONENT_CONFIG[src]) {
                                                    return $ocLazyLoad.load(COMPONENT_CONFIG[src]);
                                                }
                                                angular.forEach(MODULE_CONFIG, function(module) {
                                                    if (module.name == src) {
                                                        name = module.name;
                                                    } else {
                                                        name = src;
                                                    }
                                                });
                                                return $ocLazyLoad.load(name);
                                            });
                                        });
                                        deferred.resolve();
                                        return callback ? promise.then(function() {
                                            return callback();
                                        }) : promise;
                                    }]
                            }
                        }

                        $provide.decorator('$uiViewScroll', function($delegate) {
                            return function(uiViewElement) {
                                $('html,body').animate({
                                    scrollTop: uiViewElement.offset().top
                                }, 500);
                            };
                        });
                    }
                ]
                );
