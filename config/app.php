<?php

return [
/*
  |--------------------------------------------------------------------------
  | Encryption Key
  |--------------------------------------------------------------------------
  |
  | This key is used by the Illuminate encrypter service and should be set
  | to a random, 32 character string, otherwise these encrypted strings
  | will not be safe. Please do this before deploying an application!
  |
 */

'key' => env('APP_KEY', 'SomeRandomString!!!'),
 'cipher' => 'AES-256-CBC',
 /*
  |--------------------------------------------------------------------------
  | Application Locale Configuration
  |--------------------------------------------------------------------------
  |
  | The application locale determines the default locale that will be used
  | by the translation service provider. You are free to set this value
  | to any of the locales which will be supported by the application.
  |
 */
'locale' => env('APP_LOCALE', 'en'),
 'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),
 'APP_ENV' => 'local',
 'APP_DEBUG' => true,
 'APP_KEY' => '',
 "DB_CONNECTION" => "mysql",
 "DB_HOST" => "localhost",
 "SSO_DB_DATABASE" => "lbssso",
 "SSO_DB_USERNAME" => "root",
 "SSO_DB_PASSWORD" => "",
 /*
  * User Invite Email 
  */   
 "USER_INVITE_URL" => "http://lbssso/public/businessinvite?domain={{domain}}&user={{user}}",
 "GET_BU_DB_PATH" => "http://lbssso/buconfig",
 "SSO_LOGOUT"=>"http://lbssso/public/logout",
 /*
  * Cache 
  */
 "CACHE_DRIVER" => "memcached",
 "QUEUE_DRIVER" => "sync",

/*
  * Google Cloud Storage Detail
  */    
"SERVICE_ACCOUNT_PKCS12_FILE_PATH" => "E:/stutzen/lbs/stutzenme-55d6bec60d20.p12",
"SERVICE_ACCOUNT_EMAIL" => "stutzendev@stutzenme.iam.gserviceaccount.com",
"EXPEIRATION" => "60000",
"BUCKET_NAME" => "albumzen",

"MAIL_DRIVER" => "sendgrid",
"SENDGRID_API_KEY" => "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",

"SMS_API_KEY" => "A8346b505a8d3554f83efd4defb46eee0",
"SMS_SENDER" => "STUTZN",

"APP_STAGE" => "LIVE"
];
