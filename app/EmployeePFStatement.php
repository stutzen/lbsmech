<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeePFStatement extends Model {
    
    protected $table = 'tbl_employee_pf_statement' ;
    protected $fillable = ['emp_id','emp_salary_id','days','salary_amount','pf_percentage','pf_amount', 
        'total_pf_amount','employer_pf_percentage','employer_pf_amount','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

