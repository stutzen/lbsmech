<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Quotes
 *
 * @author Deepa
 */
class Quote extends Model {
    //put your code here
    protected  $table ="tbl_quote";
    protected  $fillable = [ 'contact_id','prefix','date','validuntil','datepaid','subtotal','discount_mode','discount_amount','credit_amount'
        ,'tax_amount','total_amount','status','paymentmethod','notes','is_active','quote_id','customer_address','created_at','created_by',
        'updated_at','updated_at','date_sent','date_accepted','advance_amount','discount_percentage','tax_id','round_off','deal_id','deal_name','payee'
        ,'consignee','insurance_charge','packing_charge','expiry_date','assigned_to','company_id'];
     protected $dates = [ 'created_at','updated_at','date','validuntil','date_sent','date_accepted' ];
        
        
   
}

?>
