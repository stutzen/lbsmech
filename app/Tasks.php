<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tasks
 *
 * @author Deepa
 */
class Tasks extends Model {
    //put your code here
    protected  $table = 'tbl_tasks';
    protected $fillable = ['name','cid','oid','o','due','remind','notes','status','pid','ttime','sub'];
}

?>
