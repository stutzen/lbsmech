<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Company extends Model {
    
    protected $table = 'tbl_company' ;
    protected $fillable = ['name','address','district','city_id','state_id','country_id','website','pin_code'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>