<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
class TaskActivityComments extends Model {
    
    protected $table = 'tbl_task_activity_comments' ;
    protected $fillable = ['task_activity_id','message','is_active','created_by','updated_by','comments'];
    protected $dates = ['created_at', 'updated_at'];
   
}