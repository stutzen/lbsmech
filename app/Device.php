<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Device extends Model{
    
     protected $table = 'tbl_device_info';
     
     protected $fillable = [
        'name','device_code','short_code' ];
}
