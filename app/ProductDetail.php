<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Item
 *
 * @author Deepa
 */
class ProductDetail extends Model {
     protected $table = 'tbl_product_detail';
     protected $fillable = ['is_active','created_by','updated_by','comments','product_id','qty','type','width'
         ,'thickners','dia','af','square','length','gauge','color','brand','pitch','product_type_id'];
     protected $dates = ['created_at','updated_at'];
    //put your code here
}

?>
