<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class WorkOrder extends Model {
    
    protected $table = 'tbl_work_order' ;
    protected $fillable = ['mo_id','customer_id','type','is_active','comments','work_order_input_id','status','work_center_id','original_production_qty','produced_qty'
        ,'task_group_detail_id','updated_by','created_by','start_date','amount','end_date','start_time','end_time'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
