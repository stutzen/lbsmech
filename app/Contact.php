<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Contact extends Model {
    
    protected $table = 'tbl_contact' ;
    protected $fillable = ['fname', 'lname', 'is_active','created_by','updated_by','email','phone','company_id','comments','notes','source_id','lead_type_id','pin_code','contact_owner_id'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>
