<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdvPaymentAllotment
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class AdvPaymentAllotment extends Model {
    
    protected $table = 'tbl_adv_payment_allotment' ;
    protected $fillable = ['adv_payment_id', 'payment_id','invoice_id','sales_order_id','customer_id','amount', 'is_active',
        'created_by','updated_by','comments'];
    protected $dates = ['created_at', 'updated_at','date'];
}
