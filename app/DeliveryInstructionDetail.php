<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeliveryInstructionDetail
 *
 * @author Stutzen Admin
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
class DeliveryInstructionDetail extends Model {

    protected $table = 'tbl_delivery_instruction_detail';

   
    protected $fillable = [
        'delivery_instruction_id', 'ref_id', 'ref_type', 'ref_date', 'customer_id','freight','driver_beta','net_weight','gross_weight',
        'volume', 'no_of_pkts','qty', 'unloading', 'drop_seq','billing_unit','store', 'created_by', 'updated_by', 'is_active', 'comments','consignee','row_total'
    ];
    protected $dates = ['created_at', 'updated_at'];

}
