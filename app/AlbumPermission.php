<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumPermission extends Model {

    protected $table = 'tbl_album_permission';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'album_id', 'album_code', 'customer_id', 'created_by_name','updated_by_name',
        'created_by', 'updated_by', 'is_active'   
    ];
    protected $dates = [
        'created_at',
        'updated_at'
        ];

//    /**
//     * The attributes excluded from the model's JSON form.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        
//    ];
}
