<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserRole
 *
 * @author Deepa
 */
class Role extends Model {
    //put your code here
    protected  $table ='tbl_role';
    protected  $fillable = ['name','comments','is_active','created_by','updated_by'];
    protected $dates = ['updated_at','created_at'];
}

?>
