<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmployeeProductionDetail
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeProductionDetail extends Model {
       protected $table = 'tbl_emp_production_detail' ;
    protected $fillable = ['emp_production_id','name','task_id','task_name','output_pdt_id','output_pdt_name','output_pdt_uom_id','output_pdt_uom'
        ,'poduction_qty','production_unit_price','amount','updated_by','created_by','is_active','comments'];
    protected $dates = ['created_at', 'updated_at'];
}
