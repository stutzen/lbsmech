<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Bom extends Model {
    
    protected $table = 'tbl_bom' ;
    protected $fillable = ['name','prev_bom_id','output_pdt_id','output_pdt_name','output_pdt_uom','updated_by','created_by','output_qty',
        'unit_production_price','is_active','comments','output_pdt_uom_id','product_for_id','product_for_name','bom_code','task_group_id'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
