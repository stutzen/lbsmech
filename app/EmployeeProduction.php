<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmployeeProduction
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeProduction extends Model {
       protected $table = 'tbl_emp_production' ;
    protected $fillable = ['emp_id','emp_name','emp_code','amount','updated_by','created_by','is_active','comments','is_present','emp_team_id','ot_amount'];
    protected $dates = ['created_at', 'updated_at','date'];
}
