<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Issue extends Model{
    protected $table='tbl_issue';
    protected $fillable=['responsible_by','mo_id','consumed_qty','issued_qty','balance_qty','status','date','is_active','comments'];   
    protected $dates=['created_at','updated_at'];
}
