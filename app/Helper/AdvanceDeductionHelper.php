<?php

namespace App\Helper;
use Carbon\Carbon;

use DB;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountHelper
 *
 * @author Deepa
 */
class AdvanceDeductionHelper {
    //put your code here
    public static function balanceDecrease ($id , $amount){
      
 DB::table('tbl_employee_advance')->where('id', $id)
         ->update(['balance_amt' =>DB::raw('balance_amt -'. $amount)]);
         
 DB::table('tbl_employee_advance')->where('id', $id)
         ->update(['total_deduction_amt' =>DB::raw('total_deduction_amt +'. $amount)]);         
      
    }
    public static function balanceIncrease ($id ,$amount){
               
 DB::table('tbl_employee_advance')->where('id', $id)
         ->update(['balance_amt' =>DB::raw('balance_amt +'. $amount)]);
         
 DB::table('tbl_employee_advance')->where('id', $id)
         ->update(['total_deduction_amt' =>DB::raw('total_deduction_amt -'. $amount)]);         
      
    }
}

?>
