<?php

namespace App\Helper;

use DB;

class ReportHelper {

    public function employeeSalary($request) {

        $emp_id = $request->input('emp_id');
        $team_id = $request->input('team_id');
        $show_all = $request->input('show_all', '');
        $month = $request->input('month', '');
        $salary_type = $request->input('salary_type');
        $payroll_type = $request->input('payroll_type');

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_employee as e')
                ->leftjoin('tbl_emp_production as ep', 'e.id', '=', 'ep.emp_id')
                ->leftjoin('tbl_emp_production_detail as epd', 'ep.id', '=', 'epd.emp_production_id')
                ->leftjoin('tbl_emp_production_attendance as epa', 'ep.id', '=', 'epa.emp_production_id')
                ->select('ep.emp_id', 'ep.emp_code as Code', 'ep.emp_name as Name'
                        , DB::raw('(sum(coalesce(epd.production_qty,0))+sum(coalesce(epa.production_qty,0))) as Qty'), 'e.salary_type'
                        , DB::raw('(sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))  as Wages')
                        ,DB::raw('"fixed_allowance_amount" as Allowance'),  DB::raw('"ot_amount" as Other'), DB::raw('"total_pay" as Total_Pay')
                        , 'e.payroll_type', 'e.max_leave_count', 'ep.emp_team_id', 'e.esi_percentage', 'e.pf_percentage', 'ep.status'
                        , DB::raw('ROUND((CASE WHEN e.salary_type="piece" THEN ((CASE WHEN (sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0))) >= 100 THEN (100*(coalesce(e.pf_percentage,0))/100) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.pf_percentage,0))/100) END)) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.pf_percentage,0))/100) END),2) AS EPF')
                        , DB::raw('ROUND((CASE WHEN e.salary_type="piece" THEN ((CASE WHEN (sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0))) >= 100 THEN (100*(coalesce(e.esi_percentage,0))/100) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.esi_percentage,0))/100) END)) ELSE  ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.esi_percentage,0))/100) END),2)  AS ESI')
                        , DB::raw('"adv_deduction_amt" as Advance')
                        , DB::raw('(CASE WHEN e.salary_type="piece" THEN ((CASE WHEN (sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0))) >= 100 THEN (100*(coalesce(e.esi_percentage,0))/100) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.esi_percentage,0))/100) END)+(CASE WHEN (sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0))) >= 100 THEN (100*(coalesce(e.pf_percentage,0))/100) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.pf_percentage,0))/100) END)) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.esi_percentage,0))/100) + ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.pf_percentage,0))/100) END)  as Total_Deduction')
                        , 'e.salary_amount As Net_Pay'
                )
                ->where('ep.is_active', '=', 1)
                ->where('ep.is_present', '=', 1)
                ->where('ep.status', '=', 'initiated')
                ->groupby('ep.emp_id');

        if (strcasecmp($salary_type, 'attendance') == 0) {
            $builder->where('e.salary_type', '=', 'attendance');
        } elseif (strcasecmp($salary_type, 'piece') == 0) {
            $builder->where('e.salary_type', '=', 'piece');
        }

        if (strcasecmp($payroll_type, 'monthly') == 0) {
            $builder->where('e.payroll_type', '=', 'monthly');
        } elseif (strcasecmp($payroll_type, 'daily') == 0) {
            $builder->where('e.payroll_type', '=', 'daily');
        } elseif (strcasecmp($payroll_type, 'weekly') == 0) {
            $builder->where('e.payroll_type', '=', 'weekly');
        }

        if (!empty($from_date)) {
            $builder->whereDate('ep.date', '>=', $from_date);
        }

        if (!empty($to_date)) {
            $builder->whereDate('ep.date', '<=', $to_date);
        }

        if (!empty($emp_id)) {
            $builder->where('e.id', '=', $emp_id);
        }
        if (!empty($team_id)) {
            $builder->where('e.team_id', '=', $team_id);
        }
        $resVal['total'] = $builder->count();
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        $from_date1 = strtotime($from_date);
        $to_date1 = strtotime($to_date);

        $datediff = $to_date1 - $from_date1;

        $days = floor($datediff / (60 * 60 * 24));
        $count = 0;
        foreach ($collection as $t) {
            $count ++;
            $c = $t->emp_id;
            $c1 = $t->emp_team_id;
            $advance = $t->Total_Deduction;

            $detail = DB::table('tbl_emp_production_attendance as epa')
                    ->leftjoin('tbl_emp_production as em', 'em.id', '=', 'epa.emp_production_id')
                    ->select(DB::raw('epa.id'), DB::raw('epa.task_id'), DB::raw('epa.task_name'), DB::raw('epa.output_pdt_id'), DB::raw('epa.output_pdt_name'), DB::raw('epa.emp_production_id'), DB::raw('epa.production_qty'), DB::raw('epa.production_unit_price'), DB::raw('epa.amount'))
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '<=', $to_date)
                    ->where('em.is_active', '=', 1)
                    ->where('em.is_present', '=', 1)
                    ->where('em.status', '=', 'initiated')
                    ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "));



            $bulider1 = DB::table('tbl_emp_production_detail as epd')
                    ->leftjoin('tbl_emp_production as em', 'em.id', '=', 'epd.emp_production_id')
                    ->select(DB::raw('epd.id'), DB::raw('epd.task_id'), DB::raw('epd.task_name'), DB::raw('epd.output_pdt_id'), DB::raw('epd.output_pdt_name'), DB::raw('epd.emp_production_id'), DB::raw('epd.production_qty'), DB::raw('epd.production_unit_price'), DB::raw('epd.amount'))
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '<=', $to_date)
                    ->where('em.is_active', '=', 1)
                    ->where('em.is_present', '=', 1)
                    ->where('em.status', '=', 'initiated')
                    ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "))
                    ->union($detail)
                    ->orderBy('id', 'asc');

            $ot_amount = DB::table('tbl_emp_production_ot as ot')
                    ->leftjoin('tbl_emp_production as em', 'em.id', '=', 'ot.emp_production_id')
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '<=', $to_date)
                    ->where('em.is_active', '=', 1)
                    ->where('em.is_present', '=', 1)
                    ->where('em.status', '=', 'initiated')
                    ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "))
                    ->sum('ot.amount');

            $ot_detail = DB::table('tbl_emp_production_ot as ot')
                    ->leftjoin('tbl_emp_production as em', 'em.id', '=', 'ot.emp_production_id')
                    ->leftjoin('tbl_allowance_master as am', 'am.id', '=', 'ot.allowance_master_id')
                    ->select('ot.id as ot_id', 'ot.amount', 'ot.allowance_master_id', 'am.name')
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '<=', $to_date)
                    ->where('em.is_active', '=', 1)
                    ->where('em.is_present', '=', 1)
                    ->where('em.status', '=', 'initiated')
                    ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "));

            $adv_deduct = DB::table('tbl_emp_adv_deduction')
                    ->whereDate('date', '>=', $from_date)
                    ->whereDate('date', '<=', $to_date)
                    ->whereRaw(DB::raw("(emp_id = '$emp_id' or  emp_id = '$c' ) "))
                    ->where('is_active', '=', 1)
                    ->sum('deduction_amt');

            $t->Advance = $adv_deduct;
            $t->Total_Deduction = $advance + $t->Advance;

            $task = $bulider1->get();
            $t->Other = $ot_amount;

            $fixed_all_detail = DB::table('tbl_emp_allowance as al')
                            ->leftjoin('tbl_emp_production as em', 'em.emp_id', '=', 'al.emp_id')
                            ->select('al.amount')
                            ->where('em.date', '>=', $from_date)
                            ->where('em.date', '<=', $to_date)
                            ->where('em.is_active', '=', 1)
                            ->where('em.is_present', '=', 1)
                            ->where('em.status', '=', 'initiated')
                            ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "))
                            ->groupby('al.id')->get();

            $calculate_emp_work_days = DB::table('tbl_emp_production')
                    ->select('id')
                    ->where('date', '>=', $from_date)
                    ->where('date', '<=', $to_date)
                    ->where('is_active', '=', 1)
                    ->where('is_present', '=', 1)
                    ->where('status', '=', 'initiated')
                    ->whereRaw(DB::raw("(emp_id = '$emp_id' or  emp_id = '$c' ) and (emp_team_id='$team_id' or emp_team_id='$c1') "))
            ;

            $fixed_all_amount = 0;
            $emp_working_days = $calculate_emp_work_days->count();
            foreach ($fixed_all_detail as $all) {
                $fixed_all_amount = $fixed_all_amount + $all->amount;
            }
            $fixed_all_amount = ($fixed_all_amount * $emp_working_days );
            $t->Allowance = $fixed_all_amount;

            $salay_amount1 = ($t->Wages + $t->Other + $fixed_all_amount) - $t->Total_Deduction;

            //For Calculate Max Leave
            $salary_type = $t->salary_type;

            $leave = array();

            if ($salary_type == 'attendance') {
                $calculate_emp_leave_days = DB::table('tbl_emp_production')
                        ->select('id')
                        ->where('date', '>=', $from_date)
                        ->where('date', '<=', $to_date)
                        ->where('is_active', '=', 1)
                        ->where('is_present', '=', 0)
                        ->where('status', '=', 'absent')
                        ->whereRaw(DB::raw("(emp_id = '$emp_id' or  emp_id = '$c' ) and (emp_team_id='$team_id' or emp_team_id='$c1') "))
                ;

                $emp_leave_days = $calculate_emp_leave_days->count();
                if ($emp_leave_days > 0) {
                    $leave = DB::table('tbl_emp_production as ep')
                                    ->leftjoin('tbl_employee as e', 'ep.emp_id', '=', 'e.id')
                                    ->select('e.max_leave_count', 'e.salary_amount as per_day_salary', DB::raw("(CASE WHEN count(status) > 1 THEN concat(count(status),' ','days') ELSE concat(count(status),' ','day') END) as casual_leave")
                                            , DB::raw('(CASE WHEN e.max_leave_count > count(status) THEN count(status) * e.salary_amount  WHEN e.max_leave_count < count(status) THEN e.max_leave_count * e.salary_amount  WHEN (e.max_leave_count = count(status)) THEN e.max_leave_count * e.salary_amount   END) as casual_leave_amount'))
                                    ->where('ep.date', '>=', $from_date)
                                    ->where('ep.date', '<=', $to_date)
                                    ->where('ep.is_active', '=', 1)
                                    ->where('ep.is_present', '=', 0)
                                    ->where('ep.status', '=', 'absent')
                                    ->whereRaw(DB::raw("(ep.emp_id = '$emp_id' or  ep.emp_id = '$c' ) and (ep.emp_team_id='$team_id' or ep.emp_team_id='$c1') "))->get();
                } else {
                    $leave = array();
                }

                $emp_leave_days = $calculate_emp_leave_days->count();
                if ($emp_leave_days == $t->max_leave_count) {
                    $max_leave_count = $t->max_leave_count;
                    $basic_salary = $t->Net_Pay;
                    $salay_amount = $salay_amount1 + ($max_leave_count * $basic_salary);
                    $t->Net_Pay = $salay_amount;
                } else if ($emp_leave_days > $t->max_leave_count) {
                    $basic_salary = $t->Net_Pay;
                    $salay_amount = $salay_amount1 + ($t->max_leave_count * $basic_salary);
                    $t->Net_Pay = $salay_amount;
                } else if ($emp_leave_days < $t->max_leave_count) {
                    $basic_salary = $t->Net_Pay;
                    $salay_amount = $salay_amount1 + ($emp_leave_days * $basic_salary);
                    $t->Net_Pay = $salay_amount;
                }
            } else {
                $t->Net_Pay = $salay_amount1;
            }
        }
        
 
        foreach ($collection as $t) {
            foreach ($t as $key => $value) {
                $t->Total_Pay = $t->Wages + $t->Other + $t->Allowance;
                if ($key == 'emp_id' || $key == 'status' || $key == 'salary_type' || $key == 'payroll_type' || $key == 'max_leave_count' || $key == 'emp_team_id' || $key == 'esi_percentage' || $key == 'pf_percentage')
                    unset($t->$key);
            }
        }




        $resVal['success'] = True;
        $resVal['total'] = $count;
        $resVal['list'] = $collection;
        return $resVal;
    }

    public function employeeSalary1($request) {

        $emp_id = $request->input('emp_id');
        $preparation_id = $request->input('preparation_id');
        $emp_team_id = $request->input('emp_team_id');
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_emp_salary as esy')
                ->leftjoin("tbl_emp_team as et", 'esy.emp_team_id', '=', 'et.id')
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'esy.emp_id')
                ->select('e.emp_code as code', 'e.f_name as Name', 'esy.overall_qty as Qty', 'esy.week_salary as Wages', 'esy.fixed_allowance_amount as Allowance', 'esy.ot_amount as other', DB::raw('(esy.fixed_allowance_amount)+(esy.ot_amount)+(esy.week_salary) as Total_Pay'), 'esy.esi_amount as ESI', 'esy.pf_amount AS EPF', 'esy.adv_amount as Advance', 'esy.deduction_amount as Deduction', 'esy.total_amount as Net');


        if (!empty($emp_id)) {
            $builder->where('esy.emp_id', '=', $emp_id);
        }
        if (!empty($emp_team_id)) {
            $builder->where('esy.emp_team_id', '=', $emp_team_id);
        }
        if (!empty($id)) {
            $builder->where('esy.id', '=', $id);
        }
        if (!empty($from_date)) {
            $builder->whereDate('esy.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('esy.date', '<=', $to_date);
        }

        if (!empty($preparation_id)) {
            $builder->where('esy.emp_salary_prepare_id', '=', $preparation_id);
        }

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        $count = $builder->count();
        $resVal['success'] = True;
        $resVal['total'] = $count;
        $resVal['list'] = $collection;
        return $resVal;
    }

}
