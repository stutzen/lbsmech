<?php

namespace App\Helper;

use Carbon\Carbon;
use App\Transaction;
use Illuminate\Support\Facades\Auth;
use App\Helper\GeneralHelper;
use DB;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountHelper
 *
 * @author Deepa
 */
class TransactionHelper {

    //put your code here
    public static function saveTransaction($request, $code) {
        $currentuser = Auth::user();
  
        if (!empty($request->edit_id)&&($request->edit_id)!=0)
            $transaction = Transaction::findOrFail($request->edit_id);
        else
            $transaction = new Transaction;


        $mydate = $request->date;
        $month = date("m", strtotime($mydate));
        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();

        $transaction->fiscal_month_code = $month;
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }
        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

         
        $transaction->name = $currentuser->f_name;
        $transaction->transaction_date = $mydate;
        ;
         
        if (!empty($request->credit)&&($request->credit)!=0.00)
            $transaction->credit = $request->credit;
        if (!empty($request->debit)&&($request->debit)!=0.00)
            $transaction->debit = $request->debit;

        $transaction->acode = $request->acode;         
        $transaction->acoderef = $request->acoderef;
        ;
        $transaction->account_id = $request->account_id;
        $transaction->pmtcode = $request->pmtcode;
        $transaction->pmtcoderef = $request->pmtcoderef;
        $transaction->is_cash_flow = $request->is_cash_flow;
        ;
        $transaction->voucher_type = $request->voucher_type;
        $transaction->account = $request->account;
        $transaction->particulars = $request->particulars;
        ;
        $transaction->voucher_number = $request->voucher_number;
        ;
        $transaction->payment_mode = $request->payment_mode;
        $transaction->customer_id = $request->customer_id;
        $transaction->account_category = $request->account_category;
        $transaction->transaction_type = $request->transaction_type;          
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;


        $transaction->is_active = 1;
        $transaction->prefix = $request->prefix;

        if($code=='transaction')
        {
            $transaction->fill($request->all());
            $transaction->save();
        }

        $transaction->save();
    }

}

?>
