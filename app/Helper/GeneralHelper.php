<?php

namespace App\Helper;

use DateTime;
use App\Inventory;
use App\InventoryAdjustment;
use Carbon\Carbon;
use App\Product;
use App\Invoiceitem;
use App\InvoiceItemTax;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Purchaseinvoiceitem;
use App\Invoicetax;
use App\SalesOrderTax;
use App\QuoteTax;
use App\Purchaseinvoicetax;
use App\Account;
use App\Acode;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Deepa
 */
class GeneralHelper {

    //put your code here
    public static function decrementEmployee($team_id) {

        DB::table('tbl_emp_team')->where('id', $team_id)
                ->update(['current_count' => DB::raw('current_count -' . 1)]);
    }

    public static function incrementEmployee($team_id) {

        DB::table('tbl_emp_team')->where('id', $team_id)
                ->update(['current_count' => DB::raw('current_count +' . 1)]);
    }

    public static function getDateFormat() {
        $dateFormat = '';
        $get_currency_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'date_format')
                ->first();
        $dateFormat = $get_currency_format->value;
        return $dateFormat;
    }

    public static function formatDate($date, $format) {


        if (empty($date) || empty($format)) {

            return $date;
        }
        $new_format = str_replace("YYYY", "Y", $format);
        $new_format = str_replace("yyyy", "y", $new_format);
        $new_format = str_replace("MM", "M", $new_format);
        $new_format = str_replace("mm", "m", $new_format);
        $new_format = str_replace("DD", "D", $new_format);
        $new_format = str_replace("dd", "d", $new_format);

        $new_date = date_create($date);

        return date_format($new_date, $new_format);
    }

    public static function setCurrency() {
        $get_currency_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'currency')
                ->first();
        $symbol = $get_currency_format->value;
        return $symbol;
    }

    public static function getCurrencySymbol() {
        $get_currency_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'currency')
                ->first();
        $symbol = '';
        if (!empty($get_currency_format)) {
            $symbol = $get_currency_format->value;
        }

        return "( " . $symbol . ". " . ")";
    }

    public static function setSignature() {
        $get_currency_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'signature')
                ->first();
        $sign = $get_currency_format->value;
        return $sign;
    }

    public static function setDateFormat($date) {
        $get_date_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'date_format')
                ->first();
        $symbol = $get_date_format->value;
        return $date;
    }

    public static function getThosandSeperatorFormat() {
        $get_thousand_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'thousand_seperator')
                ->first();
        $thous_sep = 'dd,dd,ddd';
        if (!empty($get_thousand_format)) {
            $thous_sep = $get_thousand_format->value;
        }
        return $thous_sep;
    }

    public static function convertNumberFormat($number, $thous_sep) {

        $number = number_format((float) $number, 2, '.', '');
        list($whole, $decimal) = explode('.', $number);
        $number = (float) $number;
        if ($thous_sep == 'd,dddd,dddd') {
            $number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/", ",", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'd dddd dddd') {
            $number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/", " ", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'dd,dd,ddd') {
            //$number = preg_replace("/\B(?=(?:\d{4})+(?!\d))/",",",$whole);
            if (strlen($whole) < 4) {
                $number = $whole;
            } else {
                $tail = substr($whole, -3);
                $head = substr($whole, 0, -3);
                $head = preg_replace("/\B(?=(?:\d{2})+(?!\d))/", ",", $head);
                $whole = $head . "," . $tail;
            }
            $number = $whole . "." . $decimal;
        } else if ($thous_sep == 'dd ddd ddd') {
            $number = preg_replace("/\B(?=(?:\d{3})+(?!\d))/", " ", $whole);
            $number = $number . "." . $decimal;
        } else if ($thous_sep == 'dd,ddd,ddd') {
            $number = preg_replace("/\B(?=(?:\d{3})+(?!\d))/", ",", $whole);
            $number = $number . "." . $decimal;
        }
        return $number;
    }

    public static function StockAdjustmentSave($request, $code, $id) {

        $inventory = new Inventory;
        if ($code == 'purchase' || $code == 'update' || $code == 'invoice_return' || $code == 'grn' || $code == 'grn_update_insert' || $code == 'work_order_delete') {
            $productArray = $request;
        } else {
            $productArray = $request->product;
        }
        $currentuser = Auth::user();
        foreach ($productArray as $product) {
            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $product['product_id'];

            if ($product['product_id'] != '') {
                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $product['product_id'])->get();
                if ($get_product_det->count() >= 1) {
                    if ($code == 'adjust')
                        $product_id = $product['product_id'];
                    if ($product['product_id'] != '')
                        $product_id = $product['product_id'];

                    if ($id)
                        $inventory_adu->ref_id = $id;
                    if ($code == 'purchase')
                        $inventory_adu->type = "purchase_create";
                    else if ($code == 'update')
                        $inventory_adu->type = "purchase_update";
                    else if ($code == 'grn_update_insert')
                        $inventory_adu->type = "grn_update";
                    else if ($code == 'work_order_delete')
                        $inventory_adu->type = "work_order_delete";
                    else if ($code == 'grn')
                        $inventory_adu->type = "grn_create";
                    else if ($code == 'invoice_return')
                        $inventory_adu->type = "invoice_return";
                    else
                        $inventory_adu->type = "stock_adjust";
                    $inventotyCollection = Inventory::where('product_id', "=", $product['product_id'])->get();

                    $get_uom_name = DB::table('tbl_uom')
                                    ->select('name')
                                    ->where('id', '=', $product['uom_id'])->get();

                    $inventotyCollection = $inventotyCollection->first();
                    if ($inventotyCollection != null) {

                        $total_stock = $inventotyCollection->quantity + $product['qty'];
                        $inventotyCollection->quantity = $total_stock;
                        $inventotyCollection->save();

                        $inventory_adu->sku = $product['product_sku'];
                        $inventory_adu->uom_id = $product['uom_id'];
                        if ($code == 'invoice' || $code == 'update')
                            $inventory_adu->comments = "From Invoice";
                        else if ($code == 'grn')
                            $inventory_adu->comments = "From GRN";
                        else if ($code == 'grn_update_insert')
                            $inventory_adu->comments = "From GRN Update";
                        else if ($code == 'work_order_delete')
                            $inventory_adu->comments = "From Work Order Delete";
                        else if ($code == 'purchase')
                            $inventory_adu->comments = "From Purchase";
                        else if ($code == 'invoice_return')
                            $inventory_adu->comments = "From Sales invoice return";
                        else
                            $inventory_adu->comments = $product['comments'];
                        if ($get_uom_name->count() >= 1) {
                            $get_uom_name1 = $get_uom_name->first();
                            $inventory_adu->uom_name = $get_uom_name1->name;
                        } else
                            $inventory_adu->uom_name = "";
                        $inventory_adu->total_stock = $total_stock;
                        $inventory_adu->quantity_credit = $product['qty'];
                        $inventory_adu->created_by = $currentuser->id;
                        $inventory_adu->updated_by = $currentuser->id;
                        $inventory_adu->is_active = "1";
                        $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->save();
                    }
                }
            }
        }
    }

    public static function StockAdjustmentDelete($request, $code, $id) {
        $inventory = new Inventory;
        if ($code == 'pupdate' || $code == 'iupdate') {
            $productArray = $request;
        }

        $currentuser = Auth::user();
        $inventory_adu = new InventoryAdjustment;

        if ($code == 'iupdate' || $code == 'idelete') {
            $invoiceItemCollection = Invoiceitem::where('invoice_id', "=", $id)
                    ->get();
        } else if ($code == 'pupdate' || $code == 'pdelete') {
            $invoiceItemCollection = Purchaseinvoiceitem::where('purchase_invoice_id', "=", $id)
                    ->get();
        } else if ($code == 'invoice_return_delete' || $code == 'grn_update' || $code == 'work_order_update') {
            $invoiceItemCollection = $request;
        }
        //print_r($invoiceItemCollection);
        foreach ($invoiceItemCollection as $product) {
            $product_id = $product->product_id;
            if ($product_id != 0 || $product_id != '') {
                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $product_id)->get();
                if ($get_product_det->count() >= 1) {
                    $qty = $product->qty;
                    $get_inventory = Inventory::where('product_id', '=', $product_id)
                            ->first();
                    if ($code == 'iupdate' || $code == 'idelete' || $code == 'work_order_update')
                        $get_inventory->quantity = $get_inventory->quantity + $qty;
                    else if ($code == 'pupdate' || $code == 'pdelete' || $code == 'grn_update')
                        $get_inventory->quantity = $get_inventory->quantity - $qty;
                    $get_inventory->save();
                    if ($code == 'pupdate') {
                        $get_inventory_adjust = InventoryAdjustment::where('product_id', '=', $product_id)
                                ->where('ref_id', '=', $id)
                                ->where('type', '=', 'purchase_create')
                                ->orWhere('type', '=', 'purchase_update')
                                ->first();
                        $get_inventory_adjust->is_active = '0';
                        $get_inventory_adjust->save();
                    } else if ($code == 'iupdate') {
                        $get_inventory_adjust = InventoryAdjustment::where('product_id', '=', $product_id)
                                ->where('ref_id', '=', $id)
                                ->where('type', '=', 'invoice_create')
                                ->orWhere('type', '=', 'invoice_update')
                                ->first();
                        $get_inventory_adjust->is_active = '0';
                        $get_inventory_adjust->save();
                    } else if ($code == 'grn_update') {
                        $get_inventory_adjust = InventoryAdjustment::where('product_id', '=', $product_id)
                                ->where('ref_id', '=', $id)
                                ->where('type', '=', 'grn_create')
                                ->first();
                        if (!empty($get_inventory_adjust)) {
                            $get_inventory_adjust->is_active = '0';
                            $get_inventory_adjust->save();
                        }
                    } else if ($code == 'work_order_update') {
                        $get_inventory_adjust = InventoryAdjustment::where('product_id', '=', $product_id)
                                ->where('ref_id', '=', $id)
                                ->where('type', '=', 'work_order_create')
                                ->first();
                        if (!empty($get_inventory_adjust)) {
                            $get_inventory_adjust->is_active = '0';
                            $get_inventory_adjust->save();
                        }
                    } else {
                        $inventory_adu = new InventoryAdjustment;
                        $inventory_adu->product_id = $product_id;

                        $inventory_adu->ref_id = $id;
                        if ($code == 'iupdate' || $code == 'idelete') {
                            $inventory_adu->type = "invoice_delete";
                            $inventory_adu->comments = "Invoice Delete";
                        } else if ($code == 'pupdate' || $code == 'pdelete') {
                            $inventory_adu->type = "purchase_delete";
                            $inventory_adu->comments = "Purchase Delete";
                        } else if ($code == 'invoice_return_delete') {
                            $inventory_adu->type = "invoice_return_delete";
                            $inventory_adu->comments = "Invoice Return Delete";
                        }
                        $inventotyCollection = Inventory::where('product_id', "=", $product_id)->get();

                        $get_uom_name = DB::table('tbl_uom')
                                        ->select('name')
                                        ->where('id', '=', $product->uom_id)->get();

                        $inventotyCollection = $inventotyCollection->first();
                        $total_stock = $inventotyCollection->quantity;


                        $inventory_adu->sku = $product->product_sku;
                        $inventory_adu->uom_id = $product->uom_id;

                        if ($get_uom_name->count() >= 1) {
                            $get_uom_name1 = $get_uom_name->first();
                            $inventory_adu->uom_name = $get_uom_name1->name;
                        } else
                            $inventory_adu->uom_name = "";
                        $inventory_adu->total_stock = $total_stock;
                        if ($code == 'iupdate' || $code == 'idelete' || $code == 'invoice_return_delete')
                            $inventory_adu->quantity_credit = $qty;
                        else if ($code == 'pupdate' || $code == 'pdelete')
                            $inventory_adu->quantity_debit = $qty;

                        $inventory_adu->created_by = $currentuser->id;
                        $inventory_adu->updated_by = $currentuser->id;
                        $inventory_adu->is_active = "1";
                        $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->save();
                    }
                }
            }
        }
    }

    public static function StockWastageSave($request, $code, $id) {
        $inventory = new Inventory;
        if ($code == 'invoice' || $code == 'update' || $code == 'grn_delete' || $code == 'work_order' || $code == 'work_order_update_insert' || $code == 'purchase_invoice_quality_check' || $code == 'mo_quality_check') {
            $productArray = $request;
        } else {
            $productArray = $request->product;
        }
        $currentuser = Auth::user();
        foreach ($productArray as $product) {
            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $product['product_id'];

            if ($product['product_id'] != '') {
                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $product['product_id'])->get();
                if ($get_product_det->count() >= 1) {
                    if ($code == 'adjust')
                        $product_id = $product['product_id'];

                    if ($id)
                        $inventory_adu->ref_id = $id;
                    if ($code == 'invoice')
                        $inventory_adu->type = "invoice_create";
                    else if ($code == 'grn_delete')
                        $inventory_adu->type = "grn_delete";
                    else if ($code == 'update')
                        $inventory_adu->type = "invoice_update";
                    else if ($code == 'work_order')
                        $inventory_adu->type = "work_order_create";
                    else if ($code == 'work_order_update_insert')
                        $inventory_adu->type = "work_order_update";
                    else if ($code == 'purchase_invoice_quality_check')
                        $inventory_adu->type = "purchase_invoice_quality_check";
                    else if ($code == 'mo_quality_check')
                        $inventory_adu->type = "mo_quality_check";
                    else
                        $inventory_adu->type = "stock_wastage";
                    $inventotyCollection = Inventory::where('product_id', "=", $product['product_id'])->get();

                    $get_uom_name = DB::table('tbl_uom')
                                    ->select('name')
                                    ->where('id', '=', $product['uom_id'])->get();
                    $inventotyCollection = $inventotyCollection->first();
                    
                    if ($inventotyCollection != null) {
                        $total_stock = $inventotyCollection->quantity - $product['qty'];
                        $inventotyCollection->quantity = $total_stock;
                        $inventotyCollection->save();

                        $inventory_adu->sku = $product['product_sku'];
                        $inventory_adu->uom_id = $product['uom_id'];
                        if ($code == 'invoice' || $code == 'update')
                            $inventory_adu->comments = "From Invoice";
                        else if ($code == 'work_order')
                            $inventory_adu->comments = "From Work Order Create";
                        else if ($code == 'grn_delete')
                            $inventory_adu->comments = "From Work Order Delete";
                        else if ($code == 'work_order_update_insert')
                            $inventory_adu->comments = "From Work Order Update";
                        else
                            $inventory_adu->comments = $product['comments'];
                        if ($get_uom_name->count() >= 1) {
                            $get_uom_name1 = $get_uom_name->first();
                            $inventory_adu->uom_name = $get_uom_name1->name;
                        } else
                            $inventory_adu->uom_name = "";
                        $inventory_adu->total_stock = $total_stock;
                        $inventory_adu->quantity_debit = $product['qty'];
                        $inventory_adu->created_by = $currentuser->id;
                        $inventory_adu->updated_by = $currentuser->id;
                        $inventory_adu->is_active = "1";
                        $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $inventory_adu->save();
                    }
                }
            }
        }
    }

    //This for LBSPOS calculate tax from product

    public static function getProductTax($item) {

        $product_tax_det = array();
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        print_r($item);
        foreach ($item as $product) {
            $product_tax_det1 = array();
            $pro_id = $product['product_id'];
            $builder = DB::table('tbl_tax as i')
                    ->leftJoin('tbl_tax_mapping as c', 'c.tax_id', '=', 'i.id')
                    ->select('i.*')
                    ->where('c.is_active', '=', 1)
                    ->where('c.product_id', '=', $pro_id)
                    ->whereRaw(DB::raw("(start_date <= '$current_date' or  `start_date` is null) and (end_date >= '$current_date' or end_date is null) "))
                    ->where('i.is_active', '=', 1)
                    ->first();

            $product['tax_list'] = array();
            if ($builder != "") {
                $product['tax_id'] = $builder->id;
                $tax_list = GeneralHelper::getGroupProductTax($builder->id);
                $product['tax_list'] = $tax_list['tax_list'];
                $product['tax_percentage'] = $tax_list['tax_percentage'];
            }
            array_push($product_tax_det, $product);
        }
        return $product_tax_det;
    }

    public static function getProductTaxforPurchase($item) {

        $product_tax_det = array();
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        foreach ($item as $product) {
            $product_tax_det1 = array();
            $pro_id = $product['product_id'];
            $builder = DB::table('tbl_tax')
                    ->select('*')
                    ->where('id', '=', $product['tax_id'])
                    ->first();

            $product['tax_list'] = array();
            if ($builder != "") {
                $product['tax_id'] = $builder->id;
                $tax_list = GeneralHelper::getGroupProductTax($builder->id);
                $product['tax_list'] = $tax_list['tax_list'];
                $product['tax_percentage'] = $tax_list['tax_percentage'];
            }
            array_push($product_tax_det, $product);
        }

        return $product_tax_det;
    }

    public static function getGroupProductTax($tax_id) {
        $tax_list_det = DB::table('tbl_tax')
                ->select('*')
                ->where('id', '=', $tax_id)
                ->first();
        $tax_list = array();
        if ($tax_list_det->is_group == 1) {
            $associate_tax_id = explode(',', $tax_list_det->group_tax_ids);
            $bilder = DB::table('tbl_tax')->select('*')
                            ->where('is_active', '=', 1)
                            ->whereIn('id', $associate_tax_id)->get();

            $tax_percentage = DB::table('tbl_tax')->where('is_active', '=', 1)
                    ->whereIn('id', $associate_tax_id)
                    ->sum('tax_percentage');
            $tax_list['tax_list'] = $bilder;
            $tax_list['tax_percentage'] = $tax_percentage;
        } else {
            $bilder = DB::table('tbl_tax')
                    ->select('*')
                    ->where('id', '=', $tax_id)
                    ->get();
            $tax_list['tax_list'] = $bilder;
            $tax_list['tax_percentage'] = $tax_list_det->tax_percentage;
        }
        return $tax_list;
    }

    public static function insertProductTax($product_tax_list, $id, $invoice_item_id) {
        $currentuser = Auth::user();
        $total_tax_amount = 0;
        $tax_list = $product_tax_list['tax_list'];
        foreach ($tax_list as $tax) {
            $invoicetax = new InvoiceItemTax;
            $invoicetax->invoice_id = $id;
            $invoicetax->invoice_item_id = $invoice_item_id;
            $invoicetax->tax_id = $tax->id;
            $invoicetax->tax_name = $tax->tax_name;
            $invoicetax->tax_percentage = $tax->tax_percentage;
            $product_amount = $product_tax_list['unit_price'] * $product_tax_list['qty'];
            $invoicetax->product_amt = $product_amount;
            $invoicetax->tax_applicable_amt = $tax->tax_applicable_amt;
            $tax_amount = ($product_tax_list['produt_rem_amount'] ) * ($tax->tax_percentage / 100);
            $invoicetax->tax_amount = $tax_amount;
            $total_tax_amount = $total_tax_amount + $tax_amount;
            $invoicetax->is_active = 1;
            $invoicetax->created_by = $currentuser->id;
            $invoicetax->updated_by = $currentuser->id;
            $invoicetax->save();
        }
        return $total_tax_amount;
    }

    public static function calculateDiscount($item) {
        if ($item['discount_mode'] == "%") {
            $product_discount_amount = ($item['qty'] * $item['unit_price']) * ( $item['discount_value'] / 100);
        } else {
            $product_discount_amount = $item['discount_value'];
        }
        return $product_discount_amount;
    }

    //This is for maintain purchase inventory

    public static function updatePurchaseInventory($item) {
        $currentuser = Auth::user();
        foreach ($item as $product) {

            if (gettype($product) == 'array') {
                $purchase_invoice_item_id = $product['purchase_invoice_item_id'];
                $qty = $product['qty'];
            } else {
                $purchase_invoice_item_id = $product->purchase_invoice_item_id;
                $qty = $product->qty;
            }

            if (!empty($purchase_invoice_item_id) || $purchase_invoice_item_id != 0) {


                DB::table('tbl_purchase_invoice_item')->where('id', '=', $purchase_invoice_item_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand -' . $qty),
                                    "sales_qty" => DB::raw('sales_qty +' . $qty)
                                )
                );
            }
        }
    }

    public static function updatePurchaseInventoryForUpdate($item) {
        $currentuser = Auth::user();
        foreach ($item as $product) {
            if (gettype($product) == 'array') {
                $purchase_invoice_item_id = $product['purchase_invoice_item_id'];
                $qty = $product['qty'];
            } else {
                $purchase_invoice_item_id = $product->purchase_invoice_item_id;
                $qty = $product->qty;
            }
            if (!empty($purchase_invoice_item_id) || $purchase_invoice_item_id != 0) {


                DB::table('tbl_purchase_invoice_item')->where('id', '=', $purchase_invoice_item_id)
                        ->update(
                                array(
                                    "qty_in_hand" => DB::raw('qty_in_hand +' . $qty),
                                    "sales_qty" => DB::raw('sales_qty -' . $qty)
                                )
                );
            }
        }
    }

    public static function InvoiceTaxSave($id, $tax_id, $taxable_amount, $sub_amount, $code) {
        if ($tax_id != null) {

            $currentuser = Auth::user();
            $tax_list = GeneralHelper::calculatedInvoiceTax($id, $tax_id, $taxable_amount, $code);

            foreach ($tax_list as $tax) {
                if ($code == 'sales')
                    $invoicetax = new Invoicetax;
                else if ($code == 'purchase')
                    $invoicetax = new Purchaseinvoicetax;
                else if ($code == 'sales_order')
                    $invoicetax = new SalesOrderTax;
                else if ($code == 'sales_quote')
                    $invoicetax = new QuoteTax;
                $invoicetax->invoice_id = $id;
                $invoicetax->tax_id = $tax->id;
                $invoicetax->tax_name = $tax->tax_name;
                $invoicetax->tax_percentage = $tax->tax_percentage;
                $invoicetax->product_amt = 0;
                $invoicetax->applicable_amt = $tax->tax_applicable_amt;
                $invoicetax->taxable_amount = $tax->taxable_amount;
                $invoicetax->tax_amount = $tax->tax_amount;
                $invoicetax->tax_type = $tax->tax_type;
                $invoicetax->is_active = 1;
                $invoicetax->created_by = $currentuser->id;
                $invoicetax->updated_by = $currentuser->id;

                $invoicetax->save();
            }
        }
    }

    public static function calculatedInvoiceTax($invoide_id, $tax_id, $amount, $code) {
        $tax_list = array();

        if ($tax_id == 'invoiced_tax_rate') {
            $tax_list = GeneralHelper::calculatedInvoiceTax_BasedOnInvoiceGenerate($invoide_id, $amount, $code);
        } else {
            $tax_list = GeneralHelper::calculatedInvoiceTax_BasedOnCurrentTaxRate($tax_id, $amount);
        }
        return $tax_list;
    }

    public static function calculatedInvoiceTax_BasedOnCurrentTaxRate($tax_id, $amount) {
        $tax_collection = DB::table('tbl_tax')->select('*')
                        ->where('id', '=', $tax_id)
                        ->where('is_active', '=', 1)->get();
        $total_tax_amount = 0;
        $tax_list = array();
        if (count($tax_collection) > 0) {

            $tax = $tax_collection->first();
            if (!empty($tax->is_group)) {
                $associate_tax_id = explode(',', $tax->group_tax_ids);
                $chargeAmount = 0;
                $associate_tax_collection = DB::table('tbl_tax')->select('*')
                                ->where('is_active', '=', 1)
                                ->whereIn('id', $associate_tax_id)->get();

                foreach ($associate_tax_collection as $associate_tax) {

                    $tax_amount = 0;

                    if ($associate_tax->tax_type != 'tax+charge') {
                        $tax_amount = $amount * $associate_tax->tax_percentage / 100;
                        $tax_amount = round($tax_amount, 2);
                        $total_tax_amount += $tax_amount;

                        if ($associate_tax->tax_type == 'charge') {
                            $chargeAmount += $tax_amount;
                        }
                        $associate_tax->taxable_amount = $amount;
                        $associate_tax->tax_amount = $tax_amount;
                    }
                }
                foreach ($associate_tax_collection as $associate_tax) {

                    $tax_amount = 0;

                    if ($associate_tax->tax_type == 'tax+charge') {
                        $tax_amount += ($amount + $chargeAmount) * $associate_tax->tax_percentage / 100;
                        $tax_amount = round($tax_amount, 2);
                        $total_tax_amount += $tax_amount;
                        $associate_tax->taxable_amount = $amount + $chargeAmount;
                        $associate_tax->tax_amount = $tax_amount;
                    }
                }

                //sorting the tax in creation order
                foreach ($associate_tax_id as $asso_tax_id) {

                    foreach ($associate_tax_collection as $associate_tax) {
                        if ($asso_tax_id == $associate_tax->id) {
                            array_push($tax_list, $associate_tax);
                            break;
                        }
                    }
                }
            } else {

                $tax_amount = $amount * $tax->tax_percentage / 100;
                $tax_amount = round($tax_amount, 2);
                $total_tax_amount = $tax_amount;
                $tax->taxable_amount = $amount;
                $tax->tax_amount = $tax_amount;
                array_push($tax_list, $tax);
            }
        }
        return $tax_list;
    }

    public static function calculatedInvoiceTax_BasedOnInvoiceGenerate($invoide_id, $amount, $code) {

        if ($code == 'sales') {
            $tax_collection = DB::table('tbl_invoice_tax')->select('*')
                            ->where('invoice_id', '=', $invoide_id)
                            ->where('is_active', '=', 1)->get();
        } else if ($code == 'purchase') {
            $tax_collection = DB::table('tbl_purchase_invoice_tax')->select('*')
                            ->where('invoice_id', '=', $invoide_id)
                            ->where('is_active', '=', 1)->get();
        } else if ($code == 'sales_order') {
            $tax_collection = DB::table('tbl_sales_order_tax')->select('*')
                            ->where('invoice_id', '=', $invoide_id)
                            ->where('is_active', '=', 1)->get();
        } else if ($code == 'sales_quote') {
            $tax_collection = DB::table('tbl_quote_tax')->select('*')
                            ->where('invoice_id', '=', $invoide_id)
                            ->where('is_active', '=', 1)->get();
        }

        $total_tax_amount = 0;
        $tax_list = array();
        $chargeAmount = 0;
        if (count($tax_collection) > 0) {


            foreach ($tax_collection as $associate_tax) {

                $tax_amount = 0;

                if ($associate_tax->tax_type != 'tax+charge') {
                    $tax_amount = $amount * $associate_tax->tax_percentage / 100;
                    $tax_amount = round($tax_amount, 2);
                    $total_tax_amount += $tax_amount;

                    if ($associate_tax->tax_type == 'charge') {
                        $chargeAmount += $tax_amount;
                    }
                    $associate_tax->taxable_amount = $amount;
                    $associate_tax->tax_amount = $tax_amount;
                }
            }

            foreach ($tax_collection as $associate_tax) {

                $tax_amount = 0;

                if ($associate_tax->tax_type == 'tax+charge') {
                    $tax_amount += ($amount + $chargeAmount) * $associate_tax->tax_percentage / 100;
                    $tax_amount = round($tax_amount, 2);
                    $total_tax_amount += $tax_amount;
                    $associate_tax->taxable_amount = $amount + $chargeAmount;
                    $associate_tax->tax_amount = $tax_amount;
                }
            }
        }
        return $tax_collection;
    }

    public static function InvoiceTaxUpdate($id, $tax_id, $taxable_amount, $sub_amount, $code) {
        $currentuser = Auth::user();
        if ($tax_id == 'invoiced_tax_rate') {
            $invoice_tax_list = GeneralHelper::calculatedInvoiceTax_BasedOnInvoiceGenerate($id, $taxable_amount, $code);

            try {

                foreach ($invoice_tax_list as $tax) {
                    if ($code == 'sales') {
                        $invoicetax = Invoicetax::findOrFail($tax->id);
                    } else if ($code == 'purchase') {
                        $invoicetax = Purchaseinvoicetax::findOrFail($tax->id);
                    } else if ($code == 'sales_order') {
                        $invoicetax = SalesOrderTax::findOrFail($tax->id);
                    } else if ($code == 'sales_quote') {
                        $invoicetax = QuoteTax::findOrFail($tax->id);
                    }
                    $invoicetax->taxable_amount = $tax->taxable_amount;
                    $invoicetax->tax_amount = $tax->tax_amount;
                    $invoicetax->is_active = 1;
                    $invoicetax->updated_by = $currentuser->id;
                    $invoicetax->save();
                }
            } catch (ModelNotFoundException $e) {
                
            }
        } else {
            if ($code == 'sales') {
                DB::table('tbl_invoice_tax')
                        ->where('invoice_id', '=', $id)
                        ->delete();
            } else if ($code == 'purchase') {
                DB::table('tbl_purchase_invoice_tax')
                        ->where('invoice_id', '=', $id)
                        ->delete();
            } else if ($code == 'sales_order') {
                DB::table('tbl_sales_order_tax')
                        ->where('invoice_id', '=', $id)
                        ->delete();
            } else if ($code == 'sales_quote') {
                DB::table('tbl_quote_tax')
                        ->where('invoice_id', '=', $id)
                        ->delete();
            }
            GeneralHelper::InvoiceTaxSave($id, $tax_id, $taxable_amount, $sub_amount, $code);
        }
    }

    public static function InvoiceTaxDelete($id, $code) {
        if ($code == 'sales') {
            DB::table('tbl_invoice_tax')->where('invoice_id', $id)
                    ->update(['is_active' => 0]);
        } else if ($code == 'purchase') {
            DB::table('tbl_purchase_invoice_tax')->where('invoice_id', $id)
                    ->update(['is_active' => 0]);
        } else if ($code == 'sales_order') {
            DB::table('tbl_sales_order_tax')->where('invoice_id', $id)
                    ->update(['is_active' => 0]);
        } else if ($code == 'sales_quote') {
            DB::table('tbl_quote_tax')->where('invoice_id', $id)
                    ->update(['is_active' => 0]);
        }
    }

    public static function purchaseInvoiceItemTax($purchaseinvoiceitems) {
        $currentuser = Auth::user();
        $taxAmt = 0.00;
        $taxPercentage = 0;
        if ($purchaseinvoiceitems->tax_id != null || $purchaseinvoiceitems->tax_id != 0) {
            $taxes = DB::table('tbl_tax')
                            ->select('*')
                            ->where('id', $purchaseinvoiceitems->tax_id)->get();
            foreach ($taxes as $tax) {
                if ($tax->is_group == 0) {
                    $invoicetax = new Purchaseinvoicetax;
                    $invoicetax->purchase_invoice_item_id = $purchaseinvoiceitems->id;
                    $invoicetax->invoice_id = $purchaseinvoiceitems->purchase_invoice_id;
                    $invoicetax->is_active = 1;
                    $invoicetax->created_by = $currentuser->id;
                    $invoicetax->updated_by = $currentuser->id;
                    $taxPercentage = $tax->tax_percentage + $taxPercentage;
                    $invoicetax->tax_id = $tax->id;
                    $invoicetax->product_amt = $purchaseinvoiceitems->purchase_value; //qty*selling_price
                    $invoicetax->tax_amount = ($purchaseinvoiceitems->purchase_value - $purchaseinvoiceitems->discount_amount) * ($tax->tax_percentage / 100);
                    $taxAmt = $invoicetax->tax_amount + $taxAmt;
                    $invoicetax->tax_name = $tax->tax_name;
                    $invoicetax->tax_percentage = $tax->tax_percentage;
                    $invoicetax->tax_applicable_amt = $tax->tax_applicable_amt;
                    $invoicetax->save();
                } else {
                    $splittedstring = explode(",", $tax->group_tax_ids);
                    foreach ($splittedstring as $key => $value) {
                        $gTs = DB::table('tbl_tax')
                                        ->select('*')
                                        ->where('id', $value)->get();
                        foreach ($gTs as $gT) {
                            //print_r($gT->id);
                            $invoicetax = new Purchaseinvoicetax;
                            $invoicetax->purchase_invoice_item_id = $purchaseinvoiceitems->id;
                            $invoicetax->invoice_id = $purchaseinvoiceitems->purchase_invoice_id;
                            $invoicetax->is_active = 1;
                            $invoicetax->created_by = $currentuser->id;
                            $invoicetax->updated_by = $currentuser->id;
                            $invoicetax->product_amt = $purchaseinvoiceitems->purchase_price_wo_tax;
                            $invoicetax->product_amt = $purchaseinvoiceitems->purchase_value; //qty*selling_price
                            $invoicetax->tax_amount = ($purchaseinvoiceitems->purchase_value - $purchaseinvoiceitems->discount_amount) * ($gT->tax_percentage / 100);
                            $taxAmt = $invoicetax->tax_amount + $taxAmt;
                            $taxPercentage = $gT->tax_percentage + $taxPercentage;
                            $invoicetax->tax_id = $gT->id;
                            $invoicetax->tax_name = $gT->tax_name;
                            $invoicetax->tax_percentage = $gT->tax_percentage;
                            $invoicetax->tax_applicable_amt = $gT->tax_applicable_amt;
                            $invoicetax->save();
                        }
                    }
                }
            }
            $purchaseinvoiceitems->tax_amount = $taxAmt;
            $purchaseinvoiceitems->tax_percentage = $taxPercentage;
            $purchaseinvoiceitems->total_price = $purchaseinvoiceitems->purchase_value - $purchaseinvoiceitems->discount_amount + $purchaseinvoiceitems->tax_amount;
            $purchaseinvoiceitems->save();
        }
    }

    public static function getCustomerAcode($customer_id) {

        $retValue = '';
        if (!empty($customer_id)) {
            $customer = DB::table('tbl_customer')->where('id', $customer_id)->first();
            if ($customer != null) {
                $retValue = $customer->acode;
            }
        }
        return $retValue;
    }

    public static function getPaymentAcode($account_id) {
        $retValue = '';
        if (!empty($customer_id)) {
            $account = DB::table('tbl_accounts')->where('id', $account_id)->first();
            if ($account != null) {
                $retValue = $account->acode;
            }
        }
        return $retValue;
    }

    public static function getExpenseTrackDetail($mapping, $alowance_id, $amount, $date) {
        $mapping_amount = $mapping->amount;
        $currentuser = Auth::user();

        $role_id = $currentuser->role_id;
        if ($mapping->type == 'Daily') {
            $exp_track_amount = DB::table('tbl_expense_tracker')
                    ->select('*')
                    ->where('allowance_id', '=', $alowance_id)
                    ->where('role_id', '=', $role_id)
                    ->whereDate('date', '=', $date)
                    ->where('status', '!=', 'cancelled')
                    ->where('is_active', '=', 1)
                    ->sum('amount');
        } else if ($mapping->type == 'Yearly') {
            $year = DateTime::createFromFormat("Y-m-d", $date);
            $year = $year->format("Y");
            $exp_track_amount = DB::table('tbl_expense_tracker')
                    ->select('*')
                    ->where('allowance_id', '=', $alowance_id)
                    ->where('role_id', '=', $role_id)
                    ->whereYear('date', '=', $year)
                    ->where('is_active', '=', 1)
                    ->where('status', '!=', 'cancelled')
                    ->sum('amount');
        } else if ($mapping->type == 'Monthly') {
            $year = DateTime::createFromFormat("Y-m-d", $date);
            $year = $year->format("m");
            $exp_track_amount = DB::table('tbl_expense_tracker')
                    ->select('*')
                    ->where('allowance_id', '=', $alowance_id)
                    ->where('role_id', '=', $role_id)
                    ->whereMonth('date', '=', $year)
                    ->where('is_active', '=', 1)
                    ->where('status', '!=', 'cancelled')
                    ->sum('amount');
        } else if ($mapping->type == 'Weekly') {

            $custom_date = strtotime(date('Y-m-d', strtotime($date)));
            $week_start = date('Y-m-d', strtotime('this week last sunday', $custom_date));
            $week_end = date('Y-m-d', strtotime('this week saturday', $custom_date));

            $year = DateTime::createFromFormat("Y-m-d", $date);
            $year = $year->format("Y");
            $exp_track_amount = DB::table('tbl_expense_tracker')
                    ->select('*')
                    ->where('allowance_id', '=', $alowance_id)
                    ->where('role_id', '=', $role_id)
                    ->whereDate('date', '>=', $week_start)
                    ->whereDate('date', '<=', $week_end)
                    ->where('status', '!=', 'cancelled')
                    ->where('is_active', '=', 1)
                    ->sum('amount');
        }
        $tot_track_amount = $exp_track_amount + $amount;
        return $tot_track_amount;
    }

    public static function getCustomerAccountId($customer_id) {
        $retValue = '';
        if (!empty($customer_id)) {
            $account = DB::table('tbl_acode')->where('is_active', 1)->where('ref_id', $customer_id)
                            ->where('ref_type', 'customer')->first();
            ;
            if ($account != null) {
                $retValue = $account->id;
            }
        }
        return $retValue;
    }

    public static function getCustomerAccount($account_id) {
        $retValue = '';
        if (!empty($account_id)) {
            $account = DB::table('tbl_accounts')->where('is_active', 1)->where('id', $account_id)->first();
            ;
            if ($account != null) {
                $retValue = $account->account;
            }
        }
        return $retValue;
    }

    public static function getContraAccountId($currentuser, $type) {
        if ($type == 'creditNote')
            $contra_account = DB::table('tbl_accounts')->where('is_active', 1)->where('account', 'creditNote')->get();
        else
            $contra_account = DB::table('tbl_accounts')->where('is_active', 1)->where('account', 'debitNote')->get();
        if (count($contra_account) > 0) {
            $contra_account = $contra_account->first();
            $contra_account_id = $contra_account->id;
        } else {
            $con_acc = new Account;
            $con_acc->account = $type;
            $con_acc->is_active = 1;
            $con_acc->created_by = $currentuser->id;
            $con_acc->updated_by = $currentuser->id;
            $con_acc->save();

            $acode = new Acode;
            $acode->created_by = $currentuser->id;
            $acode->updated_by = $currentuser->id;
            $acode->ref_type = Acode::$ACCOUNT_REF;
            $acode->ref_id = $con_acc->id;
            $acode->name = $con_acc->account;
            $acode->save();
            $acode->code = $acode->id;
            $acode->save();

            $con_acc->acode = $acode->code;
            $con_acc->save();
            $contra_account_id = $con_acc->id;
        }
        return $contra_account_id;
    }

    public static function InventoryUpdate($product_id, $qty, $type) {
        $pdt_qty = 0;
        $inventory = DB::table('tbl_inventory')
                ->where('product_id', '=', $product_id)
                ->where('is_active', '=', 1)
                ->first();
        if ($inventory != '') {
            $pdt_qty = $inventory->quantity;
        }
        if ($type == 'order_output') {
            $qty1 = $pdt_qty + $qty;
        } else if ($type == 'order_input') {
            $qty1 = $pdt_qty - $qty;
        } else if ($type == 'journal_from') {
            $qty1 = $pdt_qty - $qty;
        } else if ($type == 'journal_to') {
            $qty1 = $pdt_qty + $qty;
        } else if ($type == 'qty_decrease') {
            $qty1 = $pdt_qty - $qty;
        } else if ($type == 'qty_increase') {
            $qty1 = $pdt_qty + $qty;
        }

        DB::table('tbl_inventory')
                ->where('product_id', '=', $product_id)
                ->where('is_active', '=', 1)
                ->update(['quantity' => $qty1]);
    }

    
      public static function CheckInventoryAdjust($id,$type,$product_id) {

        $builder = DB::table('tbl_inventory_adjustment')->where('ref_id', $id)
                ->where('type',$type)
                ->where('product_id',$product_id)
                ->get();
        return $builder;
                
    }
    
}

?>
