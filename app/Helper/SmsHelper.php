<?php

namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;
use App\Helper\GeneralHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Deepa
 */
class SmsHelper {

    //put your code here

    public static function serviceRequestCompletedtedNotification($service) {

        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "service_request_complete_sms")
                ->first();

        $msg = $get_template->message;
        //print_r($msg);                

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $service->customer_id)
                ->first();

        $get_emp_det = DB::table("tbl_employee")
                ->where('is_active', '=', 1)
                ->where('id', '=', $service->assigned_emp_id)
                ->first();

        $get_product_det = DB::table("tbl_product")
                ->where('id', '=', $service->product_id)
                ->first();

        $phone = '';

        $emp_name = $get_emp_det->f_name;
        $emp_phone = $get_emp_det->ph_no;

        if ($get_cus_phone != null) {
            $phone = $get_cus_phone->phone;
            $msg = str_replace("{{customer}}", $get_cus_phone->fname, $msg);
        }
        $msg = str_replace("{{product}}", $get_product_det->name, $msg);
        $msg = str_replace("{{emp_name}}", $emp_name, $msg);
        $msg = str_replace("{{emp_phone}}", $emp_phone, $msg);
        $msg = str_replace("{{id}}", $service->id, $msg);

        $phone = explode(",", $phone);
        for ($i = 0; $i < count($phone); $i++) {
            SmsHelper::sendSms($phone[$i], $msg);
        }
    }

    public static function serviceRequestCreatedNotification($service) {

        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "service_request_create_sms")
                ->first();

        $msg = $get_template->message;
        //print_r($msg);                

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $service->customer_id)
                ->first();

        $get_emp_det = DB::table("tbl_employee")
                ->where('is_active', '=', 1)
                ->where('id', '=', $service->assigned_emp_id)
                ->first();
        $get_product_det = DB::table("tbl_product")
                ->where('id', '=', $service->product_id)
                ->first();
        $phone = '';

        $emp_name = $get_emp_det->f_name;
        $emp_phone = $get_emp_det->ph_no;

        if ($get_cus_phone != null) {
            $phone = $get_cus_phone->phone;
            $msg = str_replace("{{customer}}", $get_cus_phone->fname, $msg);
        }
        $msg = str_replace("{{product}}", $get_product_det->name, $msg);
        $msg = str_replace("{{emp_name}}", $emp_name, $msg);
        $msg = str_replace("{{emp_phone}}", $emp_phone, $msg);
        $msg = str_replace("{{id}}", $service->id, $msg);
        $phone = explode(",", $phone);
        for ($i = 0; $i < count($phone); $i++) {
            SmsHelper::sendSms($phone[$i], $msg);
        }
    }

    public static function invoiceCreatedNotification($invoice) {

        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_created_sms")
                ->first();

        $msg = $get_template->message;
        //print_r($msg);

        $new_msg = str_replace("{{invoiceid}}", $invoice->invoice_code, $msg);


        $currency = GeneralHelper::setCurrency($invoice->total_amount);
        $totalAmount = $currency . "." . $invoice->total_amount;
        $new_msg = str_replace("{{invoice_amount}}", $totalAmount, $new_msg);

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $invoice->customer_id)
                ->first();
        $phone = '';
        if ($get_cus_phone != null) {
            $phone = $get_cus_phone->phone;
            $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
        }
        $phone = explode(",", $phone);
        for ($i = 0; $i < count($phone); $i++) {
            SmsHelper::sendSms($phone[$i], $new_msg);
        }
    }

    public static function invoicePaymentNotification($payment) {

        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_payment_sms")
                ->first();

        //print_r($get_template);
        $msg = $get_template->message;
        //print_r($msg);
        $new_msg = str_replace("{{payment_id}}", $payment->id, $msg);
        $curreny = GeneralHelper::setCurrency();
        $paymentAmount = $curreny . "." . $payment->amount;
        $new_msg = str_replace("{{payment_amount}}", $paymentAmount, $new_msg);

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $payment->customer_id)
                ->first();
        $phone = '';
        if ($get_cus_phone != null) {
            $phone = $get_cus_phone->phone;
            $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
        }


        $phone = explode(",", $phone);
        for ($i = 0; $i < count($phone); $i++) {
            SmsHelper::sendSms($phone[$i], $new_msg);
        }
    }

    public static function invoiceReminderNotification($id) {

        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_reminder_sms")
                ->first();

        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');

        $amount = DB::table('tbl_invoice')
                ->where('id', '=', $id)
                ->sum('total_amount');

        $paid_amount = DB::table('tbl_payment')
                ->where('date', "<", $current_date)
                ->where('invoice_id', '=', $id)
                ->sum('amount');


        $bal_amount = $amount - $paid_amount;

        $get_cus_id = DB::table("tbl_invoice")
                ->where('id', '=', $id)
                ->first();

        $cus_id = $get_cus_id->customer_id;

        $msg = $get_template->message;

        $curreny = GeneralHelper::setCurrency();
        $bal_amount = $curreny . "." . $bal_amount;


        $new_msg = str_replace("{{invoiceid}}", $get_cus_id->invoice_code, $msg);

        $new_msg = str_replace("{{invoice_amount}}", $bal_amount, $new_msg);
        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $cus_id)
                ->first();
        $phone = $get_cus_phone->phone;

        $phone = explode(",", $phone);
        for ($i = 0; $i < count($phone); $i++) {
            SmsHelper::sendSms($phone[$i], $new_msg);
        }
    }

    public static function sendSms($phone, $content) {

        if (empty($phone) || empty($content)) {
            return;
        }

        $smsSetting = DB::table("tbl_app_config")
                ->where('setting', '=', "sms_gateway_url")
                ->first();
        if (empty($smsSetting) || empty($smsSetting->value)) {
            return;
        }

        $smsvalue = DB::table('tbl_app_config')
                        ->select('*')
                        ->where('setting', '=', 'sms_count')->first();
        $sms = $smsvalue->value;
        if ($sms > 0) {
            $url = str_replace("{{phoneno}}", $phone, $smsSetting->value);
            $url = str_replace("{{message}}", urlencode($content), $url);
            $con_status = SmsHelper::check_internet_connection('www.api.sendgrid.com');
            $app_stage = env('APP_STAGE');
            if ($app_stage == 'LIVE' && $con_status == 1) {
                try {

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_POSTFIELDS => "",
                    ));
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);

                    $send = DB::table('tbl_app_config')
                            ->where('setting', '=', 'sms_count')
                            ->decrement('value', 1);
                } catch (Guzzle\Http\Exception\ConnectException $e) {

                    return "SMS NOT SEND";
                }
            }
            return "smssend";
        } else {
            return "not";
        }
    }

    static function check_internet_connection($sCheckHost) {
        return (bool) @fsockopen($sCheckHost, 80, $iErrno, $sErrStr, 5);
    }

    public static function paymentDetails($sales, $type) {

        $currency = GeneralHelper::setCurrency();
        $thosandSeperator = GeneralHelper::getThosandSeperatorFormat();

        if ($type == "sales") {
            $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "sales_order_created_sms")
                    ->first();

            $msg = $get_template->message;

            $date = $sales['date']->format('Y-m-d');
            $totalAmount = $sales['total_amount'];
            $advance = DB::table("tbl_adv_payment")
                    ->select(DB::raw("SUM(amount) as amount"))
                    ->where('voucher_type', '=', "advance_payment")
                    ->where('voucher_no', '=', $sales['id'])
                    ->where('is_active', '=', 1)
                    ->groupBy('voucher_no')
                    ->first();
            $advAmount = 0;
            $balance = 0;

            if ($advance != "") {
                $advAmount = $advance;
                $balance = $totalAmount - $advance;
            } else {
                $balance = $totalAmount;
            }

            $new_msg = str_replace("{{date}}", $date, $msg);
            $new_msg = str_replace("{{sales_order_no}}", $sales['sales_order_no'], $new_msg);
            $new_msg = str_replace("{{totalAmount}}", $currency . '.' . $totalAmount, $new_msg);
            $new_msg = str_replace("{{advAmt}}", $currency . '.' . $advAmount, $new_msg);
            $new_msg = str_replace("{{balance}}", $currency . '.' . $balance, $new_msg);
            $get_cus_phone = DB::table("tbl_customer")
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $sales->customer_id)
                    ->first();
            $phone = '';
            if ($get_cus_phone != null) {
                $phone = $get_cus_phone->phone;
                $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
            }
            $phone = explode(",", $phone);
            for ($i = 0; $i < count($phone); $i++) {
                SmsHelper::sendSms($phone[$i], $new_msg);
            }
        } else if ($type == "advancePayment") {
            $get_templates = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "advance_payment_created_sms")
                    ->first();

            $msg = $get_templates->message;

            $totalAmount = $sales['amount'];
            $sales_order_id = DB::table("tbl_sales_order")
                    ->select('sales_order_no', 'total_amount')
                    ->where('id', '=', $sales['voucher_no'])
                    ->first();
            $advAmount = 0;
            $balance = 0;
            $advance = DB::table("tbl_adv_payment")
                    ->select(DB::raw("SUM(amount) as amount"))
                    ->where('voucher_type', '=', "advance_payment")
                    ->where('voucher_no', '=', $sales['voucher_no'])
                    ->where('is_active', '=', 1)
                    ->groupBy('voucher_no')
                    ->first();

            if ($advance != "") {
                $balance = $sales_order_id->total_amount - $advance->amount;
            } else {
                $balance = $totalAmount;
            }

            $new_msg = str_replace("{{totalAmount}}", $currency . '.' . $totalAmount, $msg);
            $new_msg = str_replace("{{sales_order_no}}", $sales_order_id->sales_order_no, $new_msg);
            $new_msg = str_replace("{{balance}}", $currency . '.' . $balance, $new_msg);
            $get_cus_phone = DB::table("tbl_customer")
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $sales->customer_id)
                    ->first();
            $phone = '';
            if ($get_cus_phone != null) {
                $phone = $get_cus_phone->phone;
                $new_msg = str_replace("{{customername}}", $get_cus_phone->fname, $new_msg);
            }
            $phone = explode(",", $phone);
            for ($i = 0; $i < count($phone); $i++) {
                SmsHelper::sendSms($phone[$i], $new_msg);
            }
        }
    }

    public static function customerWish($array, $type) {
        foreach ($array as $id) {
            if ($type == 'dob') {
                $get_template = DB::table("tbl_email_templates")
                        ->where('is_active', '=', 1)
                        ->where('tplname', '=', "customer_birthday_sms")
                        ->first();
            } elseif ($type == 'customer_special_date1') {
                $get_template = DB::table("tbl_email_templates")
                        ->where('is_active', '=', 1)
                        ->where('tplname', '=', "customer_anniversary_sms")
                        ->first();
            }

            $get_cus_id = DB::table('tbl_customer')
                    ->select('*')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $id)
                    ->first();

            $msg = $get_template->message;
            $cus_id = $get_cus_id->id;
            $new_msg = str_replace("{{fname}}", $get_cus_id->fname, $msg);
            $new_msg = str_replace("{{lname}}", $get_cus_id->lname, $new_msg);

            $get_cus_phone = DB::table("tbl_customer")
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $cus_id)
                    ->first();

            $phone = $get_cus_phone->phone;
            $phone = explode(",", $phone);
            for ($i = 0; $i < count($phone); $i++) {
                $msg = SmsHelper::sendSms($phone[$i], $new_msg);
            }
        }
        return $msg;
    }

}

?>
