<?php

namespace App\Helper;

use App\InventoryAdjustment;
use Carbon\Carbon;
use App\Inventory;
use App\Purchaseinvoiceitem;
use Illuminate\Support\Facades\Auth;
use DB;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InventoryHelper
 *
 * @author Deepa
 * 
 */
class InventoryHelper {

    //put your code here
    public static function increase($request, $code, $id, $type) {

        $product = $request;
        $total_stock = 0;
        if ($type == 'debit' || $type == 'credit') {
            $uom_id = $product['uom_id'];
            $product_sku = $product['product_sku'];
        }
        if ($type == 'debit_update') {
            $uom_id = $product->uom_id;
            $product_sku = $product->product_sku;
        }
        $currentuser = Auth::user();
        $get_uom_name = DB::table('tbl_uom')
                        ->select('name')
                        ->where('id', '=', $uom_id)->first();
        if ($type == 'debit')
            $product_id = $product['product_id'];
        if ($type == 'credit')
            $product_id = $product['internal_product_id'];
        if ($type == 'debit_update')
            $product_id = $product->internal_product_id;

        $inventory_adu = new InventoryAdjustment;

        $inventory_adu->product_id = $product_id;
        $get_product_det = DB::table('tbl_product')
                        ->select('has_inventory', 'is_active')
                        ->where('has_inventory', '=', '1')
                        ->where('is_active', '=', '1')
                        ->where('id', '=', $product_id)->get();

        $inventotyCollection = Inventory::where('product_id', "=", $product_id)->get();
        if (count($inventotyCollection) > 0) {
            $inventotyCollection = $inventotyCollection->first();
            if ($type == 'debit')
                $total_stock = $inventotyCollection->quantity - $product['qty'];
            if ($type == 'credit')
                $total_stock = $inventotyCollection->quantity + $product['qty'];
            if ($type == 'debit_update')
                $total_stock = $inventotyCollection->quantity - $product->qty;
            $inventotyCollection->quantity = $total_stock;
            $inventotyCollection->save();
        }
        if ($type == 'debit')
            $inventory_adu->quantity_debit = $product['qty'];
        if ($type == 'credit')
            $inventory_adu->quantity_credit = $product['qty'];
        if ($type == 'debit_update')
            $inventory_adu->quantity_debit = $product->qty;


        $inventory_adu->ref_id = $id;
        if ($code == 'purchase') {
            $inventory_adu->type = "purchase_create";
            $inventory_adu->comments = "From Purchase";
        }
        if ($code == 'update') {
            $inventory_adu->type = "purchase_update";
            $inventory_adu->comments = "From Purchase";
        }


        $inventory_adu->sku = $product_sku;
        $inventory_adu->uom_id = $uom_id;

        if (!empty($get_uom_name))
            $inventory_adu->uom_name = $get_uom_name->name;
        $inventory_adu->total_stock = $total_stock;

        $inventory_adu->created_by = $currentuser->id;
        $inventory_adu->updated_by = $currentuser->id;
        $inventory_adu->is_active = "1";
        $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $inventory_adu->save();
    }

    public static function decrease($id, $amount) {

        DB::table('tbl_accounts')->where('id', $id)
                ->update(['balance' => DB::raw('balance -' . $amount)]);
    }

    public static function purchaseStockAdjustDelete($id) {
        $invoiceItemCollection = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', '=', $id)->get();

        foreach ($invoiceItemCollection as $product) {

            $checkInvenAd = GeneralHelper::CheckInventoryAdjust($id, 'purchase_create',$product->product_id);
            if ($checkInvenAd->count() > 0) {
                if (array_key_exists('internal_product_id', $product)) {
                    if (!empty($product->internal_product_id)) {
                        $product_id = $product->internal_product_id;
                    } else {
                        $product_id = $product->product_id;
                    }
                } else {
                    $product_id = $product->product_id;
                }

                $get_product_det = DB::table('tbl_product')
                                ->select('has_inventory', 'is_active')
                                ->where('has_inventory', '=', '1')
                                ->where('is_active', '=', '1')
                                ->where('id', '=', $product_id)->get();
                if ($get_product_det->count() >= 1) {
                    $qty = $product->qty;
                    $get_inventory = Inventory::where('product_id', '=', $product_id)
                            ->first();

                    $get_inventory->quantity = $get_inventory->quantity - $qty;
                    $get_inventory->save();
                }
                DB::table("tbl_inventory_adjustment")
                        ->whereRaw(DB::Raw("type='purchase_create' or type = 'purchase_update'"))
                        ->where('ref_id', '=', $id)
                        ->update(['is_active' => 0]);
            }
            }
        }
    }

?>
