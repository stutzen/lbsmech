<?php

namespace App\Helper;

use Carbon\Carbon;
use App\Product;
use App\Invoiceitem;
use Illuminate\Support\Facades\Auth;
use DB;
use App\SalesOrder;
use App\Invoice;
use App\Purchaseinvoice;
use App\AdvancePayment;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Deepa
 */
class SalesInvoiceHelper {

    //put your code here

    public static function updateSalesOrderStatus($salesOrderId) {


        try {
            $sales = SalesOrder::findOrFail($salesOrderId);
            if ($sales->is_active == 1) {

                $salesorder_items = DB::table('tbl_sales_order_item')->where('sales_order_id', $salesOrderId)->get();
                $invoice_salesorder_items = DB::table('tbl_invoice_item as it')
                        ->leftJoin('tbl_invoice as i', 'i.id', '=', 'it.invoice_id')
                        ->where('i.is_active', 1)
                        ->where('i.quote_id', $salesOrderId)
                        ->select(DB::raw('sum(it.qty) as qty, it.product_id'))
                        ->groupBy('product_id')
                        ->get();

                if (count($invoice_salesorder_items) > 0) {

                    $hasFullyInvoiced = true;
                    foreach ($salesorder_items as $order_item) {

                        $hasFullyInvoiced = SalesInvoiceHelper::hasEqualedOrderedAndInvoiceQty($order_item, $invoice_salesorder_items);
                        if (!$hasFullyInvoiced) {
                            $hasFullyInvoiced = false;
                            break;
                        }
                    }
                    if ($hasFullyInvoiced) {
                        $sales->status = 'invoiced';
                    } else {
                        $sales->status = 'pending';
                    }
                    $sales->save();
                } else {

                    $sales->status = 'new';
                    $sales->save();
                }
            }
        } catch (ModelNotFoundException $e) {

            throw new Exception('Sales Order Id is not exist');
        }
    }

    public static function hasEqualedOrderedAndInvoiceQty($product, $invoiced_productCollction) {

        $retValue = false;
        foreach ($invoiced_productCollction as $invoiced_item) {

            if ($invoiced_item->product_id == $product->product_id) {

                if ($invoiced_item->qty >= $product->qty) {
                    $retValue = true;
                } else {
                    $retValue = false;
                }
                break;
            }
        }

        return $retValue;
    }

    public static function updateAdvPaymentStatusAndBalance($adv_payment_id) {
        try {
            $advance_payment = AdvancePayment::findOrFail($adv_payment_id);
            if ($advance_payment->is_active == 1) {
                if ($advance_payment->voucher_type == 'advance_invoice')  {
                $advanceamount = DB::table('tbl_payment')
                        ->where('adv_payment_id', $adv_payment_id)
                        ->where('is_active', '=', 1)
                        ->sum('amount');
                }
                if ($advance_payment->voucher_type == 'advance_purchase')  {
                $advanceamount = DB::table('tbl_purchase_payment')
                        ->where('adv_payment_id', $adv_payment_id)
                        ->where('is_active', '=', 1)
                        ->sum('amount');
                }
                if ($advance_payment->voucher_type == 'advance_invoice' || $advance_payment->voucher_type == 'advance_purchase') {
                    $balance = $advance_payment->amount - $advanceamount;

                    $advance_payment->balance_amount = $balance;
                    $advance_payment->used_amount = $advanceamount;
                    if ($balance > 0) {
                        $advance_payment->status = 'available';
                        $advance_payment->save();
                    } else if ($balance == 0) {
                        $advance_payment->status = 'used';
                        $advance_payment->save();
                    }
                }
            }
        } catch (ModelNotFoundException $e) {

            throw new Exception('Advance Payment Id is not exist');
        }
        //This is for balance update
    }

    public static function updateInvoiceStatus($invoiceId) {
        try {
            $invoice = Invoice::findOrFail($invoiceId);
            if ($invoice->is_active == 1) {

                $payment = DB::table('tbl_payment')->where('invoice_id', $invoiceId)->where('is_active', '=', 1)->sum('amount');
                $invoice->paid_amount = $payment;
                $totalamount = $invoice->total_amount;
                if (($totalamount <= $payment)) {
                    $invoice->status = 'paid';
                } elseif ($payment == 0) {
                    $invoice->status = 'unpaid';
                } elseif ($totalamount > $payment) {

                    $invoice->status = 'partiallyPaid';
                }
                $invoice->save();
            }
        } catch (ModelNotFoundException $e) {

            throw new Exception('Invoice Id is not exist');
        }
    }
    
    public static function updatePurchaseInvoiceStatus($invoiceId) {
        try {            
            $invoice = Purchaseinvoice::findOrFail($invoiceId); 
            if ($invoice->is_active == 1) {
                 $payment = DB::table('tbl_purchase_payment')->where('purchase_invoice_id', $invoiceId)->where('is_active', '=', 1)->sum('amount');
                $invoice->paid_amount = $payment;
                $totalamount = $invoice->total_amount;
                if (($totalamount <= $payment)) {
                    $invoice->status = 'paid';
                } elseif ($payment == 0) {
                    $invoice->status = 'unpaid';
                } elseif ($totalamount > $payment) {

                    $invoice->status = 'partiallyPaid';
                }
                $invoice->save();
            }
        } catch (ModelNotFoundException $e) {

            throw new Exception('Purchase Invoice Id is not exist');
        }
    }

}

?>
