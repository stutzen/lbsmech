<?php

namespace App\Helper;

use Log;
use DateTime;
use App\User;
use Carbon\Carbon;
use App\PersistentLogin;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\Message;
use Swift_Message;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;
use DB;
use App\Helper\GeneralHelper;
use PDO;
use PDOException;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Deepa
 */
class SSOHelper {

    //put your code here


    public static function getDatabaseDetail() {


        $domain = $_SERVER['SERVER_NAME'];
        $config = config('app');
        $db_path = $config['GET_BU_DB_PATH'];
        $json_loc = $db_path . "/" . $domain . '.json';

        $filecontent = '';
        $filename = $json_loc;
        $fh = fopen($filename, "r");
        while (!feof($fh)) {
            $filecontent .= fgets($fh);
        }
        fclose($fh);

        $json_a = json_decode($filecontent, true);

        $retval = array();

        $retval['host'] = $json_a['host'];
        $retval['database'] = $json_a['dbname'];
        $retval['password'] = $json_a['password'];
        $retval['username'] = $json_a['dbuser'];

        return $retval;
    }

    public static function businessList() {
        $currentUser = Auth::user();
        $retval = array();
        $retval['success'] = true;
        $retval['list'] = array();
        $businessList = array();
        if (!empty($currentUser)) {

            $domain = $_SERVER['SERVER_NAME'];

            Log::info('Stutzen SSOHelper.php domain = ' . $domain);

            $sso_server = getenv('DB_HOST');
            $sso_db_username = getenv('SSO_DB_USERNAME');
            $sso_db_password = getenv('SSO_DB_PASSWORD');
            $sso_db_name = getenv('SSO_DB_DATABASE');

            try {
                $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
            } catch (PDOException $e) {
                die("Connection failed: " . $conn->connect_error);
            }
            $sql = "SELECT bumap.bu_name, bu.company_name, bu.status, bu.type FROM tbl_user_bumapping as bumap left join tbl_bu as bu on bumap.bu_id = bu.id left join tbl_user as u on bumap.user_id = u.id WHERE u.username = '$currentUser->username' and bumap.is_active=1 order by bu_name asc ";
            if ($res = $conn->query($sql)) {

                Log::info('Stutzen $res->rowCount()  = ' . $res->rowCount());
                /* Check the number of rows that match the SELECT statement */
                if ($res->rowCount() > 0) {



                    foreach ($res as $row) {
                        $business = array();
                        $business['bu_name'] = $row['bu_name'];
                        $business['company_name'] = $row['company_name'];
                        $business['status'] = $row['status'];
                        $business['type'] = $row['type'];
                        array_push($businessList, $business);
                    }
                } else {

                    die("Connection failed: " . $conn->connect_error);
                }
            }
        }
        $retval['list'] = $businessList;

        return $retval;
    }
    public static function businessDelete($username){
        
            $domain = $_SERVER['SERVER_NAME'];
            $sso_server = getenv('DB_HOST');
            $sso_db_username = getenv('SSO_DB_USERNAME');
            $sso_db_password = getenv('SSO_DB_PASSWORD');
            $sso_db_name = getenv('SSO_DB_DATABASE');
            try {
                $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
            } catch (PDOException $e) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            $sql = "update tbl_user_bumapping bumapp left join tbl_bu b on bumapp.bu_id=b.id LEFT JOIN tbl_user u on bumapp.user_id=u.id set u.is_active=0,bumapp.is_active=0 where bumapp.bu_name='$domain' and u.username='$username' ";
            $res = $conn->query($sql);
            
            $sql1 = "delete from persistent_logins where username='$username' ";
             $conn->query($sql1);
            
        return $res;
    }

}

?>
