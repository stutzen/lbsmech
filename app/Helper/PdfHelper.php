<?php

namespace App\Helper;

use DateTime;
use App\User;
use App\Customer;
use Carbon\Carbon;
use App\PersistentLogin;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\Message;
use DB;
use App\Helper\GeneralHelper;
use mPDF;
use mpdfform;
use Log;
use ReplacePlaceHolder;
use ReplacePlaceHolderForSalesQuote;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use App\Http\Utility\CommonUtil;
require_once __DIR__.'/../../public/ReplacePlaceHolder.php';
require_once __DIR__.'/../../public/ReplacePlaceHolderForSalesQuote.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/* * 922
 * Description of Helper
 *
 * @author Deepa
 */

class PdfHelper {

    public static function pdfGenerate($no)
    {
        $replace = new ReplacePlaceHolder;
        $replace_msg = $replace->placeHolderFun($no,'for_pdf','','','');       

        $mpdf = new Mpdf();
        $mpdf->WriteHTML($replace_msg);

        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/pdf";
        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . "/" . $filePath;
        $mpdf->Output($filePath . '/invoice' . $no . '.pdf', 'F');
    }
    public static function pdfPreview($no)
    {
        $replace = new ReplacePlaceHolder;
        $replace_msg = $replace->placeHolderFun($no,'for_pdf','','','');       

        $mpdf = new Mpdf();
        $mpdf->WriteHTML($replace_msg);
        $mpdf->Output();
    }
    public static function quotePdf($no){
        $replace = new ReplacePlaceHolderForSalesQuote;
        $replace_msg = $replace->placeHolderFun($no, 'for_pdf', '', '', '');
        
        $mpdf = new Mpdf();
//        $mpdf->ignore_invalid_utf8 = true;
        $mpdf->WriteHTML($replace_msg); 
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/pdf";
        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . "/" . $filePath;
       $mpdf->Output($filePath . '/quote' . $no . '.pdf', 'F');
        
    }
    
    
    public static function AttachImage($id) {
        $attch_array = array();
        $d = DB::table('tbl_quote_item')->where('quote_id', '=', $id)->where('is_active', '=', 1);
        $detail = DB::table('tbl_quote_item')->where('quote_id', '=', $id)->where('is_active', '=', 1)->get();
        if($d->count() >0){
        foreach ($detail as $d) {
            $img = DB::table('tbl_attachment')->where('ref_id', '=', $d->product_id)->where('is_active', '=', 1)->where('type', '=', 'product')->first();
            if($img!=''){
            $objName = $img->url;
            $resp = CommonUtil::urlSignIn('GET', '', $objName);
            try{
                $data = file_get_contents($resp);
            }
            catch (Exception $e) {
                  //echo 'Message: ' . $e->getMessage();
                  // print_r("E".$e->getMessage());
             }
            Storage::put('/' . $objName, $data);
            array_push($attch_array, $img->url);
            }
        }
        }
        return $attch_array;
        //   Storage::put('product/'.$img->id.'jpg', file_get_contents($fileName));
        //$fileName='http://storage.googleapis.com/albumzen/product//64172-BaciFormaggio.jpg?Expires=1525524988&GoogleAccessId=stutzendev@stutzenme.iam.gserviceaccount.com&Signature=Tv2HhK65FlKngeG0u8ix1zAOaXBBJwxHPxhAEyFjJBKIbEb9ndfbdq6nqfNcbLZ3N6tm230lG1MKj9kw8ggvmsgRAQlEZ8%2FIv3FmCmCA9wz8lr%2Fe9HZikyTf%2BJLjNiRdGCb0JCYvrTzKkhPsiU9dxexniWbDLIoTyB3IV1hjwX4YqovxZvSJLfFe9Y4mjUwWW2yaIlJFmIspLdV7BAOK%2BNQS%2Bm8rBDyu1tG98ZL9iw1imE%2FMiPSWoeBIlasUER454jT8GXAq6PKHnubjaZ%2FpdlmWWtofC21z6BWCvFu5BrgBTUu7bIdg%2Bz%2FzRtOt5Ldm69TIpCBT3kwAZXlJa%2FPb%2BQ%3D%3D';
        //$url = "http://" . $serverName . "/public/urlsign/url?contentype=&verb=GET&objName=" . 'product%2F%2F64172-BaciFormaggio.jpg';
        //Storage::put($file->getClientOriginalName(), storage_path($file));
        //  $extension = $file->getClientOriginalExtension();
        // Storage::disk('local')->put('BaciFormaggio'.'jpg', File::get('https://storage.googleapis.com/albumzen/product//64172-BaciFormaggio.jpg?Expires=1525524988&GoogleAccessId=stutzendev@stutzenme.iam.gserviceaccount.com&Signature=Tv2HhK65FlKngeG0u8ix1zAOaXBBJwxHPxhAEyFjJBKIbEb9ndfbdq6nqfNcbLZ3N6tm230lG1MKj9kw8ggvmsgRAQlEZ8%2FIv3FmCmCA9wz8lr%2Fe9HZikyTf%2BJLjNiRdGCb0JCYvrTzKkhPsiU9dxexniWbDLIoTyB3IV1hjwX4YqovxZvSJLfFe9Y4mjUwWW2yaIlJFmIspLdV7BAOK%2BNQS%2Bm8rBDyu1tG98ZL9iw1imE%2FMiPSWoeBIlasUER454jT8GXAq6PKHnubjaZ%2FpdlmWWtofC21z6BWCvFu5BrgBTUu7bIdg%2Bz%2FzRtOt5Ldm69TIpCBT3kwAZXlJa%2FPb%2BQ%3D%3D'));
        //Storage::putFile('photos.jpeg', new File('https://storage.googleapis.com/albumzen/product//64172-BaciFormaggio.jpg?Expires=1525524988&GoogleAccessId=stutzendev@stutzenme.iam.gserviceaccount.com&Signature=Tv2HhK65FlKngeG0u8ix1zAOaXBBJwxHPxhAEyFjJBKIbEb9ndfbdq6nqfNcbLZ3N6tm230lG1MKj9kw8ggvmsgRAQlEZ8%2FIv3FmCmCA9wz8lr%2Fe9HZikyTf%2BJLjNiRdGCb0JCYvrTzKkhPsiU9dxexniWbDLIoTyB3IV1hjwX4YqovxZvSJLfFe9Y4mjUwWW2yaIlJFmIspLdV7BAOK%2BNQS%2Bm8rBDyu1tG98ZL9iw1imE%2FMiPSWoeBIlasUER454jT8GXAq6PKHnubjaZ%2FpdlmWWtofC21z6BWCvFu5BrgBTUu7bIdg%2Bz%2FzRtOt5Ldm69TIpCBT3kwAZXlJa%2FPb%2BQ%3D%3D'));
        // Storage::put($filePath, 'https://storage.googleapis.com/albumzen/product//64172-BaciFormaggio.jpg?Expires=1525524988&GoogleAccessId=stutzendev@stutzenme.iam.gserviceaccount.com&Signature=Tv2HhK65FlKngeG0u8ix1zAOaXBBJwxHPxhAEyFjJBKIbEb9ndfbdq6nqfNcbLZ3N6tm230lG1MKj9kw8ggvmsgRAQlEZ8%2FIv3FmCmCA9wz8lr%2Fe9HZikyTf%2BJLjNiRdGCb0JCYvrTzKkhPsiU9dxexniWbDLIoTyB3IV1hjwX4YqovxZvSJLfFe9Y4mjUwWW2yaIlJFmIspLdV7BAOK%2BNQS%2Bm8rBDyu1tG98ZL9iw1imE%2FMiPSWoeBIlasUER454jT8GXAq6PKHnubjaZ%2FpdlmWWtofC21z6BWCvFu5BrgBTUu7bIdg%2Bz%2FzRtOt5Ldm69TIpCBT3kwAZXlJa%2FPb%2BQ%3D%3D');
        //   Storage::put('https://storage.googleapis.com/albumzen/product//64172-BaciFormaggio.jpg?Expires=1525524988&GoogleAccessId=stutzendev@stutzenme.iam.gserviceaccount.com&Signature=Tv2HhK65FlKngeG0u8ix1zAOaXBBJwxHPxhAEyFjJBKIbEb9ndfbdq6nqfNcbLZ3N6tm230lG1MKj9kw8ggvmsgRAQlEZ8%2FIv3FmCmCA9wz8lr%2Fe9HZikyTf%2BJLjNiRdGCb0JCYvrTzKkhPsiU9dxexniWbDLIoTyB3IV1hjwX4YqovxZvSJLfFe9Y4mjUwWW2yaIlJFmIspLdV7BAOK%2BNQS%2Bm8rBDyu1tG98ZL9iw1imE%2FMiPSWoeBIlasUER454jT8GXAq6PKHnubjaZ%2FpdlmWWtofC21z6BWCvFu5BrgBTUu7bIdg%2Bz%2FzRtOt5Ldm69TIpCBT3kwAZXlJa%2FPb%2BQ%3D%3D', storage_path($filePath));
    }

}

?>