<?php

namespace App\Helper;

use DateTime;
use App\User;
use Carbon\Carbon;
use App\PersistentLogin;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\Message;
use Swift_Message;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;
use DB;
use Log;
use App\Helper\GeneralHelper;
use Swift_Attachment;
use SendGrid;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Deepa
 */
class MailHelper {

    //put your code here
    public function invoiceMailSend($request) {
      
        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_created_mail")
                ->first();
        $configureDateFormat = GeneralHelper::getDateFormat();
        //print_r($get_template);
        $msg = $get_template->message;
        $invoice_id = $request->invoice_id;
        $invoice = DB::table("tbl_invoice")
                ->where('is_active', '=', 1)
                ->where('id', '=', $invoice_id)
                ->first();
         
         
        /*$thous_sep = GeneralHelper::getThosandSeperatorFormat();
        $currency = GeneralHelper::setCurrency();

        $inv_total = GeneralHelper::convertNumberFormat($invoice->total_amount, $thous_sep);
        $amount = $currency . ". " .$inv_total;
        
        $date = $invoice->duedate;

        //print_r($msg);
        $new_msg = str_replace("{{invoice_id}}", $invoice->invoice_code, $msg);

        $new_msg = str_replace("{{invoice_amount}}", $amount, $new_msg);

        $new_msg = str_replace("{{duedate}}", GeneralHelper::formatDate($date, $configureDateFormat), $new_msg);

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $invoice->customer_id)
                ->first();


        $get_addtr = DB::table("tbl_app_config")
                ->where('setting', '=', 'app_title')
                ->first();
         *
         */
        $new_msg =  "welcome";
        $company = "Sdemo";
        $from = $request->from;
        $cc = $request->cc;
        $email = $request->to;


        $new_msg = str_replace("{{companyname}}", $company, $new_msg);
        $subject = $request->subject;

        $subject = str_replace("{{invoice_id}}", $invoice->invoice_code, $subject);

        $attch_array = array();

        if ($request->attahed_pdf_copy == 1) {
            $serverName = $_SERVER['SERVER_NAME'];
            $attachment_path = getcwd() . "/download/" . $serverName . "/pdf/invoice" . $request->invoice_id . ".pdf";

            array_push($attch_array, $attachment_path);
        }
        $mail_obj = new MailHelper;

        $mail_obj->sendMail($new_msg, $subject, $from, $email, $cc, $attch_array);
    }

    public function userInivitationNotification($user) {


        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "user_invitation_mail")
                ->first();
        $currentUser = Auth::user();
        $msg = $get_template->message;

        $configureDateFormat = GeneralHelper::getDateFormat();

        $get_addtr = DB::table("tbl_app_config")
                ->where('setting', '=', 'app_title')
                ->first();
        $invited_by = $currentUser->f_name;
        $domain = $_SERVER['SERVER_NAME'];
        $companyname = $get_addtr->value;
        $link = getenv('USER_INVITE_URL');
        $link = str_replace("{{domain}}", $domain, $link);
        $link = str_replace("{{user}}", $user->username, $link);
        $link1 = str_replace("http://", '', $link);
        //$img = "http://".$domain."/resources/assets/img/logo.png";
        $img = "http://localhost/lbs/resources/assets/img/logo.png";
        $new_msg = str_replace("{{invited_by}}", $invited_by, $msg);
        $new_msg = str_replace("{{logo}}", $img, $new_msg);
        $new_msg = str_replace("{{company_name}}", $companyname, $new_msg);
        $new_msg = str_replace("{{link}}", $link, $new_msg);
        $new_msg = str_replace("{{link1}}", $link1, $new_msg);

        $new_msg = str_replace("{{domain}}", $domain, $new_msg);
        $new_msg = str_replace("{{user}}", $currentUser->f_name, $new_msg);



        $subject_msg = $get_template->subject;
        $subject_msg = str_replace("{{invited_by}}", $invited_by, $subject_msg);
        $subject_msg = str_replace("{{company_name}}", $companyname, $subject_msg);
        $subject_msg = str_replace("{{link}}", $link, $subject_msg);
        $subject_msg = str_replace("{{domain}}", $domain, $subject_msg);


        $from = $currentUser->username;
        $cc = $currentUser->username;
        $email = $user->username;
        $attch_array = array();

        $mail_obj = new MailHelper;
        $mail_obj->sendMail($new_msg, $subject_msg, $from, $email, $cc, $attch_array);
    }

    public function invoiceCreatedNotification($invoice) {
        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_created_mail")
                ->first();

        //print_r($get_template);
        $msg = $get_template->message;
        
        $thous_sep = GeneralHelper::getThosandSeperatorFormat();
        $currency = GeneralHelper::setCurrency();
        
        $inv_total = GeneralHelper::convertNumberFormat($invoice->total_amount,$thous_sep);

        $amount = $currency . ". " .$inv_total;
        
        $date = $invoice->duedate;
        $configureDateFormat = GeneralHelper::getDateFormat();
        //print_r($msg);
        $new_msg = str_replace("{{invoice_id}}", $invoice->invoice_code, $msg);

        $new_msg = str_replace("{{invoice_amount}}", $amount, $new_msg);

        $new_msg = str_replace("{{duedate}}", GeneralHelper::formatDate($date, $configureDateFormat), $new_msg);

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $invoice->customer_id)
                ->first();

        $get_from = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicefrom')
                ->first();
        $get_cc = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicecc')
                ->first();

        $get_addtr = DB::table("tbl_app_config")
                ->where('setting', '=', 'app_title')
                ->first();

        $from = $get_from->value;
        $company = $get_addtr->value;
        $cc = $get_cc->value;
        $email = $get_cus_phone->email;

        $new_msg = str_replace("{{companyname}}", $company, $new_msg);
        $subject = $get_template->subject;

        $subject = str_replace("{{invoice_id}}", $invoice->invoice_code, $subject);

        $attch_array = array();

        $serverName = $_SERVER['SERVER_NAME'];
        $attachment_path = getcwd() . "/download/" . $serverName . "/pdf/invoice" . $invoice->id . ".pdf";

        array_push($attch_array, $attachment_path);
        $mail_obj = new MailHelper;

        $mail_obj->sendMail($new_msg, $subject, $from, $email, $cc, $attch_array);
    }

    public function invoicePaymentNotification($payment) {
        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_payment_mail")
                ->first();

        //print_r($get_template);
        $msg = $get_template->message;
        $thous_sep = GeneralHelper::getThosandSeperatorFormat();
        $currency = GeneralHelper::setCurrency();
        
        $pay_amount = GeneralHelper::convertNumberFormat($payment->amount,$thous_sep);
        $amount = $currency . ". " .$pay_amount;
        $date = $payment->date;
        $invoice_id = $payment->invoice_id;
        $sales_order_id  = $payment->sales_order_id;
        $configureDateFormat = GeneralHelper::getDateFormat();
        $date = GeneralHelper::formatDate($date, $configureDateFormat);

        if (!empty($invoice_id)) {
            $invoice_code_det = DB::table("tbl_invoice")
                    ->where('id', '=', $invoice_id)
                    ->first();

            $invoice_prefix = $invoice_code_det->invoice_code;
            $new_msg = str_replace("{{invoice_id}}", $invoice_prefix, $msg);
        } 
        else if (!empty($sales_order_id)) {
            $new_msg = str_replace("{{invoice_id}}", $payment->sales_order_id, $msg);
        }
        else
            $new_msg = str_replace("{{invoice_id}}", $payment->quote_id, $msg);
        //print_r($msg);
        $new_msg = str_replace("{{paymentid}}", $payment->id, $new_msg);

        $new_msg = str_replace("{{payment_amount}}", $amount, $new_msg);

        $new_msg = str_replace("{{duedate}}", $date, $new_msg);

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $payment->customer_id)
                ->first();

        $get_from = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicefrom')
                ->first();
        $get_cc = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicecc')
                ->first();
        $get_addtr = DB::table("tbl_app_config")
                ->where('setting', '=', 'app_title')
                ->first();

        $from = $get_from->value;
        $cc = $get_cc->value;
        $company = $get_addtr->value;
        $email = $get_cus_phone->email;
        $new_msg = str_replace("{{companyname}}", $company, $new_msg);
        $subject = $get_template->subject;

        if (!empty($invoice_id))
            $subject = str_replace("{{invoice_id}}", $invoice_prefix, $subject);
        else if (!empty($sales_order_id))  {
            $subject = str_replace("{{invoice_id}}", $payment->sales_order_id, $subject);
            $subject = $subject . "   " . "For Sales Order";
        }
        else {
            $subject = str_replace("{{invoice_id}}", $payment->quote_id, $subject);
            $subject = $subject . "   " . "For QUOTE";
        }

        $attachment = array();
        $mail_obj = new MailHelper;
        $mail_obj->sendMail($new_msg, $subject, $from, $email, $cc, $attachment);
    }

    public function invoiceReminderNotification($id) {
        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "invoice_reminder_mail")
                ->first();
        $thous_sep = GeneralHelper::getThosandSeperatorFormat();
        $currency = GeneralHelper::setCurrency();
        
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        $invoicedetails = DB::table("tbl_invoice")
                        ->where('id', "=", $id)->first();
        $customer_id = $invoicedetails->customer_id;
        $invoice_no = $invoicedetails->invoice_no;
        $invoice_code = $invoicedetails->invoice_code;
        $cus_addr = $invoicedetails->customer_address;
        $customerdetails = DB::table("tbl_customer")
                        ->where('id', "=", $customer_id)->first();
        $cus_name = $customerdetails->fname . "  " . $customerdetails->lname;
        $cus_phone = $customerdetails->phone;

        $cus_email = $customerdetails->email;

        $date = GeneralHelper::formatDate($invoicedetails->date, $configureDateFormat);
        $status = $invoicedetails->status;
        $no1 = $invoicedetails->invoice_code;
        ;
        $duedate = GeneralHelper::formatDate($invoicedetails->duedate, $configureDateFormat);
        $total_amount = $invoicedetails->total_amount;
        $discount = $invoicedetails->discount_amount;
        $subtotal = $invoicedetails->subtotal;
        $tax = $invoicedetails->tax_amount;
        $new_total_amount = ( $subtotal + $tax ) - $discount;

        $paid_amount = $invoicedetails->paid_amount;
        $balance = $new_total_amount - $paid_amount;
        $balance = floor($balance);
        $balance = GeneralHelper::convertNumberFormat($balance,$thous_sep);
        $get_cus_id = DB::table("tbl_invoice")
                ->where('id', '=', $id)
                ->first();

        $cus_id = $get_cus_id->customer_id;
        $configureDateFormat = GeneralHelper::getDateFormat();
        $amount =$currency . ". " . $balance;
        $duedate = GeneralHelper::formatDate($get_cus_id->duedate, $configureDateFormat);
        $date = GeneralHelper::formatDate($get_cus_id->date, $configureDateFormat);

        //print_r($get_template);
        $msg = $get_template->message;
        //print_r($msg);
        //$invoice_id = $payment->invoice_id;

        $invoice_code_det = DB::table("tbl_invoice")
                ->where('id', '=', $id)
                ->first();

        $invoice_prefix = $invoice_code_det->invoice_code;

        //print_r($msg);

        $new_msg = str_replace("{{invoice_id}}", $invoice_prefix, $msg);



        $new_msg = str_replace("{{invoice_amount}}", $amount, $new_msg);

        $new_msg = str_replace("{{duedate}}", $duedate, $new_msg);
        $new_msg = str_replace("{{date}}", $date, $new_msg);

        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $cus_id)
                ->first();

        $get_from = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicefrom')
                ->first();
        $get_cc = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicecc')
                ->first();
        $get_addtr = DB::table("tbl_app_config")
                ->where('setting', '=', 'app_title')
                ->first();

        $from = $get_from->value;
        $cc = $get_cc->value;
        $company = $get_addtr->value;
        $email = $get_cus_phone->email;
        $new_msg = str_replace("{{companyname}}", $company, $new_msg);

        $subject = $get_template->subject;


        $subject = str_replace("{{invoice_id}}", $invoice_prefix, $subject);
        $attch_array = array();

        $serverName = $_SERVER['SERVER_NAME'];
        $attachment_path = getcwd() . "/download/" . $serverName . "/pdf/invoice" . $id . ".pdf";

        array_push($attch_array, $attachment_path);

        $attachment = array();
        $mail_obj = new MailHelper;
        $mail_obj->sendMail($new_msg, $subject, $from, $email, $cc, $attch_array);
    }

    public function sendMail($new_msg, $var, $from, $to, $cc, $attachment) {

        if (empty($from) || empty($to)) {
            return;
        }

        $app_stage = env('APP_STAGE');
        $mail_obj = new MailHelper;
        $con_status = $mail_obj->check_internet_connection('www.api.sendgrid.com');
        $cc1 = array();
        if (!empty($cc)) {
            $cc1 = explode(",", $cc);
        }
        if ($app_stage == 'LIVE' && $con_status == 1) {

            $api_key = env('SENDGRID_API_KEY');
//            $client = new HttpClient();
//            $this->transport = new SendgridTransport($client, $api_key);
//            $message = new Message($this->getMessage($new_msg, $var));
//            $message->from($from, $company)
//                    ->to($email, 'To');
//            for ($index = 0; $index < count($cc1); $index++) {
//
//                $message->addcc($cc1[$index], $cc1[$index]);
//            }
            $sendgrid = new SendGrid($api_key);



            $email = new SendGrid\Email();
            $email
                    ->addTo($to)
                    ->setFrom($from)
                    ->setSubject($var)
                    ->setHtml($new_msg);

            for ($index = 0; $index < count($attachment); $index++) {
                $email->addAttachment($attachment[$index]);
            }

            for ($index = 0; $index < count($cc1); $index++) {

                $email->addcc($cc1[$index]);
            }
            $sendgrid->send($email);
        }
    }

    public function AlbumPermissionCreated($album_code, $customer_id) {
        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "album_permission_create")
                ->first();

        //print_r($get_template);
        $msg = $get_template->message;

        $new_msg = str_replace("{{code}}", $album_code, $msg);


        $get_cus_phone = DB::table("tbl_customer")
                ->where('is_active', '=', 1)
                ->where('id', '=', $customer_id)
                ->first();

        $get_from = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicefrom')
                ->first();
        $get_cc = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicecc')
                ->first();

        $get_addtr = DB::table("tbl_app_config")
                ->where('setting', '=', 'app_title')
                ->first();

        $from = $get_from->value;
        $company = $get_addtr->value;
        $cc = $get_cc->value;
        $to = $get_cus_phone->email;

        $new_msg = str_replace("{{companyname}}", $company, $new_msg);
        $subject = $get_template->subject;


        $attch_array = array();


        $mail_obj = new MailHelper;

        $mail_obj->sendMail($new_msg, $subject, $from, $to, $cc, $attch_array);
    }

    function check_internet_connection($sCheckHost) {
        return (bool) @fsockopen($sCheckHost, 80, $iErrno, $sErrStr, 5);
    }

    public function getMessage($new_msg, $var) {
        return new Swift_Message($var, $new_msg);
    }
    
    public function DealMailSend($dealrequest){
        $value=array();
        
        $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "deal_create_mail")
                    ->first();

            $deal = DB::table('tbl_deal as d')
                            ->leftjoin('tbl_contact as c', 'd.contact_id', '=', 'c.id')
                            ->leftjoin('tbl_employee as e', 'd.emp_id', '=', 'e.id')
                            ->select('d.amount', 'e.user_id', DB::raw("concat(c.fname,'',c.lname) as contact_name"), DB::raw("concat(e.f_name,'',e.l_name) as employee_name"))
                            ->where('d.id', '=', $dealrequest->id)->first();

            $con_name = $deal->contact_name;
            $emp_name = $deal->employee_name;
            $deal_amount = $deal->amount;

            $content = $get_template->message;
            $new_msg = str_replace("{{dealid}}", $dealrequest->id, $content);
            $new_msg = str_replace("{{customer_name}}", $con_name, $new_msg);
            $new_msg = str_replace("{{emp_name}}", $emp_name, $new_msg);
            $new_msg = str_replace("{{deal_amount}}", $deal_amount, $new_msg);
        
        $get_from = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicefrom')
                ->first();
        $get_cc = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicecc')
                ->first();

        $get_addtr = DB::table("tbl_app_config")
                ->where('setting', '=', 'app_title')
                ->first();
        
        $user=DB::table('tbl_user')->where('rolename','=','admin')->where('is_active','=',1)->get();
        
       foreach ($user as $val) {
           array_push($value,$val->email);
        }
        
       
        if(!empty($deal->user_id)){
           $user_id=DB::table('tbl_user')->where('id','=',$deal->user_id)->first();
           array_push($value,$user_id->email);
        }
        
        foreach($value as $mail){
        $from = $get_from->value;
        $company = $get_addtr->value;
        $cc = $get_cc->value;
        $to = $mail;

        $new_msg = str_replace("{{companyname}}", $company, $new_msg);
        $subject = $get_template->subject;

        $attch_array = array();

        $mail_obj = new MailHelper;

        $mail_obj->sendMail($new_msg, $subject, $from, $to, $cc, $attch_array);
            
        }
        
        
    }
    
    public function quoteCreatedNotificationMail($estimate,$imgs) {
        $get_template = DB::table("tbl_email_templates")
                ->where('is_active', '=', 1)
                ->where('tplname', '=', "estimate_created_mail")
                ->first();

        //print_r($get_template);
        $msg = $get_template->message;
        
        $thous_sep = GeneralHelper::getThosandSeperatorFormat();
        $currency = GeneralHelper::setCurrency();
        
        $inv_total = GeneralHelper::convertNumberFormat($estimate->total_amount,$thous_sep);

        $amount = $currency . ". " .$inv_total;
        
        //$date = $invoice->duedate;
        $configureDateFormat = GeneralHelper::getDateFormat();
        //print_r($msg);
        $new_msg = str_replace("{{estimate_id}}", $estimate->id, $msg);

        $new_msg = str_replace("{{estimate_amount}}", $amount, $new_msg);
        
       // $new_msg = str_replace("{{duedate}}", GeneralHelper::formatDate($date, $configureDateFormat), $new_msg);

        $get_cus_phone = DB::table("tbl_contact")
                ->where('is_active', '=', 1)
                ->where('id', '=', $estimate->contact_id)
                ->first();

        $get_from = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicefrom')
                ->first();
        $get_cc = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoicecc')
                ->first();

        $get_addtr = DB::table("tbl_app_config")
                ->where('setting', '=', 'app_title')
                ->first();

        $from = $get_from->value;
        $company = $get_addtr->value;
        $cc = $get_cc->value;
        $email = $get_cus_phone->email;
        $new_msg = str_replace("{{companyname}}", $company, $new_msg);
        $subject = $get_template->subject;

        $subject = str_replace("{{estimate_id}}", $estimate->id, $subject);

        $attch_array = array();

        $serverName = $_SERVER['SERVER_NAME'];
        $attachment_path = getcwd() . "/download/" . $serverName . "/pdf/quote" . $estimate->id . ".pdf";
        foreach ($imgs as $d) {
           $contents =  storage_path('/app/'.$d);
           array_push($attch_array, $contents);
        }
        array_push($attch_array, $attachment_path);
        $mail_obj = new MailHelper;

        $mail_obj->sendMail($new_msg, $subject, $from, $email, $cc, $attch_array);
    }


}

?>