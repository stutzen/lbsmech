<?php

namespace App\Helper;
use Carbon\Carbon;

use DB;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountHelper
 *
 * @author Deepa
 */
class AccountHelper {
    //put your code here
    public static function deposit ($id , $amount){
      
 DB::table('tbl_accounts')->where('id', $id)
         ->update(['balance' =>DB::raw('balance +'. $amount)]);
         
         
      
    }
    public static function withDraw ($id ,$amount){
               
 DB::table('tbl_accounts')->where('id', $id)
         ->update(['balance' =>DB::raw('balance -'. $amount)]);
    }
    
    public static function addTransaction($credit , $debit,$account_id){
        if (!empty($credit) || $credit>0) {
            $account = AccountHelper::deposit($account_id, $credit);
        }
    
    if (!empty($debit) || ($debit>0)) {
        $account = AccountHelper::withDraw($account_id, $debit);
    }
}
}

?>
