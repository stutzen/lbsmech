<?php

namespace App\Helper;

use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Auth;
use DB;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Deepa
 */
class AuthorizationHelper {

    //put your code here

    public static function check($screen_code, $screen_action) {
        $currentuser = Auth::user();
        if (empty($screen_code)) {
            return true;
        } elseif ($currentuser->role_id == 0) {
            //Superadmin Role
            return true;
        }
        $role_id = $currentuser->role_id;
        $builder = DB::table('tbl_role_access')
                ->select('is_create', 'is_modify', 'is_view', 'is_delete')
                ->where('role_id', "=", $role_id)
                ->where('screen_code', "=", $screen_code)
                ->where('is_active', "=", 1)
                ->get();
        if ($builder->count() == 0) {
            return false;
        } else {
            $builder = $builder->first();
            if ($screen_action == 'save') {
                if ($builder->is_create == 1)
                    return true;
                else
                    return false;
            }
            else if ($screen_action == 'update') {
                if ($builder->is_modify == 1)
                    return true;
                else
                    return false;
            }
            else if ($screen_action == 'delete') {
                if ($builder->is_delete == 1)
                    return true;
                else
                    return false;
            }
            else if ($screen_action == 'list') {
                if ($builder->is_view == 1)
                    return true;
                else
                    return false;
            }
        }
    }
    
    public static function checkVisblityAccess($screenCOde) {
        $currentuser = Auth::user();
        $role_id = $currentuser->role_id;
        $builder = DB::table('tbl_role_access')
                ->select('*')
                ->where('role_id', "=", $role_id)
                ->where('screen_code', "=", $screenCOde)
                ->where('is_active', "=", DB::Raw("'1'"))
                ->where('is_visible_to', "=", DB::Raw("'All'"))
                ->get();
        if ($builder->count() == 0) {
            return false;
        } else {
            return true;
        }
    }

}

?>
