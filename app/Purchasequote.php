<?php
namespace App;
use Illuminate\Database\Eloquent\Model;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Quotepurchaseorder
 *
 * @author Deepa
 */
class Purchasequote extends Model {
    
    //put your code here
     protected  $table ="tbl_purchase_quote";
    protected  $fillable = [ 'customer_id','prefix','date','validuntil','datepaid','subtotal','discount_mode','discount_amount','credit_amount','round_off'
        ,'tax_amount','total_amount','status','paymentmethod','notes','is_active','purchase_quote_id','customer_address','created_at','created_by',
        'updated_at','updated_at','date_sent','date_accepted','advance_amount','discount_percentage','tax_id'];
     protected $dates = [ 'created_at','updated_at','date','validuntil','date_sent','date_accepted' ];
}

?>
