<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Country extends Model {
    
    protected $table = 'tbl_country' ;
    protected $fillable = ['code', 'name','dual_language'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>