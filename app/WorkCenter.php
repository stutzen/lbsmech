<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class WorkCenter extends Model {
    
    protected $table = 'tbl_work_center' ;
    protected $fillable = ['name','code','updated_by','created_by','is_active','comments'];
    protected $dates = ['created_at', 'updated_at'];

}
