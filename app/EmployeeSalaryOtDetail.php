<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeSalaryOtDetail extends Model {
    
    protected $table = 'tbl_emp_salary_ot_detail' ;
    protected $fillable = ['emp_salary_id','ot_id','allowance_master_id','amount'
        ,'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

