<?php

use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Support\Facades\Session;

//use Illuminate\Http\Request;
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$app->get('/', function () use ($app) {
    return $app->version();
});
$app->group(['prefix' => 'customReport'], function (Laravel\Lumen\Application $app) {
    $app->Get('/view', 'CustomizedReportController@customReport');
});

$app->group(['prefix' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('login', 'AuthController@login');
    $app->post('ssologin', 'AuthController@ssologin');
    $app->post('convertSSOTokenToAPIToken', 'AuthController@convertSSOTokenToAPIToken');
});

$app->group(['prefix' => 'auth', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('logout', 'AuthController@logout');
    $app->get('userInfo', 'AuthController@userInfo');
});

$app->group(['prefix' => 'company'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'CompanyController@save');
});

//product
$app->group(['prefix' => 'product', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'ProductController@save');
    $app->GET('/listAll', 'ProductController@listAll');

    $app->PUT('/modify/{id:[\d]+}', 'ProductController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'ProductController@delete');
});
//Uom
$app->group(['prefix' => 'uom', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'UomController@save');
    $app->GET('/listAll', 'UomController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'UomController@update');
    $app->get('/listByIdDesc', 'UomController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'UomController@delete');
});
//Contact
$app->group(['prefix' => 'contact', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'ContactController@save');
    $app->GET('/listAll', 'ContactController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'ContactController@update');
    $app->get('/listByIdDesc', 'ContactController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'ContactController@delete');
    $app->Get('/search', 'ContactController@search');
    $app->Get('/detail/{id:[\d]+}', 'ContactController@detail');
});
//country
$app->group(['prefix' => 'city', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'CityController@save');
    $app->GET('/listAll', 'CityController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'CityController@update');
    $app->get('/listByIdDesc', 'CityController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'CityController@delete');
});
$app->group(['prefix' => 'country', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'CountryController@save');
    $app->PUT('/update/{id:[\d]+}', 'CountryController@update');
    $app->GET('/listAll', 'CountryController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'CountryController@delete');
});
$app->group(['prefix' => 'state', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'StateController@save');
    $app->GET('/listAll', 'StateController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'StateController@delete');
    $app->PUT('/update/{id:[\d]+}', 'StateController@update');
});
//Album
$app->group(['prefix' => 'urlsign'], function (\Laravel\Lumen\Application $app) {

    $app->GET('/url', 'UrlSignController@url');
});
//Album
$app->group(['prefix' => 'album', 'middleware' => 'auth', 'permissions' => 'access'], function (\Laravel\Lumen\Application $app) {

    $app->post('/permissionCheck', 'AlbumController@permissionCheck');
    $app->post('/save', 'AlbumController@save');
    $app->GET('/listAll', 'AlbumController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumController@update');

    $app->DELETE('/delete/{id:[\d]+}', 'AlbumController@delete');
});
$app->group(['prefix' => 'album'], function (\Laravel\Lumen\Application $app) {
    $app->GET('/listAll', 'AlbumController@listAll');
    $app->get('/detail', 'AlbumController@detail');
});


//AlbumPermission
$app->group(['prefix' => 'albumpermission', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('/save', 'AlbumPermissionController@save');
    $app->Get('/listAll', 'AlbumPermissionController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumPermissionController@delete');
});
//Albumasset       
$app->group(['prefix' => 'albumasset', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetController@save');
    $app->Get('/listAll', 'AlbumassetController@listAll');
//    $app->Get('/detail', 'QuoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumassetController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumassetController@delete');
});
$app->group(['prefix' => 'albumasset'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetController@save');
    $app->Get('/listAll', 'AlbumassetController@listAll');
});
//Albumassetcomments       
$app->group(['prefix' => 'albumassetcomments', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetcommentsController@save');
    $app->Get('/listAll', 'AlbumassetcommentsController@listAll');
//    $app->Get('/detail', 'QuoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumassetcommentsController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumassetcommentsController@delete');
});
$app->group(['prefix' => 'albumassetcomments'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetcommentsController@save');
    $app->Get('/listAll', 'AlbumassetcommentsController@listAll');
});
$app->group(['prefix' => 'uom', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'UomController@save');
    $app->GET('/listAll', 'UomController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'UomController@update');
    $app->get('/listByIdDesc', 'UomController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'UomController@delete');
});
//UserRole
$app->group(['prefix' => 'role', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'RoleController@save');
    $app->GET('/listAll', 'RoleController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'RoleController@update');
    $app->get('/listByIdDesc', 'RoleController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'RoleController@delete');
});
//UserRole
$app->group(['prefix' => 'paymentTerm', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'PaymentTermController@save');
    $app->GET('/listAll', 'PaymentTermController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'PaymentTermController@update');
    $app->get('/listByIdDesc', 'PaymentTermController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'PaymentTermController@delete');
});
//Customer
$app->group(['prefix' => 'customer', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'CustomerController@save');
    $app->Get('/listAll', 'CustomerController@listAll');
    $app->Get('/detail', 'CustomerController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'CustomerController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'CustomerController@delete');
    $app->Get('/search', 'CustomerController@search');
    $app->Get('/wishList', 'CustomerController@wishList');
    $app->post('/sendSms', 'CustomerController@sendSms');
});
//Customer Dual Info
$app->group(['prefix' => 'customerdualinfo', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'CustomerDualController@save');
    $app->Get('/listAll', 'CustomerDualController@listAll');
    $app->Get('/detail', 'CustomerDualController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'CustomerDualController@update');
    $app->Get('/search', 'CustomerDualCustomerController@search');
});
//Invoices
$app->group(['prefix' => 'invoice', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'InvoiceController@save');
    $app->Get('/listAll', 'InvoiceController@listAll');
    $app->Get('/list_All', 'InvoiceController@list_All');
    
    $app->GET('/serialNo', 'InvoiceController@serialNoDuplication');
    $app->GET('/serialNoSearch', 'InvoiceController@serialNoSearch');
    
    $app->GET('/salesinvoicejson_csvexport', 'InvoiceController@salesinvoicejson_csvexport');
    $app->Get('/detail', 'InvoiceController@detail');
    $app->get('/invoicecreate', 'InvoiceController@invoicecreate');
    $app->PUT('/modify/{id:[\d]+}', 'InvoiceController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'InvoiceController@delete');
    $app->Get('/invoiceReminder/{id:[\d]+}', 'InvoiceController@invoiceReminder');
    $app->Get('/pdfPreview/{id:[\d]+}', 'InvoiceController@pdfPreview');
    $app->Get('/salesTaxReport', 'InvoiceController@salesTaxReport');

    $app->Get('/salesTaxReportSummery', 'InvoiceController@salesTaxReportSummery');
    $app->Get('/summary', 'InvoiceController@summary');

    $app->Get('/receivable_list', 'InvoiceController@receivable_list');
    $app->POST('/invoiceMailSend', 'InvoiceController@invoiceMailSend');
    $app->Get('/customerItemList', 'InvoiceController@customerItemList');
    $app->Get('/machineInfo', 'InvoiceController@machineInfo');
});
//Albumasset       
$app->group(['prefix' => 'albumasset', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetController@save');
    $app->Get('/listAll', 'AlbumassetController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumassetController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumassetController@delete');
});
//Albumassetcomments       
$app->group(['prefix' => 'albumassetcomments', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AlbumassetcommentsController@save');
    $app->Get('/listAll', 'AlbumassetcommentsController@listAll');
//    $app->Get('/detail', 'QuoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'AlbumassetcommentsController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AlbumassetcommentsController@delete');
});
//Quotes        
$app->group(['prefix' => 'quote', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'QuoteController@save');
    $app->Get('/listAll', 'QuoteController@listAll');
    $app->Get('/detail', 'QuoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'QuoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'QuoteController@delete');
    $app->Get('/sendEmail/{id:[\d]+}', 'QuoteController@sendEmail');
});

//Sales Order       
$app->group(['prefix' => 'salesorder', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'SalesOrderController@save');
    $app->Get('/listAll', 'SalesOrderController@listAll');
    $app->Get('/detail', 'SalesOrderController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'SalesOrderController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'SalesOrderController@delete');
});

//QuotePurchaseOrder
$app->group(['prefix' => 'purchasequote', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchasequoteController@save');
    $app->Get('/listAll', 'PurchasequoteController@listAll');
    $app->Get('/detail', 'PurchasequoteController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'PurchasequoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'PurchasequoteController@delete');
});


//QuotePurchaseOrder
$app->group(['prefix' => 'purchaseinvoice', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchaseinvoiceController@save');

    $app->Get('/listAll', 'PurchaseinvoiceController@listAll');
    $app->Get('/detail', 'PurchaseinvoiceController@detail');
    $app->PUT('/modify/{id:[\d]+}', 'PurchaseinvoiceController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'PurchaseinvoiceController@delete');
    $app->Get('/payable_list', 'PurchaseinvoiceController@payable_list');
    $app->Get('/invoiceDetail', 'PurchaseinvoiceController@invoiceDetail');
});

//Quotesitems        
$app->group(['prefix' => 'quotesitem', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'QuotesitemController@save');
    $app->Get('/listAll', 'QuotesitemController@listAll');

    $app->PUT('/modify/{id:[\d]+}', 'QuotesitemController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'QuotesitemController@delete');
});

//Purchasequotesitems        
$app->group(['prefix' => 'purchasequoteitem', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchasequoteitemController@save');
    $app->Get('/listAll', 'PurchasequoteitemController@listAll');

    $app->PUT('/modify/{id:[\d]+}', 'PurchasequoteitemController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'PurchasequoteitemController@delete');
});

//Purchaseinvoiceitems        
$app->group(['prefix' => 'purchaseinvoiceitem', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'PurchaseinvoiceitemController@save');
    $app->Get('/listAll', 'PurchaseinvoiceitemController@listAll');

    $app->PUT('/modify/{id:[\d]+}', 'PurchaseinvoiceitemController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'PurchaseinvoiceitemController@delete');
});


//Attributestypes
$app->group(['prefix' => 'attributestype', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->Get('/listAll', 'AttributestypeController@listAll');
});

//Screen
$app->group(['prefix' => 'screen', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->Get('/listAll', 'ScreenController@listAll');
});
//Appconfig
$app->group(['prefix' => 'appconfig', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('/modify', 'ApconfigController@update');
    $app->Get('/listAll', 'ApconfigController@listAll');
});
//userscreenacess
$app->group(['prefix' => 'roleacess', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('/modify', 'RoleAccessController@update');
    $app->Get('/listAll', 'RoleAccessController@listAll');
});

//Attribute
$app->group(['prefix' => 'attribute', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'AttributeController@save');
    $app->Get('/listAll', 'AttributeController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'AttributeController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AttributeController@delete');
});
//Tax
$app->group(['prefix' => 'tax', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'TaxController@save');
    $app->Get('/listAll', 'TaxController@listAll');
    $app->Get('/activeListAll', 'TaxController@expiredListAll');
    $app->PUT('/modify/{id:[\d]+}', 'TaxController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'TaxController@delete');
    $app->get('/calculate', 'TaxController@calculate');
    $app->Get('/groupdetail', 'TaxController@groupDetail');
});
//Debitnote
$app->group(['prefix' => 'debitnote', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'DebitnoteController@save');
    $app->Get('/listAll', 'DebitnoteController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'DebitnoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DebitnoteController@delete');
});
//Credit Node
$app->group(['prefix' => 'creditnote', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'CreditnoteController@save');
    $app->Get('/listAll', 'CreditnoteController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'CreditnoteController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'CreditnoteController@delete');
});


$app->group(['prefix' => 'user', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('list', 'UserController@index');
    $app->post('save', 'UserController@save');
    $app->post('userInvite', 'UserController@userInvite');
    $app->put('update/{id}', 'UserController@update');
    $app->delete('delete/{id}', 'UserController@delete');
    $app->GET('/listingAll', 'UserController@listAll');
    $app->post('/changePwd', 'UserController@changePwd');
    $app->GET('/businessList', 'UserController@businessList');
    $app->put('updateLocation/{id}', 'UserController@updateLocation');
});


//CLIENT
$app->group(['prefix' => 'client', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/save', 'ClientController@save');
    $app->PUT('/modify/{id:[\d]+}', 'ClientController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'ClientController@delete');
    $app->GET('/listAll', 'ClientController@listAll');
    $app->GET('/listWithLimit', 'ClientController@listWithLimit');
    $app->GET('/listById/{id:[\d]+}', 'ClientController@listById');
    $app->GET('/index', 'UserController@index1');
});

//CATEGORY
$app->group(['prefix' => 'category', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'CategoryController@save');
    $app->PUT('/modify/{id:[\d]+}', 'CategoryController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'CategoryController@delete');
    $app->GET('/listWithLimit', 'CategoryController@listWithLimit');
    $app->get('/listByIdNameDesc', 'CategoryController@listByIdNameDesc');
    $app->GET('/listAll', 'CategoryController@listAll');
    $app->GET('/listBYIdName', 'CategoryController@listBYIdName');
    $app->GET('/{id:[\d]+}', [
        'as' => 'tbl_category.listById',
        'uses' => 'CategoryController@listById'
    ]);
    $app->post('listAll', 'CategoryController@listAll');
});

//DEVICE
$app->group(['prefix' => 'device', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'DeviceController@save');
    $app->PUT('/modify/{id:[\d]+}', 'DeviceController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DeviceController@delete');
    $app->GET('/listAll', 'DeviceController@listAll');
    $app->GET('/deviceValidation', 'DeviceController@validation');
});



$app->put('/company', 'CompanyController@save');
$app->get('/company', 'CompanyController@listAll');
$app->get('/company/listwithlimit', 'CompanyController@listWithLimit');
$app->get('/company/customlist', 'CompanyController@customlist');
$app->get('/company/listAll', 'CompanyController@listAll');


//payment
$app->group(['prefix' => 'payment', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('listAll', 'PaymentController@listAll');
    $app->post('save', 'PaymentController@save');
    $app->put('modify/{id}', 'PaymentController@update');
    $app->delete('delete/{id}', 'PaymentController@delete');
    $app->post('updateSettledamt', 'PaymentController@updateSettledamt');
});

//payment
$app->group(['prefix' => 'purchasepayment', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('listAll', 'PurchasePaymentController@listAll');
    $app->post('save', 'PurchasePaymentController@save');
    $app->put('modify/{id}', 'PurchasePaymentController@update');
    $app->delete('delete/{id}', 'PurchasePaymentController@delete');
    $app->post('updateSettledamt', 'PurchasePaymentController@updateSettledamt');
});


$app->group(['prefix' => 'system', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->get('save', 'SystemController@index');
});
////Account
//$app->group(['prefix' => 'account', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
//
//    $app->get('list', 'PaymentController@index');
//    $app->post('withdraw', 'AccountController@withdrawSave');
//    $app->put('update/{id}', 'PaymentController@update');
//    $app->delete('delete/{id}', 'PaymentController@delete');
//    $app->post('updateSettledamt', 'PaymentController@updateSettledamt');
//    $app->GET('listAll', 'AccountController@listAll');
//});
//Expense

$app->group(['prefix' => 'expense', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('save', 'ExpenseController@save');
    $app->GET('listAll', 'ExpenseController@listAll');
    $app->put('update/{id}', 'ExpenseController@update');

    $app->Get('/detail', 'ExpenseController@detail');
});

//TransationCategory

$app->group(['prefix' => 'transationcategory', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('save', 'TransationCategoryController@save');
    $app->GET('listAll', 'TransationCategoryController@listAll');
    $app->put('update/{id}', 'TransationCategoryController@update');
    $app->DELETE('delete/{id}', 'TransationCategoryController@delete');
    $app->Get('/detail', 'TransationCategoryController@detail');
});
//Transation
$app->group(['prefix' => 'advancepayment', 'middleware' => 'auth'], function(\Laravel\Lumen\Application $app) {
    $app->POST('save', 'AdvancePaymentController@save');
    $app->GET('listAll', 'AdvancePaymentController@listAll');
    $app->put('update/{id}', 'AdvancePaymentController@update');
    $app->DELETE('delete/{id}', 'AdvancePaymentController@delete');
    $app->GET('purchaseAdvanceBalance', 'AdvancePaymentController@purchaseAdvanceBalance');
    $app->put('creditDebtUpdate/{id}', 'AdvancePaymentController@creditDebtUpdate');

});
$app->group(['prefix' => 'paymentallotment', 'middleware' => 'auth'], function(\Laravel\Lumen\Application $app) {
    $app->POST('save', 'AdvPaymentAllotmentController@save');
    $app->GET('listAll', 'AdvPaymentAllotmentController@listAll');
    $app->put('update/{id}', 'AdvPaymentAllotmentController@update');
    $app->DELETE('delete/{id}', 'AdvPaymentAllotmentController@delete');
});
$app->group(['prefix' => 'transaction', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->POST('save', 'TransactionController@save');
    $app->POST('transferSave', 'TransactionController@transferSave');
    $app->GET('transferList', 'TransactionController@transferList');
    $app->DELETE('transferDelete/{id}', 'TransactionController@transferDelete');

    $app->GET('listAll', 'TransactionController@listAll');
    $app->POST('listAllByDate', 'TransactionController@listAllByDate');
    $app->DELETE('delete/{id}', 'TransactionController@delete');
    $app->Get('/detail', 'TransactionController@detail');
    $app->GET('listByIncome', 'TransactionController@listAllByIncome');
    $app->GET('listBYExpense', 'TransactionController@listAllBYExpanse');
    $app->DELETE('transactionIncomeDelete/{id}', 'TransactionController@transactionIncomeDelete');
    $app->DELETE('transactionExpenseDelete/{id}', 'TransactionController@transactionExpenseDelete');

    $app->GET('accountStatement', 'TransactionController@accountStatement');
    $app->GET('acodeStatement', 'TransactionController@acodeStatement');
});

//EmailTemplates

$app->group(['prefix' => 'emailTemplates', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->put('update', 'EmailTemplatesController@update');
    $app->GET('listAll', 'EmailTemplatesController@listAll');
});



//Transation

$app->group(['prefix' => 'fiscalyear', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {


    $app->GET('listAll', 'FiscalYearController@listAll');
});

//ActivityLogController
$app->group(['prefix' => 'activitylog', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {


    $app->GET('listAll', 'ActivityLogController@listAll');
});
//FiscalMonth
$app->group(['prefix' => 'fiscalmonth', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {


    $app->GET('listAll', 'FiscalMonthController@listAll');
});

//Dashboard
$app->group(['prefix' => 'dashboard', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Get('/InvoiceVsPayment/{id:[\d]+}', 'DashBoardController@InvoiceVsPayment');
    $app->Get('/InvoicePurchaseVsPayment/{id:[\d]+}', 'DashBoardController@InvoicePurchaseVsPayment');
    $app->Get('/IncomeVsExpenses/{id:[\d]+}', 'DashBoardController@IncomeVsExpenses');
    $app->GET('/categoryVsIncomeSummery/{id:[\d]+}', 'DashBoardController@categoryVsIncomeSummery');
    $app->GET('/categoryVsExpenseSummery/{id:[\d]+}', 'DashBoardController@categoryVsExpenseSummery');
    $app->GET('/incomeVsExpenseSummery', 'DashBoardController@incomeVsExpenseSummery');
    $app->Get('/SalesPayment/{id:[\d]+}', 'DashBoardController@SalesPayment');
    $app->Get('/SalesPurchaseMonthlyReport/{id:[\d]+}', 'DashBoardController@SalesPurchaseMonthlyReport');
    $app->Get('/salesMonthReport', 'DashBoardController@salesMonthReport');
    $app->Get('/salesMonthTotalReport/{id:[\d]+}', 'DashBoardController@salesMonthTotalReport');
    $app->Get('/todayIncomeVsExpenses', 'DashBoardController@todayIncomeVsExpenses');
    $app->Get('/AgeingReport', 'DashBoardController@AgeingReport');
});
//StockManagement
$app->group(['prefix' => 'stock', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/StockAdjustment', 'StockManagmentController@StockAdjustment');
    $app->Post('/StockWastage', 'StockManagmentController@StockWastage');
    $app->GET('/InventoryAdjustment_list', 'StockManagmentController@InventoryAdjustment_list');
    $app->GET('/listAll', 'StockManagmentController@listAll');
    $app->GET('/MinStocklist', 'StockManagmentController@MinStocklist');
});
//Reports
$app->group(['prefix' => 'reports', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->GET('ItemWiseReport', 'ReportsController@ItemWiseReport');
    $app->GET('ItemWisePurchaseReport', 'ReportsController@ItemWisePurchaseReport');
    $app->GET('ItemReport', 'ReportsController@ItemReport');
    $app->GET('PartyWiseSalesReport', 'ReportsController@PartyWiseSalesReport');
    $app->GET('PartyWisePurchaseReport', 'ReportsController@PartyWisePurchaseReport');
    $app->GET('ProductionReport', 'ReportsController@ProductionReport');
    $app->GET('/userBasedReport', 'ReportsController@assignDealBasedReport');
    $app->GET('/createdDealReport', 'ReportsController@createdDealReport');
    $app->GET('/stageWiseReport', 'ReportsController@stageWiseReport');
    $app->GET('/dealTypeBasedReport', 'ReportsController@dealTypeBasedReport');
    $app->GET('/dealCityBasedReport', 'ReportsController@dealCityBasedReport');
    $app->GET('/dealNameBasedReport', 'ReportsController@dealNameBasedReport');
    $app->GET('/dailyActivity', 'ReportsController@dailyActivity');
    $app->GET('/inactiveDeals', 'ReportsController@inactiveDeals');
    $app->GET('/dealWithoutLead', 'ReportsController@dealWithoutLead');
     $app->GET('/salesOrderPendingReport', 'ReportsController@SalesOrderPendingReport');
      $app->GET('/customerReport', 'ReportsController@customerReport');
      $app->GET('/MaterialRecuritmentReport', 'ReportsController@MaterialRecuritmentReport');
});
//Employee Team 
$app->group(['prefix' => 'employeeteam', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'EmployeeTeamController@save');
    $app->GET('/listAll', 'EmployeeTeamController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'EmployeeTeamController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeTeamController@delete');
});

//Employee  
$app->group(['prefix' => 'employee', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'EmployeeController@save');
    $app->GET('/listAll', 'EmployeeController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'EmployeeController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeController@delete');
    $app->GET('/workingDayCount', 'EmployeeController@workingDayCount');
    $app->GET('/empProductionInfo', 'EmployeeController@empProductionInfo');
});

//Deal Activty
$app->group(['prefix' => 'dealactivity', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'DealActivityController@save');
    $app->GET('/listAll', 'DealActivityController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'DealActivityController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DealActivityController@delete');
});

//Deal Stage
$app->group(['prefix' => 'dealstage', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'DealStageController@save');
    $app->GET('/listAll', 'DealStageController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'DealStageController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DealStageController@delete');
});

//Deal 
$app->group(['prefix' => 'deal', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'DealController@save');
    $app->GET('/listAll', 'DealController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'DealController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'DealController@delete');
});

$app->group(['prefix' => 'general', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/nextInvoiceNo', 'GeneralController@findCode');
});

//Accounts
$app->group(['prefix' => 'accounts', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'AccountController@save');
    $app->GET('/listAll', 'AccountController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'AccountController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AccountController@delete');
    $app->PUT('/accounthelper', 'AccountController@accounthelper');
});

$app->group(['prefix' => 'acode', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'AcodeController@save');
    $app->GET('/listAll', 'AcodeController@listAll');
    $app->GET('/search', 'AcodeController@search');
    $app->PUT('/update/{id:[\d]+}', 'AcodeController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AcodeController@delete');
    $app->GET('/search', 'AcodeController@search');
    $app->GET('/acodeReport', 'AcodeController@acodeReport');
});

//csv download api
$app->GET('/product/product_csvexport', 'ProductController@product_csvexport');
$app->Get('/customer/customerdetails_exportcsv', 'CustomerController@customerdetails_exportcsv');
$app->GET('/invoice/invoice_csvexport', 'InvoiceController@invoice_csvexport');
$app->GET('/invoice/invoicelist_csvexport', 'InvoiceController@invoicelist_csvexport');
$app->GET('/invoice/salesTaxReport_csvexport', 'InvoiceController@salesTaxReport_csvexport');
$app->Get('/invoice/salesTaxReportSummery_csvexport', 'InvoiceController@salesTaxReportSummery_csvexport');
$app->GET('/invoice/receivablereport_csvexport', 'InvoiceController@receivablereport_csvexport');
$app->GET('/transaction/accountstatement_csvexport', 'TransactionController@accountstatement_csvexport');
$app->GET('/transaction/acodestatement_csvexport', 'TransactionController@acodestatement_csvexport');
$app->GET('/transaction/ExpenseReport_csvexport', 'TransactionController@ExpenseReport_csvexport');
$app->GET('/reports/itemizedreport_csvexport', 'ReportsController@itemizedreport_csvexport');
$app->GET('/reports/PartyWiseSalesReport_csvexport', 'ReportsController@PartyWiseSalesReport_csvexport');
$app->GET('/reports/PartyWisePurchaseReport_csvexport', 'ReportsController@PartyWisePurchaseReport_csvexport');
$app->Get('/payablereport_csvexport', 'PurchaseinvoiceController@payablereport_csvexport');
$app->Get('/partywise_sales_csvexport', 'PurchaseinvoiceController@partywise_sales_csvexport');
//CLIENT

$app->post('/client/save', 'ClientController@save');
$app->put('client/update/{id:[\d]+}', 'ClientController@update');
$app->delete('/client/delete/{id:[\d]+}', 'ClientController@delete');
$app->get('/client/listWithLimit', 'ClientController@listWithLimit');
$app->get('/client/listAll', 'ClientController@listAll');
$app->get('/client/listByName', 'ClientController@listByName');
$app->get('/client/listById/{id:[\d]+}', 'ClientController@listById');

$app->post('uploadfile', 'FileuploadController@saveFile');
$app->post('uploadlogo', 'FileuploadController@saveLogo');

//Employee  
$app->group(['prefix' => 'employee', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->post('/save', 'EmployeeController@save');
    $app->GET('/listAll', 'EmployeeController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'EmployeeController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeController@delete');
    $app->GET('/workingDayCount', 'EmployeeController@workingDayCount');
    $app->GET('/empProductionInfo', 'EmployeeController@empProductionInfo');
});

//DeliveryInstruction
$app->group(['prefix' => 'deliveryInstruction', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/save', 'DeliveryInstructionController@save');
    $app->PUT('/update/{id:[\d]+}', 'DeliveryInstructionController@update');
    $app->GET('/listAll', 'DeliveryInstructionController@listAll');
    $app->GET('/detail', 'DeliveryInstructionController@detail');
    $app->GET('/getUnMappedInvoice', 'DeliveryInstructionController@getUnMappedInvoice');
    $app->DELETE('/delete/{id:[\d]+}', 'DeliveryInstructionController@delete');
});

//DeliveryInstructionDetail
$app->group(['prefix' => 'deliveryInstructionDetail', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/detailSave', 'DeliveryInstructionDetailController@detailSave');
    $app->POST('/delete', 'DeliveryInstructionDetailController@delete');
    $app->GET('/listAll', 'DeliveryInstructionDetailController@listAll');
});

//Attendance
$app->group(['prefix' => 'attendance', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/punchIn', 'AttendanceController@punchIn');
    $app->POST('/punchInMe', 'AttendanceController@punchInMe');
    $app->GET('/listAll', 'AttendanceController@listAll');
    $app->GET('/currentStatus', 'AttendanceController@currentStatus');
    $app->GET('/attendancedetails', 'AttendanceController@attendancedetails');
    $app->GET('/totalWorkingReport', 'AttendanceController@totalWorkingReport');
    $app->POST('/attendanceSave', 'AttendanceController@attendanceSave');
});

//Transport
$app->group(['prefix' => 'transport', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->POST('/save', 'TransportController@save');
    $app->PUT('/update/{id:[\d]+}', 'TransportController@update');
    $app->GET('/listAll', 'TransportController@listAll');
    $app->GET('/search', 'TransportController@search');
    $app->DELETE('/delete/{id:[\d]+}', 'TransportController@delete');
});

//Bom
$app->group(['prefix' => 'bom', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'BomController@save');
    $app->put('update/{id}', 'BomController@update');
    $app->Get('/listAll', 'BomController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'BomController@delete');
    $app->Get('/detail', 'BomController@detail');
});


//Production
$app->group(['prefix' => 'empProduction', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'EmployeeProductionController@save');
    $app->Get('/listAll', 'EmployeeProductionController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'EmployeeProductionController@update');
    $app->Get('/detail', 'EmployeeProductionController@detail');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeProductionController@delete');
    $app->Get('/empSalary', 'EmployeeProductionController@empSalary');
    $app->Post('/empSalarySave', 'EmployeeProductionController@empSalarySave');
    $app->Get('/productionDetail', 'EmployeeProductionController@productionDetail');
    $app->Get('/empSalaryDetail', 'EmployeeProductionController@empSalaryDetail');
    $app->Get('/noProdutionEnterList', 'EmployeeProductionController@noProdutionEnterList');
});
//Employee Bonus

$app->group(['prefix' => 'employeeBonus', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'EmployeeBonusController@save');
    $app->put('update/{id}', 'EmployeeBonusController@update');
    $app->Get('/listAll', 'EmployeeBonusController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeBonusController@delete');
    $app->Get('/empBonus', 'EmployeeBonusController@empBonus');
    $app->Get('/empBonusList', 'EmployeeBonusController@empBonusList');
});

//Employee Advance

$app->group(['prefix' => 'employeeAdvance', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'EmployeeAdvanceController@save');
    $app->put('update/{id}', 'EmployeeAdvanceController@update');
    $app->Get('/listAll', 'EmployeeAdvanceController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeAdvanceController@delete');
    $app->Get('EmployeeAdvanceReport', 'EmployeeAdvanceController@EmployeeAdvanceReport');
    $app->Get('EmployeeBalanceReport', 'EmployeeAdvanceController@EmployeeBalanceReport');
});

//Employee Advance Deduction

$app->group(['prefix' => 'employeeAdvDeduct', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'EmployeeAdvDeductionController@save');
    $app->put('update/{id}', 'EmployeeAdvDeductionController@update');
    $app->Get('/listAll', 'EmployeeAdvDeductionController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeAdvDeductionController@delete');
});

//Employee Allowance

$app->group(['prefix' => 'employeeAllowance', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'EmployeeAllowanceController@save');
    $app->put('update/{id}', 'EmployeeAllowanceController@update');
    $app->Get('/listAll', 'EmployeeAllowanceController@listAll');
    $app->post('/delete', 'EmployeeAllowanceController@delete');
});

// Allowance Master
$app->group(['prefix' => 'allowanceMaster', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'AllowanceMasterController@save');
    $app->Get('/listAll', 'AllowanceMasterController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'AllowanceMasterController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'AllowanceMasterController@delete');
});

// Employee Task Activities
$app->group(['prefix' => 'empTaskActivities', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'EmployeeTaskActivitiesController@save');
    $app->Post('/listAll', 'EmployeeTaskActivitiesController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'EmployeeTaskActivitiesController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeTaskActivitiesController@delete');
});

// Comments
$app->group(['prefix' => 'comments', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'CommentsController@save');
    $app->Get('/listAll', 'CommentsController@listAll');
    $app->PUT('/update', 'CommentsController@update');
    $app->DELETE('/delete', 'CommentsController@delete');
});

// Employee Task Type
$app->group(['prefix' => 'empTaskType', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'EmployeeTaskTypeController@save');
    $app->Get('/listAll', 'EmployeeTaskTypeController@listAll');
    $app->PUT('/update/{id:[\d]+}', 'EmployeeTaskTypeController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'EmployeeTaskTypeController@delete');
});

// Employee Salary Preparation
$app->group(['prefix' => 'empsalary', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Get('/listAll', 'EmployeeSalaryPreparationController@listAll');
});

// Employee PF Statement
$app->group(['prefix' => 'empPF', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Get('/listAll', 'EmployeePFStatementController@listAll');
});

// Employee ESI Statement
$app->group(['prefix' => 'empESI', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Get('/listAll', 'EmployeeESIStatementController@listAll');
});

// Employee ESI Statement
$app->group(['prefix' => 'empBonusPrepare', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Get('/listAll', 'EmployeeBonusPreparationController@listAll');
});

// Work Order
$app->group(['prefix' => 'workOrder', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'WorkOrderController@save');
    $app->PUT('/update/{id:[\d]+}', 'WorkOrderController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'WorkOrderController@delete');
    $app->Get('/listAll', 'WorkOrderController@listAll');
    $app->Get('/inputList', 'WorkOrderController@inputList');
    $app->Get('/detailList', 'WorkOrderController@detailList');
    $app->Get('/detail', 'WorkOrderController@detail');
    $app->PUT('/orderPlan/{id:[\d]+}', 'WorkOrderController@WorkOrderPlan');
    $app->Get('/orderDetail', 'WorkOrderController@workOrderDetailList');
    $app->Get('/orderInput', 'WorkOrderController@workOrderInputList');
    $app->Get('/inventoryUpdate', 'WorkOrderController@inventoryUpdate');
    $app->Get('/produced', 'WorkOrderController@produced');
    $app->post('/sessionSave', 'WorkOrderController@orderSession');
    $app->get('/sessionList', 'WorkOrderController@sessionList');
    $app->put('/orderStatusUpdate/{id:[\d]+}', 'WorkOrderController@orderStatusUpdate');
    $app->put('/markAsDone/{id:[\d]+}', 'WorkOrderController@markAsDone');
    $app->put('/checkAvailablity/{id:[\d]+}', 'WorkOrderController@checkAvailablity');
    $app->POST('/reservingQty', 'WorkOrderController@reservingQty');
     $app->Get('/OrderPendingList', 'WorkOrderController@workOrderPendingList');
     $app->Post('/customerAssignOrder', 'WorkOrderController@customerAssignOrder');
      $app->put('/workOrderUpdate/{id:[\d]+}', 'WorkOrderController@workOrderUpdate');
      $app->get('/moPrint', 'WorkOrderController@moPrint');
});

// Source
$app->group(['prefix' => 'source', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'SourceController@save');
    $app->PUT('/update/{id:[\d]+}', 'SourceController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'SourceController@delete');
    $app->Get('/listAll', 'SourceController@listAll');
});

// Work Order
$app->group(['prefix' => 'grn', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'GrnController@save');
    $app->PUT('/update/{id:[\d]+}', 'GrnController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'GrnController@delete');
    $app->Get('/listAll', 'GrnController@listAll');
    $app->Get('/detail', 'GrnController@detail');
    $app->GET('GrnReport', 'GrnController@GrnReport');
});

//Task 
$app->group(['prefix' => 'task', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'TaskController@save');
    $app->GET('/listAll', 'TaskController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'TaskController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'TaskController@delete');
});

//Task Type
$app->group(['prefix' => 'taskType', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'TaskTypeController@save');
    $app->GET('/listAll', 'TaskTypeController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'TaskTypeController@update');
    $app->get('/listByIdDesc', 'TaskTypeController@listByIdDesc');
    $app->DELETE('/delete/{id:[\d]+}', 'TaskTypeController@delete');
});

//Task  Activity
$app->group(['prefix' => 'taskActivity', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'TaskActivityController@save');
    $app->post('/listAll', 'TaskActivityController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'TaskActivityController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'TaskActivityController@delete');
});

//Task  Activity Comments
$app->group(['prefix' => 'taskActivityComments', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'TaskActivityCommentsController@save');
    $app->GET('/listAll', 'TaskActivityCommentsController@listAll');
    $app->PUT('/update', 'TaskActivityCommentsController@update');
    $app->DELETE('/delete', 'TaskActivityCommentsController@delete');
});

//company

$app->group(['prefix' => 'company', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'CompanyController@save');
    $app->PUT('/update/{id:[\d]+}', 'CompanyController@update');
    $app->GET('/listAll', 'CompanyController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'CompanyController@delete');
});

//Service Request

$app->group(['prefix' => 'serviceRequest', 'miserviceRequsddleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'ServiceRequestController@save');
    $app->PUT('/update/{id:[\d]+}', 'ServiceRequestController@update');
    $app->GET('/listAll', 'ServiceRequestController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'ServiceRequestController@delete');
    $app->GET('/detail', 'ServiceRequestController@detail');
});

//Lead Type

$app->group(['prefix' => 'leadType', 'miserviceRequsddleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'LeadTypeController@save');
    $app->PUT('/update/{id:[\d]+}', 'LeadTypeController@update');
    $app->GET('/listAll', 'LeadTypeController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'LeadTypeController@delete');
});
// Device Detail
$app->group(['prefix' => 'deviceDetail', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'DeviceDetailController@save');
    $app->PUT('/update/{id:[\d]+}', 'DeviceDetailController@update');
    $app->Get('/listAll', 'DeviceDetailController@listAll');
    $app->get('/delete', 'DeviceDetailController@delete');
});

// Device Notifiction
$app->group(['prefix' => 'notification', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->get('/notificationSend', 'NotificationController@notificationSend');
});

// Amc
$app->group(['prefix' => 'amc', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'AmcController@save');
    $app->PUT('/update/{id:[\d]+}', 'AmcController@update');
    $app->Get('/listAll', 'AmcController@listAll');
    $app->delete('/delete/{id:[\d]+}', 'AmcController@delete');
    $app->Get('/serialNoCheck', 'AmcController@serialNoCheck');
});

// Expense Tracker 
$app->group(['prefix' => 'ExpenseTracker', 'middleWare' => 'auth'], function (\Laravel\Lumen\Application $app) {
    $app->Post('/save', 'ExpenseTrackerController@save');
    $app->PUT('/update/{id:[\d]+}', 'ExpenseTrackerController@update');
    $app->Get('/listAll', 'ExpenseTrackerController@listAll');
    $app->delete('/delete/{id:[\d]+}', 'ExpenseTrackerController@delete');
    $app->Post('/StatusUpdate', 'ExpenseTrackerController@StatusUpdate');
    $app->Get('/Report', 'ExpenseTrackerController@Report');
    $app->GET('/userBasedReport', 'ExpenseTrackerController@userBasedReport');
});


$app->group(['prefix' => 'allowanceMapping', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/update', 'AllowanceMappingController@update');
    $app->get('/listAll', 'AllowanceMappingController@listAll');
    $app->get('/mappingList', 'AllowanceMappingController@mappingList');
});


//DEal Followers
$app->group(['prefix' => 'dealFollowers', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'DealFollowersController@save');
    $app->GET('/listAll', 'DealFollowersController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'DealFollowersController@delete');
});

$app->group(['prefix' => 'journal', 'middleware' => 'auth'], function (\Laravel\Lumen\Application $app) {

    $app->post('/save', 'JournalController@save');
    $app->GET('/listAll', 'JournalController@listAll');
    $app->PUT('/modify/{id:[\d]+}', 'JournalController@update');
    $app->get('/detail', 'JournalController@detail');
    $app->DELETE('/delete/{id:[\d]+}', 'JournalController@delete');
});


//Task Group
$app->group(['prefix' => 'taskGroup', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'TaskGroupController@save');
    $app->put('update/{id}', 'TaskGroupController@update');
    $app->Get('/listAll', 'TaskGroupController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'TaskGroupController@delete');
    $app->Get('/detail', 'TaskGroupController@detail');
});

//Work Center
$app->group(['prefix' => 'workCenter', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'WorkCenterController@save');
    $app->put('update/{id}', 'WorkCenterController@update');
    $app->Get('/listAll', 'WorkCenterController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'WorkCenterController@delete');
    $app->Get('/detail/{id:[\d]+}', 'WorkCenterController@detail');
});

// Department
$app->group(['prefix' => 'department', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'DepartmentController@save');
    $app->put('update/{id}', 'DepartmentController@update');
    $app->Get('/listAll', 'DepartmentController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'DepartmentController@delete');
    
});

// ProductType
$app->group(['prefix' => 'productType', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'ProductTypeController@save');
    $app->put('update/{id}', 'ProductTypeController@update');
    $app->Get('/listAll', 'ProductTypeController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'ProductTypeController@delete');
    
});

// ConversionJournal
$app->group(['prefix' => 'conversionJournal', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'ConversionJournalController@save');
    $app->put('update/{id}', 'ConversionJournalController@update');
    $app->Get('/listAll', 'ConversionJournalController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'ConversionJournalController@delete');
    
});

// Qcp
$app->group(['prefix' => 'qcp', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'QcpController@save');
    $app->put('update/{id}', 'QcpController@update');
    $app->Get('/listAll', 'QcpController@listAll');
    $app->DELETE('/delete/{id:[\d]+}', 'QcpController@delete');
     $app->post('/qualityCheck', 'QcpController@qualityCheck');
    
});
//Grn Reports

$app->group(['prefix' => 'grnReports', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Get('/externalType', 'GrnReportsController@externalWiseProduct');
    $app->Get('/expectedQtyReport', 'GrnReportsController@expectedQtyReport');
    $app->Get('/receivedQtyReport', 'GrnReportsController@receivedQtyReport');
    $app->Get('/expectedReceivedDiffQtyReport', 'GrnReportsController@expectedReceivedDiffQtyReport');
    $app->Get('/internalMoReport', 'GrnReportsController@internalMoReport');
    $app->Get('/InternalReceivedBasedMoReport', 'GrnReportsController@InternalReceivedBasedMoReport');
    $app->Get('/MoItemWiseIssueReport', 'GrnReportsController@MoItemWiseIssueReport');
    
});

$app->group(['prefix' => 'issue', 'middleWare' => 'auth'], function (Laravel\Lumen\Application $app) {

    $app->Post('/save', 'IssueController@save');
    $app->PUT('/update/{id:[\d]+}', 'IssueController@update');
    $app->DELETE('/delete/{id:[\d]+}', 'IssueController@delete');
    $app->Get('/listAll', 'IssueController@listAll');
    $app->Get('/detail','IssueController@detail');
});