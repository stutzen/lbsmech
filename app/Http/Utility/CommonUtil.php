<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommonUtil
 *
 * @author LENOVO
 */
namespace App\Http\Utility;
class CommonUtil {
    
    public static function urlSignIn($verb,$contentType,$objName){
        $ttl = time() + 3600;
        $config = config('app');
        $location_to_key_file = $config['SERVICE_ACCOUNT_PKCS12_FILE_PATH'];
        $bucketName = $config['BUCKET_NAME'];
        $stringToSign = $verb . "\n\n" . $contentType . "\n" . $ttl . "\n" . '/' . $bucketName . '/' . $objName;
        
        $privateKey = '';
        if (file_exists($location_to_key_file)) {
            $fh = fopen($location_to_key_file, "r");
            while (!feof($fh)) {
                $privateKey .= fgets($fh);
            }
            fclose($fh);
        }
        $signer = new Google_P12Signer($privateKey, "notasecret");
        $signature = $signer->sign($stringToSign);
        $finalSignature = base64_encode($signature);
        $host = "http://storage.googleapis.com/" . $bucketName;
       
        $serviceAccountName = $config['SERVICE_ACCOUNT_EMAIL'];
         $url = $host . "/" . $objName . "?Expires=" . $ttl . "&GoogleAccessId=" .
                $serviceAccountName . "&Signature=" . urlencode($finalSignature);
        return $url;
    }
}
    
    
    class Google_P12Signer extends Google_Signer {

    // OpenSSL private key resource
    private $privateKey;

    // Creates a new signer from a .p12 file.
    function __construct($p12, $password) {
        if (!function_exists('openssl_x509_read')) {
            throw new Exception(
            'The Google PHP API library needs the openssl PHP extension');
        }

        // This throws on error
        $certs = array();
        if (!openssl_pkcs12_read($p12, $certs, $password)) {
            throw new Google_AuthException("Unable to parse the p12 file.  " .
            "Is this a .p12 file?  Is the password correct?  OpenSSL error: " .
            openssl_error_string());
        }
        // TODO(beaton): is this part of the contract for the openssl_pkcs12_read
        // method?  What happens if there are multiple private keys?  Do we care?
        if (!array_key_exists("pkey", $certs) || !$certs["pkey"]) {
            throw new Google_AuthException("No private key found in p12 file.");
        }
        $this->privateKey = openssl_pkey_get_private($certs["pkey"]);
        if (!$this->privateKey) {
            throw new Google_AuthException("Unable to load private key in ");
        }
    }

    function __destruct() {
        if ($this->privateKey) {
            openssl_pkey_free($this->privateKey);
        }
    }

    function sign($data) {
        if (!openssl_sign($data, $signature, $this->privateKey, "sha256")) {
            throw new Google_AuthException("Unable to sign data");
        }

        return $signature;
    }

}

abstract class Google_Signer {

    /**
     * Signs data, returns the signature as binary data.
     */
    abstract public function sign($data);
}
