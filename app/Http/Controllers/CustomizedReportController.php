<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\MailHelper;
use App\Helper\ReportHelper;
use App\Helper\ActivityLogHelper;

class CustomizedReportController extends Controller {

    public function customReport(Request $request) {
        $report_type = $request->input('report_type');
        $templateConfig = DB::table('tbl_email_templates')
                ->select('*')
                ->where('tplname', '=', 'custom_report_template')
                ->first();
        
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        
        $tpl = $templateConfig->message;
        $re = new ReportHelper;
        if (strcasecmp($report_type, 'empSalary') == 0) {
            $tpl = str_replace('{{report_title}}', 'Employee Salary Report', $tpl);
            $tpl = str_replace('{{report_name}}', 'Employee Salary Report', $tpl);
            $tpl = str_replace('{{from_date}}', $from_date, $tpl);
            $tpl = str_replace('{{to_date}}', $to_date, $tpl);

            $resVal = $re->employeeSalary($request);
        }
        $header = '';
        $items = '';
        if ($resVal != null && count($resVal['list']) > 0) {
            $header = $header . '<th colspan="1" class="paddint-cell">Sno</th>';
            foreach ($resVal['list'][0] as $key => $value) {
                $header = $header . '<th colspan="1" class="paddint-cell">' . $key . '</th>';
            }
            $header = $header . '<th colspan="1" class="paddint-cell">Signature</th>';
            $count = 1;
            foreach ($resVal['list'] as $t) {
                $items = $items . '<tr class="bg-green">';
                $items = $items . '<td>' . $count . '</td>';
                foreach ($t as $key => $value) {
                    if (strcasecmp(gettype($value), 'string') == 0) {
                        $items = $items . '<td class="paddint-cell bg-green">' . $value . '</td>';
                    } else {
                        $items = $items . '<td style="text-align:right;">' . $value . '</td>';
                    }
                }
                $items = $items . '<td> <input class="center inputs input2 in" readonly="readonly" style="width:100px" value="" type="textbox"/><br>
</td>';
                $items = $items . '</tr>';
                $count++;
            }
        }
        $tpl = str_replace('{{column_headers}}', $header, $tpl);
        $tpl = str_replace('{{column_items}}', $items, $tpl);


        echo $tpl; 
        //$handle = printer_open(DEFAULT_PRINTER);
    }

}

?>
 