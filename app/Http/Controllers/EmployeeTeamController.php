<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\EmployeeTeam;
use App\DealStage;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class EmployeeTeamController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Employee Team Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $emp_team = new EmployeeTeam;

        $emp_team->created_by = $currentuser->id;
        $emp_team->updated_by = $currentuser->id;
        $emp_team->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $emp_team->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $emp_team->fill($request->all());
        $emp_team->save();

        $resVal['id'] = $emp_team->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active = $request->input('is_active', '');
        $emp_id = $request->input('emp_id');
        $emp_name = $request->input('emp_name', '');
        $type=$request->input('type');

      

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_emp_team')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($emp_id)) {
            $builder->where('emp_id', '=', $emp_id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

 
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }

        if (!empty($emp_name)) {
            $builder->where('emp_name', 'like', '%' . $emp_name . '%');
        }
         if ($type == 1) {
            $builder->whereColumn('current_count', '<>', 'count');
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Employee Team Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_team = EmployeeTeam::findOrFail($id);
            if ($emp_team->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Team   Not found';
            return $resVal;
        }
        $emp_team->updated_by = $currentuser->id;
        $emp_team->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $emp_team->fill($request->all());
        $emp_team->save();
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Team  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');

        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_team = EmployeeTeam::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Team    Not found';
            return $resVal;
        }

        $emp_team->updated_by = $currentuser->id;
        $emp_team->is_active = 0;

        $emp_team->update();
        $resVal['id'] = $emp_team->id;
        return $resVal;
    }

}

?>
