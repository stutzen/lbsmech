<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Deal;
use App\Attachment;
use App\DealActivity;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class DealActivityController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Deal Activity Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
//        $time = $request->input('time');
//        $timestamp = strtotime($time);

        $deal = new DealActivity;

        $deal->created_by = $currentuser->id;
        $deal->updated_by = $currentuser->id;
        $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        //      $deal->time = $timestamp;
        //  print_r($request->input('type'));
//        $deal->type = $request->input('type');
//        $deal->description = $request->input('description');
//        $deal->log_activity_type = $request->input('log_activity_type');
//        $deal->log_activity_sub_type = $request->input('log_activity_sub_type');
//        $deal->contact_id = $request->input('contact_id');
//        $deal->date = $request->input('date');
//        $deal->user_id = $request->input('user_id');
//        $deal->user_name = $request->input('user_name');
//        $deal->is_active = $request->input('is_active');
//        $deal->deal_id = $request->input('deal_id');
//        if(!empty($request->input('latitude')))
//        $deal->latitude = $request->input('latitude');
//        if(!empty($request->input('longitude')))
//        $deal->longitude = $request->input('longitude');
//        if(!empty($request->input('geo_point')))
//        $deal->geo_point = $request->input('geo_point');
//        if(!empty($request->input('ip_address')))
//        $deal->ip_address = $request->input('ip_address');
        $deal->fill($request->all());
        //  $deal->fill($request->all());

        $deal->save();
        $resVal['id'] = $deal->id;
        if ($request->input('type') == 'note' || $request->input('type') == 'logActivity')
            ActivityLogHelper::dealActivitySave($deal);

        if ($request->input('type') == 'note') {
            $attachmentcol = $request->input('attachment');
            foreach ($attachmentcol as $attachment) {
                $attach = new Attachment;
                $attach->type = "deal_note";
                $attach->ref_id = $deal->id;
                $attach->url = $attachment['file_path'];
                $attach->created_by = $currentuser->id;
                $attach->updated_by = $currentuser->id;
                $attach->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $attach->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $attach->is_active = "1";
                $attach->comments = "deal note";
                $attach->save();
            }
        }

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $type = $request->input('type', '');
        $deal_id = $request->input('deal_id', '');
        $contact_id = $request->input('contact_id');
        $is_active = $request->input('is_active');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_deal_activity')
                ->select('*', DB::raw('CONCAT("No Attachment") AS attachment'));
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }
        if ($deal_id != '') {
            $builder->where('deal_id', '=', $deal_id);
        }
        if ($contact_id != '') {
            $builder->where('contact_id', '=', $contact_id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        $dealcollection = $builder->get();
//var_dump($builder);
        foreach ($dealcollection as $deal) {
            $type = $deal->type;
            if ($type == 'note') {
                $deal_id = $deal->id;
                $attachment = DB::table('tbl_attachment')
                        ->where('is_active', '=', '1')
                        ->where('ref_id', '=', $deal_id)
                        ->get();
                $deal->attachment = $attachment;
            }
        }
        foreach ($dealcollection as $deal) {
            $time = $deal->time;
            $time = date('h:i A', strtotime($time));

            $deal->time = $time;
        }
        $resVal['list'] = $dealcollection;

        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Deal Activity Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $deal = DealActivity::findOrFail($id);
            if ($deal->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Deal Activity  Not found';
            return $resVal;
        }
//        $time = $request->time;
//
//        $timestamp = strtotime($time);
//        $deal->updated_by = $currentuser->id;
//        $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
//        $deal->time = $timestamp;
//        $deal->type = $request->type;
//        $deal->description = $request->description;
//        $deal->log_activity_type = $request->log_activity_type;
//        $deal->log_activity_sub_type = $request->log_activity_sub_type;
//        $deal->contact_id = $request->contact_id;
//        $deal->date = $request->date;
//        $deal->user_id = $request->user_id;
//        $deal->user_name = $request->user_name;
//        $deal->is_active = $request->is_active;
//        $deal->deal_id = $request->deal_id;
//        $deal->comments = $request->comments;

        $deal->fill($request->all());


        $deal->save();

        if ($request->input('type') == 'note' || $request->input('type') == 'logActivity') {
            ActivityLogHelper::dealActivityUpdate($deal);
        }
        if ($request->input('type') == 'note') {
            $builder = DB::table('tbl_attachment')->where('ref_id', $id)
                    ->where('type', '=', 'deal_note')
                    ->delete();

            $attachmentcol = $request->attachment;
            foreach ($attachmentcol as $attachment) {
                $attach = new Attachment;
                $attach->type = "deal_note";
                $attach->ref_id = $deal->id;
                $attach->url = $attachment['file_path'];
                $attach->created_by = $currentuser->id;
                $attach->updated_by = $currentuser->id;
                $attach->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $attach->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $attach->is_active = "1";
                $attach->comments = "deal note";
                $attach->save();
            }
        }

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Deal Activity  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $deal = DealActivity::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Deal  Not found';
            return $resVal;
        }

        $deal->updated_by = $currentuser->id;
        $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $deal->is_active = 0;
        $deal->update();
        $resVal['id'] = $deal->id;
        if ($request->input('type') == 'note' || $request->input('type') == 'logActivity')
            ActivityLogHelper::dealActivityDelete($deal);

        if ($request->input('type') == 'note') {
            DB::table('tbl_attachment')->where('ref_id', $id)
                    ->where('type', '=', 'deal_note')
                    ->update(['is_active' => 0]);
        }

        return $resVal;
    }

}

?>
