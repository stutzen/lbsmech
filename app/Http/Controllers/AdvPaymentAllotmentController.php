<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdvPaymentAllotment
 *
 * @author Stutzen Admin
 */
namespace App\Http\Controllers;
use DB;
use Validator;
use App\AdvPaymentAllotment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
class AdvPaymentAllotmentController extends Controller{
    //put your code here
    
     public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Advance Payment Allotment  saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $apa = new AdvPaymentAllotment;
        $apa->date=$request->date;
        $apa->created_by = $currentuser->id;
        $apa->updated_by = $currentuser->id;
        $apa->is_active = $request->input('is_active', 1);
        $apa->fill($request->all());
        $apa->save();
        $resVal['id'] = $apa->id;
        return $resVal;
    }
    
    
    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Advance Payment Allotment Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $apa = AdvPaymentAllotment::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Advance Payment Allotment  Not found';
            return $resVal;
        }
        $apa->updated_by = $currentuser->id;
        $apa->fill($request->all());
        $apa->save();
        $resVal['id'] = $apa->id;
        return $resVal;
    }
    
    
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');
        $advPaymentId = $request->input('adv_payment_id');
        $paymentId = $request->input('payment_id');
        $invoiceId = $request->input('invoice_id');
        $salesOrderId = $request->input('sales_order_id');
        $customerId = $request->input('customer_id');
        $date = $request->input('date','');
        $is_active = $request->input('is_active', '');


        $builder = DB::table('tbl_adv_payment_allotment')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        
        if (!empty($advPaymentId)) {
            $builder->where('adv_payment_id', '=', $advPaymentId);
        }
        
        if (!empty($paymentId)) {   
            $builder->where('payment_id', '=', $paymentId);
        }
        
        if (!empty($invoiceId)) {
            $builder->where('invoice_id', '=', $invoiceId);
        }
        
        if (!empty($salesOrderId)) {
                $builder->where('sales_order_id', '=', $salesOrderId);
        }
        
        if (!empty($customerId)) {
            $builder->where('customer_id', '=', $customerId);
        }
        
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($date)) {
            $builder->where('date', '=', $date);
        }

        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $builder = $builder->get();
        } else {

            $builder = $builder->skip($start)->take($limit)->get();
        }
        $resVal['list'] = $builder;
        return ($resVal);
    }
    
    public function delete(Request $request, $id) {
        $currentuser = Auth::user();
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Advance Payment Allotment  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $apa = AdvPaymentAllotment::findOrFail($id);
            if ($apa->is_active == 0) {
                return $resVal;
            }

           $apa->is_active = 0;
        $apa->updated_by = $currentuser->id;
//$invoice->fill($request->all());
        $apa->update();

        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Advance Payment Allotment  Not found';
            return $resVal;
        }
        
        $resVal['id'] = $apa->id;

        return $resVal;
    }
    
}
