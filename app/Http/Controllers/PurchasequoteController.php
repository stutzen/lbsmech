<?php

namespace App\Http\Controllers;

use DB;
use App\Purchasequote;
use App\Purchasequoteitem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\AccountHelper;
use App\PurchasePayment;
use App\Transaction;
use App\Helper\GeneralHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuotepurchaseorderController
 *
 * @author Deepa
 */
class PurchasequoteController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = ' Purchase Quote Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
                 
        $quote_no = $request->input('quote_no', '');
        $prefix = $request->input('prefix','');
        if (!empty($quote_no)) {
             $get_inv_no = DB::table('tbl_purchase_quote')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('quote_no', '=', $quote_no)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Purchase Order no already exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $quote_no = $quote_no;
                $quote_code = $prefix . $quote_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_purchase_quote')
                    ->select('quote_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('quote_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $sales_no = $get_inv_no->quote_no;
                $sales_no = $sales_no + 1;
            } else {
                $sales_no = 1;
            }
            $quote_no = $sales_no;
            $quote_code = $prefix . $sales_no;
        }
        
        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax(0, $taxId, $taxable_amount, 'sales');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff;
        $total = floatval(number_format($total, 2, '.', ''));
        
        $currentuser = Auth::user();


        $purchaseorder = new Purchasequote;
        $purchaseorder->subtotal = $subtotal;
        $purchaseorder->tax_amount = $tax_amount;
        $purchaseorder->discount_amount = $discount_amount;
        $purchaseorder->round_off = $roundoff;
        $purchaseorder->total_amount = $total;
        $purchaseorder->created_by = $currentuser->id;
        $purchaseorder->updated_by = $currentuser->id;
        $purchaseorder->quote_no = $quote_no;
        $purchaseorder->quote_code = $quote_code;
        $purchaseorder->is_active = $request->input('is_active', 1);

        $purchaseorder->fill($request->all());
        $purchaseorder->save();

        $quoteItemCol = ($request->input('item'));

        foreach ($quoteItemCol as $item) {

            $purchasequoteitems = new Purchasequoteitem;
            $purchasequoteitems->fill($item);
            $purchasequoteitems->purchase_quote_id = $purchaseorder->id;
            $purchasequoteitems->is_active = $request->input('is_active', 1);
            $purchasequoteitems->duedate = $purchaseorder->duedate;
            $purchasequoteitems->paymentmethod = $purchaseorder->paymentmethod;
            $purchasequoteitems->tax_amount = $purchaseorder->tax_amount;

            $purchasequoteitems->save();
        }

        $resVal['id'] = $purchaseorder->id;

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Quote Item Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $purchasequoteitem = Purchasequote::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Quote item Order Not found';
            return $resVal;
        }
          
        $quote_no = $request->input('quote_no', '');
        $prefix = $request->input('prefix','');
        if (!empty($quote_no)) {
             $get_inv_no = DB::table('tbl_purchase_quote')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('quote_no', '=', $quote_no)
                     ->where('id', '!=', $id)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Purchase Order no already exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $quote_no = $quote_no;
                $quote_code = $prefix . $quote_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_purchase_quote')
                    ->select('quote_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->where('id', '!=', $id)
                    ->orderBy('quote_no', 'desc')                    
                    ->first();
            if (isset($get_inv_no)) {
                $sales_no = $get_inv_no->quote_no;
                $sales_no = $sales_no + 1;
            } else {
                $sales_no = 1;
            }
            $quote_no = $sales_no;
            $quote_code = $prefix . $sales_no;
        }
        
        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax($id, $taxId, $taxable_amount, 'sales');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff;
        $total = floatval(number_format($total, 2, '.', ''));
        
        $builder = DB::table('tbl_purchase_quote_item')->where('purchase_quote_id', $id)->delete();
        $quoteItemCol = ($request->input('item'));
        foreach ($quoteItemCol as $item) {

            $purchasquoteitems = new Purchasequoteitem;
            $purchasquoteitems->fill($item);
            $purchasquoteitems->purchase_quote_id = $purchasequoteitem->id;
            $purchasquoteitems->is_active = $request->input('is_active', 1);
            $purchasquoteitems->duedate = $purchasequoteitem->duedate;
            $purchasquoteitems->paymentmethod = $purchasequoteitem->paymentmethod;
            $purchasquoteitems->tax_amount = $purchasequoteitem->tax_amount;

            $purchasquoteitems->save();
        }
        $purchasequoteitem->created_by = $currentuser->id;        
        $purchasequoteitem->fill($request->all());
        $purchasequoteitem->subtotal = $subtotal;
        $purchasequoteitem->tax_amount = $tax_amount;
        $purchasequoteitem->discount_amount = $discount_amount;
        $purchasequoteitem->round_off = $roundoff;
        $purchasequoteitem->quote_no = $quote_no;
        $purchasequoteitem->quote_code = $quote_code;
        $purchasequoteitem->total_amount = $total;
        $purchasequoteitem->save();

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $customerid = $request->input('customer_id', '');
        $mobile = $request->input('mobile','');
        $fromValidDate = $request->input('from_validdate', '');
        $toValidDate = $request->input('to_validdate', '');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $quoteDate=$request->input('quote_date','');
        $validDate=$request->input('valid_date','');
        $status = $request->input('status');
        $is_active = $request->input('is_active', '');

        //$status = $request->input('status', '');
//        $builder = DB::table('tbl_purchase_quote')
//                ->select('*');

        $builder = DB::table('tbl_purchase_quote as q')
                ->leftJoin('tbl_customer as c', 'q.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
                ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }
        if (!empty($customerid)) {
            $builder->where('q.customer_id', '=', $customerid);
        }
        if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }
        if (!empty($fromValidDate)) {

            $builder->whereDate('q.validuntil', '>=', $fromValidDate);
        }
        if (!empty($toValidDate)) {

            $builder->whereDate('q.validuntil', '<=', $toValidDate);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('q.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('q.date', '<=', $toDate);
        }
		if (!empty($quoteDate)) {

            $builder->whereDate('q.date', '=', $quoteDate);
        }
        if (!empty($validDate)) {

            $builder->whereDate('q.validuntil', '=', $validDate);
        }
        if (!empty($status)) {
            $builder->where('q.status', '=', $status);
        }
        if ($is_active != '') {
            $builder->where('q.is_active', '=', $is_active);
        }


        $builder->orderBy('q.id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Quote Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $purchaseorder = Purchasequote::findOrFail($id);
            if ($purchaseorder->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Quote Order Not found';
            return $resVal;
        }

        $purchaseorder->is_active = 0;


        //$quotes->fill($request->all());
        $purchaseorder->update();

        /*
         * Code to adjust bank money and transaction table entry 
         */
        $paymentCollection = PurchasePayment::where('purchase_invoice_id', '=', 0)
                        ->where('purchase_quote_id', '=', $id)
                        ->where('is_active', '=', 1)->get();
        foreach ($paymentCollection as $payment) {

            AccountHelper::deposit($payment->account_id, $payment->amount);

            //deavtivate the transaction
            DB::table('tbl_transaction')
                    ->where('voucher_type', '=', Transaction::$PURCHASE_PAYMENT)
                    ->where('voucher_number', '=', $payment->id)->update(['is_active' => 0]);
        }


        $resVal['id'] = $purchaseorder->id;
        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');

        $is_active = $request->input('is_active', '');
        //$builder = DB::table('tbl_quotes')->select('*');
        $builder = DB::table('tbl_purchase_quote as q')
                ->leftJoin('tbl_customer as c', 'q.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
                ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email')
                ->where('q.id', '=', $id);

        $purchasequotestitems = DB::table('tbl_purchase_quote_item')->where('purchase_quote_id', $id);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }

        if ($is_active != '') {
            $builder->where('q.is_active', '=', $is_active);
        }
        $builder->orderBy('q.id', 'desc');

        $resVal['total'] = $builder->count();
        $quotestCollection = $builder->skip($start)->take($limit)->get();
        if (count($quotestCollection) == 1) {

            $quotest = $quotestCollection->first();

            $quotest->item = $purchasequotestitems->get();
            $resVal['data'] = $quotest;
        }
        // $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

}

?>
