<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\ConversionJournal;
use App\ConversionJournalDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;

class ConversionJournalController extends Controller {

    public function save(Request $request) {
        $resVal['message'] = 'Conversion Journal Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentUser = Auth::user();

        $ConJourn = new ConversionJournal;
        $ConJourn->fill($request->all());
        $ConJourn->is_active = 1;
        $ConJourn->created_by = $currentUser->id;
        $ConJourn->updated_by = $currentUser->id;
        $ConJourn->save();

        $detail = $request->input('detail');

        foreach ($detail as $det) {
            $JournDet = New ConversionJournalDetail;
            $JournDet->fill($det);
            $JournDet->is_active = 1;
            $JournDet->conversion_journal_id = $ConJourn->id;
            $JournDet->created_by = $currentUser->id;
            $JournDet->updated_by = $currentUser->id;
            
            $fromUom=DB::table('tbl_product')->where('id','=',$JournDet->from_pdt_id)->first();
            $toUom=DB::table('tbl_product')->where('id','=',$JournDet->to_pdt_id)->first();
            if($fromUom != ''){
                $JournDet->from_uom_id=$fromUom->uom_id;
            }
            if($toUom != ''){
                $JournDet->to_uom_id=$toUom->uom_id;
            }
            $JournDet->save();

            if (!empty($JournDet->from_qty)) {
                GeneralHelper::InventoryUpdate($JournDet->from_pdt_id, $JournDet->from_qty, 'journal_from');
            } else if (!empty($JournDet->to_qty)) {
                GeneralHelper::InventoryUpdate($JournDet->to_pdt_id, $JournDet->to_qty, 'journal_to');
            }
        }

        $resVal['id'] = $ConJourn->id;

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Conversion Journal Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentUser = Auth::user();
        try {
            $ConJourn = ConversionJournal::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Conversion Journal Not found';
            return $resVal;
        }
        $ConJourn->fill($request->all());
        $ConJourn->is_active = 1;
        $ConJourn->updated_by = $currentUser->id;
        $ConJourn->save();

        $journDetail = DB::table('tbl_conversion_journal_detail')->where('conversion_journal_id', '=', $id)->get();

        foreach ($journDetail as $jouDet) {
            
            if (($jouDet->from_qty)>0) { 
                GeneralHelper::InventoryUpdate($jouDet->from_pdt_id, $jouDet->from_qty, 'journal_to');
            } else if (($jouDet->to_qty) > 0) {  
                GeneralHelper::InventoryUpdate($jouDet->to_pdt_id, $jouDet->to_qty, 'journal_from');
            }
        }

        DB::table('tbl_conversion_journal_detail')->where('conversion_journal_id', '=', $id)->delete();

        $detail = $request->input('detail');

        foreach ($detail as $det) {
            $JournDet = New ConversionJournalDetail;
            $JournDet->fill($det);
            $JournDet->is_active = 1;
            $JournDet->conversion_journal_id = $ConJourn->id;
            $JournDet->created_by = $currentUser->id;
            $JournDet->updated_by = $currentUser->id;
                       
            $fromUom=DB::table('tbl_product')->where('id','=',$JournDet->from_pdt_id)->first();
            $toUom=DB::table('tbl_product')->where('id','=',$JournDet->to_pdt_id)->first();
            if($fromUom != ''){
                $JournDet->from_uom_id=$fromUom->uom_id;
            }
            if($toUom != ''){
                $JournDet->to_uom_id=$toUom->uom_id;
            }
            $JournDet->save();

            if (!empty($JournDet->from_qty)) {
                GeneralHelper::InventoryUpdate($JournDet->from_pdt_id, $JournDet->from_qty, 'journal_from');
            } else if (!empty($JournDet->to_qty)) {
                GeneralHelper::InventoryUpdate($JournDet->to_pdt_id, $JournDet->to_qty, 'journal_to');
            }
        }

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $date = $request->input('date');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $is_active = $request->input('is_active', '');

        $builder = DB::table('tbl_conversion_journal')
                ->select('*');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }

        if (!empty($date)) {
            $builder->whereDate('date', '=', $date);
        }
        if (!empty($from_date)) {
            $builder->whereDate('date', '>=', $from_date);
        }

        if (!empty($to_date)) {
            $builder->whereDate('date', '<=', $to_date);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $Collection = $builder->get();
        } else {

            $Collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($Collection as $collect) {
            $detail = DB::table('tbl_conversion_journal_detail as cj')
                    ->select('cj.*','p.name as from_pdt_name','pr.name as to_pdt_name')
                    ->leftjoin('tbl_product as p','p.id','=','cj.from_pdt_id')
                    ->leftjoin('tbl_product as pr','pr.id','=','cj.to_pdt_id')
                    ->where('cj.conversion_journal_id', '=', $collect->id)
                    ->where('cj.is_active', '=', 1)
                    ->get();

            $collect->detail = $detail;
        }
        $resVal['list'] = $Collection;

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Conversion Journal Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentUser = Auth::user();
        try {
            $ConJourn = ConversionJournal::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Conversion Journal Not found';
            return $resVal;
        }

        $ConJourn->is_active = 0;
        $ConJourn->updated_by = $currentUser->id;
        $ConJourn->save();
        
        $journDetail = DB::table('tbl_conversion_journal_detail')->where('conversion_journal_id', '=', $id)->get();

        foreach ($journDetail as $jouDet) {
            if (($jouDet->from_qty)>0) {
                GeneralHelper::InventoryUpdate($jouDet->from_pdt_id, $jouDet->from_qty, 'journal_to');
            } else if (($jouDet->to_qty)>0) {
                GeneralHelper::InventoryUpdate($jouDet->to_pdt_id, $jouDet->to_qty, 'journal_from');
            }
        }

        DB::table('tbl_conversion_journal_detail')->where('conversion_journal_id', '=', $id)->update(['is_active' => 0]);

        return $resVal;
    }

}
