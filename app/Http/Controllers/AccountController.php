<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Account;
use App\Acode;
use App\Transaction;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountsController
 *
 * @author Deepa
 */
class AccountController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Account Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $validator = Validator::make($request->all(), [
                    'account' => 'required|unique:tbl_accounts,account'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            if (array_key_exists('account', $validator->failed())) {
                $resVal['message'] = 'Account name is already exist';
            }

            return $resVal;
        }

        $currentuser = Auth::user();

        $item = new Account;

        $item->created_by = $currentuser->id;
        $item->updated_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();
        if (empty($item->acode)) {
            
            $acode = new Acode;
            $acode->created_by = $currentuser->id;
            $acode->updated_by = $currentuser->id;
            $acode->ref_type = Acode::$ACCOUNT_REF;
            $acode->ref_id = $item->id;
            $acode->name  = $item->account;
            $acode->save();
            
            $acode->code = $acode->id;
            $acode->save();
            
            $item->acode = $acode->code;
            $item->save();
        }

//         

        $transaction = new Transaction;

        // $mydate = $request->updated_at;

        $mydate = date("Y/m/d");
        $month = date("m", strtotime($mydate));
        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();

        $transaction->fiscal_month_code = $month;
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }
        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

        $transaction->name = $currentuser->f_name;
        $transaction->transaction_date = $mydate;
        
        $transaction->credit = $request->input('balance');
        $transaction->acode = NULL;
        $transaction->acoderef = NULL;
        $transaction->is_cash_flow = 1;
        $transaction->pmtcode = $item->acode;
        $transaction->pmtcoderef = $item->id;
        $transaction->prefix = $item->prefix;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $id = $request->input('account_id', '');


        $transaction->voucher_type = "inital_balance";
        $transaction->particulars = "Initial Balance";
        $transaction->voucher_number = 0;
        $transaction->is_active = 1;
        $transaction->fill($request->all());
        $transaction->save();

        $resVal['id'] = $item->id;

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Accounts Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $item = Account::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Accounts Not found';
            return $resVal;
        }

        $item->delete();

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $account = $request->input('account');
        $bank_name = $request->input('bank_name', '');
        $account_number = $request->input('account_number');
        $acode = $request->input('acode');

        $builder = DB::table('tbl_accounts')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($account)) {
            $builder->where('account', 'like', '%' . $account . '%');
        }
        if (!empty($bank_name)) {
            $builder->where('bank_name', 'like', '%' . $bank_name . '%');
        }
        if (!empty($account_number)) {
            $builder->where('account_number', '=', $account_number);
        }
        if (!empty($acode)) {
            $builder->where('acode', '=', $acode);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Accounts Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $item = Account::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Account Not found';
            return $resVal;
        }
        $item->created_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();

        return $resVal;
    }

}

?>
