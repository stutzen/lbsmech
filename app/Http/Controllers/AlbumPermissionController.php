<?php

namespace App\Http\Controllers;

use DB;
Use App\Customer;
use App\User;
use App\Album;
use App\AlbumPermission;
Use Illuminate\Http\Request;
use App\Transformer\CustomerTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\MailHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomerController
 *
 * @author Deepa
 */
class AlbumPermissionController extends Controller {
    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Album Permission Saved Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $permissionlist = $request->all();

        foreach ($permissionlist as $std) {
            $AlbumapermissiomCollection = AlbumPermission::where('album_id', "=", $std['album_id'])
                    ->where('customer_id', "=", $std['customer_id'])
                    ->get();
            if (count($AlbumapermissiomCollection) > 0) {
                $albumpermission = $AlbumapermissiomCollection->first();
            } else {
                $albumpermission = new AlbumPermission;
            }
            $albumpermission->fill($std);
            $albumpermission->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $albumpermission->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $albumpermission->created_by = $currentuser->id;
            $albumpermission->is_active = 1;
            $albumpermission->updated_by = $currentuser->id;
            $albumpermission->save();
            $mail_obj = new MailHelper;
            $customer_id = $std['customer_id'];
            $album_code = $std['album_code'];
            $mail_obj->AlbumPermissionCreated($album_code, $customer_id);
        }

        return $resVal;
    }
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $code = $request->input('code');
        $album_id = $request->input('album_id');
        $cutomer_id = $request->input('cutomer_id');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_album_permission as a')
                ->leftJoin('tbl_customer as c', 'a.customer_id', '=', 'c.id')
                ->select('a.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 'c.email', 'c.phone')
                ->where('a.is_active','=',1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($album_id)) {
            $builder->where('a.album_id', '=', $album_id);
        }
        if (!empty($code)) {
            $builder->where('a.album_code', '=', $code);
        }
        if (!empty($cutomer_id)) {
            $builder->where('c.cutomer_id', '=', $cutomer_id);
        }

        $builder->orderBy('a.id', 'asc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $albumCollection = $builder->get();
        } else {

            $albumCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $albumCollection;
        //$resVal['list'] = $builder->paginate(10);
        return ($resVal);
    }
    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Album Permission Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $album = AlbumPermission::findOrFail($id);
            if ($album->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Receipt Not found';
            return $resVal;
        }
        $album->is_active = 0;
        $album->updated_by = $currentuser->id;
        //$invoice->fill($request->all());
        $album->update();
        return $resVal;
    }     
}

?>
