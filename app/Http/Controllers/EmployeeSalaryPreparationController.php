<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use DB;
use App\EmployeeSalaryPreparation;
use Carbon\Carbon;
use App\EmployeeProductionInput;
use App\EmployeeProductionAttendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;

class EmployeeSalaryPreparationController extends Controller {
    
      public function listAll(Request $request){
          $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $from_date=$request->input('from_date');
        $to_date=$request->input('to_date');
        $id=$request->input('id');
        $team_id=$request->input('team_id');
        $salary_type=$request->input('salary_type');
        $payroll_type=$request->input('payroll_type');
        
        $builder=DB::table('tbl_emp_salary_preparation as esp')
                ->leftjoin('tbl_emp_team as et','et.id','=','esp.team_id')
                 ->leftjoin('tbl_accounts as a', 'a.id', '=', 'esp.account_id')
                ->select('esp.*','et.name as team_name','a.account as accountName');
        
        if(!empty($id)){
            $builder->where('esp.id','=',$id);
        }
        if(!empty($team_id)){
            $builder->where('esp.team_id','=',$team_id);
        }
        if(!empty($salary_type)){
            $builder->where('esp.salary_type','=',$salary_type);
        }
          if(!empty($payroll_type)){
            $builder->where('esp.payroll_type','=',$payroll_type);
        }
        if ((!empty($from_date)) && (!empty($to_date))) {
           $builder->whereRaw(DB::raw("DATE_FORMAT((esp.created_at),'%Y-%m-%d') >= DATE_FORMAT('$from_date','%Y-%m-%d') AND DATE_FORMAT((esp.created_at),'%Y-%m-%d') <= DATE_FORMAT('$to_date','%Y-%m-%d')"));
       }
       $builder->orderBy('esp.id', 'desc');
       
       $start = $request->input('start', 0);
       $limit = $request->input('limit', 100);
        
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $collection;
        return ($resVal);
        
      }

}