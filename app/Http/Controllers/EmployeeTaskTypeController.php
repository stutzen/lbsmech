<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\EmployeeTaskType;
use App\EmployeeTaskActivities;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;

class EmployeeTaskTypeController extends Controller {
    
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Employee Task Type Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
         $currentuser = Auth::user();
           
        $employee=new EmployeeTaskType;
        $employee->fill($request->all());
        $employee->created_by=$currentuser->id;
        $employee->updated_by=$currentuser->id;
        $employee->is_active=$request->input('is_active',1);
        
        $employee->save();
        $resVal['id']=$employee->id;
       return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $is_active=$request->input('is_active');
       
        $builder = DB::table('tbl_emp_task_type')
                ->select('*');
        $resVal['success'] = TRUE;
       
        
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
       
       if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
       
       
        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        
        $resVal['list'] = $builder->get();
      
        return ($resVal);
    }
    
    
     public function update(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Task Type Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
            
        
        try {
            $employee = EmployeeTaskType::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Task Type Not found';
            return $resVal;
        }
        $employee->fill($request->all());
        $employee->created_by=$currentuser->id;
        $employee->updated_by=$currentuser->id;
        $employee->is_active=$request->input('is_active',1);
                    
          $employee->save();
          $type_name=$employee->name;
          $type_id=$employee->id;
        
         DB::table('tbl_emp_task_activities')->where('type_id', $type_id)->update(['type_name' => $type_name]);
         
             return $resVal;
    }
    
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Task Type Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
      
        try {
            $employee = EmployeeTaskType::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Task Type Not found';
            return $resVal;
        }
        $employee->is_active = 0;
        $employee->updated_by = $currentuser->id;
        $employee->save();
        
        return $resVal;
    }
}
