<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Attributestype;
use DB;
use Illuminate\Http\Request;
use App\Transformer\AttributesTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AttributesController
 *
 * @author Deepa
 */
class AttributeController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Attributes Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();


        if ($request->input('attribute_code') != NULL) {

            $attributeCollection = Attribute::where('attribute_code', "=", $request->input('attribute_code'))->get();

            if (count($attributeCollection) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Attribute Code is already exits';
                return $resVal;
            }
        }


        $attributes = new Attribute;


        $attributes->created_by = $currentuser->id;
        $attributes->updated_by = $currentuser->id;
        $attributes->fill($request->all());

        $attributetype = Attributestype::findOrFail($request->input('attributetype_id'));
        $attributes->attributetype_code = $attributetype->code;

        $attributes->save();

        $resVal['id'] = $attributes->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $attributecode = $request->input('attribute_code');
        $attributetypeid = $request->input('attributetype_id');
        $attributetypecode = $request->input('attributetypecode');
        $isactive = $request->input('is_active', '');
        $shortinlist = $request->input('is_show_in_list', '');
        $shortininvoice = $request->input('is_show_in_invoice', '');
        $mode = $request->input('mode', '');

        $builder = DB::table('tbl_attributes as a')
                ->leftJoin('tbl_attribute_types as at', 'a.attributetype_id', '=', 'at.id')
                ->select('a.*', 'at.code as attributetypecode','at.display_name as attribute_display_name');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('a.id', '=', $id);
        }
        if (!empty($shortinlist)) {
            $builder->where('a.is_show_in_list', '=', $shortinlist);
        }
        if (!empty($shortininvoice)) {
            $builder->where('a.is_show_in_invoice', '=', $shortininvoice);
        }
        if (!empty($attributetypeid)) {
            $builder->where('a.attributetype_id', '=', $attributetypeid);
        }
        if (!empty($attributetypecode)) {
            $builder->where('at.code', '=', $attributetypecode);
        }
        if (!empty($attributecode)) {
            $builder->where('a.attribute_code', 'like', '%' . $attributecode . '%');
        }


        if ($isactive != '') {
            $builder->where('a.is_active', '=', $isactive);
        }




        if ($mode == 1) {
            $builder->orderBy('a.sno', 'asc');
        } else {
            $builder->orderBy('a.id', 'desc');
        }

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Attributes Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
             try {
            $attributes = Attribute::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Attributes Not found';
            return $resVal;
        }

        $attributes->updated_by = $currentuser->id;

        $attributes->fill($request->all());
        $attributetype = Attributestype::findOrFail($request->input('attributetype_id'));
        $attributes->attributetype_code = $attributetype->code;

        $attributes->save();

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Attributes Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $attributes = Attribute::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Attributes Not found';
            return $resVal;
        }

        $attributes->delete();

        return $resVal;
    }

}

?>
