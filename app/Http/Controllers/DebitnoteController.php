<?php
namespace App\Http\Controllers;
use DB;
use App\Debitnote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DebitnoteController
 *
 * @author Deepa
 */
class DebitnoteController extends Controller {
    //put your code here
    public function save(Request $request){
        $resVel = array();
        $resVel ['Message'] = 'Debit Note added sucessfully';
        $resVel['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $debitnote =new Debitnote ;
        $debitnote->created_by = $currentuser->id;
        $debitnote->updated_by = $currentuser->id;
        $debitnote->fill($request->all());
        $debitnote->save();
        $resVel['id']=$debitnote->id;
        return $resVel ;
         
    }
    public function update(Request $request , $id){
         $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Debit Note Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $debitnote = Debitnote::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Debit Note Not found';
            return $resVal;
        }
        $debitnote->updated_by = $currentuser->id;
        $debitnote->fill($request->all());
        $debitnote->save();

        return $resVal;
        
    }
     public function listAll(Request $request) {
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
        
        $is_active =$request->input('is_active','');
        
        $builder = DB::table('tbl_debit_note')->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
         if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        
        $builder->orderBy('id','desc');
        
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }
    public function delete($id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Debit note Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $debit = Debitnote::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Debit note Not found';
            return $resVal;
        }

        $debit->delete();

        return $resVal;
    }
}

?>
