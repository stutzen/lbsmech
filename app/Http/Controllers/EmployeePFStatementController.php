<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Http\Controllers;

use DB;
use App\EmployeePFStatement;
use Carbon\Carbon;
use App\EmployeeProductionInput;
use App\EmployeeProductionAttendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;

class EmployeePFStatementController extends Controller {
    
   public function listAll(Request $request){
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
        $emp_id=$request->input('emp_id');
        $id=$request->input('id');
        $emp_salary_id=$request->input('emp_salary_id');
        $from_date=$request->input('from_date');
        $to_date=$request->input('to_date');
        
        $builder=DB::table('tbl_emp_salary as s')
                ->leftjoin('tbl_employee_pf_statement as pf','pf.emp_salary_id','=','s.id')                ->leftjoin('tbl_employee as e','pf.emp_id','=','e.id')
                ->select('pf.*','e.f_name','e.emp_code');
        
        if(!empty($id)){
            $builder->where('pf.id','=',$id);
        }
         if(!empty($emp_id)){
            $builder->where('pf.emp_id','=',$emp_id);
        }
         if(!empty($emp_salary_id)){
            $builder->where('pf.emp_salary_id','=',$emp_salary_id);
        }
        if ((!empty($from_date)) && (!empty($to_date))) {
           $builder->whereRaw(DB::raw("s.from_date >= '$from_date'  AND s.to_date <= '$to_date'"));
       }
        
        $builder->orderBy('pf.id', 'desc');
       
       $start = $request->input('start', 0);
       $limit = $request->input('limit', 100);
        
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $collection;
        return ($resVal);
        
    }
}
    
