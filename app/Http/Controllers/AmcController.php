<?php
namespace App\Http\Controllers;
use DB;
use Validator;
use App\Amc;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
class AmcController extends Controller
{
    public function save(Request $request) { 
        $resVal = array();
        $resVal['message'] = 'Amc Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        

        $currentuser=Auth::user();
        $amc=new Amc;      
        $amc->status='New';
        $amc->updated_by=$currentuser->id;
        $amc->created_by=$currentuser->id;
        $amc->fill($request->all());
        $amc->save();
        
        $resVal['id'] = $amc->id;

        return $resVal;
    }
      public function update(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Amc Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();

        try {
            $amc = Amc::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Amc Not found';
            return $resVal;
        }
       
        $amc->updated_by=$currentuser->id;
        $amc->fill($request->all());
        $amc->save();
        
        return $resVal;
    }
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $product_id=$request->input('product_id');
        $status=$request->input('status','');
        $customer_id=$request->input('customer_id');
        $invoice_id=$request->input('invoice_id');
        $invoice_item_id=$request->input('invoice_item_id');
        $from_date=$request->input('from_date');
        $to_date=$request->input('to_date');
        $date=$request->input('date');
        $is_active=$request->input('is_active');
        
        $builder = DB::table('tbl_amc as a')
                ->leftjoin('tbl_product as p','p.id','=','a.product_id')
                ->leftjoin('tbl_customer as c','c.id','=','a.customer_id')
                ->select('a.*','c.fname as customer_name','p.name as product_name');
        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('a.id', '=', $id);
        }
        if ($is_active != '' ) {
            $builder->where('a.is_active', '=', $is_active);
        }
        if (!empty($product_id)) {
            $builder->where('a.product_id', '=', $product_id);
        }
        if (!empty($customer_id)) {
            $builder->where('a.customer_id', '=', $customer_id);
        }
        if (!empty($invoice_id)) {
            $builder->where('a.invoice_id', '=', $invoice_id);
        }
        if (!empty($invoice_item_id)) {
            $builder->where('a.invoice_item_id', '=', $invoice_item_id);
        }
        
        if (!empty($status)) {
            //$builder->where('a.status', '=', $status);
            $builder->where('a.status', 'like', '%' . $status . '%');
            
        }
        
        if (!empty($from_date)) {
            $builder->whereDate('a.from_date', '=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('a.to_date', '=', $to_date);
        }
        if (!empty($date)) {
            $builder->whereDate('a.from_date', '>=', $date);
            $builder->whereDate('a.to_date', '<=', $date);
        }
 
        
        $builder->orderBy('a.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $amcCollection = $builder->get();
        } else {

            $amcCollection = $builder->skip($start)->take($limit)->get();
        }
                
        $resVal['list'] = $amcCollection;
        return ($resVal);
    }
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Amc Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();

        try {
            $amc = Amc::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Amc Not found';
            return $resVal;
        }
        $amc->is_active=0;
        $amc->updated_by=$currentuser->id;
        $amc->save();
        return $resVal;
    }
    public function serialNoCheck(Request $request){
         $resVal = array();
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $invoice_item_id=$request->input('invoice_item_id');
        $serial_no=$request->input('serial_no');
        if(empty($invoice_item_id)){
            $builder=DB::table('tbl_invoice_item')->where('serial_no','=',$serial_no)->where('is_active','=',1)->count();
        }else{
            $builder=DB::table('tbl_invoice_item')->where('serial_no','=',$serial_no)->where('id','!=',$invoice_item_id)->where('is_active','=',1)->count();
        }
        
        if($builder>0){
            $resVal['message'] = 'Serial No Already Exsists';
            $resVal['success'] = FALSE;
        }else{
            $resVal['message'] = 'Serial No Does Not Exsists';
            $resVal['success'] = True;
        }
       
        return $resVal;
    }
}
?>
