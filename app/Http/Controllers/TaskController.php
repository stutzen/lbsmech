<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use DB;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;

class TaskController extends Controller{
    
    public function save(Request $request){
        $resVal = array();
        $resVal['message'] = 'Task  Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $task = new  Task;
        $task->created_by = $currentuser->id;
        $task->updated_by = $currentuser->id;
        $task->is_active = $request->input('is_active', 1);
        $task->fill($request->all());
        $task->save();
        $resVal['id'] = $task->id;
//         LogHelper::info('Task Save'.$request->fullurl());
        return $resVal;
    }
    
       public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Task Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $task = Task::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Not found';
            return $resVal;
        }
        $task->updated_by = $currentuser->id;
        $task->fill($request->all());
        $task->save();

//         LogHelper::info('Task Update'.$request->fullurl());
        return $resVal;
    }
    
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $task_name = $request->input('task_name', '');
        $is_active = $request->input('is_active', '');

        $builder = DB::table('tbl_task')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($task_name)) {
            $builder->where('task_name', 'like', '%' . $task_name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
//         LogHelper::info('Task Filters'.$request->fullurl());
  //        LogHelper::info('Task List'.$resVal['list'] );
        return ($resVal);
    }

    public function delete(Request $request,$id) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Task Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $task = Task::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Not found';
            return $resVal;
        }

        $task->is_active=0;
        $task->updated_by=$currentuser->id;
        $task->save();

//         LogHelper::info('Task Delete'.$request->fullurl());
        return $resVal;
    }
}