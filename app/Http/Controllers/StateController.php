<?php
namespace App\Http\Controllers;
use DB;
use Validator;
use App\State;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
class StateController extends Controller{
    public function save(Request $request)
    {
        $resVal = array();
        $resVal['message'] = 'State Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $validator = Validator::make($request->all(), [
                    'code' => 'required|unique:tbl_state,code'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            if (array_key_exists('code', $validator->failed())) {
                $resVal['message'] = 'State Code is already exist';
            }
            return $resVal;
    }
    $currentuser=Auth::user();
    $state=new State;
    $state->created_by=$currentuser->id;
    $state->updated_by=$currentuser->id;
    $state->fill($request->all());
    $state->save();
    $resVal['id']=$state->id;
    return $resVal;
    
  }
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $name = $request->input('name');
        $country_name = $request->input('country_name');
        $country_id = $request->input('country_id');
        $builder = DB::table('tbl_state')
               // ->leftJoin('tbl_state as s', 'c.id', '=', 's.country_id')
                ->select('*');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
         if (!empty($name)) {
            $builder->where('name',  'like', '%' . $name . '%');
        }
         if (!empty($country_name)) {
            $builder->where('country_name', '=' , $country_name );
        }

        if (!empty($country_id)) {
            $builder->where('country_id', '=', $country_id);
        }

        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $stateCollection = $builder->get();
        } else {

            $stateCollection = $builder->skip($start)->take($limit)->get();
        }
        
        $resVal['list'] = $stateCollection;
        return ($resVal);
    }
     public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'State Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $state = State::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'State Not found';
            return $resVal;
        }
        $state->delete();
        return $resVal;
    }
    public function update(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'State Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $state = State::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'State Not found';
            return $resVal;
        }
        $state->created_by=$currentuser->id;
        $state->updated_by=$currentuser->id;
        $state->fill($request->all());
        $state->save();

        return $resVal;
    }
}
?>
