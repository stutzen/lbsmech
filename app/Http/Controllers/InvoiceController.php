<?php

namespace App\Http\Controllers;

use DateTime;
use App\User;
use Carbon\Carbon;
use App\PersistentLogin;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\Message;
use Swift_Message;
use DB;
use App\ActivityLog;
use App\PurchasePayment;
use App\Payment;
use App\Invoice;
use App\Invoiceitem;
use App\InvoiceitemSerialNo;
use App\InvoiceTax;
use App\Attributevalue;
use App\Attribute;
use App\DealActivity;
use app\Customer;
use App\Templates;
use App\Transaction;
use App\Helper\SmsHelper;
use App\Helper\MailHelper;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\AccountHelper;
use App\Helper\PdfHelper;
use App\Helper\SalesInvoiceHelper;
use App\Helper\NotificationHelper;
use App\Events\InvoiceNotificationEvent;
use Event;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class InvoiceController extends Controller {

//put your code here
    public function Save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Invoice Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $invoice_no = $request->input('invoice_no', '');
        $prefix = $request->input('prefix', '');

        $invoice_code = '';
        if (!empty($invoice_no)) {

            $get_inv_no = DB::table('tbl_invoice')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('invoice_no', '=', $invoice_no)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Invoice no already exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $invoice_no = $invoice_no;
                $invoice_code = $prefix . $invoice_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_invoice')
                    ->select('invoice_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('invoice_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->invoice_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $invoice_no = $inv_no;
            $invoice_code = $prefix . $inv_no;
        }

        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $refName = $request->input('ref_name', '');
        $refId = $request->input('ref_id', '');
        if (!empty($refName)) {
            if ($refName == 'sales_order' || $refName == 'spare_request') {
                DB::table('tbl_sales_order')->where('id', $refId)->update(['status' => 'Invoiced']);
            } else if ($refName == 'amc') {
                DB::table('tbl_amc')->where('id', $refId)->update(['status' => 'Invoiced']);
            } else if ($refName == 'sales_invoice') {
                // DB::table('tbl_invoice')->where('id', $refId)->update(['status' => 'Invoiced']);
            } else if ($refName == 'service_request') {
                DB::table('tbl_service_request')->where('id', $refId)->update(['status' => 'Invoiced']);
            }
        }
        


        $invoiceItemCol = $request->input('item');
        $invoiceItemCols = array();
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            array_push($invoiceItemCols, $item);
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax(0, $taxId, $taxable_amount, 'sales');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }

        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);

        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));

        $currentuser = Auth::user();
        $invoice = new Invoice;
        $invoice->fill($request->all());

        $invoice->created_by = $currentuser->id;
        $invoice->updated_by = $currentuser->id;
        $invoice->is_active = $request->input('is_active', 1);

        $invoice->subtotal = $subtotal;
        $invoice->tax_amount = $tax_amount;
        $invoice->discount_amount = $discount_amount;
        $invoice->round_off = $roundoff;

        $invoice->total_amount = $total;
        $invoice->invoice_no = $invoice_no;
        $invoice->invoice_code = $invoice_code;

        $invoice->save();



        $totalamount = $invoice->total_amount;

        $payment = DB::table('tbl_payment')->where('invoice_id', $invoice->id)->where('is_active', '=', 1)->sum('amount');

        if (($totalamount <= $payment)) {
// DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
            $invoice->status = 'paid';
        } elseif ($payment == 0) {
            $invoice->status = 'unpaid';
        } elseif ($totalamount > $payment) {

            $invoice->status = 'partiallyPaid';
        }

        $customerItemCol = ($request->input('customattribute'));

        $attribute = array();
        foreach ($customerItemCol as $item) {
            $attribute[$item['attribute_code']] = $item['value'];
        }
        if ($request->input('customattribute') != null) {
            $json_att = json_encode($attribute);
            $invoice->custom_attribute_json = $json_att;
        }
        $invoice->paid_amount = $payment;
        if ($refName == 'sales_invoice') {
            $invoice->ref_id = $invoice->id;
        }


        $invoice->save();


        foreach ($invoiceItemCols as $item) {
            $expiry_date = date_add($invoice->date, date_interval_create_from_date_string('1 year'));

            $invoiceitems = new Invoiceitem;
            $invoiceitems->fill($item);

            $invoiceitems->created_by = $currentuser->id;
            $invoiceitems->updated_by = $currentuser->id;
            $invoiceitems->invoice_id = $invoice->id;
            $invoiceitems->is_active = $request->input('is_active', 1);
            $invoiceitems->customer_id = $invoice->customer_id;
            $invoiceitems->duedate = $invoice->duedate;
            $invoiceitems->paymentmethod = $invoice->paymentmethod;
            $invoiceitems->tax_amount = $invoice->tax_amount;
            $invoiceitems->expiry_date = $expiry_date;
            $invoiceitems->save();

            $invoiceItemSerial = array();
            $invoiceItemSerial['invoice_id'] = $invoice->id;
            $invoiceItemSerial['invoice_item_id'] = $invoiceitems->id;
            $invoiceItemSerial['owned_by'] = $invoice->customer_id;
            $invoiceItemSerial['purchased_by'] = $invoice->customer_id;
            $invoiceItemSerial['product_id'] = $invoiceitems->product_id;
            $invoiceItemSerial['serail_no'] = $invoiceitems->serial_no;
            $invoiceItemSerial['is_active'] = 1;
            $invoiceItemSerial['created_by'] = $currentuser->id;
            $invoiceItemSerial['updated_by'] = $currentuser->id;
            if(!empty($invoiceItemSerial['serail_no']))
                $this->saveInvoiceItemSerial($invoiceItemSerial);
        }

        if ($request->input('customattribute') != null) {
            foreach ($customerItemCol as $attribute) {
//print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $invoice->id;
                $customerattribute->value = $attribute['value'];
                if (isset($attribute['sno'])) {
                    $customerattribute->sno = $attribute['sno'];
                }
                $customerattribute->attribute_code = $attribute['attribute_code'];

                $customerattribute->created_by = $currentuser->id;
                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
//print_r($attribute->id);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

//$customerattribute->fill($attribute);       

                $customerattribute->save();
            }
        }

        $builder = DB::table('tbl_customer')
                        ->select('*')
                        ->where('id', '=', $invoice->customer_id)->first();
        $cus_name = $builder->fname . " " . $builder->lname;


        // Transaction entry for invoice
        $transaction = new Transaction;
        $mydate = $invoice->date;
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

        $transaction->transaction_date = $invoice->date;
        $transaction->voucher_type = Transaction::$SALES;
        $transaction->voucher_number = $invoice->id;
        $transaction->credit = 0.00;
        $transaction->debit = $invoice->total_amount;
        $transaction->account_category = '';

        $transaction->acode = GeneralHelper::getCustomerAcode($invoice->customer_id);
        $transaction->acoderef = $invoice->customer_id;
        $transaction->account = $request->input('account', '');
        $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
        $transaction->pmtcoderef = $request->input('account_id', '');
        $transaction->is_cash_flow = 0;


        $transaction->payment_mode = '';

        $transaction->particulars = '#' . $invoice->id . 'Sales invoice Created for ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->prefix = $invoice->prefix;
        $transaction->save();


        ActivityLogHelper::invoiceSave($invoice);

        if ($request->input('quote_id') != NULL && !empty($request->input('quote_id'))) {

            //Update SalesOrder Status
            SalesInvoiceHelper::updateSalesOrderStatus($invoice->quote_id);
        }

        $resVal['id'] = $invoice->id;
        $code = "invoice";
        GeneralHelper::StockWastageSave($request->input('item'), $code, $invoice->id);
        SmsHelper::invoiceCreatedNotification($invoice);

        GeneralHelper::InvoiceTaxSave($invoice->id, $request->input('tax_id'), $taxable_amount, $subtotal, 'sales');
        $get_pdf_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoice_print_template')
                ->first();



        $invoice_global = $invoice->id;
        PdfHelper::pdfGenerate($invoice->id);

        /* if ($get_pdf_format->value == 't1')

          else if ($get_pdf_format->value == 't2')
          PdfHelper::invoicePrintTemplate2($invoice->id);
          else if ($get_pdf_format->value == 't3')
          PdfHelper::invoicePrintTemplate3($invoice->id);
          else if ($get_pdf_format->value == 't4')
          PdfHelper::invoicePrintTemplate4($invoice->id); */


        $mail_obj = new MailHelper;
        $mail_obj->invoiceCreatedNotification($invoice);
        $response = Event::fire(new InvoiceNotificationEvent($invoice));
        $resVal['notification'] = $response;
        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'invoice';
            $deal->description = "created an invoice";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->ref_id = $invoice->id;

            $deal->deal_id = $deal_id;
            $deal->comments = "created an invoice";

            $deal->save();
        }
        return $resVal;
    }

    public function saveInvoiceItemSerial($invoiceItemSerial) {
        $invoiceserial = new InvoiceitemSerialNo;
        $invoiceserial->fill($invoiceItemSerial);
        $invoiceserial->save();
    }

    public function invoice_csvexport(Request $request) {
        $studentLists = $this->listAll($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
//create a file

        $filename = "Invoice" . "_" . time() . ".csv";
//  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";

        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('#', 'Date', 'Invoice', 'Customer Name', 'Paid Amount', 'Amount'));
        foreach ($studentLists['list'] as $values) {
            fputcsv($output, array($sr, $values->date, 'Invoice No #' . $values->invoice_code, $values->customer_fname, $values->paid_amount, $values->total_amount));
// id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
            $sr++;
        }
        return response()->download($filePath . "/" . $filename, 'Invoice.csv');
    }

    public function salesinvoicejson_csvexport(Request $request) {
        $salesinvoicejsonLists = $this->listAll($request);
        $sr = 1;
        $output = fopen('salesinvoicejson.csv', 'w');
        fputcsv($output, array('#', 'Date', 'Invoice', 'Customer Name', 'Paid Amount', 'Amount', 'detail', 'json'));
        foreach ($salesinvoicejsonLists['list'] as $values) {
            $sx = $values->custom_attribute_json;
            // echo $sx;
            fputcsv($output, array($sr, $values->date, 'Invoice No #' . $values->invoice_code,
                $values->customer_fname, $values->paid_amount, $values->total_amount, $values->custom_attribute_json));

            $sr++;
        }
        return response()->download(getcwd() . "/salesinvoicejson.csv");
    }

    public function invoicelist_csvexport(Request $request) {

        $salesinvoicejsonLists = $this->listAll($request);
        $sr = 1;
        $filename = "SalesInvoice" . "_" . time() . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];
        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);
            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {
                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];
                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;

        $attributeCollection = Attribute::where('attributetype_code', "=", 'invoice')
                        ->where('is_active', '=', 1)
                        ->where('is_show_in_list', '=', 1)
                        ->orderBy('sno', 'asc')->get();
        $output = fopen($filePath . "/" . $filename, 'w');
        $csvHeader = array('#', 'Customer', 'Total', 'Paid', 'Invoice Date', 'Due Date', 'Status');
        foreach ($attributeCollection as $attribute) {
            array_push($csvHeader, $attribute->attribute_label);
        }
        fputcsv($output, $csvHeader);
        foreach ($salesinvoicejsonLists['list'] as $invoice) {
            $json_att = $invoice->custom_attribute_json;
//isset($json_att['attribute_code']) 
            $csv_row = array($invoice->id, $invoice->customer_fname, $invoice->total_amount, $invoice->paid_amount, $invoice->date
                , $invoice->duedate, $invoice->status);
            foreach ($attributeCollection as $attribute) {
                $key = $attribute->attribute_code;
                if (isset($json_att->$key)) {
                    array_push($csv_row, $json_att->$key);
                } else
                    array_push($csv_row, '');
            }
            fputcsv($output, $csv_row);
        }
        return response()->download($filePath . "/" . $filename, 'SalesInvoice.csv');
    }

    public function listAll(Request $request) {
        $resVal = array();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $customerid = $request->input('customer_id');
        $mobile = $request->input('mobile', '');
        $invoice_code = $request->input('invoice_code');
        $fromDate = $request->input('from_duedate', '');
        $toDate = $request->input('to_duedate', '');
        $isactive = $request->input('is_active', '');
        $status = $request->input('status');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $mode = $request->input('mode', '');
        $notes = $request->input('notes');
        $attribute_value = $request->input('attribute_value');
        $discountpercentage = $request->input('discount_percentage');
        $deal_id = $request->input('deal_id', '');
        $company_id = $request->input('company_id');
        $refId = $request->input('refId');
        $refName = $request->input('refName');
        $date=$request->input('invoice_date','');
        $dueDate=$request->input('due_date','');

//print_r($builder);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
//$builder = DB::table('tbl_invoice')
// ->select('*');
        $builder = DB::table('tbl_invoice as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_customer as d', 'i.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'i.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->leftjoin('tbl_company as com', 'com.id', '=', 'i.company_id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 't.tax_percentage as taxpercentage', 'd.fname as payee_name', 'e.fname as consignee_name', 'com.name as company_name','c.city','c.billing_city','c.shopping_city');


        if (!empty($status)) {
            $builder->where('i.status', '=', $status);
        }
        if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }
        if (!empty($invoice_code)) {
            $builder->where('i.invoice_code', 'like', '%' . $invoice_code . '%');
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }
        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }
        if (!empty($company_id)) {
            $builder->where('i.company_id', '=', $company_id);
        }
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('i.duedate', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('i.duedate', '<=', $toDate);
        }
        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }
        if (!empty($date)) {

            $builder->whereDate('i.date', '=', $date);
        }
        if (!empty($dueDate)) {

            $builder->whereDate('i.duedate', '=', $dueDate);
        }
        if (!empty($notes)) {
            $builder->where('i.notes', '=', $notes);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        if ($deal_id != '') {
            $builder->where('i.deal_id', '=', $deal_id);
        }

        if ($attribute_value != '') {
            $builder->groupBy('a.refernce_id');
            $builder->where('a.value', '=', $attribute_value);
        }

        if ($refId != '') {
            $builder->where('i.ref_id', '=', $refId);
        }

        if (!empty($refName)) {
            $builder->where('i.ref_name', '=', $refName);
        }

        $arrays[] = $request->toArray();
//     print_r($arrays);
        $arrays2 = $arrays[0];
        $array_key = array_keys($arrays2);
        $custom_array = array();
        for ($index = 0; $index < count($array_key); $index ++) {
            $arry_ind = $array_key[$index];
            $result = substr($arry_ind, 0, 8);
            if ($result == 'cus_attr') {
                array_push($custom_array, $arry_ind);
            }
        }
        for ($index = 0; $index < count($custom_array); $index ++) {
            $status1 = $custom_array[$index];
            $status = $arrays2[$status1];
            if (!empty($status)) {
                $status = explode("::", $status);
                $key = $status[0];
                $value = $status[1];
                $str = "'%" . '"' . $key . '":"' . $value . '"%' . "'";
                $builder->whereRaw('i.custom_attribute_json  like' . "'%" . '"' . $key . '":"' . $value . '"%' . "'");
//  $builder->Where ('custom_attribute_json', 'like',  $str);                        
            }
        }

        if ($mode == 0 || $mode == '') {
            $builder->orderBy('i.id', 'desc');
        } else if ($mode == 1) {
            $builder->orderBy('i.date', 'asc');
        }

        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $invoiceCollection = $builder->get();
        } else {

            $invoiceCollection = $builder->skip($start)->take($limit)->get();
        }
        $show_all = $request->input('show_all', '');
        foreach ($invoiceCollection as $invoice) {
            $inv_id = $invoice->id;
            if ($show_all == 1) {
                $invoiceitems = DB::table('tbl_invoice_item')->where('invoice_id', $inv_id);
                $invoice->item = $invoiceitems->get();
            }
            $tot_qty = DB::table('tbl_invoice_item')->where('invoice_id', '=', $inv_id)->where('is_active', '=', 1)->sum('qty');
            $invoice->total_qty = $tot_qty;

            $json_att = $invoice->custom_attribute_json;
            if (empty($json_att)) {
                $json_att = "{}";
            }
            $json_att = json_decode($json_att);
            $invoice->custom_attribute_json = $json_att;
        }

        $resVal['list'] = $invoiceCollection;

        return ($resVal);
    }

    public function detail(Request $request) {

        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $invoice_no = $request->input('invoice_no');


        $isactive = $request->input('isactive', '');
        $discountpercentage = $request->input('discount_percentage', '');

//$builder = DB::table('tbl_invoice')->select('*');
        $builder = DB::table('tbl_invoice as i')
                ->leftJoin('tbl_user as cu', 'cu.id', '=', 'i.created_by')
                ->leftJoin('tbl_user as u', 'u.id', '=', 'i.updated_by')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_customer as d', 'i.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'i.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->leftjoin('tbl_company as com', 'com.id', '=', 'i.company_id')
                ->select('i.id', 'i.invoice_no', 'i.invoice_code', 'i.customer_id', 'i.prefix', 'i.date', 'i.duedate', 'i.datepaid'
                , 'i.subtotal', 'i.discount_mode', 'i.discount_amount', 'i.credit_amount', 'i.tax_amount', 'i.total_amount', 'i.status'
                , 'i.paymentmethod', 'i.notes', 'i.is_active', 'i.created_by', 'i.updated_by', 'i.updated_at', 'i.created_at'
                , 'i.tax_id', 'i.insurance_percentage', 'i.packing_percentage', 'i.customer_address', 'i.quote_id', 'i.discount_percentage', 'i.paid_amount', 'i.round_off', 'i.deal_id', 'i.deal_name', 'i.payee', 'i.consignee', 'i.insurance_charge', 'i.packing_charge'
                , 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email', 'com.name as company_name'
                , 't.tax_percentage as taxpercentage', 'cu.f_name as created_byfname', 'cu.l_name as created_bylname', 'u.f_name as updated_byfname', 'u.l_name as updated_bylame', 'd.fname as payee_name', 'e.fname as consignee_name');
        ;

        $invoiceitems = DB::table('tbl_invoice_item')->where('invoice_id', $id)
                ->where('is_active', '=', 1);
// $customerattribute = DB::table('tbl_attribute_value')->where('refernce_id', $id);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($invoice_no)) {
            $builder->where('i.invoice_no', '=', $invoice_no);
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        $builder->orderBy('i.id', 'desc');

        $invoiceCollection = $builder->skip($start)->take($limit)->get();
        if (count($invoiceCollection) == 1) {

            $invoice = $invoiceCollection->first();
            $invoiceItem = $invoiceitems->get();

            foreach ($invoiceItem as $invit) {
                $invoiceitemserialdet = DB::table('tbl_invoice_item_serial_no')->where('invoice_id', $id)
                                ->where('invoice_item_id', $invit->id)
                                ->where('is_active', '=', 1)->first();
                $invit->itemSerialDetail = $invoiceitemserialdet;
            }
            $invoice->item = $invoiceItem;



            foreach ($invoiceItem as $inItem) {
                $amcDate = DB::table('tbl_amc')->select('to_date')
                                ->whereRaw(DB::raw("to_date = (select max(to_date) from tbl_amc where is_active=1)"))
                                ->where('invoice_item_id', '=', $inItem->id)->first();
                if (!empty($amcDate)) {
                    $inItem->amc_date = $amcDate->to_date;
                } else {
                    $inItem->amc_date = null;
                }
            }
//$invoice->customattribute = $customerattribute->get();
            $customAttributeValue = DB::table('tbl_attribute_value as a')
                            ->leftJoin('tbl_attributes as abs', 'a.attributes_id', '=', 'abs.id')
                            ->select('a.*', 'abs.attribute_label as attribute_label')
                            ->where('a.refernce_id', '=', $id)
                            ->where('abs.attributetype_code', '=', 'invoice')->get();


            $customattribute = DB::table('tbl_attributes as abs')
                            ->select('abs.*')
                            ->where('abs.attributetype_code', '=', 'invoice')->get();
            if (count($customAttributeValue) > 0) {

                foreach ($customAttributeValue as $item) {

                    foreach ($customattribute as $customattributeItem) {

                        if ($item->attribute_code == $customattributeItem->attribute_code) {

                            $customattributeItem->value = $item->value;
                        }
                    }
                }
            }

            $invoice->customattribute = $customattribute;

            $invoice_tax_list_collection = DB::table('tbl_invoice_tax')
                            ->select('*')
                            ->where('invoice_id', '=', $id)
                            ->where('is_active', '=', 1)->get();

            $invoice->tax_list = $invoice_tax_list_collection;

            $resVal['data'] = $invoice;
        }

        return ($resVal);
    }

    public function update(Request $request, $id) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Invoices Updated Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $invoice = Invoice::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Invoices Not found';
            return $resVal;
        }
        $invoice_no = $request->input('invoice_no', '');
        $prefix = $request->input('prefix', '');
        $invoice_code = '';
        if (!empty($invoice_no)) {

            $get_inv_no = DB::table('tbl_invoice')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('invoice_no', '=', $invoice_no)
                    ->where('id', '!=', $id)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Invoice no already exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $invoice_no = $invoice_no;
                $invoice_code = $prefix . $invoice_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_invoice')
                    ->select('invoice_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('invoice_no', 'desc')
                    ->where('id', '!=', $id)
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->invoice_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $invoice_no = $inv_no;
            $invoice_code = $prefix . $inv_no;
        }

        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $sales_order_id = $invoice->sales_order_id;
        DB::table('tbl_sales_order')->where('id', $sales_order_id)->update(['status' => 'unpaid']);

        if ($request->input('sales_order_id') != NULL && !empty($request->input('sales_order_id'))) {

            DB::table('tbl_payment')->where('sales_order_id', '=', $request->input('sales_order_id'))->where('is_active', '=', 1)->update(['invoice_id' => $invoice->id]);
            DB::table('tbl_sales_order')->where('id', $request->input('sales_order_id'))->update(['status' => 'invoiced']);
        }

        $invoiceItemCol = $request->input('item');
        $invoiceItemCols = array();
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            array_push($invoiceItemCols, $item);
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax($id, $taxId, $taxable_amount, 'sales');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }

        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);

        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = ($subtotal - $discount_amount) + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));

        $code = "iupdate";
        GeneralHelper::StockAdjustmentDelete($request->input('item'), $code, $id);

        $builder = DB::table('tbl_invoice_item')->where('invoice_id', $id)->delete();
        $builder = DB::table('tbl_invoice_item_serial_no')->where('invoice_id', $id)->delete();

        $invoice->updated_by = $currentuser->id;
        $input_request_data = $request->all();
        if ($input_request_data['tax_id'] == 'invoiced_tax_rate') {
            unset($input_request_data['tax_id']);
        }
        $invoice->fill($input_request_data);
        $invoice->subtotal = $subtotal;
        $invoice->tax_amount = $tax_amount;
        $invoice->discount_amount = $discount_amount;
        $invoice->round_off = $roundoff;
        $invoice->total_amount = $total;
        $invoice->invoice_no = $invoice_no;
        $invoice->invoice_code = $invoice_code;
        $invoice->save();

        foreach ($invoiceItemCols as $item) {
            $expiry_date = date_add($invoice->date, date_interval_create_from_date_string('1 year'));

            $invoiceitems = new Invoiceitem;
            $invoiceitems->invoice_id = $invoice->id;
            $invoiceitems->updated_by = $currentuser->id;
            $invoiceitems->fill($item);
//$invoiceitems->invoice_id = $invoice->id;
            $invoiceitems->is_active = $request->input('is_active', 1);
            $invoiceitems->customer_id = $invoice->customer_id;

            $invoiceitems->duedate = $invoice->duedate;
            $invoiceitems->paymentmethod = $invoice->paymentmethod;
            $invoiceitems->tax_amount = $invoice->tax_amount;

            $invoiceitems->expiry_date = $expiry_date;
            $invoiceitems->save();


            $invoiceItemSerial = array();
            $invoiceItemSerial['invoice_id'] = $invoice->id;
            $invoiceItemSerial['invoice_item_id'] = $invoiceitems->id;
            $invoiceItemSerial['owned_by'] = $invoice->customer_id;
            $invoiceItemSerial['purchased_by'] = $invoice->customer_id;
            $invoiceItemSerial['product_id'] = $invoiceitems->product_id;
            $invoiceItemSerial['serail_no'] = $invoiceitems->serial_no;
            $invoiceItemSerial['is_active'] = 1;
            $invoiceItemSerial['created_by'] = $currentuser->id;
            $invoiceItemSerial['updated_by'] = $currentuser->id; 
            if(!empty($invoiceItemSerial['serail_no']))
                $this->saveInvoiceItemSerial($invoiceItemSerial);
        }

        $attributeCollection = DB::table('tbl_attribute_types')->where('code', 'invoice')->get();
        $attribute = $attributeCollection->first();


        if (count($attributeCollection) > 0) {


            $builder = DB::table('tbl_attribute_value')
                    ->where('attributetype_id', '=', $attribute->id)
                    ->where('refernce_id', '=', $id)
                    ->delete();
//print_r($builder);
        }
        if ($request->input('customattribute') != null) {
            $customerItemCol = ($request->input('customattribute'));

            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $invoice->custom_attribute_json = $json_att;



            foreach ($customerItemCol as $attribute) {
//print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $invoice->id;
                $customerattribute->value = $attribute['value'];
                if (isset($attribute['sno'])) {
                    $customerattribute->sno = $attribute['sno'];
                }

                $customerattribute->attribute_code = $attribute['attribute_code'];

                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();

                    $attribute = $attributeCollection->first();
//print_r($attribute->id);
                    $customerattribute->attributes_id = $attribute->id;
                    $customerattribute->attributetype_id = $attribute->attributetype_id;
                }

                $customerattribute->updated_by = $currentuser->id;
                $customerattribute->save();
            }
        }

        $builder = DB::table('tbl_customer')
                ->select('*')
                ->where('id', '=', $invoice->customer_id)
                ->first();
        $cus_name = $builder->fname . " " . $builder->lname;
        $voucher_number = $invoice->id;
        $builder1 = DB::table('tbl_transaction')
                ->select('*')
                ->where('voucher_number', '=', $voucher_number)
                ->where('voucher_type', '=', 'sales_invoice')
                ->first();
        // Transaction entry for invoice
        $id = $builder1->id;
        $transaction = Transaction::findOrFail($id);
        $mydate = $invoice->date;
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;
        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }
        $transaction->transaction_date = $invoice->date;
        $transaction->voucher_type = Transaction::$SALES;
        $transaction->voucher_number = $invoice->id;
        $transaction->credit = 0.00;
        $transaction->debit = $invoice->total_amount;
        $transaction->account_category = '';
        $transaction->acode = GeneralHelper::getCustomerAcode($invoice->customer_id);
        $transaction->acoderef = $invoice->customer_id;
        $transaction->account = $request->input('account', '');
        $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
        $transaction->pmtcoderef = $request->input('account_id', '');
        $transaction->is_cash_flow = 0;

        $transaction->payment_mode = '';

        $transaction->particulars = '#' . $invoice->id . 'Sales invoice Created for ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

        $transaction->save();
        $totalamount = $invoice->total_amount;

        $payment = DB::table('tbl_payment')->where('invoice_id', '=', $id)->where('is_active', '=', 1)->sum('amount');

        if ($payment == 0) {
            $invoice->status = 'unpaid';
        } else if ($totalamount <= $payment) {

            $invoice->status = 'paid';
        } else if ($totalamount >= $payment) {

            $invoice->status = 'partiallyPaid';
        }
        $invoice->paid_amount = $payment;
        $invoice->save();

        if ($request->input('quote_id') != NULL && !empty($request->input('quote_id'))) {

            //Update SalesOrder Status
            SalesInvoiceHelper::updateSalesOrderStatus($invoice->quote_id);
        }

        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'invoice';
            $deal->description = "updated an invoice";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->ref_id = $invoice->id;

            $deal->deal_id = $deal_id;
            $deal->comments = "updated an invoice";

            $deal->save();
        }

        ActivityLogHelper::invoiceModify($invoice);
        GeneralHelper::StockWastageSave($request->input('item'), "update", $id);

        GeneralHelper::InvoiceTaxUpdate($id, $request->input('tax_id'), $taxable_amount, $subtotal, 'sales');
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Invoice Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $invoice = Invoice::findOrFail($id);
            if ($invoice->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Invoice Not found';
            return $resVal;
        }

        $paymentCollection = Payment::where('invoice_id', '=', $id)->where('is_active', '=', 1)->get();
        if ($paymentCollection->count() > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable to delete, Please remove the payment transaction';
            return $resVal;
        }

//        $trans_id = DB::table('tbl_transaction')
//                ->select('*')
//                ->where('voucher_number', '=', $id)
//                ->where('voucher_type', '=', 'sales_invoice')
//                ->first();
//        $id2 = $trans_id->id;
//        $transaction = Transaction::findOrFail($id2);
//        $transaction->updated_by = $currentuser->id;
//        $transaction->is_active = 0;
//        $transaction->save();
        DB::table('tbl_invoice_item')->where('invoice_id', '=', $id)->update(['is_active' => 0]);

        DB::table('tbl_invoice_item_serial_no')->where('invoice_id', '=', $id)->update(['is_active' => 0]);


        $invoice->is_active = 0;
        $invoice->updated_by = $currentuser->id;
//$invoice->fill($request->all());
        $invoice->update();

        $resVal['id'] = $invoice->id;
        ActivityLogHelper::invoiceDelete($invoice);

        $code = "idelete";
        GeneralHelper::StockAdjustmentDelete($request->input('item'), $code, $invoice->id);
        GeneralHelper::InvoiceTaxDelete($id, 'sales');
        $deal_id = $invoice->deal_id;
        if ($deal_id != 0) {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'invoice';
            $deal->description = "delete an invoice";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->ref_id = $invoice->id;

            $deal->deal_id = $deal_id;
            $deal->comments = "delete an invoice";

            $deal->save();
        }

        return $resVal;
    }

    public function invoiceReminder(Request $request, $id) {
        $get_pdf_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoice_print_template')
                ->first();
        PdfHelper::pdfGenerate($id);
        SmsHelper::invoiceReminderNotification($id);
        $mail_obj = new MailHelper;
        $mail_obj->invoiceReminderNotification($id);

        $resVal['message'] = 'SMS Send Successfully';
        $resVal['success'] = TRUE;
        return $resVal;
    }

    public function pdfPreview(Request $request, $id) {

        $get_pdf_format = DB::table("tbl_app_config")
                ->where('setting', '=', 'invoice_print_template')
                ->first();
        PdfHelper::pdfPreview($id);

        $resVal['message'] = 'Pdf Download Successfully';
        $resVal['success'] = TRUE;
        return $resVal;
    }

    public function salesTaxReport_csvexport(Request $request) {
        $salesTaxReportLists = $this->salesTaxReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
//create a file

        $filename = "salesTaxReport" . "_" . time() . ".csv";
//  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";

        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('ID', 'Tax_Name', 'Particular', 'Amount'));
        foreach ($salesTaxReportLists['list'] as $values) {
            fputcsv($output, array($values->invoice_id, $values->tax_name, 'Invoice #' . $values->invoice_id, $values->tax_amount));
// id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        return response()->download($filePath . "/" . $filename, 'salesTaxReport.csv');
    }

    public function salesTaxReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $from_Date = $request->input('from_Date');
        $to_Date = $request->input('to_Date');
        $tax_id = $request->input('tax_id', '');


        $builder = DB::table('tbl_invoice_tax as t')
                ->leftJoin('tbl_invoice as i', 'i.id', '=', 't.invoice_id')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->select(DB::raw('i.id as invoice_id,i.invoice_code as invoice_code,t.tax_name,t.tax_amount,t.taxable_amount,i.date,c.fname,c.lname'), DB::raw('CONCAT("Invoice", "#", i.invoice_code) AS particulare'))
                ->whereDate('i.date', '>=', $from_Date)
                ->whereDate('i.date', '<=', $to_Date)
                ->where('i.is_active', '=', 1)
                ->orderBy('i.id', 'asc')
                ->orderBy('t.tax_name', 'asc');
        if (!empty($tax_id)) {
            $builder->where('t.tax_id', '=', $tax_id);
        }

        $taxCollection = $builder->get();

        $resVal['list'] = $taxCollection;


        return ($resVal);
    }

    public function salesTaxReportSummery_csvexport(Request $request) {
        $salesTaxReportSummeryLists = $this->salesTaxReportSummery($request);
        $sr = 1;
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
//create a file

        $filename = "salesTaxReportSummery" . "_" . time() . ".csv";
//  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";

        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Serial_No', 'Tax Name', 'Amount'));
        foreach ($salesTaxReportSummeryLists['list'] as $values) {
            fputcsv($output, array($sr, $values->tax_name, $values->amount));
// id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
            $sr++;
        }
        return response()->download($filePath . "/" . $filename, 'salesTaxReportSummery.csv');
    }

    public function salesTaxReportSummery(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
//Get  Previous Date
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $from_Date = $request->input('from_Date');
        $to_Date = $request->input('to_Date');


        $builder = DB::table('tbl_invoice_tax as t ')
                ->leftJoin('tbl_invoice as i', 'i.id', '=', 't.invoice_id')
                ->select(DB::raw('t.tax_name,SUM(t.tax_amount) AS amount'))
                ->whereDate('i.date', '>=', $from_Date)
                ->whereDate('i.date', '<=', $to_Date)
                ->where('t.is_active', '=', 1)
                ->orderBy('i.date', 'asc')
                ->orderBy('t.tax_name', 'asc')
                ->groupBy('t.tax_name')
                ->get();
        $resVal['list'] = $builder;



        return ($resVal);
    }

//Summary Display
    public function summary(Request $request) {


        $resVal = array();
        $resVal['message'] = "Success";
        $resVal['success'] = TRUE;

        $ldate = date('Y-m-d');

//Over due Date
        $bal_amount = DB::table('tbl_invoice')->where('duedate', "<", $ldate)
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1)
                ->sum(DB::raw('total_amount - paid_amount'));
        $count = DB::table('tbl_invoice')->where('duedate', "<", $ldate)
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1)
                ->count();

        $overDueVal = array();
        $overDueVal['amount'] = $bal_amount;
        $overDueVal['invoice_count'] = $count;
        $resVal['overdue'] = $overDueVal;

//Today Due Date                   

        $bal_amount = DB::table('tbl_invoice')->where('duedate', "=", $ldate)
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1)
                ->sum(DB::raw('total_amount - paid_amount'));
        $count = DB::table('tbl_invoice')->where('duedate', "=", $ldate)
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1)
                ->count();

        $todaydueval = array();
        $todaydueval['amount'] = $bal_amount;
        $todaydueval['invoice_count'] = $count;

        $resVal['todate'] = $todaydueval;

//Next 30 Days Due Date
        $ndate = date('Y-m-d', strtotime("+30 days"));
        $bal_amount = DB::table('tbl_invoice')->whereBetween('duedate', array($ldate, $ndate))
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1)
                ->sum(DB::raw('total_amount - paid_amount'));
        $count = DB::table('tbl_invoice')->whereBetween('duedate', array($ldate, $ndate))
                ->whereColumn('paid_amount', "<", 'total_amount')
                ->where('is_active', "=", 1)
                ->count();

        $nextDueVal = array();
        $nextDueVal['amount'] = $bal_amount;
        $nextDueVal['invoice_count'] = $count;

        $resVal['nextdate'] = $nextDueVal;

        return $resVal;
    }

    public function receivablereport_csvexport(Request $request) {
        $receivablereportLists = $this->receivable_list($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
//create a file
        $filename = "receivablereport" . "_" . time() . ".csv";
//  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;

        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('#', 'Customer Name', 'Phone', 'Email', 'Billing Address', 'Shipping Address', 'Amount'));
        foreach ($receivablereportLists['list'] as $values) {
            fputcsv($output, array($sr, $values->customer_fname, $values->phone, $values->email, $values->billing_address, $values->shopping_address, $values->balance));
            $sr++;
        }
        return response()->download($filePath . "/" . $filename, 'receivablereport.csv');
    }

    public function receivable_list(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $customerid = $request->input('customer_id');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $overdue = $request->input('overdue', '');
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $builder = DB::table('tbl_invoice as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('c.id', 'c.fname as customer_fname', 'c.lname as customer_lname', 'c.company', 'c.phone', 'c.email', 'c.billing_address', 'c.shopping_address', 'c.billing_city', 'c.shopping_city', 'c.shopping_state', 'c.billing_state', 'c.billing_country', 'c.shopping_pincode', 'c.billing_pincode', 'c.billing_country', 'c.is_sale', 'c.is_purchase', DB::raw('sum(i.total_amount) - sum(i.paid_amount) as balance'))
                ->where('i.status', '!=', 'paid')
                ->where('i.is_active', '=', '1')
                ->groupBy('c.id')
        ;


        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }

        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {

            $invoiceCollection = $builder->get();
        } else {

            $invoiceCollection = $builder->skip($start)->take($limit)->get();
        }

        /* 9 foreach ($invoiceCollection as $invoice) {
          $json_att = $invoice->custom_attribute_json;

          if (empty($json_att)) {
          $json_att = "{}";
          }
          $json_att = json_decode($json_att);

          $invoice->custom_attribute_json = $json_att;
          } */

        $resVal['list'] = $invoiceCollection;

        return ($resVal);
    }

    public function invoiceMailSend(Request $request) {
        $resVal['message'] = 'Mail Send Successfully';
        $resVal['success'] = TRUE;

        $invoice = $request->invoice_id;
        if ($request->attahed_pdf_copy == 1) {

            $get_pdf_format = DB::table("tbl_app_config")
                    ->where('setting', '=', 'invoice_print_template')
                    ->first();
            if ($get_pdf_format->value == 't1')
                PdfHelper::invoicePrintTemplate1($invoice);
            else if ($get_pdf_format->value == 't2')
                PdfHelper::invoicePrintTemplate2($invoice);
            else if ($get_pdf_format->value == 't3')
                PdfHelper::invoicePrintTemplate3($invoice);
            else if ($get_pdf_format->value == 't4')
                PdfHelper::invoicePrintTemplate4($invoice);
        }
        $mail_obj = new MailHelper;
        $mail_obj->invoiceMailSend($request);
        return $resVal;
    }

    public function customerItemList(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $customer_id = $request->input('customer_id');
        $search = $request->input('search');


        $builder = DB::table('tbl_invoice as i')
                ->leftJoin('tbl_invoice_item as it', function($join) use ($customer_id) {
                    $join->on('i.id', '=', 'it.invoice_id');
                    //   $join->on('it.customer_id', '=', DB::raw("'" . $customer_id . "'"));
                    $join->on('it.is_active', '=', DB::raw("'1'"));
                })
                ->leftjoin('tbl_product as p', 'p.id', '=', 'it.product_id')
                ->join('tbl_customer as c', 'i.customer_id', 'c.id')
                ->leftJoin('tbl_amc as a', 'a.invoice_item_id', '=', 'it.id')
                ->select(DB::raw('ifnull(it.id,0) as invoice_item_id'), 'it.*', 'i.date as sales_date', 'c.fname as customer_name', 'it.product_id as prod_id'
                        , 'i.customer_id', 'it.product_name as product_name', DB::raw("(Case When curdate() <= it.expiry_date then 'i/w' When curdate() <= a.to_date then 'amc' else 'o/w' end) as machine_status")
                        , DB::raw("(case when TIMESTAMPDIFF( YEAR, i.date, now() )> 0 then concat(TIMESTAMPDIFF( YEAR, i.date, now() ),' years ',TIMESTAMPDIFF( MONTH, i.date, now() ) % 12 ,' months '
                                        ,FLOOR( TIMESTAMPDIFF( DAY, i.date, now() ) % 30.4375 ) ,' days ') when TIMESTAMPDIFF( MONTH, i.date, now() ) % 12 > 0 then concat(
                                        TIMESTAMPDIFF( MONTH, i.date, now() ) % 12 ,' months '
                                        ,FLOOR( TIMESTAMPDIFF( DAY, i.date, now() ) % 30.4375 ) ,' days ') else  concat(FLOOR( TIMESTAMPDIFF( DAY, i.date, now() ) % 30.4375 ) ,' days ') end) as life_time"))
                ->where('i.is_active', '=', 1)
                ->where('i.customer_id', '=', $customer_id)
                ->where('p.type','!=','service');

        if (!empty($search)) {
            $builder->whereRaw(DB::raw("(p.name like '%$search%' or it.serial_no='$search')"));
        }

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {

            $invoiceCollection = $builder->get();
        } else {

            $invoiceCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $invoiceCollection;

        return $resVal;
    }

    public function machineInfo(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $invoice_item_id = $request->input('invoice_item_id');

        $builder = DB::table('tbl_invoice as i')
                ->leftjoin('tbl_invoice_item as it', 'it.invoice_id', '=', 'i.id')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'it.product_id')
                ->leftjoin('tbl_customer as c', 'c.id', '=', 'i.customer_id')
                ->leftjoin('tbl_amc as a', 'it.id', '=', 'a.invoice_item_id')
                ->select('it.id as invoice_item_id', 'it.product_id', 'p.name as product_name', 'i.customer_id', 'c.fname as customer_name', 'i.date as sales_date', 'it.expiry_date'
                        , DB::raw("(Case When curdate() <= it.expiry_date then 'i/w' When curdate() <= a.to_date then 'amc' else 'o/w' end) as machine_status"), 'it.unit_price as amount'
                        , DB::raw("(case when TIMESTAMPDIFF( YEAR, i.date, now() )> 0 then concat(TIMESTAMPDIFF( YEAR, i.date, now() ),'years',TIMESTAMPDIFF( MONTH, i.date, now() ) % 12 ,'months'
                                        ,FLOOR( TIMESTAMPDIFF( DAY, i.date, now() ) % 30.4375 ) ,'days') when TIMESTAMPDIFF( MONTH, i.date, now() ) % 12 > 0 then concat(
                                        TIMESTAMPDIFF( MONTH, i.date, now() ) % 12 ,'months'
                                        ,FLOOR( TIMESTAMPDIFF( DAY, i.date, now() ) % 30.4375 ) ,'days') else  concat(FLOOR( TIMESTAMPDIFF( DAY, i.date, now() ) % 30.4375 ) ,'days') end) as life_time"))
                ->where('it.id', '=', $invoice_item_id)
                ->where('it.is_active', '=', 1);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {

            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $collection;

        return $resVal;
    }

    public function serialNoDuplication(Request $request) {
        $resVal = array();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $serialNo=$request->input('serial_no');
        $itemId = $request->input('itemId');
        
         if(!empty($itemId)){
             $serial_no = DB::table('tbl_invoice_item_serial_no')
                ->select('serail_no')
                ->where('serail_no','=',$request->serial_no)
                ->where('is_active','=',1)
                ->where('invoice_item_id','!=',$itemId)
                     ->first();
         }
        else {
             $serial_no = DB::table('tbl_invoice_item_serial_no')
                ->select('serail_no')
                ->where('serail_no','=',$request->serial_no)
                ->where('is_active','=',1)
                     ->first();
        }
        if(!empty($serial_no)){
            $resVal['message'] = $serialNo.' is already exist';
            $resVal['success'] = FALSE;
            return $resVal;
        }
    else {
        
            $resVal['message'] = $serialNo.' serial_no Not  exist';
            $resVal['success'] = TRUE;
            return $resVal;
    }
    }

    public function serialNoSearch(Request $request) {
        $resVal = array();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $serial_no = $request->input('serial_no');
        $customerid = $request->input('customer_id');
        $product_id = $request->input('product_id', '');
        $is_active = $request->input('is_active');
        $shopping_city_id = $request->input('shopping_city_id');
        $billing_city_id = $request->input('billing_city_id');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');


        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
//$builder = DB::table('tbl_invoice')
// ->select('*');
        $builder = DB::table('tbl_invoice_item as it')
                ->leftJoin('tbl_customer as c', 'it.customer_id', '=', 'c.id')
                ->leftJoin('tbl_invoice as i', 'it.invoice_id', '=', 'i.id')
                ->leftJoin('tbl_company as com', 'i.company_id', '=', 'com.id')
                ->leftJoin('tbl_product as p', 'it.product_id', '=', 'p.id')
                ->select('it.id', 'it.serial_no', 'i.invoice_code', 'i.invoice_no', 'i.company_id','it.created_at'
                        , 'c.fname as customer_fname', 'c.lname as customer_lname', 'i.type', 'i.ref_name', 'com.name', 'p.name as product_name','it.product_sku as sku','it.unit_price as price','it.qty','c.shopping_city','c.billing_city');
//print_r($builder->toSql());

        if ($is_active != '') {
            $builder->where('it.is_active', '=', $is_active);
        }

        if (!empty($serial_no)) {
            $builder->where('it.serial_no', '=', $serial_no);
        }

        if (!empty($customerid)) {
            $builder->where('it.customer_id', '=', $customerid);
        }

        if (!empty($product_id)) {
            $builder->where('it.product_id', '=', $product_id);
        }  
        if (!empty($fromDate)) {

            $builder->whereDate('it.created_at', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('it.created_at', '<=', $toDate);
        }
        
        if(!empty($billing_city_id)){
            $builder->where('c.billing_city_id','=',$billing_city_id );
        }
        if(!empty($shopping_city_id)){
            $builder->where('c.shopping_city_id', '=',$shopping_city_id);
        }

        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $invoiceCollection = $builder->get();
        } else {

            $invoiceCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $invoiceCollection;

        return ($resVal);
    }

}

?>