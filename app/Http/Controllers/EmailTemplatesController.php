<?php
namespace App\Http\Controllers;
use DB;
use App\EmailTemplates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailTemplatesController
 *
 * @author Deepa
 */
class EmailTemplatesController extends Controller {
    //put your code here
    
     public function update(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'EmailTemplates Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
      
           $emailtemplateCol = $request->all();
           
            foreach ($emailtemplateCol as $std) {
                 if ($std['id'] != NULL) {
                $emailtemplate = EmailTemplates::findOrFail($std['id']);
                 }
                 if (count($emailtemplate) > 0) {
                $emailtemplate->fill($std);
            $emailtemplate->created_by = $currentuser->id;
            $emailtemplate->updated_by = $currentuser->id;
            $emailtemplate->save();
            }
            
            else {
            $resVal['success'] = False;
            $resVal['message'] = 'EmailTemplates Not found';
            return $resVal;
        }
           
            }
        return $resVal;
    }
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $type = $request->input('type', ''); 
        $tplname=$request->input('tplname','');
        
        $builder = DB::table('tbl_email_templates')
                ->select('*');
       
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

       
        if (!empty($type)) {
            $builder->where('type', 'like', '%' . $type . '%');

        }
        if (!empty($tplname)) {
            $builder->where('tplname', 'like', '%' . $tplname . '%');
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }
    
    
    
   
}

?>
