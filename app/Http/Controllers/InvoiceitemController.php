<?php
use App\Http\Controllers;
use DB;
use Invoiceitem;
use Illuminate\Http\Request;
use App\Transformer\InvoiceitemsTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InvoiceitemsController
 *
 * @author Deepa
 */
class InvoiceitemController extends Controller {
    //put your code here
    public function save (Request $request){
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
       $resVal = array();
        $resVal['message'] = 'Invoice item Added Successfully.';
        $resVal['success'] = TRUE;
        
        $currentuser = Auth::user();
        
        
        $invoiceitems = new Invoiceitem;
       
        $invoiceitems->created_by = $currentuser->id;
        $invoiceitems->updated_by = $currentuser->id;
        $invoiceitems->fill($request->all());
        $invoiceitems->save();

        $resVal['id'] = $invoiceitems->id;

        return $resVal;
    
    }
}

?>
