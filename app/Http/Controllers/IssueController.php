<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Issue;
use App\IssueDetail;
use DB;
use App\Helper\GeneralHelper;

class IssueController extends Controller{
    public function save(Request $request){
        $resVal=array();
        $resVal['message']='Issue saved successfully';
        $resVal['success']=True;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
       
        $currentuser = Auth::user()->id;
       
        $issue=new Issue;
        $issue->created_by=$currentuser;
        $issue->updated_by=$currentuser;
        $issue->fill($request->all());        
        $issue->is_active=1;
        $issue->save();
        
        $issue_id=$issue->id;
        $issue_detail=$request->input('issue_detail');
        $balance_qty=0;
        $issued_qty=0;
        $consumed_qty=0;
        if(!empty($issue_detail)){
         foreach($issue_detail as $issue_details){
             $issued=new IssueDetail;
             $balance_qty+=$issue_details['balance_qty'];
             $issued_qty+=$issue_details['issued_qty'];
             $consumed_qty+=$issue_details['consumed_qty'];
             $issued->created_by=$currentuser;
             $issued->updated_by=$currentuser;
             $issued->fill($issue_details);
             $issued->issue_id=$issue_id;            
             $issued->is_active=1;
             $issued->save();
             IssueController::workInputUpdate($issue_details);
         }        
        }
         Issue::where('id','=',$issue_id)->update(['balance_qty'=>$balance_qty,'issued_qty'=>$issued_qty,'consumed_qty'=>$consumed_qty]);
         $resVal['id']=$issue->id;
         return $resVal;
    }
     public function update(Request $request,$id){
        $resVal=array();
        $resVal['message']='Issue updated successfully';
        $resVal['success']=True;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
       
        $currentuser = Auth::user()->id;
        
        try{
            $issue=Issue::findOrFail($id);
        } catch (ModelNotFoundException $ex) {
            $resVal['message']='Issue not found';
            $resVal['success']=False;
            return $resVal;
        }        
        $issue->updated_by=$currentuser;
        $issue->fill($request->all());        
        $issue->is_active=1;
        $issue->save();
        
        $issue_detail=$request->input('issue_detail');
        $issuede=IssueDetail::where('issue_id','=',$id)->delete();
         $balance_qty=0;
        $issued_qty=0;
        $consumed_qty=0;
        if(!empty($issue_detail)){
         foreach($issue_detail as $issue_details){
             $issued=new IssueDetail;
              $balance_qty+=$issue_details['balance_qty'];
             $issued_qty+=$issue_details['issued_qty'];
             $consumed_qty+=$issue_details['consumed_qty'];
              $issued->created_by=$currentuser;
             $issued->updated_by=$currentuser;
             $issued->fill($issue_details);
             $issued->issue_id=$id;            
             $issued->is_active=1;
             $issued->save();
             IssueController::workInputUpdate($issue_details);
         }        
        }
         Issue::where('id','=',$id)->update(['balance_qty'=>$balance_qty,'issued_qty'=>$issued_qty,'consumed_qty'=>$consumed_qty]);
         return $resVal;
    }
     public function delete(Request $request,$id){
        $resVal=array();
        $resVal['message']='Issue deleted successfully';
        $resVal['success']=True;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user()->id;
        try{
            $issue=Issue::findOrFail($id);
        } catch (ModelNotFoundException $ex) {
            $resVal['message']='Issue not found';
            $resVal['success']=False;
            return $resVal;
        }          
        $issue->updated_by=$currentuser;        
        $issue->is_active=0;
        $issue->save();   
        $issued=IssueDetail::where('issue_id','=',$id)->update(['is_active'=>0,'updated_by'=>$currentuser]);    
        return $resVal;
    }
    public function listAll(Request $request){
        $resVal=array();
        $resVal['success']=True;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id=$request->input('id','');
        $responsible_by=$request->input('responsible_by','');
        $mo_id=$request->input('mo_id','');       
        $status=$request->input('status','');
        $is_active=$request->input('is_active','');
        $from_date=$request->input('from_date','');
        $to_date=$request->input('to_date','');
        $date=$request->input('date','');
        $start=$request->input('start',0);
        $limit=$request->input('limit',100);
        
        $builder=DB::table('tbl_issue as i')
                ->leftjoin('tbl_employee as e','e.id','=','i.responsible_by')
                ->leftjoin('tbl_manufacturing_order as m','m.id','=','i.mo_id')
                ->leftjoin('tbl_bom as b','b.id','=','m.bom_id')
                ->select('i.*','e.f_name as emp_name','b.name as bom_name');
        
        if(!empty($id)){
            $builder->where('i.id','=',$id);
        }
        if(!empty($responsible_by)){
            $builder->where('i.responsible_by','=',$responsible_by);
        }
        if(!empty($mo_id)){
            $builder->where('i.mo_id','=',$mo_id);
        }
        if(!empty($from_date)){
            $builder->where('i.date','>=',$from_date);
        }
        if(!empty($to_date)){
            $builder->where('i.date','<=',$to_date);
        }
        if(!empty($date)){
            $builder->where('i.date','=',$date);
        }

        if(!empty($status)){
            $builder->where('i.status','=',$status);
        }
        if($is_active!=''){
            $builder->where('i.is_active','=',$is_active);
        }
        $builder->orderBy('i.id','desc');
        $resVal['total']=$builder->count();
        if($start==0 && $limit==0)
        $collection=$builder->get();
        else
        $collection=$builder->skip($start)->take($limit)->get();
        
        $resVal['list']=$collection;
        
        return $resVal;       
    }
    public function detail(Request $request){
        $resVal=array();
        $resVal['success']=True;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id=$request->input('id','');
        $builder=DB::table('tbl_issue as i')
                ->leftjoin('tbl_employee as e','e.id','=','i.responsible_by')
                ->leftjoin('tbl_manufacturing_order as m','m.id','=','i.mo_id')
                ->leftjoin('tbl_bom as b','b.id','=','m.bom_id')
                ->select('i.*','e.f_name as emp_name','b.name as bom_name')
                ->where('i.id','=',$id)
                ->where('i.is_active','=',1);
       
        $collection = $builder->get();
        $resVal['total']=$collection->count();
        if (count($collection) > 0) {
            $issue = $collection->first();
            $issueDetail = DB::table('tbl_issue_detail')->where('issue_id','=',$id)->where('is_active','=',1);
            $issue->issue_detail = $issueDetail->get();           
            $resVal['list'] = $collection;
           
        } else {
            $resVal['list'] = null;
        }
        
        return $resVal;
    }
    
    public static function workInputUpdate($data){
        $id = $data['work_order_input_id']; //workOrderId
            $qty = $data['issued_qty']; //reservedQty
            $resVal['success'] = TRUE;
            $resVal['message'] = "Quantity Reserved";

            $work = DB::table('tbl_work_order_input')
                    ->where('id', '=', $id)
                    ->where('is_active', '=', 1)
                    ->first();
            $newQty = $work->reserved_qty + $qty;
            $moId = $work->mo_id;

            if ($newQty >= $work->to_consume) {
                $status = 'Issued';
            } else {
                $status = 'Partially Issued';
            }
            GeneralHelper::InventoryUpdate($work->input_pdt_id, $qty, 'order_input');
            DB::table('tbl_work_order_input')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $id)
                    ->update(
                            array(
                                "reserved_qty" => DB::raw('reserved_qty +' . $qty),
                                "status" => $status
                            )
            );

            $workOrder = DB::table('tbl_work_order_input')
                    ->select(DB::raw("SUM(reserved_qty) as reserved_qty, SUM(to_consume) as to_consume"))
                    ->where('mo_id', '=', $moId)
                    ->where('is_active', '=', 1)
                    ->first();
            if ($workOrder->reserved_qty >= $workOrder->to_consume) {
                $moStatus = 'Issued';
            } else {
                $moStatus = 'Partially Issued';
            }

            DB::table('tbl_manufacturing_order')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $moId)
                    ->update(['wo_status' => $moStatus]);
    }
}

