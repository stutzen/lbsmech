<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\EmployeeAllowance;
use App\DealStage;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class EmployeeAllowanceController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Employee Allowance Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        $emp_id=$request->input('emp_id');
        $att = DB::table('tbl_emp_allowance')->where('emp_id', $emp_id)->delete();
        $allowance = $request['allowance'];
        foreach($allowance as $allow)
        {
        
        $emp_allowance = new EmployeeAllowance;
        $emp_allowance->created_by = $currentuser->id;
        $emp_allowance->updated_by = $currentuser->id;
        $emp_allowance->fill($allow);
        $emp_allowance->save();
        
     
        }
      

        //$resVal['id'] = $emp_allowance->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $emp_code = $request->input('emp_code', '');
        $emp_allow_mas_id = $request->input('emp_allow_mas_id', '');
        $is_active = $request->input('is_active', '');
        $type = $request->input('type', '');
        $emp_id = $request->input('emp_id', '');
        $date = $request->input('date');



        //$status = $request->input('status', '');
        $builder = DB::table('tbl_emp_allowance as ea')
                ->leftjoin('tbl_allowance_master as am','am.id','=','ea.emp_allow_mas_id')
                ->select('ea.*','am.name');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('ea.id', '=', $id);
        }
        if (!empty($emp_allow_mas_id)) {
            $builder->where('ea.emp_allow_mas_id', '=', $emp_allow_mas_id);
        }
        if (!empty($date)) {
            $builder->where('ea.date', '=', $date);
        }
        if (!empty($type)) {
            $builder->where('ea.type', '=', $type);
        }
        if ($is_active != '') {
            $builder->where('ea.is_active', '=', $is_active);
        }
        

        if (!empty($emp_code)) {
            $builder->where('ea.emp_code', 'like', '%' . $emp_code . '%');
        }
        
        if (!empty($emp_id)) {
            $builder->where('ea.emp_id', '=', $emp_id);
        }

        $builder->orderBy('ea.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Employee Allowance Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_allowance = EmployeeAllowance::findOrFail($id);
            if ($emp_allowance->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Allowance   Not found';
            return $resVal;
        }
        $emp_allowance->updated_by = $currentuser->id;
        $emp_allowance->fill($request->all());
        $emp_allowance->save();
        return $resVal;
    }

    public function delete(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Allowance  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');

        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        $allowance=$request->all();
      
        foreach($allowance as $allow)
        {
        $id=$allow['id'];
        
        try {
            $emp_allowance = EmployeeAllowance ::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Allowance     Not found';
            return $resVal;
        }

        $emp_allowance->updated_by = $currentuser->id;
        $emp_allowance->is_active = 0;

        $emp_allowance->save();
        
        $resVal['id'] = $emp_allowance->id;
        }
        return $resVal;
    }

}

?>
