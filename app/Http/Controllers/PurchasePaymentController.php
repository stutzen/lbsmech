<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use App\ActivityLog;
use App\PurchasePayment;
use App\Purchaseinvoice;
use App\Purchasequote;
use App\Transaction;
use App\Helper\AccountHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\SalesInvoiceHelper;
use App\Helper\GeneralHelper;
use App\AdvancePayment;
use App\Events\PurchasePaymentNotificationEvent;
use Event;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PurchasePaymentController
 *
 * @author Deepa
 */
class PurchasePaymentController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Purchase Payment Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $purchasepayment = new PurchasePayment;

        $purchasepayment->created_by = $currentuser->id;
        $purchasepayment->updated_by = $currentuser->id;

        $purchasepayment->fill($request->all());
        $purchasepayment->save();

        $cus_name = '';
        $user_name = $currentuser->f_name . ' ' . $currentuser->l_name;

        if ($request->input('purchase_invoice_id') != NULL && !empty($request->input('purchase_invoice_id'))) {
            $paidamount = DB::table('tbl_purchase_payment')
                    ->where('purchase_invoice_id', $request->input('purchase_invoice_id'))
                    ->where('is_active', '=', 1)
                    ->sum('amount');

            $invoiceCollection = Purchaseinvoice::where('id', "=", $request->input('purchase_invoice_id'))->get();
            $collection = $invoiceCollection->first();
            $totalamount = $collection->total_amount;

            if ($paidamount == 0) {
                $collection->status = 'unpaid';
            } else if (($totalamount <= $paidamount)) {
                // DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
                $collection->status = 'paid';
            } elseif ($totalamount >= $paidamount) {

                $collection->status = 'partiallyPaid';
            }

            $collection->paid_amount = $paidamount;

            $collection->save();

            //Code to find purchase customer 
            $cus_det = $collection->customer_id;
            $builder = DB::table('tbl_customer')
                            ->select('*')
                            ->where('id', '=', $cus_det)->first();
            if ($builder != null) {
                $cus_name = $builder->fname . " " . $builder->lname;
            }
        }
        if ($request->input('purchase_quote_id') != NULL && !empty($request->input('purchase_quote_id'))) {
            $paidamount = DB::table('tbl_purchase_payment')
                    ->where('purchase_quote_id', $request->input('purchase_quote_id'))
                    ->where('is_active', '=', 1)
                    ->sum('amount');

            $quoteCollection = Purchasequote::where('id', "=", $request->input('purchase_quote_id'))->get();
            $collection = $quoteCollection->first();
            $totalamount = $collection->total_amount;

            if ($paidamount == 0) {
                $collection->status = 'unpaid';
            } else if (($totalamount <= $paidamount)) {
                // DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
                $collection->status = 'paid';
            } elseif ($totalamount >= $paidamount) {

                $collection->status = 'partiallyPaid';
            }
            $collection->advance_amount = $paidamount;
            $collection->save();

            //Code to find purchase customer 
            $cus_det = $collection->customer_id;
            $builder = DB::table('tbl_customer')
                            ->select('*')
                            ->where('id', '=', $cus_det)->first();
            if ($builder != null) {
                $cus_name = $builder->fname . " " . $builder->lname;
            }
        }
        $transaction = new Transaction;
        $mydate = $purchasepayment->date;
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        if (empty($purchasepayment->adv_payment_id)) {

            $transaction->fiscal_year_id = 0;
            $transaction->fiscal_year_code = $year;
            if ($fiscalYearCollection->count() > 0) {
                $fiscalYear = $fiscalYearCollection->first();
                $transaction->fiscal_year_id = $fiscalYear->id;
                $transaction->fiscal_year_code = $fiscalYear->code;
            }

            $transaction->transaction_date = $purchasepayment->date;
            $transaction->voucher_type = Transaction::$PURCHASE_PAYMENT;
            $transaction->voucher_number = $purchasepayment->id;
            $transaction->credit = 0.00;
            $transaction->debit = $purchasepayment->amount;
            $transaction->account_category = $purchasepayment->tra_category;

            $transaction->acode = GeneralHelper::getCustomerAcode($purchasepayment->customer_id);
            $transaction->acoderef = $purchasepayment->customer_id;
            $transaction->account = $request->input('account', '');
            $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
            $transaction->pmtcoderef = $request->input('account_id', '');
            $transaction->is_cash_flow = 1;



            $transaction->payment_mode = $purchasepayment->payment_mode;
            if ($request->input('purchase_quote_id') != NULL && !empty($request->input('purchase_quote_id')))
                $transaction->particulars = '#' . $request->input('purchase_quote_id') . 'Purchase invoice Quote Advance Payment has paid to ' . $cus_name . ' by ' . $user_name;
            else
                $transaction->particulars = '#' . $purchasepayment->purchase_invoice_id . 'Purchase invoice Payment has paid to ' . $cus_name . ' by ' . $user_name;
            $transaction->created_by = $currentuser->id;
            $transaction->updated_by = $currentuser->id;
            $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $transaction->fill($request->all());
            $transaction->is_active = 1;
            $account_id = $transaction->pmtcoderef;
            AccountHelper::addTransaction($credit = 0, $purchasepayment->amount, $account_id);
            $transaction->prefix = $collection->prefix;

            $transaction->save();
        } else {
            SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($purchasepayment->adv_payment_id);
        }
        SalesInvoiceHelper::updatePurchaseInvoiceStatus($purchasepayment->purchase_invoice_id);
        ActivityLogHelper::purchaseinvoicePaymentSave($collection, $purchasepayment);
       $response=Event::fire(new PurchasePaymentNotificationEvent($purchasepayment));
        $resVal['notification']=$response;
        $resVal['id'] = $purchasepayment->id;

        //Insert Advance Payment 

        $advance_payment = new AdvancePayment;
        $advance_payment->created_by = $currentuser->id;
        $advance_payment->updated_by = $currentuser->id;
        $advance_payment->date = $request->date;
        $advance_payment->amount = $request->amount;
        $advance_payment->used_amount = $request->amount;
        $advance_payment->voucher_type = 'direct_purchase_payment';
        $advance_payment->voucher_no = $purchasepayment->id;
        $advance_payment->customer_id = $request->customer_id;
        $advance_payment->comments = $request->comments;
        $advance_payment->payment_mode = $request->payment_mode;
        $advance_payment->tra_category = $request->tra_category;
        $advance_payment->account_id = $request->account_id;
        $advance_payment->cheque_no=$purchasepayment->cheque_no;
        $advance_payment->status = 'used';
        $advance_payment->is_active = 1;
        $advance_payment->save();

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Payment Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $purchasepayment = PurchasePayment::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Payment Not found';
            return $resVal;
        }
        $purchasepayment->fill($request->all());

        $purchasepayment->created_by = $currentuser->id;
        $purchasepayment->updated_by = $currentuser->id;
        $purchasepayment->save();

        $currentuser = Auth::user();

        $AdvCollection = AdvancePayment::where('voucher_no', "=", $id)
                ->where('voucher_type', "=", 'direct_purchase_payment')
                ->get();
        $advance_payment = $AdvCollection->first();

        if (!empty($advance_payment)) {
            $advance_payment->updated_by = $currentuser->id;
            $advance_payment->date = $request->date;
            $advance_payment->amount = $request->amount;
            $advance_payment->used_amount = $request->amount;
            $advance_payment->voucher_type = 'direct_purchase_payment';
            $advance_payment->voucher_no = $purchasepayment->id;
            $advance_payment->customer_id = $request->customer_id;
            $advance_payment->comments = $request->comments;
            $advance_payment->payment_mode = $request->payment_mode;
            $advance_payment->tra_category = $request->tra_category;
            $advance_payment->account_id = $request->account_id;
            $advance_payment->status = 'used';
            $advance_payment->is_active = 1;
            $advance_payment->save();
        }


        if ($request->input('purchase_invoice_id') != NULL && !empty($request->input('purchase_invoice_id'))) {
            $paidamount = DB::table('tbl_purchase_payment')->where('purchase_invoice_id', $request->input('purchase_invoice_id'))
                    ->sum('amount');

            $invoiceCollection = Purchaseinvoice::where('id', "=", $request->input('purchase_invoice_id'))->get();
            $collection = $invoiceCollection->first();
            $totalamount = $collection->total_amount;

            if (($totalamount <= $paidamount)) {
                // DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
                $collection->status = 'paid';
            } elseif ($totalamount > $paidamount) {

                $collection->status = 'partiallyPaid';
            } else {
                $collection->status = 'unpaid';
            }
            $collection->paid_amount = $paidamount;

            $collection->save();
        }
        if ($request->input('purchase_quote_id') != NULL && !empty($request->input('purchase_quote_id'))) {
            $advanceamount = DB::table('tbl_purchase_payment')->where('purchase_quote_id', $request->input('purchase_quote_id'))->sum('amount');

            $quoteCollection = Purchasequote::where('id', "=", $request->input('purchase_quote_id'))->get();
            $collection = $quoteCollection->first();
            $totalamount = $collection->total_amount;

            if (($totalamount <= $advanceamount)) {
                // DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
                $collection->status = 'paid';
            } elseif ($totalamount >= $advanceamount) {

                $collection->status = 'partiallyPaid';
            } else {
                $collection->status = 'unpaid';
            }
            $collection->advance_amount = $advanceamount;
            $collection->save();
        }

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $invoiceid = $request->input('purchase_invoice_id', '');
        $quoteid = $request->input('purchase_quote_id', '');
        $customerid = $request->input('customer_id');
        $isactive = $request->input('is_active', '');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $paymentmethod = $request->input('payment_mode', '');
        $cheque = $request->input('cheque_no', '');
        $status = $request->input('status', '');
        $mode = $request->input('mode');
        $recevied_by = $request->input('recevied_by');
        //$status = $request->input('status', '');

        $builder = DB::table('tbl_purchase_payment as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_user as u', 'i.created_by', '=', 'u.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname'
                , 'u.f_name as Receiver_fname', 'u.l_name as Receiver_lname');


        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($invoiceid)) {
            $builder->where('i.purchase_invoice_id', '=', $invoiceid);
        }
        if (!empty($quoteid)) {
            $builder->where('i.purchase_quote_id', '=', $quoteid);
        }
        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }
        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('i.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('i.date', '<=', $toDate);
        }
        if (!empty($paymentmethod)) {

            $builder->whereDate('i.payment_mode', '=', $paymentmethod);
        }
        if (!empty($cheque)) {

            $builder->whereDate('i.cheque_no', '=', $cheque);
        }
        if (!empty($status)) {

            $builder->whereDate('i.status', '=', $status);
        }
        if (!empty($recevied_by)) {
            $builder->where('i.created_by', '=', $recevied_by);
        }
        if ($mode == 0 || $mode == '') {
            $builder->orderBy('i.id', 'desc');
        } else {
            $builder->orderBy('i.date', 'asc');
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Payment Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $purchasePayment = PurchasePayment::findOrFail($id);
            if ($purchasePayment->is_active == 0) {
                return $resVal;
            }
            $purchasePaymentinvoice = $purchasePayment->purchase_invoice_id;
            $purchasePaymentquote = $purchasePayment->purchase_quote_id;
            $purchasePaymentamount = $purchasePayment->amount;
            $purchasePaymentisactive = $purchasePayment->is_active;

            $purchasePayment->is_active = 0;
            $purchasePayment->updated_by = $currentuser->id;
            $purchasePayment->update();

            if ($purchasePayment->adv_payment_id != 0) {

                SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($purchasePayment->adv_payment_id);
            }

            if (!empty($purchasePaymentinvoice) && ($purchasePaymentisactive == 1)) {

                $paidamount = DB::table('tbl_purchase_payment')
                                ->where('is_active', '=', 1)
                                ->where('purchase_invoice_id', '=', $purchasePaymentinvoice)->sum('amount');


                $paymentinvoiceCollection = Purchaseinvoice::where('id', "=", $purchasePaymentinvoice)->get();
                $collection = $paymentinvoiceCollection->first();
                $invoicetotalamount = $collection->total_amount;


                if ($paidamount == 0) {
                    $collection->status = 'unpaid';
                } else if (($invoicetotalamount <= $paidamount)) {
                    $collection->status = 'paid';
                } elseif ($invoicetotalamount > $paidamount) {

                    $collection->status = 'partiallyPaid';
                }

                $collection->paid_amount = $paidamount;
                $collection->save();

                $transactionCollection = Transaction::where('voucher_number', "=", $purchasePayment->id)
                        ->where('voucher_type', "=", 'purchase_invoice_payment')
                        ->get();
                $transaction = $transactionCollection->first();

               // AccountHelper::addTransaction($transaction->debit, $debit = 0, $transaction->pmtcoderef);

                if(count($transactionCollection)>0){
               AccountHelper::addTransaction($transaction->debit, $debit = 0, $transaction->pmtcoderef);
               }

                if ($purchasePayment->adv_payment_id == 0) {
                    $transaction->is_active = 0;
                    $transaction->update();
                }

                ActivityLogHelper::purchaseinvoicePaymentDelete($collection, $purchasePayment);
            } else if (!empty($purchasePaymentquote) && ($purchasePaymentisactive == 1)) {

                //DB::connection()->enableQueryLog();
                $paidamount = DB::table('tbl_purchase_payment')
                                ->where('is_active', '=', 1)
                                ->where('purchase_quote_id', '=', $purchasePaymentquote)->sum('amount');

//                $queries = DB::getQueryLog();
//                $last_query = end($queries);
//
//                echo 'Query<pre>';
//                print_r($last_query);


                $paymentinvoiceCollection = Purchasequote::where('id', "=", $purchasePaymentquote)->get();
                $collection = $paymentinvoiceCollection->first();
                $invoicetotalamount = $collection->total_amount;


                if ($paidamount == 0) {
                    $collection->status = 'unpaid';
                } else if (($invoicetotalamount <= $paidamount)) {
                    $collection->status = 'paid';
                } elseif ($invoicetotalamount > $paidamount) {

                    $collection->status = 'partiallyPaid';
                }

                $collection->advance_amount = $paidamount;
                $collection->save();

                $transactionCollection = Transaction::where('voucher_number', "=", $purchasePayment->id)
                        ->where('voucher_type', "=", 'purchase_invoice_payment')
                        ->get();
                $transaction = $transactionCollection->first();

                AccountHelper::addTransaction($transaction->debit, $debit = 0, $transaction->pmtcoderef);

               
                // $transaction->is_active = 0;
                // $transaction->update();

                ActivityLogHelper::purchaseinvoicePaymentDelete($collection, $purchasePayment);
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Payment found';
            return $resVal;
        }

DB::table('tbl_adv_payment')->where('voucher_no','=',$id)->where('voucher_type','=', 'direct_purchase_payment')
                   ->update(['is_active'=>0]);
 
        $AdvCollection = AdvancePayment::where('voucher_no', "=", $id)
                ->where('voucher_type', "=", 'direct_purchase_payment')
                ->get();
        $advance_payment = $AdvCollection->first();


        $resVal['id'] = $purchasePayment->id;


        return $resVal;
    }

}

?>
