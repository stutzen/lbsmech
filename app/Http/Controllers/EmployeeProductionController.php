<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmployeeProductionController
 *
 * @author Stutzen Admin
 */

namespace App\Http\Controllers;

use DB;
use App\EmployeeProduction;
use App\EmployeeTeam;
use App\Account;
use App\EmployeeProductionDetail;
use App\EmployeeProductionOt;
use App\EmployeeSalary;
use App\EmployeeSalaryOtDetail;
use App\EmployeeSalaryTaskDetail;
use App\EmployeeSalaryAllowanceDetail;
use App\EmployeeESIStatement;
use App\EmployeePFStatement;
use App\EmployeeSalaryPreparation;
use Carbon\Carbon;
use App\EmployeeProductionInput;
use App\EmployeeProductionAttendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;
use App\Transaction;
use App\Helper\GeneralHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\AccountHelper;

class EmployeeProductionController extends Controller {

    public function save(Request $request) {
        $resVal['message'] = 'Employee  Production Details Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $employees = $request['employees'];
        foreach ($employees as $employee) {
            $ot_amount = $employee['ot'];
            $ots = 0;
            foreach ($ot_amount as $amount) {
                $ot = $amount['amount'];
                $ots+=$ot;
            }
            $ep = new EmployeeProduction;
            if (!empty($employee['emp_id'])) {
                $emp = DB::table('tbl_employee')
                        ->select('*')
                        ->where('id', '=', $employee['emp_id'])
                        ->first();
                $ep->emp_name = $emp->f_name . ' ' . $emp->l_name;
                $ep->emp_code = $emp->emp_code;
            }

            $currentuser = Auth::user();
            $ep->emp_team_id = $request->input('emp_team_id');
            $ep->date = $request->input('date');
            $ep->ot_amount = $ots;
            $ep->updated_by = $currentuser->id;
            $ep->created_by = $currentuser->id;
//            $ep->status = 'initiated';
            $ep->is_active = 1;
            $ep->fill($employee);
            if ($ep->is_present == 1) {
                 $ep->status = 'initiated';
            } else {
                 $ep->status = 'absent';
            }
			
            $ep->save();
            $details = $employee['details'];
            $ot_details = $employee['ot'];
            if ($employee['details'] != null || $employee['details'] != '')
                EmployeeProductionController::PieceOrAttendanceSave($ep->id, $details, $currentuser->id);
            if ($employee['ot'] != null || $employee['ot'] != '') {
                EmployeeProductionController::employeeOtSave($ep->id, $ot_details, $currentuser->id, $employee['emp_id']);
            }
        }
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Production details  Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        try {
            $empProduction = EmployeeProduction::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Production Not found';
            return $resVal;
        }
        $empProduction->updated_by = $currentuser->id;
        $empProduction->fill($request->all());
         if($empProduction->is_present==1){
                 $empProduction->status = 'initiated';
            }
            else
            {
                 $empProduction->status = 'absent';
            }
        $empProduction->save();

        EmployeeProductionController::PieceOrAttendanceDelete($id);
        EmployeeProductionController::OtDelete($id);
        $details = $request['details'];
        $ot_details = $request['ot'];
        if ($request['details'] != null || $request['details'] != '')
            EmployeeProductionController::PieceOrAttendanceSave($id, $details, $currentuser->id);
        if ($request['ot'] != null || $request['ot'] != '') {
            EmployeeProductionController::employeeOtSave($id, $ot_details, $currentuser->id, $request->emp_id);
        }
        return $resVal;
    }

    public static function PieceOrAttendanceDelete($id) {
        $attendanceDetails = DB::table('tbl_emp_production_attendance')
                        ->select('*')
                        ->where('emp_production_id', '=', $id)->get();
        if ($attendanceDetails != null || $attendanceDetails != '') {
            foreach ($attendanceDetails as $attendance) {
                DB::table('tbl_inventory')->where('id', $attendance->output_pdt_id)
                        ->update(['quantity' => DB::raw('quantity -' . $attendance->production_qty)]);
            }
            $att = DB::table('tbl_emp_production_attendance')->where('emp_production_id', $id)->delete();
        }

        $productionDetails = DB::table('tbl_emp_production_detail')
                        ->select('*')
                        ->where('emp_production_id', '=', $id)->get();
        if ($productionDetails != null || $productionDetails != '') {

            foreach ($productionDetails as $production) {
                DB::table('tbl_inventory')->where('id', $production->output_pdt_id)
                        ->update(['quantity' => DB::raw('quantity -' . $production->production_qty)]);
            }
            $ep = DB::table('tbl_emp_production_detail')->where('emp_production_id', $id)->delete();
        }

        $inputDetails = DB::table('tbl_emp_production_input')
                        ->select('*')
                        ->where('emp_production_id', '=', $id)->get();
        if ($inputDetails != null || $inputDetails != '') {

            foreach ($inputDetails as $input) {
                DB::table('tbl_inventory')->where('id', $input->input_pdt_id)
                        ->update(['quantity' => DB::raw('quantity +' . $input->input_qty)]);
            }
            $ei = DB::table('tbl_emp_production_input')->where('emp_production_id', $id)->delete();
        }
    }

    public static function OtDelete($id) {

        DB::table('tbl_emp_production_ot')->where('emp_production_id', $id)->delete();
    }

    public static function PieceOrAttendanceSave($empProductionId, $details, $currentUserId) {
        foreach ($details as $detail) {
            $epd = new EmployeeProductionDetail();
            $epi = new EmployeeProductionInput();
            $epa = new EmployeeProductionAttendance();
            if (($detail['type']) == "attendance") {
                if (!empty($detail['output_pdt_id'])) {
                    $taskConfig = DB::table('tbl_bom')
                            ->select('*')
                            ->where('output_pdt_id', '=', $detail['output_pdt_id'])
                            ->first();
                    $epa->output_pdt_name = $taskConfig->output_pdt_name;
                    $epa->output_pdt_uom = $taskConfig->output_pdt_uom;
                    $epa->output_pdt_uom_id = $taskConfig->output_pdt_uom_id;
                    $epa->production_unit_price = $taskConfig->unit_production_price;
                    $epa->output_pdt_id = $detail['output_pdt_id'];

                    //updating in inventory

                    DB::table('tbl_inventory')->where('id', $detail['output_pdt_id'])
                            ->update(['quantity' => DB::raw('quantity +' . $detail['production_qty'])]);
                }


                if (!empty($detail['task_id'])) {
                    $taskInfo = DB::table('tbl_bom')
                            ->select('*')
                            ->where('id', '=', $detail['task_id'])
                            ->first();
                    $epa->task_name = $taskInfo->name;
                }


                $epa->fill($detail);
                $epa->task_id = $detail['task_id'];
                $epa->production_qty = $detail['production_qty'];
                $epa->amount = $detail['amount'];
                $epa->emp_production_id = $empProductionId;
                $epa->updated_by = $currentUserId;
                $epa->created_by = $currentUserId;
                $epa->is_active = 1;
                $epa->comments = $detail['comments'];
                $epa->save();

                $inputTask = $detail['inputs'];
                EmployeeProductionController::InputTaskSave($empProductionId, $epa->id, $inputTask, $currentUserId, "Attendance");
            } else {  //ie piece
                $epd = new EmployeeProductionDetail();
                if (!empty($detail['output_pdt_id'])) {
                    $taskConfig = DB::table('tbl_bom')
                            ->select('*')
                            ->where('output_pdt_id', '=', $detail['output_pdt_id'])
                            ->first();
                    $epd->output_pdt_name = $taskConfig->output_pdt_name;
                    $epd->output_pdt_uom = $taskConfig->output_pdt_uom;
                    $epd->output_pdt_uom_id = $taskConfig->output_pdt_uom_id;
                    $epd->production_unit_price = $taskConfig->unit_production_price;
                    $epd->output_pdt_id = $detail['output_pdt_id'];
                    DB::table('tbl_inventory')->where('id', $detail['output_pdt_id'])
                            ->update(['quantity' => DB::raw('quantity +' . $detail['production_qty'])]);
                }

                if (!empty($detail['task_id'])) {
                    $taskInfo = DB::table('tbl_bom')
                            ->select('*')
                            ->where('id', '=', $detail['task_id'])
                            ->first();
                    $epd->task_name = $taskInfo->name;
                }


                $epd->fill($detail);
                $epd->task_id = $detail['task_id'];
                $epd->production_qty = $detail['production_qty'];
                $epd->amount = $detail['amount'];
                $epd->emp_production_id = $empProductionId;
                $epd->updated_by = $currentUserId;
                $epd->created_by = $currentUserId;
                $epd->is_active = 1;
                $epd->comments = $detail['comments'];
                $epd->save();
                $inputTask = $detail['inputs'];
                EmployeeProductionController::InputTaskSave($empProductionId, $epd->id, $inputTask, $currentUserId, "Piece");
            }
        }
    }

    public static function employeeOtSave($empProductionId, $ot_detail, $currentUserId, $emp_id) {

        foreach ($ot_detail as $ot) {
            $epi = new EmployeeProductionOt();


            $epi->fill($ot);
            $epi->emp_production_id = $empProductionId;
            $epi->emp_id = $emp_id;
            $epi->updated_by = $currentUserId;
            $epi->created_by = $currentUserId;
            $epi->is_active = 1;
            $epi->save();
        }
    }

    public static function InputTaskSave($empProductionId, $empProductionAttendanceId, $inputTasks, $currentUserId, $type) {
        foreach ($inputTasks as $inputTask) {
            $epi = new EmployeeProductionInput();
            $epi->emp_production_id = $empProductionId;
            $epi->ref_id = $empProductionAttendanceId;
            $epi->ref_name = $type;
            $epi->task_id = $inputTask['task_id'];
            $epi->input_qty = $inputTask['input_qty'];
            if (!empty($inputTask['config_input_id'])) {
                $taskInput = DB::table('tbl_bom_detail')
                        ->select('*')
                        ->where('id', '=', $inputTask['config_input_id'])
                        ->first();
                $epi->input_pdt_name = $taskInput->input_pdt_name;
                $epi->input_pdt_uom = $taskInput->input_pdt_uom;
                $epi->input_pdt_uom_id = $taskInput->input_pdt_uom_id;
                $epi->input_pdt_id = $taskInput->input_pdt_id;

                DB::table('tbl_inventory')->where('id', $inputTask['input_pdt_id'])
                        ->update(['quantity' => DB::raw('quantity -' . $inputTask['input_qty'])]);
            }
            $epi->fill($inputTask);
            if (!empty($inputTask['task_id'])) {
                $taskInfo = DB::table('tbl_bom')
                        ->select('*')
                        ->where('id', '=', $inputTask['task_id'])
                        ->first();
                $epi->task_name = $taskInfo->name;
            }

            $epi->updated_by = $currentUserId;
            $epi->created_by = $currentUserId;
            $epi->is_active = 1;
            $epi->save();
        }
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $emp_id = $request->input('emp_id', '');
        $emp_team_id = $request->input('emp_team_id', '');
        $emp_name = $request->input('emp_name', '');
        $emp_code = $request->input('emp_code', '');
        $is_active = $request->input('is_active', '');
        $date = $request->input('date');
        $present = $request->input('is_present');


        $builder = DB::table('tbl_emp_production as ep')
                ->leftjoin('tbl_emp_team as et', 'ep.emp_team_id', '=', 'et.id')
                ->select('ep.*', 'et.name as emp_team_name');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('ep.id', '=', $id);
        }
        if (!empty($date)) {
            $builder->where('ep.date', '=', $date);
        }

        if (!empty($emp_id)) {
            $builder->where('ep.emp_id', '=', $emp_id);
        }
        if (!empty($emp_team_id)) {
            $builder->where('ep.emp_team_id', '=', $emp_team_id);
        }
         if ($present!='') {
            $builder->where('ep.is_present', '=', $present);
                }

        if (!empty($emp_name)) {
            $builder->where('ep.emp_name', 'like', '%' . $emp_name . '%');
        }


        if (!empty($emp_code)) {
            $builder->where('ep.emp_code', '=', $emp_code);
        }

        if ($is_active != '') {
            $builder->where('ep.is_active', '=', $is_active);
        }
        $builder->orderBy('ep.id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $empProduction = $builder->get();
        } else {

            $empProduction = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $empProduction;
        return ($resVal);
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');

        $builder = DB::table('tbl_emp_production as ep')
                ->leftjoin('tbl_emp_team as et', 'ep.emp_team_id', '=', 'et.id')
                ->leftjoin('tbl_employee as e', 'ep.emp_id', '=', 'e.id')
                ->select('ep.*', 'et.name as emp_team_name', 'e.salary_type as emp_type', 'e.salary_amount as emp_salary','e.payroll_type')
                ->where('ep.is_active', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('ep.id', '=', $id);
        }

        $builder->orderBy('ep.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $cl) {
            $c = array();
            $details = array();
            $ot = array();
            $detail = DB::table('tbl_emp_production_attendance as epa')
                    ->leftjoin('tbl_bom as tc', 'tc.id', '=', 'epa.task_id')
                    ->select(DB::raw('epa.id'), DB::raw('task_id'), DB::raw('task_name'), DB::raw('epa.output_pdt_id'), DB::raw('epa.output_pdt_name'), DB::raw('emp_production_id')
                            , DB::raw('epa.output_pdt_uom'), DB::raw('production_qty'), DB::raw('production_unit_price'), DB::raw('amount'), DB::raw('"Attendance" as type'), DB::raw('duration'), DB::raw('product_for_id'), DB::raw('product_for_name'))
                    ->where('emp_production_id', '=', $id)
                    ->where('epa.is_active', '=', 1);

            $builder1 = DB::table('tbl_emp_production_detail as epd')
                    ->leftjoin('tbl_bom as tc', 'tc.id', '=', 'epd.task_id')
                    ->select('epd.id', DB::raw('task_id'), DB::raw('task_name'), DB::raw('epd.output_pdt_id'), DB::raw('epd.output_pdt_name'), DB::raw('emp_production_id')
                            , DB::raw('epd.output_pdt_uom'), DB::raw('production_qty'), DB::raw('production_unit_price'), DB::raw('amount'), DB::raw('"Piece" as type'), DB::raw('"0" as duration'), DB::raw('product_for_id'), DB::raw('product_for_name'))
                    ->where('emp_production_id', '=', $id)
                    ->where('epd.is_active', '=', 1)
                    ->union($detail)
                    ->orderBy('id', 'asc');
            $details = $builder1->get();
            $ot_detail = DB::table('tbl_emp_production_ot as ot')
                    ->leftJoin('tbl_employee as e', 'ot.emp_id', '=', 'e.id')
                    ->leftJoin('tbl_allowance_master as al', 'ot.allowance_master_id', '=', 'al.id')
                    ->select('ot.*', DB::raw('e.emp_code'), DB::raw('al.name'))
                    ->where('ot.emp_production_id', '=', $id)
                    ->where('ot.is_active', '=', 1)
                    ->get();
            foreach ($details as $c) {
                $inputs = DB::table('tbl_emp_production_input as epi')
                        ->leftjoin('tbl_bom_detail as tci', 'epi.config_input_id', '=', 'tci.id')
                        ->select('epi.*', 'tci.input_qty as production_input_qty')
                        ->where('epi.ref_id', $c->id)
                        ->where('epi.ref_name', $c->type)
                        ->where('epi.emp_production_id', $c->emp_production_id);
                $c->inputs = $inputs->get();
            }
            $cl->details = $details;
            $cl->ot = $ot_detail;
        }

        $resVal['list'] = $collection;
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Production details Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $is_active = 0;

        $currentuser = Auth::user();
        try {
            $ep = EmployeeProduction::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Production detalis Not found';
            return $resVal;
        }
        EmployeeProductionController::PieceOrAttendanceDelete($id);
        EmployeeProductionController::OtDelete($id);
        $ep->is_active = $is_active;
        $ep->updated_by = $currentuser->id;
        $ep->fill($request->all());
        $ep->save();
        return $resVal;
    }

    public function empSalary(Request $request) {
             $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }


        $emp_id = $request->input('emp_id');
        $team_id = $request->input('team_id');
        $show_all = $request->input('show_all', '');
        $month = $request->input('month', '');
        $salary_type=$request->input('salary_type');
        $payroll_type=$request->input('payroll_type'); 
        if (strcasecmp($payroll_type,'monthly')==0) {

            $current_date = Carbon::now();
            $year = $current_date->format('Y');

            $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $from_date = $year . "-" . $month . "-" . "1";
            $to_date = $year . "-" . $month . "-" . $days;
        }elseif(strcasecmp($payroll_type,'daily')==0){
           $from_date = $request->input('from_date');
            $to_date = $request->input('to_date');
        } elseif(strcasecmp($payroll_type,'weekly')==0) {
            $from_date = $request->input('from_date');
            $to_date = $request->input('to_date');
        }


        $builder = DB::table('tbl_employee as e')
                ->leftjoin('tbl_emp_production as ep', 'e.id', '=', 'ep.emp_id')
                ->leftjoin('tbl_emp_production_detail as epd', 'ep.id', '=', 'epd.emp_production_id')
                ->leftjoin('tbl_emp_production_attendance as epa', 'ep.id', '=', 'epa.emp_production_id')
                ->select('ep.emp_id', 'ep.emp_name', 'e.salary_type', 'e.salary_amount','e.payroll_type', 'e.max_leave_count', 'ep.emp_code', 'ep.emp_team_id', 'e.esi_percentage', 'e.pf_percentage', 'ep.status', DB::raw('(sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))  as week_salary')
                        , DB::raw('(CASE WHEN e.salary_type="piece" THEN ((CASE WHEN (sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0))) >= 100 THEN (100*(coalesce(e.esi_percentage,0))/100) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.esi_percentage,0))/100) END)) ELSE  ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.esi_percentage,0))/100) END)  AS esi_amount')
                        , DB::raw('(CASE WHEN e.salary_type="piece" THEN ((CASE WHEN (sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0))) >= 100 THEN (100*(coalesce(e.pf_percentage,0))/100) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.pf_percentage,0))/100) END)) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.pf_percentage,0))/100) END) AS pf_amount')
                        , DB::raw('(CASE WHEN e.salary_type="piece" THEN ((CASE WHEN (sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0))) >= 100 THEN (100*(coalesce(e.esi_percentage,0))/100) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.esi_percentage,0))/100) END)+(CASE WHEN (sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0))) >= 100 THEN (100*(coalesce(e.pf_percentage,0))/100) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.pf_percentage,0))/100) END)) ELSE ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.esi_percentage,0))/100) + ((sum(coalesce(epd.amount,0))+sum(coalesce(epa.amount,0)))*(coalesce(e.pf_percentage,0))/100) END)  as deduction_amount')
                        , DB::raw('(sum(coalesce(epd.production_qty,0))+sum(coalesce(epa.production_qty,0))) as overall_qty'))
                ->where('ep.is_active', '=', 1)
                ->where('ep.is_present', '=', 1)
                ->where('ep.status', '=', 'initiated')
                ->groupby('ep.emp_id');
        
         if (strcasecmp($salary_type,'attendance')==0){
            $builder->where('e.salary_type', '=', 'attendance');
        } elseif  (strcasecmp($salary_type,'piece')==0) {
            $builder->where('e.salary_type', '=', 'piece');
        }
//        if  (strcasecmp($salary_type,'attendance')==0) { 
//            $builder->where('e.salary_type', '=', 'attendance');
//        } elseif  (strcasecmp($salary_type,'piece')==0) {
//            $builder->where('e.salary_type', '=', 'piece');
//        }

        if (strcasecmp($payroll_type,'monthly')==0) {
            $builder->where('e.payroll_type', '=', 'monthly');
        } elseif(strcasecmp($payroll_type,'daily')==0){
              $builder->where('e.payroll_type', '=', 'daily');
        }elseif(strcasecmp($payroll_type,'weekly')==0) {
            $builder->where('e.payroll_type', '=', 'weekly');
        }

        if (!empty($from_date)) { 
            $builder->whereDate('ep.date', '>=', $from_date);
        }

        if (!empty($to_date)) {
            $builder->whereDate('ep.date', '<=', $to_date);
        }
     
        if (!empty($emp_id)) {
            $builder->where('e.id', '=', $emp_id);
        }
        if (!empty($team_id)) {
            $builder->where('e.team_id', '=', $team_id);
        }
        $resVal['total'] = $builder->count();
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        $from_date1 = strtotime($from_date);
        $to_date1 = strtotime($to_date);

        $datediff = $to_date1 - $from_date1;

        $days = floor($datediff / (60 * 60 * 24));
        $count = 0;
        foreach ($collection as $t) {
            $count ++;
            $c = $t->emp_id;
            $c1 = $t->emp_team_id;
            $advance=$t->deduction_amount;  
            
            $detail = DB::table('tbl_emp_production_attendance as epa')
                    ->leftjoin('tbl_emp_production as em', 'em.id', '=', 'epa.emp_production_id')
                    ->select(DB::raw('epa.id'), DB::raw('epa.task_id'), DB::raw('epa.task_name'), DB::raw('epa.output_pdt_id'), DB::raw('epa.output_pdt_name'), DB::raw('epa.emp_production_id'), DB::raw('epa.production_qty'), DB::raw('epa.production_unit_price'), DB::raw('epa.amount'))
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '<=', $to_date)
                    ->where('em.is_active', '=', 1)
                    ->where('em.is_present', '=', 1)
                    ->where('em.status', '=', 'initiated')
                    ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "));



            $bulider1 = DB::table('tbl_emp_production_detail as epd')
                    ->leftjoin('tbl_emp_production as em', 'em.id', '=', 'epd.emp_production_id')
                    ->select(DB::raw('epd.id'), DB::raw('epd.task_id'), DB::raw('epd.task_name'), DB::raw('epd.output_pdt_id'), DB::raw('epd.output_pdt_name'), DB::raw('epd.emp_production_id'), DB::raw('epd.production_qty'), DB::raw('epd.production_unit_price'), DB::raw('epd.amount'))
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '<=', $to_date)
                    ->where('em.is_active', '=', 1)
                    ->where('em.is_present', '=', 1)
                    ->where('em.status', '=', 'initiated')
                    ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "))
                    ->union($detail)
                    ->orderBy('id', 'asc');

            $ot_amount = DB::table('tbl_emp_production_ot as ot')
                    ->leftjoin('tbl_emp_production as em', 'em.id', '=', 'ot.emp_production_id')
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '<=', $to_date)
                    ->where('em.is_active', '=', 1)
                    ->where('em.is_present', '=', 1)
                    ->where('em.status', '=', 'initiated')
                    ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "))
                    ->sum('ot.amount');

            $ot_detail = DB::table('tbl_emp_production_ot as ot')
                    ->leftjoin('tbl_emp_production as em', 'em.id', '=', 'ot.emp_production_id')
                    ->leftjoin('tbl_allowance_master as am', 'am.id', '=', 'ot.allowance_master_id')
                    ->select('ot.id as ot_id', 'ot.amount', 'ot.allowance_master_id', 'am.name')
                    ->where('em.date', '>=', $from_date)
                    ->where('em.date', '<=', $to_date)
                    ->where('em.is_active', '=', 1)
                    ->where('em.is_present', '=', 1)
                    ->where('em.status', '=', 'initiated')
                    ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "));
            
            $adv_deduct=DB::table('tbl_emp_adv_deduction')
                    
                    ->whereDate('date','>=',$from_date)
                    ->whereDate('date','<=',$to_date)
                    ->whereRaw(DB::raw("(emp_id = '$emp_id' or  emp_id = '$c' ) "))
                    ->where('is_active','=',1)
                    ->sum('deduction_amt');
            
            $t->adv_deduction_amt=$adv_deduct;
            $t->deduction_amount=$advance+ $t->adv_deduction_amt;
            
            $task = $bulider1->get();
            $t->ot_amount = $ot_amount;

            $fixed_all_detail = DB::table('tbl_emp_allowance as al')
                            ->leftjoin('tbl_emp_production as em', 'em.emp_id', '=', 'al.emp_id')
                            ->select('al.amount')
                            ->where('em.date', '>=', $from_date)
                            ->where('em.date', '<=', $to_date)
                            ->where('em.is_active', '=', 1)
                            ->where('em.is_present', '=', 1)
                            ->where('em.status', '=', 'initiated')
                            ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "))
                            ->groupby('al.id')->get();

            $calculate_emp_work_days = DB::table('tbl_emp_production')
                    ->select('id')
                    ->where('date', '>=', $from_date)
                    ->where('date', '<=', $to_date)
                    ->where('is_active', '=', 1)
                    ->where('is_present', '=', 1)
                    ->where('status', '=', 'initiated')
                    ->whereRaw(DB::raw("(emp_id = '$emp_id' or  emp_id = '$c' ) and (emp_team_id='$team_id' or emp_team_id='$c1') "))
            ;

            $fixed_all_amount = 0;
            $emp_working_days = $calculate_emp_work_days->count();
            foreach ($fixed_all_detail as $all) {
                $fixed_all_amount = $fixed_all_amount + $all->amount;
            }
            $fixed_all_amount = ($fixed_all_amount * $emp_working_days );
            $t->fixed_allowance_amount = $fixed_all_amount;

            $salay_amount1 = ($t->week_salary + $t->ot_amount + $fixed_all_amount) - $t->deduction_amount;

            //For Calculate Max Leave
            $salary_type = $t->salary_type;

            $leave = array();

            if ($salary_type == 'attendance') {
                $calculate_emp_leave_days = DB::table('tbl_emp_production')
                        ->select('id')
                        ->where('date', '>=', $from_date)
                        ->where('date', '<=', $to_date)
                        ->where('is_active', '=', 1)
                        ->where('is_present', '=', 0)
                        ->where('status', '=', 'absent')
                        ->whereRaw(DB::raw("(emp_id = '$emp_id' or  emp_id = '$c' ) and (emp_team_id='$team_id' or emp_team_id='$c1') "))
                ;
                
                   $emp_leave_days = $calculate_emp_leave_days->count();
                if ($emp_leave_days > 0) {
                    $leave = DB::table('tbl_emp_production as ep')
                                    ->leftjoin('tbl_employee as e', 'ep.emp_id', '=', 'e.id')
                                    ->select('e.max_leave_count', 'e.salary_amount as per_day_salary', DB::raw("(CASE WHEN count(status) > 1 THEN concat(count(status),' ','days') ELSE concat(count(status),' ','day') END) as casual_leave")
                                            , DB::raw('(CASE WHEN e.max_leave_count > count(status) THEN count(status) * e.salary_amount  WHEN e.max_leave_count < count(status) THEN e.max_leave_count * e.salary_amount  WHEN (e.max_leave_count = count(status)) THEN e.max_leave_count * e.salary_amount   END) as casual_leave_amount'))
                         ->where('ep.date', '>=', $from_date)
                        ->where('ep.date', '<=', $to_date)
                        ->where('ep.is_active', '=', 1)
                        ->where('ep.is_present', '=', 0)
                        ->where('ep.status', '=', 'absent')
                        ->whereRaw(DB::raw("(ep.emp_id = '$emp_id' or  ep.emp_id = '$c' ) and (ep.emp_team_id='$team_id' or ep.emp_team_id='$c1') "))->get();
                } else {
                    $leave = array();
                   }
                   
                $emp_leave_days = $calculate_emp_leave_days->count();
                if ($emp_leave_days == $t->max_leave_count) {
                    $max_leave_count = $t->max_leave_count;
                    $basic_salary = $t->salary_amount;
                    $salay_amount = $salay_amount1 + ($max_leave_count * $basic_salary);
                    $t->salay_amount = $salay_amount;
                } else if ($emp_leave_days > $t->max_leave_count) {
                    $basic_salary = $t->salary_amount;
                    $salay_amount = $salay_amount1 + ($t->max_leave_count * $basic_salary);
                    $t->salay_amount = $salay_amount;
                } else if ($emp_leave_days < $t->max_leave_count) {
                    $basic_salary = $t->salary_amount;
                    $salay_amount = $salay_amount1 + ($emp_leave_days * $basic_salary);
                    $t->salay_amount = $salay_amount;
                }
            } else {
                               $t->salay_amount = $salay_amount1;
            }
            
            if ($show_all == 1) {
                $t->ot_detail = $ot_detail->get();
                $t->task_detail = $task;
                $t->leave = $leave;

                $fixed_all_detail = DB::table('tbl_emp_allowance as al')
                        ->leftjoin('tbl_emp_production as em', 'em.emp_id', '=', 'al.emp_id')
                        ->leftjoin('tbl_allowance_master as am', 'am.id', '=', 'al.emp_allow_mas_id')
                        ->select('al.id as allowance_id', 'al.amount', 'al.emp_allow_mas_id', 'am.name')
                        ->where('em.date', '>=', $from_date)
                        ->where('em.date', '<=', $to_date)
                        ->where('em.is_active', '=', 1)
                        ->where('em.is_present', '=', 1)
                        ->where('em.status', '=', 'initiated')
                        ->whereRaw(DB::raw("(em.emp_id = '$emp_id' or  em.emp_id = '$c' ) and (em.emp_team_id='$team_id' or em.emp_team_id='$c1') "))
                        ->groupby('al.id');

                $t->fixed_allowance = $fixed_all_detail->get();
            }
        }
        $resVal['success'] = True;
        $resVal['total'] = $count;
        $resVal['list'] = $collection;
        return $resVal;
    }

    public function empSalarySave(Request $request) {
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $resVal['message'] = 'Salary Sucessfully saved';
        $resVal['success'] = FALSE;

        $collection = $request->json()->all();
        $account_id = $request->input('account_id');
        //$collection = $request->all();

        $account_name = DB::table('tbl_accounts')
                        ->select('account')->where('id', '=', $account_id)->first();

        $account_type = $account_name->account;


        $overall_amount = 0;
        $paid_count = 0;

        foreach ($collection as $preparation) {
            $emp_id = $preparation['emp_id'];
            $team_id = $preparation['emp_team_id'];
            $from_date = $preparation['from_date'];
            $to_date = $preparation['to_date'];
            $month = $preparation['month'];
            $salary_type=$preparation['salary_type'];
            $payroll_type=$preparation['payroll_type'];
            $amount = $preparation['salary_amount'];

            $overall_amount+=$amount;
            $team = count($team_id);
            $paid_count+=$team;

            if ((!empty($from_date)) && (!empty($to_date))) {
                $type =$payroll_type;
                $month_name = '';
                $emp_type =$salary_type;
            }

            if (!empty($month)) {
                $current_date = Carbon::now();
                $year = $current_date->format('Y');

                $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                $from_date = $year . "-" . $month . "-" . "1";
                $to_date = $year . "-" . $month . "-" . $days;

                $month_name = date('F', mktime(0, 0, 0, $month, 15));
                $type = $payroll_type;
                $emp_type =$salary_type;
            }
        }

//        $team_count = DB::table('tbl_emp_team')
//                        ->select('current_count')->where('id', '=', $team_id)->get();
//
//        foreach ($team_count as $team) {
//            $teamCount = $team->current_count;
//        }
        $value = DB::table('tbl_emp_salary_preparation')
                ->select('total_paid')->where('not_paid', '>', 0)
                ->whereDate('from_date', '=', $from_date)
                ->whereDate('to_date', '=', $to_date)
                ->where('team_id', '=', $team_id)
                ->where('payroll_type','=',$type)
                ->where('salary_type', '=', $emp_type)
                ->get();

        $totalPaid = 0;
        foreach ($value as $total) {
            $totalPaid = $total->total_paid;
        }

        $team_count = DB::table('tbl_employee')->select(DB::raw('count(salary_type) as emp_count'))->where('salary_type', '=', $emp_type)
                ->where('payroll_type','=',$type)
                        ->where('team_id', '=', $team_id)->first();

        $teamCount = $team_count->emp_count;

        $not_paid = $teamCount - $paid_count - $totalPaid;
        $total_paid = $totalPaid + $paid_count;

        $emp_salary_prepare = new EmployeeSalaryPreparation;
        $emp_salary_prepare->from_date = $from_date;
        $emp_salary_prepare->to_date = $to_date;
        $emp_salary_prepare->salary_type = $emp_type;
        $emp_salary_prepare->salary_month = $month_name;
        $emp_salary_prepare->overall_amount = $overall_amount;
        $emp_salary_prepare->team_id = $team_id;
        $emp_salary_prepare->created_by = $currentuser->id;
        $emp_salary_prepare->updated_by = $currentuser->id;
        $emp_salary_prepare->is_active = 1;
        $emp_salary_prepare->account_id = $account_id;
        $emp_salary_prepare->team_count = $teamCount;
        $emp_salary_prepare->paid_count = $paid_count;
        $emp_salary_prepare->not_paid = $not_paid;
        $emp_salary_prepare->total_paid = $total_paid;
        $emp_salary_prepare->payroll_type=$type;
        $emp_salary_prepare->save();
        if ($account_id != 0) {
            $account = Account::where('id', '=', $account_id)->first();
            if ($account != null) {
                $account->balance = $account->balance - $overall_amount;
                $account->updated_by = $currentuser->id;
                $account->save();
            }
        }

        foreach ($collection as $salary) {
            $emp_id = $salary['emp_id'];
            $team_id = $salary['emp_team_id'];
            $from_date = $salary['from_date'];
            $to_date = $salary['to_date'];
            $month = $salary['month'];
            $salary_type=$preparation['salary_type'];
            $payroll_type=$preparation['payroll_type'];
            $amount = $salary['salary_amount'];
            $overall_amount+=$amount;


            if ((!empty($from_date)) && (!empty($to_date))) {
                $type = $payroll_type;
                $month_name = '';
            }

            if (!empty($month)) {
                $current_date = Carbon::now();
                $year = $current_date->format('Y');

                $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                $from_date = $year . "-" . $month . "-" . "1";
                $to_date = $year . "-" . $month . "-" . $days;

                $month_name = date('F', mktime(0, 0, 0, $month, 15));
                $type =$payroll_type;
            }

            $empsalary = new EmployeeSalary;
            $current_date = Carbon::now();
            $current_date = $current_date->format('Y-m-d');



            $empsalary->emp_team_id = $salary['emp_team_id'];
            $empsalary->from_date = $from_date;
            $empsalary->to_date = $to_date;
            $empsalary->emp_id = $salary['emp_id'];
            $empsalary->esi_percentage = $salary['esi_percentage'];
            $empsalary->pf_percentage = $salary['pf_percentage'];
            $empsalary->week_salary = $salary['week_salary'];
            $empsalary->esi_amount = $salary['esi_amount'];
            $empsalary->pf_amount = $salary['pf_amount'];
            $empsalary->deduction_amount = $salary['deduction_amount'];
            $empsalary->salary_amount = $salary['salary_amount'];
            $empsalary->overall_qty = $salary['overall_qty'];
            $empsalary->ot_amount = $salary['ot_amount'];
            $empsalary->fixed_allowance_amount = $salary['fixed_allowance_amount'];
            $empsalary->date = $current_date;
            $empsalary->salary_type = $type;
            $empsalary->salary_month = $month_name;
            $empsalary->emp_salary_prepare_id = $emp_salary_prepare->id;
            $empsalary->created_by = $currentuser->id;
            $empsalary->updated_by = $currentuser->id;
            $empsalary->is_active = 1;
            $empsalary->save();

            //Activity log
            ActivityLogHelper::EmployeeSalarySave($empsalary);

            $ot_detail = $salary['ot_detail'];
            foreach ($ot_detail as $ot_det) {
                $ot = new EmployeeSalaryOtDetail;

                $ot->emp_salary_id = $empsalary->id;
                $ot->amount = $ot_det['amount'];
                $ot->allowance_master_id = $ot_det['allowance_master_id'];
                $ot->emp_salary_prepare_id = $emp_salary_prepare->id;
                $ot->is_active = 1;
                $ot->ot_id = $ot_det['ot_id'];
                $ot->created_by = $currentuser->id;
                $ot->updated_by = $currentuser->id;
                $ot->save();
            }
            $task_detail = $salary['task_detail'];
            foreach ($task_detail as $task_det) {
                $task = new EmployeeSalaryTaskDetail;

                $task->emp_salary_id = $empsalary->id;
                $task->productName = $task_det['output_pdt_name'];
                $task->task_config_id=$task_det['output_pdt_id'];
                $task->unit_price = $task_det['production_unit_price'];
                $task->outputQty = $task_det['production_qty'];
                $task->emp_salary_prepare_id = $emp_salary_prepare->id;
                $task->is_active = 1;
                $task->created_by = $currentuser->id;
                $task->updated_by = $currentuser->id;
                $task->save();
            }
            
            $leave_detail=$salary['leave']; 
            foreach ($leave_detail as $task_det) {
                $leave = new EmployeeSalaryTaskDetail;

                $leave->emp_salary_id = $empsalary->id;
                $leave->productName = 'casual_leave';
                $leave->task_config_id=0;
                $leave->unit_price = $task_det['casual_leave_amount'];
                $leave->emp_salary_prepare_id = $emp_salary_prepare->id;
                $leave->is_active = 1;
                $leave->created_by = $currentuser->id;
                $leave->updated_by = $currentuser->id;
                
//                $value=explode(' ', $task_det['casual_leave']);
                if($task_det['max_leave_count'] > $task_det['casual_leave']){
                    $leave->outputQty = $task_det['casual_leave'];
                }else
                {
                     $leave->outputQty = $task_det['max_leave_count'];
                }
                $leave->save();
            }
            
            $allowance_detail = $salary['fixed_allowance'];
            foreach ($allowance_detail as $allo_det) {
                $allowance = new EmployeeSalaryAllowanceDetail;

                $allowance->emp_salary_id = $empsalary->id;
                $allowance->allowance_id = $allo_det['allowance_id'];
                $allowance->amount = $allo_det['amount'];
                $allowance->emp_salary_prepare_id = $emp_salary_prepare->id;
                $allowance->is_active = 1;
                $allowance->created_by = $currentuser->id;
                $allowance->updated_by = $currentuser->id;
                $allowance->save();
            }
            DB::table('tbl_emp_production')
                    ->where('emp_id', '=', $emp_id)
                    ->where('emp_team_id', '=', $team_id)
                    ->where('is_present', '=', 1)
                    ->where('status', '=', 'initiated')
                    ->whereDate('date', "<=", $to_date)
                    ->whereDate('date', ">=", $from_date)
                    ->update(['status' => 'provided']);
            DB::table('tbl_emp_production')
                    ->where('emp_id', '=', $emp_id)
                    ->where('emp_team_id', '=', $team_id)
                    ->where('is_present', '=', 0)
                    ->update(['status' => 'absent']);

            $emp_detail = DB::table('tbl_employee')
                    ->select('*')
                    ->whereRaw(DB::raw("id = '$emp_id' "))
                    ->where('is_active', '=', 1)
                    ->first();

            $from_date1 = strtotime($from_date);
            $to_date1 = strtotime($to_date);

            $datediff = $to_date1 - $from_date1;

            $days = floor($datediff / (60 * 60 * 24));

            $employer_percentage = $emp_detail->employer_esi_percentage;
            $employer_percentage_amount = ($salary['week_salary']) * ($employer_percentage / 100);
            $emp_esi = new EmployeeESIStatement;
            $emp_esi->emp_id = $salary['emp_id'];
            $emp_esi->esi_percentage = $salary['esi_percentage'];
            $emp_esi->esi_amount = $salary['esi_amount'];
            $emp_esi->days = $days;
            $emp_esi->emp_salary_id = $empsalary->id;
            $emp_esi->salary_amount = $salary['week_salary'];
            $emp_esi->employer_esi_percentage = $employer_percentage;
            $emp_esi->employer_esi_amount = $employer_percentage_amount;
            $emp_esi->total_esi_amount = $employer_percentage_amount + $salary['esi_amount'];
            $emp_esi->emp_salary_prepare_id = $emp_salary_prepare->id;
            $emp_esi->created_by = $currentuser->id;
            $emp_esi->updated_by = $currentuser->id;
            $emp_esi->is_active = 1;
            $emp_esi->save();

            $employer_pf_percentage = 0.00;
            $employer_pf_percentage_amount = 0.00;

            $emp_pf = new EmployeePFStatement;
            $emp_pf->emp_id = $salary['emp_id'];
            $emp_pf->pf_percentage = $salary['pf_percentage'];
            $emp_pf->pf_amount = $salary['pf_amount'];
            $emp_pf->days = $days;
            $emp_pf->emp_salary_id = $empsalary->id;
            $emp_pf->emp_salary_prepare_id = $emp_salary_prepare->id;
            $emp_pf->salary_amount = $salary['week_salary'];
            $emp_pf->employer_pf_percentage = $employer_pf_percentage;
            $emp_pf->employer_pf_amount = $employer_pf_percentage_amount;
            $emp_pf->total_pf_amount = $employer_pf_percentage_amount + $salary['pf_amount'];
            $emp_pf->created_by = $currentuser->id;
            $emp_pf->updated_by = $currentuser->id;
            $emp_pf->is_active = 1;
            $emp_pf->save();
        }



        $transaction = new Transaction;
        $mydate = date('Y-m-d');
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

        $transaction->transaction_date = $mydate;
        $transaction->voucher_type = Transaction::$SALARY_AMOUNT;
        $transaction->voucher_number = $emp_salary_prepare->id;
        $transaction->credit = 0.00;
        $transaction->debit = $emp_salary_prepare->overall_amount;
        $transaction->account_category = '';

        $transaction->acode = '';
        $transaction->acoderef = $emp_salary_prepare->team_id;
        $transaction->account = $account_type;
        $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
        $transaction->pmtcoderef = $request->input('account_id', '');
        $transaction->is_cash_flow = 0;


        $transaction->payment_mode = '';

        $transaction->particulars = '#' . $emp_salary_prepare->id . 'Salary amount for team_id : ' . $emp_salary_prepare->team_id . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

        $transaction->save();
         AccountHelper::addTransaction($credit = 0, $transaction->debit, $transaction->pmtcoderef);       

        DB::table('tbl_emp_adv_deduction')->where('emp_id','=',$emp_id)->whereDate('date','>=',$from_date)->whereDate('date','<=',$to_date)->update(['is_deducted'=>1,'account_id'=>$request->input('account_id', '')]);

        $resVal['success'] = True;
        $resVal['id'] = $empsalary->id;

//        $resVal['list'] = $collection;
        return $resVal;
    }

    public function productionDetail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $emp_id = $request->input('emp_id');
        $team_id = $request->input('team_id');

        $bulider = DB::table('tbl_product as ti')
                ->leftjoin(DB::raw("(select output_pdt_id,sum(epa.production_qty) as epa_qty from tbl_emp_production_attendance epa left join tbl_product t1 on t1.id = output_pdt_id  left join tbl_emp_production ep on ep.id = epa.emp_production_id where ep.date >= '$from_date' and  ep.date <= '$to_date' and ep.is_present='1' group by t1.id) as epa1 on epa1.output_pdt_id=ti.id")
                        , function($join2) {
                    
                })
                ->leftjoin(DB::raw("(select output_pdt_id,sum(edp.production_qty) as epd_qty from tbl_emp_production_detail edp left join tbl_product t0 on t0.id = output_pdt_id left join tbl_emp_production ep on ep.id = edp.emp_production_id where ep.date >= '$from_date' and  ep.date <= '$to_date' and ep.is_present='1'  group by t0.id)as epd1 on epd1.output_pdt_id=ti.id")
                        , function($join2) {
                    
                })
                ->leftjoin(DB::raw("(select epi.input_pdt_id as input_id,sum(epi.input_qty) as input_qty from tbl_emp_production_input epi left join tbl_product t2 on t2.id =epi.input_pdt_id left join tbl_emp_production ep on ep.id = epi.emp_production_id where ep.date >= '$from_date' and ep.date <= '$to_date' and ep.is_present='1' group by t2.id)as epi1 on epi1.input_id=ti.id")
                        , function($join2) {
                    
                })
                ->select('ti.id', 'ti.name as productName', DB::raw('coalesce(epd1.epd_qty,0)+coalesce(epa1.epa_qty,0) as outputQty'), 'epi1.input_qty')
                ->distinct('ti.id');


        $resVal['list'] = $bulider->get();
        return $resVal;
    }

    public function noProdutionEnterList(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $date = $request->input('date');

        $builder = DB::table('tbl_emp_team')
                ->select('id', 'name')
                ->whereNotIn('id', function($q)use ($date) {
            $q->select('emp_team_id')
            ->from('tbl_emp_production')
            ->where('date', '=', $date);
        });


        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);




        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }


        $resVal['list'] = $collection;
        return ($resVal);
    }

    public function empSalaryDetail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $emp_id = $request->input('emp_id');
        $preparation_id = $request->input('preparation_id');
        $emp_team_id = $request->input('emp_team_id');
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_emp_salary as esy')
                ->leftjoin("tbl_emp_team as et", 'esy.emp_team_id', '=', 'et.id')
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'esy.emp_id')
                ->select('e.f_name', 'e.emp_code','e.payroll_type', DB::raw('esy.*'), 'et.name as team_name', 'esy.emp_team_id as team_id');


        if (!empty($emp_id)) {
            $builder->where('esy.emp_id', '=', $emp_id);
        }
        if (!empty($emp_team_id)) {
            $builder->where('esy.emp_team_id', '=', $emp_team_id);
        }
        if (!empty($id)) {
            $builder->where('esy.id', '=', $id);
        }
        if (!empty($from_date)) {
            $builder->whereDate('esy.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('esy.date', '<=', $to_date);
        }

        if (!empty($preparation_id)) {
            $builder->where('esy.emp_salary_prepare_id', '=', $preparation_id);
        }

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $salary) {
            $id = $salary->id;
            $builder1 = DB::table('tbl_emp_salary_allowance_detail')
                    ->select('*')
                    ->where('emp_salary_id', '=', $id);


            $task = DB::table('tbl_emp_salary_task_detail')
                    ->select('*')
                    ->where('emp_salary_id', '=', $id);

            $ot = DB::table('tbl_emp_salary_ot_detail')
                    ->select('*')
                    ->where('emp_salary_id', '=', $id);

            $pfstmt = DB::table('tbl_employee_pf_statement')
                    ->select('*')
                    ->where('emp_salary_id', '=', $id);

            $esistmt = DB::table('tbl_employee_esi_statement')
                    ->select('*')
                    ->where('emp_salary_id', '=', $id);


            $salary->employee_salary_allowance_detail = $builder1->get();
            $salary->employee_salary_ot_detail = $ot->get();
            $salary->employee_salary_task_detail = $task->get();
            $salary->employee_pf_statement = $pfstmt->get();
            $salary->employee_esi_statement = $esistmt->get();
        }


        $resVal['list'] = $collection;
        return ($resVal);
    }
  
}
