<?php

namespace App\Http\Controllers;

use DB;
Use App\Customer;
use App\User;
use App\Album;
Use Illuminate\Http\Request;
use App\Transformer\CustomerTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\AlbumViewingHistoryLog;
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Storage\StorageObject;
use Google\Cloud\Storage;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomerController
 *
 * @author Deepa
 */
abstract class Google_Signer {

    /**
     * Signs data, returns the signature as binary data.
     */
    abstract public function sign($data);
}

/**
 * Signs data.
 *
 * Only used for testing.
 *
 * @author Brian Eaton <beaton@google.com>
 */
class Google_P12Signer extends Google_Signer {

    // OpenSSL private key resource
    private $privateKey;

    // Creates a new signer from a .p12 file.
    function __construct($p12, $password) {
        if (!function_exists('openssl_x509_read')) {
            throw new Exception(
            'The Google PHP API library needs the openssl PHP extension');
        }

        // This throws on error
        $certs = array();
        if (!openssl_pkcs12_read($p12, $certs, $password)) {
            throw new Google_AuthException("Unable to parse the p12 file.  " .
            "Is this a .p12 file?  Is the password correct?  OpenSSL error: " .
            openssl_error_string());
        }
        // TODO(beaton): is this part of the contract for the openssl_pkcs12_read
        // method?  What happens if there are multiple private keys?  Do we care?
        if (!array_key_exists("pkey", $certs) || !$certs["pkey"]) {
            throw new Google_AuthException("No private key found in p12 file.");
        }
        $this->privateKey = openssl_pkey_get_private($certs["pkey"]);
        if (!$this->privateKey) {
            throw new Google_AuthException("Unable to load private key in ");
        }
    }

    function __destruct() {
        if ($this->privateKey) {
            openssl_pkey_free($this->privateKey);
        }
    }

    function sign($data) {
        if (!openssl_sign($data, $signature, $this->privateKey, "sha256")) {
            throw new Google_AuthException("Unable to sign data");
        }

        return $signature;
    }

}

class UrlSignController extends Controller {

    //put your code here
    public function url(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $config = config('app');

        $location_to_key_file = $config['SERVICE_ACCOUNT_PKCS12_FILE_PATH'];
        $serviceAccountName = $config['SERVICE_ACCOUNT_EMAIL'];
        $expiration = $config['EXPEIRATION'];
        $bucketName = $config['BUCKET_NAME'];

        $verb = $request->input('verb');
        $contentype = $request->input('contentype');
        $id = $request->input('objName');
        $privateKey = '';

        if (file_exists($location_to_key_file)) {
            $fh = fopen($location_to_key_file, "r");
            while (!feof($fh)) {
                $privateKey .= fgets($fh);
            }
            fclose($fh);
        }
        else
        {
            $resVal['message'] = 'P12 file is not found';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $signer = new Google_P12Signer($privateKey, "notasecret");
        $ttl = time() + 3600;
        $stringToSign = $verb . "\n\n" . $contentype . "\n" . $ttl . "\n" . '/' . $bucketName . '/' . $id;
        $signature = $signer->sign($stringToSign);
        // echo '';
        $finalSignature = base64_encode($signature);
        $host = "https://storage.googleapis.com/" . $bucketName;

//echo $host;
        $urlArray = array();
//        $url = $host . "/" . $id . "?Expires=" . $ttl . "&GoogleAccessId=" .
//                urlencode($serviceAccountName) . "&Signature=" .
//                str_replace(array('-', '_',), array('%2B', '%2F'), urlencode($finalSignature)) . '%3D';


        $url = $host . "/" . $id . "?Expires=" . $ttl . "&GoogleAccessId=" .
                $serviceAccountName . "&Signature=" . urlencode($finalSignature);


        $urlArray['url'] = $url;

        $resVal['data'] = $urlArray;
        return $resVal;
    }

}

?>
