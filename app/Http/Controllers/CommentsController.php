<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Comments;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;

class CommentsController extends Controller {
    
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Comments Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
         $currentuser = Auth::user();
           
        $empcomments=$request->all();
     
        foreach($empcomments as $comment)
        {
       
        $comments=new Comments;
        $comments->fill($comment);
        $comments->created_by=$currentuser->id;
        $comments->updated_by=$currentuser->id;
        $comments->is_active=$request->input('is_active',1);
        
          $comments->save();
        }
      
       return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $emp_task_activity_id=$request->input('emp_task_activity_id');
        $is_active=$request->input('is_active');
       
        $builder = DB::table('tbl_comments')
                ->select('*');
        $resVal['success'] = TRUE;
       
        
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($emp_task_activity_id)) {
            $builder->where('emp_task_activity_id', '=', $emp_task_activity_id);
        }
           
       if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
       
       
        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        
        $resVal['list'] = $builder->get();
      
        return ($resVal);
    }
    
    
     public function update(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Comments Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
            
         $empcomments=$request->all();
     
        foreach($empcomments as $comment)
        {
       $cid=$comment['id'];
    
        try {
            $comments = Comments::findOrFail($cid);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Comments Not found';
            return $resVal;
        }
        $comments->fill($comment);
        $comments->created_by=$currentuser->id;
        $comments->updated_by=$currentuser->id;
        $comments->is_active=$request->input('is_active',1);
        
          $comments->save();
        }

        return $resVal;
    }
    
    public function delete(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Comments Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       $empcomments=$request->all();
     
        foreach($empcomments as $comment)
        {
       $cid=$comment['id'];
    
        try {
            $comments = Comments::findOrFail($cid);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Comments Not found';
            return $resVal;
        }
        $comments->is_active = 0;
        $comments->updated_by = $currentuser->id;
        $comments->save();
        }
        return $resVal;
    }
}
