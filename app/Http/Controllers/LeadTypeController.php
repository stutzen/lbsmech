<?php
namespace App\Http\Controllers;
use DB;
use Validator;
use App\LeadType;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
class LeadTypeController extends Controller
{
    public function save(Request $request) {
       
        $resVal = array();
        $resVal['message'] = 'Lead Type Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

         $value=DB::table('tbl_lead_master')->where('name','=',$request->input('name'))->where('is_active','=',1)->get();
        if(count($value) > 0){
         $resVal['success'] = FALSE;
         $resVal['message'] = 'Lead Type Already Exsists';
            return $resVal;
        }else{
        $currentuser=Auth::user();
        $lead=new LeadType;      
        $lead->updated_by=$currentuser->id;
        $lead->created_by=$currentuser->id;
        $lead->fill($request->all());
        $lead->is_active=1;
        $lead->save();
       
        $resVal['id'] = $lead->id;
        }

        return $resVal;
    }
      public function update(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Lead Type Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $value=DB::table('tbl_lead_master')->where('name','=',$request->input('name'))->where('id','!=',$id)->where('is_active','=',1)->get();
        if(count($value) > 0){
         $resVal['success'] = FALSE;
         $resVal['message'] = 'Lead Type Already Exsists';
            return $resVal;
        }else{
         $currentuser = Auth::user();
        try {
            $lead= LeadType::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Lead Type Not found';
            return $resVal;
        }
        $lead->updated_by=$currentuser->id;
        $lead->is_active=1;
        $lead->fill($request->all());
        $lead->save();
        }
        return $resVal;
    }
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        /*$ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }*/
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active=$request->input('is_active');
        $type=$request->input('type');
        
        $builder = DB::table('tbl_lead_master')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (strcasecmp($type,'lead')==0) {
            $builder->whereRaw(DB::raw("(type='lead' or type='both')"));
        }else if (strcasecmp($type,'deal')==0) {
            $builder->whereRaw(DB::raw("(type='deal' or type='both')"));
        }else if (strcasecmp($type,'both')==0) {
            $builder->whereRaw(DB::raw("type='both'"));
        }
        if($is_active != ''){
            $builder->where('is_active','=',1);
        }
        
 
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $Collection = $builder->get();
        } else {

            $Collection = $builder->skip($start)->take($limit)->get();
        }
        
        $resVal['list'] = $Collection;
        return ($resVal);
    }
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Lead Type Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $lead = LeadType::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Lead Type Not found';
            return $resVal;
        }
        $currentuser = Auth::user();
        $lead->is_active=0;
        $lead->updated_by=$currentuser->id;
        $lead->save();
        return $resVal;
    }
}
?>
