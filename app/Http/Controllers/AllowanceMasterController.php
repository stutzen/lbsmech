<?php


namespace App\Http\Controllers;

use DB;
use Validator;
use App\AllowanceMaster;
use App\DealStage;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;

class AllowanceMasterController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Allowance Master Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $allowance = new AllowanceMaster;

        $allowance->created_by = $currentuser->id;
        $allowance->updated_by = $currentuser->id;
        $allowance->is_active=$request->input('is_active',1);
        $allowance->fill($request->all());
        $allowance->save();

        $resVal['id'] = $allowance->id;

        return $resVal;
    }
    
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Allowance Master  Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $allowance = AllowanceMaster::findOrFail($id);
            if ($allowance->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = ' Allowance Master   Not found';
            return $resVal;
        }
       $allowance->updated_by = $currentuser->id;
        $allowance->fill($request->all());
        $allowance->save();
        return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name=$request->input('name');
        $is_active=$request->input('is_active');
       



        //$status = $request->input('status', '');
        $builder = DB::table('tbl_allowance_master')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
       
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }
    
      public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Allowance Master  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');

        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $allowance = AllowanceMaster ::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = ' Allowance Master Not found';
            return $resVal;
        }

        $allowance->updated_by = $currentuser->id;
        $allowance->is_active = 0;

        $allowance->update();
        $resVal['id'] = $allowance->id;
        return $resVal;
    }

}
