<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransportController
 *
 * @author Stutzen Admin
 */

namespace App\Http\Controllers;

use DB;
use App\Transport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

class TransportController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Transport Details Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $transport = new Transport;
        $transport->created_by = $currentuser->id;
        $transport->updated_by = $currentuser->id;
        $transport->is_active = 1;
        $transport->fill($request->all());
        $transport->save();
        $resVal['id'] = $transport->id;
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Trasnport Details  Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $transport = Transport::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Trasnport Details  Not found';
            return $resVal;
        }
        $transport->updated_by = $currentuser->id;
        $transport->is_active = 1;
        $transport->fill($request->all());
        $transport->save();
        $resVal['id'] = $transport->id;
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transport Details Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        $currentuser = Auth::user();
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $transport = Transport::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transport Details Not found';
            return $resVal;
        }
        $transport->updated_by = $currentuser->id;
        $transport->is_active = 0;
        $transport->save();
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $email = $request->input('email', '');
        $mobile_no = $request->input('mobile_no', '');
        $city_name = $request->input('city_name', '');
        $is_active = $request->input('is_active');

        $builder = DB::table('tbl_transport')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        if (!empty($email)) {
            $builder->where('email', 'like', '%' . $email . '%');
        }
        if (!empty($mobile_no)) {
            $builder->where('mobile_no', 'like', '%' . $mobile_no . '%');
        }

        if (!empty($city_name)) {
            $builder->where('city_name', 'like', '%' . $city_name . '%');
        }
        if (!empty($is_active)) {
            $builder->where('is_active', '=', $is_active);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function search(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $search = $request->input('search');
        $builder = DB::table('tbl_transport')->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);


        $builder->whereRaw(" (name like '%" . $search . "%'  or email like '%" . $search . "%' or mobile_no like '%" .
                $search . "%' or city_name like '%" . $search . "%'   ) and is_active = 1");

        $builder->orderBy('id', 'desc');

        //$resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {
            $collection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $collection;
        return ($resVal);
    }

}
