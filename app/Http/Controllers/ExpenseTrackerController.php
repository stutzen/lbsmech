<?php

namespace App\Http\Controllers;

use DB;
use App\ExpenseTracker;
use App\Attachment;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;
use DateTime;
use App\Helper\GeneralHelper;
use Carbon\Carbon;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserScreenAccess
 *
 * @author Deepa
 */
class ExpenseTrackerController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Expense Tracker Saved Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $error = array();
        $currentuser = Auth::user();

        $expenselist = $request->all();

        foreach ($expenselist as $std) {

            $alowance_id = $std['allowance_id'];
            $role_id = $currentuser->role_id;
            $amount = $std['amount'];


            $builder = DB::table('tbl_allowance_mapping as am')
                    ->Join('tbl_allowance_master as a', 'am.allowance_id', '=', 'a.id')
                    ->select('am.*', 'a.name')
                    ->where('am.allowance_id', '=', $alowance_id)
                    ->where('am.role_id', '=', $role_id)
                    ->where('am.is_active', '=', 1)
                    ->get();


            if (count($builder) > 0) {
                $mapping = $builder->first();
                $mapping_amount = $mapping->amount;
                $tot_track_amount = GeneralHelper::getExpenseTrackDetail($mapping, $alowance_id, $amount, $std['date']);

                if ($tot_track_amount <= $mapping_amount) {
                    $expense = new ExpenseTracker;

                    $expense->amount = $std['amount'];
                    $expense->comments = $std['comments'];
                    $expense->allowance_id = $std['allowance_id'];
                    $expense->date = $std['date'];
                    $expense->role_id = $currentuser->role_id;
                    $expense->created_by = $currentuser->id;
                    $expense->is_active = 1;
                    $expense->updated_by = $currentuser->id;
                    $expense->user_id = $currentuser->id;
                    $expense->status = 'initiated';
                    $expense->save();

                    $attachmentcol = $std['attachment'];
                    foreach ($attachmentcol as $attachment) {
                        $attach = new Attachment;
                        $attach->type = "expense_tracker";
                        $attach->ref_id = $expense->id;
                        $attach->url = $attachment['url'];
                        $attach->created_by = $currentuser->id;
                        $attach->updated_by = $currentuser->id;
                        $attach->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $attach->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                        $attach->is_active = "1";
                        $attach->comments = "Expense Tracker";
                        $attach->save();
                    }
                } else {
                    array_push($error, 'Your Allowance # ' . $alowance_id . " is Not Saved");
                }
            }else{
                $resVal['message']="Expense Tracker not saved because allowance is not mapped";
                $resVal['success']=False;
            }
        }
        if (count($error) > 0)
            $resVal['message'] = $error;
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Expense Tracker Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $error = array();
        $currentuser = Auth::user();

        try {
            $expense = ExpenseTracker::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Expense Tracker Not found';
            return $resVal;
        }
        $alowance_id = $request->input('allowance_id', '');
        ;
        $expense->role_id = $currentuser->role_id;
        $role_id=$expense->role_id;

        $amount = $request->input('amount', '');
        $date = $request->input('date', '');


        $builder = DB::table('tbl_allowance_mapping as am')
                ->Join('tbl_allowance_master as a', 'am.allowance_id', '=', 'a.id')
                ->select('am.*', 'a.name')
                ->where('am.allowance_id', '=', $alowance_id)
                ->where('am.role_id', '=', $role_id)
                ->where('am.is_active', '=', 1)
                ->get();


        $builder1 = DB::table('tbl_attachment')->where('ref_id', $id)
                ->where('type', '=', 'expense_tracker')
                ->delete();

        if (count($builder) > 0) {

            $mapping = $builder->first();
            $mapping_amount = $mapping->amount;

            $tot_track_amount = GeneralHelper::getExpenseTrackDetail($mapping, $alowance_id, $amount, $date);

            if ($tot_track_amount <= $mapping_amount) {

                //$expense->fill($request->all());

                $expense->amount = $request->input('amount');
                $expense->comments = $request->input('comments');
                $expense->allowance_id = $request->input('allowance_id');



                $expense->created_by = $currentuser->id;
                $expense->is_active = 1;
                $expense->updated_by = $currentuser->id;
                $expense->user_id = $currentuser->id;
                $status = $request->input('status', '');
                if (!empty($status))
                    $expense->status = $status;
                $expense->save();

                $expense->created_by = $currentuser->id;
                $expense->is_active = 1;
                $expense->date = $request->input('date');
                $expense->updated_by = $currentuser->id;
                $expense->user_id = $currentuser->id;
                $expense->save();
                $attachmentcol = $request->input('attachment');
                foreach ($attachmentcol as $attachment) {
                    $attach = new Attachment;
                    $attach->type = "expense_tracker";
                    $attach->ref_id = $expense->id;
                    $attach->url = $attachment['url'];
                    $attach->created_by = $currentuser->id;
                    $attach->updated_by = $currentuser->id;
                    $attach->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                    $attach->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                    $attach->is_active = "1";
                    $attach->comments = "Expense Tracker";
                    $attach->save();
                }
            } else {
                $resVal['message'] = 'Your Allowance # ' . $alowance_id . " is Not Updated";
            }
        }



        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Expense Tracker Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper ::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;

            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $expense = ExpenseTracker::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Expense Tracker Not found';

            return $resVal;
        }

        DB::table('tbl_attachment')->where('ref_id', $id)
                ->where('type', '=', 'expense_tracker')
                ->update(['is_active' => 0]);

        $expense->is_active = 0;
        $expense->updated_by = $currentuser->id;
        $expense->save();



        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper ::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;

            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id', '');
        $roleid = $request->input('role_id', '');
        $allowance = $request->input('allowance_id', '');
        $isactive = $request->input('is_active', '');
        $status = $request->input('status', '');
        $user_id=$request->input('user_id');

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $builder = DB::table('tbl_expense_tracker as am')
                ->Join('tbl_allowance_master as a', 'am.allowance_id', '=', 'a.id')
                ->Join('tbl_role as r', 'am.role_id', '=', 'r.id')
                ->Join('tbl_user as u', 'am.user_id', '=', 'u.id')
                ->select('am.*', 'r.name', 'a.name', 'u.f_name', 'u.l_name');
        
        
        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;

        $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
         if (!$isVisible) {
           $builder->where('am.user_id', '=', $curr_id);
         }
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//             $builder->where('am.user_id', '=', $curr_id);
//        }

        if (!empty($id)) {
            $builder->where('am.id', '=', $id);
        }
         if (!empty($user_id)) {
            $builder->where('u.id', '=', $user_id);
        }
        if ($roleid != '') {
            $builder->where('am.role_id', '=', $roleid);
        }
        if ($status != '') {
            $builder->where('am.status', '=', $status);
        }
        if ($allowance != '') {
            $builder->where('am.allowance_id', '=', $allowance);
        }
        if ($isactive != '') {
            $builder->where('am.is_active', '=', $isactive);
        }

        if (!empty($from_date)) {

            $builder->whereDate('am.date', '>=', $from_date);
        }
        if (!empty($to_date)) {

            $builder->whereDate('am.date', '<=', $to_date);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }


        foreach ($collection as $col) {

            $exp_id = $col->id;
            $attachment = DB::table('tbl_attachment')
                    ->where('is_active', '=', '1')
                    ->where('ref_id', '=', $exp_id)
                    ->where('type', '=', 'expense_tracker')
                    ->get();
            $col->attachment = $attachment;
        }

        $resVal['list'] = $collection;

        return ($resVal);
    }

    public static function StatusUpdate(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Expense Tracker Staus updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper ::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;

            return $resVal;
        }


        $expenselist = $request->all();

        foreach ($expenselist as $std) {

            $id = $std['id'];
            $status = $std['status'];

            $currentuser = Auth::user();
            try {
                $expense = ExpenseTracker::findOrFail($id);
            } catch (ModelNotFoundException $e) {
                
            }
            $expense->status = $status;
            $expense->updated_by = $currentuser->id;
            $expense->save();
        }

        return $resVal;
    }

    public function Report(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $show_all = $request->input('show_all', '');

        $builder = DB::table('tbl_expense_tracker as e')
                ->Join('tbl_allowance_master as a', 'e.allowance_id', '=', 'a.id')
                ->Join('tbl_role as r', 'e.role_id', '=', 'r.id')
                ->select(DB::raw("e.id,SUM(e.amount) as amount,    e.date"))
                ->whereDate('e.date', '>=', $from_date)
                ->whereDate('e.date', '<=', $to_date)
                ->where('e.is_active', '=', 1)
                ->groupBy(DB::raw("e.date"))
                ->get();

        $resVal['success'] = TRUE;

        if ($show_all == 1) {

            foreach ($builder as $tax) {
                $date = $tax->date;
                $builder_detail = DB::table('tbl_expense_tracker as e')
                        ->Join('tbl_allowance_master as a', 'e.allowance_id', '=', 'a.id')
                        ->Join('tbl_user as r', 'e.user_id', '=', 'r.id')
                        ->select(DB::raw(" e.id,SUM(e.amount) as amount,    e.date,e.user_id,r.f_name,l_name"))
                        ->whereDate('e.date', '=', $date)
                        ->where('e.is_active', '=', 1)
                        ->groupBy(DB::raw("e.user_id"))
                ;

                $tax->data = $builder_detail->get();
            }
        }
        $resVal['list'] = $builder;
        return ($resVal);
    }
    
    
     public function userBasedreport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $from_date = $request->input('fromDate');
        $to_date = $request->input('toDate');
        $teamId = $request->input('teamId');
        $userId = $request->input('userId');
        $empId = $request->input('empId');

        $builder = DB::table('tbl_employee as e')
                ->Join('tbl_user as u', 'e.user_id', '=', 'u.id')
                ->select(DB::raw('CONCAT(e.f_name, " ", e.l_name) AS empName'),'e.id as empId','u.id as userId','u.role_id as roleId','u.rolename as roleName')
                ->where('e.is_active', '=', 1)
                 ->where('u.is_active', '=', 1);
        
         if (!empty($teamId)) {
            $builder->where('e.team_id', '=', $teamId);
        }
        
        if (!empty($userId)) {
            $builder->where('e.user_id', '=', $userId);
        }
        
        if (!empty($empId)) {
            $builder->where('e.id', '=', $empId);
        }
        $builder = $builder->get();
        foreach($builder as $b){
            $roleId = $b->roleId;
            $builder1 = DB::table('tbl_expense_tracker as et')
                ->leftJoin('tbl_allowance_master as at', 'at.id', '=', 'et.allowance_id')
                ->leftJoin('tbl_allowance_mapping as am ', function($join) use ($roleId) {
                        $join->on('at.id', '=', 'am.allowance_id');
                        $join->on('am.role_id', '=', DB::raw("'" . $roleId . "'"));
                    })
                //->leftJoin('tbl_allowance_mapping as am', 'at.id', '=', 'am.allowance_id')
                ->select('at.id as allowancedId','at.name as allowanceName','am.type as allowanceType','am.amount as allowedAmount',DB::raw("sum(et.amount) as spentAmount"))
                ->where('et.role_id', '=', $b->roleId)
                ->where('et.user_id', '=', $b->userId)
                ->where('et.is_active', '=', 1)
                 ->groupBy('et.allowance_id')
                ->groupBy('et.user_id');
                    
            if (!empty($from_date)) {
                $builder1->whereDate('et.date', '>=', $from_date);
            }
            if (!empty($to_date)) {
                $builder1->whereDate('et.date', '<=', $to_date);
            }
            // print_r($builder1->toSql());
            $b->detail = $builder1->get();
        }
        
//        $start = $request->input('start', 0);
//        $limit = $request->input('limit', 100);
//        if ($start == 0 && $limit == 0) {
//            $resVal['list'] = $builder->get();
//        } else {
//            $resVal['list'] = $builder->skip($start)->take($limit)->get();
//        }
        $resVal['list'] = $builder;
        return ($resVal);
    }

}

?>
