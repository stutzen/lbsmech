<?php
namespace App\Http\Controllers;
use DB;
use App\Purchaseinvoiceitem;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PurchaseinvoiceitemController
 *
 * @author Deepa
 */
class PurchaseinvoiceitemController extends Controller {
    
    //put your code here
    public function save (Request $request){
       $resVal = array();
        $resVal['message'] = 'Purchase Invoice item Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        
        $purchaseinvoiceitem = new Purchaseinvoiceitem;
       
        $purchaseinvoiceitem->created_by = $currentuser->id;
        $purchaseinvoiceitem->updated_by = $currentuser->id;
        $purchaseinvoiceitem->fill($request->all());
        $purchaseinvoiceitem->save();

        $resVal['id'] = $purchaseinvoiceitem->id;

        return $resVal;
    
    }
    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Perchase Invoice Item Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        try {
            $purchaseinvoiceitem = Purchaseinvoiceitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Perchase invoice Item Not found';
            return $resVal;
        }
        $purchaseinvoiceitem->created_by = $currentuser->id;
        $purchaseinvoiceitem->fill($request->all());
        $purchaseinvoiceitem->save();

        return $resVal;
    }
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
       
        $isactive= $request->input('is_active','');
       
        $builder = DB::table('tbl_purchase_invoice_item')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
         
      
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }
        
        $builder->orderBy('id','desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }
    public function delete($id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Invoice Item Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $purchaseinvoiceitem = Purchaseinvoiceitem::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Item  Not found';
            return $resVal;
        }

        $purchaseinvoiceitem->delete();

        return $resVal;
    }
}

?>
