<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\LogHelper;
use App\Helper\NotificationHelper;
use App\Helper\AuthorizationHelper;
use App\Listeners\NotificationListener;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentController
 *
 * @author Deepa
 */
class NotificationController extends Controller {

    //put your code here
    public function notificationSend(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $device_id = $request->input('device_id', '');
        $user_id = $request->input('user_id', '');

        $response = NotificationListener::sendNotification($user_id,$device_id,'','');
        $resVal['response'] = $response;
        return $resVal;
    }

}

?>