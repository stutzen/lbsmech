<?php

namespace App\Http\Controllers;

use DB;
use App\EmployeeAdvance;
use App\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Transaction;
use App\Helper\GeneralHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\AccountHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class EmployeeAdvanceController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Employee Advance Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $account_id = $request->input('account_id');
        $account_name = DB::table('tbl_accounts')
                        ->select('account')->where('id', '=', $account_id)->first();

        $account_type = $account_name->account;

        $emp_adv = new EmployeeAdvance;

        AccountHelper::addTransaction($credit = 0, $request->input('adv_amount'), $request->input('account_id'));



        $emp_adv->created_by = $currentuser->id;
        $emp_adv->updated_by = $currentuser->id;
        $emp_adv->fill($request->all());
        $emp_adv->save();

        $transaction = new Transaction;
        $mydate = date('Y-m-d');
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

        $transaction->transaction_date = $mydate;
        $transaction->voucher_type = Transaction::$ADVANCE_AMOUNT;
        $transaction->voucher_number = $emp_adv->id;
        $transaction->credit = 0.00;
        $transaction->debit = $emp_adv->adv_amount;
        $transaction->account_category = '';

        $transaction->acode = '';
        $transaction->acoderef = $emp_adv->team_id;
        $transaction->account = $account_type;
        $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
        $transaction->pmtcoderef = $request->input('account_id', '');
        $transaction->is_cash_flow = 0;


        $transaction->payment_mode = '';

        $transaction->particulars = '#' . $emp_adv->id . 'Employee Advance amount for team_id : ' . $emp_adv->team_id . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

        $transaction->save();

        //Activity log
        ActivityLogHelper::AdvancePaymentSave($emp_adv);

        $resVal['id'] = $emp_adv->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $emp_code = $request->input('emp_code', '');
        $is_active = $request->input('is_active', '');
        $date = $request->input('date');
        $name = $request->input('name', '');
        $type = $request->input('type', '');
        $emp_id = $request->input('emp_id');



        //$status = $request->input('status', '');
        $builder = DB::table('tbl_employee_advance')
                ->select('*');

        $builder = DB::table('tbl_employee_advance as ad')
                ->leftjoin('tbl_employee as ep', 'ep.id', '=', 'ad.emp_id')
                ->leftjoin('tbl_accounts as a', 'a.id', '=', 'ad.account_id')
                ->select('ad.*', 'ep.f_name', 'ep.l_name', 'a.account as accountName');


        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('ad.id', '=', $id);
        }
        if (!empty($emp_id)) {
            $builder->where('ep.id', '=', $emp_id);
        }
        if (!empty($date)) {
            $builder->where('ad.date', '=', $date);
        }
        if ($is_active != '') {
            $builder->where('ad.is_active', '=', $is_active);
        }


        if (!empty($emp_code)) {
            $builder->where('ad.emp_code', 'like', '%' . $emp_code . '%');
        }
        if (!empty($name)) {
            $builder->where('ep.f_name', 'like', '%' . $name . '%');
        }
        if ($type == 1) {
            $builder->where('ad.balance_amt', '>', 0);
        }

        $builder->orderBy('ad.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Employee Advance Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_adv = EmployeeAdvance::findOrFail($id);
            if ($emp_adv->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Advance   Not found';
            return $resVal;
        }
        $oldBalance = $emp_adv->adv_amount;
        $newBalance = $request->input('adv_amount');
        $bal = $oldBalance - $newBalance;

        AccountHelper::addTransaction($oldBalance, $debit = 0, $request->input('account_id'));
        AccountHelper::addTransaction($credit = 0, $newBalance, $request->input('account_id'));

        $emp_adv->updated_by = $currentuser->id;
        $emp_adv->fill($request->all());
        $emp_adv->save();

        //Activity log
        ActivityLogHelper::AdvancePaymentUpdate($emp_adv);

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Advance  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');

        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_adv = EmployeeAdvance ::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Advance     Not found';
            return $resVal;
        }
        AccountHelper::addTransaction($emp_adv->adv_amount, $debit = 0, $emp_adv->account_id);

        $emp_adv->updated_by = $currentuser->id;
        $emp_adv->is_active = 0;

        $emp_adv->update();

        $transactionCollection = Transaction::where('voucher_number', "=", $emp_adv->id)
                ->where('voucher_type', "=", 'advance_amount')
                ->get();
        $transaction = $transactionCollection->first();


        $transaction->is_active = 0;
        $transaction->update();

        //Activity log
        ActivityLogHelper::AdvancePaymentDelete($emp_adv);

        $resVal['id'] = $emp_adv->id;
        return $resVal;
    }

    public function EmployeeAdvanceReport(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $team_id = $request->input('team_id');
        $emp_id = $request->input('emp_id');
        $show_all = $request->input('show_all');

        $builder = DB::table('tbl_employee_advance as ea')
                ->leftjoin('tbl_employee as e', 'ea.emp_id', '=', 'e.id')
                ->leftjoin('tbl_emp_team as et', 'et.id', '=', 'e.team_id')
                ->select('ea.emp_id', 'e.f_name as employee_name', 'e.emp_code', 'e.team_id', 'et.name as team_name', DB::raw('sum(coalesce(ea.adv_amount,0)) as advance_amount'), DB::raw('sum(coalesce(ea.total_deduction_amt,0)) as deduction_amount'), 'ea.balance_amt as balance_amount')
                ->where('ea.is_active', '=', 1)
                ->groupby('ea.emp_id');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($from_date)) {
            $builder->whereDate('ea.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('ea.date', '<=', $to_date);
        }
        if (!empty($team_id)) {
            $builder->where('e.team_id', '=', $team_id);
        }
        if (!empty($emp_id)) {
            $builder->where('ea.emp_id', '=', $emp_id);
        }

        $builder->orderBy('ea.emp_id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }
        if ($show_all == 1) {
            foreach ($collection as $detail) {
                $emp_id = $detail->emp_id;

                $deduction = DB::table('tbl_emp_adv_deduction as ead')
                        ->leftjoin('tbl_employee as e', 'ead.emp_id', '=', 'e.id')
                        ->select('ead.date', DB::raw("0 as advance_amount"), DB::raw('coalesce(ead.deduction_amt,0) as deduction_amount'))
                        ->where('ead.is_active', '=', 1)
                        ->where('ead.emp_id', '=', $emp_id);

                if (!empty($from_date)) {
                    $deduction->whereDate('ead.date', '>=', $from_date);
                }
                if (!empty($to_date)) {
                    $deduction->whereDate('ead.date', '<=', $to_date);
                }

                $advance = DB::table('tbl_employee_advance as ea')
                        ->leftjoin('tbl_employee as e', 'ea.emp_id', '=', 'e.id')
                        ->select('ea.date', DB::raw('coalesce(ea.adv_amount,0) as advance_amount'), DB::raw("0 as deduction_amount"))
                        ->where('ea.is_active', '=', 1)
                        ->union($deduction)
                        ->where('ea.emp_id', '=', $emp_id);

                if (!empty($from_date)) {
                    $advance->whereDate('ea.date', '>=', $from_date);
                }
                if (!empty($to_date)) {
                    $advance->whereDate('ea.date', '<=', $to_date);
                }

                $detail->advance_detail = $advance->get();
            }
        }

        $resVal['list'] = $collection;
        return ($resVal);
    }

    public function EmployeeBalanceReport(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $team_id = $request->input('team_id');
        $emp_id = $request->input('emp_id');

        $advance_amount = DB::table('tbl_employee_advance')
                ->whereDate('date', '<', $from_date)
                ->where('emp_id', '=', $emp_id)
                ->where('is_active', '=', 1)
                ->sum('adv_amount');


        $deduction_amount = DB::table('tbl_emp_adv_deduction')
                ->whereDate('date', '<', $from_date)
                ->where('emp_id', '=', $emp_id)
                ->where('is_active', '=', 1)
                ->sum('deduction_amt');

        $advance = $advance_amount - $deduction_amount;

        $advance_list = array();
        $advance_list['date'] = date('Y-m-d', strtotime($from_date . '-1 day'));
        $advance_list['description  '] = 'Opening Balance';
        $advance_list['advance_amount'] = $advance;
        $advance_list['deduction_amount'] = 0.00;
        $resVal['openBalance'] = $advance_list;

        $builder1 = DB::table('tbl_employee_advance as ea')
                ->leftjoin('tbl_employee as e', 'ea.emp_id', '=', 'e.id')
                ->leftjoin('tbl_emp_team as et', 'e.team_id', '=', 'et.id')
                ->select(DB::raw('0 as  id'), 'ea.emp_id', 'e.f_name as emp_name', 'e.emp_code', 'e.team_id', 'et.name as team_name', 'ea.date', DB::raw('ea.adv_amount as advance_amount'), DB::raw('ea.adv_amount*0 as deduction_amount'))
                ->where('ea.emp_id', '=', $emp_id)
                ->where('ea.date', '>=', $from_date)
                ->where('ea.date', '<=', $to_date)
                //  ->orderBy('ea.date', 'asc')
                //   ->orderBy('ea.id', 'asc')
                ->where('ea.is_active', '=', 1);

        $builder = DB::table('tbl_emp_adv_deduction as ead')
                ->leftjoin('tbl_employee as e', 'ead.emp_id', '=', 'e.id')
                ->leftjoin('tbl_emp_team as et', 'e.team_id', '=', 'et.id')
                ->select('ead.id', 'ead.emp_id', 'e.f_name as emp_name', 'e.emp_code', 'e.team_id', 'et.name as team_name', 'ead.date', DB::raw('ead.deduction_amt*0 as advance_amount'), DB::raw('ead.deduction_amt as deduction_amount'))
                ->where('ead.emp_id', '=', $emp_id)
                ->where('ead.date', '>=', $from_date)
                ->where('ead.date', '<=', $to_date)
                ->where('ead.is_active', '=', 1)
                // ->orderBy('ead.id', 'asc')
                ->union($builder1);
        $builder->orderBy('date', 'asc');

        $resVal['list'] = $builder->get();

        return ($resVal);
    }

}

?>
