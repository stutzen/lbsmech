<?php

namespace App\Http\Controllers;

use DB;
use App\Quote;
use App\Quotesitem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Transaction;
use App\Helper\AccountHelper;
use App\Payment;
use App\DealActivity;
use App\Helper\ActivityLogHelper;
use Carbon\Carbon;
use App\Helper\GeneralHelper;
use App\Helper\MailHelper;
use App\Helper\PdfHelper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuotesController
 *
 * @author Deepa
 */
class QuoteController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Quotes Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax(0, $taxId, $taxable_amount, 'sales_quote');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);

        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));

        $currentuser = Auth::user();
        $quotes = new Quote;
        $quotes->created_by = $currentuser->id;
        $quotes->updated_by = $currentuser->id;
        $quotes->is_active = $request->input('is_active', 1);
        $quotes->fill($request->all());
        $quotes->subtotal = $subtotal;
        $quotes->tax_amount = $tax_amount;
        $quotes->discount_amount = $discount_amount;
        $quotes->round_off = $roundoff;
        $quotes->total_amount = $total;

        $quotes->status = $request->input('status', 'new');
        $quotes->save();
        $quoteItemCol = ($request->input('item'));

        foreach ($quoteItemCol as $item) {

            $quoteitems = new Quotesitem;
            $quoteitems->fill($item);
            $quoteitems->quote_id = $quotes->id;
            $quoteitems->is_active = $request->input('is_active', 1);
            $quoteitems->contact_id = $request->input('contact_id');
            $quoteitems->duedate = $quotes->duedate;
            $quoteitems->paymentmethod = $quotes->paymentmethod;
            $quoteitems->tax_amount = $quotes->tax_amount;
            $quoteitems->created_by = $currentuser->id;
            $quoteitems->updated_by = $currentuser->id;
            $quoteitems->save();
        }

        $resVal['id'] = $quotes->id;
        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;

            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'estimate';
            $deal->description = "created an estimate";
            $deal->contact_id = $request->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->deal_id = $deal_id;
            $deal->ref_id = $quotes->id;
            $deal->comments = "created an estimate";

            $deal->save();
        }
        GeneralHelper::InvoiceTaxSave($quotes->id, $request->input('tax_id'), $taxable_amount, $subtotal, 'sales_quote');
        
       PdfHelper::quotePdf($quotes->id);
       PdfHelper::AttachImage($quotes->id);
       $imgs  = PdfHelper::AttachImage($quotes->id);
         $mail_obj = new MailHelper;
       $mail_obj->quoteCreatedNotificationMail($quotes,$imgs);
        ActivityLogHelper::quoteSave($quotes);
        return $resVal;
    }

    public function sendEmail($id) {
        $quotes = Quote::findOrFail($id);
        PdfHelper::quotePdf($id);
        $imgs  = PdfHelper::AttachImage($id);
        $mail_obj = new MailHelper;  
        $mail_obj->quoteCreatedNotificationMail($quotes,$imgs);
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $customerid = $request->input('contactid');
        $fromValidDate = $request->input('from_validdate', '');
        $toValidDate = $request->input('to_validdate', '');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $deal_id = $request->input('deal_id', '');
        $status = $request->input('status');
        $quoteDate=$request->input('quote_date','');
        $validDate=$request->input('valid_date','');
        $assigned_to=$request->input('assigned_to');
         $company_id=$request->input('company_id');


        $isactive = $request->input('is_active', '');
        //$builder = DB::table('tbl_quote')->select('*');
        $builder = DB::table('tbl_quote as q')
                ->leftjoin('tbl_employee as emp','q.assigned_to','=','emp.id')
                ->leftJoin('tbl_contact as c', 'q.contact_id', '=', 'c.id')
                ->leftJoin('tbl_customer as d', 'q.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'q.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
                ->leftjoin('tbl_user as u','q.created_by','=','u.id')
                ->leftjoin('tbl_company as com','com.id','=','q.company_id')
                ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone','com.name as company_name'
                        , 'c.email', 'd.fname as payee_name', 'e.fname as consignee_name','emp.f_name as assigned_to_name','u.f_name as created_by_name','d.city','d.billing_city','d.shopping_city');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }

        if ($isactive != '') {
            $builder->where('q.is_active', '=', $isactive);
        }
        if (!empty($assigned_to)) {
            $builder->where('q.assigned_to', '=', $assigned_to);
        }
         if (!empty($company_id)) {
            $builder->where('q.company_id', '=', $company_id);
        }
        if (!empty($fromValidDate)) {

            $builder->whereDate('q.validuntil', '>=', $fromValidDate);
        }
        if (!empty($toValidDate)) {

            $builder->whereDate('q.validuntil', '<=', $toValidDate);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('q.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('q.date', '<=', $toDate);
        }
		if (!empty($quoteDate)) {

            $builder->whereDate('q.date', '=', $quoteDate);
        }
        if (!empty($validDate)) {

            $builder->whereDate('q.validuntil', '=', $validDate);
        }
        if (!empty($status)) {
            $builder->where('q.status', '=', $status);
        }
        if (!empty($customerid)) {
            $builder->where('q.contact_id', '=', $customerid);
        }
        if ($deal_id != '') {
            $builder->where('q.deal_id', '=', $deal_id);
        }

        $builder->orderBy('q.id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Quotes Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $quotes = Quote::findOrFail($id);

            if ($quotes->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Quotes Not found';
            return $resVal;
        }
        $quotes->created_by = $currentuser->id;
        $quotes->updated_by = $currentuser->id;
        $quotes->is_active = 0;
        //$quotes->fill($request->all());
        $quotes->update();


        /*
         * Code to adjust bank money and transaction table entry 
         */
        $paymentCollection = Payment::where('invoice_id', '=', 0)
                        ->where('quote_id', '=', $id)
                        ->where('is_active', '=', 1)->get();
        foreach ($paymentCollection as $payment) {

            AccountHelper::withDraw($payment->account_id, $payment->amount);

            //deavtivate the transaction
            DB::table('tbl_transaction')
                    ->where('voucher_type', '=', Transaction::$SALES_PAYMENT)
                    ->where('voucher_number', '=', $payment->id)->update(['is_active' => 0]);
        }

        $deal_id = $quotes->deal_id;
        if ($deal_id != 0) {
            $deal = new DealActivity;

            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'estimate';
            $deal->description = "delete an estimate";
            $deal->contact_id = $request->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->deal_id = $deal_id;
            $deal->ref_id = $quotes->id;
            $deal->comments = "delete an estimate";

            $deal->save();
        }


        $resVal['id'] = $quotes->id;
        GeneralHelper::InvoiceTaxDelete($id, 'sales_quote');

        ActivityLogHelper::quoteDelete($quotes);
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Quotes Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $quotes = Quote::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Quotes Not found';
            return $resVal;
        }

        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax($id, $taxId, $taxable_amount, 'sales_quote');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }

        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);

        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        ;
        $total = floatval(number_format($total, 2, '.', ''));

        $builder = DB::table('tbl_quote_item')->where('quote_id', $id)->delete();
        $quoteItemCol = ($request->input('item'));
        foreach ($quoteItemCol as $item) {

            $quoteitems = new Quotesitem;
            $quoteitems->fill($item);
            $quoteitems->quote_id = $quotes->id;
            $quoteitems->is_active = $request->input('is_active', 1);
            $quoteitems->contact_id = $request->input('contact_id');
            $quoteitems->duedate = $quotes->duedate;
            $quoteitems->paymentmethod = $quotes->paymentmethod;
            $quoteitems->tax_amount = $quotes->tax_amount;
            $quoteitems->created_by = $currentuser->id;
            $quoteitems->updated_by = $currentuser->id;
            $quoteitems->save();
        }
        $quotes->fill($request->all());
        $quotes->subtotal = $subtotal;
        $quotes->tax_amount = $tax_amount;
        $quotes->discount_amount = $discount_amount;
        $quotes->round_off = $roundoff;
        $quotes->total_amount = $total;
        $quotes->created_by = $currentuser->id;
        $quotes->updated_by = $currentuser->id;
        $quotes->save();

        $totalamount = $request->input('total_amount');

        $payment = DB::table('tbl_payment')->where('quote_id', '=', $id)->where('is_active', '=', 1)->sum('amount');

        if ($payment == 0) {
            $quotes->status = 'unpaid';
        } else if ($totalamount <= $payment) {

            $quotes->status = 'paid';
        } else if ($totalamount >= $payment) {

            $quotes->status = 'partiallyPaid';
        }
        $quotes->status=$request->input('status');
        $quotes->advance_amount = $payment;
        $quotes->save();

        $resVal['id'] = $quotes->id;

        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'estimate';
            $deal->description = "updated an estimate";
            $deal->contact_id = $request->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->ref_id = $quotes->id;
            $deal->deal_id = $deal_id;
            $deal->comments = "updated an estimate";

            $deal->save();
        }
        GeneralHelper::InvoiceTaxUpdate($id, $request->input('tax_id'), $taxable_amount, $subtotal, 'sales_quote');

        ActivityLogHelper::quoteModify($quotes);
        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');

        $is_active = $request->input('is_active', '');
        //$builder = DB::table('tbl_quotes')->select('*');
        $builder = DB::table('tbl_quote as q')
                 ->leftjoin('tbl_employee as emp','q.assigned_to','=','emp.id')
                ->leftJoin('tbl_contact as c', 'q.contact_id', '=', 'c.id')
                ->leftJoin('tbl_customer as d', 'q.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'q.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
                ->leftjoin('tbl_company as com','com.id','=','q.company_id')
                ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 'com.name as company_name','t.tax_name as taxname','emp.f_name as assigned_to_name', 'c.phone', 'c.email', 'd.fname as payee_name', 'e.fname as consignee_name')
                ->where('q.id', '=', $id);

        $quotestitems = DB::table('tbl_quote_item')->where('quote_id', $id)
                ->where('is_active', '=', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }

        if ($is_active != '') {
            $builder->where('q.is_active', '=', $is_active);
        }
        $builder->orderBy('q.id', 'desc');

        $resVal['total'] = $builder->count();
        $quotestCollection = $builder->skip($start)->take($limit)->get();
        if (count($quotestCollection) == 1) {

            $quotest = $quotestCollection->first();

            $quotest->item = $quotestitems->get();
            $resVal['data'] = $quotest;
        }
        // $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

}

?>