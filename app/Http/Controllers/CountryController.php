<?php
namespace App\Http\Controllers;
use DB;
use Validator;
use App\Country;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
class CountryController extends Controller
{
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Country Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $validator = Validator::make($request->all(), [
                    'code' => 'required|unique:tbl_country,code'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            if (array_key_exists('code', $validator->failed())) {
                $resVal['message'] = 'Country Code is already exist';
            }

            return $resVal;
        }
        $currentuser=Auth::user();
        $country=new Country;      
        $country->updated_by=$currentuser->id;
        $country->created_by=$currentuser->id;
        $country->fill($request->all());
        $country->save();
       
        $resVal['id'] = $country->id;

        return $resVal;
    }
      public function update(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Country Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $country = Country::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Country Not found';
            return $resVal;
        }
        $country->created_by = $currentuser->id;
        $country->updated_by=$currentuser->id;
        $country->fill($request->all());
        $country->save();

        return $resVal;
    }
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $builder = DB::table('tbl_country')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
 
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $countryCollection = $builder->get();
        } else {

            $countryCollection = $builder->skip($start)->take($limit)->get();
        }
                
        $resVal['list'] = $countryCollection;
        return ($resVal);
    }
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Country Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $country = Country::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Country Not found';
            return $resVal;
        }
        $country->delete();
        return $resVal;
    }
}
?>
