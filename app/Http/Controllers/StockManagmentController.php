<?php

//

namespace App\Http\Controllers;

use DateTime;
use DateInterval;
use DatePeriod;
use DB;
use App\ActivityLog;
use App\PurchasePayment;
use Carbon\Carbon;
use App\Invoice;
use App\Invoiceitem;
use App\Inventory;
use App\InventoryAdjustment;
use App\Attributevalue;
use App\Attribute;
use app\Customer;
use Illuminate\Http\Request;
use App\Transformer\InvoiceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class StockManagmentController {

    //put your code here
    public function StockAdjustment(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        GeneralHelper::StockAdjustmentSave($request, 'adjust', '');
        return $resVal;
    }

    public function StockWastage(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        GeneralHelper::StockWastageSave($request, 'adjust', '');



        return $resVal;
    }

    public function InventoryAdjustment_list(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $product_id = $request->input('product_id');
        $type = $request->input('type');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $is_active=$request->input('is_active',1);

        $builder = DB::table('tbl_inventory_adjustment as ia')
                ->leftJoin('tbl_product as p', 'p.id', '=', 'ia.product_id')
                ->where('p.is_active', '=', 1)
                ->where('p.has_inventory', '=', 1)
                ->select('ia.*', 'p.name')
                ->orderBy('ia.id', 'asc');
        if (!empty($id)) {
            $builder->where('ia.id', '=', $id);
        }
        if (!empty($type)) {
            if ($type == 1)
                $builder->where('ia.type', '=', 'stock_adjust');
            else if ($type == 2)
                $builder->where('ia.type', '=', 'stock_wastage');
            else if ($type == 3)
                $builder->where('ia.type', '=', 'invoice_create');
            else if ($type == 4)
                $builder->where('ia.type', '=', 'invoice_update');
            else if ($type == 5)
                $builder->where('ia.type', '=', 'invoice_delete');
        }
        if (!empty($product_id)) {

            $builder->where('ia.product_id', '=', $product_id);
        }
        if (!empty($toDate)) {

            $builder->whereDate('ia.created_at', '<=', $toDate);
        }
        if (!empty($fromDate)) {
            $builder->whereDate('ia.created_at', '>=', $fromDate);
        }
        if($is_active != ''){
            $builder->where('ia.is_active', '=', $is_active);
        }

        if ($start == 0 && $limit == 0) {
            $inventoryCollection = $builder->get();
        } else {

            $inventoryCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $inventoryCollection;

        return ($resVal);
    }

    public function listAll(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $name = $request->input('product_name');
        $type=$request->input('type');
        $category_id=$request->input('category_id');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);


        $cat_array = array();
        $builder = DB::table('tbl_product as p')
        ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
        ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
        ->where('p.is_active', '=', 1)
        ->where('p.has_inventory', '=', 1)
        ->select(DB::raw('p.*,i.quantity, c.name as category_name'))
        ->orderBy('c.name', 'asc')
        ->orderBy('p.name', 'asc');

        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if (!empty($name)) {
            $builder->where('p.name', 'like','%'. $name.'%');
        }
        if($type == 1){
            $builder->whereColumn('p.min_stock_qty','<','i.quantity');
        }elseif($type == 2){
             $builder->whereColumn('p.min_stock_qty','>','i.quantity');
        }
        if(!empty($category_id)){
            $builder->where('c.id','=',$category_id);
        }
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $cat_array = $builder->get();
        } else {

            $cat_array = $builder->skip($start)->take($limit)->get();
        }
    
        $resVal['list'] = $cat_array;
        return $resVal;
    }

    public function MinStocklist(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');

        $builder = DB::table('tbl_product as p')
                ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                ->whereColumn('i.quantity', '<=', 'p.min_stock_qty')
                ->where('p.is_active', '=', 1)
                ->where('p.has_inventory', '=', 1)
                ->select(DB::raw('p.*,i.quantity'));
        ;
        $id = $request->input('id');
        $name = $request->input('product_name');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $productCollection = $builder->get();
        } else {

            $productCollection = $builder->skip($start)->take($limit)->get();
        }
        $resVal['list'] = $productCollection;
        return $resVal;
    }

}

?>