<?php

namespace App\Http\Controllers;

use DB;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\PurchasePayment;
use App\Helper\AccountHelper;
use App\Helper\AuthorizationHelper;
use Carbon\Carbon;
use App\Helper\GeneralHelper;
use App\Helper\ActivityLogHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TransactionController
 *
 * @author Deepa
 */
class TransactionController extends Controller {

//put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Trasanction Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $transaction = new Transaction;

        $mydate = $request->transaction_date;

        $mydate = date(($mydate));
        $month = date("m", strtotime($mydate));
        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();

        $transaction->fiscal_month_code = $month;
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }
        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }


        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $id = $request->input('account_id', '');

        AccountHelper::addTransaction($request->input('credit', 0),$request->input('debit', 0),$id);
//        if (!empty($request->input('credit', 0))) {
//            $amount = $request->input('credit');
//            $account = AccountHelper::deposit($id, $amount);
//            $transaction->voucher_type = Transaction::$DIRECT_INCOME;
//        }
//        if (!empty($request->input('debit', 0))) {
//            $amount = $request->input('debit');
//            $account = AccountHelper::withDraw($id, $amount);
//            $transaction->voucher_type = Transaction::$DIRECT_EXPENSE;
//        }
        if (!empty($request->input('customer_id', ''))) {
            $transaction->acode = GeneralHelper::getCustomerAcode($request->input('customer_id'));
            $transaction->acoderef = $request->input('customer_id');
        }
        if (!empty($id)) {
            $transaction->pmtcode = GeneralHelper::getPaymentAcode($id);
            $transaction->pmtcoderef = $id;
        }

        $transaction->is_cash_flow = 1;


        $transaction->voucher_number = 0;
        $transaction->is_active = 1;
        $transaction->fill($request->all());
        $transaction->save();
//         
        $resVal['id'] = $transaction->id;



        return $resVal;
    }

    public function acodestatement_csvexport(Request $request) {
        $accountstmt = $this->acodeStatement($request);
        $bal = 0;
        $openblnc = $accountstmt['openBalance'];
        $output = fopen('acodestatement.csv', 'w');
        fputcsv($output, array('Transaction_date', 'Narration', 'Debit', 'Credit', 'Balance',));
        $bal = $openblnc['credit'] - $openblnc['debit'];
        fputcsv($output, array('', $openblnc['description  '], '', '', $bal));
        foreach ($accountstmt['list'] as $values) {
            $bal = $bal + $values->credit;
            $bal = $bal - $values->debit;
            fputcsv($output, array($values->transaction_date, $values->particulars, $values->debit, $values->credit, $bal));
        }
        return response()->download(getcwd() . "/acodestatement.csv");
    }
    
    public function accountstatement_csvexport(Request $request) {
        $accountstmt = $this->accountStatement($request);
        $bal = 0;
        $openblnc = $accountstmt['openBalance'];
        $output = fopen('accountstatement.csv', 'w');
        fputcsv($output, array('Transaction_date', 'Narration', 'Debit', 'Credit', 'Balance',));
        $bal = $openblnc['credit'] - $openblnc['debit'];
        fputcsv($output, array('', $openblnc['description  '], $openblnc['debit'], $openblnc['credit'], $bal));
        foreach ($accountstmt['list'] as $values) {
            $bal = $bal + $values->credit;
            $bal = $bal - $values->debit;
            fputcsv($output, array($values->transaction_date, $values->particulars, $values->debit, $values->credit, $bal));
        }
        return response()->download(getcwd() . "/accountstatement.csv");
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');


//$status = $request->input('status', '');
        $builder = DB::table('tbl_transaction')
                ->select('*')
                ->where('is_active','=',1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }


        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function listAllByDate(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $cus_id = $request->input('cus_id');
        $acc_id = $request->input('acc_id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $created_by = $request->input('created_by');
        $created_by_name = $request->input('created_by_name');
        $acc_cat = $request->input('acc_cat');
        $name = $request->input('created_by_name', '');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');

//$status = $request->input('status', '');
        $builder = DB::table('tbl_transaction')
                ->select('*');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($cus_id)) {
            $builder->where('customer_id', '=', $cus_id);
        }
        if (!empty($acc_id)) {
            $builder->where('account_id', '=', $acc_id);
        }
        if (!empty($created_by)) {
            $builder->where('created_by', '=', $created_by);
        }
        if (!empty($acc_cat)) {
            $builder->where('account_category', '=', $acc_cat);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('transaction_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('transaction_date', '<=', $toDate);
        }
        if (!empty($name)) {
            $builder->where('created_by_name', 'like', '%' . $name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transaction Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $transaction = Transaction::findOrFail($id);
            if ($transaction->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transaction Not found';
            return $resVal;
        }

        $transaction->updated_by = $currentuser->id;
        $transaction->is_active = 0;

        return $resVal;
    }

    public function listAllByIncome(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');

        $fromDate = $request->input('from_Date', '');

        $toDate = $request->input('to_Date', '');
        $is_active = $request->input('is_active', 1);
        $acc_cat = $request->input('account_category', '');
        $created_by = $request->input('created_by');
        $name = $request->input('created_by_name', '');

        $builder = DB::table('tbl_transaction')
                ->select('id', 'name', 'fiscal_year_id', 'fiscal_year_code', 'transaction_date', 'voucher_type', 'voucher_number', 'credit As amount', 'account', 'acode', 'acoderef', 'pmtcode', 'pmtcoderef', 'particulars', 'created_by', 'updated_by', 'updated_at', 'created_at', 'created_by_name', 'updated_by_name', 'account_category', 'payment_mode', 'fiscal_month_code', 'is_active')
                ->where('voucher_type', '!=', 'transfer')
                ->where('voucher_type', '!=', 'inital_balance')
                ->where('credit', '!=', 0)
                ->where('is_cash_flow', '=', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($cus_id)) {
            $builder->where('customer_id', '=', $cus_id);
        }
        if (!empty($acc_id)) {
            $builder->where('account_id', '=', $acc_id);
        }
        if (!empty($acc_cat)) {
            $builder->where('account_category', '=', $acc_cat);
        }
        if (!empty($is_active)) {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($created_by)) {
            $builder->where('created_by', '=', $created_by);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('transaction_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('transaction_date', '<=', $toDate);
        }
        if (!empty($name)) {
            $builder->where('created_by_name', 'like', '%' . $name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function ExpenseReport_csvexport(Request $request) {
        $listAllBYExpenseLists = $this->listAllBYExpanse($request);

        $output = fopen('ExpenseReport.csv', 'w');
        fputcsv($output, array('Date', 'Particular', 'Category', 'Created_by', 'Amount'));
        foreach ($listAllBYExpenseLists['list'] as $values) {
            fputcsv($output, array($values->created_at, $values->particulars, $values->account_category, $values->created_by_name, $values->amount));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        return response()->download(getcwd() . "/ExpenseReport.csv");
    }

    public function listAllBYExpanse(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');

        $is_active = $request->input('is_active', 1);

        $fromDate = $request->input('from_Date', '');
        $toDate = $request->input('to_Date', '');
        $acc_cat1 = $request->input('account_category', '');
        $created_by = $request->input('created_by');
        $name = $request->input('created_by_name', '');
        $mode = $request->input('mode', '');

        $builder = DB::table('tbl_transaction')
                ->select('id', 'name', 'fiscal_year_id', 'fiscal_year_code', 'transaction_date', 'voucher_type', 'voucher_number', 'debit As amount', 'account', 'acode', 'acoderef', 'pmtcode', 'pmtcoderef', 'particulars', 'created_by', 'updated_by', 'updated_at', 'created_at', 'created_by_name', 'updated_by_name', 'account_category', 'payment_mode', 'fiscal_month_code', 'is_active')
                ->where('voucher_type', '!=', 'transfer')
                ->where('voucher_type', '!=', 'inital_balance')
                ->where('debit', '!=', 0)
                ->where('is_cash_flow', '=', 1);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }


        if (!empty($acc_cat1)) {
            $builder->where('account_category', '=', $acc_cat1);
        }
        if (!empty($created_by)) {
            $builder->where('created_by', '=', $created_by);
        }
        if (!empty($is_active)) {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('transaction_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('transaction_date', '<=', $toDate);
        }
        if (!empty($name)) {
            $builder->where('created_by_name', 'like', '%' . $name . '%');
        }

        if ($mode == 1) {
            $builder->orderBy('transaction_date', 'asc');
        } else {
            $builder->orderBy('id', 'desc');
        }


        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function transactionIncomeDelete(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transaction Income Deleted Successfully';
        $currentuser = Auth::user();
        try {

            $transaction = Transaction::findOrFail($id);

            if ($transaction->is_active == 0) {
                return $resVal;
            }


            $transaction->updated_by = $currentuser->id;
            $transaction->is_active = 0;

            $account_id = $transaction->pmtcoderef;


            if (!empty($transaction->credit)) {

                AccountHelper::withDraw($account_id, $transaction->credit);
            }

            $transaction->update();
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transaction Income Not found';
            return $resVal;
        }



        return $resVal;
    }

    public function transactionExpenseDelete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transaction Expense Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {

            $transaction = Transaction::findOrFail($id);
            if ($transaction->is_active == 0) {
                return $resVal;
            }


            $transaction->updated_by = $currentuser->id;
            $transaction->is_active = 0;

            $account_id = $transaction->pmtcoderef;


            if (!empty($transaction->debit)) {

                AccountHelper::deposit($account_id, $transaction->debit);
            }

            $transaction->update();
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transaction Expense Not found';
            return $resVal;
        }


        return $resVal;
    }

    public function accountStatement(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
//Get Current and Previous Date
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $account_id = $request->input('account_id');
        $from_Date = $request->input('from_Date');
        $to_Date = $request->input('to_Date');

        $amountBuilder = DB::table('tbl_transaction')
                ->select(DB::raw('id'))
                ->whereDate('transaction_date', '<', $from_Date)
                ->where('is_cash_flow', '=', 1)
                ->where('is_active', '=', 1);
        if (!empty($account_id)) {
            $amountBuilder->where('pmtcoderef', '=', $account_id);
        }
        $amount = $amountBuilder->sum('credit');

        $debitamountBuilder = DB::table('tbl_transaction')
                ->select(DB::raw('id'))
                ->where('transaction_date', '<', $from_Date)
                ->where('is_cash_flow', '=', 1)
                ->where('is_active', '=', 1);
        if (!empty($account_id)) {
            $debitamountBuilder->where('pmtcoderef', '=', $account_id);
        }
        $debitamount = $debitamountBuilder->sum('debit');
        $account_list = array();
// $account_list['date'] = date('d-m-Y', strtotime($from_Date.'-1 day'));   
        $account_list['description  '] = 'Opening Balance';
        $account_list['credit'] = $amount;
        $account_list['debit'] = $debitamount;



        $resVal['openBalance'] = $account_list;


        $builder = DB::table('tbl_transaction')
                ->select(DB::raw('*'))
                ->whereDate('transaction_date', '>=', $from_Date)
                ->whereDate('transaction_date', '<=', $to_Date)
                ->where('is_active', '=', 1)
                ->where('is_cash_flow', '=', 1)
                ->orderBy('transaction_date');

        if (!empty($account_id)) {
            $builder->where('pmtcoderef', '=', $account_id);
        }
        $resVal['list'] = $builder->get();
//        
//         $builder = DB::table('tbl_payment')     
//                    ->select( DB::raw('id'),
//                            DB::raw('"Payment" as type'),
//                            DB::raw('date'),
//                            DB::raw('CONCAT("Sales Invoice1", " ", invoice_id) AS description'),
//                            DB::raw('amount*0 as debit'),
//                            DB::raw('amount as credit'))
//                     ->where('customer_id', '=', $id)
//                     ->where('date','>=',$from_date)
//                     ->where('date','<=',$to_date)    
//                     ->union($builder1)
//                    ->orderBy('date','asc')
//                    ->get();           
//         $resVal['list'] = $builder;

        return ($resVal);
    }

    public function acodeStatement(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
//Get Current and Previous Date
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $acode = $request->input('acode');
        $from_Date = $request->input('from_Date');
        $to_Date = $request->input('to_Date');

        $amountBuilder = DB::table('tbl_transaction')
                ->select(DB::raw('id'))
                ->whereDate('transaction_date', '<', $from_Date)
                ->where('is_active', '=', 1);
        if (!empty($acode)) {
            $amountBuilder->where('acode', '=', $acode);
        }
        $amount = $amountBuilder->sum('credit');

        $debitamountBuilder = DB::table('tbl_transaction')
                ->select(DB::raw('id'))
                ->where('transaction_date', '<', $from_Date)
                ->where('is_active', '=', 1);
        if (!empty($acode)) {
            $debitamountBuilder->where('acode', '=', $acode);
        }
        $debitamount = $debitamountBuilder->sum('debit');
        $account_list = array();
// $account_list['date'] = date('d-m-Y', strtotime($from_Date.'-1 day'));   
        $account_list['description  '] = 'Opening Balance';
        $account_list['credit'] = $amount;
        $account_list['debit'] = $debitamount;



        $resVal['openBalance'] = $account_list;



        $builder = DB::table('tbl_transaction')
                ->select(DB::raw('*'))
                ->whereDate('transaction_date', '>=', $from_Date)
                ->whereDate('transaction_date', '<=', $to_Date)
                ->where('is_active', '=', 1)
                ->orderBy('transaction_date');

        if (!empty($acode)) {
            $builder->where('acode', '=', $acode);
        }
        $resVal['list'] = $builder->get();

        return ($resVal);
    }

    public function transferSave(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Transfer Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $sourse_account = $request->source_account;
        $desig_account = $request->desig_account;
        $amount = $request->amount;
        $mydate = $request->date;
        $comments = $request->comments;
        $currentuser = Auth::user();

        $account = AccountHelper::deposit($desig_account, $amount);
        $account = AccountHelper::withDraw($sourse_account, $amount);
        for ($i = 0; $i <= 1; $i++) {
            $transaction = new Transaction;
            $mydate = date(($mydate));
            $month = date("m", strtotime($mydate));
            $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();

            $transaction->fiscal_month_code = $month;
            if ($fiscalMonthCollection->count() > 0) {
                $fiscalMonth = $fiscalMonthCollection->first();
                $transaction->fiscal_month_code = $fiscalMonth->id;
            }
            $year = date("Y", strtotime($mydate));
            $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

            $transaction->fiscal_year_id = 0;
            $transaction->fiscal_year_code = $year;
            if ($fiscalYearCollection->count() > 0) {
                $fiscalYear = $fiscalYearCollection->first();
                $transaction->fiscal_year_id = $fiscalYear->id;
                $transaction->fiscal_year_code = $fiscalYear->code;
            }
            $transaction->transaction_date = $mydate;
            $transaction->created_by = $currentuser->id;
            $transaction->updated_by = $currentuser->id;
            $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

            $transaction->voucher_type = "transfer";
            if ($i == 0) {
                $transaction->voucher_number = $desig_account;
                $transaction->pmtcode = GeneralHelper::getPaymentAcode($desig_account);
                $transaction->pmtcoderef = $desig_account;
            } else {
                $transaction->voucher_number = $tran_id;
                $transaction->pmtcode = GeneralHelper::getPaymentAcode($sourse_account);
                $transaction->pmtcoderef = $sourse_account;
            }
            if ($i == 0)
                $transaction->credit = $amount;
            else
                $transaction->debit = $amount;

            $transaction->is_cash_flow = 1;
            $transaction->is_active = 1;
            $transaction->particulars = $comments;
            $transaction->fill($request->all());
            $transaction->save();
            $resVal['id'] = $transaction->id;
            $tran_id = $transaction->id;
        }
        ActivityLogHelper::transferSave($transaction);
        return $resVal;
    }

    public function transferDelete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Transfer Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $transaction = Transaction::findOrFail($id);
            if ($transaction->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Transaction Not found';
            return $resVal;
        }
        $transaction->updated_by = $currentuser->id;
        $desig_account = $transaction->pmtcoderef;
        $builder = DB::table('tbl_transaction')
                ->select(DB::raw('pmtcoderef'))
                ->where('voucher_number', '=', $id)
                ->where('voucher_type', '=', 'transfer')
                ->first();
        $source_account = $builder->pmtcoderef;

        $amount = $transaction->credit;
        $account = AccountHelper::deposit($source_account, $amount);
        $account = AccountHelper::withDraw($desig_account, $amount);
        $transaction->is_active = 0;
        $transaction->save();
        DB::table('tbl_transaction')->where('voucher_number', '=', $id)->where('voucher_type', '=', 'transfer')
                ->update(['is_active' => 0]);
        ActivityLogHelper::transferDelete($transaction);
        return $resVal;
    }

    public function transferList(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $id = $request->input('id');
        $created_by = $request->input('created_by');
        $builder = DB::table('tbl_transaction as t')
                ->leftJoin('tbl_accounts as c', 't.pmtcoderef', '=', 'c.id')
                ->select('t.*', 'c.account')
                ->where('t.voucher_type', '=', 'transfer')
                ->where('t.credit', '!=', 0)
                ->where('t.is_active', '=', 1)
                ->orderBy('t.id', 'desc');
        if (!empty($id)) {
            $builder->where('t.id', '=', $id);
        }
        if (!empty($created_by)) {
            $builder->where('t.created_by', '=', $created_by);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('t.transaction_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('t.transaction_date', '<=', $toDate);
        }
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return $resVal;
    }

}

?>
