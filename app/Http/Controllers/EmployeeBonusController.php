<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\EmployeeBonus;
use App\EmployeeBonusPreparation;
use App\Employee;
use App\Account;
use App\EmployeeProduction;
use App\DealStage;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;
use App\Transaction;
use App\Helper\GeneralHelper;
use App\Helper\AccountHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class EmployeeBonusController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Employee Bonus Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

//        $from_date=$request->input('from_date');
//        $to_date=$request->input('to_date');       
        $lists = $request->input('list');

        $account_id = $request->input('account_id');

        $account_name = DB::table('tbl_accounts')
                        ->select('account')->where('id', '=', $account_id)->first();

        $account_type = $account_name->account;
        $overall_amount = 0;

        foreach ($lists as $preparation) {
            $from_date = $preparation['from_date'];
            $to_date = $preparation['to_date'];
            $team_id = $preparation['emp_team_id'];
            $amount = $preparation['bonus_amount'];
            $overall_amount+=$amount;
        }

        $emp_bonus_prepare = new EmployeeBonusPreparation;

        $emp_bonus_prepare->from_date = $from_date;
        $emp_bonus_prepare->to_date = $to_date;
        $emp_bonus_prepare->team_id = $team_id;
        $emp_bonus_prepare->overall_bonus_amount = $overall_amount;
        $emp_bonus_prepare->is_active = 1;
        $emp_bonus_prepare->updated_by = $currentuser->id;
        $emp_bonus_prepare->created_by = $currentuser->id;
        $emp_bonus_prepare->save();

        if ($request->input('account_id') != 0) {
            $account = Account::where('id', '=', $request->input('account_id'))->first();
            if ($account != null) {
                $account->balance = $account->balance - $overall_amount;
                $account->updated_by = $currentuser->id;
                $account->save();
            }
        }

        foreach ($lists as $list) {
            $emp_bonus = new EmployeeBonus;

            $emp_bonus->from_date = $list['from_date'];
            $emp_bonus->to_date = $list['to_date'];
            $emp_bonus->emp_id = $list['emp_id'];
            $emp_bonus->emp_code = $list['emp_code'];
            $emp_bonus->emp_name = $list['emp_name'];
            $emp_bonus->emp_team_id = $list['emp_team_id'];
            $emp_bonus->present_count = $list['present_count'];
            $emp_bonus->absent_count = $list['absent_count'];
            $emp_bonus->salary_amount = $list['salary_amount'];
            $emp_bonus->bonus_percentage = $list['bonus_percentage'];
            $emp_bonus->bonus_amount = $list['bonus_amount'];
            $emp_bonus->emp_bonus_prepare_id = $emp_bonus_prepare->id;
            $emp_bonus->is_active = 1;
            $emp_bonus->updated_by = $currentuser->id;
            $emp_bonus->created_by = $currentuser->id;

            $emp_bonus->save();

            //Activity log
            ActivityLogHelper::EmployeeBonusSave($emp_bonus);
        }
        $transaction = new Transaction;
        $mydate = date('Y-m-d');
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

        $transaction->transaction_date = $mydate;
        $transaction->voucher_type = Transaction::$BONUS_AMOUNT;
        $transaction->voucher_number = $emp_bonus_prepare->id;
        $transaction->credit = 0.00;
        $transaction->debit = $emp_bonus_prepare->overall_bonus_amount;
        $transaction->account_category = '';

        $transaction->acode = '';
        $transaction->acoderef = $emp_bonus_prepare->team_id;
        $transaction->account = $account_type;
        $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
        $transaction->pmtcoderef = $request->input('account_id', '');
        $transaction->is_cash_flow = 0;


        $transaction->payment_mode = '';

        $transaction->particulars = '#' . $emp_bonus_prepare->id . 'Employee Bonus amount for team_id : ' . $emp_bonus_prepare->team_id . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

        $transaction->save();

        AccountHelper::addTransaction($credit = 0, $transaction->debit, $transaction->pmtcoderef);


        $resVal['id'] = $emp_bonus->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $emp_code = $request->input('emp_code', '');
        $is_active = $request->input('is_active', '');
        $date = $request->input('date');
        $team_id = $request->input('team_id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');



        //$status = $request->input('status', '');
        $builder = DB::table('tbl_employee_bonus')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($team_id)) {
            $builder->where('emp_team_id', '=', $team_id);
        }
        if ((!empty($from_date)) && (!empty($to_date))) {
            $builder->whereRaw(DB::raw("DATE_FORMAT((created_at),'%Y-%m-%d') >= DATE_FORMAT('$from_date','%Y-%m-%d') AND DATE_FORMAT((created_at),'%Y-%m-%d') <= DATE_FORMAT('$to_date','%Y-%m-%d')"));
        }
        if (!empty($date)) {
            $builder->whereRaw(DB::raw("DATE_FORMAT((created_at),'%Y-%m-%d') = DATE_FORMAT('$date','%Y-%m-%d')"));
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }


        if (!empty($emp_code)) {
            $builder->where('emp_code', 'like', '%' . $emp_code . '%');
        }


        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Employee BONUS Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_bonus = EmployeeBonus::findOrFail($id);
            if ($emp_bonus->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee BONUS   Not found';
            return $resVal;
        }
        $emp_bonus->updated_by = $currentuser->id;
        $emp_bonus->fill($request->all());
        $emp_bonus->save();

        //Activity log
        ActivityLogHelper::EmployeeBonusUpdate($emp_bonus);
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Bonus  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');

        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_bonus = EmployeeBonus ::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Bonus  Not found';
            return $resVal;
        }

        $emp_bonus->updated_by = $currentuser->id;
        $emp_bonus->is_active = 0;
        $get_accout = DB::table('tbl_emp_bonus_preparation')->where('id', "=", $emp_bonus->emp_bonus_prepare_id)->first();
        $account_id = $get_accout->account_id;

        $account_name = DB::table('tbl_accounts')
                        ->select('account')->where('id', '=', $account_id)->first();

        $account_type = $account_name->account;

        $transaction = new Transaction;
        $mydate = date('Y-m-d');
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

        $transaction->transaction_date = $mydate;
        $transaction->voucher_type = Transaction::$BONUS_DELETE;
        $transaction->voucher_number = $id;
        $transaction->credit = $emp_bonus->bonus_amount;
        $transaction->debit = 0.00;
        $transaction->account_category = '';

        $transaction->acode = '';
        $transaction->acoderef = '';
        $transaction->account = $account_type;
        $transaction->pmtcode = GeneralHelper::getPaymentAcode($account_id);
        $transaction->pmtcoderef = $account_id;
        $transaction->is_cash_flow = 0;


        $transaction->payment_mode = '';

        $transaction->particulars = '#' . $id . 'Employee Bonus amount for Employee : ' . $emp_bonus->emp_id . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

        $transaction->save();

        AccountHelper::addTransaction($emp_bonus->bonus_amount, $debit=0, $account_id);

 
        $emp_bonus->update();


        //Activity log
        ActivityLogHelper::EmployeeBonusDelete($emp_bonus);
        $resVal['id'] = $emp_bonus->id;
        return $resVal;
    }

    public function empBonus(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $team_id = $request->input('team_id');


        $builder = DB::table('tbl_employee as e')
                ->leftjoin('tbl_emp_production as ep', 'e.id', '=', 'ep.emp_id')
                ->select('ep.emp_id', 'ep.emp_code', 'ep.emp_name', 'ep.emp_team_id', 'e.bonus_percentage', DB::raw('((sum(case when ep.is_present=1 then ep.amount end)-sum(case when ep.is_present=1 then ep.ot_amount end))* e.bonus_percentage / 100) as bonus_amount')
                        , DB::raw('sum(case when ep.is_present=1 then ep.amount end) as salary_amount')
                        , DB::raw('sum(case when ep.is_present=1 then ep.amount end) - sum(case when ep.is_present=1 then ep.ot_amount End)  as week_salary')
                        , DB::raw('count(case ep.is_present when 1 then 1 end) as present_count ')
                        , DB::raw('count(case ep.is_present when 0 then 1 end) as absent_count'), DB::raw("$from_date as from_date"), DB::raw("$to_date as to_date"))
                ->groupby('e.id');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($from_date)) {
            $builder->whereDate('ep.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('ep.date', '<=', $to_date);
        }
        if (!empty($team_id)) {
            $builder->where('ep.emp_team_id', '=', $team_id);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $t) {
            $c = $t->emp_id;
            $c1 = $t->emp_team_id;



            $fixed_all_detail = DB::table('tbl_emp_allowance as al')
                            ->leftjoin('tbl_emp_production as em', 'em.emp_id', '=', 'al.emp_id')
                            ->select('al.amount')
                            ->where('em.date', '>=', $from_date)
                            ->where('em.date', '<=', $to_date)
                            ->where('em.is_active', '=', 1)
                            ->where('em.is_present', '=', 1)
                            ->whereRaw(DB::raw("(em.emp_id = '$c' ) and ( em.emp_team_id='$c1') "))
                            ->groupby('al.id')->get();

            $calculate_emp_work_days = DB::table('tbl_emp_production')
                    ->select('id')
                    ->where('date', '>=', $from_date)
                    ->where('date', '<=', $to_date)
                    ->where('is_active', '=', 1)
                    ->where('is_present', '=', 1)
                    ->whereRaw(DB::raw("( emp_id = '$c' ) and (emp_team_id='$c1') "))
            ;

            $fixed_all_amount = 0;
            $emp_working_days = $calculate_emp_work_days->count();
            foreach ($fixed_all_detail as $all) {
                $fixed_all_amount = $fixed_all_amount + $all->amount;
            }

            $fixed_all_amount = ($fixed_all_amount * $emp_working_days );



            $bonus = ($t->week_salary + $fixed_all_amount) * ($t->bonus_percentage / 100);
            $t->bonus_amount = $bonus;
        }

        $resVal['total'] = $builder->count();
        $resVal['list'] = $collection;




        return ($resVal);
    }

}

?>
