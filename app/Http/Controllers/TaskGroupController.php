<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\TaskGroup;
use App\TaskGroupDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

class TaskGroupController extends Controller {

    public function save(Request $request) {
        $resVal['message'] = 'Task Group Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $code = DB::table('tbl_task_group')
                    ->select('code')
                    ->where('code','=',$request->input('code'))
                    ->where('is_active','=',1);

//        if ($code->count()>0) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Task Group code is already exist';
//            return $resVal;
//        }
          $currentuser = Auth::user();
          
        $task = new TaskGroup;
        $task->updated_by = $currentuser->id;
        $task->created_by = $currentuser->id;
        $task->is_active = 1;
        $task->fill($request->all());
        $task->save();
        $detailTask = $request->input('detail');
        TaskGroupController::DetailTaskSave($task->id, $detailTask, $currentuser->id);
        $resVal['id'] = $task->id;
        return $resVal;
    }

    public static function DetailTaskSave($taskId, $detailTask, $currentUserId) {

        foreach ($detailTask as $detail) {
            $ti = new TaskGroupDetail;
            $ti->fill($detail);
            $ti->task_group_id = $taskId;
            $ti->updated_by = $currentUserId;
            $ti->created_by = $currentUserId;
            $ti->is_active = 1;
            $ti->save();
        }
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Task Group Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentuser = Auth::user();
        try {
            $task= TaskGroup::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Group Not found';
            return $resVal;
        }
        
       $code = DB::table('tbl_task_group')
                    ->select('code')
                    ->where('code','=',$request->input('code'))
               ->where('id','!=',$id)
                    ->where('is_active','=',1);

//        if ($code->count()>0) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Task Group code is already exist';
//            return $resVal;
//        }
        $task->updated_by = $currentuser->id;
        $task->is_active = 1;
        $task->fill($request->all());
        $task->save();
        $detailTask = $request->input('detail');
         $builder = DB::table('tbl_task_group_detail')->where('task_group_id', $id)->delete();
        TaskGroupController::DetailTaskSave($task->id, $detailTask, $currentuser->id);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $name = $request->input('name','');
        $code = $request->input('code','');
        $department_id = $request->input('department_id');
        $is_active = $request->input('is_active','');
        
        $builder=DB::table('tbl_task_group as tg')
                ->select('tg.*');
                
        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('tg.id', '=', $id);
        }

        if (!empty($name)) {
            $builder->where('tg.name', 'like', '%' . $name . '%');
        }
         if (!empty($code)) {
            $builder->where('tg.code','=',$code);
        }


        if ($is_active != '') {
            $builder->where('tg.is_active', '=', $is_active);
        }
        $builder->orderBy('tg.id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $taskCollection = $builder->get();
        } else {

            $taskCollection = $builder->skip($start)->take($limit)->get();
        }
        
         foreach ($taskCollection as $it) {
           $id = $it->id;
            $detail = DB::table('tbl_task_group_detail as td')
                    ->leftjoin('tbl_department as dep','dep.id','=','td.department_id')
                    ->leftjoin('tbl_work_center as wc','wc.id','=','td.work_center_id')
                    ->select('td.*','wc.name as work_center_name','dep.name as department_name')
                    ->where('td.task_group_id', $id)
                    ->orderBy('sequence_no','asc');
            
            
            if(!empty($department_id)){
                $detail->where('td.department_id','=',$department_id);
            }
            
            $it->detail = $detail->get();
        }
        
        $resVal['list'] = $taskCollection;
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Task Group Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentuser = Auth::user();
        try {
            $task = TaskGroup::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Group Not found';
            return $resVal;
        }
        $builder = DB::table('tbl_task_group_detail')->where('task_group_id', $id)->delete();
        $task->is_active = 0;
        $task->updated_by = $currentuser->id;
        $task->save();
        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
        $id=$request->input('id');
        $department_id = $request->input('department_id');
       
       $builder=DB::table('tbl_task_group as tg')
                ->select('tg.*')
              ->where('tg.id','=',$id)
                ->where('tg.is_active','=',1);
               
        
        $resVal['success'] = TRUE;
        $builder->orderBy('tg.id', 'desc');
        
        $resVal['total'] = $builder->count();

            $taskCollection = $builder->get();
        foreach ($taskCollection as $it) {
           $id = $it->id;
             $detail = DB::table('tbl_task_group_detail as td') 
                    ->leftjoin('tbl_department as dep','dep.id','=','td.department_id')
                    ->leftjoin('tbl_work_center as wc','wc.id','=','td.work_center_id')
                    ->select('td.*','wc.name as work_center_name','dep.name as department_name')
                    ->where('td.task_group_id', $id)
                      ->orderBy('sequence_no','asc'); 
             
              if(!empty($department_id)){
                $detail->where('td.department_id','=',$department_id);
            }
            $it->detail = $detail->get();
        }
        $resVal['list'] = $taskCollection;
        return ($resVal);
    }

}
