<?php

namespace App\Http\Controllers;

use DB;
use App\Apconfig;
use Illuminate\Http\Request;
//use App\Transformer\InvoiceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use pdo;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApconfigController
 *
 * @author Deepa
 */
class ApconfigController extends Controller {

    //put your code here
    public function update(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Appconfig Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $setting = $request->all();
//
        foreach ($setting as $std) {

            foreach ($std as $key => $p) {

                if ($std['setting'] != NULL) {
                    $apconfigCollection = Apconfig::where('setting', "=", $std['setting'])->get();
                    if ($std['setting'] == 'timezone') {
                        $value = $std['value'];
                        $currentWorkingDir = getcwd();

                        $currentDir = substr($currentWorkingDir, 0, strlen($currentWorkingDir) - 6);
                        $envArray = parse_ini_file($currentDir . '.env');
                        $sso_server = $envArray['DB_HOST'];
                        $sso_db_username = $envArray['SSO_DB_USERNAME'];
                        $sso_db_password = $envArray['SSO_DB_PASSWORD'];
                        $sso_db_name = $envArray['SSO_DB_DATABASE'];
                        $app_user_name = '';
                        try {
                            $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
                        } catch (Exception $e) {
                            die("Connection failed: " . $conn->connect_error);
                        }
                        $domain = $_SERVER['SERVER_NAME'];
                        $sql = "update tbl_bu set time_zone = '" . $value . "' where  name= '" . $domain . "'";
                        $res = $conn->query($sql);
                    }
                }

                if (count($apconfigCollection) > 0) {
                    $apconfig = $apconfigCollection->first();
                } else {
                    $apconfig = new Apconfig;
                }

                $apconfig->setting = $std['setting'];
                $apconfig->value = $std['value'];
                $apconfig->created_by = $currentuser->id;
                $apconfig->updated_by = $currentuser->id;

                $apconfig->save();
            }
        }



        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $setting = $request->input('setting', '');
        $value = $request->input('value', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_app_config')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($value)) {
            $builder->where('value', 'like', '%' . $value . '%');
        }

        if (!empty($setting)) {
            $builder->where('setting', 'like', '%' . $setting . '%');
        }
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

}

?>
