<?php
namespace App\Http\Controllers;
use DB;
use App\RoleAccess;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserScreenAccess
 *
 * @author Deepa
 */
class RoleAccessController extends Controller{
    //put your code here
    public function update(Request $request){
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Role Access Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $accesslist = $request->all();

        foreach ($accesslist as $std) 
        {
            if ($std['role_id'] != NULL) {
                $useracessCollection = RoleAccess::where('role_id', "=", $std['role_id'])
                        ->where('screen_id', "=", $std['screen_id'])
                        ->get();
            }

            if (count($useracessCollection) > 0) {
                $useraccess = $useracessCollection->first();
            } else {
                $useraccess = new RoleAccess;
            }
            
            $builder = DB::table('tbl_screen')
                ->select('code')
                ->where('id', "=", $std['screen_id'])->first();            
            $useraccess->fill($std);
            $useraccess->screen_code = $builder->code;
            $useraccess->created_by = $currentuser->id;
            $useraccess->is_active = 1;
            $useraccess->screen_name=$useraccess->screen_name;
            $useraccess->updated_by = $currentuser->id;
            $useraccess->save();
        }


        return $resVal;
    }
     public function listAll(Request $request) {
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
        $roleid = $request->input('role_id', '');
        $isactive =$request->input('is_active','');
        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);
        $builder = DB::table('tbl_role_access as us')
                ->leftJoin('tbl_screen as s', 'us.screen_id', '=', 's.id')
                ->select('us.*', 's.code as code','s.sno as sno','s.section_sqno');

        if (!empty($id)) {
            $builder->where('us.id', '=', $id);
        }
        if ($roleid != '') {
            $builder->where('us.role_id', '=', $roleid);
        }
        if ($isactive != '') {
            $builder->where('us.is_active', '=', $isactive);
        }
        
        $builder ->orderBy('s.section_sqno','asc');
        
        $resVal['total'] = $builder->count();
       if ($start == 0 && $limit == 0) {
           $resVal['list']= $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }
}

?>
