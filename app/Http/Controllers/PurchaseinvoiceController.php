<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use App\ActivityLog;
use App\Transaction;
use App\PurchasePayment;
use App\Purchaseinvoice;
use App\Purchaseinvoiceitem;
use Illuminate\Http\Request;
use App\Attributevalue;
use App\Attribute;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\AccountHelper;
use App\Helper\NotificationHelper;
use App\Events\PurchaseNotificationEvent;
use App\Helper\InventoryHelper;
use Event;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PurchaseinvoiceController
 *
 * @author Deepa
 */
class PurchaseinvoiceController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = ' Purchase invoice Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $purchase_no = $request->input('purchase_no', '');
        $prefix = $request->input('prefix', '');
        $purchase_code = '';
        if (!empty($purchase_no)) {

            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('purchase_no', '=', $purchase_no)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Purchase Invoice no already exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $purchase_no = $purchase_no;
                $purchase_code = $prefix . $purchase_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('purchase_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('purchase_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->purchase_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $purchase_no = $inv_no;
            $purchase_code = $prefix . $inv_no;
        }
        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax(0, $taxId, $taxable_amount, 'purchase');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff;
        $total = floatval(number_format($total, 2, '.', ''));

        $currentuser = Auth::user();


        $purchaseinvoice = new Purchaseinvoice;
        $purchaseinvoice->fill($request->all());
        $purchaseinvoice->created_by = $currentuser->id;
        $purchaseinvoice->updated_by = $currentuser->id;
        $purchaseinvoice->is_active = $request->input('is_active', 1);

        $purchaseinvoice->subtotal = $subtotal;
        $purchaseinvoice->tax_amount = $tax_amount;
        $purchaseinvoice->discount_amount = $discount_amount;
        $purchaseinvoice->round_off = $roundoff;
        $purchaseinvoice->total_amount = $total;
        $purchaseinvoice->purchase_no = $purchase_no;
        $purchaseinvoice->purchase_code = $purchase_code;


        $purchaseinvoice->save();

        if ($request->input('purchase_quote_id') != NULL && $request->input('purchase_quote_id') != 0 && !empty($request->input('purchase_quote_id'))) {

            DB::table('tbl_purchase_payment')->where('purchase_quote_id', $request->input('purchase_quote_id'))
                    ->where('is_active', "=", 1)
                    ->update(['purchase_invoice_id' => $purchaseinvoice->id]);
            DB::table('tbl_purchase_quote')->where('id', $request->input('purchase_quote_id'))->update(['status' => 'invoiced']);
        }


        $totalamount = $purchaseinvoice->total_amount;


        $payment = 0;
        if ($request->input('purchase_quote_id') != NULL && $request->input('purchase_quote_id') != 0 && !empty($request->input('purchase_quote_id'))) {

            $payment = DB::table('tbl_purchase_payment')->where('purchase_quote_id', $request->input('purchase_quote_id'))
                    ->where('is_active', "=", 1)
                    ->sum('amount');
        }

        if (($totalamount <= $payment)) {
            // DB::table('tbl_invoice') ->where('id',$invoice->id) ->update(['status' => 'paid' ]);
            $purchaseinvoice->status = 'paid';
        } elseif ($payment == 0) {
            $purchaseinvoice->status = 'unpaid';
        } elseif ($totalamount > $payment) {

            $purchaseinvoice->status = 'partiallyPaid';
        }

        $customerItemCol = ($request->input('customattribute'));
        $purchaseinvoice->paid_amount = $payment;
        $attribute = array();

        $purchaseinvoice->save();

        foreach ($invoiceItemCol as $item) {

            $purchaseinvoiceitems = new Purchaseinvoiceitem;
            $purchaseinvoiceitems->fill($item);

            $purchaseinvoiceitems->created_by = $currentuser->id;
            $purchaseinvoiceitems->updated_by = $currentuser->id;
            $purchaseinvoiceitems->purchase_invoice_id = $purchaseinvoice->id;
            $purchaseinvoiceitems->is_active = $request->input('is_active', 1);
            $purchaseinvoiceitems->customer_id = $request->input('customer_id');
            $purchaseinvoiceitems->duedate = $purchaseinvoice->duedate;
            $purchaseinvoiceitems->paymentmethod = $purchaseinvoice->paymentmethod;
            $purchaseinvoiceitems->tax_amount = $purchaseinvoice->tax_amount;

            $existType = DB::table('tbl_qcp')
                    ->select('*')
                    ->where('type', '=', 'purchase_invoice_item')
                    ->where('product_id', '=', $item['product_id'])
                    ->where('is_active', '=', 1)
                    ->first();
            if (!empty($existType))
                $purchaseinvoiceitems->qc_status = 'required';

            $purchaseinvoiceitems->save();
        }
        if ($request->input('customattribute') != null) {

            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $purchaseinvoice->custom_attribute_json = $json_att;
            $purchaseinvoice->save();

            foreach ($customerItemCol as $attribute) {
                //print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $purchaseinvoice->id;
                $customerattribute->value = $attribute['value'];

                $customerattribute->attribute_code = $attribute['attribute_code'];

                $customerattribute->created_by = $currentuser->id;
                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
                        //print_r($attribute->id);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

                //$customerattribute->fill($attribute);       

                $customerattribute->save();
            }
        }

        $builder = DB::table('tbl_customer')
                        ->select('*')
                        ->where('id', '=', $purchaseinvoice->customer_id)->first();
        $cus_name = $builder->fname . " " . $builder->lname;


        // Transaction entry for invoice
        $transaction = new Transaction;
        $mydate = $purchaseinvoice->date;
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

        $transaction->transaction_date = $purchaseinvoice->date;
        $transaction->voucher_type = Transaction::$PURCHASE;
        $transaction->voucher_number = $purchaseinvoice->id;
        $transaction->credit = $purchaseinvoice->total_amount;
        $transaction->debit = 0.00;
        $transaction->account_category = '';

        $transaction->acode = GeneralHelper::getCustomerAcode($purchaseinvoice->customer_id);
        $transaction->acoderef = $purchaseinvoice->customer_id;
        $transaction->account = $request->input('account', '');
        $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
        $transaction->pmtcoderef = $request->input('account_id', '');
        $transaction->is_cash_flow = 0;


        $transaction->payment_mode = '';

        $transaction->particulars = '#' . $purchaseinvoice->id . 'Purchase invoice Created for ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->prefix = $purchaseinvoice->prefix;
        $transaction->save();


        $resVal['id'] = $purchaseinvoice->id;
        ActivityLogHelper::purchaseinvoiceSave($purchaseinvoice);
        if ($purchaseinvoice->type != 'grn')
            GeneralHelper::StockAdjustmentSave($request->input('item'), 'purchase', $purchaseinvoice->id);

        foreach ($invoiceItemCol as $item) {
            if (array_key_exists('internal_product_id', $item)) {
                if (!empty($item['internal_product_id'])) {
                    InventoryHelper::increase($item, 'purchase', $purchaseinvoice->id, 'debit');
                    InventoryHelper::increase($item, 'purchase', $purchaseinvoice->id, 'credit');
                }
            }
        }

        GeneralHelper::InvoiceTaxSave($purchaseinvoice->id, $request->input('tax_id'), $taxable_amount, $subtotal, 'purchase');
        $response = Event::fire(new PurchaseNotificationEvent($purchaseinvoice));
        $resVal['notification'] = $response;

        if ($purchaseinvoice->type == 'grn' && $purchaseinvoice->ref_id != '') {
            DB::table('tbl_grn')->where('id', '=', $purchaseinvoice->ref_id)->update(["status" => "invoiced"]);
        }
        return $resVal;
    }

    public function update(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase invoice Updated Successfully';
        $currentuser = Auth::user();
        try {
            $purchaseinvoice = Purchaseinvoice::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase invoice Not found';
            return $resVal;
        }

        $purchase_no = $request->input('purchase_no', '');
        $prefix = $request->input('prefix', '');
        $purchase_code = '';
        if (!empty($purchase_no)) {

            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('purchase_no', '=', $purchase_no)
                    ->where('id', '!=', $id)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Purchase Invoice no already exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $purchase_no = $purchase_no;
                $purchase_code = $prefix . $purchase_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('purchase_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->where('id', '!=', $id)
                    ->orderBy('purchase_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->purchase_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $purchase_no = $inv_no;
            $purchase_code = $prefix . $inv_no;
        }
        $items = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', '=', $id)->get();

        if ($purchaseinvoice->type != 'grn') {
           
                InventoryHelper::purchaseStockAdjustDelete($purchaseinvoice->id);
        }
        $builder = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->delete();

        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax($id, $taxId, $taxable_amount, 'purchase');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff;
        $total = floatval(number_format($total, 2, '.', ''));



        $input_request_data = $request->all();
        if ($input_request_data['tax_id'] == 'invoiced_tax_rate') {
            unset($input_request_data['tax_id']);
        }
        $purchaseinvoice->fill($input_request_data);
        $purchaseinvoice->subtotal = $subtotal;
        $purchaseinvoice->tax_amount = $tax_amount;
        $purchaseinvoice->discount_amount = $discount_amount;
        $purchaseinvoice->round_off = $roundoff;
        $purchaseinvoice->total_amount = $total;
        $purchaseinvoice->purchase_no = $purchase_no;
        $purchaseinvoice->purchase_code = $purchase_code;

        $purchaseinvoice->save();


        foreach ($invoiceItemCol as $item) {

            $purchaseinvoiceitems = new Purchaseinvoiceitem;
            $purchaseinvoiceitems->purchase_invoice_id = $purchaseinvoice->id;
            $purchaseinvoiceitems->fill($item);
            //$invoiceitems->invoice_id = $invoice->id;
            $purchaseinvoiceitems->is_active = $request->input('is_active', 1);
            $purchaseinvoiceitems->customer_id = $request->input('customer_id');
            $purchaseinvoiceitems->duedate = $purchaseinvoice->duedate;
            $purchaseinvoiceitems->paymentmethod = $purchaseinvoice->paymentmethod;
            $purchaseinvoiceitems->tax_amount = $purchaseinvoice->tax_amount;

            $existType = DB::table('tbl_qcp')
                    ->select('*')
                    ->where('type', '=', 'purchase_invoice_item')
                    ->where('product_id', '=', $item['product_id'])
                    ->where('is_active', '=', 1)
                    ->first();
            if (!empty($existType)) {
                $purchaseinvoiceitems->qc_status = 'required';
            }

            $purchaseinvoiceitems->save();
        }

        $attributeCollection = DB::table('tbl_attribute_types')->where('code', 'purchaseinvoice')->get();
        $attribute = $attributeCollection->first();

        if (count($attributeCollection) > 0) {


            $builder = DB::table('tbl_attribute_value')
                    ->where('attributetype_id', '=', $attribute->id)
                    ->where('refernce_id', '=', $id)
                    ->delete();
            //print_r($builder);
        }
        if ($request->input('customattribute') != null) {
            $customerItemCol = ($request->input('customattribute'));
            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $purchaseinvoice->custom_attribute_json = $json_att;



            foreach ($customerItemCol as $attribute) {
                //print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $purchaseinvoice->id;
                $customerattribute->value = $attribute['value'];

                $customerattribute->attribute_code = $attribute['attribute_code'];

                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();

                    $attribute = $attributeCollection->first();
                    //print_r($attribute->id);
                    $customerattribute->attributes_id = $attribute->id;
                    $customerattribute->attributetype_id = $attribute->attributetype_id;
                }


                $customerattribute->save();
            }
        }
        $builder = DB::table('tbl_customer')
                ->select('*')
                ->where('id', '=', $purchaseinvoice->customer_id)
                ->first();
        $cus_name = $builder->fname . " " . $builder->lname;
        $currentuser = Auth::user();
        $voucher_number = $purchaseinvoice->id;
        $builder1 = DB::table('tbl_transaction')
                ->select('*')
                ->where('voucher_number', '=', $voucher_number)
                ->where('voucher_type', '=', 'purchase_invoice')
                ->first();
        // Transaction entry for invoice
        $id1 = $builder1->id;
        $transaction = Transaction::findOrFail($id1);
        $mydate = $purchaseinvoice->date;
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }

        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }

        $transaction->transaction_date = $purchaseinvoice->date;
        $transaction->voucher_type = Transaction::$PURCHASE;
        $transaction->voucher_number = $purchaseinvoice->id;
        $transaction->credit = $purchaseinvoice->total_amount;
        $transaction->debit = 0.00;
        $transaction->account_category = '';

        $transaction->acode = GeneralHelper::getCustomerAcode($purchaseinvoice->customer_id);
        $transaction->acoderef = $purchaseinvoice->customer_id;
        $transaction->account = $request->input('account', '');
        $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
        $transaction->pmtcoderef = $request->input('account_id', '');
        $transaction->is_cash_flow = 0;


        $transaction->payment_mode = '';

        $transaction->particulars = '#' . $purchaseinvoice->id . 'Purchase invoice Created for ' . $cus_name . ' by ' . $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->prefix = $purchaseinvoice->prefix;
        $transaction->save();

        $totalamount = $purchaseinvoice->total_amount;


        $payment = DB::table('tbl_purchase_payment')
                ->where('purchase_invoice_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('amount');

        if ($payment == 0) {
            $purchaseinvoice->status = 'unpaid';
        } else if ($totalamount <= $payment) {

            $purchaseinvoice->status = 'paid';
        } else if ($totalamount >= $payment) {

            $purchaseinvoice->status = 'partiallyPaid';
        }

        $purchaseinvoice->paid_amount = $payment;
        $purchaseinvoice->save();

        ActivityLogHelper::purchaseinvoiceModify($purchaseinvoice);
        if ($purchaseinvoice->type != 'grn')
            GeneralHelper::StockAdjustmentSave($request->input('item'), "update", $id);


        foreach ($invoiceItemCol as $item) {
            if (array_key_exists('internal_product_id', $item)) {
                if (!empty($item['internal_product_id'])) {
                    InventoryHelper::increase($item, 'update', $purchaseinvoice->id, 'debit');
                    InventoryHelper::increase($item, 'update', $purchaseinvoice->id, 'credit');
                }
            }
        }


        GeneralHelper::InvoiceTaxUpdate($id, $request->input('tax_id'), $taxable_amount, $subtotal, 'purchase');

        if ($purchaseinvoice->type == 'grn' && $purchaseinvoice->ref_id != '') {
            DB::table('tbl_grn')->where('id', '=', $purchaseinvoice->ref_id)->update(["status" => "invoiced"]);
        }

        return $resVal;
    }

    public function payablereport_csvexport(Request $request) {
        $payableLists = $this->payable_list($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "payablereport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('#', 'Customer Name', 'Phone', 'Email', 'Billing Address', 'Shipping Address', 'Amount'));
        foreach ($payableLists['list'] as $values) {
            fputcsv($output, array($sr, $values->customer_fname, $values->phone, $values->email, $values->billing_address, $values->shopping_address, $values->balance));
            $sr++;
        }
        return response()->download($filePath . "/" . $filename, 'payablereport.csv');
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $purchase_code = $request->input('purchase_code');
        $reference_no = $request->input('reference_no');
        $customerid = $request->input('customer_id', '');
        $mobile = $request->input('mobile', '');
        $fromDate = $request->input('from_duedate', '');
        $toDate = $request->input('to_duedate', '');
        $status = $request->input('status');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $invoiceDate = $request->input('invoice_date', '');
        $dueDate = $request->input('due_date', '');
        $discountpercentage = $request->input('discount_percentage');
        $is_active = $request->input('is_active', '');
        $mode = $request->input('mode');
        $type = $request->input('type');
        $ref_id = $request->input('ref_id');
        //$status = $request->input('status', '');
//        $builder = DB::table('tbl_purchase_invoice')
//                ->select('*');

        $builder = DB::table('tbl_purchase_invoice as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 't.tax_percentage as taxpercentage');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }
        if (!empty($purchase_code)) {
            $builder->where('i.purchase_code', 'like', '%' . $purchase_code . '%');
        }

        if (!empty($reference_no)) {
            $builder->where('i.reference_no', 'like', '%' . $reference_no . '%');
        }

        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }

        if (!empty($fromDate)) {

            $builder->whereDate('i.duedate', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('i.duedate', '<=', $toDate);
        }
        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }
        if (!empty($invoiceDate)) {
            $builder->whereDate('i.date', '=', $invoiceDate);
        }
        if (!empty($dueDate)) {
            $builder->whereDate('i.duedate', '=', $dueDate);
        }
        if (!empty($status)) {
            $builder->where('i.status', '=', $status);
        }
        if (!empty($ref_id)) {
            $builder->where('i.ref_id', '=', $ref_id);
        }
        if (!empty($type)) {
            $builder->where('i.type', '=', $type);
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }

        if ($is_active != '') {
            $builder->where('i.is_active', '=', $is_active);
        }

        $arrays[] = $request->toArray();
        //     print_r($arrays);
        $arrays2 = $arrays[0];
        $array_key = array_keys($arrays2);
        $custom_array = array();
        for ($index = 0; $index < count($array_key); $index ++) {
            $arry_ind = $array_key[$index];
            $result = substr($arry_ind, 0, 8);
            if ($result == 'cus_attr') {
                array_push($custom_array, $arry_ind);
            }
        }
        for ($index = 0; $index < count($custom_array); $index ++) {
            $status1 = $custom_array[$index];
            $status = $arrays2[$status1];
            if (!empty($status)) {
                $status = explode("::", $status);
                $key = $status[0];
                $value = $status[1];
                $str = "'%" . '"' . $key . '":"' . $value . '"%' . "'";
                $builder->whereRaw('i.custom_attribute_json  like' . "'%" . '"' . $key . '":"' . $value . '"%' . "'");
                //  $builder->Where ('custom_attribute_json', 'like',  $str);                        
            }
        }


        if ($mode == 0 || $mode == '') {
            $builder->orderBy('i.id', 'desc');
        } else {
            $builder->orderBy('i.date', 'asc');
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $purchaseinvoiceCollection = $builder->get();
        } else {

            $purchaseinvoiceCollection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($purchaseinvoiceCollection as $invoice) {
            $json_att = $invoice->custom_attribute_json;

            if (empty($json_att)) {
                $json_att = "{}";
            }
            $json_att = json_decode($json_att);
            $invoice->custom_attribute_json = $json_att;
        }
        $resVal['list'] = $purchaseinvoiceCollection;
        return ($resVal);
    }

    public function payable_list(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $customerid = $request->input('customer_id', '');
        $fromdate = $request->input('from_date');
        $todate = $request->input('to_date');
        $overdue = $request->input('overdue', '');
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');

        $builder = DB::table('tbl_purchase_invoice as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('c.id', 'c.fname as customer_fname', 'c.lname as customer_lname', 'c.company', 'c.phone', 'c.email', 'c.billing_address', 'c.shopping_address', 'c.billing_city', 'c.shopping_city', 'c.shopping_state', 'c.billing_state', 'c.billing_country', 'c.shopping_pincode', 'c.billing_pincode', 'c.billing_country', 'c.is_sale', 'c.is_purchase', DB::raw('sum(i.total_amount) - sum(i.paid_amount) as balance'))
                ->where('i.status', '!=', 'paid')
                ->where('i.is_active', '=', '1')
                ->groupBy('c.id');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if ($overdue == 1) {
            $builder->where('i.duedate', '<=', $current_date);
        }
        if (!empty($customerid)) {
            $builder->where('i.customer_id', '=', $customerid);
        }

        if (!empty($fromdate)) {

            $builder->whereDate('i.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('i.date', '<=', $todate);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Purchase Invoice Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $currentuser = Auth::user();
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $purchaseinvoice = Purchaseinvoice::findOrFail($id);
            if ($purchaseinvoice->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Purchase Invoice Not found';
            return $resVal;
        }
        $trans_id = DB::table('tbl_transaction')
                ->select('*')
                ->where('voucher_number', '=', $id)
                ->where('voucher_type', '=', 'purchase_invoice')
                ->first();

        $id2 = $trans_id->id;
        $transaction = Transaction::findOrFail($id2);
        $transaction->updated_by = $currentuser->id;
        $transaction->is_active = 0;
        $transaction->save();
        $paymentCollection = PurchasePayment::where('purchase_invoice_id', '=', $id)->where('is_active', '=', 1)->get();
        if ($paymentCollection->count() > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Unable to delete, Please remove the payment transaction';
            return $resVal;
        }

        $purchaseinvoiceitem = DB::table('tbl_purchase_invoice_item')->where('purchase_invoice_id', $id)->update(['is_active' => 0]);
        $purchaseinvoice->is_active = $request->input('is_active', 0);
        //$purchaseinvoice->fill($request->all());
        $purchaseinvoice->update();
        $currentuser = Auth::user();

        $resVal['id'] = $purchaseinvoice->id;
        ActivityLogHelper::purchaseinvoiceDelete($purchaseinvoice);
        if ($purchaseinvoice->type != 'grn') {           
                InventoryHelper::purchaseStockAdjustDelete($purchaseinvoice->id);
        }
        GeneralHelper::InvoiceTaxDelete($id, 'purchase');

        //$purchaseinvoice->delete();

        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id', '');


        $isactive = $request->input('isactive', '');
        $discountpercentage = $request->input('discount_percentage', '');

        //$builder = DB::table('tbl_invoice')->select('*');
        $builder = DB::table('tbl_purchase_invoice as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'i.tax_id', '=', 't.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.company as company_name', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email'
                        , 't.tax_percentage as taxpercentage')
                ->where('i.id', '=', $id);


        $purchaseinvoiceitems = DB::table('tbl_purchase_invoice_item as pi')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'pi.internal_product_id')
                ->select('pi.*', 'p.name as internal_product_name')
                ->where('purchase_invoice_id', $id)
                ->where('pi.is_active', '=', 1);
        $customerattribute = DB::table('tbl_attribute_value')->where('refernce_id', $id);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }
        if (!empty($discountpercentage)) {
            $builder->where('i.discount_percentage', '=', $discountpercentage);
        }

        if ($isactive != '') {
            $builder->where('i.is_active', '=', $isactive);
        }
        $builder->orderBy('i.id', 'desc');

        $invoiceCollection = $builder->skip($start)->take($limit)->get();
        if (count($invoiceCollection) == 1) {

            $purchaseinvoice = $invoiceCollection->first();

            $purchaseinvoice->item = $purchaseinvoiceitems->get();

            //$invoice->customattribute = $customerattribute->get();
            $customAttributeValue = DB::table('tbl_attribute_value as a')
                            ->leftJoin('tbl_attributes as abs', 'a.attributes_id', '=', 'abs.id')
                            ->select('a.*', 'abs.attribute_label as attribute_label')
                            ->where('a.refernce_id', '=', $id)
                            ->where('abs.attributetype_code', '=', 'purchaseinvoice')->get();

            $customattribute = DB::table('tbl_attributes as abs')
                            ->select('abs.*')
                            ->where('abs.attributetype_code', '=', 'purchaseinvoice')->get();
            if (count($customAttributeValue) > 0) {

                foreach ($customAttributeValue as $item) {

                    foreach ($customattribute as $customattributeItem) {

                        if ($item->attribute_code == $customattributeItem->attribute_code) {

                            $customattributeItem->value = $item->value;
                        }
                    }
                }
            }
            $purchaseinvoice->customattribute = $customattribute;

            $invoice_tax_list_collection = DB::table('tbl_purchase_invoice_tax')
                            ->select('*')
                            ->where('invoice_id', '=', $id)
                            ->where('is_active', '=', 1)->get();

            $purchaseinvoice->tax_list = $invoice_tax_list_collection;

            $resVal['data'] = $purchaseinvoice;
        }

        return ($resVal);
    }

    public function invoiceDetail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $customer_id = $request->input('customer_id');
        $reference_no = $request->input('reference_no');
        $purchase_invoice_id = $request->input('purchase_invoice_id');

        $builder = DB::table('tbl_purchase_invoice')
                        ->where('customer_id', '=', $customer_id)
                        ->where('reference_no', '=', $reference_no)
                        ->where('is_active', '=', 1)->get();

        if ((count($builder) > 0) && ($reference_no != '') && ($purchase_invoice_id == 0)) {
//                         $resVal['message'] = 'The Reference no  already Exists';
            $resVal['success'] = FALSE;
            return $resVal;
        } else {
//            $resVal['message'] = 'The Reference no  does not  Exists';
            $resVal['success'] = TRUE;
            return $resVal;
        }
    }

}

?>
