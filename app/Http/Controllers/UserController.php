<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Validator;
use App\User;
use Illuminate\Http\Request;
use App\Helper\SmsHelper;
use App\Helper\MailHelper;
use App\Transformer\UserTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Helper\AuthorizationHelper;
use PDO;
use App\Helper\SSOHelper;

/**
 * Class BooksController
 * @package App\Http\Controllers
 */
class UserController extends Controller {

    /**
     * GET /books
     * @return array
     */
    public function index(Request $request) {

        $currentUser = Auth::user();

        
        
        $limit = $request->input('limit', 100);

        return User::pagicnate($limit);
    }

    public function userInvite(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Successfully';
        $currentuser = Auth::user();

        $accesslist = $request->all();

        foreach ($accesslist as $std) {
            if ($std['email'] != NULL) {
                $userCollection = User::where('username', "=", $std['email'])
                        ->where('is_active', "=", 0)
                        ->get();
                $userCollection1 = User::where('username', "=", $std['email'])
                        ->where('is_active', "=", 1)
                        ->get();
            }
            $user_save = 0;
            if (count($userCollection) == 1) {
                $userdet = $userCollection->first();
                $user_save = 1;
            } else if (count($userCollection1) == 0) {
                $userdet = new User;
                $user_save = 1;
            }

            if ($user_save == 1) {
                $userdet->username = $std['email'];
                $userdet->email = $std['email'];
                $userdet->status = 'invited';
                $userdet->created_by = $currentuser->id;
                $userdet->is_active = 1;
                $userdet->updated_by = $currentuser->id;
                $userdet->save();
                $mail_obj = new MailHelper;
                $mail_obj->userInivitationNotification($userdet);
            }
        }


        return $resVal;
    }

    /**
     * POST /books
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */ 
    public function save(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'User Saved Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
//        $validator = Validator::make($request->all(), [
//                    'email' => 'required|unique:tbl_user,username'
//        ]);
//        if ($validator->fails()) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Error in request format';
//            if (array_key_exists('email', $validator->failed())) {
//                $resVal['message'] = 'User name is already exist';
//            }
//
//            return $resVal;
//        }

        $userCollection = User::where('username', "=", $request->input('email'))->where('is_active','=',1)->get();
        if (count($userCollection) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Username is already exits.';
            return $resVal;
        }

        $currentuser = Auth::user();
        $user = new User;
        $user->f_name = $request->input('f_name');
        $user->l_name = $request->input('l_name');
        $user->gender = $request->input('gender');
        $user->dob = $request->input('dob');
        $user->email = $request->input('email');
        $user->ph_no = $request->input('ph_no');
        $user->city = $request->input('city');
        $user->state = $request->input('state');
        $user->country = $request->input('country');
        $user->address = $request->input('address');
        $user->post_code = $request->input('post_code');
        $user->user_type = $request->input('user_type','user');
        $user->role_id = $request->input('role_id');
        $user->rolename = $request->input('rolename');
        $user->is_active = $request->input('isactive', 1);
        $user->comp_id = $currentuser->comp_id;
        $user->created_by = $currentuser->id;
        $user->updated_by = $currentuser->id;    
        $user->imagepath = $request->input('imagepath');
        $user->device_id = $request->input('device_id');
        $user->username = $request->input('email');
        //$user->password = Hash::make($request->input('password'));
        $user->status = 'invited';
        $user->save();
        $resVal['id'] = $user->id;

        $mail_obj = new MailHelper;
        $mail_obj->userInivitationNotification($user);
        return $resVal;
    }

    /**
     * PUT /books/{id}
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'User Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
          $userCollection = User::where('username', "=", $request->input('email'))->where('is_active','=',1)->where('id','!=',$id)->get();
        if (count($userCollection) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Username is already exits.';
            return $resVal;
        }else{
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'User Not found';
            return $resVal;
        }
         
        $user->fill($request->all());
        $user->save();
        }
        return $resVal;
    }

    /**
     * DELETE /books/{id}
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'User Deleted Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'User Not found';
            return $resVal;
        }
        $user->updated_by = $currentuser->id;
        $user->is_active = 0;

        $user->update();
          SSOHelper::businessDelete($user->username);
          DB::table('persistent_logins')->where('username','=',$user->username)->delete();
          DB::table('tbl_employee')->where('user_id','=',$id)->update(['is_active'=>0]);
          
        $resVal['id'] = $user->id;
        return $resVal;
    }

    public function userByName($userName) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        return User::where('username', "=", $userName) . get();
    }

    //LIST ALL
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $userId = $request->input('userId');

        $name = $request->input('name');
        $userName = $request->input('username');
        $email = $request->input('email');
        $city = $request->input('city');
        $country = $request->input('country');
        $phno = $request->input('phno');
        $userType = $request->input('userType');
        $rolename = $request->input('role_name', '');
        $isactive = $request->input('isactive', '');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $builder = DB::table('tbl_user as us')
                ->leftJoin('tbl_role as r', 'us.role_id', '=', 'r.id')
                ->select('us.*', 'r.name as Role_name');

        if (!empty($userId)) {
            $builder->where('us.id', '=', $request->input('userId'));
        }

        if (!empty($name)) {
            $builder->where('us.f_name', 'like', $name . '%');
        }
        if (!empty($userName)) {
            $builder->where('us.username', 'like', $userName . '%');
        }
        if (!empty($rolename)) {
            $builder->where('r.name', 'like', $rolename . '%');
        }
        if (!empty($email)) {
            $builder->where('us.email', 'like', $email . '%');
        }
        if (!empty($city)) {
            $builder->where('us.city', 'like', $city . '%');
        }
        if (!empty($country)) {
            $builder->where('us.country', 'like', $country . '%');
        }
        if (!empty($phno)) {
            $builder->where('us.ph_no', 'like', $phno . '%');
        }   
        if (!empty($userType)) {
            $builder->where('us.user_type', 'like', $userType . '%');
        }
       if ($isactive != '') {
            $builder->where('us.is_active', '=', $isactive);
        }
        $builder->orderBy('us.id', 'desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

    public function changePwd(Request $request) {
        $resVal = array();
        // $user = new User;
        $currentuser = Auth::user();
        try {
            $userCollection = User::where('username', "=", $request->input('username'))->get();
            if (!$userCollection->isEmpty()) {                
                $user = $userCollection->first();
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'User Not found';
            return $resVal;
        }


        $currentWorkingDir = getcwd();
        $currentDir = substr($currentWorkingDir, 0, strlen($currentWorkingDir) - 6);
        $envArray = parse_ini_file($currentDir . '.env');
        $sso_server = $envArray['DB_HOST'];
        $sso_db_username = $envArray['SSO_DB_USERNAME'];
        $sso_db_password = $envArray['SSO_DB_PASSWORD'];
        $sso_db_name = $envArray['SSO_DB_DATABASE'];
        $app_user_name = '';


        try {
            $conn = new PDO("mysql:host=$sso_server;dbname=$sso_db_name", $sso_db_username, $sso_db_password);
        } catch (Exception $e) {
            die("Connection failed: " . $conn->connect_error);
        }
        $domain = $_SERVER['SERVER_NAME'];
        $sql = "select * from tbl_user  where username  = '" . $user->username . "'";
        $res = $conn->query($sql);
        $new_pass = $request->input('newPassword');
        foreach ($res as $row) {

            $username = $row['username'];
            $sso_pass = $row['password'];
        }
        if (Hash::check($request->input('password'), $sso_pass)) {
            $user->updated_by = $currentuser->id;
            $user->password = Hash::make($request->input('newPassword'));
            $user->save();
            $resVal['success'] = TRUE;
            $resVal['message'] = 'Password Updated Successfully';
            
            $new_pass = Hash::make($new_pass);
            $date=Date('Y-m-d');
            $sql = "update tbl_user set password ='" . $new_pass . "',password_updated_at ='" . $date . "'  where username ='" . $username . "'";
            $res = $conn->query($sql);
            $domain = $_SERVER['SERVER_NAME'];
        } else {
            $resVal['success'] = false;
            $resVal['message'] = 'Password Not Matched';
        }


        return $resVal;
    }

    //paymentPending 
    public function paymentPending(Request $request) {

        $currentuser = Auth::user();
        $user1 = new User();
        $user1->comp_id = $currentuser->comp_id;
        $user = DB::table('tbl_user as u')
                ->leftjoin('tbl_client as c', 'u.id', '=', 'c.customer_of')
                ->select('u.*')
                ->where('u.comp_id', '=', $user1->comp_id)
                ->distinct();
        //->get();
        $userId = $request->input('userId');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($userId)) {
            $user->where('u.id', '=', $request->input('userId'));
        }
        $resVal['total'] = $user->count('u.id');
        $resVal['list'] = $user->skip($start)->take($limit)->get();
        $i = 0;


        foreach ($resVal['list'] as $category) {
            $category->CategoryList = DB::table('tbl_category as ca')
                    ->leftjoin('tbl_client as c', 'ca.id', '=', 'c.category_id')
                    ->select('ca.id', 'ca.name', DB::raw('sum(c.balance_amount) as amount'))
                    ->where('c.comp_id', '=', $user1->comp_id)
                    ->groupby('ca.id')
                    ->ORDERBY('ca.created_at', 'desc')
                    // ->distinct()
                    ->get();
            // print_r($category);
            $i++;
        }

        return $resVal;
    }

    public function index1() {
        $i = 0;
        foreach ($student as $value) {

            //print_r($value);
            //$value['stutzenID'] = $i;
            $value->stutzenID = DB::table('tbl_course')->get();
            //     $resVal[]=
            $i++;
        }
    }

    public function collections() {
        print 'checking';
        DB::connection()->enableQueryLog();
        var_dump(DB::getQueryLog());
        $builder = User::all();
        DB::connection()->enableQueryLog();
        var_dump(DB::getQueryLog());
        return $builder;
        //  return $this->collection($builder, new UserTransformer());
    }

    public function forgetPwd(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'OTP has sent';


        $validator = Validator::make($request->all(), [
                    'email' => 'required'
        ]);

        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            return $resVal;
        }


        $userCollection = DB::table('users')->where('email', $request->input('email'))->get();


        if (count($userCollection) > 0) {
            $userId = $userCollection->first();
            $userId = $userId->id;

            $user = User::findOrFail($userId);

            $user->otp = rand(100000, 999999);
            $user->update();
            $builder = DB::table('users')
                    ->where('email', '=', $request->input('email'))
                    ->select('email', 'otp');
            $resVal['data'] = $builder->first();
        } else {

            $resVal['message'] = 'email is not valid';
        }

        return $resVal;
    }

    public function verifyOtp(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'OTP has sent';


        $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'otp' => 'required'
        ]);

        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            return $resVal;
        }


        $userCollection = DB::table('users')->where('email', '=', $request->input('email'))
                ->where('otp', '=', $request->input('otp'))
                ->get();

        if (count($userCollection) > 0) {
            $userId = $userCollection->first();

            $builder = DB::table('users')
                    ->where('email', '=', $request->input('email'))
                    ->where('otp', '=', $request->input('otp'))
                    ->select('email', 'otp');
            $resVal['data'] = $builder->first();
        } else {
            $resVal['success'] = FALSE;

            $resVal['message'] = 'Otp is not valid';
            return $resVal;
        }
        return $resVal;
    }

    public function updatePwd(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Password Reset Successfully';


        $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'otp' => 'required'
        ]);

        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            return $resVal;
        }


        $userCollection = DB::table('users')->where('email', '=', $request->input('email'))
                ->where('otp', '=', $request->input('otp'))
                ->get();

        if (count($userCollection) > 0) {
            $userId = $userCollection->first();

            $userId = $userId->id;

            $user = User::findOrFail($userId);

            $user->password = Hash::make($request->input('newPassword'));
            $user->update();
        } else {
            $resVal['success'] = FALSE;

            $resVal['message'] = 'Password Reset  Not Successfully';
            return $resVal;
        }
        return $resVal;
    }

    //LIST ALL
    public function businessList(Request $request) {

        $resVal = SSOHelper::businessList();

        return $resVal;
    }
    
    // update latitude and longitude
     public function updateLocation(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'User Location Details Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'User Not found';
            return $resVal;
        }
        
        $currentuser = Auth::user();
        $user->updated_by = $currentuser->id;
        $user->ip_address=$request->ip_address;
        $user->fill($request->all());
        $user->save();
     
//        function getRealIpAddr()
//      {
//      if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
//      {
//      $ip_address=$_SERVER['HTTP_CLIENT_IP'];
//      }
//      elseif (!empty($_SERVER['HTpTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
//      {
//       $ip_address=$_SERVER['HTTP_X_FORWARDED_FOR'];
//      }
//     else
//      {
//      $ip_address=$_SERVER['REMOTE_ADDR'];
//      }
//      echo "IP ADD $ip_address";
//     return $ip_address;
//      }
//       //$ip_address = $_SERVER['HTTP_CLIENT_IP']?:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?:$_SERVER['REMOTE_ADDR']);     
//       //echo "IP ADDress $ip_address";
//       $user->ip_address=@$_SERVER['HTTP_CLIENT_IP'];
//      // print_r(@$_SERVER['HTTP_CLIENT_IP']);
//    // print_r($user->ip_address);
//       $user->save();
      return $resVal;
    }
}
