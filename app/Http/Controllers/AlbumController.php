<?php

namespace App\Http\Controllers;

use DB;
Use App\Customer;
use App\User;
use App\Album;
Use Illuminate\Http\Request;
use App\Transformer\CustomerTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\AlbumViewingHistoryLog;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomerController
 *
 * @author Deepa
 */
class AlbumController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Album Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //  $code = genRandomString();

        $a = $b = '';

        for ($i = 0; $i < 3; $i++) {
            $a .= chr(mt_rand(65, 89)); // see the ascii table why 65 to 89.    
            $b .= mt_rand(0, 9);
        }

        $code = $a . $b;
        $currentuser = Auth::user();
        $album = new Album;
        $album->created_by = $currentuser->id;
        $album->updated_by = $currentuser->id;
        $album->fill($request->all());
        $album->code = $code;


        $album->save();
        $resVal['id'] = $album->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');

        $code = $request->input('code');
        $type = $request->input('type');
        $mode = $request->input('mode');
        $cutomer_id = $request->input('cutomer_id');
        $is_active = $request->input('is_active');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_album')
                ->select('*')
                ->where('is_active', '=', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($code)) {
            $builder->where('code', '=', $code);
        }
        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }

        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }

        if (!empty($cutomer_id)) {
            $builder->where('cutomer_id', 'like', '%' . $cutomer_id . '%');
        }
        if (!empty($is_active)) {
            $builder->where('is_active', '=', is_active);
        }
        if ($mode == 1 || $mode == '')
            $builder->orderBy('id', 'desc');
        else if ($mode == 2)
            $builder->orderBy('name', 'asc');


        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $albumCollection = $builder->get();
        } else {

            $albumCollection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($albumCollection as $album) {
              $album->asset=array();
              $id=$album->id;
              $albumAsset = DB::table('tbl_album_asset')->where('album_id',$id)->where('is_active', 1)->take(3)->get();
              $album->asset=$albumAsset;             
          }
        $resVal['list'] = $albumCollection;
        //$resVal['list'] = $builder->paginate(10);
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Album Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $album = Album::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Customer Not found';
            return $resVal;
        }
        $album->fill($request->all());

        $album->updated_by = $currentuser->id;
        $album->save();

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Album Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $album = Album::findOrFail($id);
            if ($album->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Receipt Not found';
            return $resVal;
        }
        $album->is_active = 0;
        $album->updated_by = $currentuser->id;
        //$invoice->fill($request->all());
        $album->update();
        return $resVal;
    }

    public function detail(Request $request) {

        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $id = $request->input('id');
        $code = $request->input('code');
        //$status = $request->input('status', '');
        $builder = DB::table('tbl_album')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($code)) {
            $builder->where('code', '=', $code);
        }
        $builder->where('is_active', 1);
        $builder->orderBy('id', 'desc');
        $albumCollection = $builder->get();
                
        if (count($albumCollection) > 0) {
            
            $album = $albumCollection->first();
            $album->asset =  array();
            $albumAsset = DB::table('tbl_album_asset')->where('album_id', $id)->where('is_active', 1)->orderBy('seq_no', 'asc');
            
            $album->asset = $albumAsset->get();
            $resVal['data'] = $album;
        }
        else 
        {
            $resVal['data'] = null;
        }
        return ($resVal);
    }

    public function permissionCheck(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //  $code = genRandomString();
        $code = $request->input('album_code');
        $customer = $request->input('customername');
        $password = $request->input('password');

        $builder = DB::table('tbl_album as a')
                        ->leftJoin('tbl_album_permission as c', 'a.id', '=', 'c.album_id')
                        ->select('a.*', 'c.customer_id')
                        ->where('a.is_active', '=', 1)
                        ->where('c.album_code', '=', $code)->get();
        if ($builder->count() <= 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'No Permission';
            return $resVal;
        }
        foreach ($builder as $album_det) {
            $type = $album_det->type;
            $album_id = $album_det->id;
            $customer_id = $album_det->customer_id;
            $builder1 = DB::table('tbl_customer')
                            ->select('*')
                            ->Orwhere('fname', '=', $customer)
                            ->Orwhere('phone', '=', $customer)
                            ->Orwhere('email', '=', $customer)
                            ->where('is_active', '=', 1)->get();
            if ($builder1->count() <= 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'No Permission';
                return $resVal;
            }
            foreach ($builder1 as $cus) {
                $phone = $cus->phone;
                $email = $cus->email;

                if ($type == 'private') {
                    $cus_pass = $cus->password;
                    if (Hash::check($password, $cus->password)) {
                        
                    } else {
                        $resVal['success'] = FALSE;
                        $resVal['message'] = 'No Permission';
                        return $resVal;
                    }
                }
            }
        }
        $currentuser = Auth::user();
        $albumhist = new AlbumViewingHistoryLog;
        $albumhist->album_id = $album_id;
        $albumhist->album_code = $code;
        $albumhist->email = $email;
        $albumhist->phoneno = $phone;
        $albumhist->customer_id = $customer_id;
        $albumhist->ip = "ip";
        $albumhist->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $albumhist->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $albumhist->created_by = $currentuser->id;
        $albumhist->is_active = 1;
        $albumhist->updated_by = $currentuser->id;
        $albumhist->save();
        $resVal['id'] = $albumhist->id;

        return $resVal;
    }

}

?>
