<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\City;
use Illuminate\Http\Request;
use App\Transformer\CityTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CityController
 *
 * @author Deepa
 */
class CityController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'City Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $validator = Validator::make($request->all(), [
                    'code' => 'required|unique:tbl_city,code'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            if (array_key_exists('code', $validator->failed())) {
                $resVal['message'] = 'City Code is already exist';
            }

            return $resVal;
        }


        $city = new City;
        $city->created_by = $currentuser->id;
        $city->updated_by = $currentuser->id;
        $city->is_active = $request->input('is_active', 1);

        $city->fill($request->all());
        $city->save();

        $resVal['id'] = $city->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $state_id = $request->input('state_id', '');
        $country_name = $request->input('country_name','');
        $state_name = $request->input('state_name','');
        $country_id = $request->input('country_id', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_city')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {            
            $builder->where('id', '=', $id);
        }
        if ($state_id != '') {
            $builder->where('state_id', '=', $state_id);
        }
        if ($country_id != '') {
            $builder->where('country_id', '=', $country_id);
        }
        if (!empty($name)) {
            $builder->where('name',  'like', '%' . $name . '%');
        }
        
        if (!empty($country_name)) {
            $builder->where('country_name', '=' , $country_name );
        }
        if (!empty($state_name)) {
            $builder->where('state_name', '=' , $state_name );
        }
        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $cityCollection = $builder->get();
        } else {
            $cityCollection = $builder->skip($start)->take($limit)->get();
        }
        
        $resVal['list'] = $cityCollection;
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'City Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $city = City::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'City Not found';
            return $resVal;
        }
        $city->created_by = $currentuser->id;
        $city->fill($request->all());
        $city->save();

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'City Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $city = City::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'City Not found';
            return $resVal;
        }

        $city->delete();

        return $resVal;
    }

}

?>
