<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AttendanceController
 *
 * @author Stutzen Admin
 */

namespace App\Http\Controllers;

use DB;
use App\Attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use DateTime;

class AttendanceController extends Controller {

    public function punchIn(Request $request) {
        $resVal = array();
        $resVal['message'] = ' Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        AttendanceController :: SaveDetails($request, 0);
        // $resVal['id'] = $a->id;
        return $resVal;
    }

    public function SaveDetails($at, $i) {
        $currentuser = Auth::user();
        if ($i == 0) {
            $at->user_id = $at->user_id;
        } else {
            $at->user_id = $currentuser->id;
        }
        $emp_id=DB::table('tbl_employee')->where('user_id','=',$at->user_id )->first();
        $now = new DateTime();
        $format = $now->format('Y-m-d H:i:s');    // MySQL datetime format
        $currentTimeStamp = $now->format('Y-m-d H:i:s');
        DB::connection()->enableQueryLog();
        $user = DB::table('tbl_attendance')
                        ->select('id', 'start_time')->where('user_id', '=', $at->user_id)->where('end_time', null)->get()->first();
        if ($currentTimeStamp != null && $user != null) {
            try {
                $att = Attendance::findOrFail($user->id);
            } catch (ModelNotFoundException $e) {

                $resVal['success'] = FALSE;
                $resVal['message'] = ' Details  Not found';
                return $resVal;
            }

            $att->end_geo_text = $at->end_geo_text;
            $att->end_img_url = $at->end_img_url;
            $att->end_time = $currentTimeStamp;
            if ($at->lat != null || $at->lat != '' && $at->lng != null || $at->lng != '') {
                $address = AttendanceController :: getaddress($at->lat, $at->lng);
                $att->end_location = $at->lat.','.$at->lng;
            }
            $neweventDateStr = new DateTime($user->start_time);
            $neweventDateEnd = new DateTime($currentTimeStamp);
            $diff = $neweventDateStr->diff($neweventDateEnd);
            $hr = $diff->h;
            $min = $diff->i;
            $sec = $diff->s;
            if(!empty($emp_id)){
                $att->emp_id=$emp_id->id;
            }
            $att->duration = "$hr:$min:$sec";
            $att->updated_by = $currentuser->id;
            $att->is_active = 1;
//            $att->fill($at->all());
            $att->save();
        } else {
            $atd = new Attendance;
            if ($at->lat != null && $at->lng != null) {
                $address = AttendanceController :: getaddress($at->lat, $at->lng);
                $atd->start_location = $at->lat.','.$at->lng;
            }
            if(!empty($emp_id)){
                $atd->emp_id=$emp_id->id;
            }
            $atd->user_id = $at->user_id;
            $atd->fill($at->all());
            $atd->start_time = $currentTimeStamp;
            $atd->created_by = $currentuser->id;
            $atd->updated_by = $currentuser->id;
            $atd->is_active = 1;
            $atd->save();
        }
    }

    public function punchInMe(Request $request) {
        $resVal = array();
        $resVal['message'] = ' Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        AttendanceController :: SaveDetails($request, 1);
        // $resVal['id'] = $a->id;
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $user_id = $request->input('user_id', '');
        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $is_active = $request->input('is_active', '');
        $emp_id=$request->input('emp_id');
        $ref_id=$request->input('ref_id');
        $ref_type=$request->input('ref_type');
        $currentuser = Auth::user();

        $builder = DB::table('tbl_attendance as a')
                ->leftjoin('tbl_employee as e','e.id','=','a.emp_id')
                ->select('a.*','e.f_name as employee_name');
        
        
         $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        
        $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
         if (!$isVisible) {
            $builder->where('a.user_id', '=', $curr_id);
         }
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//             $builder->where('a.user_id', '=', $curr_id);
//        }
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('a.id', '=', $id);
        }
        if (!empty($emp_id)) {
            $builder->where('a.emp_id', '=', $emp_id);
        }
        if (!empty($ref_id)) {
            $builder->where('a.ref_id', '=', $ref_id);
        }
        if (!empty($ref_type)) {
            $builder->where('a.ref_type', '=', $ref_type);
        }
        if (!empty($user_id)) {
            $builder->where('a.user_id', '=', $user_id);
        }
        if ($is_active != '') {
            $builder->where('a.is_active', '=', $is_active);
        }

        if (!empty($from_date)) {
            $builder->whereDate('a.start_time', '>=', $from_date);
        }

        if (!empty($to_date)) {
            $builder->whereDate('a.start_time', '<=', $to_date);
        }

        $builder->orderBy('a.id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function currentStatus(Request $request) {
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $user_id = $request->input('user_id');
        $is_active = $request->input('is_active', '');
        $emp_id=$request->input('emp_id');
        $ref_id=$request->input('ref_id');

        $builder = DB::table('tbl_attendance')
                        ->select('*')->where('end_time', null);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($user_id)) {
            $builder->where('user_id', '=', $user_id);
        } else {
            $builder->where('user_id', '=', $currentuser->id);
        }
        if ($is_active != '') {
            $builder->where('is_active', 'like', '%' . $is_active . '%');
        }
         if (!empty($emp_id)) {
            $builder->where('emp_id', '=', $emp_id);
        }
        if (!empty($ref_id)) {
            $builder->where('ref_id', '=', $ref_id);
        }


        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    function getaddress($lat, $lng) {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
        $json = @file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        if ($status == "OK") {
            return $data->results[0]->formatted_address;
        } else {
            return false;
        }
    }

    public function attendancedetails(Request $request) {
        $resVal = array();
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $user_id = $request->input('user_id', '');
        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $emp_id=$request->input('emp_id');
        $ref_id=$request->input('ref_id');



        if ((empty($from_date)) && (empty($to_date))) {
            $from_date = date('Y-m-d');
            $to_date = date('Y-m-d');
        }

        // print_r($from_date);
        //print_r($to_date);

        $bulider = DB::table('tbl_attendance')
                ->select('id', 'user_id','emp_id','ref_id','start_time','end_time', 'start_location', 'end_location', 'start_geo_text', 'end_geo_text', 'start_img_url', 'end_img_url', 'created_by', 'updated_by', 'created_at', 'updated_at', 'is_active', 'comments', DB::raw('SEC_TO_TIME( SUM(time_to_sec(duration))) As totalDuration'))
                ->groupby('user_id');


        if (!empty($user_id)) {
            $bulider->where('user_id', $user_id);
        }
        if (!empty($from_date)) {

            $bulider->whereDate('start_time', '>=', $from_date);
        }
        if (!empty($to_date)) {

            $bulider->whereDate('start_time', '<=', $to_date);
        }
         if (!empty($emp_id)) {
            $bulider->where('emp_id', '=', $emp_id);
        }
        if (!empty($ref_id)) {
            $bulider->where('ref_id', '=', $ref_id);
        }

        $resVal['list'] = $bulider->get();
        return $resVal;
    }

    public function attendanceSave(Request $request) {
        $resVal = array();
        $resVal['message'] = ' Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $days=0;
        $currentuser = Auth::user();
        $attendance = new Attendance;
        $attendance->updated_by = $currentuser->id;
        $attendance->created_by = $currentuser->id;
        $attendance->fill($request->all());
        $attendance->is_active = 1;
        $lat=$request->input('lat');
         $lng=$request->input('lng');
         if ($lat != null || $lat != '' && $lng != null || $lng != '') {
                $address = AttendanceController :: getaddress($lat, $lng);
                $attendance->start_location = $lat.','.$lng;
            }
            $neweventDateStr = new DateTime($request->input('start_time'));
            $neweventDateEnd = new DateTime($request->input('end_time'));
            $diff = $neweventDateStr->diff($neweventDateEnd); 
           if($diff->d > 0){
               $days=($diff->d*24);
           }
            $hr = $diff->h + $days;
            $min = $diff->i;
            $sec = $diff->s;
            $attendance->duration = "$hr:$min:$sec";
        $attendance->save();

        $resVal['id'] = $attendance->id;

        return $resVal;
    }

    public function totalWorkingReport(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $emp_id = $request->input('emp_id', '');
        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $show_all = $request->input('show_all', '');
        $team_id=$request->input('team_id');

        $builder = DB::table('tbl_attendance as a')
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'a.emp_id')
                ->join('tbl_emp_team as et','e.team_id','=','et.id')
                ->select('a.id','a.emp_id', 'e.f_name', 'e.l_name', DB::raw("sec_to_time(sum(time_to_sec(a.duration))) as duration"),'et.name as team_name')
                ->whereDate('start_time', '>=', $from_date)
                ->whereDate('start_time', '<=', $to_date)
                ->groupBy('a.emp_id');


        if (!empty($emp_id)) {
            $builder->where('a.emp_id', '=', $emp_id);
        }
         if (!empty($team_id)) {
            $builder->where('e.team_id', '=', $team_id);
        }

        $collection = $builder->get();

        if ($show_all == 1) {
            foreach ($collection as $a) {

                $emp_id1 = $a->emp_id;
                
                $builder1 = DB::table('tbl_attendance')
                        ->select('id','start_time','end_time','duration')
                        ->whereDate('start_time', '>=', $from_date)
                        ->whereDate('start_time', '<=', $to_date)
                        ->where('emp_id', '=', $emp_id1)
                        ->get();
                $a->data = $builder1;
            }
        }

        $resVal['list'] = $collection;

        return ($resVal);
    }

}
