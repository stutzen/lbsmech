<?php
namespace App\Http\Controllers;
use DB;
use Validator;
use App\Company;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
class CompanyController extends Controller
{
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Company Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

//        $validator = Validator::make($request->all(), [
//                    'website' => 'required|unique:tbl_company,website'
//        ]);
//        if ($validator->fails()) {
//            $resVal['success'] = FALSE;
//            $resVal['message'] = 'Error in request format';
//            if (array_key_exists('website', $validator->failed())) {
//                $resVal['message'] = 'Company website is already exist';
//            }
//
//            return $resVal;
//        }
        $currentuser=Auth::user();
        $company=new Company;      
        $company->updated_by=$currentuser->id;
        $company->created_by=$currentuser->id;
        $company->fill($request->all());
        $company->is_active=1;
        $company->save();
       
        $resVal['id'] = $company->id;

        return $resVal;
    }
      public function update(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Company Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $company = Company::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Company Not found';
            return $resVal;
        }
        $company->updated_by=$currentuser->id;
        $company->is_active=1;
        $company->fill($request->all());
        $company->save();

        return $resVal;
    }
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active=$request->input('is_active');
        $city=$request->input('city_id');
        $country=$request->input('country_id');
        $district=$request->input('district');
        $state=$request->input('state_id');
        $builder = DB::table('tbl_company as c')
                ->leftjoin('tbl_country as ct','c.country_id','=','ct.id')
                ->leftjoin('tbl_state as s','s.id','=','c.state_id')
                ->leftjoin('tbl_city as ci','ci.id','=','c.city_id')
                ->select('c.*','ct.name as country_name','s.name as state_name','ci.name as city_name');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        if (!empty($id)) {
            $builder->where('c.id', '=', $id);
        }
        if($is_active != ''){
            $builder->where('c.is_active','=',1);
        }
        if (!empty($city)) {
            $builder->where('c.city_id','=', $city );
        }
        if (!empty($country)) {
            $builder->where('c.country_id', '=', $country );
        }
        if (!empty($district)) {
            $builder->where('c.district', 'like', '%' . $district . '%');
        }
        if (!empty($state)) {
            $builder->where('c.state_id', '=',$state );
        }
 
        if (!empty($name)) {
            $builder->where('c.name', 'like', '%' . $name . '%');
        }
        $builder->orderBy('c.name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $Collection = $builder->get();
        } else {

            $Collection = $builder->skip($start)->take($limit)->get();
        }
        
        $resVal['list'] = $Collection;
        return ($resVal);
    }
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Company Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $company = Company::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Company Not found';
            return $resVal;
        }
        $currentuser = Auth::user();
        $company->is_active=0;
        $company->updated_by=$currentuser->id;
        $company->save();
        return $resVal;
    }
}
?>
