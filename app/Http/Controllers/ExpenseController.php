<?php
namespace App\Http\Controllers;
use DB;
use Carbon\Carbon;
use App\Expense;
use App\ExpensePayment;
use App\PurchasePayment;
use App\Transaction;
use App\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExpenseController
 *
 * @author Deepa
 */
class ExpenseController extends Controller {
    //put your code here
     public function save(Request $request){
         $resVal = array();
        $resVal['message'] = 'Expense Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $expense = new Expense;
        
        $expense->created_by = $currentuser->id;
        $expense->updated_by = $currentuser->id; 
        
        $expense->fill($request->all());
        $expense->save();
        $expensePaymentItem = ($request->input('payment'));
       if($expensePaymentItem != NULL){

        foreach ($expensePaymentItem as $item) {
            $expensePayment = new ExpensePayment;
           
            $expensePayment->fill($item);
            $expensePayment->expense_id = $expense->id;
             
             $expensePayment->created_by = $currentuser->id;
            $expensePayment->updated_by = $currentuser->id; 
            $expensePayment->save();
 
            }
       }
      
         $attachement = ($request->input('media'));
        if($attachement != NULL){
         foreach ($attachement as $std) {

                $attachement = new Attachment;
                $attachement->is_active =  $request->input('isactive', 1);
               
                $attachement->created_by = $currentuser->ename;
                $attachement->updated_by = $currentuser->ename;
                $attachement->fill($std);

                $attachement->save();
           
        }
        }
        $cus_det = $expense->customer_id;
        $builder = DB::table('tbl_customer')
                ->select('*')
                ->where('id','=',$cus_det)->first();        
        $cus_name = $builder->fname ." ".$builder->lname;
        
        $builder = DB::table('tbl_user')
                ->select('*')
                ->where('id','=',$currentuser->id)->first();        
        $user_name = $builder->username;
        
        
        $mydate = $expense->payment_date;
       $month = date("m",strtotime($mydate));
       $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code',"=",$month )->get(); 
       $fiscalMonth = $fiscalMonthCollection->first();
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();
        $fiscalYear = $fiscalYearCollection->first();
        $transaction = new Transaction;
        $transaction->fiscal_year_id = $fiscalYear->id;
        $transaction->fiscal_year_code = $fiscalYear->code;
        $transaction->transaction_date = $expense->payment_date;
        $transaction->voucher_type = PurchasePayment::$SALES;
        $transaction->voucher_number = $expense->id;
        $transaction->credit =0.00;
        $transaction->debit = $expense->total_amount;
         $transaction->payment_mode = $expense->payment_type;
        $transaction->account_category =$expense->tra_category;
        $transaction->customer_id = $expense->customer_id;
        $transaction->account =  PurchasePayment::$PURCHASEAccount;
        $transaction->account_id =  PurchasePayment::$PURCHASEAccountID; 
        $transaction->particulars = '#'.$expense->id.'Expense invoice Payment has  Received from '.$cus_name.' by '.$user_name;        
        $transaction->fiscal_month_code = $fiscalMonth->id;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name.' '.$currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name.' '.$currentuser->l_name;
        $transaction->fill($request->all());
        $transaction->save();
         $activitylogo = new ActivityLog;
        
        $activitylogo->date = Carbon::now();
        $activitylogo->type= PurchasePayment::$TYPEPURCHASESAVE;
        $activitylogo->description=PurchasePayment::$TYPEPURCHASESAVE.':'.$expense->id.' ' .$expense->total_amount;
        $activitylogo->userid=$currentuser->id;
        $activitylogo->is_active= $request->input('is_active','1');
       $activitylogo->created_by = $currentuser->id;
        $activitylogo->updated_by = $currentuser->id;
       $activitylogo->created_by_name = $currentuser->f_name.' '.$currentuser->l_name;
        $activitylogo->updated_by_name = $currentuser->f_name.' '.$currentuser->l_name;
        $activitylogo->fill($request->all());
        $activitylogo->save();
        $resVal['id'] = $expense->id;
        
        return $resVal;

    
    }
    
    
      public function update(Request $request ,$id){
         $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Expense Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $expense =Expense::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Expense Not found';
            return $resVal;
        }
        $builder = DB::table('tbl_expense_payment')->where('expense_id','=', $id)->delete();

        $expensePaymentItem = ($request->input('payment'));
       if($expensePaymentItem != NULL){

        foreach ($expensePaymentItem as $item) {
            $expensePayment = new ExpensePayment;
           
            $expensePayment->fill($item);
            $expensePayment->expense_id = $expense->id;
             
             $expensePayment->created_by = $currentuser->id;
            $expensePayment->updated_by = $currentuser->id; 
            $expensePayment->save();
 
            }
       }
        $expense->fill($request->all());
        $expense->created_by = $currentuser->id;
        $expense->updated_by = $currentuser->id;
        $expense->save();
        $resVal['id'] = $expense->id;
        return $resVal;
    }
    
    
   public function listAll(Request $request ){
       $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $id = $request->input('id');
         $fname = $request->input('fname', '');
         $customerid = $request->input('customer_id');
         $taxid = $request->input('tax_id');
         $builder = DB::table('tbl_expense as e')
                ->leftJoin('tbl_customer as c', 'e.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'e.tax_id', '=', 't.id')
                ->leftJoin('tbl_category as ca', 'e.category_id', '=', 'ca.id')
                ->select('e.*',  'ca.name','c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname');

        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }
        if (!empty($fname)) {
            $builder->where('c.fname', 'like', '%' . $fname . '%');
        }
         if (!empty($customerid)) {
            $builder->where('e.customer_id', '=', $customerid);
        }
        if (!empty($taxid)) {
            $builder->where('e.tax_id', '=', $taxid);
        }

        
        $builder->orderBy('e.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        
        
        // $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }
    
     public function detail(Request $request ){
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $id = $request->input('id');

        
       $builder = DB::table('tbl_expense as e')
                ->leftJoin('tbl_customer as c', 'e.customer_id', '=', 'c.id')
                ->leftJoin('tbl_tax as t', 'e.tax_id', '=', 't.id')
                ->leftJoin('tbl_category as ca', 'e.category_id', '=', 'ca.id')
                ->select('e.*', 'ca.name','c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname')
                ->where('e.id', '=', $id);

        $expansePayment = DB::table('tbl_expense_payment as ep')->where('expense_id', $id);
        $media = DB::table('tbl_media as ep')->where('media_type_id', $id);
        $resVal['success'] = TRUE;
        
        if (!empty($id)) {
            $builder->where('e.id', '=', $id);
        }

       
        $builder->orderBy('e.id', 'desc');

        $resVal['total'] = $builder->count();
        $expanseCollection = $builder->get();
        if (count($expanseCollection) == 1) {

            $expanse = $expanseCollection->first();

            $expansePaymentCollection = $expanse->payment = $expansePayment->get();
            
            if(count($expansePaymentCollection) == 1)
                $expansepayment = $expansePaymentCollection->first();
                $expanse->media =$media->get();
            $resVal['data'] = $expanse;
        }
       
        return ($resVal);
    }
    
    
    
}

?>
