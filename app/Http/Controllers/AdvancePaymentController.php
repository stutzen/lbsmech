<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\AdvancePayment;
use App\Payment;
use App\Transaction;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Http\Controllers\Carbon;
use App\Helper\AccountHelper;
use App\Helper\GeneralHelper;
use App\Helper\SalesInvoiceHelper;
use App\Helper\TransactionHelper;

 class AdvancePaymentController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Advance Payment Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $advance_payment = new AdvancePayment;
        $advance_payment->created_by = $currentuser->id;
        $advance_payment->updated_by = $currentuser->id;
        $advance_payment->fill($request->all());
        $advance_payment->save();

        //This is for save payment when vouher type is invoice_payment
        //Transaction save
        $transaction = new Transaction;
        $mydate = $advance_payment->date;
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }
        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();
        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }
        $transaction->transaction_date = $advance_payment->date;
        $transaction->voucher_type = $advance_payment->voucher_type;
        $transaction->voucher_number = $advance_payment->id;
        $contraTrans = new Transaction;
        $contraTrans = clone $transaction;

        $voch_type = $advance_payment->voucher_type;
        if ($voch_type == 'creditNote' || $voch_type == 'debitNote') {

            $customer_id = $request->input('customer_id', '');
            $account_id = GeneralHelper::getCustomerAccountId($customer_id);
            $account = GeneralHelper::getCustomerAccount($account_id);


            $advance_payment->account_id = $account_id;
            $advance_payment->account = $account;
            $advance_payment->save();

            $transaction->account_id = $account_id;
            $transaction->account = $account;
            $transaction->pmtcode = GeneralHelper::getPaymentAcode($account_id);
            $transaction->pmtcoderef = $account_id;
            $transaction->voucher_number = $advance_payment->id;
        } else {
            $transaction->account = $request->input('account', '');
            $transaction->account_id = $request->input('account_id', '');
            $transaction->account = $request->input('account', '');
            $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
            $transaction->pmtcoderef = $request->input('account_id', '');
        }

        if ($advance_payment->voucher_type == 'advance_invoice') {
            $transaction->credit = $advance_payment->amount;
            $transaction->debit = 0.00;
            $transaction->particulars = 'Advance Payment amount is received ';
        }
        // Debit Note and credit Note have contra entry
        else if ($advance_payment->voucher_type == 'debitNote') { // sales Invoice
            $transaction->debit = 0.00 ;
            $transaction->credit = $advance_payment->amount;
            $transaction->particulars = 'Debit Note received ';
            AccountHelper::deposit($account_id, $advance_payment->amount);

            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'debitNote');
            $contra_account = GeneralHelper::getCustomerAccount($contra_account_id);
            $contraTrans->account_id = $contra_account_id;
            $contraTrans->account = $contra_account;
            $contraTrans->pmtcode = GeneralHelper::getPaymentAcode($contra_account_id);
            $contraTrans->pmtcoderef = $contra_account_id;
            $contraTrans->voucher_number = $advance_payment->id;

            $contraTrans->credit = 0.00;
            $contraTrans->debit = $advance_payment->amount ;
            $contraTrans->particulars = 'Debit Note ContraEntry ';
            AccountHelper::withDraw($contra_account_id, $advance_payment->amount);

            AdvancePaymentController::transactionSave($advance_payment, $contraTrans, $currentuser);
        } else if ($advance_payment->voucher_type == 'creditNote') { // purchase Invoice
            $transaction->credit =0.00;
            $transaction->debit = $advance_payment->amount;
            $transaction->particulars = 'Credit Note received';
            AccountHelper::withDraw($account_id, $advance_payment->amount);

            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'creditNote');
            $contra_account = GeneralHelper::getCustomerAccount($contra_account_id);
            $contraTrans->account_id = $contra_account_id;
            $contraTrans->account = $contra_account;
            $contraTrans->pmtcode = GeneralHelper::getPaymentAcode($contra_account_id);
            $contraTrans->pmtcoderef = $contra_account_id;
            $contraTrans->debit = 0.00;
            $contraTrans->credit = $advance_payment->amount;
            $contraTrans->particulars = 'Credit Note ContraEntry';
            $contraTrans->voucher_number = $advance_payment->id;

            AccountHelper::deposit($contra_account_id, $advance_payment->amount);

            AdvancePaymentController::transactionSave($advance_payment, $contraTrans, $currentuser);
        } else {
            $transaction->credit = 0.00;
            $transaction->debit = $advance_payment->amount;
        }
        AdvancePaymentController::transactionSave($advance_payment, $transaction, $currentuser);


        if ($advance_payment->voucher_type == 'invoice_payment') { // sales Invoice
            $payment = new Payment;
            $payment->date = $advance_payment->date;
            $payment->comments = "Invoice Payment";
            $payment->amount = $advance_payment->amount;
            $payment->tra_category = $advance_payment->tra_category;
            $payment->customer_id = $advance_payment->customer_id;
            $payment->account = $advance_payment->account;
            $payment->account_id = $advance_payment->account_id;
            $payment->payment_mode = $advance_payment->payment_mode;
            $payment->invoice_id = $advance_payment->voucher_no;
            $payment->cheque_no = $advance_payment->reference_no;
            $payment->adv_payment_id = $advance_payment->id;
            $payment->is_active = 1;
            $payment->created_by = $currentuser->id;
            $payment->updated_by = $currentuser->id;
            $payment->save();
            SalesInvoiceHelper::updateInvoiceStatus($advance_payment->voucher_no);
            //Update Sales Order status 
            $get_sales_id = DB::table('tbl_invoice')->where('is_active', 1)
                            ->where('id', '=', $advance_payment->voucher_no)->first();
            if (!empty($get_sales_id->quote_id)) {
                SalesInvoiceHelper::updateSalesOrderStatus($get_sales_id->quote_id);
            }
        }



        if ($advance_payment->voucher_type == 'advance_invoice') {

            //Update Account Balance
            AccountHelper::deposit($advance_payment->account_id, $advance_payment->amount);
            //Update advance amount balance amount
            SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($advance_payment->id);
        } if ($advance_payment->voucher_type == 'advance_purchase') {
            //Update Account Balance
            AccountHelper::withDraw($advance_payment->account_id, $advance_payment->amount);
            //Update advance amount balance amount
            SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($advance_payment->id);
        }

        $resVal['id'] = $advance_payment->id;
         return $resVal;
    }

    public function creditDebtUpdate(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = 'Advance Payment Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $advance_payment = AdvancePayment::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Advance Payment Not found';
            return $resVal;
        }

        //This is for save payment when vouher type is invoice_payment
        //Transaction save

        $voch_type = $advance_payment->voucher_type;
        $adv_acc_id = $advance_payment->account_id;
        $adv_amount = $advance_payment->amount;

        if ($voch_type == 'creditNote') {
            AccountHelper::deposit($adv_acc_id, $adv_amount);
            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'creditNote');
            AccountHelper::withDraw($contra_account_id, $advance_payment->amount);
        }
        if ($voch_type == 'debitNote') {
            AccountHelper::withDraw($adv_acc_id, $adv_amount);
            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'debitNote');
            AccountHelper::deposit($contra_account_id, $advance_payment->amount);
        }

        $builder = DB::table('tbl_transaction')->where('voucher_number', $id)
                ->where('voucher_type', $voch_type)->delete();


        $advance_payment->updated_by = $currentuser->id;
        $advance_payment->fill($request->all());
        $advance_payment->save();
        //Transaction save
        $transaction = new Transaction;
        $mydate = $advance_payment->date;
        $month = date("m", strtotime($mydate));
        $transaction->fiscal_month_code = $month;

        $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();
        if ($fiscalMonthCollection->count() > 0) {
            $fiscalMonth = $fiscalMonthCollection->first();
            $transaction->fiscal_month_code = $fiscalMonth->id;
        }
        $year = date("Y", strtotime($mydate));
        $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();
        $transaction->fiscal_year_id = 0;
        $transaction->fiscal_year_code = $year;
        if ($fiscalYearCollection->count() > 0) {
            $fiscalYear = $fiscalYearCollection->first();
            $transaction->fiscal_year_id = $fiscalYear->id;
            $transaction->fiscal_year_code = $fiscalYear->code;
        }
        $transaction->transaction_date = $advance_payment->date;
        $transaction->voucher_type = $advance_payment->voucher_type;
        $transaction->voucher_number = $advance_payment->id;
        $contraTrans = new Transaction;
        $contraTrans = clone $transaction;

        $voch_type = $advance_payment->voucher_type;
        if ($voch_type == 'creditNote' || $voch_type == 'debitNote') {

            $customer_id = $request->input('customer_id', '');
            $account_id = GeneralHelper::getCustomerAccountId($customer_id);
            $account = GeneralHelper::getCustomerAccount($account_id);


            $advance_payment->account_id = $account_id;
            $advance_payment->account = $account;
            $advance_payment->save();

            $transaction->account_id = $account_id;
            $transaction->account = $account;
            $transaction->pmtcode = GeneralHelper::getPaymentAcode($account_id);
            $transaction->pmtcoderef = $account_id;
        } else {
            $transaction->account = $request->input('account', '');
            $transaction->account_id = $request->input('account_id', '');
            $transaction->account = $request->input('account', '');
            $transaction->pmtcode = GeneralHelper::getPaymentAcode($request->input('account_id', ''));
            $transaction->pmtcoderef = $request->input('account_id', '');
        }

        if ($advance_payment->voucher_type == 'advance_invoice') {
            $transaction->credit = $advance_payment->amount;
            $transaction->debit = 0.00;
            $transaction->particulars = 'Advance Payment amount is received ';
        }
        // Debit Note and credit Note have contra entry
        else if ($advance_payment->voucher_type == 'debitNote') { // sales Invoice 
            $transaction->debit = 0.00;
            $transaction->credit = $advance_payment->amount;
            $transaction->particulars = 'Debit Note received ';
            AccountHelper::deposit($account_id, $advance_payment->amount);

            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'debitNote');
            $contra_account = GeneralHelper::getCustomerAccount($contra_account_id);
            $contraTrans->account_id = $contra_account_id;
            $contraTrans->account = $contra_account;
            $contraTrans->pmtcode = GeneralHelper::getPaymentAcode($contra_account_id);
            $contraTrans->pmtcoderef = $contra_account_id;

            $contraTrans->credit = 0.00;
            $contraTrans->debit =$advance_payment->amount ;
            $contraTrans->particulars = 'Debit Note ContraEntry ';
            AccountHelper::withDraw($contra_account_id, $advance_payment->amount);

            AdvancePaymentController::transactionSave($advance_payment, $contraTrans, $currentuser);
        } else if ($advance_payment->voucher_type == 'creditNote') { // purchase Invoice\
            $transaction->credit = 0.00;
            $transaction->debit = $advance_payment->amount;
            $transaction->particulars = 'Credit Note received';
            AccountHelper::withDraw($account_id, $advance_payment->amount);

            $contra_account_id = GeneralHelper::getContraAccountId($currentuser, 'creditNote');
            $contra_account = GeneralHelper::getCustomerAccount($contra_account_id);
            $contraTrans->account_id = $contra_account_id;
            $contraTrans->account = $contra_account;
            $contraTrans->pmtcode = GeneralHelper::getPaymentAcode($contra_account_id);
            $contraTrans->pmtcoderef = $contra_account_id;
            $contraTrans->debit = 0.00;
            $contraTrans->credit = $advance_payment->amount;
            $contraTrans->particulars = 'Credit Note ContraEntry';

            AccountHelper::deposit($contra_account_id, $advance_payment->amount);

            AdvancePaymentController::transactionSave($advance_payment, $contraTrans, $currentuser);
        } else {
            $transaction->credit = 0.00;
            $transaction->debit = $advance_payment->amount;
        }
        AdvancePaymentController::transactionSave($advance_payment, $transaction, $currentuser);

        $resVal['id'] = $advance_payment->id;
         return $resVal;
    }

    public function transactionSave($advance_payment, $transaction, $currentuser) {
        $transaction->account_category = $advance_payment->tra_category;
        $transaction->customer_id = $advance_payment->customer_id;
        $transaction->acode = GeneralHelper::getCustomerAcode($advance_payment->customer_id);
        $transaction->acoderef = $advance_payment->customer_id;
        $transaction->payment_mode = $advance_payment->payment_mode;
        $transaction->is_active = 1;
        $transaction->created_by = $currentuser->id;
        $transaction->updated_by = $currentuser->id;
        $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $transaction->prefix = $advance_payment->prefix;
        $transaction->save();
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $customer_id = $request->input('customer_id');
        $created_by = $request->input('created_by');
        $created_by_id = $request->input('created_by_id');
        $amount = $request->input('amount');
        $account = $request->input('account');
        $payment_mode = $request->input('payment_mode');
        $sales_payment = $request->input('sales_payment');
        $status = $request->input('status');
        $voucher = $request->input('voucher_type');
        $voucher_no = $request->input('voucher_no');
        $is_active = $request->input('is_active');
        $mode = $request->input('mode');
        $purchase_invoice_id=$request->input('purchase_invoice_id');
        $invoice_id=$request->input('invoice_id');

        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');
        $type = $request->input('type', '');

        $builder = DB::table('tbl_adv_payment as p')
                ->leftJoin('tbl_customer as c', 'p.customer_id', '=', 'c.id')
                ->leftJoin('tbl_user as u', 'p.created_by', '=', 'u.id')
                ->leftjoin('tbl_purchase_payment as pp','pp.id','=','p.voucher_no')
                ->leftjoin('tbl_payment as pa','pa.id','=','p.voucher_no')
                ->select('p.*', 'c.fname','c.city','c.billing_city','shopping_city','u.f_name as created_by_user',DB::raw("(CASE WHEN p.voucher_type='direct_purchase_payment' THEN pp.purchase_invoice_id  WHEN p.voucher_type='direct_sales_payment' THEN pa.invoice_id END) as ref_id"));


        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        
        if (!empty($created_by)) {
            $builder->where('u.f_name', 'like', $created_by . '%');
        }
        if (!empty($created_by_id)) {
            $builder->where('p.created_by', '=', $created_by_id);
        }
        if (!empty($account)) {
            $builder->where('p.account_id','=', $account);
        }
        if (!empty($payment_mode)) {
            $builder->where('p.payment_mode', 'like', $payment_mode . '%');
        }
        if (!empty($amount)) {
            $builder->where('p.amount', '=',$amount);
        }
        if ($sales_payment==1) {
            $builder->where('p.voucher_type', '=','advance_invoice');
        }
        else if ($sales_payment==2) {
            $builder->where('p.voucher_type', '=','advance_purchase');
        }
        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if (!empty($customer_id)) {
            $builder->where('p.customer_id', '=', $customer_id);
        }
        if (!empty($purchase_invoice_id)) {
            $builder->where('pp.purchase_invoice_id', '=', $purchase_invoice_id);
        }
        if (!empty($invoice_id)) {
            $builder->where('pa.invoice_id', '=', $invoice_id);
        }       
        if (!empty($status)) {
            $builder->where('p.status', '=', $status);
        }

        if (!empty($voucher)) {
            $builder->where('p.voucher_type', '=', $voucher);
        }
        if (!empty($is_active)) {
            $builder->where('p.is_active', '=', $is_active);
        }
        if (!empty($voucher_no)) {
            $builder->where('p.voucher_no', '=', $voucher_no);
        }
        if (!empty($status)) {
            $builder->where('p.status', '=', $status);
        }

        if ($type == 1) {
            $builder->whereRaw(DB::raw("(p.voucher_type = 'advance_invoice' or  p.voucher_type = 'direct_sales_payment') "));
        } else if ($type == 2) {
            $builder->whereRaw(DB::raw("(p.voucher_type = 'direct_purchase_payment' or  p.voucher_type = 'advance_purchase') "));

            
        }
        if (!empty($from_date)) {
            $builder->whereDate('p.date', '>=', $from_date);
        }

        if (!empty($to_date)) {

            $builder->whereDate('p.date', '<=', $to_date);
            // $builder->addselect(DB::raw("SUM(balance_amount) as balance_amouny, SUM(p.amount) as amount,sum(p.used_amount) as used_amount"));
            // $builder->groupby('p.customer_id');
        }
        //if(!empty($tra_category)){
        //   $builder->where('tra_category','=',$tra_category);
        // }
        if (!empty($mode)) {
            if ($mode == 2)
                $builder->where('p.balance_amount', '>', 0);
        }
        $builder->orderBy('p.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $paymentCollection = $builder->get();
        } else {

            $paymentCollection = $builder->skip($start)->take($limit)->get();
        }
//        LogHelper::info('Advance Payment Filters' . $request->fullurl());
//        LogHelper::info('Advance Payment List' . $paymentCollection);
        $resVal['list'] = $paymentCollection;
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Advance Payment Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $advance_payment = AdvancePayment::findOrFail($id);
            if ($advance_payment->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Advance Payment List Not found';
            return $resVal;
        }

        if ($advance_payment->voucher_type == 'invoice_payment') {
            DB::table('tbl_payment')->where('adv_payment_id', $id)->update(['is_active' => 0]);

            SalesInvoiceHelper::updateInvoiceStatus($advance_payment->voucher_no);
            AccountHelper::addTransaction($credit = 0, $advance_payment->amount, $advance_payment->account_id);

            $get_sales_id = DB::table('tbl_invoice')->where('is_active', 1)
                            ->where('id', '=', $advance_payment->voucher_no)->first();
            if (!empty($get_sales_id->quote_id)) {
                SalesInvoiceHelper::updateSalesOrderStatus($get_sales_id->quote_id);
            }
        } else if ($advance_payment->voucher_type == 'advance_invoice') {
            $builder = DB::table('tbl_payment')->where('adv_payment_id', $id)
                    ->where('is_active', '=', 1)
                    ->get();
            DB::table('tbl_payment')->where('adv_payment_id', $id)->update(['is_active' => 0]);
            foreach ($builder as $pament_det) {
                $invoice_id = $pament_det->invoice_id;
                SalesInvoiceHelper::updateInvoiceStatus($invoice_id);
                AccountHelper::addTransaction($credit = 0, $advance_payment->amount, $advance_payment->account_id);

                $get_sales_id = DB::table('tbl_invoice')->where('is_active', 1)
                                ->where('id', '=', $advance_payment->voucher_no)->first();
                if (!empty($get_sales_id->quote_id)) {
                    SalesInvoiceHelper::updateSalesOrderStatus($get_sales_id->quote_id);
                }
            }
        } else if ($advance_payment->voucher_type == 'advance_purchase') {
            $builder = DB::table('tbl_purchase_payment')->where('adv_payment_id', $id)
                    ->where('is_active', '=', 1)
                    ->get();
            DB::table('tbl_purchase_payment')->where('adv_payment_id', $id)->update(['is_active' => 0]);
            AccountHelper::addTransaction($advance_payment->amount, $debit=0, $advance_payment->account_id);

             foreach ($builder as $pament_det) {
                $invoice_id = $pament_det->purchase_invoice_id;
                SalesInvoiceHelper::updatePurchaseInvoiceStatus($invoice_id);
            }
        }
        
        DB::table('tbl_transaction')->where('voucher_number',$id)
                ->whereRaw(DB::raw("(voucher_type= 'invoice_payment' or voucher_type= 'advance_invoice' or voucher_type= 'advance_purchase')"))
                ->update(['is_active' => 0]);

        //Update Advance balance Status
        SalesInvoiceHelper::updateAdvPaymentStatusAndBalance($advance_payment->id);
        $advance_payment->is_active = 0;
        $advance_payment->updated_by = $currentuser->id;
        $advance_payment->update();

        $resVal['id'] = $advance_payment->id;

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Advance Payment Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $payment = AdvancePayment::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Advance Payment Not found';
            return $resVal;
        }
        $payment->created_by = $currentuser->id;
        $payment->updated_by = $currentuser->id;
        $payment->fill($request->all());
        $payment->save();

        return $resVal;
    }

    public function purchaseAdvanceBalance(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $customer_id = $request->input('customer_id');
        $show_all = $request->input('show_all');

        $builder = DB::table('tbl_adv_payment as a')
                ->leftJoin('tbl_customer as c', 'a.customer_id', '=', 'c.id')
                ->select(DB::raw(" a.customer_id,SUM(a.balance_amount) as balance_amount ,a.customer_id,c.fname, c.lname"))
                ->where('a.is_active', '=', 1)
                ->where('a.voucher_type', '=', 'advance_purchase')
                ->groupBy(DB::raw("a.customer_id"));

        if ($customer_id != '') {
            $builder->where('a.customer_id', '=', $customer_id);
        }
        if (!empty($from_date)) {

            $builder->whereDate('a.date', '>=', $from_date);
        }
        if (!empty($to_date)) {

            $builder->whereDate('a.date', '<=', $to_date);
        }
        $builder = $builder->get();
        $resVal['success'] = TRUE;
        if ($show_all == 1) {
            foreach ($builder as $tax) {
                $id = $tax->customer_id;
                $builder_detail = DB::table('tbl_adv_payment as a')
                        ->leftJoin('tbl_customer as c', 'a.customer_id', '=', 'c.id')
                        ->select(DB::raw("a.date, a.amount,a.balance_amount,a.used_amount,c.fname, c.lname"))
                        ->where('a.voucher_type', '=', 'advance_purchase')
                        ->where('a.customer_id', '=', $id)
                        ->where('a.is_active', '=', 1);

                if ($customer_id != '') {
                    $builder_detail->where('a.customer_id', '=', $customer_id);
                }
                if (!empty($from_date)) {

                    $builder_detail->whereDate('a.date', '>=', $from_date);
                }
                if (!empty($to_date)) {

                    $builder_detail->whereDate('a.date', '<=', $to_date);
                }


                $tax->data = $builder_detail->get();
            }
        }

        $resVal['list'] = $builder;


        return ($resVal);
    }

}

?>