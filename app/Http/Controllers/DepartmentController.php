<?php



namespace App\Http\Controllers;

use DB;
use Validator;
use App\Department;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation;
use App\Helper\AuthorizationHelper;
class DepartmentController  extends Controller{
    
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Department Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
            
        }
        $code = DB::table('tbl_department')
                ->select('code')
                ->where('code','=',$request->code)
                ->where('is_active','=',1)
                ->first();
        
        if(!empty($code)){
            $resVal['message'] = 'Department Code is already exist';
            return $resVal;
        }
        
        $currentuser=Auth::user();
        $department = new Department;      
        $department->updated_by=$currentuser->id;
        $department->created_by=$currentuser->id;
        $department->is_active = 1;
        $department->fill($request->all());
        $department->save();
        $resVal['id'] = $department->id;

        return $resVal;
    
    }
    public function update(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Department Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $department = Department::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Department Not found';
            return $resVal;
        }
        $idd = DB::table('tbl_department')
                ->select('id')
                ->where('id','=',$id)
                ->where('is_active','=',0)
                ->first(); 
        if(is_null($idd)){
           $existcode = DB::table('tbl_department')
                ->select('code')
                ->where('id','=',$id)
                ->where('code','=',$request->code)
                ->first();
            $code = DB::table('tbl_department')
                ->select('code')
                ->where('code','=',$request->code)
                ->where('is_active','=',1)
                ->first();
        
             if(!empty($code) && empty($existcode)){
                 $resVal['message'] = 'Department Code is already exist';
                 return $resVal;
             }
             if(!empty($existcode)){
                 $department->created_by = $currentuser->id;
                 $department->updated_by=$currentuser->id;
                 $department->fill($request->all());
                 $department->save();}
             }
        else{
            $resVal['message'] = 'Department Not found';
        }
        return $resVal;
    }
    
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $code = $request->input('code','');
        $builder = DB::table('tbl_department')
                ->select('*')
                ->where('is_active','=',1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
 
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        } 
        
        if(!empty($code)){
            $builder->where('code','=',$code);
        }
        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $departmentCollection = $builder->get();
        } else {

            $departmentCollection = $builder->skip($start)->take($limit)->get();
        }
                
        $resVal['list'] = $departmentCollection;
        return ($resVal);
    }
    
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Department Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');    
        $currentuser = Auth::user();
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $department = Department::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Department Not found';
            return $resVal;
        }
        $department->is_active = 0;
        $department->updated_by = $currentuser->id;
        $department->save();
        return $resVal;
    }
    
}
