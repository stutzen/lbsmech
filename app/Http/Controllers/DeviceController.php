<?php

namespace App\Http\Controllers;

use App\Device;
use Illuminate\Http\Request;
use App\Transformer\DeviceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

class DeviceController extends controller{
    public function save(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $deviceCollection = Device::where('device_code', "=", $request->input('device_code'))->get();
        $sc = Device::where('short_code', "=", $request->input('short_code'))->get();
         if(count($deviceCollection) > 0 && count($sc) > 0){
             $resVal['success'] = FALSE;
            $resVal['message'] = 'Device Code & Short Code is already exits.';
            return $resVal;
        }
       else if (count($deviceCollection) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Device Code is already exits.';
            return $resVal;
        }
        
      else  if (count($sc) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Short Code is already exits.';
            return $resVal;
        }
        $resVal = array();
        $resVal['message'] = 'Device Added Successfully.';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $device = new Device;
        $device->created_by = $currentuser->id;
        $device->modified_by = $currentuser->id;
        $device->fill($request->all());
        $device->save();

        $resVal['id'] = $device->id;

        return $resVal;
    }
//UPDATE
    public function update(Request $request, $id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $device = Device::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                        'error' => [
                            'message' => 'Device not found'
                        ]
                            ], 404);
        }
        $resVal['message'] = 'Device Updated Successfully.';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        
        $device->modified_by = $currentuser->id;
        $device->fill($request->all());
       $device->save();
        $resVal['id'] = $device->id;
        return $resVal;
    }

    //DELETE
    public function delete($id) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $device = Device::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                        'error' => [
                            'message' => 'Device not found'
                        ]
                            ], 404);
        }

        $device->delete();

        $resVal['message'] = 'Device deleted Successfully.';
        $resVal['success'] = TRUE;
          $resVal['id'] = $device->id;
        return $resVal;
    }
    
    //LIST ALL
    public function listAll(Request $request) {
       $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $name = $request->input('name');
        $deviceCode = $request->input('deviceCode');
        $shortCode = $request->input('shortCode');
        $resVal{'total'} = Device::count();
        $resVal['success'] = TRUE;
         $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $builder = Device::query();
         if (!empty($id)) {
            $builder->where('id', '=', $request->input('id'));
        }
        if (!empty($name)) {
            $builder->where('name', 'like', $name . '%');
        }
         if (!empty($deviceCode)) {
            $builder->where('device_code', 'like', $deviceCode . '%');
        }
         if (!empty($shortCode)) {
            $builder->where('short_code', 'like', $shortCode . '%');
        }
         $device =$builder->paginate($limit);
              $this->data =   $this->collection($device, new DeviceTransformer());
         $resVal['total'] = $builder->count();
         $resCol= $this->collection($builder->skip($start)->take($limit)->get(), new DeviceTransformer());
      return array_merge ($resVal, $resCol);
    }
    
    //Device Validation
    public function validation(Request $request){
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $builder = Device::query();
        $deviceCode = $request->input('deviceCode');
         try {
              
            if (!empty($deviceCode)) {
            $builder->where('device_code', '!=', $deviceCode);
        }
        } catch (ModelNotFoundException $e) {
             $resVal['message'] = 'Unauthorized device code';
        $resVal['success'] = FALSE;
            return $resVal;
        }
        $device =new Device();
        // Device::where('device_code', '=', $deviceCode). get();
        $resVal['message'] = 'Authorized device code';
        $resVal['success'] = TRUE;
         $resVal['deviceCode'] = $device->device_code;
//          $resVal['deviceCode'] = $device->device_code;
           $resVal['shortCode'] = $device->short_code;
         return $resVal ;
    }
}
