<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Bom;
use App\BomDetail;
use App\BomByProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

class BomController extends Controller {

    public function save(Request $request) {
        $resVal['message'] = 'BOM Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $code = DB::table('tbl_bom')
                    ->select('bom_code')
                    ->where('bom_code','=',$request->input('bom_code'))
                    ->where('is_active','=',1);

        if ($code->count()>0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Bom code is already exist';
            return $resVal;
        }
        $bom = new Bom;
        $uom_id = $request->input('uom_id');

        if (!empty($request->input('output_pdt_id'))) {
            $product = DB::table('tbl_product')
                    ->select('name', 'uom', 'uom_id')
                    ->where('id', '=', $request->input('output_pdt_id'))
                    ->first();
            $bom->output_pdt_name = $product->name;
            $bom->output_pdt_uom = $product->uom;
            $bom->output_pdt_uom_id = $product->uom_id;
        }

        $currentuser = Auth::user();
        $bom->updated_by = $currentuser->id;
        $bom->created_by = $currentuser->id;
        $bom->is_active = 1;
        $bom->fill($request->all());
        $bom->save();
        $detailBom = $request->input('detail');
        $productBom = $request->input('productBom');
        BomController::DetailBomSave($bom->id, $detailBom, $currentuser->id,$productBom);
        $resVal['id'] = $bom->id;
        return $resVal;
    }

    public static function DetailBomSave($bomId, $detailBom, $currentUserId,$productBom) {

        foreach ($detailBom as $detail) {
            $ti = new BomDetail;
            if (!empty($detail['input_pdt_id'])) {
                $product = DB::table('tbl_product')
                        ->select('name', 'uom', 'uom_id')
                        ->where('id', '=', $detail['input_pdt_id'])
                        ->first();
                $ti->input_pdt_name = $product->name;
                $ti->input_pdt_uom = $product->uom;
                $ti->input_pdt_uom_id = $product->uom_id;
            }
            $ti->fill($detail);
            $ti->bom_id = $bomId;
            $ti->updated_by = $currentUserId;
            $ti->created_by = $currentUserId;
            $ti->is_active = 1;
            $ti->save();
        } 
        foreach($productBom as $proBom){ 
            $pro = new BomByProduct;
            $pro ->bom_id = $bomId;
            $pro->fill($proBom);
            $pro->updated_by = $currentUserId;
            $pro->created_by = $currentUserId;
            $pro->is_active = 1;
            $pro->save();
        }
        
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Bom Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $uom_id = $request->input('uom_id');
        $currentuser = Auth::user();
        try {
            $bom= Bom::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Bom Not found';
            return $resVal;
        }
        
        $code = DB::table('tbl_bom')
                    ->select('bom_code')
                    ->where('id','!=',$id)
                ->where('bom_code','=',$request->input('bom_code'))
                  ->where('is_active','=',1);
        

        if ($code->count()>0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Bom code is already exist';
            return $resVal;
        }
        if (!empty($request->input('output_pdt_id'))) {
            $product = DB::table('tbl_product')
                    ->select('name', 'uom', 'uom_id')
                    ->where('id', '=', $request->input('output_pdt_id'))
                    ->first();
            $bom->output_pdt_name = $product->name;
            $bom->output_pdt_uom = $product->uom;
            $bom->output_pdt_uom_id = $product->uom_id;
        }

        $bom->updated_by = $currentuser->id;
        $bom->fill($request->all());
        $bom->save();
        $builder = DB::table('tbl_bom_detail')->where('bom_id', $id)->delete();
        $builder1 = DB::table('tbl_bom_by_product')->where('bom_id', $id)->delete();
        $detailBom= $request->input('detail');
        $productBom = $request->input('productBom');
        BomController::DetailBomSave($bom->id, $detailBom, $currentuser->id,$productBom);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $name = $request->input('name','');
        $prev_bom_id = $request->input('prev_bom_id','');
        $output_pdt_id = $request->input('output_pdt_id','');
        $is_active = $request->input('is_active','');
        $bom_code = $request->input('bom_code','');
        $task_group_id=$request->input('task_group_id');
        $output_pdt_name = $request->input('output_pdt_name','');
        $builder = DB::table('tbl_bom as c')
                 ->leftJoin('tbl_bom as tc', 'tc.id', '=','c.prev_bom_id')
                ->leftjoin('tbl_task_group as tg','tg.id','=','c.task_group_id')
                ->select('c.*','tc.name as prev_bom_name','tg.name as task_group_name','tg.type');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('c.id', '=', $id);
        }

        if (!empty($name)) {
            $builder->where('c.name', 'like', '%' . $name . '%');
        }


        if (!empty($task_group_id)) {
            $builder->where('c.task_group_id', '=', $task_group_id);
        }
         if (!empty($prev_bom_id)) {
            $builder->where('c.prev_bom_id', '=', $prev_bom_id);
        }

        if (!empty($output_pdt_id)) {
            $builder->where('c.output_pdt_id', '=', $output_pdt_id);
        }
        if (!empty($bom_code)) {
            $builder->where('c.bom_code', 'like', '%' . $bom_code . '%');
        }
        if (!empty($output_pdt_name)) {
            $builder->where('c.output_pdt_name', 'like', '%' . $output_pdt_name . '%');
        }
        if ($is_active != '') {
            $builder->where('c.is_active', '=', $is_active);
        }
        $builder->orderBy('c.id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $taskCollection = $builder->get();
        } else {

            $taskCollection = $builder->skip($start)->take($limit)->get();
        }
        
        $resVal['list'] = $taskCollection;
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Bom Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $is_active = 0;

        $currentuser = Auth::user();
        try {
            $bom = Bom::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Bom Not found';
            return $resVal;
        }
        $builder = DB::table('tbl_bom_detail')->where('bom_id', $id)->delete();
        $builder1 = DB::table('tbl_bom_by_product')->where('bom_id', $id)->delete();
        $bom->is_active = $is_active;
        $bom->updated_by = $currentuser->id;
        $bom->fill($request->all());
        $bom->save();
        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $name = $request->input('name', '');
        $prev_bom_id = $request->input('prev_bom_id', '');
        $output_pdt_id = $request->input('output_pdt_id');
        $is_active = $request->input('is_active', '');
        $bom_code = $request->input('bom_code', '');
        $output_pdt_name = $request->input('output_pdt_name', '');
        $builder = DB::table('tbl_bom as c')
             ->leftJoin('tbl_bom as tc', 'tc.id', '=','c.prev_bom_id')
                ->leftjoin('tbl_task_group as tg','tg.id','=','c.task_group_id')
                ->select('c.*','tc.name as prev_bom_name','tg.name as task_group_name','tg.type');
//                ->where('c.is_active', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

       if (!empty($id)) {
            $builder->where('c.id', '=', $id);
        }
        if (!empty($name)) {
           $builder->where('c.name', 'like', '%' . $name . '%');
        }
       if (!empty($bom_code)) {
            $builder->where('c.bom_code', 'like', '%' . $bom_code . '%');
       }
       if (!empty($prev_bom_id)) {
           $builder->where('c.prev_bom_id', '=', $prev_bom_id);
        }
       if (!empty($output_pdt_id)) {
           $builder->where('c.output_pdt_id', '=', $output_pdt_id);
       }
       if (!empty($output_pdt_name)) {
           $builder->where('c.output_pdt_name', 'like', '%' . $output_pdt_name . '%');
        }
        if ($is_active != '') {
            $builder->where('c.is_active', '=', $is_active);
        } 
        $builder->orderBy('c.id', 'desc');
        
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $taskCollection = $builder->get();
        } else {

            $taskCollection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($taskCollection as $it) {
           $id = $it->id;
            $detail = DB::table('tbl_bom_detail as b')
                    ->leftjoin('tbl_task_group_detail as tg','b.task_group_detail_id','=','tg.id')
                    ->select('b.*','tg.operation as task_group_detail_name')
                    ->where('b.bom_id', $id);
            $it->detail = $detail->get();
            $proBom = DB::table('tbl_bom_by_product as bp')
                    ->leftjoin('tbl_product as pro','bp.product_id','=','pro.id')
                    ->leftjoin('tbl_uom as u','bp.uom_id','=','u.id')
                    ->select('bp.*','pro.name as product_name','u.name as uom_name')
                    ->where('bp.bom_id', $id);
            $it->proByProduct = $proBom->get();
        } 
        $resVal['list'] = $taskCollection;
        return ($resVal);
    }

}
