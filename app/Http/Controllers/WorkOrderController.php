<?php

namespace App\Http\Controllers;

use DateTime;
use App\User;
use App\MoByProduct;
use Carbon\Carbon;
use App\PersistentLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Mail\Message;
use DB;
use App\ActivityLog;
use App\WorkOrderInput;
use App\WorkOrder;
use App\WorkOrderSession;
use App\ManufacturingOrder;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\PlaceHolders\WorkOrderExternalPrint;
use App\PlaceHolders\WorkOrderInternalPrint;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class WorkOrderController extends Controller {

//put your code here
    public function Save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Manufacturing Order created Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $rootId = 0;

        $data = WorkOrderController::bomDetail($request);
        if (array_key_exists('success', $data)) {
            $resVal['message'] = 'Recursion Occured Check Configuration';
            $resVal['success'] = False;
        } else {
            //foreach ($data as $d) {
            for ($b = 0; $b < sizeof($data); $b++) {
                $d = $data[$b];
                $mo = new ManufacturingOrder;
                $mo->created_by = $currentuser->id;
                $mo->updated_by = $currentuser->id;
                $mo->is_active = $request->input('is_active', 1);
                $mo->bom_id = $d->bom_id;
                $mo->qty = $d->qty;
                $mo->customer_id = $d->customer_id;
                $mo->user_id = $d->user_id;
                $mo->deadline_date = $d->deadline_date;
                $mo->date = $d->deadline_date;
                $mo->produced_qty = $d->produced_qty;
                $mo->task_group_id = $d->task_group_id;
                $mo->work_type = $d->work_type;
                $mo->status = $d->status;
                $mo->is_root = $d->is_root;
                $mo->ref_id = $d->ref_id;
                $mo->root_id = $rootId;
                $mo->wo_status = 'New';
                $mo->output_pdt_id = $d->output_pdt_id;
                $temp = array();
                array_push($temp, $d);
                $mo->fill($temp);
                $mo->save();
                if ($b == 0) {
                    $rootId = $mo->id;
                }
                $qc = DB::table('tbl_qcp')->where('type', '=', 'mo')->where('product_id', '=', $mo->output_pdt_id)
                                ->where('is_active', '=', 1)->get();
                if (count($qc) > 0) {
                    $mo->qc_status = 'required';
                    $mo->save();
                }
                WorkOrderController::workOrderInput($d, $mo);
                WorkOrderController::moInput($mo->bom_id, $mo->id);
            }
        }


        return $resVal;
    }

    public function bomDetail($request) {
        $bomArray = array();
        $testArray = array();
        $bom_id = $request->input('bom_id');
        array_push($testArray, $bom_id);
        $data = array();
        array_push($bomArray, $request);
        for ($b = 0; $b < sizeof($bomArray); $b++) {
            $rec = $bomArray[$b];
            $bomId = $rec->bom_id;
            $qty = $rec->qty;
            $bomDetail = DB::table('tbl_bom_detail')
                    ->select('*')
                    ->where('bom_id', '=', $bomId)
                    ->where('is_active', '=', 1)
                    ->get();
            $i = 1;
            foreach ($bomDetail as $bomDet) {
                $val = 'test' . $i;
                $$val = array();
                $bomDets = DB::table('tbl_bom as b')
                        ->leftjoin('tbl_task_group as t','t.id','=','b.task_group_id')
                        ->where('b.output_pdt_id', '=', $bomDet->input_pdt_id)
                        ->where('b.is_active', '=', 1)
                        ->first();
                $iq = $bomDet->input_qty;
                if ($bomDets != '') {
                    $oVal = new ManufacturingOrder;
                    $oVal->qty = $qty * $iq;
                    $oVal->bom_id = $bomDets->id;
                    $oVal->user_id = $request->user_id;
                    $oVal->customer_id = $request->customer_id;
                    $oVal->deadline_date = $request->deadline_date;
                    $oVal->produced_qty = $request->produced_qty;
                    $oVal->task_group_id = $bomDets->task_group_id;
                    $oVal->work_type = $bomDets->type;
                    $oVal->status = $request->status;
                    $oVal->is_root = 0;
                    $oVal->root_id = $request->root_id;
                    $oVal->ref_id = 0;
                    $oVal->output_pdt_id = $bomDets->output_pdt_id;

                    if ($b == 0) {
                        array_push($$val, $bomId, $oVal->bom_id);
                        array_push($data, $$val);
                        array_push($bomArray, $oVal);
                        array_push($testArray, $oVal->bom_id);
                    } else {
                        foreach ($data as $da) {

                            if (in_array($bomId, $da) && $b != 0) {

                                if (end($da) == $bomId) {
                                    if (!in_array($oVal->bom_id, $da)) {
                                        array_push($da, $oVal->bom_id);
                                        array_push($$val, $da);
                                        array_push($bomArray, $oVal);
                                        array_push($testArray, $oVal->bom_id);
                                    } else {
                                        $bomArray = array();
                                        $bomArray['success'] = false;
                                        break;
                                    }
                                } elsE if (end($da) != $oVal->bom_id) {
                                    array_push($$val, $da);
                                    $vals = array_pop($da);
                                    if (end($da) == $bomId) {
                                        if (!in_array($oVal->bom_id, $da)) {
                                            array_push($da, $oVal->bom_id);
                                            if (!in_array($da, $data)) {
                                                array_push($$val, $da);
                                                array_push($bomArray, $oVal);
                                                array_push($testArray, $oVal->bom_id);
                                            }
                                        } else {
                                            $bomArray = array();
                                            $bomArray['success'] = false;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                array_push($$val, $da);
                            }
                        }
                        $data = $$val;
                    }
                }
                $i++;
            }
        }

        return $bomArray;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Work Order Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $work = ManufacturingOrder::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Work Order Not found';
            return $resVal;
        }
        $work->created_by = $currentuser->id;
        $work->fill($request->all());
        $work->save();

        DB::table('tbl_work_order_input')
                ->where('is_active', '=', 1)
                ->where('mo_id', '=', $id)
                ->update(['to_consume' => DB::raw("(input_qty * " . $work->qty . ")")]);

        DB::table('tbl_work_order')
                ->where('is_active', '=', 1)
                ->where('mo_id', '=', $id)
                ->update(['original_production_qty' => ($work->qty)]);

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Work Order Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $work = ManufacturingOrder::findOrFail($id);
            if ($work->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Work Order  Not found';
            return $resVal;
        }

        $workinput = DB::table('tbl_work_order_input')->where('mo_id', $id)->get();

//        foreach ($workinput as $win) {
//            $ex_array = array();
//            $ex_array1 = array();
//
//            $get_product = DB::table("tbl_product")
//                    ->where('id', '=', $win->input_pdt_id)
//                    ->first();
//            $sku = $get_product->sku;
//
//            $ex_array['product_id'] = $win->input_pdt_id;
//            $ex_array['product_sku'] = $sku;
//            $ex_array['uom_id'] = $win->input_pdt_uom_id;
//            $ex_array['qty'] = $win->input_qty;
//
//            array_push($ex_array1, $ex_array);
//            GeneralHelper::StockAdjustmentSave($ex_array1, 'work_order_delete', $id);
//        }



        $workitem = DB::table('tbl_work_order')->where('mo_id', $id)->update(['is_active' => 0]);
        $workinput = DB::table('tbl_work_order_input')->where('mo_id', $id)->update(['is_active' => 0]);

        $work->is_active = $request->input('is_active', 0);
        $work->update();

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $customerId = $request->input('customerId', '');
        $bomId = $request->input('bomId', '');
        $bomName = $request->input('bomName', '');
        $prodStatus=$request->input('productStatus','');
        $userId = $request->input('userId', '');
        $workType = $request->input('workType', '');
        $woStatus = $request->input('woStatus', '');
        $status = $request->input('status', '');
        $fromDate=$request->input('fromDate','');
        $search=$request->input('search','');
        $toDate=$request->input('toDate','');
        $taskGroupId = $request->input('taskGroupId', '');
        $outputProductId = $request->input('outputProductId', '');
        $isActive = $request->input('isActive', '');

        $builder = DB::table('tbl_manufacturing_order as w')
                ->leftjoin(DB::raw(" (select (Case when productStatus in ('issued')  then 'issued' when productStatus in ('waiting')  then 'waiting' when  productStatus in ('paritally') then 'paritally' when   productStatus in ('available') then 'available' End) as productStatus,mo_id from "
                        . "(select (Case when wi.to_consume = wi.reserved_qty Then 'Issued' when i.quantity >= wi.to_consume Then 'available' when  i.quantity = 0 then 'waiting' When i.quantity < wi.to_consume Then 'paritally'   End) as productStatus,wi.mo_id,i.product_id "
                        . "from tbl_work_order_input as wi left join tbl_inventory as i on i.product_id = wi.input_pdt_id) d group by mo_id) as wi on wi.mo_id=w.id"), function($join2) {
                    
                })
                ->leftjoin(DB::raw("(select  count(w.status) as status,w.mo_id from tbl_manufacturing_order as m LEFT JOIN tbl_work_order as w on m.id=w.mo_id where w.status = 'completed' group by m.id) as wro on wro.mo_id=w.id"), function($join1) {
                    
                })
                 ->leftjoin(DB::raw("(select t.operation as last,w.mo_id from  tbl_work_order as w left join tbl_task_group_detail as t on t.id=w.task_group_detail_id  where status = 'completed' order by w.id desc limit 1) as wr on wr.mo_id=w.id"), function($join) {
                    
                })
                ->leftjoin(DB::raw("(select t.operation as current,w.mo_id from  tbl_work_order as w left join tbl_task_group_detail as t on t.id=w.task_group_detail_id  where status in ('ready','inprogress') ) as wr1 on wr1.mo_id=w.id"), function($join5) {
                    
                })
                ->leftjoin(DB::raw("(select t.operation as next,w.mo_id from  tbl_work_order as w left join tbl_task_group_detail as t on t.id=w.task_group_detail_id  where status = 'pending' order by w.id asc limit 1) as wr2 on wr2.mo_id=w.id"), function($join3) {
                    
                })
                ->leftJoin('tbl_customer as c', 'w.customer_id', '=', 'c.id')
                ->leftJoin('tbl_bom as b', 'w.bom_id', '=', 'b.id')
                ->leftJoin('tbl_user as u', 'w.user_id', '=', 'u.id')
                ->leftJoin('tbl_task_group as tg', 'w.task_group_id', '=', 'tg.id')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'w.output_pdt_id')
                ->select('w.*', DB::raw("Concat(c.fname,' ',c.lname ) as customerName"), 'b.name as bomName', 'tg.name as taskGroupName', DB::raw("Concat(u.f_name,' ',u.l_name ) as userName")
                , DB::raw("wro.status as status_count"), 'wro.mo_id', 'p.name as output_pdt_name','wr.last','wr1.current','wr2.next','wi.productStatus');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('w.id', '=', $id);
        }
        if ($isActive != '') {
            $builder->where('w.is_active', '=', $isActive);
        }

        if (!empty($bomName)) {
            $builder->where('b.name', 'like', '%' . $bomName . '%');
        }

        if ($customerId != '') {
            $builder->where('w.customer_id', '=', $customerId);
        }

        if ($userId != '') {
            $builder->where('w.user_id', '=', $userId);
        }

        if ($bomId != '') {
            $builder->where('w.bom_id', '=', $bomId);
        }

        if ($workType != '') {
            $builder->where('w.work_type', '=', $workType);
        }

        if ($woStatus != '') {
            $builder->where('w.wo_status', '=', $woStatus);
        }

        if ($taskGroupId != '') {
            $builder->where('w.task_group_id', '=', $taskGroupId);
        }


        if ($outputProductId != '') {
            $builder->where('w.output_product_id', '=', $outputProductId);
        }
        if(!empty($fromDate)){
            $builder->whereDate('w.deadline_date','>=',$fromDate);
        }
        
        if(!empty($toDate)){
            $builder->whereDate('w.deadline_date','<=',$toDate);
        }

        if ($status != '') {
            $builder->where('w.status', '=', $status);
        }
        if(!empty($prodStatus)){
            $builder->where('wi.productStatus','=',$prodStatus);
        }
        if(!empty($search)){
            $builder->where('w.id','=',$search)
                    ->orWhere('b.name','like','%'.$search.'%');            
        }
        $builder->orderBy('w.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $collect) {
            $mod_id = $collect->id;
            $data = DB::table('tbl_work_order')
                    ->select(DB::raw(" distinct (count(status)) as status_count"))
                    ->where('mo_id', '=', $mod_id)
                    ->where('is_active', '=', 1)
                    ->first();
            if ($collect->mo_id == null) {
                $collect->isCompleted = 0;
            } else if ($collect->status_count == $data->status_count) {
                $collect->isCompleted = 1;
            } else {
                $collect->isCompleted = 0;
            }
        }
        $resVal['list'] = $collection;
        return ($resVal);
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');

        $builder = DB::table('tbl_manufacturing_order as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->select('i.*', 'c.fname as customer_fname', 'c.lname as customer_lname');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }

        $builder->orderBy('i.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $cl) {
            $c = array();
            $details = array();
            $ot = array();
            $detail = DB::table('tbl_work_order as wod')
                    ->select('wod.*', 'tc.name as task_name')
                    ->leftjoin('tbl_task_config as tc', 'wod.task_id', '=', 'tc.id')
                    ->where('wod.mo_id', '=', $id)
                    ->get();

            foreach ($detail as $c) {
                $inputs = DB::table('tbl_work_order_input as woi')
                        ->select('woi.*', 'tci.name as task_name', 'tci.input_qty as task_input_qty')
                        // ->leftjoin('tbl_task_config as tc','woi.task_id','=','tc.id')
                        ->leftjoin('tbl_task_config_input as tci', 'woi.config_input_id', '=', 'tci.id')
                        ->where('woi.mo_id', '=', $id)
                        ->where('woi.work_order_detail_id', '=', $c->id);
                $c->inputs = $inputs->get();
            }
            $cl->details = $detail;
        }

        $resVal['list'] = $collection;
        return ($resVal);
    }

    public function workOrderInput($data, $work) {
        $currentUser = Auth::user();
        // foreach ($data as $da) {
        $detail = DB::table('tbl_bom_detail')
                ->where('bom_id', '=', $data->bom_id)
                ->where('is_active', '=', 1)
                ->get();

        foreach ($detail as $det) {
            $input = new WorkOrderInput;
            $input->input_pdt_id = $det->input_pdt_id;
            $input->input_pdt_name = $det->input_pdt_name;
            $input->input_pdt_uom_id = $det->input_pdt_uom_id;
            $input->input_pdt_uom = $det->input_pdt_uom;
            $input->input_qty = $det->input_qty;
            $input->bom_id = $det->bom_id;
            $input->mo_id = $work->id;
            $input->bom_detail_id = $det->id;
            $input->task_group_detail_id = $det->task_group_detail_id;
            $input->to_consume = ($det->input_qty * $work->qty);
            $input->consumed = 0;
            $input->reserved_qty = 0;
            $input->status = 'New';
            $input->is_active = 1;
            $input->created_by = $currentUser->id;
            $input->updated_by = $currentUser->id;
            $input->save();
        }
        //}
    }

    public function moInput($bomId, $moId) {
        $currentUser = Auth::user();
        $product = DB::table('tbl_bom_by_product')
                ->where('bom_id', '=', $bomId)
                ->where('is_active', '=', 1)
                ->get();

        foreach ($product as $pro) {
            $input = new MoByProduct;
            $input->mo_id = $moId;
            $input->pro_id = $pro->product_id;
            $input->qty = $pro->qty;
            $input->produced_qty = 0.00;
            $input->uom_id = $pro->uom_id;
            $input->is_active = 1;
            $input->created_by = $currentUser->id;
            $input->updated_by = $currentUser->id;
            $input->save();
        }
        //}
    }

    public function WorkOrderPlan(Request $request, $id) {
        $resVal = array();
        $resVal['message'] = 'Work Order Plan Successfully';
        $resVal['success'] = TRUE;

        //$id = $request->input('id');

        $currentUser = Auth::user();

        $workOrder = DB::table('tbl_manufacturing_order')
                ->where('id', '=', $id)
                ->where('is_active', '=', 1)
                ->first();

        if ($workOrder != '') {
            $task_group_id = $workOrder->task_group_id;

            $groupDetail = DB::table('tbl_task_group_detail')
                    ->where('task_group_id', '=', $task_group_id)
                    ->orderBy('sequence_no', 'asc')
                    ->where('is_active', '=', 1)
                    ->get();

            foreach ($groupDetail as $task) {
                $work = new WorkOrder;
                $work->work_center_id = $task->work_center_id;
                $work->task_group_detail_id = $task->id;
                $work->mo_id = $workOrder->id;
                $work->type = $task->type;
                $work->amount = $task->amount;
                $work->produced_qty = 0;
                $work->original_production_qty = $workOrder->qty;


                $input = DB::table('tbl_work_order_input')
                        ->where('mo_id', '=', $workOrder->id)
                        ->where('task_group_detail_id', '=', $task->id)
                        ->where('is_active', '=', 1)
                        ->first();
                if ($input != '') {
                    $work->work_order_input_id = $input->id;
                    $work->original_production_qty = $input->to_consume;
                }
                $work->is_active = 1;
                $work->created_by = $currentUser->id;
                $work->updated_by = $currentUser->id;
                $work->sequence_no = $task->sequence_no;
                $work->status = 'pending';
                $work->save();
            }
        }

        DB::table('tbl_work_order')
                ->where('mo_id', '=', $id)
                ->where('is_active', '=', 1)
                ->limit(1)
                ->update(['status' => 'ready'])
        ;

        DB::table('tbl_manufacturing_order')
                ->where('id', '=', $id)
                ->where('is_active', '=', 1)
                ->update(['status' => 'planned']);

        return $resVal;
    }

    public function inputList(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $moId = $request->input('moId', '');
        $productId = $request->input('productId', '');
        $taskGroupDetailId = $request->input('taskGroupDetailId', '');
        $bomId = $request->input('bomId', '');
        $bomDetailId = $request->input('bomDetailId', '');
        $isActive = $request->input('isActive', '');

        $builder = DB::table('tbl_work_order_input as w')
                ->leftJoin('tbl_bom as b', 'w.bom_id', '=', 'b.id')
                ->leftJoin('tbl_inventory as i','w.input_pdt_id','=','i.product_id')
                ->select('w.*', 'b.name as bomName','i.quantity');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('w.id', '=', $id);
        }
        if ($isActive != '') {
            $builder->where('w.is_active', '=', $isActive);
        }

        if ($moId != '') {
            $builder->where('w.mo_id', '=', $moId);
        }

        if ($productId != '') {
            $builder->where('w.input_pdt_id', '=', $productId);
        }

        if ($taskGroupDetailId != '') {
            $builder->where('w.task_group_detail_id', '=', $taskGroupDetailId);
        }


        if ($bomId != '') {
            $builder->where('w.bom_id', '=', $bomId);
        }


        if ($bomDetailId != '') {
            $builder->where('w.bom_detail_id', '=', $bomDetailId);
        }



        $builder->orderBy('w.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function detailList(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $moId = $request->input('moId', '');
        $productId = $request->input('productId', '');
        $isActive = $request->input('isActive', '');
        $type = $request->input('type');
        $customer_id = $request->input('customer_id');
        $status = $request->input('status', '');

        $builder = DB::table('tbl_work_order as wd')
                ->leftJoin('tbl_work_center as wc', 'wd.work_center_id', '=', 'wc.id')
                ->leftjoin('tbl_customer as c', 'c.id', '=', 'wd.customer_id')
                ->leftjoin('tbl_work_order_input as wi', 'wi.id', '=', 'wd.work_order_input_id')
                ->leftJoin('tbl_manufacturing_order as wo', 'wd.mo_id', '=', 'wo.id')
                ->leftJoin('tbl_task_group_detail as tgd', 'wd.task_group_detail_id', '=', 'tgd.id')
                ->leftjoin('tbl_department as d', 'd.id', '=', 'tgd.department_id')
                ->select('wd.*', 'wc.name as workCenterName', 'tgd.operation as taskGroupDetailName', 'wo.qty', DB::raw("Concat(c.fname,' ',c.lname) as customerName"), 'wi.input_pdt_id as product_id', 'wi.input_pdt_name as product_name', 'wi.input_pdt_uom_id as uom_id', 'wi.input_pdt_uom as uom', 'd.name as department_name');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('wd.id', '=', $id);
        }
        if ($isActive != '') {
            $builder->where('wd.is_active', '=', $isActive);
        }

        if ($moId != '') {
            $builder->where('wd.mo_id', '=', $moId);
        }
        if ($customer_id != '') {
            $builder->where('wd.customer_id', '=', $customer_id);
        }
        if ($type != '') {
            $builder->where('wd.type', '=', $type);
        }
        if ($status != '') {
            $builder->where('wd.status', '=', $status);
        }

        if ($productId != '') {
            $builder->where('wd.output_pdt_id', '=', $productId);
        }

        $builder->orderBy('wd.sequence_no', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function inventoryUpdate(Request $request) {
        $id = $request->input('id');
        $resVal['success'] = TRUE;
        $resVal['message'] = "Work Order Completed SuccessFully";
        $workOrder = DB::table('tbl_manufacturing_order')
                ->where('id', '=', $id)
                ->where('is_active', '=', 1)
                ->first();

        $workInput = DB::table('tbl_work_order_input')
                ->where('mo_id', '=', $id)
                ->where('is_active', '=', 1)
                ->get();

        foreach ($workInput as $work) {
            $qty = $work->to_consume;
            GeneralHelper::InventoryUpdate($work->input_pdt_id, $qty, 'order_input');
        }


        if ($workOrder != '') {
            $bom_id = $workOrder->bom_id;
            $bom = DB::table('tbl_bom')
                    ->where('id', '=', $bom_id)
                    ->where('is_active', '=', 1)
                    ->first();

            if ($bom != '') {
                $product_id = $bom->output_pdt_id;
                GeneralHelper::InventoryUpdate($product_id, $workOrder->produced_qty, 'order_output');
            }
        }
        return $resVal;
    }

    public function produced(Request $request) {
        $id = $request->input('id');
        $resVal['success'] = TRUE;
        $resVal['message'] = "Qty Updated SuccessFully";
        $input = DB::table('tbl_work_order_input')
                ->where('mo_id', '=', $id)
                ->where('is_active', '=', 1)
                ->get();
        foreach ($input as $inp) {
            DB::table('tbl_work_order_input')
                    ->where('id', '=', $inp->id)
                    ->where('is_active', '=', 1)
                    ->update(['consumed' => $inp->to_consume]);
        }
        return $resVal;
    }

    public function orderSession(Request $request) {
        $resVal['success'] = TRUE;
        $resVal['message'] = "Work Order Details Completed SuccessFully";

        $currentUser = Auth::user();

        $start_date = new DateTime($request->input('start_date'));
        $end_date = new DateTime($request->input('end_date'));
        $work_order_id = $request->input('work_order_id');

        $interval = $start_date->diff($end_date);
        $hr = $interval->h;
        $min = $interval->i;
        $sec = $interval->s;
        $duration = "$hr:$min:$sec";

        $orderSess = new WorkOrderSession;
        $orderSess->fill($request->all());
        $orderSess->created_by = $currentUser->id;
        $orderSess->updated_by = $currentUser->id;
        $orderSess->is_active = 1;
        $orderSess->duration = $duration;
        $orderSess->save();

        $workOrder = WorkOrder::findOrFail($work_order_id);

        if ($workOrder != null) {
            if ($workOrder->original_production_qty != $workOrder->produced_qty) {
                $workOrder->produced_qty = $workOrder->produced_qty + $orderSess->qty;
            }
            if ($workOrder->start_date == null) {
                $date = explode(' ', $orderSess->start_date);
                $workOrder->start_date = $date[0];
                $workOrder->start_time = $date[1];
                $workOrder->status = 'inprogress';
                DB::table('tbl_manufacturing_order')->where('id', '=', $workOrder->mo_id)->update(['status' => 'inprogress']);
            }
            if ($workOrder->original_production_qty == $workOrder->produced_qty) {
                $date = explode(' ', $orderSess->end_date);
                $workOrder->end_date = $date[0];
                $workOrder->end_time = $date[1];
                $workOrder->status = 'completed';
            }
        }
        $workOrder->save();

        if ($workOrder->status == 'completed') {
            DB::table('tbl_work_order')
                    ->where('mo_id', '=', $workOrder->mo_id)
                    ->where('status', 'pending')
                    ->where('is_active', '=', 1)
                    ->limit(1)
                    ->update(['status' => 'ready']);
        }

        if ($workOrder->work_order_input_id > 0) {
            DB::table('tbl_work_order_input')
                    ->where('id', '=', $workOrder->work_order_input_id)
                    ->where('is_active', '=', 1)
                    ->update(['consumed' => DB::raw("(consumed + " . $orderSess->qty . ")")]);
        }
        return $resVal;
    }

    public function sessionList(Request $request) {

        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $work_order_id = $request->input('work_order_id');
        $is_active = $request->input('is_active');


        $builder = DB::table('tbl_work_order_session as ws')
                ->leftjoin('tbl_work_order as w', 'w.id', '=', 'ws.work_order_id')
                ->select('ws.*', 'w.start_time', 'w.end_time');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($work_order_id)) {
            $builder->where('work_order_id', '=', $work_order_id);
        }

        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function orderStatusUpdate(Request $request, $id) {
        $resVal['success'] = TRUE;
        $resVal['message'] = "work Order Status Updated SuccessFully";

        $status = $request->input('status');
        $customer_id = $request->input('customer_id');
        $work = WorkOrder::findOrFail($id);
        $work->status = $status;
        $work->save();

        if ($work->type == 'external') {
            $work->customer_id = $customer_id;
            $work->save();
        }

//        if ($work != '') {
//            if ($work->status == 'inprogress') {
//                DB::table('tbl_manufacturing_order')
//                        ->where('id', '=', $work->mo_id)
//                        ->update(['status' => 'pending']);
//            } elseif ($work->status == 'completed') {
//                DB::table('tbl_work_order')
//                        ->where('mo_id', '=', $work->mo_id)
//                        ->where('status', 'pending')
//                        ->where('is_active', '=', 1)
//                        ->limit(1)
//                        ->update(['status' => 'ready']);
//            }
//        }
        return $resVal;
    }

    public function markAsDone(Request $request, $id) {
        $resVal['success'] = TRUE;
        $resVal['message'] = "Manufacturing Order Completed SuccessFully";

        $work = ManufacturingOrder::findOrFail($id);
        $work->status = 'completed';
        $work->save();

//        DB::table('tbl_inventory')
//                ->where('product_id', '=', $work->output_pdt_id)
//                ->where('is_active', '=', 1)
//                ->update(['quantity' => DB::raw("quantity+".$work->qty."")]);

        return $resVal;
    }

    public function checkAvailablity(Request $request, $mo_id) {
        $resVal['success'] = TRUE;
        $resVal['message'] = "Qty Updated SuccessFully";

        $workInput = DB::table('tbl_work_order_input')
                ->where('is_active', '=', 1)
                ->where('mo_id', '=', $mo_id)
                ->get();


        foreach ($workInput as $work) {
            $product_id = $work->input_pdt_id;
            $need_qty = $work->to_consume;
            $reservedQty = $work->reserved_qty;
            $consumed_qty = $work->consumed;
            $check_qty = $need_qty - $reservedQty;
            $available_qty = DB::table('tbl_inventory')
                    ->select('*')
                    ->where('product_id', '=', $product_id)
                    ->where('is_active', '=', 1)
                    ->first();

            if ($need_qty != $consumed_qty) {
                if ($available_qty != '') {
                    if ($available_qty->quantity <= $check_qty) {

                        DB::table('tbl_work_order_input')
                                ->where('is_active', '=', 1)
                                ->where('id', '=', $work->id)
                                ->update(['reserved_qty' => DB::raw("reserved_qty+$available_qty->quantity")]);

                        GeneralHelper::InventoryUpdate($product_id, $available_qty->quantity, 'order_input');
                    } elseif ($available_qty->quantity >= $check_qty) {
                        DB::table('tbl_work_order_input')
                                ->where('is_active', '=', 1)
                                ->where('id', '=', $work->id)
                                ->update(['reserved_qty' => DB::raw("reserved_qty+$check_qty")]);
                        GeneralHelper::InventoryUpdate($product_id, $check_qty, 'order_input');
                    }
                }
            }
        }
        return $resVal;
    }

    public function reservingQty(Request $request) {
        $tp = $request->all();
        foreach ($tp as $tps) {
            $id = $tps['id']; //workOrderId
            $qty = $tps['qty']; //reservedQty
            $resVal['success'] = TRUE;
            $resVal['message'] = "Quantity Reserved";

            $work = DB::table('tbl_work_order_input')
                    ->where('id', '=', $id)
                    ->where('is_active', '=', 1)
                    ->first();
            $newQty = $work->reserved_qty + $qty;
            $moId = $work->mo_id;

            if ($newQty >= $work->to_consume) {
                $status = 'Issued';
            } else {
                $status = 'Partially Issued';
            }
            GeneralHelper::InventoryUpdate($work->input_pdt_id, $qty, 'order_input');
            DB::table('tbl_work_order_input')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $id)
                    ->update(
                            array(
                                "reserved_qty" => DB::raw('reserved_qty +' . $qty),
                                "status" => $status
                            )
            );

            $workOrder = DB::table('tbl_work_order_input')
                    ->select(DB::raw("SUM(reserved_qty) as reserved_qty, SUM(to_consume) as to_consume"))
                    ->where('mo_id', '=', $moId)
                    ->where('is_active', '=', 1)
                    ->first();
            if ($workOrder->reserved_qty >= $workOrder->to_consume) {
                $moStatus = 'Issued';
            } else {
                $moStatus = 'Partially Issued';
            }

            DB::table('tbl_manufacturing_order')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $moId)
                    ->update(['wo_status' => $moStatus]);
        }
        return $resVal;
    }

    public function workOrderPendingList(Request $request) {

        $department_id = $request->input('department_id');
        $operation=$request->input('operation','');

        $builder = DB::table('tbl_work_order as w')
                ->select('w.*', 'd.name as department_name', 'td.operation', 'wc.name as work_center_name')
                ->leftjoin('tbl_task_group_detail as td', 'td.id', '=', 'w.task_group_detail_id')
                ->leftjoin('tbl_department as d', 'd.id', '=', 'td.department_id')
                ->leftjoin('tbl_work_center as wc', 'wc.id', '=', 'w.work_center_id')
                ->where('w.type', '=', 'external')
                ->whereIn('w.status', ['ready', 'pending'])
                ->where('w.is_active', '=', 1);

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($department_id)) {
            $builder->where('td.department_id', '=', $department_id);
        }
        if(!empty($operation)){
            $builder->where('td.operation','=',$operation);
        }
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function customerAssignOrder(Request $request) {

        $resVal['success'] = TRUE;
        $resVal['message'] = "Order Assigned SuccessFully";

        $customer_id = $request->input('customer_id');
        $orders = $request->input('order');

        DB::table('tbl_work_order')->where('is_active', '=', 1)->whereIn('id', $orders)->update(['customer_id' => $customer_id, 'status' => 'inprogress']);

        $workOrder = DB::table('tbl_work_order as w')
                ->leftjoin('tbl_work_order_input as wi', 'wi.id', '=', 'w.work_order_input_id')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'wi.input_pdt_id')
                ->where('w.is_active', '=', 1)
                ->whereIn('w.id', $orders)
                ->get();
        $data = array();
        $req = array();
        foreach ($workOrder as $work) {
            if (!empty($work->work_order_input_id)) {
                $req['product_id'] = $work->input_pdt_id;
                $req['uom_id'] = $work->input_pdt_uom_id;
                $req['qty'] = $work->to_consume;
                $req['product_sku'] = $work->sku;
                array_push($data, $req);
            }
            GeneralHelper::StockWastageSave($data, 'work_order', $work->id);
        }

        return $resVal;
    }

    public function workOrderUpdate(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Work Order Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $work = WorkOrder::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Work Order Not found';
            return $resVal;
        }
        $work->updated_by = $currentuser->id;
        $work->fill($request->all());
        $work->save();

        return $resVal;
    }

    public function moPrint(Request $request) {
        $id = $request->input('id');
        $type = $request->input('type');
        if (strcasecmp($type, 'external') == 0) {
            $replace = new WorkOrderExternalPrint;
            $replace_msg = $replace->placeHolderFun($id, 'for_print', '', '', '');
        } else if (strcasecmp($type, 'internal') == 0) {
            $replace = new WorkOrderInternalPrint;
            $replace_msg = $replace->placeHolderFun($id, 'for_print', '', '', '');
        }
        return $replace_msg;
    }

}

?>