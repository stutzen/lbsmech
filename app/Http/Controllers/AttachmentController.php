<?php
namespace App\Http\Controllers;
use App\Attachment;
use DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AttachementController
 *
 * @author Deepa
 */
class AttachmentController extends Controller {
    //put your code here
    
    public function save(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Attachment Save Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $attachement = $request->all();
        foreach ($attachement as $std) {

                $attachement = new Attachment;
                $attachement->is_active =  $request->input('isactive', 1);
               
                $attachement->created_by = $currentuser->ename;
                $attachement->updated_by = $currentuser->ename;
                $attachement->fill($std);

                $attachement->save();
           
        }
        $resVal['id'] = $attachement->id;

        return $resVal;
    }
    
     public function listAll(Request $request) {
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
        $id = $request->input('id');
        $type= $request->input('type','');
        $refid = $request->input('ref_id','');
        $isactive =$request->input('is_active','');
       
        
        $builder = DB::table('tbl_attachment')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }
        if (!empty($refid)) {
            $builder->where('ref_id', '=', $refid);
        }
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }
       $builder->orderBy('id','desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }
    
     public function delete($id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'AttachmentDeleted Successfully';

        try {
            $attachment = Attachment::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'AttachmentNot found';
            return $resVal;
        }

        $attachment->delete();

        return $resVal;
    }
    
    
    
}

?>
