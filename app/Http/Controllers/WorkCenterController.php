<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\WorkCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

class WorkCenterController extends Controller {

    public function save(Request $request) {
        $resVal['message'] = 'Work Center Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $code = DB::table('tbl_work_center')
                    ->select('code')
                    ->where('code','=',$request->input('code'))
                    ->where('is_active','=',1);

        if ($code->count()>0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Work Center code is already exist';
            return $resVal;
        }
          $currentuser = Auth::user();
          
        $task = new WorkCenter;
        $task->updated_by = $currentuser->id;
        $task->created_by = $currentuser->id;
        $task->is_active = 1;
        $task->fill($request->all());
        $task->save();
      
        $resVal['id'] = $task->id;
        return $resVal;
    }


    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Work Center Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentuser = Auth::user();
        try {
            $work= WorkCenter::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Work Center Not found';
            return $resVal;
        }
        
       $code = DB::table('tbl_work_center')
                    ->select('code')
                    ->where('code','=',$request->input('code'))
               ->where('id','!=',$id)
                    ->where('is_active','=',1);

        if ($code->count()>0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Work Center code is already exist';
            return $resVal;
        }
        $work->updated_by = $currentuser->id;
        $work->is_active = 1;
        $work->fill($request->all());
        $work->save();
       
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $name = $request->input('name','');
        $code = $request->input('code','');
        $is_active = $request->input('is_active','');
        
        $builder=DB::table('tbl_work_center')
                ->select('*');
                
        
        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }

        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
         if (!empty($code)) {
            $builder->where('code','=',$code);
        }


        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $taskCollection = $builder->get();
        } else {

            $taskCollection = $builder->skip($start)->take($limit)->get();
        }
        
        $resVal['list'] = $taskCollection;
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Work Center Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentuser = Auth::user();
        try {
            $work = WorkCenter::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Work Center Not found';
            return $resVal;
        }
        
        $work->is_active = 0;
        $work->updated_by = $currentuser->id;
        $work->save();
        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
       
      $builder=DB::table('tbl_work_center')
                ->select('*')
              ->where('id','=',$id)
                ->where('is_active','=',1)
                ->get();
               
        $resVal['success'] = TRUE;
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder;
        return ($resVal);
    }

}
