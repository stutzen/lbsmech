<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Helper\AuthorizationHelper;

class FileuploadController extends Controller {

    public function saveFile(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $tempPath = $_FILES['file']['tmp_name'];
        $fileName = time() . '-' . $_FILES['file']['name'];
        $filePath = $request->input('filepath', '');
        $filePrefix = $request->input('file_prefix', '');

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        if ($filePath) {
            $uploadPath = $filePath . DIRECTORY_SEPARATOR . $fileName;
        } else {
            $uploadPath = $fileName;
        }
        move_uploaded_file($tempPath, $uploadPath);

        $resVal['success'] = TRUE;
        $resVal['filepath'] = $uploadPath;

        return $resVal;
    }

    public function saveLogo(Request $request) {

        $tempPath = $_FILES['file']['tmp_name'];
        $fileName = time() . '-' . $_FILES['file']['name'];
        $filePath = $request->input('filepath', '');

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        if ($filePath) {
            $uploadPath = $filePath . DIRECTORY_SEPARATOR . $fileName;
        } else {
            $uploadPath = $fileName;
        }
        $domain = $_SERVER['SERVER_NAME'];
        $uploadPath = 'img/logo/'.$domain.'.png';
        
        //echo 'uploadPath =' . $uploadPath;
        move_uploaded_file($tempPath, $uploadPath);

        $resVal['success'] = TRUE;
        $resVal['filepath'] = $uploadPath;

        return $resVal;
    }

}
