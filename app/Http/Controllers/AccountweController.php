<?php

namespace App\Http\Controllers;

use Validator;
use App\Account;
use App\Client;
use DB;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use App\Transformer\AccountTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;

/**
 * Class BooksController
 * @package App\Http\Controllers
 */
class AccountController extends Controller {

    /**
     * GET /books
     * @return array
     */
    public function index(Request $request) {

        $currentUser = Auth::user();

      //  print_r($currentUser);

        $limit = $request->input('limit', 100);

        return User::paginate($limit);
    }

    /**
     * POST /account/withdraw
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function withdrawSave(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Withdraw Saved Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if($ret_auth=='false')
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $validator = Validator::make($request->all(), [
                    'credit' => 'required',
                    'particulars' => 'required',
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            return $resVal;
        }

        $currentuser = Auth::user();
        $particulars = "Withdrawn by " . $currentuser->username . " FirstName " . $currentuser->f_name . " - " . $request->input('particulars', '');

        $balance = Account::where('comp_id', '=', $currentuser->comp_id)->sum('debit') - Account::where('comp_id', '=', $currentuser->comp_id)->sum('credit') - $request->input('credit', 0);

        $account = new Account;
        $account->particulars = $particulars;
        $account->acc_category = $request->input('acc_category', 1);
        $account->debit = '';
        $account->credit = $request->input('credit', '');
        $account->balance = $balance;
        $account->source_type = 'WITHDRAW';
        $account->source_id = '';
        $account->created_by = $currentuser->id;
        $account->updated_by = $currentuser->id;
        $account->is_active = $request->input('isactive', 1);
        $account->date = \Carbon\Carbon::now();
        $account->comp_id = $currentuser->comp_id;
        $account->client_id = 0;
        $account->sub_category = 0;

        $account->save();
        $resVal['id'] = $account->id;

        return $resVal;
    }

    /**
     * POST /account/saveOpenBalance
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveOpenBalance(Request $request) {
        
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Opening balance saved successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if($ret_auth=='false')
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $validator = Validator::make($request->all(), [
                    'clientId' => 'required',
                    'date' => 'required',
                    'amount' => 'required',
                    'particulars' => 'required',
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            return $resVal;
        }

        $currentuser = Auth::user();
        try {

            $client = Client::where('id', '=', $request->input('clientId'))->get();

            $particulars = "Opening balance of " . $currentuser->username . " " . $client->name . " - " . $request->input('particulars', '');

            $balance = Account::where('comp_id', '=', $currentuser->comp_id)->sum('debit') - Account::where('comp_id', '=', $currentuser->comp_id)->sum('credit') - $request->input('amount', 0);

            $account = new Account;
            $account->particulars = $particulars;
            $account->acc_category = $request->input('acc_category', 1);
            $account->debit = '';
            $account->credit = $request->input('amount', '');
            $account->balance = $balance;
            $account->source_type = 'RECEIVABLE';
            $account->source_id = '';
            $account->created_by = $currentuser->id;
            $account->updated_by = $currentuser->id;
            $account->is_active = $request->input('isactive', 1);
            $account->date = $request->input('date'); //\Carbon\Carbon::now();
            $account->comp_id = $currentuser->comp_id;
            $account->client_id = $client->id;
            $account->sub_category = $client->category_id;
            $account->save();

            //update client balance amount
            $client->balance_amount = $client->balance_amount + $request->input('amount', 0);
            $client->save();

           
            $resVal['id'] = $account->id;
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Opening balance update failed';
            return $resVal;
        }



        return $resVal;
    }

    //LIST ALL
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
       $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if($ret_auth=='false')
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $fromDate = $request->input('fromDate');
        $toDate= $request->input('toDate');
        $category= $request->input('category');
        $subCategory = $request->input('subCategory');
        $clientId= $request->input('clientId');
        $sourceType = $request->input('sourceType');
       // $resVal['list'] = array();
        //$builder = Account::query();
        $builder=DB::table('tbl_account as a')
                ->join ('tbl_user as u','a.created_by','=','u.id')
                 ->join ('tbl_client as c','a.client_id','=','c.id')
                ->select('a.*','c.name as cName','u.username as uName');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
       $limit = $request->input('limit', 100);
       

        
        if (!empty($fromDate)) {
            
            $builder->where ('date','>=', '$fromDate');
  
//            DB::connection()->enableQueryLog();  
//            var_dump(DB::getQueryLog());
        }
        
        if (!empty($toDate)) {
            
            $builder->where ('date','<=', '$toDate');
//            DB::connection()->enableQueryLog();  
//            var_dump(DB::getQueryLog());
        }
        
        if (!empty($fromDate) && !empty($toDate) ) {
            
            $builder->where ('date','>=', '$fromDate')
                   ->where ('date','<=', '$toDate');
//            DB::connection()->enableQueryLog();  
//            var_dump(DB::getQueryLog());
        }
         if (!empty($category)) {
            $builder->where('acc_category', '=', $category);
        }
        if (!empty($subCategory)) {
            $builder->where('sub_category', '=', $subCategory);
        }
        if (!empty($clientId)) {
            $builder->where('client_id', '=', $clientId);
        }
        if (!empty($sourceType)) {
            $builder ->where('source_type', 'like', $sourceType . '%');
        } 
         DB::connection()->enableQueryLog();  
            var_dump(DB::getQueryLog());
       $resVal['total'] = $builder->count(); 
       $resVal['list']= $builder->skip($start)->take($limit)->get();
      return ($resVal);

    }
    
}
