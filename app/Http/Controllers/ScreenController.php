<?php
namespace App\Http\Controllers;

use DB;
use App\Screen;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ScreenController
 *
 * @author Deepa
 */
class ScreenController extends Controller {
    //put your code here
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $sno = $request->input('sno', '');
        $is_active = $request->input('is_active', '');
        $modulename =$request->input('module_name','core');
        
        //$status = $request->input('status', '');
        $builder = DB::table('tbl_screen')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
         if (!empty($sno)) {
            $builder->where('sno', '=', $sno);
        }
        if (!empty($modulename)) {
            $builder->where('module_name', '=', $modulename);
        }
        
        $builder ->orderBy('section_sqno', 'asc')
                 ->orderBy('sno', 'asc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }
}

?>
