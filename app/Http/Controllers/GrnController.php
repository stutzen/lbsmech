<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Grn;
use Illuminate\Http\Request;
use App\Transformer\GrnTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;
use App\GrnDetail;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GrnController
 *
 * @author Deepa
 */
class GrnController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Grn Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $grn = new Grn;

        $grn->fill($request->all());
        $grn->created_by = $currentuser->id;
        $grn->is_active = 1;
        $grn->updated_by = $currentuser->id;
        $grn->status='New';
        $grn->save();

        $detail = $request->input('detail');
        foreach ($detail as $det) {
            $grnDet = new GrnDetail;
            $grnDet->fill($det);
            $grnDet->grn_id = $grn->id;
            $grnDet->created_by = $currentuser->id;
            $grnDet->is_active = 1;
            $grnDet->updated_by = $currentuser->id;
            $grnDet->save();
            
            $qc=DB::table('tbl_qcp')->where('type','=','grn')->where('product_id','=',$grnDet->product_id)
                    ->where('is_active','=',1)->get();
            if(count($qc) > 0){
                $grnDet->qc_status='required';
                $grnDet->save(); 
            }
            
            GrnController::workOrderUpdate($det);
        }

        // GeneralHelper::StockAdjustmentSave($ex_array, 'grn', $grn->id);

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $customer_id = $request->input('customer_id', '');
        $is_active = $request->input('is_active', '');
        $fromdate = $request->input('from_date', '');
        $todate = $request->input('to_date', '');
        $status=$request->input('status','');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_grn as g')
                ->leftjoin('tbl_customer as c', 'g.customer_id', '=', 'c.id')
                ->select('g.*', 'c.fname as customer_name ','c.city','c.billing_city','c.shopping_city');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('g.id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('g.is_active', '=', $is_active);
        }

        if ($customer_id != '') {
            $builder->where('g.customer_id', '=', $customer_id);
        }
         if ($status != '') {
            $builder->where('g.status', '=', $status);
        }
        if (!empty($fromdate)) {

            $builder->whereDate('g.date', '>=', $fromdate);
        }
        if (!empty($todate)) {

            $builder->whereDate('g.date', '<=', $todate);
        }

        $builder->orderBy('g.id', 'desc');
        
        $resVal['total'] = $builder->count();
        
         if ($start == 0 && $limit == 0) {
            $Collection = $builder->get();
        } else {

            $Collection = $builder->skip($start)->take($limit)->get();
        }
        
        foreach($Collection as $collect){
            $grn_id=$collect->id;
            $detail=DB::table('tbl_grn_detail as gd')
                    ->select('gd.*','wo.original_production_qty','wo.produced_qty','tg.operation','wc.name as work_center_name')
                    ->leftjoin('tbl_manufacturing_order as m','m.id','=','gd.mo_id')
                    ->leftjoin('tbl_work_order as wo','m.id','=','wo.mo_id')
                    ->leftjoin('tbl_task_group_detail as tg','tg.id','=','wo.task_group_detail_id')
                    ->leftjoin('tbl_work_center as wc','wc.id','=','wo.work_center_id')
                    ->where('gd.grn_id','=',$grn_id)
                    ->where('gd.is_active','=',1)
                    ->get();
            
            $collect->detail=$detail;
        }
        $resVal['list'] =$Collection;
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Grn Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $grn = Grn::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Grn Not found';
            return $resVal;
        }

     //   GeneralHelper::StockAdjustmentDelete($ex_array, 'grn_update', $id);
        $grn->updated_by = $currentuser->id;
        $grn->fill($request->all());
        $grn->save();
        DB::table('tbl_grn_detail')->where('grn_id','=',$id)->delete();
        
       $detail = $request->input('detail'); 
        foreach ($detail as $det) { 
            $grnDet = new GrnDetail;
            $grnDet->fill($det);
            $grnDet->grn_id = $grn->id;
            $grnDet->created_by = $currentuser->id;
            $grnDet->is_active = 1;
            $grnDet->updated_by = $currentuser->id;
            $grnDet->save();
            
             $qc=DB::table('tbl_qcp')->where('type','=','grn')->where('product_id','=',$grnDet->product_id)
                    ->where('is_active','=',1)->get();
            if(count($qc) > 0){
                $grnDet->qc_status='required';
                $grnDet->save();
            }  
            GrnController::workOrderUpdate($det);

        }

        ////GeneralHelper::StockAdjustmentSave($ex_array1, "grn_update_insert", $id);


        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Grn Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $grn = Grn::findOrFail($id);
            if ($grn->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Grn Not found';
            return $resVal;
        }

        $grn->is_active = 0;
        $grn->save();
        
         DB::table('tbl_grn_detail')->where('grn_id','=',$id)->delete(); 
       // GeneralHelper::StockWastageSave($ex_array, "grn_delete", $id);


        return $resVal;
    }

    public function GrnReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $customer_id = $request->input('customer_id');
        $show_all = $request->input('show_all');


        $open_qty = DB::table('tbl_grn')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $customer_id)
                ->where('is_active', '=', 1)
                ->sum('qty');

        $open_list = array();
        $open_list['date'] = date('d-m-Y', strtotime($from_date . '-1 day'));
        $open_list['description  '] = 'Opening qty';
        $open_list['qty'] = $open_qty;
        $resVal['openQty'] = $open_list;

        $builder = DB::table('tbl_grn as a')
                ->leftJoin('tbl_customer as c', 'a.customer_id', '=', 'c.id')
                ->select(DB::raw("a.product_name, a.product_id,SUM(a.qty) as qty , a.uom,a.customer_id,c.fname, c.lname"))
                ->whereDate('a.date', '>=', $from_date)
                ->whereDate('a.date', '<=', $to_date)
                ->where('a.is_active', '=', 1)
                ->where('a.customer_id', '=', $customer_id)
                ->groupBy(DB::raw("a.product_id"))
                ->get();

        $resVal['success'] = TRUE;
        if ($show_all == 1) {
            foreach ($builder as $tax) {
                $id = $tax->product_id;
                $builder_detail = DB::table('tbl_grn as a')
                        ->leftJoin('tbl_customer as c', 'a.customer_id', '=', 'c.id')
                        ->select(DB::raw("a.product_name, a.product_id,a.date,c.fname, a.qty,c.lname"))
                        ->whereDate('a.date', '>=', $from_date)
                        ->whereDate('a.date', '<=', $to_date)
                        ->where('a.is_active', '=', 1)
                        ->where('a.customer_id', '=', $customer_id)
                        ->where('a.product_id', '=', $id);

                $tax->data = $builder_detail->get();
            }
        }

        $resVal['list'] = $builder;
        $close_qty = DB::table('tbl_grn')
                ->where('date', ">", $to_date)
                ->where('customer_id', '=', $customer_id)
                ->where('is_active', '=', 1)
                ->sum('qty');

        $open_list = array();
        $open_list['date'] = date('d-m-Y', strtotime($to_date . '+1 day'));
        $open_list['description  '] = 'Closing qty';
        $open_list['qty'] = $close_qty;
        $resVal['closeqty'] = $open_list;
        return ($resVal);
    }
    
    public static function workOrderUpdate($detail){
        
        $mo_id=$detail->mo_id;
        $work=DB::table('tbl_work_order')->where('mo_id','=',$mo_id)->where('type','=','external')->first();
        
        if($work != null){
        $workOrder = WorkOrder::findOrFail($work->id);

        if ($workOrder != null) {
            if ($workOrder->original_production_qty != $workOrder->produced_qty) {
                $workOrder->produced_qty = $workOrder->produced_qty + $detail->qty;
                  $workOrder->status = 'inprogress';
                 DB::table('tbl_manufacturing_order')->where('id', '=', $workOrder->mo_id)->update(['status' => 'inprogress']);
            }
            
            if ($workOrder->original_production_qty == $workOrder->produced_qty) {
                $workOrder->status = 'completed';
            }
        }
        $workOrder->save();
        }

    
    }

}

?>
