<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use DB;
use App\TaskActivity;
use App\TaskFollowers;
use App\Attachment;
use App\DealActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;

class TaskActivityController extends Controller {

    public function save(Request $request) {

        $resVal = array();
        $resVal['message'] = 'Task Activity  Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $taskActivity = new TaskActivity;
        $taskActivity->created_by = $currentuser->id;
        $taskActivity->updated_by = $currentuser->id;
        $taskActivity->is_active = $request->input('is_active', 1);
        $taskActivity->fill($request->all());
        $taskActivity->save();

        $urlattachments = $request->input('attachments');

        foreach ($urlattachments as $attachments) {

            $attachment = new Attachment;
            $attachment->fill($attachments);
            $attachment->ref_id = $taskActivity->id;
            $attachment->type = 1;
            $attachment->created_by = $currentuser->id;
            $attachment->updated_by = $currentuser->id;
            $attachment->is_active = $request->input('is_active', 1);


            $attachment->save();
        }

        $followersList = $request->input('followers');

        foreach ($followersList as $followers) {

            $follow = new TaskFollowers;
            $follow->fill($followers);
            $follow->task_activity_id = $taskActivity->id;
            $follow->task_id = $request->input('task_id');
            ;
            $follow->created_by = $currentuser->id;
            $follow->updated_by = $currentuser->id;
            $follow->is_active = $request->input('is_active', 1);


            $follow->save();
        }
        
        if($taskActivity->ref_name == 'deal'){
            $deal=new DealActivity;
            $deal->type='task';
            $deal->description=$taskActivity->title;
            $deal->log_activity_type=$taskActivity->title;
            $deal->log_activity_sub_type='';
            $deal->deal_id=$taskActivity->ref_id;
            
            if(!empty($taskActivity->ref_id)){
            $contact=DB::table('tbl_deal')->select('contact_id','user_id')->where('id','=',$taskActivity->ref_id)->first();
            
            $deal->contact_id=$contact->contact_id;
            if($contact->user_id !=0){
            $deal->user_id=$contact->user_id;
            
            $user=DB::table('tbl_user')->select('f_name')->where('id','=', $deal->user_id)->first(); 
            $deal->user_name=$user->f_name;
            }
            }
            $deal->created_by=$currentuser->id;
            $deal->updated_by=$currentuser->id;
            
            $creater=DB::table('tbl_user')->select('f_name','l_name')->where('id','=',$currentuser->id)->first();
            
            $deal->created_by_name=$creater->f_name.' '.$creater->l_name;
            $deal->updated_by_name=$creater->f_name.' '.$creater->l_name;
            $deal->comments=$taskActivity->title;
            $deal->ref_id=$taskActivity->id;
            $deal->description = "task activity created";
            $deal->is_active=1;
            $deal->save();
        }
        $resVal['id'] = $taskActivity->id;
//        LogHelper::info('Task Activity Save' . $request->fullurl());
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Task Activity Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $taskActivity = TaskActivity::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Activity Not found';
            return $resVal;
        }

        $taskActivity->updated_by = $currentuser->id;
        $taskActivity->fill($request->all());
        $taskActivity->save();

        $activity_id = $taskActivity->id;
        DB::table('tbl_attachment')->where('ref_id', '=', $activity_id)->delete();
        DB::table('tbl_task_followers')->where('task_activity_id', '=', $activity_id)->delete();

        $urlattachments = $request->input('attachments');

        foreach ($urlattachments as $attachments) {

            $attachment = new Attachment;
            $attachment->fill($attachments);
            $attachment->ref_id = $taskActivity->id;
            $attachment->type = 1;
            $attachment->created_by = $currentuser->id;
            $attachment->updated_by = $currentuser->id;
            $attachment->is_active = $request->input('is_active', 1);

            $attachment->save();
        }
        $followersList = $request->input('followers');

        foreach ($followersList as $followers) {

            $follow = new TaskFollowers;
            $follow->fill($followers);
            $follow->task_activity_id = $taskActivity->id;
            $follow->task_id = $request->input('task_id');
            ;
            $follow->created_by = $currentuser->id;
            $follow->updated_by = $currentuser->id;
            $follow->is_active = $request->input('is_active', 1);


            $follow->save();
        }
        
  if($taskActivity->ref_name == 'deal'){
            $deal=new DealActivity;
            $deal->type='taskactivity';
            $deal->description=$taskActivity->title;
            $deal->log_activity_type=$taskActivity->title;
            $deal->log_activity_sub_type='';
            $deal->deal_id=$taskActivity->ref_id;
            
            if(!empty($taskActivity->ref_id)){
            $contact=DB::table('tbl_deal')->select('contact_id','user_id')->where('id','=',$taskActivity->ref_id)->first();
            
            $deal->contact_id=$contact->contact_id;
            if($contact->user_id !=0){
            $deal->user_id=$contact->user_id;
            
            $user=DB::table('tbl_user')->select('f_name')->where('id','=', $deal->user_id)->first(); 
            $deal->user_name=$user->f_name;
            }
            }
            $deal->created_by=$currentuser->id;
            $deal->updated_by=$currentuser->id;
            
            $creater=DB::table('tbl_user')->select('f_name','l_name')->where('id','=',$currentuser->id)->first();
            
            $deal->created_by_name=$creater->f_name.' '.$creater->l_name;
            $deal->updated_by_name=$creater->f_name.' '.$creater->l_name;
            $deal->comments=$taskActivity->title;
            $deal->ref_id=$taskActivity->id;
            $deal->description = "task activity updated";
            $deal->is_active=1;
            $deal->save();
        }
//        LogHelper::info('Task Activity Update' . $request->fullurl());
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        /*$ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }*/
        $currentuser = Auth::user();

        $id = $request->input('id');
        $emp_id = $request->input('emp_id');
        $emp_name = $request->input('emp_name');
        $type_id = $request->input('type_id');
        $type_name = $request->input('type_name');
        $task_id = $request->input('task_id');
        $ref_id = $request->input('ref_id');
        $ref_name = $request->input('ref_name');
        $task_name = $request->input('task_name');
        $is_active = $request->input('is_active');
        $config_type = $request->input('config_type');
        $estimated_date = $request->input('estimated_date');
        $low = $request->input('low');
        $medium = $request->input('medium');
        $high = $request->input('high');
        $new = $request->input('initiated');
        $inprogress = $request->input('inprogress');
        $completed = $request->input('completed');
        $closed = $request->input('closed');
//        $typeCount = $request->input('typeCount');
//        $assignCount=$request->input('assignCount');
//        $createCount=$request->input('createCount');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $date = $request->input('date');
        $created_for = $currentuser->id;
        $assign_to = $request->input('assign_to');
        $type = $request->input('type');

        $sadmin = DB::table('tbl_user as u')
                ->select('u.rolename','e.id')
                ->leftjoin('tbl_employee as e','u.id','=','e.user_id')
                ->where('u.id', '=', $created_for)->first();
        $result = $sadmin->rolename;
        $created_for_empid=$sadmin->id;

        $builder = DB::table('tbl_task_activity')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        
        $visibleTo = array(); 
         if (strcasecmp($result,'superadmin')==0 || strcasecmp($result,'admin')==0 || strcasecmp($result,'BACKEND TEAM')==0 || strcasecmp($result,'ACCOUNTS')==0) {  
        }else{
            if (!empty($created_for_empid)) { 
              
                $value=DB::table('tbl_task_activity')->select('*')->where('emp_id', '=', $created_for_empid)
                        ->orwhere('created_for', '=', $created_for_empid)->get();
                
                 foreach($value as $val){
            $empid = $val->emp_id; 
           
       $follower = DB::table('tbl_task_followers')
                            ->select('*')->where('emp_id', '=', $empid)->get();    
        foreach($follower as $f){
             array_push($visibleTo,$f->task_activity_id);
         }
        }
        $builder->where(function ($query) use ($visibleTo) {
            foreach ($visibleTo as $ass) {
                if (!empty($ass)) { 
                    $query->orwhere('id', "=", $ass);
                }
            }
        });
        $builder ->orwhereRaw(DB::raw("(emp_id = $created_for_empid or created_for = $created_for_empid)"));
            }
         }
        
//         if (strcasecmp($result,'superadmin')==0 || strcasecmp($result,'admin')==0 || strcasecmp($result,'BACKEND TEAM')==0 || strcasecmp($result,'ACCOUNTS')==0) {  
//        }else{
//            if (!empty($created_for_empid)) { 
//              
//                $value=DB::table('tbl_task_activity')->select('*')->where('emp_id', '=', $created_for_empid)
//                        ->orwhere('created_for', '=', $created_for_empid)->get();
//                
//                 foreach($value as $val){
//            $empid = $val->emp_id; 
//           
//       $follower = DB::table('tbl_task_followers')
//                            ->select('*')->where('emp_id', '=', $empid)->get();    
//        foreach($follower as $f){
//             array_push($visibleTo,$f->task_activity_id);
//         }
//        }
//        $builder->where(function ($query) use ($visibleTo) {
//            foreach ($visibleTo as $ass) {
//                if (!empty($ass)) { 
//                    $query->orwhere('id', "=", $ass);
//                }
//            }
//        });
//        $builder ->orwhereRaw(DB::raw("(emp_id = $created_for_empid or created_for = $created_for_empid)"));
//            }
//        }

        if (!empty($date)) {
            $builder->where('date', '=', $date);
        }
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($emp_id)) {
            $builder->where('emp_id', '=', $emp_id);
        }
         if ($ref_id != '') {
            $builder->where('ref_id', '=', $ref_id);
        }
       if ($ref_name != '') {
            $builder->where('ref_name', 'like', '%' . $ref_name . '%');
        }
        if (!empty($emp_name)) {
            $builder->where('emp_name', 'like', '%' . $emp_name . '%');
        }
        if (!empty($type_name)) {
            $builder->where('type_name', 'like', '%' . $type_name . '%');
        }
        if (!empty($type_id)) {
            $builder->where('type_id', '=', $type_id);
        }
        if (!empty($task_id)) {
            $builder->where('task_id', '=', $task_id);
        }
        if (!empty($task_name)) {
            $builder->where('task_name', 'like', '%' . $task_name . '%');
        }
        if (!empty($config_type)) {
            $builder->where('config_type', '=', $config_type);
        }
        if (!empty($estimated_date)) {
            $builder->where('estimated_date', '<=', $estimated_date);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if (($low == 1) and ($medium == 1)) {
            $builder->where('priority', '<>', 'high');
        } elseif (($high == 1) and ($medium == 1)) {
            $builder->where('priority', '<>', 'low');
        } elseif (($low == 1) and ($high == 1)) {
            $builder->where('priority', '<>', 'medium');
        } elseif (($low == 1)) {
            $builder->where('priority', '=', 'low');
        } elseif ($medium == 1) {
            $builder->where('priority', '=', 'medium');
        } elseif ($high == 1) {
            $builder->where('priority', '=', 'high');
        }
        if ($new == 1 and $inprogress == 1 and $completed == 1) {
            $builder->where('status', '<>', 'closed');
        } elseif ($new == 1 and $inprogress == 1 and $closed == 1) {
            $builder->where('status', '<>', 'completed');
        } elseif ($new == 1 and $completed == 1 and $closed == 1) {
            $builder->where('status', '<>', 'inprogress');
        } elseif ($completed == 1 and $inprogress == 1 and $closed == 1) {
            $builder->where('status', '<>', 'new');
        } elseif ($new == 1 and $inprogress == 1) {
            $builder->whereRaw(DB::raw("(status = 'new' or status = 'inprogress')"));
        } elseif ($completed == 1 and $inprogress == 1) {
             $builder->whereRaw(DB::raw("(status = 'completed' or status = 'inprogress')"));
        } elseif ($completed == 1 and $closed == 1) {
             $builder->whereRaw(DB::raw("(status = 'completed' or status = 'closed')"));
        } elseif ($completed == 1 and $new == 1) {
             $builder->whereRaw(DB::raw("(status = 'completed' or status = 'new')"));
        } elseif ($closed == 1 and $new == 1) {
            $ $builder->whereRaw(DB::raw("(status = 'closed' or status = 'new')"));
        } elseif ($closed == 1 and $inprogress == 1) {
             $builder->whereRaw(DB::raw("(status = 'closed' or status = 'inprogress')"));
        } elseif (($new == 1)) {
            $builder->where('status', '=', 'new');
        } elseif ($inprogress == 1) {
            $builder->where('status', '=', 'inprogress');
        } elseif ($completed == 1) {
            $builder->where('status', '=', 'completed');
        } elseif ($closed == 1) {
            $builder->where('status', '=', 'closed');
        }

      
        $builder->where(function ($query) use ($type) {
            foreach ($type as $value) {
                if (!empty($value)) {
                    $query->orwhere('type_id', "=", $value);
                }
            }
        });
        $builder->where(function ($query) use ($assign_to) {
            foreach ($assign_to as $assign) {
                if (!empty($assign)) {
                    $query->orwhere('emp_id', "=", $assign);
                }
            }
        });

        
        if (!empty($from_date)) {
            $builder->whereDate('date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('date', '<=', $to_date);
        }
//print_r($builder->toSql());
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $Collection = $builder->get();
        } else {

            $Collection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($Collection as $attachments) {
            $id = $attachments->id;
            $attachment = DB::table('tbl_attachment')
                            ->select('*')->where('ref_id', '=', $id)->get();
           $follower = DB::table('tbl_task_followers as tf')
                   ->leftjoin('tbl_employee as e','tf.emp_id','=','e.id')
                           ->select('tf.*','e.f_name as emp_name')->where('task_activity_id', '=', $id)->get();

            $attachments->attachment = $attachment;
            $attachments->followers = $follower;
            
        }
        
        $resVal['list'] = $Collection;

        //LogHelper::info('Task Activity Filters' . $request->fullurl());
       // LogHelper::info('Task Activity List' . $resVal['list']);
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Task Activity Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $taskActivity = TaskActivity::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Activity Not found';
            return $resVal;
        }

        $taskActivity->is_active = 0;
        $taskActivity->updated_by = $currentuser->id;
        $activity_id = $taskActivity->id;
        $taskActivity->save();
       
       if($taskActivity->ref_name=='deal'){
             $deal=new DealActivity;
            $deal->type='taskactivity';
            $deal->description=$taskActivity->title;
            $deal->log_activity_type=$taskActivity->title;
            $deal->log_activity_sub_type='';
            $deal->deal_id=$taskActivity->ref_id;
            
            if(!empty($taskActivity->ref_id)){
            $contact=DB::table('tbl_deal')->select('contact_id','user_id')->where('id','=',$taskActivity->ref_id)->first();
            
            $deal->contact_id=$contact->contact_id;
            if($contact->user_id !=0){
            $deal->user_id=$contact->user_id;
            
            $user=DB::table('tbl_user')->select('f_name')->where('id','=', $deal->user_id)->first(); 
            $deal->user_name=$user->f_name;
            }
            }
            $deal->created_by=$currentuser->id;
            $deal->updated_by=$currentuser->id;
            
            $creater=DB::table('tbl_user')->select('f_name','l_name')->where('id','=',$currentuser->id)->first();
            
            $deal->created_by_name=$creater->f_name.' '.$creater->l_name;
            $deal->updated_by_name=$creater->f_name.' '.$creater->l_name;
            $deal->comments=$taskActivity->title;
            $deal->ref_id=$taskActivity->id;
            $deal->description = "task activity deleted";
            $deal->is_active=0;
            $deal->save();
       }
        DB::table('tbl_task_followers')->where('task_activity_id', '=', $activity_id)->delete();
       // LogHelper::info('Task Activity Delete' . $request->fullurl());
        return $resVal;
    }

}