<?php
namespace App\Http\Controllers;
use DB;
use App\Journal;
use App\JournalDetail;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\AccountHelper;

class JournalController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Journal Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $j = new Journal;
        $j->created_by = $currentuser->id;
        $j->updated_by = $currentuser->id;
        $j->is_active = 1;
        $j->fill($request->all());
        $j->save();
        JournalController::DetailSave($request,$j->id,$request->input('detail'),$currentuser);
        $resVal['id'] = $j->id;
         return $resVal;
    }
     public function DetailSave($request,$journalId,$details,$currentuser) {
         foreach ($details as $d) {
            $dt = new JournalDetail();
            $dt->journal_id = $journalId; 
            $dt->created_by = $currentuser->id;
            $dt->updated_by = $currentuser->id;
            $dt->is_active = 1; 
            $dt->ref_id = $d['ref_id']; 
            $dt->ref_type = $d['ref_type']; 
            $dt->acode = $d['acode']; 
            $dt->amount_type = $d['amount_type']; 
            $dt->credit = $d['credit']; 
            $dt->debit = $d['debit']; 
            $dt->comments = $d['comments'];  
            $dt->save();
             if ($dt->credit != 0 && $dt->ref_type=='account') {
                AccountHelper::deposit($d['ref_id'], $d['credit']);
            } else if ($dt->debit != 0  && $dt->ref_type=='account') {
                AccountHelper::withDraw($d['ref_id'], $d['debit']);
            }
            JournalController::TransactionSave($request,$journalId,$dt,$currentuser);
         }
     }
     
     public function TransactionSave($request,$journalId,$details,$currentuser) {
            $transaction = new Transaction;
            $mydate = date(($request->date));
            $month = date("m", strtotime($mydate));
            $fiscalMonthCollection = DB::table('tbl_fiscal_month')->where('code', "=", $month)->get();

            $transaction->fiscal_month_code = $month;
            if ($fiscalMonthCollection->count() > 0) {
                $fiscalMonth = $fiscalMonthCollection->first();
                $transaction->fiscal_month_code = $fiscalMonth->id;
            }
            $year = date("Y", strtotime($mydate));
            $fiscalYearCollection = DB::table('tbl_fiscal_year')->where('is_active', 1)->get();

            $transaction->fiscal_year_id = 0;
            $transaction->fiscal_year_code = $year;
            if ($fiscalYearCollection->count() > 0) {
                $fiscalYear = $fiscalYearCollection->first();
                $transaction->fiscal_year_id = $fiscalYear->id;
                $transaction->fiscal_year_code = $fiscalYear->code;
            }
            $transaction->transaction_date = $mydate;
            $transaction->created_by = $currentuser->id;
            $transaction->updated_by = $currentuser->id;
            $transaction->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $transaction->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $transaction->voucher_type = "Journal";
            $transaction->voucher_number = $details->id;
            $transaction->account_type = $details->ref_type;
            $transaction->account_id = $details->ref_id;
            $transaction->credit = $details->credit;
            $transaction->debit = $details->debit;
            $transaction->particulars = $details->comments;
            if($details->ref_type=='account'){
            $transaction->pmtcode = $details->acode;
            $transaction->pmtcoderef = $details->ref_id;
            }
            else  if($details->ref_type=='customer'){
            $transaction->acode = $details->acode;
            $transaction->acoderef = $details->ref_id;
            $transaction->customer_id = $details->ref_id;
            }
            $transaction->acode = $details->acode;
            $transaction->is_active = 1;
            $transaction->is_cash_flow = 1;
          //  $transaction->fill($details);
            $transaction->save();
         
     }

     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Journal Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $j = Journal::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Journal Not found';
            return $resVal;
        }
        $j->updated_by = $currentuser->id;
        $j->fill($request->all());
        $j->save();
        
        JournalController::TransactionDelete($id);
        JournalController::DetailDelete($id);
        JournalController::DetailSave($request,$j->id,$request->input('detail'),$currentuser);
        return $resVal;
    }
     
     public function TransactionDelete($id) {
         $detail = DB::table('tbl_journal_detail')->where('journal_id', $id)->get();
         foreach ($detail as $d) {
            $builder = DB::table('tbl_transaction')->where('voucher_type', 'Journal')->where('voucher_number', $d->id)->delete();
           if ($d->credit != 0 && $d->ref_type=='account') {
                AccountHelper::withDraw($d->ref_id, $d->credit);
            } else if ($d->debit != 0  && $d->ref_type=='account') {
               AccountHelper::deposit($d->ref_id, $d->debit);
            }
         }
     }
     
      public function DetailDelete($id) {
         $builder = DB::table('tbl_journal_detail')->where('journal_id', $id)->delete();
     }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $is_active = $request->input('is_active', '');
        $fromDate = $request->input('fromDate', '');
        $toDate = $request->input('toDate', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_journal')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($fromDate)) {
            $builder->where('date', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->where('date', '<=', $toDate);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
          
        return ($resVal);
    }
    
    
    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $is_active = $request->input('is_active', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_journal')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        $builder->orderBy('id', 'desc');
        if ($start == 0 && $limit == 0) {
                $builder = $builder->get();
            } else {

                $builder = $builder->skip($start)->take($limit)->get();
            }
         foreach ($builder as $pdt) {
              $details = DB::table('tbl_journal_detail as jd')
                        ->leftJoin('tbl_journal as j', 'j.id', '=', 'jd.journal_id')
                        ->select('jd.*',DB::raw('case when jd.ref_type="account" then (select account from tbl_accounts where id=jd.ref_id) '
                                . ' else (select fname from tbl_customer where id=jd.ref_id) end as name'))
                         ->where('journal_id','=',$pdt->id);
          $pdt->detail = $details->get();   
         }
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder;
        
        return ($resVal);
    }

    

    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Journal Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');    
        $currentuser = Auth::user();
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $j = Journal::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Journal Not found';
            return $resVal;
        }
        JournalController::TransactionDelete($id);
        JournalController::DetailDelete($id);
        $j->updated_by = $currentuser->id;
        $j->is_active = 0;
        $j->fill($request->all());
        $j->save();
         
        return $resVal;
    }

}

?>
