<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Contact;
use Illuminate\Http\Request;
use App\Transformer\ContactTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Attachment;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactController
 *
 * @author Deepa
 */
class ContactController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Contact Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $phone_no = $request->input('phone');
        $num = explode(',', $phone_no);
        foreach ($num as $no) {
            $no = trim($no, " ");
            $phone = DB::table('tbl_contact')->where('is_active', '=', 1)
                    ->Whereraw("FIND_IN_SET('" . $no . "',phone)");
            if ($phone->count() > 0) {
                $resVal['message'] = $no . ' Phone Number is Already Exsists';
                $resVal['success'] = False;
                return $resVal;
            }
        }

        $contact = new Contact;

        $contact->created_by = $currentuser->id;
        $contact->updated_by = $currentuser->id;
        $contact->fill($request->all());
        $contact->save();

        $contact_attachment = $request->input('attachment');

        foreach ($contact_attachment as $attachment) {
            $contactAttachment = new Attachment;
            $contactAttachment->fill($attachment);
            $contactAttachment->ref_id = $contact->id;
            $contactAttachment->created_by = $currentuser->id;
            $contactAttachment->updated_by = $currentuser->id;
            $contactAttachment->is_active = $request->input('is_active', 1);
            $contactAttachment->save();
        }


        $resVal['id'] = $contact->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active = $request->input('is_active', '');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $source_id = $request->input('source_id');
        $lead_type_name = $request->input('lead_type_name');
        $company_name = $request->input('company_name');
        $company_id = $request->input('company_id');
        $lead_type_id = $request->input('lead_type_id');
        $from_date=$request->input('from_date');
        $to_date=$request->input('to_date');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_contact as c')
                ->leftjoin('tbl_employee as u', 'u.id', '=', 'c.contact_owner_id')
                ->leftjoin('tbl_lead_master as l', 'c.lead_type_id', '=', 'l.id')
                ->leftjoin('tbl_source as s', 's.id', '=', 'c.source_id')
                ->leftjoin('tbl_company as com', 'c.company_id', '=', 'com.id')
                ->select('c.*', 's.name as source_name', 'com.name as company_name', 'l.name as lead_type_name', 'u.f_name as contact_owner_name');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);


        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();
        if (!empty($empid)) {
        $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
         if (!$isVisible) {
           $builder->where('c.contact_owner_id', '=', $empid->id);
         }
        }
//        if (strcasecmp($role_name, 'superadmin') == 0 || strcasecmp($role_name, 'admin') == 0 || strcasecmp($role_name, 'BACKEND TEAM') == 0 || strcasecmp($role_name, 'ACCOUNTS') == 0) {
//            
//        } else {
//            $builder->where('c.contact_owner_id', '=', $empid->id);
//        }
        if (!empty($id)) {
            $builder->where('c.id', '=', $id);
        }
        if (!empty($lead_type_id)) {
            $builder->where('l.id', '=', $lead_type_id);
        }
        if (!empty($source_id)) {
            $builder->where('c.source_id', '=', $source_id);
        }
        if ($is_active != '') {
            $builder->where('c.is_active', '=', $is_active);
        }
        if (!empty($lead_type_name)) {
            $builder->where('l.name', '=', $lead_type_name);
        }
        if (!empty($company_name)) {
            $builder->where('com.name', 'like', '%' . $company_name . '%');
        }
        if (!empty($company_id)) {
            $builder->where('c.company_id', '=', $company_id);
        }

        if (!empty($name)) {
            $builder->where('c.fname', 'like', '%' . $name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        if (!empty($email)) {
            $builder->where('c.email', 'like', '%' . $email . '%');
        }
        if (!empty($phone)) {
            $builder->where('c.phone', 'like', '%' . $phone . '%');
        }
        if (!empty($from_date)) {
            $builder->where('c.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->where('c.created_at', '<=', $to_date);
        }

        $resVal['total'] = $builder->count();
        $builder->orderBy('c.fname', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $contactCollection = $builder->get();
        } else {
            $contactCollection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($contactCollection as $contact) {
            $attachmet = DB::table('tbl_attachment')->where('ref_id', '=', $contact->id)->where('type', '=', 'contact')->get();

            $contact->attachment = $attachmet;
        }

        $resVal['list'] = $contactCollection;
        return ($resVal);
    }

    public function search(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $search = $request->input('search');


        //$status = $request->input('status', '');
        $builder = DB::table('tbl_contact')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);


        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();

//        if (strcasecmp($role_name, 'superadmin') == 0 || strcasecmp($role_name, 'admin') == 0 || strcasecmp($role_name, 'BACKEND TEAM') == 0 || strcasecmp($role_name, 'ACCOUNTS') == 0) {
//            
//        } else {
//            $builder->where('contact_owner_id', '=', $empid->id);
//        }



        $builder->whereRaw(" (fname like '%" . $search . "%' or phone like '%" . $search . "%'  or email like '%" . $search . "%'   ) and is_active = 1");

        $builder->orderBy('updated_at', 'desc');

        //$resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $customerCollection = $builder->get();
        } else {
            $customerCollection = $builder->skip($start)->take($limit)->get();
        }
        $resVal['list'] = $customerCollection;
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Contact Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $phone_no = $request->input('phone');
        $num = explode(',', $phone_no);
        foreach ($num as $no) {
            $no = trim($no, " ");
            $phone = DB::table('tbl_contact')->where('is_active', '=', 1)
                            ->Whereraw("FIND_IN_SET('" . $no . "',phone)")->where('id', '!=', $id);
            if ($phone->count() > 0) {
                $resVal['message'] = $no . ' Phone Number is Already Exsists';
                $resVal['success'] = False;
                return $resVal;
            }
        }

        try {
            $contact = Contact::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Contact Not found';
            return $resVal;
        }
        $contact->created_by = $currentuser->id;
        $contact->fill($request->all());
        $contact->save();

        DB::table('tbl_attachment')->where('ref_id', '=', $id)->where('type', '=', 'contact')->delete();

        $contact_attachment = $request->input('attachment');

        foreach ($contact_attachment as $attachment) {
            $contactAttachment = new Attachment;
            $contactAttachment->fill($attachment);
            $contactAttachment->ref_id = $contact->id;
            $contactAttachment->created_by = $currentuser->id;
            $contactAttachment->updated_by = $currentuser->id;
            $contactAttachment->is_active = $request->input('is_active', 1);
            $contactAttachment->save();
        }

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Contact Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentuser = Auth::user();
        try {
            $contact = Contact::findOrFail($id);

            if ($contact->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Contact   Not found';
            return $resVal;
        }
        $contact->created_by = $currentuser->id;
        $contact->updated_by = $currentuser->id;
        $contact->is_active = 0;
        //$sales->fill($request->all());
        $contact->update();

        DB::table('tbl_attachment')->where('ref_id', '=', $id)->where('type', '=', 'contact')->delete();

        return $resVal;
    }

    public function detail(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $builder = DB::table('tbl_contact as c')
                ->select('c.*', 'co.name as company_name', 'co.address as company_address', 'co.website as company_website', DB::raw("Concat(e.f_name,' ',e.l_name) as contact_owner_name")
                        , 's.name as source_name', 'l.name as lead_type_name')
                ->leftjoin('tbl_company as co', 'c.company_id', '=', 'co.id')
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'c.contact_owner_id')
                ->leftjoin('tbl_lead_master as l', 'c.lead_type_id', '=', 'l.id')
                ->leftjoin('tbl_source as s', 's.id', '=', 'c.source_id')
                ->where('c.is_active', '=', 1)
                ->where('c.id', '=', $id)
                ->get();

        $resVal['list'] = $builder;

        return $resVal;
    }

}

?>
