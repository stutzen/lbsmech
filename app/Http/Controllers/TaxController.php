<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Tax;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use App\Transformer\TaxTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaxController
 *
 * @author Deepa
 */
class TaxController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Tax Added Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $tax = new Tax;


        $tax->created_by = $currentuser->id;
        $tax->updated_by = $currentuser->id;
        $tax->is_active = $request->input('is_active', 1);
        $tax->fill($request->all());
        $tax->save();

        $resVal['id'] = $tax->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $is_group = $request->input('is_group', '');

        $name = $request->input('tax_name', '');
        $is_active = $request->input('is_active');
        $tax_percentage = $request->input('tax_percentage', '');

        $is_active = $request->input('is_active', 1);


        $builder = DB::table('tbl_tax')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($tax_percentage)) {
            $builder->where('tax_percentage', '=', $tax_percentage);
        }

        if (!empty($name)) {
            $builder->where('tax_name', 'like', '%' . $name . '%');
        }
        if ($is_group != null) {
            $builder->where('is_group', '=', $is_group);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function expiredListAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');

        $id = $request->input('id');

        $name = $request->input('tax_name', '');
        $date = $request->input('date', '');

        $is_active = $request->input('is_active');
        $tax_no = $request->input('tax_no', '');

        $builder = DB::table('tbl_tax')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if (!empty($tax_no)) {
            $builder->where('tax_no', '=', $tax_no);
        }
        if (!empty($tax_percentage)) {
            $builder->where('tax_percentage', '=', $tax_percentage);
        }

        if (!empty($date)) {
            $builder->where('start_date', '<=', $date);
            $builder->where('end_date', '>=', $date);
        } else {
            $builder->where('end_date', '>=', $current_date);
            $builder->where('start_date', '<=', $current_date);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Deleted Successfully';
        $currentuser = Auth::user();
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $tax = Tax::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Tax Not found';
            return $resVal;
        }

        $tax->created_by = $currentuser->id;
        $tax->is_active = 0;
        $tax->save();

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $tax = Tax::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Tax Not found';
            return $resVal;
        }
        $tax->created_by = $currentuser->id;
        $tax->fill($request->all());
        $tax->save();

        return $resVal;
    }

    public function calculate(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Value Calculated Successfully';

        $validator = Validator::make($request->all(), [
                    'amount' => 'required'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            return $resVal;
        }

        $tax_id = $request->input('tax_id', '0');
        $amount = $request->input('amount');
        $invoice_id = $request->input('invoice_id', '');
        $invoice_type = $request->input('invoice_type', '');

        $tax_collection = DB::table('tbl_tax')->select('*')
                        ->where('id', '=', $tax_id)
                        ->where('is_active', '=', 1)->get();
        $total_tax_amount = 0;


        $tax_list = GeneralHelper::calculatedInvoiceTax($invoice_id, $tax_id, $amount, $invoice_type);


        foreach ($tax_list as $tax) {

            $total_tax_amount += $tax->tax_amount;
        }

        $resVal['tax_amount'] = $total_tax_amount;
        $resVal['list'] = $tax_list;
        return ($resVal);
    }

    public function groupDetail(Request $request) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Tax Group detaill Success';

        $validator = Validator::make($request->all(), [
                    'id' => 'required',
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            return $resVal;
        }

        $tax_id = $request->input('id');

        $tax_collection = DB::table('tbl_tax')->select('*')
                        ->where('id', '=', $tax_id)
                        ->where('is_group', '=', 1)
                        ->where('is_active', '=', 1)->get();
        $tax_list = array();

        if (count($tax_collection) > 0) {

            $tax = $tax_collection->first();
            $tax->tax_list = array();
            if (!empty($tax->is_group)) {
                $associate_tax_id = explode(',', $tax->group_tax_ids);

                $associate_tax_collection = DB::table('tbl_tax')->select('*')
                                ->where('is_active', '=', 1)
                                ->whereIn('id', $associate_tax_id)->get();

                //sorting the tax in creation order
                foreach ($associate_tax_id as $asso_tax_id) {

                    foreach ($associate_tax_collection as $associate_tax) {
                        if ($asso_tax_id == $associate_tax->id) {
                            array_push($tax_list, $associate_tax);
                            break;
                        }
                    }
                }
                $tax->tax_list = $tax_list;
            }
            $resVal['data'] = $tax;
        } else {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Is Not Valid Taxgroup';
            $resVal['data'] = null;
        }
        return ($resVal);
    }

}

?>
