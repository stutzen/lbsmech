<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\EmployeeTaskActivities;
use App\Attachment;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;

class EmployeeTaskActivitiesController extends Controller {
    
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Employee Task Activities Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
           
        $currentuser = Auth::user();
        
        $employee=new EmployeeTaskActivities;
        $employee->fill($request->all());
        $employee->created_by=$currentuser->id;
        $employee->updated_by=$currentuser->id;
        $employee->created_for=$currentuser->id;
        $employee->is_active=$request->input('is_active',1);
        
        $employee->save();
        
        $urlattachments=$request->input('attachment');
       
        foreach($urlattachments as $attachments)
        {
        
        $attachment=new Attachment;
        $attachment->fill($attachments);
        $attachment->ref_id=$employee->id;
        $attachment->type=1;
        $attachment->created_by=$currentuser->id;
        $attachment->updated_by=$currentuser->id;
        $attachment->is_active=$request->input('is_active',1);
          
        
        $attachment->save();
        }
        
       

 $resVal['id']=$employee->id;
        return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $created_emp_name=$request->input('created_emp_name');
        $created_emp_code=$request->input('created_emp_code');
        $created_emp_team_id=$request->input('created_emp_team_id');
        $assigned_emp_name=$request->input('assigned_emp_name');
        $assigned_emp_code=$request->input('assigned_emp_code');
        $assigned_emp_team_id=$request->input('assigned_emp_team_id');
        $task_id=$request->input('task_id');
        $task_name=$request->input('task_name');
        $is_active=$request->input('is_active');
        $config_type=$request->input('config_type');
        $estimated_date=$request->input('estimated_date');
        $low=$request->input('low');
        $medium=$request->input('medium');
        $high=$request->input('high');
        $initiated=$request->input('initiated');
        $inprogress=$request->input('inprogress');
        $completed=$request->input('completed');
        $closed=$request->input('closed');
        $type= $request->input('type');
        $assign_to=$request->input('assign_to');
        $created_by=$request->input('created_by');
        $from_date=$request->input('from_date');
        $to_date=$request->input('to_date');
        $date=$request->input('date') ; 
        $created_for=$currentuser->id;
        
          $sadmin=DB::table('tbl_user')->where('id','=',$created_for)->first();
$result=$sadmin->rolename;

       
        $builder = DB::table('tbl_emp_task_activities')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        
        if(!empty($from_date)) {
            $builder->where('date', '=', $date);
        }
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($created_emp_team_id)) {
            $builder->where('created_emp_team_id', '=', $created_emp_team_id);
        }        
        if (!empty($created_emp_name)) {
            $builder->where('created_emp_name', 'like', '%' . $created_emp_name . '%');
        }
        if (!empty($created_emp_code)) {
            $builder->where('created_emp_code', 'like', '%' . $created_emp_code . '%');
        }
        if (!empty($assigned_emp_team_id)) {
            $builder->where('assigned_emp_team_id', '=', $assigned_emp_team_id);
        }
       if (!empty($assigned_emp_code)) {
            $builder->where('assigned_emp_code', 'like', '%' . $assigned_emp_code . '%');
        }
         if (!empty($assigned_emp_name)) {
            $builder->where('assigned_emp_name', 'like', '%' . $assigned_emp_name . '%');
        }
        if (!empty($task_id)) {
            $builder->where('task_id', '=', $task_id);
        }
         if (!empty($task_name)) {
            $builder->where('task_name', 'like', '%' . $task_name . '%');
        }
        if (!empty($config_type)) {
            $builder->where('config_type', '=',$config_type);
        }
       if (!empty($estimated_date)) {
            $builder->where('estimated_date', '<=', $estimated_date);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
        if(($low==1) and ($medium==1)){
            $builder->where('priority','<>','high');                 
        }
        elseif(($high==1) and ($medium==1)){
             $builder->where('priority','<>','low');
        }
        elseif(($low==1) and ($high==1)){
            $builder->where('priority','<>','medium');
        }
        elseif (($low==1)){
            $builder->where('priority', '=', 'low');
        }
        elseif($medium==1){
             $builder->where('priority', '=', 'medium');
        }
       elseif($high==1){
            $builder->where('priority', '=', 'high');
       }
       if($initiated==1 and $inprogress==1 and $completed==1){
          $builder->where('status','<>','closed'); 
       }
       elseif($initiated==1 and $inprogress==1 and $closed==1){
          $builder->where('status','<>','completed'); 
       }
       elseif($initiated==1 and $completed==1 and $closed==1){
          $builder->where('status','<>','inprogress'); 
       }
       elseif($completed==1 and $inprogress==1 and $closed==1){
          $builder->where('status','<>','new'); 
       }
       elseif($initiated==1 and $inprogress==1){
           $builder->whereRaw(DB::raw("(status = 'new' or status = 'inprogress')"));
        } elseif ($completed == 1 and $inprogress == 1) {
             $builder->whereRaw(DB::raw("(status = 'completed' or status = 'inprogress')"));
        } elseif ($completed == 1 and $closed == 1) {
             $builder->whereRaw(DB::raw("(status = 'completed' or status = 'closed')"));
        } elseif ($completed == 1 and $new == 1) {
             $builder->whereRaw(DB::raw("(status = 'completed' or status = 'new')"));
        } elseif ($closed == 1 and $new == 1) {
            $ $builder->whereRaw(DB::raw("(status = 'closed' or status = 'new')"));
        } elseif ($closed == 1 and $inprogress == 1) {
             $builder->whereRaw(DB::raw("(status = 'closed' or status = 'inprogress')"));
       }
       elseif (($initiated==1)){ 
            $builder->where('status', '=', 'new');
        }
        elseif($inprogress==1){
             $builder->where('status', '=', 'inprogress');
        }
       elseif($completed==1){
            $builder->where('status', '=', 'completed');
       }
       elseif($closed==1){
            $builder->where('status', '=', 'closed');
       }
        
       
//            if($typeCount>0){
//           for($i=1;$i<=$typeCount;$i++){
//               $types = 'type_'.$i ;
//               $builder = $builder->orWhere('type_id',"=", $request->input($types) ) ;
//           }
//       }
//        if($assignCount>0){
//           for($i=1;$i<=$assignCount;$i++){
//               $types = 'assigntype_'.$i ;
//               $builder = $builder->orWhere('assigned_emp_id',"=", $request->input($types) ) ;
//           }
//       }
//       if($createCount>0){
//           for($i=1;$i<=$createCount;$i++){
//               $types = 'createtype_'.$i ;
//               $builder = $builder->orWhere('created_emp_id',"=", $request->input($types) ) ;
//           }
//       }
      $builder->where(function ($query) use ($type){
       foreach($type as $value){
           if(!empty($value)){
            $query->orwhere('type_id',"=", $value ) ;
           }
         }});
       $builder->where(function ($query) use ($assign_to){
       foreach($assign_to as $assign){
            if(!empty($assign)){
           $query->orwhere('assigned_emp_id',"=", $assign ) ;
}}});
 $builder->where(function ($query) use ($created_by){
         foreach($created_by as $create){
           if(!empty($create)){
            $query->orwhere('created_emp_id',"=", $create ) ;
           }
         }});
      
       
       if (!empty($from_date)) {
            $builder->whereDate('date', '>=', $from_date);
        }
         if (!empty($to_date)) {
            $builder->whereDate('date', '<=', $to_date);
        }
        
        $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
         if (!$isVisible) {
            if (!empty($created_for)) {
                $builder->where('assigned_emp_id', '=', $created_for)
                        ->orwhere('created_for', '=', $created_for);
            }
        }
        
//       if($result !=  'superadmin'){
//       if(!empty($created_for))
//       {
//            $builder->where('assigned_emp_id','=',$created_for)
//                   ->orwhere('created_for','=',$created_for);
//       }
//       }
        
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $employeeCollection = $builder->get();
        } else {

            $employeeCollection = $builder->skip($start)->take($limit)->get();
        }
        
        foreach($employeeCollection as $attachments)
        {
            $id=$attachments->id;
            $attachment=DB::table('tbl_attachment')
                    ->select('*')->where('ref_id','=',$id)->get();
            
           $attachments->attachment=$attachment;
    }
        
        $resVal['list'] = $employeeCollection;
      
        return ($resVal);
    }
    
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Task Activities Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $employee = EmployeeTaskActivities::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Task Activities Not found';
            return $resVal;
        }

       

        $employee->fill($request->all());
        
         $urlattachments=$request->input('attachment');
       
        foreach($urlattachments as $attachments)
        {
            $cid=$attachments['id'];
           if($cid==0)
           {
        $attachment=new Attachment;
        $attachment->fill($attachments);
        $attachment->ref_id=$employee->id;
        $attachment->type=1;
        $attachment->created_by=$currentuser->id;
        $attachment->updated_by=$currentuser->id;
        $attachment->is_active=$request->input('is_active',1);
           
           $attachment->save();
           }
           else
           {
             try {
            $attachment = Attachment::findOrFail($cid);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Task Activities  Not found';
            return $resVal;
        }
       
           $attachment->fill($attachments);
        $attachment->ref_id=$employee->id;
        $attachment->type=1;
        $attachment->created_by=$currentuser->id;
        $attachment->updated_by=$currentuser->id;
        $attachment->is_active=$request->input('is_active',1);
           
           $attachment->save();
        }
        }
        
       

        $employee->updated_by = $currentuser->id;
        $employee->save();

        return $resVal;
    }
     public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Task Activities Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $employee = EmployeeTaskActivities::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Task Activities Not found';
            return $resVal;
        }
        $employee->is_active = 0;
        $employee->updated_by = $currentuser->id;
        $employee->save();

        return $resVal;
    }
}

