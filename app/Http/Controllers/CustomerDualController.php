<?php

namespace App\Http\Controllers;

use DB;
Use App\CustomerDual;
use App\User;
use App\Attribute;
use App\Attributevalue;
Use Illuminate\Http\Request;
use App\Transformer\CustomerTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomerController
 *
 * @author Deepa
 */
class CustomerDualController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Customer Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        /*
          if ($request->input('email') != NULL) {
          $userCollection = User::where('username', "=", $request->input('email'))->get();

          if (count($userCollection) > 0) {
          $resVal['success'] = FALSE;
          $resVal['message'] = 'Username is already exits.';
          return $resVal;
          }
          }

          $user = new User;
          $user->f_name = $request->input('fname');
          $user->l_name = $request->input('lname');
          $user->gender = $request->input('gender');
          $user->dob = $request->input('dob');
          $user->email = $request->input('email');
          $user->ph_no = $request->input('phone');
          $user->city = $request->input('city');
          $user->state = $request->input('state');
          $user->country = $request->input('country');

          $user->address = $request->input('address', '');
          $user->post_code = $request->input('post_code');
          $user->user_type = $request->input('user_type','customer');

          $user->isactive = $request->input('isactive', 1);
          $user->created_by = $currentuser->id;
          $user->updated_by = $currentuser->id;
          $user->imagepath = $request->input('imagepath');
          $user->device_id = $request->input('device_id');
          $user->username = $request->input('email');

          $user->rolename = "customer";
          $pwd = $request->input('password');

          if ($pwd != NULL) {

          $user->password = Hash::make($request->input('password'));
          } else {
          $user->password = Hash::make("customer123");
          }


          $user->save();
         */

        $customer = new CustomerDual;
        $customer->fill($request->all());
        $pwd = $request->input('password');

        if ($pwd != NULL) {

            $customer->password = Hash::make($request->input('password'));
        } else {
            $customer->password = Hash::make("customer123");
        }


        $customer->created_by = $currentuser->id;
        $customer->updated_by = $currentuser->id;


        //$user->id = $customer ->user_id ;
        //$customer->user_id = $user->id;
        $customer->save();

        $customerItemCol = ($request->input('customattribute'));
        if ($request->input('customattribute') != null) {
            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $customer->custom_attribute_json = $json_att;
            $customer->save();
            foreach ($customerItemCol as $attribute) {
                //print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $customer->customer_id;
                $customerattribute->value = $attribute['value'];
                //$customerattribute->sno = $attribute['sno'];
                $customerattribute->attribute_code = $attribute['attribute_code'];

                $customerattribute->created_by = $currentuser->id;
                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
                        //print_r($attribute->id);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

                //$customerattribute->fill($attribute);       

                $customerattribute->save();
            }
        }
        $resVal['id'] = $customer->id;


        $json_att = $customer->custom_attribute_json;
        if (empty($json_att)) {
            $json_att = "{}";
        }
        $json_att = json_decode($json_att);

        $customer->custom_attribute_json = $json_att;


        $resVal['data'] = $customer;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $customer_id = $request->input('customer_id','');
        $fname = $request->input('fname', '');
        $isactive = $request->input('is_active', '');
        $issale = $request->input('is_sale', '');
        $ispurchase = $request->input('is_purchase');
        $phone = $request->input('phone');
        $email = $request->input('email');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_customer_dual')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($customer_id)) {
            $builder->where('customer_id', '=', $customer_id);
        }
        if (!empty($issale)) {
            $builder->where('is_sale', '=', $issale);
        }
        if (!empty($ispurchase)) {
            $builder->where('is_purchase', '=', $ispurchase);
        }

        if (!empty($fname)) {
            $builder->where('fname', 'like', '%' . $fname . '%');
        }
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }
        if (!empty($phone)) {
            $builder->where('phone', 'like', '%' . $phone . '%');
        }
        if (!empty($email)) {
            $builder->where('email', 'like', '%' . $email . '%');
        }

        $arrays[] = $request->toArray();
        //     print_r($arrays);
        $arrays2 = $arrays[0];
        $array_key = array_keys($arrays2);
        $custom_array = array();
        for ($index = 0; $index < count($array_key); $index ++) {
            $arry_ind = $array_key[$index];
            $result = substr($arry_ind, 0, 8);
            if ($result == 'cus_attr') {
                array_push($custom_array, $arry_ind);
            }
        }
        for ($index = 0; $index < count($custom_array); $index ++) {
            $status1 = $custom_array[$index];
            $status = $arrays2[$status1];
            if (!empty($status)) {
                $status = explode("::", $status);
                $key = $status[0];
                $value = $status[1];
                $str = "'%" . '"' . $key . '":"' . $value . '"%' . "'";
                $builder->whereRaw('custom_attribute_json  like' . "'%" . '"' . $key . '":"' . $value . '"%' . "'");
                //  $builder->Where ('custom_attribute_json', 'like',  $str);                        
            }
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $customerCollection = $builder->get();
        } else {

            $customerCollection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($customerCollection as $customer) {
            $json_att = $customer->custom_attribute_json;
            if (empty($json_att)) {
                $json_att = "{}";
            }
            $json_att = json_decode($json_att);
            $customer->custom_attribute_json = $json_att;
        }
        $resVal['list'] = $customerCollection;
        //$resVal['list'] = $builder->paginate(10);
        return ($resVal);
    }

    public function search(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $search = $request->input('search');


        //$status = $request->input('status', '');
        $builder = DB::table('tbl_customer_dual')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $builder->whereRaw(" (fname like '%" . $search . "%' or phone like '%" . $search . "%'  or email like '%" . $search . "%' ) and is_active = 1");

        $builder->orderBy('id', 'desc');

        //$resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $customerCollection = $builder->get();
        } else {
            $customerCollection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($customerCollection as $customer) {
            $json_att = $customer->custom_attribute_json;
            if (empty($json_att)) {
                $json_att = "{}";
            }
            $json_att = json_decode($json_att);
            $customer->custom_attribute_json = $json_att;
        }


        $resVal['list'] = $customerCollection;
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Customer Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $customer = CustomerDual::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Customer Not found';
            return $resVal;
        }
        
        $customer->fill($request->all());
        if ($request->input('password') != null) {

            $customer->password = Hash::make($request->input('password'));
        }
        $attributeCollection = DB::table('tbl_attribute_types')->where('code', 'crm')->get();
        $attribute = $attributeCollection->first();


        if (count($attributeCollection) > 0) {


            $builder = DB::table('tbl_attribute_value')
                    ->where('attributetype_id', '=', $attribute->id)
                    ->where('refernce_id', '=', $id)
                    ->delete();
            //print_r($builder);
        }
        $customerItemCol = ($request->input('customattribute'));
        if ($request->input('customattribute') != null) {
            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $customer->custom_attribute_json = $json_att;

            foreach ($customerItemCol as $attribute) {
                //print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $customer->customer_id;
                $customerattribute->value = $attribute['value'];
                //$customerattribute->sno = $attribute['sno'];
                $customerattribute->attribute_code = $attribute['attribute_code'];


                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
                        //print_r($attribute->id);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

                //$customerattribute->fill($attribute);       
                $customerattribute->updated_by = $currentuser->id;
                $customerattribute->save();
            }
        }
        
        $customer->updated_by = $currentuser->id;
        $customer->save();

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Customer Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $customer = Customer::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Customer Not found';
            return $resVal;
        }

        $customer->delete();

        return $resVal;
    }

    public function detail(Request $request) {
       
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $customer_id = $request->input('customer_id');

        $isactive = $request->input('isactive', '');

        $builder = DB::table('tbl_customer_dual')->select('*');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($customer_id)) {
            $builder->where('customer_id', '=', $customer_id);
        }

        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }
        $builder->orderBy('id', 'desc');

        //  $resVal['total'] = $builder->count();
        //  $resVal['invoiceitems'] = $invoiceitems->count();

        $customerCollection = $builder->skip($start)->take($limit)->get();
        if (count($customerCollection) == 1) {

            $customer = $customerCollection->first();


            $customAttributeValue = DB::table('tbl_attribute_value as a')
                            ->leftJoin('tbl_attributes as abs', 'a.attributes_id', '=', 'abs.id')
                            ->select('a.*', 'abs.attribute_label as attribute_label')
                            ->where('a.refernce_id', '=', $customer->customer_id)
                            ->where('abs.attributetype_code', '=', 'crm')->get();



            $customattribute = DB::table('tbl_attributes as abs')
                            ->select('abs.*')
                            ->where('abs.attributetype_code', '=', 'crm')->get();


            if (count($customAttributeValue) > 0) {

                foreach ($customAttributeValue as $item) {

                    foreach ($customattribute as $customattributeItem) {

                        if ($item->attribute_code == $customattributeItem->attribute_code) {

                            $customattributeItem->value = $item->value;
                        }
                    }
                }
            }
            $customer->customattribute = $customattribute;
            $resVal['data'] = $customer;
        }

        return ($resVal);
       
    }
    
    public function update1(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Customer Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $customer = CustomerDual::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Customer Not found';
            return $resVal;
        }
       // $customer->fill($request->all());
        $customer->fname = 'sfsdfasf';
        $customer->save();

        return $resVal;
    }
    
}

?>
