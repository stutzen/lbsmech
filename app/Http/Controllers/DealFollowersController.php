<?php
namespace App\Http\Controllers;
use DB;
use App\DealFollowers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
class DealFollowersController extends Controller
{
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Followers Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $request = $request->all();
        $currentuser = Auth::user();
         foreach ($request as $followers) {
            $follow = new DealFollowers;
            $follow->fill($followers);
            
            $follow->created_by = $currentuser->id;
            $follow->updated_by = $currentuser->id;
            $follow->is_active = 1;
            $follow->save();
        }
        return $resVal;
    }
      
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $dealId = $request->input('dealId', '');
        $empId = $request->input('empId', '');
      //  $builder = DB::table('tbl_deal_followers')
      //          ->select('*');
          $builder = DB::table('tbl_deal_followers as df')
                ->join('tbl_deal as d','d.id','=','df.deal_id')
                ->join('tbl_employee as e','e.id','=','df.emp_id')
                ->select('df.*','d.name as dealName',DB::raw('CONCAT(e.f_name, " ", e.l_name) AS empName'));
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('df.id', '=', $id);
        }
 
        if (!empty($dealId)) {
            $builder->where('deal_id', '=', $dealId);
        }
        
         if (!empty($empId)) {
            $builder->where('emp_id', '=', $empId);
        }
        $builder->orderBy('df.id', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $countryCollection = $builder->get();
        } else {

            $countryCollection = $builder->skip($start)->take($limit)->get();
        }
                
        $resVal['list'] = $countryCollection;
        return ($resVal);
    }
    
    
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Follower Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $df = DealFollowers::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Country Not found';
            return $resVal;
        }
        $df->delete();
        return $resVal;
    }
}
?>
