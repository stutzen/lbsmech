<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Deal;
use App\DealStage;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;
use App\Events\DealNotificationEvent;
use Event;
use App\Helper\MailHelper;
use App\DealActivity;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class DealController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Deal Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $deal = new Deal;

        $deal->created_by = $currentuser->id;
        $deal->updated_by = $currentuser->id;
        $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $deal->fill($request->all());
        $deal->save();

        $resVal['id'] = $deal->id;
        ActivityLogHelper::dealSave($deal);
        ActivityLogHelper::dealDealActivitySave($deal);
        $mail_obj = new MailHelper;
        $mail_obj->DealMailSend($deal);
        $response = Event::fire(new DealNotificationEvent($deal));
        $resVal['notification'] = $response;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active = $request->input('is_active', '');
        $emp_id = $request->input('emp_id');
        $emp_name = $request->input('emp_name', '');

        $stage_id = $request->input('stage_id');
        $stage_name = $request->input('stage_name', '');
        $stage_flag = $request->input('stage_flag');
        $city_id = $request->input('city_id');
        $contact_id = $request->input('contact_id', '');
        $contact_name = $request->input('contact_name', '');
        $mode = $request->input('mode', '');
        $date = $request->input('date', '');
        $fromDate = $request->input('fromDate', '');
        $toDate = $request->input('toDate', '');
        $company_id = $request->input('company_id');
        $created_at = $request->input('created_at');
        $created_by = $request->input('created_by');
        $deal_category_id = $request->input('deal_category_id');
        $source_id = $request->input('source_id');

        $sortById = $request->input('sortById', '');
        $sortByName = $request->input('sortByName', '');
        $sortByStage = $request->input('sortByStage', '');
        $sortByLeads = $request->input('sortByLeads', '');
        $sortByNfUpDate = $request->input('sortByNfUpDate', '');
        $sortByAssignedTo = $request->input('sortByAssignedTo', '');
        $sortByCreatedAt = $request->input('sortByCreatedAt', '');
        $sortByCreatedBy = $request->input('sortByCreatedBy', '');
        $sortByAmount = $request->input('sortByAmount', '');
        $sortDealCategory = $request->input('sortDealCategory', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_deal as d')
                ->leftjoin('tbl_user as u', 'd.created_by', '=', 'u.id')
//                 ->leftjoin('tbl_deal_followers as df','df.deal_id','=','d.id')
                ->leftjoin('tbl_deal_stage as ds', 'd.stage_id', '=', 'ds.id')
                ->leftjoin('tbl_contact as c', 'c.id', '=', 'd.contact_id')
                ->leftjoin('tbl_source as s', 's.id', '=', 'c.source_id')
                ->leftjoin('tbl_company as com', 'com.id', '=', 'c.company_id')
                ->leftjoin('tbl_city as cit', 'com.city_id', '=', 'cit.id')
                ->leftjoin('tbl_employee as e', 'd.emp_id', '=', 'e.id')
                ->leftjoin('tbl_lead_master as lm', 'lm.id', '=', 'deal_category_id')
                ->leftjoin('tbl_lead_master as mas', 'mas.id', '=', 'c.lead_type_id')
                ->select('d.*', 'c.phone as contact_number', 'e.f_name as employee_name', 'com.name as company_name', 'com.address as company_address'
                , 'com.website as company_website', 's.name as source_name', 'lm.name as deal_category_name', 'mas.name as lead_segment', 'mas.id as lead_segment_id'
                , 'ds.stage_flag', 'com.city_id', 'cit.name as city_name');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();
        //print_r($empid);
        if (!empty($empid)) {
            //     print_r("IN EMp".$empid->id);
            $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
            if (!$isVisible) {
                $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid) {
                    $join->on('d.id', '=', 'df.deal_id');
                    $join->where('df.emp_id', '=', DB::raw("'" . $empid->id . "'"));
                });
                $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
            }
        }
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//             $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid){
//                $join->on('d.id', '=', 'df.deal_id');
//                $join->where('df.emp_id','=',DB::raw("'" . $empid->id . "'"));
//             });
//            $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
//        }

        if (!empty($id)) {
            $builder->where('d.id', '=', $id);
        }
        if (!empty($deal_category_id)) {
            $builder->where('d.deal_category_id', '=', $deal_category_id);
        }
        if (!empty($company_id)) {
            $builder->where('com.id', '=', $company_id);
        }
        if (!empty($city_id)) {
            $builder->where('com.city_id', '=', $city_id);
        }

        if ($is_active != '') {
            $builder->where('d.is_active', '=', $is_active);
        }

        if (!empty($fromDate)) {
            $builder->whereDate('d.created_at', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->whereDate('d.created_at', '<=', $toDate);
        }

        if (!empty($contact_name)) {

            $builder->where('d.contact_name', 'like', '%' . $contact_name . '%');
        }
        if (!empty($stage_id)) {
            $builder->where('d.stage_id', '=', $stage_id);
        }
        if (!empty($contact_id)) {
            $builder->where('d.contact_id', '=', $contact_id);
        }
        if (!empty($source_id)) {
            $builder->where('c.source_id', '=', $source_id);
        }
        if (!empty($name)) {
            $builder->where('d.name', 'like', '%' . $name . '%');
        }
        if (!empty($stage_name)) {
            $builder->where('d.stage_name', 'like', '%' . $stage_name . '%');
        }

        if ($mode != '' && $mode == 0 && !empty($date)) {
            $builder->whereDate('d.next_follow_up', '=', $date);
        }

        if ($mode != '' && $mode == 1 && !empty($date)) {
            $builder->whereDate('d.next_follow_up', '<=', $date);
        }
        if ($mode != '' && $mode == 2 && !empty($date)) {
            $builder->whereDate('d.next_follow_up', '>=', $date);
        }
        if (!empty($emp_id)) {
            $builder->where('e.id', '=', $emp_id);
        }
        if (!empty($emp_name)) {
            $builder->where('e.f_name', 'like', '%' . $emp_name . '%');
        }
        if (!empty($created_at)) {
            $builder->whereDate('d.created_at', '=', $created_at);
        }
        if (!empty($created_by)) {
            $builder->where('d.created_by', '=', $created_by);
        }

        $value = $request->all();

        foreach (array_chunk($value, 1, true) as $val) {

            if (!empty($val['sortById'])) {
                if ($val['sortById'] == 1)
                    $builder->orderBy('d.id', 'asc');
                else if ($val['sortById'] == 2)
                    $builder->orderBy('d.id', 'desc');
            }

            if (!empty($val['sortByName'])) {
                if ($val['sortByName'] == 1)
                    $builder->orderBy('d.name', 'asc');
                else if ($val['sortByName'] == 2)
                    $builder->orderBy('d.name', 'desc');
            }


            if (!empty($val['sortByStage'])) {
                if ($val['sortByStage'] == 1)
                    $builder->orderBy('d.stage_name', 'asc');
                else if ($val['sortByStage'] == 2)
                    $builder->orderBy('d.stage_name', 'desc');
            }


            if (!empty($val['sortByLeads'])) {
                if ($val['sortByLeads'] == 1)
                    $builder->orderBy('d.contact_name', 'asc');
                else if ($val['sortByLeads'] == 2)
                    $builder->orderBy('d.contact_name', 'desc');
            }

            if (!empty($val['sortByNfUpDate'])) {
                if ($val['sortByNfUpDate'] == 1)
                    $builder->orderBy('d.next_follow_up', 'asc');
                else if ($val['sortByNfUpDate'] == 2)
                    $builder->orderBy('d.next_follow_up', 'desc');
            }

            if (!empty($val['sortByAssignedTo'])) {
                if ($val['sortByAssignedTo'] == 1)
                    $builder->orderBy('e.f_name', 'asc');
                else if ($val['sortByAssignedTo'] == 2)
                    $builder->orderBy('e.f_name', 'desc');
            }

            if (!empty($val['sortByCreatedAt'])) {
                if ($val['sortByCreatedAt'] == 1)
                    $builder->orderBy('d.updated_at', 'asc');
                else if ($val['sortByCreatedAt'] == 2)
                    $builder->orderBy('d.updated_at', 'desc');
            }

            if (!empty($val['sortByCreatedBy'])) {
                if ($val['sortByCreatedBy'] == 1)
                    $builder->orderBy('u.f_name', 'asc');
                else if ($val['sortByCreatedBy'] == 2)
                    $builder->orderBy('u.f_name', 'desc');
            }

            if (!empty($val['sortByAmount'])) {
                if ($val['sortByAmount'] == 1)
                    $builder->orderBy('d.amount', 'asc');
                else if ($val['sortByAmount'] == 2)
                    $builder->orderBy('d.amount', 'desc');
            }

            if (!empty($val['sortDealCategory'])) {
                if ($val['sortDealCategory'] == 1)
                    $builder->orderBy('d.deal_category_id', 'asc');
                else if ($val['sortDealCategory'] == 2)
                    $builder->orderBy('d.deal_category_id', 'desc');
            }
        }
        if ($stage_flag == 'all') {
            
        } else if (!empty($stage_flag) && $stage_flag != 'all') {
            $builder->where('ds.stage_flag', '=', $stage_flag);
        } else if (empty($stage_id) && empty($id)) {
            $builder->where('ds.stage_flag', '=', 'intermediate');
        }
//        print_r($builder->toSql());
//        $builder->Distinct('d.id');
//         print_r($builder->toSql());
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Deal  Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $deal_value = DB::table('tbl_deal')->where('id', '=', $id)->first();
        try {
            $deal = Deal::findOrFail($id);
            if ($deal->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Deal  Not found';
            return $resVal;
        }
        $deal->updated_by = $currentuser->id;
        $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $deal->fill($request->all());
        $deal->save();

        $old_stage = DB::table('tbl_deal_stage')->where('id', '=', $deal_value->stage_id)->first();
        $new_stage = DB::table('tbl_deal_stage')->where('id', '=', $deal->stage_id)->first();
        if ($deal_value->stage_id != $deal->stage_id) {
            $dealactivity = new DealActivity;

            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $dealactivity->created_by = $currentuser->id;
            $dealactivity->updated_by = $currentuser->id;
            $dealactivity->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $dealactivity->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $dealactivity->time = $time;
            $dealactivity->date = $current_date;
            $dealactivity->type = 'deal_stage_update';
            $dealactivity->description = "Deal name : $deal->name  Stage : $new_stage->name changed from $old_stage->name";
            $dealactivity->contact_id = $deal->contact_id;
            $dealactivity->user_id = $currentuser->id;
            $dealactivity->user_name = $currentuser->f_name;
            $dealactivity->is_active = 1;
            $dealactivity->ref_id = $deal->id;

            $dealactivity->deal_id = $deal->id;
            $dealactivity->comments = "Deal name : $deal->name  Stage : $new_stage->name changed from $old_stage->name";

            $dealactivity->save();
        }
        ActivityLogHelper::dealUpdate($deal);
        ActivityLogHelper::dealDealActivityUpdate($deal);
        $mail_obj = new MailHelper;
        $mail_obj->DealMailSend($deal);
        $response = Event::fire(new DealNotificationEvent($deal));
        $resVal['notification'] = $response;
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Deal  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $deal = Deal::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Deal  Not found';
            return $resVal;
        }

        $deal->updated_by = $currentuser->id;
        $deal->is_active = 0;

        $deal->update();
        $resVal['id'] = $deal->id;
        ActivityLogHelper::dealDelete($deal);
        ActivityLogHelper::dealDealActivityDelete($deal);
        return $resVal;
    }

}

?>
