<?php
namespace App\Http\Controllers;
use DB;
use Validator;
use App\Attachment;
use App\ServiceRequest;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\SmsHelper;

class ServiceRequestController extends Controller
{
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Service Request Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        
        $currentuser=Auth::user();
        $service=new ServiceRequest;      
        $service->updated_by=$currentuser->id;
        $service->created_by=$currentuser->id;
        $service->fill($request->all());
        $service->is_active=1;
        $service->save();
        
        $attachements=$request->input('attachement');
        $user=DB::table('tbl_user')->select('*')->where('id','=',$currentuser->id)->where('is_active','=',1)->first();
        foreach($attachements as $attachement){
        $attach=new Attachment;
        $attach->ref_id=$service->id;
        $attach->updated_by=$currentuser->id;
        $attach->created_by=$currentuser->id;
        $attach->fill($attachement);
        $attach->created_by_name=$user->f_name.''.$user->l_name;
        $attach->updated_by_name=$user->f_name.''.$user->l_name;    
        $attach->is_active=1;
        $attach->save();  
        }
       
        $resVal['id'] = $service->id;
        
        SmsHelper::serviceRequestCreatedNotification($service);


        return $resVal;
    }
      public function update(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Service Request Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $service= ServiceRequest::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Service Request Not found';
            return $resVal;
        }
        $service->updated_by=$currentuser->id;
        $service->is_active=1;
        $service->fill($request->all());
        $service->save();
        
        $attachements=$request->input('attachement');
        
        DB::table('tbl_attachment')->where('ref_id', '=', $id)->delete();
        $user=DB::table('tbl_user')->select('*')->where('id','=',$currentuser->id)->where('is_active','=',1)->first();
        foreach($attachements as $attachement){
                  $attach=new Attachment;      
                  $attach->updated_by=$currentuser->id;
                  $attach->created_by=$currentuser->id;
                  $attach->fill($attachement);
                  $attach->is_active=1;
                  $attach->save();  
        }
        
        if($service->status=='Completed')
        {
            SmsHelper::serviceRequestCompletedtedNotification($service);
        }

        return $resVal;
    }
     public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        $id=$request->input('id');
        $is_active=$request->input('is_active');
        $customer_id=$request->input('customer_id');
        $product_id=$request->input('product_id');
        $model=$request->input('model');
        $customer_name=$request->input('customer_name');
        $product_name=$request->input('product_name');
        $employee_name=$request->input('employee_name');
        $assigned_emp_id=$request->input('assigned_emp_id');
        $date=$request->input('date');
        $from_date=$request->input('from_date');
        $to_date=$request->input('to_date');
        $status=$request->input('status');
        $machine_status=$request->input('machine_status');
        $billing_city_id = $request->input('billing_city_id');
        $billing_city_name = $request->input('billing_city_name');
        $builder=DB::table('tbl_service_request as sr')
                ->leftjoin('tbl_customer as c','sr.customer_id','=','c.id')
                ->leftjoin('tbl_product as p','sr.product_id','=','p.id')
                ->leftjoin('tbl_employee as e','assigned_emp_id','=','e.id')
                ->select('sr.*','c.fname as customer_name','c.company as company_name','p.name as product_name','e.f_name as employee_name','c.billing_address','c.shopping_address','c.phone','c.email','c.billing_city','c.shopping_city','c.city');
                
                
        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        
        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid=DB::table('tbl_employee')->where('user_id','=',$curr_id)->first();

        if(!empty($empid)){
         $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
         if (!$isVisible) {
           $builder->where('sr.assigned_emp_id', '=', $empid->id);
         }
        }
        
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//             $builder->where('sr.assigned_emp_id', '=', $empid->id);
//        }
       
        if(!empty($id)){
            $builder->where('sr.id','=',$id);
        }
        if(!empty($billing_city_id)){
            $builder->where('c.billing_city_id','=',$billing_city_id);
        }
        if(!empty($from_date)){
            $builder->whereDate('sr.date','>=',$from_date);
        }
        if(!empty($to_date)){
            $builder->whereDate('sr.date','<=',$to_date);
        }
        if(!empty($date)){
            $builder->whereDate('sr.date','=',$date);
        }
         if(!empty($assigned_emp_id)){
            $builder->where('sr.assigned_emp_id','=',$assigned_emp_id);
        }
        if(!empty($status)){
            $builder->where('sr.status','=',$status);
        }
        if(!empty($machine_status)){
            $builder->where('sr.machine_status','=',$machine_status);
        }
        if(!empty($customer_id)){
            $builder->where('sr.customer_id','=',$customer_id);
        }
        if(!empty($product_id)){
            $builder->where('sr.product_id','=',$product_id);
        }
        if(!empty($model)){
            $builder->where('sr.model','like','%'.$model.'%');
        }
        if(!empty($billing_city_name)){
            $builder->where('c.billing_city','like','%'.$billing_city_name.'%');
        }
        if(!empty($customer_name)){
            $builder->where('c.fname','like','%'.$customer_name.'%');
        }
        if(!empty($product_name)){
            $builder->where('p.name','like','%'.$product_name.'%');
        }
        if(!empty($employee_name)){
            $builder->where('e.f_name','like','%'.$employee_name.'%');
        }
        if($is_active != ''){
            $builder->where('sr.is_active','=',$is_active);
        }
        $builder = $builder->orderby('sr.id','desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $Collection = $builder->get();
        } else {

            $Collection = $builder->skip($start)->take($limit)->get();
        }
        
        foreach ($Collection as $collect) {
            $id=$collect->id;
            $attachment=DB::table('tbl_attachment')
                    ->select('*')->where('is_active','=',1)->where('ref_id','=',$id)
                    ->where('type','=','servicerequest')->get();
            $collect->attachment=$attachment;
        }
        
        $resVal['list'] = $Collection;
        return ($resVal);
    }
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Service Request Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $service = ServiceRequest::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Service Request Not found';
            return $resVal;
        }
        $currentuser = Auth::user();
        $service->is_active=0;
        $service->updated_by=$currentuser->id;
        $service->save();
        
        DB::table('tbl_attachment')->where('ref_id','=',$id)->where('type','=','servicerequest')->update(['is_active'=>0]);
        return $resVal;
    }
    public function detail(Request $request){
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
        $id=$request->input('id');
        
        $builder=DB::table('tbl_service_request as sr')
                ->leftjoin('tbl_customer as c','sr.customer_id','=','c.id')
                ->leftjoin('tbl_product as p','sr.product_id','=','p.id')
                ->leftjoin('tbl_employee as e','assigned_emp_id','=','e.id')
                ->select('sr.*','c.fname as customer_name','p.name as product_name','e.f_name as employee_name')
                ->where('c.is_active','=',1)
                ->where('p.is_active','=',1)
                ->where('e.is_active','=',1)
                ->where('sr.is_active','=',1)
                ->where('sr.id','=',$id);
        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $Collection = $builder->get();
        } else {

            $Collection = $builder->skip($start)->take($limit)->get();
        }
        
        if(count($Collection)==1){
            $collect=$Collection->first();
            
            $attachment=DB::table('tbl_attachment')
                    ->select('*')->where('is_active','=',1)->where('ref_id','=',$id)
                    ->where('type','=','servicerequest')->get();
            $collect->attachment=$attachment;
        }
        $resVal['list'] = $Collection;
        return ($resVal);
    }
}
?>
