<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeliveryInstructionDetailController
 *
 * @author Stutzen Admin
 */

namespace App\Http\Controllers;

use DB;
use App\DeliveryInstructionDetail;
use App\DeliveryInstruction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;

class DeliveryInstructionDetailController extends Controller {

    public function detailSave(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Delivery Instruction Details Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $dids = $request->all();
        foreach ($dids as $di) {
            if ($di['id'] == 0) {
                $did = new DeliveryInstructionDetail;
                $did->fill($di);
                if ($did->ref_type == 1) { //1 means invoice
                    $builder = DB::table('tbl_invoice')
                                    ->select('customer_id', 'date', 'consignee')->where('id', '=', $did->ref_id)->get();
                    if ($builder != "") {
                        foreach ($builder as $data) {
                            $did->customer_id = $data->customer_id;
                            $did->ref_date = $data->date;
                            $did->consignee = $data->consignee;
                            $qty = DB::table('tbl_invoice_item')
                                    ->where('invoice_id', '=', $did->ref_id)
                                    ->sum('qty');
                            $did->qty = $qty;
                        }
                    }
                } else {  // 2 is sales Order
                    $data = DB::table('tbl_sales_order')
                                    ->select('*')->where('id', '=', $did->ref_id);
                                       if ($data != "") {
                        foreach ($data as $d) {
                            $did->customer_id = $d->customer_id;
                            $did->consignee = $d->consignee;
                            $did->ref_date = $d->date;
                            $qty = DB::table('tbl_sales_order_item')
                                    ->where('sales_order_id', '=', $did->ref_id)
                                    ->sum('qty');
                            $did->qty = $qty;
                        }
                    }
                }
                $did->created_by = $currentuser->id;
                $did->updated_by = $currentuser->id;
                $did->is_active = 1;
                $did->save();
            } else {
                try {
                    $did = DeliveryInstructionDetail::findOrFail($di['id']);
                } catch (ModelNotFoundException $e) {

                    $resVal['success'] = FALSE;
                    $resVal['message'] = ' Details  Not found';
                    return $resVal;
                }

                $did->updated_by = $currentuser->id;
                $did->is_active = 1;
                $did->fill($di);
                $did->save();
            }
            DeliveryInstructionDetailController::UpdateDetails($request);
        }
        return $resVal;
    }

    public function UpdateDetails($request) {
        $freight = 0.00;
        $volume = 0.00;
        $netWeight = 0.00;
        $grossWeight = 0.00;
        $qty = 0.00;
        $noOfPackets = 0.00;
        $driver_beta = 0.00;
        $unloading = 0.00;
        $drop_seq = 0.00;
        $deliveryId = 0;
        $dids = $request->all();
        foreach ($dids as $did) {
            if (empty($did['delivery_instruction_id'])) {
                          $data = DeliveryInstructionDetail::where('delivery_instruction_id', "=", $did['id'])->first();
                                      $deliveryId = $data['delivery_instruction_id'];
            } else {
                $deliveryId = $did['delivery_instruction_id'];
            }
        }
        $ds = DB::table('tbl_delivery_instruction_detail')
                        ->select('*')
                        ->where('delivery_instruction_id', $deliveryId)->get();
        if ($ds != null) {
            foreach ($ds as $ds1) {
                $freight = $ds1->freight + $freight;
                $volume = $ds1->volume + $volume;
                $netWeight = $ds1->net_weight + $netWeight;
                $grossWeight = $ds1->gross_weight + $grossWeight;
                $qty = $ds1->qty + $qty;
                $noOfPackets = $ds1->no_of_pkts + $noOfPackets;
                $unloading = $ds1->unloading + $unloading;
                $drop_seq = $ds1->drop_seq + $drop_seq;
                $driver_beta = $ds1->driver_beta + $driver_beta;
            }
        }
        if ($deliveryId != 0) {
            $collection = DeliveryInstruction::where('id', "=", $deliveryId)->get();

            if ($collection != null) {
                $di = $collection->first();
                $di->freight = $freight;
                $di->volume = $volume;
                $di->net_weight = $netWeight;
                $di->gross_weight = $grossWeight;
                $di->qty = $qty;
                $di->no_of_pkts = $noOfPackets;
                $di->driver_beta = $driver_beta;
                $di->unloading = $unloading;
                $di->drop_seq = $drop_seq;
                $di->save();
            }
        }
    }

    public function delete(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Deleted successfully';
        $did = $request->all();
        foreach ($did as $aa) {
            if ($aa['id'] != NULL) {
                $collection = DeliveryInstructionDetail::where('id', "=", $aa['id'])
                        ->get();
            }
            if (count($collection) > 0) {
                $did = $collection->first();
                $detailDelete = DB::table('tbl_delivery_instruction_detail')->where('id', $did->id)->delete();
                DeliveryInstructionDetailController::UpdateDetails($request);
            }
        }
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $delivery_instruction_id = $request->input('delivery_instruction_id', '');
        $ref_type = $request->input('ref_type', '');
        $ref_id = $request->input('ref_id', '');
        $customer_id = $request->input('customer_id', '');
        $is_active = $request->input('is_active');

        $builder = DB::table('tbl_delivery_instruction_detail as did')
                ->leftJoin('tbl_customer as c', 'did.customer_id', '=', 'c.id')
                ->leftJoin('tbl_customer as c1', 'did.consignee', '=', 'c1.id')
                ->leftJoin('tbl_invoice AS i', function($join) {
                    $join->on('did.ref_type', '=', DB::raw("1"))
                    ->on('did.ref_id', '=', 'i.id');
                })
                ->leftJoin('tbl_sales_order AS s', function($joins) {
                    $joins->on('did.ref_type', '=', DB::raw("2"))
                    ->on('did.ref_id', '=', 's.id');
                })
                ->distinct('did.id')
                ->select('did.*', 'c1.shopping_city as consignee_city', 'i.invoice_code', 's.sales_order_code', DB::raw('CONCAT(c.fname, " ", c.lname) AS customer_name'), DB::raw('CONCAT(c.fname, " ", c.lname) AS consignee_name'));
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('did.id', '=', $id);
        }
        if (!empty($delivery_instruction_id)) {
            $builder->where('did.delivery_instruction_id', '=', $delivery_instruction_id);
        }
        if (!empty($ref_type)) {
            $builder->where('did.ref_type', '=', $ref_type);
        }
        if (!empty($ref_id)) {
            $builder->where('did.ref_id', '=', $ref_id);
        }

        if (!empty($customer_id)) {
            $builder->where('did.customer_id', '=', $customer_id);
        }


        if (!empty($is_active)) {
            $builder->where('did.is_active', '=', $is_active);
        }

        $builder->orderBy('did.id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

}
