<?php
namespace App\Http\Controllers;
use DB;
use App\Item;
use App\Uom;
use Illuminate\Http\Request;
use App\Transformer\ItemTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemController
 *
 * @author Deepa
 */
class ItemController extends Controller{
    
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Item Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        
       $item = new Item ;
       
       
        $item->created_by = $currentuser->id;
        $item->updated_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();
//         
      $resVal['id'] = $item->id;

        return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
        
        $isactive= $request->input('is_active','');
        $name = $request->input('name','');
        $sku = $request->input ('sku','');
        $type =$request->input('type','');
        $issale  = $request->input('is_sale','');
        $ispurchase = $request->input('is_purchase','');
        
        $builder = DB::table('tbl_product')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($issale)) {
            $builder->where('is_sale', '=', $issale);
        }
        if (!empty($ispurchase)) {
            $builder->where('is_purchase', '=', $ispurchase);
        }
        
        
        
        
         if (!empty($name)) {
            $builder->where('name', 'like','%'.$name .'%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        
        
         if(!empty($sku)){
             $builder->where('sku','like','%' .$sku . '%');
         }
        
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }
       if (!empty($type)) {
            $builder->where('type', 'like','%'.$type .'%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('id','desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

     public function delete($id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Item Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $item = Item::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Item Not found';
            return $resVal;
        }

        $item->delete();

        return $resVal;
    }
    
    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Item Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $item = Item::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Item Not found';
            return $resVal;
        }
        $item->updated_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();

        return $resVal;
    }
    

   
}

?>
