<?php
namespace App\Http\Controllers;
use DB;
use Validator;
use App\Albumassetcomments;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\SmsHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class AlbumassetcommentsController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Album asset comments Added Successfully.';
        $resVal['success'] = TRUE;
        // $currentuser = Auth::user();
        $album = new Albumassetcomments;
        $album->created_by = 1;
        $album->updated_by = 1;
        $album->is_active = $request->input('is_active', 1);
        $album->fill($request->all());
        $album->save();
        $resVal['id'] = $album->id;
        //$smssend=$this->testsendsms($request);
        return $resVal;
    }
      public function listAll(Request $request) {
        $resVal = array();
        $id = $request->input('id');
        $Album_id = $request->input('album_id', '');
        $Album_Asset_id = $request->input('album_asset_id');
        $Album_code = $request->input('album_code', '');
        $Comments = $request->input('comments', '');
        $is_active = $request->input('is_active', '');
         $created_by_name = $request->input('created_by_name', '');
          $updated_by_name = $request->input('updated_by_name', '');
          $builder = DB::table('tbl_album_asset_comments')
                ->select('*');
        $resVal['success'] = TRUE;
         $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
 if (!empty($Album_id)) {
            $builder->where('Album_id', '=' ,$Album_id);
        }
        if (!empty($Album_Asset_id)) {
            $builder->where('album_asset_id', '=' ,$Album_Asset_id);
        }
        if (!empty($Album_code)) {
            $builder->where('$Album_code', 'like', '%' . $Album_code . '%');
        }
        
       if (!empty($Comments)) {
            $builder->where('Comments', 'like', '%' . $Comments . '%');
        }
        if (!empty($created_by_name)) {
            $builder->where('created_by_name', 'like', '%' . $created_by_name . '%');
        }
        if (!empty($updated_by_name)) {
            $builder->where('updated_by_name', 'like', '%' . $updated_by_name . '%');
        }
       
        $builder->orderBy('id', 'asc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
       
        return $resVal;
    }
    
    
    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Album asset comments record Update Successfully';

        try {
            $album = Albumassetcomments::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Album asset comments record Not found';
            return $resVal;
        }
        $album->created_by = 1;
        $album->fill($request->all());
        $album->save();

        return $resVal;
    }
      public function delete(Request $request, $id) {

        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Album_asset_comments record Delete Successfully';

        try {
            $album = Albumassetcomments::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Album_asset_comments Not found';
            return $resVal;
        }

        $album->delete();
        return $resVal;
    }

}

?>
