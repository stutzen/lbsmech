<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class GeneralController extends Controller {

    //put your code here    
    public function findCode(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success.';
        $resVal['success'] = TRUE;
        $prefix = $request->prefix;
        $type = $request->type;
        if ($type == 'sales_order') {

            $get_inv_no = DB::table('tbl_sales_order')
                    ->select('sales_order_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('sales_order_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $sales_no = $get_inv_no->sales_order_no;
                $sales_no = $sales_no + 1;
            } else {
                $sales_no = 1;
            }
            $ret_no = $sales_no;
            $ret_code = $prefix . $sales_no;
        } else if ($type == 'sales_invoice') {

            $get_inv_no = DB::table('tbl_invoice')
                    ->select('invoice_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('invoice_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->invoice_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }

            $ret_no = $inv_no;
            $ret_code = $prefix . $inv_no;
        } else if ($type == 'purchase_invoice') {

            $get_inv_no = DB::table('tbl_purchase_invoice')
                    ->select('purchase_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('purchase_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->purchase_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $ret_no = $inv_no;
            $ret_code = $prefix . $inv_no;
        } else if ($type == 'purchase_order') {

            $get_inv_no = DB::table('tbl_purchase_quote')
                    ->select('quote_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('quote_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $inv_no = $get_inv_no->quote_no;
                $inv_no = $inv_no + 1;
            } else {
                $inv_no = 1;
            }
            $ret_no = $inv_no;
            $ret_code = $prefix . $inv_no;
        }

        $resVal['no']=$ret_no;
        $resVal['prefix']=$prefix;
        return $resVal;
    }

}

?>
