<?php

namespace App\Http\Controllers;
use DB;
use Validator;
use App\ProductType;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation;
use App\Helper\AuthorizationHelper; 

class ProductTypeController extends Controller{ 
    
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'ProductType Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $requestcode = $request->input('code');
        $code = DB::table('tbl_product_type')
                ->select('*')
                ->where('code','=',$request->code)
                ->where('is_active','=',1)
                ->first();  
           if(!empty($code) && !empty($requestcode)){
               $resVal['message'] = 'Code is already exist';
               return $resVal;
           }  
           $name = DB::table('tbl_product_type')
                ->select('name')
                ->where('name','=',$request->name)
                ->where('is_active','=',1)
                ->first();
        
        if(!empty($name)){
            $resVal['message'] = 'name is already exist';
            return $resVal;
        }
        
        $currentuser=Auth::user();
        $productType = new ProductType;      
        $productType->updated_by=$currentuser->id;
        $productType->created_by=$currentuser->id;
        $productType->fill($request->all()); 
        $productType->is_active = 1;
        $productType->save();
        $resVal['id'] = $productType->id;
        return $resVal;
    
    }  
    
    public function update(Request $request,$id) 
    {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'ProductType Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $productType = ProductType::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'ProductType Not found';
            return $resVal;
        }
            $code = DB::table('tbl_product_type')
                ->select('*')
                ->where('code','=',$request->code)
                ->where('id','!=',$id)
                ->where('is_active','=',1)
                ->first();  
            if(!empty($code) && !empty($request->code)){
                $resVal['message'] = 'ProductType Code is already exist';
                $resVal['success'] = FALSE;
                return $resVal;
            }
            $name = DB::table('tbl_product_type')
                ->select('*')
                ->where('name','=',$request->name)
                ->where('id','!=',$id)
                ->where('is_active','=',1)
                ->first();  
            if(!empty($name)){
                $resVal['message'] = 'ProductType name is already exist';
                $resVal['success'] = FALSE;
                return $resVal;
            }
            
                 $productType->created_by = $currentuser->id;
                 $productType->updated_by=$currentuser->id;
                 $productType->fill($request->all());
                 $productType->save();
              return $resVal; 

       }
        
        public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $code = $request->input('code','');
        $builder = DB::table('tbl_product_type')
                ->select('*')
                ->where('is_active','=',1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
 
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        } 
        
        if(!empty($code)){
            $builder->where('code','=',$code);
        }
        $builder->orderBy('name', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $productTypeCollection = $builder->get();
        } else {

            $productTypeCollection = $builder->skip($start)->take($limit)->get();
        }
                
        $resVal['list'] = $productTypeCollection;
        return ($resVal);
    }
    
    public function delete(Request $request,$id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'ProductType Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');    
        $currentuser = Auth::user();
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $productType = ProductType::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'ProductType Not found';
            return $resVal;
        }
        $productType->is_active = 0;
        $productType->updated_by = $currentuser->id;
        $productType->save();
        return $resVal;
    }
    
}
