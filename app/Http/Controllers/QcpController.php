<?php

namespace App\Http\Controllers;
use DB;
use Validator;
use App\Qcp;
use App\QcHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\GeneralHelper;

class QcpController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Qcp Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $type = $request->input('type', '');
        $product_id = $request->input('product_id', '');
        $currentuser = Auth::user();
        $existType = DB::table('tbl_qcp')
                ->select('*')
                ->where('type', '=', $type)
                ->where('product_id', '=', $product_id)
                ->where('is_active', '=', 1)
                ->first();
        if (!empty($existType)) {
            $resVal['message'] = 'title and id is already exist';
            return $resVal;
        }
        $qcp = new Qcp;
        $qcp->created_by = $currentuser->id;
        $qcp->updated_by = $currentuser->id;
        $qcp->is_active = 1;
        $qcp->fill($request->all());
        $qcp->save();

        $resVal['id'] = $qcp->id;
        return $resVal;
    }

    
    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Qcp  Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $type = $request->input('type', '');
        $product_id = $request->input('product_id', '');
        try {
            $qcp = Qcp::findOrFail($id);
            if ($qcp->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Qcp  Not found';
            return $resVal;
        }
        $existType = DB::table('tbl_qcp')
                ->select('*')
                ->where('type', '=', $type)
                ->where('product_id', '=', $product_id)
                ->where('is_active', '=', 1)
                ->where('id', '!=', $id)
                ->first();
        if (!empty($existType)) {
            $resVal['message'] = 'title and id is already exist';
            return $resVal;
        }
        $qcp->updated_by = $currentuser->id;
        $qcp->fill($request->all());
        $qcp->save();
        return $resVal;
    }

    
    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Qcp  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $qcp = Qcp::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Qcp  Not found';
            return $resVal;
        }

        $qcp->updated_by = $currentuser->id;
        $qcp->is_active = 0;

        $qcp->save();
        $resVal['id'] = $qcp->id;
        return $resVal;
    }

    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $title = $request->input('title', '');
        $type = $request->input('type', '');
        $product_id = $request->input('product_id', '');
        $builder = DB::table('tbl_qcp')
                ->select('*')
                ->where('is_active', '=', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }

        if (!empty($title)) {
            $builder->where('title', 'like', '%' . $title . '%');
        }

        if (!empty($type)) {
            $builder->where('type', 'like', '%' . $type . '%');
        }

        if (!empty($product_id)) {
            $builder->where('product_id', '=', $product_id);
        }
        $builder->orderBy('title', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $qcpCollection = $builder->get();
        } else {

            $qcpCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $qcpCollection;
        return ($resVal);
    }

    public function qualityCheck(Request $request) {
        $resVal['message'] = 'Qc Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $datas = array();
        $qty = 0;
        if ($request->ref_type == 'purchase_invoice_item') {
            $purchase = DB::table('tbl_purchase_invoice_item')
                    ->where('id', '=', $request->ref_id)
                    ->first();
            if ($purchase != '') {
                $qty = $purchase->qty;
            }

            DB::table('tbl_purchase_invoice_item')->where('id', '=', $request->ref_id)
                    ->update(['passed_qty' => DB::raw("passed_qty+" . $request->passed_qty), 'failed_qty' => DB::raw("failed_qty+" . $request->failed_qty)]);

            DB::table('tbl_purchase_invoice_item')->where('id', '=', $request->ref_id)
                    ->whereColumn('qty', '=', DB::raw("(passed_qty+failed_qty)"))
                    ->update(['qc_status' => 'done']);

            // GeneralHelper::InventoryUpdate($request->product_id, $request->failed_qty, 'qty_decrease');
            if ($request->failed_qty > 0) {
                $product = DB::table('tbl_product')->where('id', '=', $request->product_id)->where('is_active', '=', 1)->first();
                if ($product != '') {
                    $data['uom_id'] = $product->uom_id;
                    $data['product_sku'] = $product->sku;
                } else {
                    $data['uom_id'] = 0;
                    $data['product_sku'] = '';
                }
                $data['product_id'] = $request->product_id;
                $data['qty'] = $request->failed_qty;
                $data['comments'] = 'From Purchase Invoice Quality Check';
                array_push($datas, $data);
                GeneralHelper::StockWastageSave($datas, 'purchase_invoice_quality_check', $request->ref_id);
            }
        } else if ($request->ref_type == 'mo') {
            $mo = DB::table('tbl_manufacturing_order')
                    ->where('id', '=', $request->ref_id)
                    ->first();
            if ($mo != '') {
                $qty = $mo->qty;
            }


            DB::table('tbl_manufacturing_order')->where('id', '=', $request->ref_id)
                    ->update(['passed_qty' => DB::raw("passed_qty+$request->passed_qty"), 'failed_qty' => DB::raw("failed_qty+$request->failed_qty")]);

            DB::table('tbl_manufacturing_order')->where('id', '=', $request->ref_id)
                    ->whereColumn('qty', '=', DB::raw("(passed_qty+failed_qty)"))
                    ->update(['qc_status' => 'done']);

            GeneralHelper::InventoryUpdate($request->product_id, $request->tested_qty, 'qty_increase');
            if ($request->failed_qty > 0) {
               $product = DB::table('tbl_product')->where('id', '=', $request->product_id)->where('is_active', '=', 1)->first();
                if ($product != '') {
                    $data['uom_id'] = $product->uom_id;
                    $data['product_sku'] = $product->sku;
                } else {
                    $data['uom_id'] = 0;
                    $data['product_sku'] = '';
                }
                $data['product_id'] = $request->product_id;
                $data['qty'] = $request->failed_qty;
                $data['comments'] = 'From MO Quality Check';
                 array_push($datas, $data);
                GeneralHelper::StockWastageSave($datas, 'mo_quality_check', $request->ref_id);
            }
        } else if ($request->ref_type == 'grn') {
            $grn = DB::table('tbl_grn_detail')
                    ->where('id', '=', $request->ref_id)
                    ->first();
            if ($grn != '') {
                $qty = $grn->qty;
            }

            DB::table('tbl_grn_detail')->where('id', '=', $request->ref_id)
                    ->update(['passed_qty' => DB::raw("passed_qty+$request->passed_qty"), 'failed_qty' => DB::raw("failed_qty+$request->failed_qty")]);

            DB::table('tbl_grn_detail')->where('id', '=', $request->ref_id)
                    ->whereColumn('qty', '=', DB::raw("(passed_qty+failed_qty)"))
                    ->update(['qc_status' => 'done']);

            //GeneralHelper::InventoryUpdate($request->product_id, $request->passed_qty, 'qty_increase');
        }
        else if ($request->ref_type == 'work_order') {
            $product_id=$request->input('product_id','');
            $workOrder = DB::table('tbl_work_order')
                    ->where('id', '=', $request->ref_id)
                    ->first();
            if ($workOrder != '') {
                $qty = $workOrder->produced_qty;
            }

            DB::table('tbl_work_order')->where('id', '=', $request->ref_id)
                    ->update(['passed_qty' => DB::raw("passed_qty+$request->passed_qty"), 'failed_qty' => DB::raw("failed_qty+$request->failed_qty")]);

            DB::table('tbl_work_order')->where('id', '=', $request->ref_id)
                    ->whereColumn('produced_qty', '=', DB::raw("(passed_qty+failed_qty)"))
                    ->update(['qc_status' => 'done']);
            
             if(!empty($product_id))
                     GeneralHelper::InventoryUpdate($product_id, $request->passed_qty, 'qty_increase');
        }
        $request->overall_qty = $qty;

        QcpController::historySave($request);
        return $resVal;
    }

    public static function historySave($request) {

        $currentUser = Auth::user();

        $qc = New QcHistory;
        $qc->fill($request->all());
        $qc->is_active = 1;
        $qc->created_by = $currentUser->id;
        $qc->updated_by = $currentUser->id;
        $qc->overall_qty = $request->overall_qty;
        $qc->save();

        $resVal['id'] = $qc->id;

        return $resVal;
    }

}
