<?php

namespace App\Http\Controllers;

use DB;
//use App\SysCategory;
use App\TransationCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SysCategoryController
 *
 * @author Deepa
 */
class TransationCategoryController extends Controller {

    //put your code here 
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'TransationCategory Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();


        $syscategory = new TransationCategory;

        $transactionCollection = TransationCategory::where('name', "=", $request->input('name'))
                        ->where('type', '=', $request->input('type'))
                        ->where('is_active', '=', 1)->get();
        if (count($transactionCollection) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'name And Type is already exits';
            return $resVal;
        }
        if ($request->input('name') && $request->input('type')) {
            $syscategory->created_by = $currentuser->id;
            $syscategory->updated_by = $currentuser->id;
            $syscategory->is_active = $request->input('is_active', 1);

            $syscategory->fill($request->all());
            $syscategory->save();
        } else {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'name And Type is invalid';
            return $resVal;
        }


        $resVal['id'] = $syscategory->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id', '');
        $type = $request->input('type', '');
        $name = $request->input('name', '');
        $isactive = $request->input('is_active', '');


        //$status = $request->input('status', '');
        $builder = DB::table('tbl_tran_category')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($type)) {
            $builder->where('type', '=', $type);
        }
        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
        }
        if ($isactive != '') {
            $builder->where('is_active', '=', $isactive);
        }


        $builder->orderBy('seq_no', 'asc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'TransationCategory Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $syscategory = TransationCategory::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'TransationCategory Not found';
            return $resVal;
        }
        $syscategory->created_by = $currentuser->id;
        $syscategory->fill($request->all());
        $syscategory->save();

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'TransationCategory Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $syscategory = TransationCategory::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'TransationCategory Not found';
            return $resVal;
        }
        $syscategory->is_active = 0;
        $syscategory->update();

        return $resVal;
    }

}

?>
