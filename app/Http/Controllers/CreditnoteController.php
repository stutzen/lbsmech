<?php
namespace App\Http\Controllers;
use DB;
use App\Creditnote;
use Illuminate\Http\Request;
//use App\Transformer\ItemTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CreditnoteController
 *
 * @author Deepa
 */
class CreditnoteController extends Controller {
    //put your code here
  public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Credit note Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
      
        $creditnote = new Creditnote;
       
        $creditnote->created_by = $currentuser->id;
        $creditnote->updated_by = $currentuser->id;
        $creditnote->fill($request->all());
        $creditnote->save();

        $resVal['id'] = $creditnote->id;

        return $resVal;
    }
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Credit Note Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $creditnote = Creditnote::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Credit Note Not found';
            return $resVal;
        }
        $creditnote->updated_by = $currentuser->id;
        $creditnote->fill($request->all());
        $creditnote->save();

        return $resVal;
    }
    public function delete($id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Credit note Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $creditnote = Creditnote::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Credit Note Not found';
            return $resVal;
        }

        $creditnote->delete();

        return $resVal;
    }
     public function listAll(Request $request) {
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
       
        $builder = DB::table('tbl_credit_note')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
       
        $builder->orderBy('id','desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        
        return ($resVal);
    }
}

?>
