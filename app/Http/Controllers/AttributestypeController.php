<?php
namespace App\Http\Controllers;
use DB;
use App\Attributestype;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AttributestypesController
 *
 * @author Deepa
 */
class AttributestypeController extends Controller {
    //put your code here
    
     public function listAll(Request $request) {
         $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       
        $id = $request->input('id');
        $code = $request->input('code', '');
        $is_active =$request->input('is_active','');
        
        $builder = DB::table('tbl_attribute_types')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($code)) {
            $builder->where('code', '=', $code);
        }
         if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
         
       
        $builder->orderBy('id','desc');
        
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }
}

?>
