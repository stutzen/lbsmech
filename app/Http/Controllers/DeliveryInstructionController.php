<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeliverInstructionController
 *
 * @author Stutzen Admin
 */

namespace App\Http\Controllers;

use DB;
use App\DeliveryInstruction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

class DeliveryInstructionController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Delivery Details Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $di = new DeliveryInstruction;
        $di->created_by = $currentuser->id;
        $di->updated_by = $currentuser->id;
        $di->is_active = 1;
        $di->fill($request->all());
        $builder = DB::table('tbl_transport')
                        ->select('*')->where('id', '=', $di->transport_id)->get();
        if ($builder != "") {
            foreach ($builder as $data) {
                $di->transport_name = $data->name;
            }
        }
        $di->save();
        //$did = $request->input('detail');
        //DeliveryInstructionController::DetailSave($di,$did);
        $resVal['id'] = $di->id;
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Delivery Details  Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $di = DeliveryInstruction::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Delivery Details Details  Not found';
            return $resVal;
        }
        $di->updated_by = $currentuser->id;
        $di->is_active = 1;
        $di->fill($request->all());
        $builder = DB::table('tbl_transport')
                        ->select('*')->where('id', '=', $di->transport_id)->get();
        if ($builder != "") {
            foreach ($builder as $data) {
                $di->transport_name = $data->name;
            }
        }
        $di->save();
        //$detailDelete = DB::table('tbl_delivery_instruction_detail')->where('delivery_instruction_id', $id)->delete();
        // $did = $request->input('detail');
        //DeliveryInstructionController::DetailSave($di,$did);
        $resVal['id'] = $di->id;
        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Delivery Details Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        $currentuser = Auth::user();
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $di = DeliveryInstruction::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Delivery Details Details Not found';
            return $resVal;
        }
        $di->updated_by = $currentuser->id;
        $di->is_active = 0;
        $di->save();
        $detail = DB::table('tbl_delivery_instruction_detail')->where('delivery_instruction_id', $id)->update(['is_active' => 0]);
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $transport_id = $request->input('transport_id', '');
        $transport_name = $request->input('transport_name', '');
        $vehicle_no = $request->input('vehicle_no', '');
        $status = $request->input('status', '');
        $final_destination = $request->input('final_destination', '');
        $is_active = $request->input('is_active');

        $builder = DB::table('tbl_delivery_instruction')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($transport_id)) {
            $builder->where('transport_id', '=', $transport_id);
        }
        if (!empty($transport_name)) {
            $builder->where('transport_name', 'like', '%' . $transport_name . '%');
        }
        if (!empty($vehicle_no)) {
            $builder->where('vehicle_no', 'like', '%' . $vehicle_no . '%');
        }

        if (!empty($status)) {
            $builder->where('status', 'like', '%' . $status . '%');
        }

        if (!empty($final_destination)) {
            $builder->where('final_destination', 'like', '%' . $final_destination . '%');
        }

        if (!empty($is_active)) {
            $builder->where('is_active', '=', $is_active);
        }

        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id', '');
        $isactive = $request->input('isactive', '');

        $builder = DB::table('tbl_delivery_instruction as di')
                ->select('di.*')
                ->where('di.id', '=', $id);


        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('di.id', '=', $id);
        }
        if ($isactive != '') {
            $builder->where('di.is_active', '=', $isactive);
        }
        $builder->orderBy('di.id', 'desc');
        $resVal['total'] = $builder->count();
        $deliveryInstruction = $builder->skip($start)->take($limit)->get();
        foreach ($deliveryInstruction as $deliveryI) {
            $detail = DB::table('tbl_delivery_instruction_detail as did')
                    ->leftJoin('tbl_customer as c', 'did.customer_id', '=', 'c.id')
                    ->select('did.*', DB::raw('CONCAT(c.fname, " ", c.lname) AS customer_name'))
                    ->where('did.delivery_instruction_id', $id);
            $deliveryI->detail = $detail->get();
        }
        $resVal['data'] = $deliveryInstruction;
        return ($resVal);
    }

    public function getUnMappedInvoice(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
 $id = $request->input('id');

        $builder = DB::table('tbl_invoice as i')
                ->leftJoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                ->leftJoin('tbl_customer as c1', 'i.consignee', '=', 'c1.id')
                ->select('i.*', 'c1.shopping_city as consignee_city', DB::raw('CONCAT(c.fname, " ", c.lname) AS customer_name'), DB::raw('CONCAT(c.fname, " ", c.lname) AS consignee_name'))
                ->whereNotExists(function($query) {
            $query->select(DB::raw("1 from `tbl_delivery_instruction_detail` as `did` "
                            . " where `did`.`ref_id` = i.id and `did`.`ref_type` = 1 and `did`.`is_active` = 1"));
        });

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
//        $builder->orderBy('i.id', 'desc');
        $resVal['total'] = $builder->count();
        
        if (!empty($id)) {
            $builder->where('i.id', '=', $id);
        }

        $builder->orderBy('i.id', 'desc');
        // DB::connection()->enableQueryLog();
//        if ($start == 0 && $limit == 0) {
//            $resVal['list'] = $builder->get();
//        } else {
//
//            $resVal['list'] = $builder->skip($start)->take($limit)->get();
//        }
//        $builder = DB::getQueryLog();
//       $last_query = end($builder);
//       echo 'Query<pre>';
//       print_r($last_query);
          $invoiceCollections = $builder->skip($start)->take($limit)->get();
        if ($invoiceCollections !='' && $invoiceCollections != null) {

            foreach($invoiceCollections as $invoiceCollection){
           // $invoice = $invoiceCollection;
              //  print_r($invoiceCollection->id);
            //$invoice->item = $invoiceitems->get();

//$invoice->customattribute = $customerattribute->get();
            $customAttributeValue = DB::table('tbl_attribute_value as a')
                            ->leftJoin('tbl_attributes as abs', 'a.attributes_id', '=', 'abs.id')
                            ->select('a.*', 'abs.attribute_label as attribute_label')
                            ->where('a.refernce_id', '=', $invoiceCollection->id)
                            ->where('abs.attributetype_code', '=', 'invoice')->get();


            $customattribute = DB::table('tbl_attributes as abs')
                            ->select('abs.*')
                            ->where('abs.attributetype_code', '=', 'invoice')->get();
         //   print_r($customattribute);
            if ($customAttributeValue != null || $customAttributeValue != '') {

                foreach ($customAttributeValue as $item) {

                    foreach ($customattribute as $customattributeItem) {

                        if ($item->attribute_code == $customattributeItem->attribute_code) {

                            $customattributeItem->value = $item->value;
                          //  print_r($customattributeItem);
                        }
                    }
                }
            }

            $invoiceCollection->customattribute = $customattribute;

           
            }
            $resVal['data'] = $invoiceCollections;
        }

        return ($resVal);
    }


}
