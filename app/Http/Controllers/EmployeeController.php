<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Helper\GeneralHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\MailHelper;

class EmployeeController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Employee Details Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $emp = DB::table('tbl_employee')->where('emp_code', '=', $request->input('emp_code'))->where('is_active', '=', 1)->count();
        if ($emp > 0) {
            $validator = Validator::make($request->all(), [
                        'emp_code' => 'required|unique:tbl_employee,emp_code'
            ]);
            if ($validator->fails()) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Error in request format';
                if (array_key_exists('emp_code', $validator->failed())) {
                    $resVal['message'] = 'Employee Code is already exist';
                }

                return $resVal;
            }
        }
        if ($request->input('is_user') == 1) {
            $validator = Validator::make($request->all(), [
                        'email' => 'required|unique:tbl_employee,email'
            ]);
            if ($validator->fails()) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Error in request format';
                if (array_key_exists('email', $validator->failed())) {
                    $resVal['message'] = 'Email is already exist';
                }

                return $resVal;
            }
        }
        $currentuser = Auth::user();

        if ($request->input('is_user') == 1) {
            $validator = Validator::make($request->all(), [
                        'email' => 'required|unique:tbl_user,username'
            ]);
            if ($validator->fails()) {
                $user = User::where('email', "=", $request->input('email'))
                        ->first();
            } else {
                $role = DB::table('tbl_role')->where('name', '=', 'Employee')->first();
                $user = new User;
                $user->updated_by = $currentuser->id;
                $user->created_by = $currentuser->id;
                $user->user_type = $request->input('user_type', 'employee');
                $user->role_id = $role->id;
                $user->rolename = $role->name;
                $user->fill($request->all());
                $user->username = $request->input('email');
                $user->status='invited';
                $user->save();

                $mail_obj = new MailHelper;
                $mail_obj->userInivitationNotification($user);
            }
        }

        $employee = new Employee;
        if ($request->input('esi_percentage') != '' || $request->input('esi_percentage') != 0) {
            $employee->esi_amount = $request->input('esi_percentage') / 100;
        }

        if ($request->input('pf_percentage') != '' || $request->input('pf_percentage') != 0) {
            $employee->pf_amount = $request->input('pf_percentage') / 100;
        }
        if ($request->input('employer_esi_percentage') != '' || $request->input('employer_esi_percentage') != 0) {
            $employee->employer_esi_amount = $request->input('employer_esi_percentage') / 100;
        }

        if ($request->input('employer_pf_percentage') != '' || $request->input('employer_pf_percentage') != 0) {
            $employee->employer_pf_amount = $request->input('employer_pf_percentage') / 100;
        }
        $employee->updated_by = $currentuser->id;
        $employee->created_by = $currentuser->id;
        $employee->fill($request->all());
        $team_id = $request->input('team_id');
        if ($request->input('is_user') == 1)
            $employee->user_id = $user->id;
        $employee->save();
        GeneralHelper::incrementEmployee($team_id);
        $resVal['id'] = $employee->id;

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Details Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $currentuser = Auth::user();
        $exist = DB::table('tbl_employee')
                        //->where(['emp_code'=>$request->input('emp_code'),'id'=>$id])->get();
                        ->where('emp_code', '=', $request->input('emp_code'))
                        ->where('id', '!=', $id)->where('is_active', '=', 1)->get();
        if (count($exist) > 0) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Code already exist';
        } else {
            try {
                $employee = Employee::findOrFail($id);
            } catch (ModelNotFoundException $e) {

                $resVal['success'] = FALSE;
                $resVal['message'] = 'Employee Not found';
                return $resVal;
            }
            if ($request->input('esi_percentage') != '' || $request->input('esi_percentage') != 0) {
                $employee->esi_amount = $request->input('esi_percentage') / 100;
            }

            if ($request->input('pf_percentage') != '' || $request->input('pf_percentage') != 0) {
                $employee->pf_amount = $request->input('pf_percentage') / 100;
            }

            if (($employee->is_user == 1) && ($request->input('is_user') == 1) && ($employee->email != $request->input('email'))) {
                $user_id = $employee->user_id;
                $email = DB::table('tbl_user')->where('email', '=', $request->input('email'))->where('is_active', '=', 1)->where('id', '!=', $user_id)->count();
                if ($email > 0) {

                    $resVal['success'] = FALSE;
                    $resVal['message'] = 'Email is already exist';
                } else {
                    $user_det = User::where('id', "=", $user_id)->first();
                    if ($user_det != NULL) {
                        $role = DB::table('tbl_role')->where('name', '=', 'Employee')->first();
                        $user = User::findOrFail($user_id);
                        $user->user_type = $request->input('user_type', 'employee');
                        $user->role_id = $role->id;
                        $user->rolename = $role->name;
                        $user->fill($request->all());
                        $user->username = $request->input('email');
                        $user->status='invited';
                        $user->update();

                        $mail_obj = new MailHelper;
                        $mail_obj->userInivitationNotification($user);
                        $employee->user_id = $user->id;
                    }
                }
            }

            if (($employee->is_user == 1) && ($request->input('is_user') == 0)) {
                $user_id = $employee->user_id;
                $user_det = User::where('id', "=", $user_id)->first();
                if ($user_det != NULL) {
                    $user = User::findOrFail($user_id);
                    $user->is_active = 0;
                    $user->update();
                    $employee->user_id = 0;
                    $employee->update();
                }
            }

            if (($employee->is_user == 0) && ($request->input('is_user') == 1)) {
                $email = DB::table('tbl_user')->where('email', '=', $request->input('email'))->where('is_active', '=', 1)->count();
                if ($email > 0) {

                    $resVal['success'] = FALSE;
                    $resVal['message'] = 'Email is already exist';
                } else {
                    $role = DB::table('tbl_role')->where('name', '=', 'Employee')->first();
                    $user = new User;
                    $user->updated_by = $currentuser->id;
                    $user->created_by = $currentuser->id;
                    $user->fill($request->all());
                    $user->user_type = $request->input('user_type', 'employee');
                    $user->role_id = $role->id;
                    $user->rolename = $role->name;
                    $user->status='invited';
                    $user->username = $request->input('email');
                    $user->save();

                    $mail_obj = new MailHelper;
                    $mail_obj->userInivitationNotification($user);
                    $employee->user_id = $user->id;
                }
            }
            $previous_team_id=$employee->team_id;
            $team_id = $request->input('team_id');
            $employee->updated_by = $currentuser->id;
            $employee->fill($request->all());
            
            if($previous_team_id != $team_id){
            GeneralHelper::decrementEmployee($previous_team_id);
            GeneralHelper::incrementEmployee($team_id);
            }

            $employee->save();
        }
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('fname', '');
        $team_name = $request->input('team_name', '');
        $type_of_emp = $request->input('type_of_emp', '');
        $team_id = $request->input('team_id', '');
        $emp_code = $request->input('emp_code');
        $salary_type = $request->input('salary_type');
        $is_user = $request->input('is_user');
        $is_active = $request->input('is_active');
        $payroll_type = $request->input('payroll_type');



        $builder = DB::table('tbl_employee as e')
                ->leftJoin('tbl_emp_team as t', 'e.team_id', '=', 't.id')
                ->select('e.*', 't.name as team_name');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('e.id', '=', $id);
        }

        if (!empty($name)) {
            $builder->where('e.f_name', 'like', '%' . $name . '%');
        }
        if (!empty($team_id)) {
            $builder->where('e.team_id', '=', $team_id);
        }
        if ($type_of_emp == 2) {
            $builder->where('e.type_of_emp', '=', 2);
        }
        if ($type_of_emp == 1) {
            $builder->where('e.type_of_emp', '=', 1);
        }
        $builder->orderBy('e.id', 'desc');


        if (!empty($emp_code)) {
            $builder->where('e.emp_code', 'like', '%' . $emp_code . '%');
        }

        if (!empty($salary_type)) {
            $builder->where('e.salary_type', '=', $salary_type);
        }
        if (!empty($payroll_type)) {
            $builder->where('e.payroll_type', '=', $payroll_type);
        }

        if (!empty($is_user)) {
            $builder->where('e.is_user', '=', $is_user);
        }

        if (!empty($is_active)) {
            $builder->where('e.is_active', '=', $is_active);
        }

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $employeeCollection = $builder->get();
        } else {

            $employeeCollection = $builder->skip($start)->take($limit)->get();
        }

        $resVal['list'] = $employeeCollection;
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $is_active = 0;

        $currentuser = Auth::user();
        try {
            $employee = Employee::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Not found';
            return $resVal;
        }

        $employee->is_active = 0;
        $employee->updated_by = $currentuser->id;
        $team_id = $employee->team_id;
        $employee->save();

        if ($employee->is_user == 1) {
            $user_id = $employee->user_id;
            $user = User::findOrFail($user_id);
            $user->updated_by = $currentuser->id;
            $user->is_active = 0;
            $user->update();
        }

        GeneralHelper::decrementEmployee($team_id);

        return $resVal;
    }

    public function workingDayCount(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $team_id = $request->input('team_id', '');
        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');

        $builder = DB::table('tbl_employee as e')
                ->leftJoin('tbl_emp_team as t', 'e.team_id', '=', 't.id')
                ->leftJoin('tbl_emp_production as ep', 'ep.emp_id', '=', 'e.id')
                ->leftJoin(DB::raw('(select emp_id,count(id) as presentCount from  tbl_emp_production '
                                . ' where  is_present=1 AND date >= ' . "'$from_date'" . ' AND date<= ' . "'$to_date'" . '  group by emp_id) as pres on pres.emp_id=e.id')
                        , function($join1) {
                    
                })
                ->leftJoin(DB::raw('(select emp_id,count(id) as absentCount from  tbl_emp_production'
                                . '  where  is_present=0 AND date >= ' . "'$from_date'" . '  AND date<= ' . "'$to_date'" . ' group by emp_id) as abs on abs.emp_id=e.id')
                        , function($join2) {
                    
                })
                ->select('e.*', 't.name as team_name', DB::raw('coalesce(abs.absentCount,0) as absentCount'), DB::raw('coalesce(pres.presentCount,0) as presentCount'))
                ->distinct('e.id')
                ->where('e.is_active', '=', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('e.id', '=', $id);
        }

        if (!empty($team_id)) {
            $builder->where('e.team_id', '=', $team_id);
        }


        $builder->orderBy('e.f_name', 'asc');

        if ($start == 0 && $limit == 0) {
            $employeeCollection = $builder->get();
        } else {

            $employeeCollection = $builder->skip($start)->take($limit)->get();
        }
        //$resVal['total'] = $builder->count();
        $resVal['list'] = $employeeCollection;
        return ($resVal);
    }

    public function empProductionInfo(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $team_id = $request->input('team_id', '');
        $from_date = $request->input('from_date', '');
        $to_date = $request->input('to_date', '');

        $builder = DB::table('tbl_employee as e')
                ->leftJoin('tbl_emp_team as t', 'e.team_id', '=', 't.id')
                ->leftJoin('tbl_emp_production as ep', 'ep.emp_id', '=', 'e.id')
                ->leftJoin(DB::raw('(select emp_id,sum(epa.production_qty) as epa_qty '
                                . ' from tbl_emp_production_attendance epa left join tbl_emp_production ep1 on epa.emp_production_id = ep1.id group by emp_id) '
                                . ' as epa1 on epa1.emp_id=e.id')
                        , function($join1) {
                    
                })
                ->leftJoin(DB::raw('(select emp_id,sum(edp.production_qty) as epd_qty '
                                . ' from tbl_emp_production_detail edp left join tbl_emp_production ep0 on edp.emp_production_id = ep0.id group by emp_id) '
                                . ' as epd1 on epd1.emp_id=e.id')
                        , function($join2) {
                    
                })
                ->leftJoin(DB::raw('(select emp_id,sum(input_qty) as input_qty '
                                . ' from tbl_emp_production_input epi  left join tbl_emp_production ep2 on epi.emp_production_id = ep2.id group by emp_id) '
                                . ' as epi1 on epi1.emp_id=e.id')
                        , function($join3) {
                    
                })
                ->select('e.id', DB::raw('concat(e.f_name," ",e.l_name) as employeeName'), 't.name as teamName', DB::raw('epd1.epd_qty+epa1.epa_qty as output_qty'), 'epi1.input_qty', 'ep.date')
                ->distinct('e.id')
                ->where('e.is_active', '=', 1);
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('e.id', '=', $id);
        }

        if (!empty($team_id)) {
            $builder->where('e.team_id', '=', $team_id);
        }

        if (!empty($from_date)) {

            $builder->whereDate('ep.date', '>=', $from_date);
        }
        if (!empty($to_date)) {

            $builder->whereDate('ep.date', '<=', $to_date);
        }

        // print_r($builder->toSql());
        $builder->orderBy('e.f_name', 'asc');

        if ($start == 0 && $limit == 0) {
            $employeeCollection = $builder->get();
        } else {

            $employeeCollection = $builder->skip($start)->take($limit)->get();
        }
        //$resVal['total'] = $builder->count();
        $resVal['list'] = $employeeCollection;
        return ($resVal);
    }

}
