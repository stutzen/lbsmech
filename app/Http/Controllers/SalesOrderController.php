<?php

namespace App\Http\Controllers;

use DB;
use App\SalesOrder;
use App\SalesOrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Transaction;
use App\Helper\AccountHelper;
use App\Payment;
use App\DealActivity;
use App\Helper\ActivityLogHelper;
use Carbon\Carbon;
use App\Helper\GeneralHelper;
use App\Helper\SalesInvoiceHelper;
use App\Helper\SmsHelper;
use App\Attributevalue;
use App\Attribute;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuotesController
 *
 * @author Deepa
 */
class SalesOrderController extends Controller {

    //put your code here
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Sales Order Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $sales_order_no = $request->input('sales_order_no', '');
        $prefix = $request->input('prefix', '');

        $sales_order_code = '';
        if (!empty($sales_order_no)) {
            $get_inv_no = DB::table('tbl_sales_order')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('sales_order_no', '=', $sales_order_no)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Sales Order no already exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $sales_order_no = $sales_order_no;
                $sales_order_code = $prefix . $sales_order_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_sales_order')
                    ->select('sales_order_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('sales_order_no', 'desc')
                    ->first();
            if (isset($get_inv_no)) {
                $sales_no = $get_inv_no->sales_order_no;
                $sales_no = $sales_no + 1;
            } else {
                $sales_no = 1;
            }
            $sales_order_no = $sales_no;
            $sales_order_code = $prefix . $sales_no;
        }

        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item');
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax(0, $taxId, $taxable_amount, 'sales_order');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);
        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        $total = floatval(number_format($total, 2, '.', ''));

        $sales = new SalesOrder;
        $sales->created_by = $currentuser->id;
        $sales->updated_by = $currentuser->id;
        $sales->is_active = $request->input('is_active', 1);
        $sales->fill($request->all());
        $sales->subtotal = $subtotal;
        $sales->tax_amount = $tax_amount;
        $sales->discount_amount = $discount_amount;
        $sales->sales_order_no = $sales_order_no;
        $sales->sales_order_code = $sales_order_code;
        $sales->round_off = $roundoff;
        $sales->total_amount = $total;


        $sales->status = $request->input('status', 'new');


        $customerItemCol = ($request->input('customattribute'));
        $attribute = array();
        foreach ($customerItemCol as $item) {
            $attribute[$item['attribute_code']] = $item['value'];
        }
        if ($request->input('customattribute') != null) {
            $json_att = json_encode($attribute);
            $sales->custom_attribute_json = $json_att;
        }
        $sales->save();
        $quoteItemCol = ($request->input('item'));

        foreach ($quoteItemCol as $item) {

            $salesitems = new SalesOrderItem;
            $salesitems->fill($item);
            $salesitems->sales_order_id = $sales->id;
            $salesitems->is_active = $request->input('is_active', 1);
            $salesitems->customer_id = $request->input('customer_id');
            $salesitems->duedate = $sales->duedate;
            $salesitems->paymentmethod = $sales->paymentmethod;
            $salesitems->tax_amount = $sales->tax_amount;
            $salesitems->created_by = $currentuser->id;
            $salesitems->updated_by = $currentuser->id;
            $salesitems->save();
        }

        if ($request->input('customattribute') != null) {
            foreach ($customerItemCol as $attribute) {
//print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $sales->id;
                $customerattribute->value = $attribute['value'];
                if (isset($attribute['sno'])) {
                    $customerattribute->sno = $attribute['sno'];
                }
                $customerattribute->attribute_code = $attribute['attribute_code'];

                $customerattribute->created_by = $currentuser->id;
                $customerattribute->updated_by = $currentuser->id;
                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();
                    if (count($attributeCollection) > 0) {
                        $attribute = $attributeCollection->first();
//print_r($attribute->id);
                        $customerattribute->attributes_id = $attribute->id;
                        $customerattribute->attributetype_id = $attribute->attributetype_id;
                    }
                }

//$customerattribute->fill($attribute);       

                $customerattribute->save();
            }
        }


        $resVal['id'] = $sales->id;
        //Update SalesOrder Status
        SalesInvoiceHelper::updateSalesOrderStatus($sales->id);
        $resVal['id'] = $sales->id;
        // SmsHelper::paymentDetails($sales, "sales");

        GeneralHelper::InvoiceTaxSave($sales->id, $request->input('tax_id'), $taxable_amount, $subtotal, 'sales_order');

        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'salesorder';
            $deal->description = "created an Sales Order";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->deal_id = $deal_id;
            $deal->ref_id = $sales->id;
            $deal->comments = "created an Sales Order";

            $deal->save();
        }
        ActivityLogHelper::salesOrderSave($sales);

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $curr_id = $currentuser->id;
        $id = $request->input('id');
        $customerid = $request->input('customerid');
        $mobile = $request->input('mobile', '');
        $sales_order_code = $request->input('sales_order_code', '');
        $fromValidDate = $request->input('from_validdate', '');
        $toValidDate = $request->input('to_validdate', '');
        $fromDate = $request->input('from_date', '');
        $toDate = $request->input('to_date', '');
        $orderDate=$request->input('order_date','');
        $vaildDate=$request->input('valid_date','');
        $deal_id = $request->input('deal_id');
        $status = $request->input('status');
        $type=$request->input('type');
        $service_request_id=$request->input('service_request_id');
        $company_id=$request->input('company_id');

        $city=$request->input('city','');
 
        $isactive = $request->input('is_active', '');
        //$builder = DB::table('tbl_quote')->select('*');

        $builder = DB::table('tbl_sales_order as q')
                ->leftJoin('tbl_customer as c', 'q.customer_id', '=', 'c.id')
                ->leftjoin('tbl_company as com','com.id','=','q.company_id')
                ->leftJoin('tbl_customer as d', 'q.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'q.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
                ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email','c.city', 'd.fname as payee_name', 'e.fname as consignee_name','com.name as company_name','c.billing_city','c.shopping_city');

        /* $builder = DB::table('tbl_sales_order as q')
          ->leftJoin('tbl_customer as c', 'q.customer_id', '=', 'c.id')
          ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
          ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname'); */

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        
         $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;

//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0) {  
//        }else{
//             $builder->where('q.created_by', '=',$curr_id);
//        }

        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }
         if (!empty($company_id)) {
            $builder->where('com.id', '=', $company_id);
        }
        if (!empty($mobile)) {
            $builder->where('c.phone', '=', $mobile);
        }
        
        if (!empty($city)) {
            $builder->whereRaw(" (c.billing_city like '%" . $city . "%' or c.shopping_city like '%" . $city . "%' )  ");
        }

        if ($isactive != '') {
            $builder->where('q.is_active', '=', $isactive);
        }
        if (!empty($sales_order_code)) {
            $builder->where('q.sales_order_code', 'like', '%' . $sales_order_code . '%');
        }
        if (!empty($fromValidDate)) {

            $builder->whereDate('q.validuntil', '>=', $fromValidDate);
        }
        if (!empty($toValidDate)) {

            $builder->whereDate('q.validuntil', '<=', $toValidDate);
        }
        if (!empty($fromDate)) {

            $builder->whereDate('q.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {

            $builder->whereDate('q.date', '<=', $toDate);
        }
		 if(!empty($orderDate)){
            $builder->whereDate('q.date', '=', $orderDate);
        }
        if(!empty($vaildDate)){
            $builder->whereDate('q.validuntil','=',$vaildDate);
        }
        if (!empty($type)) {

            $builder->where('q.type', '=', $type);
        }
        if (!empty($status)) {
            $builder->where('q.status', '=', $status);
        }
        if (!empty($customerid)) {
            $builder->where('q.customer_id', '=', $customerid);
        }
        if (!empty($deal_id)) {
            $builder->where('q.deal_id', '=', $deal_id);
        }
        if (!empty($service_request_id)) {
            $builder->where('q.service_request_id', '=', $service_request_id);
        }

         $resVal['total'] = $builder->count();
        $builder->orderBy('q.id', 'desc');
        if ($start == 0 && $limit == 0) {
            $salesCollection = $builder->get();
        } else {

            $salesCollection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($salesCollection as $sales) {

            $json_att = $sales->custom_attribute_json;
            if (empty($json_att)) {
                $json_att = "{}";
            }
            $json_att = json_decode($json_att);
            $sales->custom_attribute_json = $json_att;
        }

       
        $resVal['list'] = $salesCollection;
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Sales Order Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $sales = SalesOrder::findOrFail($id);

            if ($sales->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Sales Order Not found';
            return $resVal;
        }
        $sales->created_by = $currentuser->id;
        $sales->updated_by = $currentuser->id;
        $sales->is_active = 0;
        //$sales->fill($request->all());
        $sales->update();


        /*
         * Code to adjust bank money and transaction table entry 
         */
        $paymentCollection = Payment::where('invoice_id', '=', 0)
                        ->where('quote_id', '=', 0)
                        ->where('sales_order_id', '=', $id)
                        ->where('is_active', '=', 1)->get();
        foreach ($paymentCollection as $payment) {

            AccountHelper::withDraw($payment->account_id, $payment->amount);

            //deavtivate the transaction
            DB::table('tbl_transaction')
                    ->where('voucher_type', '=', Transaction::$SALES_PAYMENT)
                    ->where('voucher_number', '=', $payment->id)->update(['is_active' => 0]);
        }

        $resVal['id'] = $sales->id;
        $deal_id = $sales->deal_id;
        if ($deal_id != 0) {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'salesorder';
            $deal->description = "delete an sales order";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->deal_id = $deal_id;
            $deal->ref_id = $sales->id;
            $deal->comments = "delete an sales order";

            $deal->save();
        }
        GeneralHelper::InvoiceTaxDelete($id, 'sales_order');

        ActivityLogHelper::salesOrderDelete($sales);
        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Sales Order Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $sales = SalesOrder::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Sales Order Not found';
            return $resVal;
        }

        $sales_order_no = $request->input('sales_order_no', '');
        $prefix = $request->input('prefix', '');
        $invoice_code = '';
        if (!empty($sales_order_no)) {

            $get_inv_no = DB::table('tbl_sales_order')
                    ->select('is_active')
                    ->where('prefix', '=', $prefix)
                    ->where('is_active', '=', 1)
                    ->where('sales_order_no', '=', $sales_order_no)
                    ->where('id', '!=', $id)
                    ->get();
            if (count($get_inv_no) > 0) {
                $resVal['message'] = 'Sales Order no already exists';
                $resVal['success'] = FALSE;
                return $resVal;
            } else {
                $sales_order_no = $sales_order_no;
                $sales_order_code = $prefix . $sales_order_no;
            }
        } else {
            $get_inv_no = DB::table('tbl_sales_order')
                    ->select('sales_order_no')
                    ->where('is_active', '=', 1)
                    ->where('prefix', '=', $prefix)
                    ->orderBy('sales_order_no', 'desc')
                    ->where('id', '!=', $id)
                    ->first();
            if (isset($get_inv_no)) {
                $sales_no = $get_inv_no->sales_order_no;
                $sales_no = $sales_no + 1;
            } else {
                $sales_no = 1;
            }
            $sales_order_no = $sales_no;
            $sales_order_code = $prefix . $sales_no;
        }

        $subtotal = 0;
        $discount_amount = 0;
        $roundoff = 0;
        $tax_id = 0;
        $tax_amount = 0;
        $taxable_amount = 0;
        $total_before_roundoff = 0;
        $total = 0;

        $invoiceItemCol = $request->input('item'); 
        foreach ($invoiceItemCol as $item) {
            $item['total_price'] = floatval($item['unit_price']) * floatval($item['qty']);

            $item['total_price'] = floatval(number_format($item['total_price'], 2, '.', ''));
            $subtotal = $subtotal + $item['total_price'];
        }
        $discountMode = $request->input('discount_mode', '');
        $discountValue = $request->input('discount_percentage', '');
        if (empty($discountMode) || empty($discountValue)) {
            $discount_amount = 0;
            $taxable_amount = $subtotal;
        } else {

            if ($discountMode == '%') {
                $discount_amount = $subtotal * $discountValue / 100;
                $discount_amount = floatval(number_format($discount_amount, 2, '.', ''));
            } else {
                $discount_amount = $discountValue;
            }

            $taxable_amount = $subtotal - $discount_amount;
        }

        $taxId = $request->input('tax_id', '');
        if (!empty($taxId)) {
            $tax_list = GeneralHelper::calculatedInvoiceTax($id, $taxId, $taxable_amount, 'sales_order');

            foreach ($tax_list as $tax) {

                $tax_amount += $tax->tax_amount;
            }
        }
        $packing_charge = $request->input('packing_charge', 0.0);
        $insurance_charge = $request->input('insurance_charge', 0.0);
        //finding roundoff value 
        $total_before_roundoff = $subtotal - $discount_amount + $tax_amount + $packing_charge + $insurance_charge;
        $total_after_roundoff = round($total_before_roundoff);
        $roundoff = $total_after_roundoff - $total_before_roundoff;
        $roundoff = floatval(number_format($roundoff, 2, '.', ''));

        //finding grant total value 
        $total = $subtotal - $discount_amount + $tax_amount + $roundoff + $packing_charge + $insurance_charge;
        $total = floatval(number_format($total, 2, '.', ''));

        $builder = DB::table('tbl_sales_order_item')->where('sales_order_id', $id)->delete();
        $quoteItemCol = ($request->input('item'));
        foreach ($quoteItemCol as $item) {

            $salesitems = new SalesOrderItem;
            $salesitems->fill($item);
            $salesitems->sales_order_id = $sales->id;
            $salesitems->is_active = $request->input('is_active', 1);
            $salesitems->customer_id = $request->input('customer_id');
            $salesitems->duedate = $sales->duedate;
            $salesitems->paymentmethod = $sales->paymentmethod;
            $salesitems->tax_amount = $sales->tax_amount;
            $salesitems->created_by = $currentuser->id;
            $salesitems->updated_by = $currentuser->id;
            $salesitems->save();
        }
        $sales->fill($request->all());
        $sales->subtotal = $subtotal;
        $sales->tax_amount = $tax_amount;
        $sales->discount_amount = $discount_amount;
        $sales->round_off = $roundoff;
        $sales->total_amount = $total;
        $sales->sales_order_no = $sales_order_no;
        $sales->sales_order_code = $sales_order_code;
        $sales->created_by = $currentuser->id;
        $sales->updated_by = $currentuser->id;
        $sales->save();

        //For Custom Attribute
        $attributeCollection = DB::table('tbl_attribute_types')->where('code', 'sales_order')->get();
        $attribute = $attributeCollection->first();


        if (count($attributeCollection) > 0) {


            $builder = DB::table('tbl_attribute_value')
                    ->where('attributetype_id', '=', $attribute->id)
                    ->where('refernce_id', '=', $id)
                    ->delete();
//print_r($builder);
        }
        if ($request->input('customattribute') != null) {
            $customerItemCol = ($request->input('customattribute'));

            $attribute = array();
            foreach ($customerItemCol as $item) {
                $attribute[$item['attribute_code']] = $item['value'];
            }
            $json_att = json_encode($attribute);
            $sales->custom_attribute_json = $json_att;



            foreach ($customerItemCol as $attribute) {
//print_r($attribute);

                $customerattribute = new Attributevalue;

                $customerattribute->refernce_id = $sales->id;
                $customerattribute->value = $attribute['value'];
                if (isset($attribute['sno'])) {
                    $customerattribute->sno = $attribute['sno'];
                }

                $customerattribute->attribute_code = $attribute['attribute_code'];

                if ($attribute['attribute_code'] != NULL) {


                    $attributeCollection = Attribute::where('attribute_code', "=", $attribute['attribute_code'])->get();

                    $attribute = $attributeCollection->first();
//print_r($attribute->id);
                    $customerattribute->attributes_id = $attribute->id;
                    $customerattribute->attributetype_id = $attribute->attributetype_id;
                }

                $customerattribute->updated_by = $currentuser->id;
                $customerattribute->save();
            }
        }
        $sales->save();

        if (!empty($request->status)) {
            $sales->status = $request->status;
            $sales->save();
        } else {
            //Update SalesOrder Status
            SalesInvoiceHelper::updateSalesOrderStatus($sales->id);
        }

        $resVal['id'] = $sales->id;

        $deal_id = $request->input('deal_id', '');
        if ($deal_id != '') {
            $deal = new DealActivity;
            $get_contact_id = DB::table('tbl_customer')->where('is_active', 1)
                            ->where('id', '=', $request->customer_id)->first();
            $current_date = date('Y-m-d');
            $timestamp = date('H:i:s');
            $timestamp = strtotime($timestamp);
            $time = date('h:i:s');
            $time = strtotime($time);

            $deal->created_by = $currentuser->id;
            $deal->updated_by = $currentuser->id;
            $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $deal->time = $time;
            $deal->date = $current_date;
            $deal->type = 'salesorder';
            $deal->description = "updated an Sales Order";
            $deal->contact_id = $get_contact_id->contact_id;
            $deal->user_id = $currentuser->id;
            $deal->user_name = $currentuser->f_name;
            $deal->is_active = 1;
            $deal->deal_id = $deal_id;
            $deal->ref_id = $sales->id;
            $deal->comments = "updated an Sales Order";

            $deal->save();
        }
        GeneralHelper::InvoiceTaxUpdate($id, $request->input('tax_id'), $taxable_amount, $subtotal, 'sales_order');

        ActivityLogHelper::salesOrderModify($sales);
        return $resVal;
    }

    public function detail(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');

        $is_active = $request->input('is_active', '');
        //$builder = DB::table('tbl_quotes')->select('*');
        $builder = DB::table('tbl_sales_order as q')
                ->leftJoin('tbl_customer as c', 'q.customer_id', '=', 'c.id')
                ->leftjoin('tbl_company as com','com.id','=','q.company_id')
                ->leftJoin('tbl_customer as d', 'q.payee', '=', 'd.id')
                ->leftJoin('tbl_customer as e', 'q.consignee', '=', 'e.id')
                ->leftJoin('tbl_tax as t', 'q.tax_id', '=', 't.id')
                ->leftJoin('tbl_service_request as sr', 'sr.id', '=', 'q.service_request_id')
                ->select('q.*', 'c.fname as customer_fname', 'c.lname as customer_lname', 't.tax_name as taxname', 'c.phone', 'c.email',
                        'd.fname as payee_name', 'e.fname as consignee_name','com.name as company_name','sr.invoice_item_id')
                ->where('q.id', '=', $id);

        $salestitems = DB::table('tbl_sales_order_item')->where('sales_order_id', $id)
                ->where('is_active', '=', 1);
        $invoiced_product_collection = DB::table('tbl_invoice_item as it')->where('quote_id', $id)
                ->leftJoin('tbl_invoice as i', 'i.id', '=', 'it.invoice_id')
                ->where('i.is_active', 1)
                ->where('i.quote_id', $id)
                ->select(DB::raw('sum(it.qty) as qty, it.product_id'))
                ->groupBy('product_id')
                ->get();
        $invoicedProductList = array();
        foreach ($invoiced_product_collection as $product) {

            $invoicedProductList[$product->product_id] = $product->qty;
        }

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if (!empty($id)) {
            $builder->where('q.id', '=', $id);
        }

        if ($is_active != '') {
            $builder->where('q.is_active', '=', $is_active);
        }
        $builder->orderBy('q.id', 'desc');

        $resVal['total'] = $builder->count();
        $salestCollection = $builder->skip($start)->take($limit)->get();
        if (count($salestCollection) == 1) {

            $salest = $salestCollection->first();
            $salest->item = $salestitems->get();
            foreach ($salest->item as $item) {

                if (array_key_exists($item->product_id, $invoicedProductList)) {
                    $item->invoiced_qty = $invoicedProductList[$item->product_id];
                } else {
                    $item->invoiced_qty = 0;
                }
            }
            $salest->advance_amount = 0;
            $advance = DB::table("tbl_adv_payment")
                    ->select(DB::raw("SUM(amount) as amount"))
                    ->where('voucher_type', '=', "advance_invoice")
                    ->where('voucher_no', '=', $salest->id)
                    ->where('is_active', '=', 1)
                    ->groupBy('voucher_no')
                    ->first();
            if ($advance != '') {
                $salest->advance_amount = $advance->amount;
            }


            $customAttributeValue = DB::table('tbl_attribute_value as a')
                            ->leftJoin('tbl_attributes as abs', 'a.attributes_id', '=', 'abs.id')
                            ->select('a.*', 'abs.attribute_label as attribute_label')
                            ->where('a.refernce_id', '=', $id)
                            ->where('abs.attributetype_code', '=', 'salesorder')->get();


            $customattribute = DB::table('tbl_attributes as abs')
                            ->select('abs.*')
                            ->where('abs.attributetype_code', '=', 'salesorder')->get();
            if (count($customAttributeValue) > 0) {

                foreach ($customAttributeValue as $item) {

                    foreach ($customattribute as $customattributeItem) {

                        if ($item->attribute_code == $customattributeItem->attribute_code) {

                            $customattributeItem->value = $item->value;
                        }
                    }
                }
            }

            $salest->customattribute = $customattribute;

            $resVal['data'] = $salest;
        }
        // $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

}

?>
