<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\AllowanceMaster;
use App\AllowanceMapping;
use App\DealStage;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;

class AllowanceMappingController extends Controller {

    public function update(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Allowance Mapping Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $allowancelist = $request->all();
        $mappingCollection = array();
        foreach ($allowancelist as $std) {

            if (($std['id'] != NULL) || ($std['id'] != 0)) {
                $mappingCollection = AllowanceMapping::where('id', "=", $std['id'])
                        ->get();
            }
            if (count($mappingCollection) > 0) {
                $mapping = $mappingCollection->first();
            } else {
                $mapping = new AllowanceMapping;
            }


            $mapping->fill($std);
            $mapping->created_by = $currentuser->id;
            $mapping->is_active = 1;
            $mapping->updated_by = $currentuser->id;
            $mapping->save();
        }

        return $resVal;
    }
    public function listAll1(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id','');
        $roleid = $request->input('role_id', '');
        $allowance = $request->input('allowance_id', '');
        $isactive = $request->input('is_active', '');

        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        $builder = DB::table('tbl_allowance_mapping as am')
                ->Join('tbl_allowance_master as a', 'am.allowance_id', '=', 'a.id')
                -> Join('tbl_role as r', 'am.role_id', '=', 'r.id')
                ->select('am.*', 'r.name', 'a.name');

        if (!empty($id)) {
            $builder->where('am.id', '=', $id);
        }
        if ($roleid != '') {
            $builder->where('am.role_id', '=', $roleid);
        }
        if ($allowance != '') {
            $builder->where('am.allowance_id', '=', $allowance);
        }
        if ($isactive != '') {
            $builder->where('am.is_active', '=', $isactive);
        }
 

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }
        $resVal['list'] =$collection;
        return ($resVal);
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

 
        $id = $request->input('id');
        $allowance_id = $request->input('allowance_id');
        $role_id= $request->input('role_id', '');
        $is_active = $request->input('is_active');
        
        $builder = DB::table('tbl_role as t')
                    ->leftJoin('tbl_allowance_mapping as tm', function($join) use ($allowance_id) {
                        $join->on('t.id', '=', 'tm.role_id');
                        $join->on('allowance_id', '=', DB::raw("'" . $allowance_id . "'"));
                        $join->on('tm.is_active', '=', DB::raw("'1'"));
                    })
                    ->leftJoin('tbl_allowance_master as a','a.id', '=','tm.allowance_id')
                    ->select( DB::raw('ifnull(tm.id, 0) as id'),'t.name as role_name','tm.type','tm.amount','a.name as allowance_name','t.id as role_id', DB::raw('CONCAT( "",'.$allowance_id.') AS allowance_id'))
                    ->where('t.is_active', '=', 1);
                    
//        $builder = DB::table('tbl_allowance_mapping as ub')
//                ->leftjoin('tbl_role as u','u.id','=','ub.role_id')
//                ->select('ub.role_id','u.name')
//               ->groupby('ub.role_id'); ;
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('tm.id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('tm.is_active', '=', $is_active);
        }
//        if (!empty($allowance_id)) {
//            $builder->where('tm.allowance_id', '=', $allowance_id);
//        }
//       
        
        $builder->orderBy('tm.id', 'desc');
        $resVal['total'] = $builder->count();

        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {

            $collection = $builder->skip($start)->take($limit)->get();
        }
        
//        foreach($collection as $collect){
//            $role_id=$collect->role_id;
//            $Mapping = DB::table('tbl_allowance_master as t')
//                    ->leftJoin('tbl_allowance_mapping as tm', function($join) use ($role_id) {
//                        $join->on('t.id', '=', 'tm.allowance_id');
//                        $join->on('role_id', '=', DB::raw("'" . $role_id . "'"));
//                        $join->on('tm.is_active', '=', DB::raw("'1'"));
//                    })
//                    ->select( DB::raw('ifnull(tm.allowance_id, 0) as allowance_id'),'t.name','tm.type','tm.amount')
//                    ->where('t.is_active', '=', 1);
//            $collect->allowanceMapping = $Mapping->get();
//            
//        }
        
        $resVal['list']=$collection;
        return $resVal;
    }

}
