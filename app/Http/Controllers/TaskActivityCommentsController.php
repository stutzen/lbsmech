<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\TaskActivityComments;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\LogHelper;

class TaskActivityCommentsController extends Controller {
    
    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Task Activity Comments Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
         $currentuser = Auth::user();
           
        $taskcomments=$request->all();
     
        foreach($taskcomments as $comment)
        {
       
        $comments=new TaskActivityComments;
        $comments->fill($comment);
        $comments->created_by=$currentuser->id;
        $comments->updated_by=$currentuser->id;
        $comments->is_active=$request->input('is_active',1);
        
          $comments->save();
        }
      
//         LogHelper::info('Task Activity Comments Save'.$request->fullurl());
       return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $task_activity_id=$request->input('task_activity_id');
        $is_active=$request->input('is_active');
       
        $builder = DB::table('tbl_task_activity_comments as tc')
                ->leftjoin('tbl_task_activity as ta','tc.task_activity_id','=','ta.id')
                ->select('tc.*','ta.emp_name as created_name');
        $resVal['success'] = TRUE;
       
        
        if (!empty($id)) {
            $builder->where('tc.id', '=', $id);
        }
        if (!empty($task_activity_id)) {
            $builder->where('tc.task_activity_id', '=', $task_activity_id);
        }
           
       if ($is_active != '') {
            $builder->where('tc.is_active', '=', $is_active);
        }
       
       
        $builder->orderBy('tc.id', 'desc');
        $resVal['total'] = $builder->count();
        
        $resVal['list'] = $builder->get();
      
//         LogHelper::info('Task Activity Comments Filters'.$request->fullurl());
  //        LogHelper::info('Task Activity Comments List '.$resVal['list']);
        return ($resVal);
    }
    
    
     public function update(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Task Activity Comments Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
            
         $taskcomments=$request->all();
     
        foreach($taskcomments as $comment)
        {
       $cid=$comment['id'];
    
        try {
            $comments = TaskActivityComments::findOrFail($cid);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Activity Comments Not found';
            return $resVal;
        }
        $comments->fill($comment);
        $comments->created_by=$currentuser->id;
        $comments->updated_by=$currentuser->id;
        $comments->is_active=$request->input('is_active',1);
        
          $comments->save();
        }

        // LogHelper::info('Task Activity Comments Update'.$request->fullurl());
        return $resVal;
    }
    
    public function delete(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Task Activity Comments Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
       $taskcomments=$request->all();
     
        foreach($taskcomments as $comment)
        {
       $cid=$comment['id'];
    
        try {
            $comments = TaskActivityComments::findOrFail($cid);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Activity Comments Not found';
            return $resVal;
        }
        $comments->is_active = 0;
        $comments->updated_by = $currentuser->id;
        $comments->save();
        }
       //  LogHelper::info('Task Activity Comments Delete'.$request->fullurl());
        return $resVal;
    }
}
