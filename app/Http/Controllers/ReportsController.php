<?php

//

namespace App\Http\Controllers;

use DateTime;
use DateInterval;
use DatePeriod;
use DB;
use App\EmployeeProductionAttendance;
use App\EmployeeProductionDetail;
use App\ActivityLog;
use App\PurchasePayment;
use Carbon\Carbon;
use App\Attributevalue;
use App\Attribute;
use app\Customer;
use Illuminate\Http\Request;
use App\Transformer\InvoiceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoicesController
 *
 * @author Deepa
 */
class ReportsController {

    //put your code here
    public function ItemWiseReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_invoice_item as a')
                ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
                ->select(DB::raw("a.product_name, a.product_id,SUM(a.qty) as qty, SUM(a.total_price) as total_price, a.uom_name"))
                ->whereDate('at.date', '>=', $from_date)
                ->whereDate('at.date', '<=', $to_date)
                ->where('a.is_active', '=', 1)
                ->groupBy(DB::raw("a.product_name"))
                ->groupBy(DB::raw("a.uom_name"))
                ->get();

        $resVal['success'] = TRUE;

        foreach ($builder as $tax) {
            $id = $tax->product_name;
            $builder_detail = DB::table('tbl_invoice_item as a')
                    ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
                    ->leftJoin('tbl_customer as c', 'at.customer_id', '=', 'c.id')
                    ->select(DB::raw("at.invoice_code,at.date,c.fname, a.qty ,  a.total_price "))
                    ->whereDate('at.date', '>=', $from_date)
                    ->whereDate('at.date', '<=', $to_date)
                    ->where('a.is_active', '=', 1)
                    ->where('a.product_name', '=', $id);

            $tax->data = $builder_detail->get();
        }

        $resVal['list'] = $builder;
        return ($resVal);
    }

    public function ItemReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();
        $resVal['success'] = TRUE;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $product_id = $request->input('product_id');
        $builder = DB::table('tbl_invoice_item as a')
                ->leftJoin('tbl_invoice as at', 'a.invoice_id', '=', 'at.id')
                ->leftJoin('tbl_customer as c', 'at.customer_id', '=', 'c.id')
                ->select(DB::raw("at.invoice_code,at.date,c.fname, a.qty ,  a.total_price "))
                ->whereDate('at.date', '>=', $from_date)
                ->whereDate('at.date', '<=', $to_date)
                ->where('a.is_active', '=', 1)
                ->where('a.product_id', '=', $product_id)
                ->get();
        $resVal['list'] = $builder;
        return ($resVal);
    }

    public function itemizedreport_csvexport(Request $request) {
        $itemizedLists = $this->ItemWiseReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "itemizedreport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Product Name', 'UOM', 'Quantity', 'Amount'));
        foreach ($itemizedLists['list'] as $values) {
            fputcsv($output, array($values->product_name, $values->uom_name, $values->qty, $values->total_price));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        return response()->download($filePath . "/" . $filename, 'itemizedreport.csv');
    }

    public function PartyWiseSalesReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');



        $amount = DB::table('tbl_invoice')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('total_amount');


        $paid_amount = DB::table('tbl_payment')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
                ->sum('amount');


        $bal_amount = $amount - $paid_amount;

        $invoice_list = array();
        $invoice_list['date'] = date('d-m-Y', strtotime($from_date . '-1 day'));
        $invoice_list['description  '] = 'Opening Balance';
        $invoice_list['debit'] = $amount;
        $invoice_list['credit'] = $paid_amount;
        $resVal['openBalance'] = $invoice_list;

        $builder1 = DB::table('tbl_invoice')
                ->select(DB::raw('id'), DB::raw('"Invoice" as type'), DB::raw('date'), DB::raw('CONCAT( "Invoice #",id) AS description'), DB::raw('total_amount as credit'), DB::raw('total_amount*0 as debit'))
                ->where('customer_id', '=', $id)
                ->where('date', '>=', $from_date)
                ->where('date', '<=', $to_date)
                ->where('is_active', '=', 1);


        $builder = DB::table('tbl_payment')
                ->select(DB::raw('id'), DB::raw('"Payment" as type'), DB::raw('date'), DB::raw('CONCAT("Invoice Payment #", " ", invoice_id) AS description'), DB::raw('amount*0 as credit'), DB::raw('amount as debit'))
                ->where('customer_id', '=', $id)
                ->where('date', '>=', $from_date)
                ->where('date', '<=', $to_date)
                ->where('is_active', '=', 1)
                ->union($builder1)
                ->orderBy('date', 'asc')
                ->get();
        $resVal['list'] = $builder;

        return ($resVal);
    }

    public function PartyWiseSalesReport_csvexport(Request $request) {
        $PartyWiseLists = $this->PartyWiseSalesReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "PartyWiseSalesReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $bal = 0;
        $openblnc = $PartyWiseLists['openBalance'];
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Transaction_date', 'Narration', 'Source_Type', 'Depit', 'Credit', 'Balance'));
        $bal = $openblnc['credit'] - $openblnc['debit'];
        fputcsv($output, array('', $openblnc['description  '], '', $openblnc['debit'], $openblnc['credit'], $bal));
        foreach ($PartyWiseLists['list'] as $values) {
            $bal = $bal + $values->credit;
            $bal = $bal - $values->debit;
            fputcsv($output, array($values->date, $values->description, $values->type, $values->debit, $values->credit, $bal));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        return response()->download($filePath . "/" . $filename, 'PartyWiseSalesReport.csv');
    }

    public function PartyWisePurchaseReport_csvexport(Request $request) {
        $PartyWisePurchaseLists = $this->PartyWisePurchaseReport($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = "PartyWisePurchaseReport" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $bal = 0;
        $openblnc = $PartyWisePurchaseLists['openBalance'];
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('Transaction_date', 'ID', 'Narration', 'Source_Type', 'Depit', 'Credit', 'Balance'));
        $bal = $openblnc['credit'] - $openblnc['debit'];
        fputcsv($output, array('', $openblnc['description  '], '', $openblnc['debit'], $openblnc['credit'], $bal));
        foreach ($PartyWisePurchaseLists['list'] as $values) {
            $bal = $bal + $values->credit;
            $bal = $bal - $values->debit;
            fputcsv($output, array($values->date, $values->id, $values->description, $values->type, $values->debit, $values->credit, $bal));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
        }
        return response()->download($filePath . "/" . $filename, 'PartyWisePurchaseReport.csv');
    }

    public function PartyWisePurchaseReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $id = $request->input('id');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');


        $amount = DB::table('tbl_purchase_invoice')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
//                ->where('status','!=','paid')
                ->sum('total_amount');


        $paid_amount = DB::table('tbl_purchase_payment')
                ->where('date', "<", $from_date)
                ->where('customer_id', '=', $id)
                ->where('is_active', '=', 1)
//                ->where('status','!=','paid')
                ->sum('amount');


        $bal_amount = $amount - $paid_amount;

        $invoice_list = array();
        $invoice_list['date'] = date('d-m-Y', strtotime($from_date . '-1 day'));
        $invoice_list['description  '] = 'Opening Balance';
        $invoice_list['debit'] = $amount;
        $invoice_list['credit'] = $paid_amount;
        $resVal['openBalance'] = $invoice_list;

        $builder1 = DB::table('tbl_purchase_invoice')
                ->select(DB::raw('id'), DB::raw('"Invoice" as type'), DB::raw('date'), DB::raw('CONCAT( "Invoice #",id) AS description'), DB::raw('total_amount*0 as credit'), DB::raw('total_amount as debit'))
                ->where('customer_id', '=', $id)
                ->where('date', '>=', $from_date)
                ->where('date', '<=', $to_date)
                ->where('status', '!=', 'paid')
                ->where('is_active', '=', 1);


        $builder = DB::table('tbl_purchase_payment')
                ->select(DB::raw('id'), DB::raw('"Payment" as type'), DB::raw('date'), DB::raw('CONCAT("Invoice Payment #", " ", purchase_invoice_id) AS description'), DB::raw('amount as credit'), DB::raw('amount*0 as debit'))
                ->where('customer_id', '=', $id)
                ->where('date', '>=', $from_date)
                ->where('date', '<=', $to_date)
                ->where('is_active', '=', 1)
                ->where('status', '!=', 'paid')
                ->union($builder1)
                ->orderBy('date', 'asc')
                ->get();
        $resVal['list'] = $builder;

        return ($resVal);
    }

    public function ItemWisePurchaseReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_purchase_invoice_item as a')
                ->leftJoin('tbl_purchase_invoice as at', 'a.purchase_invoice_id', '=', 'at.id')
                ->select(DB::raw("a.product_name, a.product_id,SUM(a.qty) as qty, SUM(a.total_price) as total_price, a.uom_name"))
                ->whereDate('at.date', '>=', $from_date)
                ->whereDate('at.date', '<=', $to_date)
                ->where('a.is_active', '=', 1)
                ->groupBy(DB::raw("a.product_name"))
                ->groupBy(DB::raw("a.uom_name"))
                ->get();

        $resVal['success'] = TRUE;

        foreach ($builder as $tax) {
            $id = $tax->product_name;
            $builder_detail = DB::table('tbl_purchase_invoice_item as a')
                    ->leftJoin('tbl_purchase_invoice as at', 'a.purchase_invoice_id', '=', 'at.id')
                    ->leftJoin('tbl_customer as c', 'at.customer_id', '=', 'c.id')
                    ->select(DB::raw("at.purchase_code,at.date,c.fname, a.qty ,  a.total_price "))
                    ->whereDate('at.date', '>=', $from_date)
                    ->whereDate('at.date', '<=', $to_date)
                    ->where('a.is_active', '=', 1)
                    ->where('a.product_name', '=', $id);

            $tax->data = $builder_detail->get();
        }

        $resVal['list'] = $builder;
        return ($resVal);
    }

    public function ProductionReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $showFlag = $request->input('showFlag');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $bulider = DB::table('tbl_product as ti')
                ->leftjoin(DB::raw("(select output_pdt_id,sum(epa.production_qty) as epa_qty from tbl_emp_production_attendance epa left join tbl_product t1 on t1.id = output_pdt_id  left join tbl_emp_production ep on ep.id = epa.emp_production_id  where ep.date >= '$from_date' and  ep.date <= '$to_date' and  ep.is_present = '1' group by t1.id) as epa1 on epa1.output_pdt_id=ti.id")
                        , function($join2) {
                    
                })
                ->leftjoin(DB::raw("(select output_pdt_id,sum(edp.production_qty) as epd_qty from tbl_emp_production_detail edp left join tbl_product t0 on t0.id = output_pdt_id left join tbl_emp_production ep on ep.id = edp.emp_production_id  where ep.date >= '$from_date' and  ep.date <= '$to_date' and  ep.is_present = '1'  group by t0.id)as epd1 on epd1.output_pdt_id=ti.id")
                        , function($join2) {
                    
                })
                ->select('ti.id', 'ti.name as productName', 'ti.uom', 'ti.uom_id', DB::raw('coalesce(epd1.epd_qty,0)+coalesce(epa1.epa_qty,0) as outputQty'))
                ->distinct('ti.id');
        $collection = $bulider->get();

        if ($showFlag == 1) {


            foreach ($collection as $value) {

                $id = $value->id;
                $list = DB::table('tbl_emp_production_attendance as epa')
                        ->leftJoin('tbl_emp_production as ep', 'epa.emp_production_id', '=', 'ep.id')
                        ->select('ep.emp_id', 'ep.emp_name', 'ep.emp_code', 'ep.date', DB::raw('sum(epa.production_qty) as qty'))
                        ->where('epa.output_pdt_id', '=', $id)
                        ->where('ep.date', '>=', $from_date)
                        ->where('ep.date', '<=', $to_date)
                        ->where('ep.is_present', '=', '1')
                        ->where('epa.production_qty', '!=', 0.00)
                        ->groupBy('emp_id');


                $listAns = DB::table('tbl_emp_production_detail as epd')
                        ->leftJoin('tbl_emp_production as ep1', 'epd.emp_production_id', '=', 'ep1.id')
                        ->select('ep1.emp_id', 'ep1.emp_name', 'ep1.emp_code', 'ep1.date', DB::raw('sum(epd.production_qty) as qty'))
                        ->where('epd.output_pdt_id', '=', $id)
                        ->where('ep1.is_present', '=', '1')
                        ->where('ep1.date', '>=', $from_date)
                        ->where('ep1.date', '<=', $to_date)
                        ->where('epd.production_qty', '!=', 0.00)
                        ->groupBy('ep1.emp_id')
//                     ->distinct()
                        ->unionall($list);



                $value->details = $listAns->get();
            }
            $resVal['list'] = $collection;
        } else {

            $resVal['list'] = $collection;
        }


        return $resVal;
    }

    public function assignDealBasedReport(Request $request) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_deal as d')
                ->select(DB::raw("Concat(e.f_name,' ',e.l_name ) as employee_name"), 'e.id as emp_id', DB::raw("count(d.id) as deal_count"), DB::raw("sum(d.amount) as overallamount"))
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'd.emp_id')
                //  ->leftjoin('tbl_deal_followers as df','df.deal_id','=','d.id')
                ->where('e.is_active', '=', 1)
                ->where('d.is_active', '=', 1)
                ->groupby('d.emp_id');

        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = "";
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();
        if (!empty($empid)) {
            $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
            if (!$isVisible) {
                $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid) {
                    $join->on('d.id', '=', 'df.deal_id');
                    $join->where('df.emp_id', '=', DB::raw("'" . $empid->id . "'"));
                });
                $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
            }
        }
        if (!empty($from_date)) {
            $builder->whereDate('d.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('d.created_at', '<=', $to_date);
        }
        $builder->orderBy('deal_count', 'desc');


        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if ($start == 0 && $limit == 0) {
            $response = $builder->get();
        } else {
            $response = $builder->skip($start)->take($limit)->get();
        }

        foreach ($response as $res) {
            $value = $builder = DB::table('tbl_deal as d')
                    ->leftjoin('tbl_deal_stage as ds', 'ds.id', '=', 'd.stage_id')
                    ->select(DB::raw("Concat(e.f_name,' ',e.l_name ) as employee_name"), 'e.id as emp_id', DB::raw("count(d.id) as deal_count"), DB::raw("sum(d.amount) as amount"), 'ds.name as stage_name')
                    ->leftjoin('tbl_employee as e', 'e.id', '=', 'd.emp_id')
                    //  ->leftjoin('tbl_deal_followers as df','df.deal_id','=','d.id')
                    ->where('e.is_active', '=', 1)
                    ->where('d.is_active', '=', 1)
                    ->groupby('d.stage_id', 'd.emp_id');

            if ($empid != '') {
                if ($empid->id == $res->emp_id) {
                    $builder->whereRaw(DB::Raw("(d.emp_id = $res->emp_id )"));
                } else {
                    $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid) {
                        $join->on('d.id', '=', 'df.deal_id');
                        //    $join->where('df.emp_id','=',DB::raw("'" . $empid->id . "'"));
                    });
                    $builder->whereRaw(DB::Raw("(df.emp_id= $empid->id and d.emp_id=$res->emp_id )"));
                }
            } else {
                $builder->whereRaw(DB::Raw("(d.emp_id= $res->emp_id )"));
            }

            if (!empty($from_date)) {
                $builder->whereDate('d.created_at', '>=', $from_date);
            }
            if (!empty($to_date)) {
                $builder->whereDate('d.created_at', '<=', $to_date);
            }
            $res->stage = $value->get();
        }

        $resVal['list'] = $response;
        return $resVal;
    }

    public function createdDealReport(Request $request) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_deal as d')
                ->select('u.f_name as employee_name', 'u.id as user_id', DB::raw("count(d.id) as deal_count"))
                ->leftjoin('tbl_user as u', 'u.id', '=', 'd.created_by')
                ->where('u.is_active', '=', 1)
                ->where('d.is_active', '=', 1)
                ->groupby('u.id');

        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
        if (!$isVisible) {
            $builder->where('d.created_by', '=', $curr_id);
        }
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//             $builder->where('d.created_by', '=',$curr_id);
//        }

        if (!empty($from_date)) {
            $builder->whereDate('d.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('d.created_at', '<=', $to_date);
        }


        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function stageWiseReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_deal as d')
                ->select('ds.name as stage_name', 'ds.id as stage_id', DB::raw("count(d.id) as stage_count"))
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'd.emp_id')
//                 ->leftjoin('tbl_deal_followers as df','df.deal_id','=','d.id')
                ->leftjoin('tbl_deal_stage as ds', 'ds.id', '=', 'd.stage_id')
                ->where('d.is_active', '=', 1)
                ->where('ds.is_active', '=', 1)
                ->where('e.is_active', '=', 1)
                ->groupBy('ds.id');

        $currentuser = Auth::user();
        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();
        $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
        if (!empty($empid)) {
            if (!$isVisible) {
                $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid) {
                    $join->on('d.id', '=', 'df.deal_id');
                    $join->where('df.emp_id', '=', DB::raw("'" . $empid->id . "'"));
                });
                $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
            }
        }
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//            // $builder->where('d.created_by', '=',$curr_id);
//            $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid){
//                $join->on('d.id', '=', 'df.deal_id');
//                $join->where('df.emp_id','=',DB::raw("'" . $empid->id . "'"));
//             });
//            $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
//        }


        if (!empty($from_date)) {
            $builder->whereDate('d.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('d.created_at', '<=', $to_date);
        }

        $builder->orderBy('stage_count', 'desc');

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function dealTypeBasedReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table('tbl_deal as d')
                ->select('lm.name as deal_category_name', 'lm.id as deal_category_id', DB::raw("count(d.id) as deal_category_count"))
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'd.emp_id')
//                 ->leftjoin('tbl_deal_followers as df','df.deal_id','=','d.id')
                ->leftjoin('tbl_lead_master as lm', 'lm.id', '=', 'd.deal_category_id')
                ->whereRaw(DB::raw("(lm.type = 'both' or lm.type='deal')"))
                ->where('d.is_active', '=', 1)
                //    ->where('lm.is_active', '=', 1)
                ->where('e.is_active', '=', 1)
                ->groupBy('lm.id');

        $currentuser = Auth::user();
        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();
        if (!empty($empid)) {
            $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
            if (!$isVisible) {
                $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid) {
                    $join->on('d.id', '=', 'df.deal_id');
                    $join->where('df.emp_id', '=', DB::raw("'" . $empid->id . "'"));
                });
                $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
            }
        }
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//            // $builder->where('d.created_by', '=',$curr_id);
//           $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid){
//                $join->on('d.id', '=', 'df.deal_id');
//                $join->where('df.emp_id','=',DB::raw("'" . $empid->id . "'"));
//             });
//             $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
//        }

        if (!empty($from_date)) {
            $builder->whereDate('d.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('d.created_at', '<=', $to_date);
        }
        $builder->orderBy('deal_category_count', 'desc');

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function dealCityBasedReport(Request $request) {

        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $builder = DB::table('tbl_deal as d')
                ->select('cit.name as city_name', 'cit.id as city_id', DB::raw("count(d.id) as deal_count"))
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'd.emp_id')
//                 ->leftjoin('tbl_deal_followers as df','df.deal_id','=','d.id')
                ->leftjoin('tbl_contact as c', 'd.contact_id', '=', 'c.id')
                ->join('tbl_company as com', 'com.id', '=', 'c.company_id')
                ->join('tbl_city as cit', 'cit.id', '=', 'com.city_id')
                ->where('d.is_active', '=', 1)
                //  ->where('cit.is_active', '=', 1)
                //   ->where('c.is_active', '=', 1)
                //    ->where('com.is_active', '=', 1)
//                ->where('e.is_active', '=', 1)
                ->groupBy('cit.id');

        $currentuser = Auth::user();
        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();
        if (!empty($empid)) {
            $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
            if (!$isVisible) {
                $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid) {
                    $join->on('d.id', '=', 'df.deal_id');
                    $join->where('df.emp_id', '=', DB::raw("'" . $empid->id . "'"));
                });
                $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
            }
        }
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//            // $builder->where('d.created_by', '=',$curr_id);
//           $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid){
//                $join->on('d.id', '=', 'df.deal_id');
//                $join->where('df.emp_id','=',DB::raw("'" . $empid->id . "'"));
//             });
//             $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
//        }

        if (!empty($from_date)) {
            $builder->whereDate('d.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('d.created_at', '<=', $to_date);
        }

        $builder->orderBy('deal_count', 'desc');

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function dealNameBasedReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $builder = DB::table("tbl_deal as d")
                ->select('d.name as deal_name', DB::raw("count(d.id) as deal_count"))
                ->leftjoin('tbl_employee as e', 'e.id', '=', 'd.emp_id')
//                ->leftjoin('tbl_deal_followers as df','df.deal_id','=','d.id')
                ->where('d.is_active', '=', 1)
                ->where('e.is_active', '=', 1)
                ->groupby('d.name');

        if (!empty($from_date)) {
            $builder->whereDate('d.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('d.created_at', '<=', $to_date);
        }
        $builder->orderBy('deal_count', 'desc');

        $currentuser = Auth::user();
        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();
        if (!empty($empid)) {
            $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
            if (!$isVisible) {
                $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid) {
                    $join->on('d.id', '=', 'df.deal_id');
                    $join->where('df.emp_id', '=', DB::raw("'" . $empid->id . "'"));
                });
                $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
            }
        }
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//            // $builder->where('d.created_by', '=',$curr_id);
//            $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid){
//                $join->on('d.id', '=', 'df.deal_id');
//                $join->where('df.emp_id','=',DB::raw("'" . $empid->id . "'"));
//             });
//            $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
//        }

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function dailyActivity(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $date = $request->input('date');
        $emp_id = $request->input('emp_id');
        $team_id = $request->input('team_id');
        $city_id = $request->input('city_id');
        $city_name = $request->input('city_name');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $updated_at = $request->input('updated_at');
        //groupby
        $builder = DB::table('tbl_employee as e')
                ->select('e.id', DB::raw("Concat(e.f_name,' ',e.l_name ) as emp_name"),
                        //   DB::raw("min(a.start_time) as punchIN"), DB::raw("max(a.end_time) as punchOut"),
                        'et.name as team_name', 'e.city as city')
                // ->leftjoin('tbl_attendance as a', 'e.id', '=', 'a.emp_id')
                ->join('tbl_emp_team as et', 'e.team_id', '=', 'et.id')
                ->where('e.is_active', '=', 1)
                // ->where('a.is_active', '=', 1)
                ->groupBy('e.id');

//        if (!empty($date)) {
//            $builder->whereDate('a.start_time', '=', $date);
//        }
        if (!empty($emp_id)) {
            $builder->where('e.id', '=', $emp_id);
        }
        if (!empty($city_id)) {
            $builder->where('e.city_id', '=', $city_id);
        }
        if (!empty($team_id)) {
            $builder->where('e.team_id', '=', $team_id);
        }

        if (!empty($city_name)) {
            $builder->where('e.city', 'like', '%' . $city_name . '%');
        }

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {
            $collection = $builder->skip($start)->take($limit)->get();
        }

        foreach ($collection as $collect) {

            $em_id = $collect->id;

            $builder1 = DB::table("tbl_deal as d")
                    ->select('d.id as deal_id', 'd.deal_category_id', 'lm.name as deal_category_name', 'd.name as subject', 'd.stage_id', 'ds.name as stage_name'
                            , 'd.amount', DB::raw("(Case when d.next_follow_up='0000-00-00 00:00:00' then null else d.next_follow_up end) as next_follow_up_date"), 'd.created_at'
                            , 'd.updated_at', 'c.fname as lead_name', 'com.name as company_name', 'com.id as company_id', 'd.created_by_name')
                    ->leftjoin('tbl_deal_activity as da', 'd.id', '=', 'da.deal_id')
                    ->leftjoin('tbl_employee as e', 'e.id', '=', 'd.emp_id')
                    ->leftjoin('tbl_deal_followers as df', 'df.deal_id', '=', 'd.id')
                    ->join('tbl_contact as c', 'd.contact_id', '=', 'c.id')
                    ->join('tbl_company as com', 'com.id', '=', 'c.company_id')
                    ->join('tbl_lead_master as lm', 'lm.id', '=', 'd.deal_category_id')
                    ->join('tbl_deal_stage as ds', 'd.stage_id', '=', 'ds.id')
                    ->where('d.is_active', '=', 1)
                    ->where('d.emp_id', '=', $em_id)
                    ->where('lm.is_active', '=', 1)
                    ->where('ds.is_active', '=', 1)
                    ->where('e.is_active', '=', 1)
//                    ->whereDate('da.date', '=', $date)
                    ->whereRaw(DB::raw("(lm.type='deal' or lm.type='both')"));


            if (!empty($date)) {
                $builder1->whereDate('da.date', '=', $date);
            }
            if (!empty($from_date)) {
                $builder1->whereDate('da.date', '>=', $from_date);
            }
            if (!empty($to_date)) {
                $builder1->whereDate('da.date', '<=', $to_date);
            }
            if (!empty($updated_at)) {
                $builder1->whereDate('da.updated_at', '=', $updated_at);
            }

            $builder1->groupby('d.id');


            //dealactivity,lead inner join, deal type 
            $collect->daily_activity = $builder1->get();
        }
        foreach ($collection as $collect) {
            $em_id = $collect->id;
            $builder = DB::table('tbl_employee as e')
                    ->select('e.id', DB::raw("Concat(e.f_name,' ',e.l_name ) as emp_name"), DB::raw("min(a.start_time) as punchIN"), DB::raw("max(a.end_time) as punchOut"), 'et.name as team_name')
                    ->leftjoin('tbl_attendance as a', 'e.id', '=', 'a.emp_id')
                    ->join('tbl_emp_team as et', 'e.team_id', '=', 'et.id')
                    ->where('e.id', '=', $em_id)
                    ->where('a.is_active', '=', 1);
            if (!empty($date)) {
                $builder->whereDate('a.start_time', '=', $date);
            }
            if (!empty($from_date)) {
                $builder->whereDate('a.start_time', '>=', $from_date);
            }
            if (!empty($to_date)) {
                $builder->whereDate('a.start_time', '<=', $to_date);
            }

            $builder1 = $builder->first();
            $collect->punchIN = $builder1->punchIN;
            $collect->punchOut = $builder1->punchOut;
        }

        $resVal['list'] = $collection;

        return $resVal;
    }

    public function inactiveDeals(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $days = $request->input('days');
        $date = Date('Y-m-d');
        $stage_id = $request->input('stage_id');


        $newdate = strtotime('-' . $days . 'day', strtotime($date));
        $newdate = date('Y-m-d', $newdate);
        //echo $newdate; 
        //2018-03-1


        $builder = DB::table('tbl_deal as d')
                        ->select('d.*', 'lm.name as deal_category_name', 'c.company_id', 'com.name as company_name', 'ds.name as deal_stage_name', 'ds.stage_flag')
                        ->leftjoin('tbl_deal_activity as da', 'd.id', '=', 'da.deal_id')
                        ->leftjoin('tbl_deal_stage as ds', 'd.stage_id', '=', 'ds.id')
                        ->join('tbl_lead_master as lm', 'lm.id', '=', 'd.deal_category_id')
                        ->leftjoin('tbl_employee as e', 'e.id', '=', 'd.emp_id')
//                ->leftjoin('tbl_deal_followers as df','df.deal_id','=','d.id')
                        ->join('tbl_contact as c', 'd.contact_id', '=', 'c.id')
                        ->join('tbl_company as com', 'c.company_id', '=', 'com.id')
                        ->whereDate('d.updated_at', '<', $newdate)
                        // ->whereRaw(DB::raw("da.updated_at NOT BETWEEN CAST(date_sub('$date',INTERVAL $days day) as date) and CAST('$date' AS DATE)"))
                        ->where('d.is_active', '=', 1)
                        ->where('lm.is_active', '=', 1)
                        ->where('e.is_active', '=', 1)
                        ->groupBy('d.id')->orderBy('d.updated_at');

        $currentuser = Auth::user();
        $curr_id = $currentuser->id;
        $user = DB::table('tbl_user')
                ->select('*')
                ->where('id', '=', $curr_id)
                ->first();
        $role_name = $user->rolename;
        $empid = DB::table('tbl_employee')->where('user_id', '=', $curr_id)->first();

        $isVisible = AuthorizationHelper::checkVisblityAccess($screen_code);
        if (!$isVisible) {
            $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid) {
                $join->on('d.id', '=', 'df.deal_id');
                $join->where('df.emp_id', '=', DB::raw("'" . $empid->id . "'"));
            });
            $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
        }

//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0 || strcasecmp($role_name,'BACKEND TEAM')==0 || strcasecmp($role_name,'ACCOUNTS')==0) {  
//        }else{
//            // $builder->where('d.created_by', '=',$curr_id);
//            $builder->leftJoin('tbl_deal_followers as df', function($join) use ($empid){
//                $join->on('d.id', '=', 'df.deal_id');
//                $join->where('df.emp_id','=',DB::raw("'" . $empid->id . "'"));
//             });
//            $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
//        }

        if (!empty($stage_id)) {
            $builder->where('d.stage_id', '=', $stage_id);
        }

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function dealWithoutLead(Request $request) {

        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $contact_id = $request->input('contact_id');
        $contact_owner_id = $request->input('contact_owner_id');


        $builder = DB::table('tbl_contact as c')
                ->select('c.*', 'lm.name as lead_type_name', 'com.name as company_name', DB::raw("Concat(e.f_name,' ',e.l_name ) as contact_owner_name"))
                ->leftjoin('tbl_deal as d', 'c.id', '=', 'd.contact_id')
                ->join('tbl_lead_master as lm', 'lm.id', '=', 'c.lead_type_id')
                ->join('tbl_company as com', 'com.id', '=', 'c.company_id')
                ->join('tbl_employee as e', 'e.id', '=', 'c.contact_owner_id')
                ->where('c.is_active', '=', 1)
                //  ->where('lm.is_active', '=', 1)
                //  ->where('com.is_active', '=', 1)
                ->whereRaw(DB::raw("d.id is null"));

//         $currentuser = Auth::user();
//        $curr_id = $currentuser->id;
//        $user = DB::table('tbl_user')
//                ->select('*')
//                ->where('id', '=', $curr_id)
//                ->first();
//        $role_name = $user->rolename;
//        $empid=DB::table('tbl_employee')->where('user_id','=',$curr_id)->first();
//        if (strcasecmp($role_name,'superadmin')==0 || strcasecmp($role_name,'admin')==0) {  
//        }else{
//            // $builder->where('d.created_by', '=',$curr_id);
//            $builder->whereRaw(DB::Raw("(d.emp_id = $empid->id or df.emp_id =$empid->id )"));
//        }

        if (!empty($from_date)) {
            $builder->whereDate('c.created_at', '=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('c.created_at', '=', $to_date);
        }
        if (!empty($contact_id)) {
            $builder->where('c.id', '=', $contact_id);
        }
        if (!empty($contact_owner_id)) {
            $builder->where('c.contact_owner_id', '=', $contact_owner_id);
        }


        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function SalesOrderPendingReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $customer_id = $request->input('customer_id');
        $city = $request->input('city', '');

        $builder = DB::table('tbl_sales_order as s')
                ->select('s.id as sales_order_id', 's.date', 'c.customer_code', 'c.fname as customer_name', 'c.company', 'c.billing_city', 'c.shopping_city', 'c.phone', 'si.product_name', 'si.product_sku')
                ->leftjoin('tbl_sales_order_item as si', 's.id', '=', 'si.sales_order_id')
                ->leftjoin('tbl_customer as c', 'c.id', '=', 's.customer_id')
                ->where('s.is_active', '=', 1)
                ->where('s.status', '!=', 'invoiced')
                ->groupby('si.id');

        if (!empty($from_date)) {
            $builder->whereDate('s.date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('s.date', '<=', $to_date);
        }
        if (!empty($customer_id)) {
            $builder->where('s.customer_id', '=', $customer_id);
        }

        if (!empty($city)) {
            $builder->where('c.billing_city_id', '=', $city);
        }

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function customerReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $customer_id = $request->input('customer_id', '');

        $work = DB::table('tbl_work_order as wo')
                ->leftjoin('tbl_customer as c', 'wo.customer_id', '=', 'c.id')
                ->select('wo.id as referenceId', 'c.id as customer_id', 'c.phone as phone', DB::raw("CONCAT(c.fname, c.lname) AS customer_name"), DB::raw("'workOrder' as type"), DB::raw("date(wo.created_at) as date"))
                ->where('wo.is_active', '=', 1);
        if (!empty($customer_id)) {
            $work->where('customer_id', '=', $customer_id);
        }
        if (!empty($from_date)) {
            $work->whereDate('wo.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $work->whereDate('wo.created_at', '>=', $from_date);
        }

        $purchase = DB::table('tbl_purchase_invoice as pi')
                ->leftjoin('tbl_customer as c', 'pi.customer_id', '=', 'c.id')
                ->select('pi.id as referenceId', 'c.id as customer_id', 'c.phone as phone', DB::raw("CONCAT(c.fname, c.lname) AS customer_name"), DB::raw("'purchase' as type"), 'pi.date')
                ->where('pi.is_active', '=', 1)
                ->where('pi.type', '=', 'grn');
        if (!empty($customer_id)) {
            $purchase->where('customer_id', '=', $customer_id);
        }
        if (!empty($to_date)) {
            $purchase->whereDate('date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $purchase->whereDate('date', '>=', $from_date);
        }

        $builder = DB::table('tbl_grn as gr')
                ->leftjoin('tbl_customer as c', 'gr.customer_id', '=', 'c.id')
                ->select('gr.id as referenceId', 'c.id as customer_id', 'c.phone as phone', DB::raw("CONCAT(c.fname, c.lname) AS customer_name"), DB::raw("'grn' as type"), 'gr.date')
                ->where('gr.is_active', '=', 1)
                ->union($work)
                ->union($purchase);

        if (!empty($customer_id)) {
            $builder->where('customer_id', '=', $customer_id);
        }
        if (!empty($from_date)) {
            $builder->whereDate('date', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('date', '>=', $from_date);
        }


        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

    public function MaterialRecuritmentReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $product_id = $request->input('product_id');
        $category_id = $request->input('category_id');

        $builder = DB::table('tbl_work_order_input as wi')
                ->select('p.name as product_name', 'p.id as product_id', DB::raw("sum(coalesce(wi.to_consume,0)) as req_qty"), DB::raw("sum(coalesce((wi.to_consume-wi.reserved_qty),0)) as bal_qty"), DB::raw("coalesce(i.quantity,0) as available_qty"), 'c.name as category_name', 'm.created_at as date')
                ->leftjoin('tbl_manufacturing_order as m', 'm.id', '=', 'wi.mo_id')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'wi.input_pdt_id')
                ->leftjoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                ->leftjoin('tbl_category as c', 'c.id', '=', 'p.category_id')
                ->whereIn('m.status', ['New', 'initiated'])
                ->where('wi.is_active', '=', 1)
                ->where('p.is_purchase', '=', 1)
                ->groupBy('p.id');

        if (!empty($from_date)) {
            $builder->whereDate('m.created_at', '>=', $from_date);
        }
        if (!empty($to_date)) {
            $builder->whereDate('m.created_at', '<=', $to_date);
        }
        if (!empty($product_id)) {
            $builder->where('wi.input_pdt_id', '=', $product_id);
        }
        if (!empty($category_id)) {
            $builder->where('c.id', '=', $category_id);
        }

        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {
            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }

        return $resVal;
    }

}

?>
