<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\DeviceDetail;
use Illuminate\Http\Request;
use App\Transformer\DeviceDetailTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeviceDetailController
 *
 * @author Deepa
 */
class DeviceDetailController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'DeviceDetail Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $Collection = DeviceDetail::where('user_id', "=", $request->user_id)
                ->where('device_id', "=", $request->device_id)
                ->where('is_active', "=", 1)
                ->get();

        if (count($Collection) == 0) {
            $device = new DeviceDetail;
            $device->created_by = $currentuser->id;
            $device->updated_by = $currentuser->id;
            $device->is_active = $request->input('is_active', 1);

            $device->fill($request->all());
            $device->save();
        } else {
            return $resVal;
        }
        $resVal['id'] = $device->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $device_id = $request->input('device_id', '');
        $user_id = $request->input('user_id', '');
        $is_active = $request->input('is_active', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_device_detail')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if (!empty($device_id)) {
            $builder->where('device_id', '=', $device_id);
        }
        if (!empty($user_id)) {
            $builder->where('user_id', '=', $user_id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }


        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'DeviceDetail Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $device = DeviceDetail::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'DeviceDetail Not found';
            return $resVal;
        }


        $Collection = DeviceDetail::where('user_id', "=", $request->user_id)
                ->where('device_id', "=", $request->device_id)
                ->where('is_active', "=", 1)
                ->get();

        if (count($Collection) == 0) {
            $device->updated_by = $currentuser->id;
            $device->fill($request->all());
            $device->save();
        } else {
            return $resVal;
        }


        return $resVal;
    }

    public function delete(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'DeviceDetail Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $id = $request->input('id');
        $device_id = $request->input('device_id', '');
        $user_id = $request->input('user_id', '');
        $currentuser = Auth::user();

        if (!empty($device_id) && !empty($user_id)) {

            $detail = DB::table('tbl_device_detail')->where('device_id', '=', $device_id)->where('user_id', '=', $user_id)
                    ->count();
            if ($detail > 0) {

                $detail = DB::table('tbl_device_detail')->where('device_id', '=', $device_id)->where('user_id', '=', $user_id)
                        ->update(['is_active' => 0], ['updated_by' => $currentuser->id]);
            }
        } elseif (!empty($id)) {
            $detail = DB::table('tbl_device_detail')->where('device_id', '=', $device_id)->where('user_id', '=', $user_id)
                    ->count();
            if ($detail > 0) {

                try {
                    $device = DeviceDetail::findOrFail($id);
                } catch (ModelNotFoundException $e) {
                    $resVal['success'] = FALSE;
                    $resVal['message'] = 'DeviceDetail Not found';
                    return $resVal;
                }

                $device->updated_by = $currentuser->id;
                $device->is_active = 0;
                $device->save();
            }
        }

        return $resVal;
    }

}

?>
