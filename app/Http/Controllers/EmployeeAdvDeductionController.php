<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\EmployeeAdvDeduction;
use App\DealStage;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\ActivityLogHelper;
use App\Helper\AdvanceDeductionHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class EmployeeAdvDeductionController extends Controller {

    //put your code here

     public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Employee Advance Deduction Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $employee_list = $request->all();
     
        foreach ( $employee_list as $employee)  {
     
            $emp_adv_deduct = new EmployeeAdvDeduction;
            $deduction_amt=$employee['deduction_amt'];
            $adv_id = $employee['advance_amt_id'];
             if($deduction_amt>0)
          {
            $emp_adv_deduct->created_by = $currentuser->id;
            $emp_adv_deduct->updated_by = $currentuser->id;
            $emp_adv_deduct->fill($employee);
            $emp_adv_deduct->save();   
            AdvanceDeductionHelper::balanceDecrease($adv_id, $deduction_amt);
           }
           
        }
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $emp_code = $request->input('emp_code', '');
        $advance_amt_id = $request->input('advance_amt_id', '');
        $is_active = $request->input('is_active', '');
        $date = $request->input('date');
        $name = $request->input('name', '');
        $type = $request->input('type', '');
         $emp_id=$request->input('emp_id');



        //$status = $request->input('status', '');
        $builder = DB::table('tbl_emp_adv_deduction as ea')
                ->leftjoin('tbl_employee as ep', 'ep.id', '=', 'ea.emp_id')
                ->leftjoin('tbl_employee_advance as ad', 'ad.id', '=', 'ea.advance_amt_id')
                ->select('ea.*','ep.f_name','ep.l_name','ad.adv_amount','ad.total_deduction_amt','ad.balance_amt'   );
        
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('ea.id', '=', $id);
        }
        if (!empty($advance_amt_id)) {
            $builder->where('ea.advance_amt_id', '=', $advance_amt_id);
        }
        if (!empty($date)) {
            $builder->where('ea.date', '=', $date);
        }
        if ($is_active != '') {
            $builder->where('ea.is_active', '=', $is_active);
        }


        if (!empty($emp_code)) {
            $builder->where('ea.emp_code', 'like', '%' . $emp_code . '%');
        }
        if (!empty($name)) {
            $builder->where('ep.f_name', 'like', '%' . $name . '%');
        }
         if (!empty($emp_id)) {
            $builder->where('ep.id', '=',$emp_id);
        }
        if($type==1)
        {
            $builder->where('ad.balance_amt', '>',  0);
        }


        $builder->orderBy('ea.id', 'desc');

        $resVal['total'] = $builder->count();
        if ($start == 0 && $limit == 0) {
            $resVal['list'] = $builder->get();
        } else {

            $resVal['list'] = $builder->skip($start)->take($limit)->get();
        }
        return ($resVal);
    }

     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Employee Advance deduction Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_adv_deduct = EmployeeAdvDeduction::findOrFail($id);
            if ($emp_adv_deduct->is_active == 0) {
                return $resVal;
            }
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee BONUS   Not found';
            return $resVal;
        }

        $adv_id = $emp_adv_deduct->advance_amt_id;
        $deduction_amt = $emp_adv_deduct->deduction_amt; 
        $deduction_amt2 = $request->input('deduction_amt');
        $amount=$deduction_amt2-$deduction_amt;
        $builder = DB::table('tbl_employee_advance')
                   ->select('balance_amt')
                   ->where('id','=',$adv_id)
                   ->first();
        $bal=$builder->balance_amt;
          if($deduction_amt2>0 && $amount<=$bal)
          {
             $emp_adv_deduct->updated_by = $currentuser->id;
             $emp_adv_deduct->fill($request->all());
             $emp_adv_deduct->save();
             AdvanceDeductionHelper::balanceDecrease($adv_id, $amount);
          }
        
       // AdvanceDeductionHelper::balanceIncrease($adv_id, $deduction_amt);
        return $resVal;
    }
    
    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Employee Advance Deduction  Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');

        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $emp_adv_deduct = EmployeeAdvDeduction ::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Employee Advance Deduction    Not found';
            return $resVal;
        }

        $adv_id = $emp_adv_deduct->advance_amt_id;
        $deduction_amt = $emp_adv_deduct->deduction_amt;
        AdvanceDeductionHelper::balanceIncrease($adv_id, $deduction_amt);

        $emp_adv_deduct->updated_by = $currentuser->id;
        $emp_adv_deduct->is_active = 0;

        $emp_adv_deduct->update();
        $resVal['id'] = $emp_adv_deduct->id;
        return $resVal;
    }

}

?>
