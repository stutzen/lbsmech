<?php
namespace App\Http\Controllers;
use DB;
use App\Tasks;
use Illuminate\Http\Request;
//use App\Transformer\AttributesTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TasksController
 *
 * @author Deepa
 */
class TasksController extends Controller {
    //put your code here
    public function save(Request $request){
         $resVal = array();
        $resVal['message'] = 'Tasks Added Successfully';
        $resVal['success'] = TRUE;
        $currentuser = Auth::user();
       $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $task = new Tasks;
       
       
        $task->created_by = $currentuser->id;
        $task->updated_by = $currentuser->id;
        $task->fill($request->all());
        $task->save();

        $resVal['id'] = $task->id;

        return $resVal;
    
    }
}

?>
