<?php
namespace App\Http\Controllers;
use DB;
use App\TaskType;
use App\TaskActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use App\Helper\LogHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaskTypeController
 *
 * @author Stutzen Admin
 */
class TaskTypeController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Task Type Saved Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $taskType = new TaskType;
        $taskType->created_by = $currentuser->id;
        $taskType->updated_by = $currentuser->id;
        $taskType->is_active = $request->input('is_active', 1);
        $taskType->fill($request->all());
        $taskType->save();
        $resVal['id'] = $taskType->id;
       //  LogHelper::info('Task Type Save'.$request->fullurl());
        return $resVal;
    }
    
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'TaskType Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();
        try {
            $taskType = TaskType::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Task Type Not found';
            return $resVal;
        }
        $taskType->updated_by = $currentuser->id;
        $taskType->fill($request->all());
        $taskType->save();
        
         
          $type_id=$taskType->id;
          $type_name=$taskType->type_name;
     
         DB::table('tbl_task_activity')->where('type_id', $type_id)->update(['type_name' => $type_name]);

        //  LogHelper::info('Task Type Update'.$request->fullurl());
        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $type_name = $request->input('type_name', '');
        $is_active = $request->input('is_active', '');

        $builder = DB::table('tbl_task_type')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($type_name)) {
            $builder->where('type_name', 'like', '%' . $type_name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('id', 'desc');
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
            //LogHelper::info('Task Type Filters'.$request->fullurl());
         // LogHelper::info('Task Type List'.$resVal['list']);
        return ($resVal);
    }

    public function delete(Request $request,$id) {
        $resVal = array();
        $currentuser = Auth::user();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'TaskType Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        try {
            $taskType = TaskType::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'TaskType Not found';
            return $resVal;
        }

        $taskType->is_active=0;
        $taskType->updated_by=$currentuser->id;
        $taskType->save();

        // LogHelper::info('Task Type Delete'.$request->fullurl());
        return $resVal;
    }

}

?>
