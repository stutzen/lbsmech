<?php

namespace App\Http\Controllers;

use DateTime;
use App\User;
use Carbon\Carbon;
use App\PersistentLogin;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Transformer\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Mail\Message;
use Swift_Message;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;
use mPDF;
use mpdfform;
use App\Helper\PdfHelper;

class PdfController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'User Login Successfully.';

        PdfHelper::invoicePrintTemplate();

         
        return $resVal;
    }

}
