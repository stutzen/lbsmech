<?php

namespace App\Http\Controllers;

use DB;
use App\Product;
use App\Inventory;
use App\InventoryAdjustment;
use App\Uom;
use App\ProductDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;
use Validator;
use App\Attachment;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemController
 *
 * @author Deepa
 */
class ProductController extends Controller {

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Product Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        
        if($request->input('sku') != ''){

        $validator = Validator::make($request->all(), [
                    'sku' => 'required|unique:tbl_product,sku'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            if (array_key_exists('sku', $validator->failed())) {
                $resVal['message'] = 'Sku Name is already exist';
            }

            return $resVal;
        }
        }

        $currentuser = Auth::user();

        $item = new Product;
        if (!empty($request->input('uom_id', ''))) {
            $get_uom_name = DB::table('tbl_uom')
                            ->select('name')
                            ->where('id', '=', $request->uom_id)->first();
            $item->uom = $get_uom_name->name;
        } else {
            $item->uom = '';
        }
        $item->min_stock_qty = $request->min_stock_qty;
        $item->has_inventory = $request->has_inventory;
        $item->created_by = $currentuser->id;
        $item->updated_by = $currentuser->id;

        $item->fill($request->all());
        $item->save();
        
        $product_attachment = $request->input('attachment');
        $product_detail=$request->input('detail');

            foreach ($product_attachment as $attachment) {
                $productAttachment = new Attachment;
                $productAttachment->fill($attachment);
                $productAttachment->ref_id = $item->id;
                $productAttachment->created_by = $currentuser->id;
                $productAttachment->updated_by = $currentuser->id;
                $productAttachment->is_active = $request->input('is_active', 1);
                $productAttachment->save();
            }
            
            foreach($product_detail as $prod){
                $detail=new ProductDetail;
                $detail->fill($prod);
                $detail->created_by=$currentuser->id;
                $detail->updated_by=$currentuser->id;
                $detail->is_active=1;
                $detail->product_id=$item->id;
                $detail->save();
            }


        if ($request->has_inventory == 1) {

            $inventory = new Inventory;
            $inventory->uom_name = $item->uom;
            $inventory->product_id = $item->id;
            $inventory->created_by = $currentuser->id;
            $inventory->updated_by = $currentuser->id;
            $inventory->quantity = $request->opening_stock;
            $inventory->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory->fill($request->all());
            $inventory->save();

            $inventory_adu = new InventoryAdjustment;
            $inventory_adu->product_id = $item->id;
            $inventory_adu->uom_name = $item->uom;
            $inventory_adu->type = "opening_stock";
            $inventory_adu->comments = "From Opening Stock";
            $inventory_adu->created_by = $currentuser->id;
            $inventory_adu->updated_by = $currentuser->id;
            $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
            $inventory_adu->total_stock = $request->opening_stock;
            $inventory_adu->quantity_credit = $request->opening_stock;
            $inventory_adu->quantity_debit = 0;
            $inventory_adu->sku = $item->sku;
            $inventory_adu->save();
        }
//         
        $resVal['id'] = $item->id;

        return $resVal;
    }

    public function product_csvexport(Request $request) {
        $productLists = $this->listAll($request);
        ini_set('max_execution_time', 600); //increase max_execution_time to 10 min if data set is very large
        //create a file
        $filename = " product" . "_" . time() . ".csv";
        //  $filename = "customer" . "_" . date("Y-m-d_H-i", time()) . ".csv";
        $serverName = $_SERVER['SERVER_NAME'];

        $filePath = "download/$serverName/csv";

        if ($filePath) {
            $folderList = explode(DIRECTORY_SEPARATOR, $filePath);

            $folderName = '';
            for ($index = 0; $index < count($folderList); $index++) {

                if ($folderName == '')
                    $folderName = $folderList[$index];
                else
                    $folderName .= DIRECTORY_SEPARATOR . $folderList[$index];

                if (!file_exists($folderName)) {
                    mkdir($folderName, 0777, true);
                }
            }
        }
        $filePath = getcwd() . '/' . $filePath;
        $sr = 1;
        $output = fopen($filePath . "/" . $filename, 'w');
        fputcsv($output, array('#', 'Id', 'Name', 'SKU', 'Sales price', 'Description'));
        foreach ($productLists['list'] as $values) {
            fputcsv($output, array($sr, $values->id, $values->name, $values->sku, $values->sales_price, $values->description));
            // id: 1, customer_id: 37, prefix: "test123r", date: "2017-05-02", duedate: "2017-05-02"
            $sr++;
        }
        return response()->download($filePath . "/" . $filename);
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
       /* $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }*/
        $currentuser = Auth::user();
        $id = $request->input('id');

        $isactive = $request->input('is_active', '');
        $name = $request->input('name', '');
        $sku = $request->input('sku', '');
        $type = $request->input('type', '');
        $issale = $request->input('is_sale', '');
        $has_inventory = $request->input('has_inventory', '');
        $ispurchase = $request->input('is_purchase', '');
        $category_id = $request->input('category_id', '');
        $categoryName = $request->input('categoryName', '');
        $local_data=$request->input('localData');
        $type_id = $request->input('product_type_id');

        $builder = DB::table('tbl_product as p')
                ->leftJoin('tbl_category as c', 'p.category_id', '=', 'c.id')
                ->leftjoin('tbl_product_type as pt','p.product_type_id','=','pt.id')
                ->where('p.is_active', '=', 1)
                ->select('p.*', 'c.name as category','pt.name as productTypeName');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('p.id', '=', $id);
        }
        if (!empty($issale)) {
            $builder->where('p.is_sale', '=', $issale);
        }
        if (!empty($ispurchase)) {
            $builder->where('p.is_purchase', '=', $ispurchase);
        }
        if (!empty($has_inventory)) {
            $builder->where('p.has_inventory', '=', $has_inventory);
        }
        if (!empty($category_id)) {
            $builder->where('p.category_id', '=', $category_id);
        }
        
        if (!empty($categoryName)) {
            $builder->where('c.name', 'like', '%' . $categoryName . '%');
        }
        
        if (!empty($name)) {
            $builder->where('p.name', 'like', '%' . $name . '%');
        }
        if (!empty($sku)) {
            $builder->where('p.sku', 'like', '%' . $sku . '%');
        }
        if ($isactive != '') {
            $builder->where('p.is_active', '=', $isactive);
        }
        if (!empty($type)) {
            $builder->where('p.type', 'like', '%' . $type . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        } 
        if(!empty($type_id)){
            $builder->where('p.product_type_id','=',$type_id);
        }
        $builder->orderBy('p.id', 'desc');
        $resVal['total'] = $builder->count();
          if($local_data==1)
        {
            if ($start == 0 && $limit == 100) {
                $products = $builder->get();
            } else {

                $products = $builder->skip($start)->take($limit)->get();
            }
        }else{
       if ($start == 0 && $limit == 0) {
                $products = $builder->get();
            } else {

                $products = $builder->skip($start)->take($limit)->get();
        }}
        foreach ($products as $product) {
            $attachmet = DB::table('tbl_attachment')->where('ref_id', '=', $product->id)->where('type', '=', 'product')->get();
            $detail = DB::table('tbl_product_detail')->where('product_id', '=', $product->id)->where('is_active', '=', 1)->get();
            $product->attachment = $attachmet;
            $product->detail = $detail;
         
        } 
        
        $resVal['list'] = $products;
        return ($resVal);
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Product Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $item = Product::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Product Not found';
            return $resVal;
        }

         $currentuser = Auth::user();
        $item->created_by = $currentuser->id;
        $item->updated_by = $currentuser->id;
        $item->is_active=0;
        $item->save();
        
         DB::table('tbl_attachment')->where('ref_id', '=', $id)->where('type', '=', 'product')->delete();
         DB::table('tbl_product_detail')->where('product_id', '=', $id)->delete();

        return $resVal;
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Product Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
         if($request->input('sku') != ''){
             $skuItem=DB::table('tbl_product')
                     ->where('sku','=',$request->input('sku'))
                     ->where('is_active','=',1)
                     ->where('id','!=',$id)->get();
             if(count($skuItem)>0){
                 $resVal['success'] = FALSE;
                  $resVal['message'] = 'Sku Name is already exist';
                   return $resVal;
             }
         }
        try {
            $item = Product::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Product Not found';
            return $resVal;
        }
        if (!empty($request->input('uom_id', ''))) {
            $get_uom_name = DB::table('tbl_uom')
                            ->select('name')
                            ->where('id', '=', $request->uom_id)->first();
            $item->uom = $get_uom_name->name;
        } else {
            $item->uom = '';
        }
        $item->created_by = $currentuser->id;
        $item->fill($request->all());
        $item->save();
        
          DB::table('tbl_attachment')->where('ref_id', '=', $id)->where('type', '=', 'product')->delete();
          DB::table('tbl_product_detail')->where('product_id', '=', $id)->delete();
          $product_detail=$request->input('detail');
          
          $product_attachment = $request->input('attachment');

            foreach ($product_attachment as $attachment) {
                $productAttachment = new Attachment;
                $productAttachment->fill($attachment);
                $productAttachment->ref_id = $item->id;
                $productAttachment->created_by = $currentuser->id;
                $productAttachment->updated_by = $currentuser->id;
                $productAttachment->is_active = $request->input('is_active', 1);
                $productAttachment->save();
            }
            
               foreach($product_detail as $prod){
                $detail=new ProductDetail;
                $detail->fill($prod);
                $detail->created_by=$currentuser->id;
                $detail->updated_by=$currentuser->id;
                $detail->is_active=1;
                $detail->product_id=$item->id;
                $detail->save();
            }


//print_r($request->all());


        if ($request->has_inventory == 1) {

            $inventory = Inventory::where('product_id', '=', $id)->where('is_active', '=', 1)->first();

            if (empty($inventory)) {
                $inventory = new Inventory;
                $inventory->uom_name = $item->uom;
                $inventory->sku = $item->sku;
                $inventory->product_id = $item->id;
                $inventory->created_by = $currentuser->id;
                $inventory->updated_by = $currentuser->id;
                // $inventory->quantity = $request->opening_stock;
                $inventory->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory->fill($request->all());
                $inventory->quantity = 0;
                $inventory->is_active = 1;
                $inventory->save();
                
                $inventory_adu = new InventoryAdjustment;
                $inventory_adu->product_id = $item->id;
                $inventory_adu->uom_name = $item->uom;
                $inventory_adu->sku = $item->sku;
                $inventory_adu->type = "opening_stock";
                $inventory_adu->comments = "From Opening Stock";
                $inventory_adu->created_by = $currentuser->id;
                $inventory_adu->updated_by = $currentuser->id;
                $inventory_adu->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory_adu->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;

                $inventory_adu->quantity_credit = 0;
                $inventory_adu->quantity_debit = 0;

                $inventory_adu->total_stock = 0;
                $inventory_adu->is_active = 1;
                $inventory_adu->save();
            } else {
                $inventory->sku = $item->sku;
                $inventory->uom_id = $item->uom_id;
                $inventory->uom_name = $item->uom;
                $inventory->product_id = $item->id;
                $inventory->created_by = $currentuser->id;
                $inventory->updated_by = $currentuser->id;
                $inventory->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
                $inventory->save();
                
                DB::table('tbl_inventory_adjustment')->where('product_id', '=', $id)
                ->update( 
                       array( 
                             "sku" => $item->sku,
                             "uom_id" => $item->uom_id,
                           "uom_name" => $item->uom                             
                             )
                       );
                 
                  
            }
        }
        return $resVal;
    }
    public function pdtListForBill(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        $current_date = Carbon::now();
        $current_date = $current_date->format('Y-m-d');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }

        $id = $request->input('id');
        $isactive = $request->input('is_active', '');
        $search = $request->input('search', '');
        $category = $request->input('category', '');

        $settings = DB::table('tbl_app_config')
                        ->select('*')->where('setting', '=', 'sales_product_list')->where('value', 'product based')->get()->first();

    //If Setting is empty then uni_price from  tbl_purchase_invoice_item else tierPricing                  
        if ($settings == "") {
           // print_r("Purchase");
            $builder = DB::table('tbl_purchase_invoice_item as pi')
                    ->leftJoin('tbl_product as p', 'p.id', '=', 'pi.product_id')
                    ->leftJoin('tbl_category as ca', 'p.category_id', '=', 'ca.id')
                    ->leftJoin('tbl_inventory as i', 'pi.product_id', '=', 'i.product_id')
                    ->select('p.*', 'p.id as product_id', 'p.name as product_name', 'p.sku', 'i.quantity as inventory_qty', 'pi.qty_in_hand', 
                            'pi.mrp_price', 'pi.selling_price', 'ca.name as category_name', 'pi.id as purchase_invoice_item_id');
            $resVal['success'] = TRUE;
            $start = $request->input('start', 0);
            $limit = $request->input('limit', 0);

            if (!empty($id)) {
                $builder->where('pi.id', '=', $id);
            }
            if (!empty($category)) {
                $builder->where('ca.id', '=',  $category);
            }
            if ($isactive != '') {
                $builder->where('pi.is_active', '=', $isactive);
            }

            if (!empty($search)) {
                $builder->whereRaw(" (p.name like '%" . $search . "%' or p.sku like '%" . $search . "%' OR pi.id = '$search' ) ");
            }
            $builder->orderBy('pi.id', 'desc');
            $resVal['total'] = $builder->count();
            if ($start == 0 && $limit == 0) {
                $products = $builder->get();
            } else {

                $products = $builder->skip($start)->take($limit)->get();
            }
            //Tax Mapping
            foreach ($products as $pdt) {
                $pdt_id = $pdt->product_id;
                $taxMapping = DB::table('tbl_tax_mapping as tm')
                        ->leftJoin('tbl_tax as t', 't.id', '=', 'tm.tax_id')
                        ->select('tm.id as id', 't.id as tax_id', 't.tax_name as tax_name', 't.tax_percentage', 't.tax_applicable_amt', 't.start_date', 't.end_date')
                        ->where('product_id', $pdt_id)->where('t.is_active', '=', 1)->where('tm.is_active', '=', 1)
//                        ->where('t.start_date', '<=', $current_date)
//                        ->where('t.end_date', '>=', $current_date);
                ->whereRaw(DB::raw("(start_date <= '$current_date' or  `start_date` is null) and (end_date >= '$current_date' or end_date is null) "));
                $pdt->taxMapping = $taxMapping->get();
            }
            
             //Tier Pricing
            foreach ($products as $pdt) {
                $pdt->tierPricing = [];
            }
            
            $resVal['list'] = $products;
        } else {
           // print_r("Product");
            $builder = DB::table('tbl_product as p')
                    ->leftJoin('tbl_category as ca', 'p.category_id', '=', 'ca.id')
                    ->leftJoin('tbl_inventory as i', 'p.id', '=', 'i.product_id')
                    ->select('p.*', 'p.id as product_id', 'p.name as product_name', 'p.sku', 'i.quantity as inventory_qty',
                            'ca.name as category_name','i.quantity as qty_in_hand', 'p.sales_price as selling_price', 
                    DB::Raw("0 as mrp_price, 0 as purchase_invoice_item_id"));
            $resVal['success'] = TRUE;
            $start = $request->input('start', 0);
            $limit = $request->input('limit', 0);

            if (!empty($id)) {
                $builder->where('p.id', '=', $id);
            }
            if (!empty($category)) {
                $builder->where('ca.id', '=',  $category);
            }
            if ($isactive != '') {
                $builder->where('p.is_active', '=', $isactive);
            }

            if (!empty($search)) {
                $builder->whereRaw(" (p.name like '%" . $search . "%' or p.sku like '%" . $search . "%' OR p.id =' $search') ");
            }
            $builder->orderBy('p.id', 'desc');
            $resVal['total'] = $builder->count();
             if ($start == 0 && $limit == 0) {
                $products = $builder->get();
            } else {

                $products = $builder->skip($start)->take($limit)->get();
            }
            
            //Tax Mapping
            foreach ($products as $pdt) {
                $pdt_id = $pdt->product_id;
                $taxMapping = DB::table('tbl_tax_mapping as tm')
                        ->leftJoin('tbl_tax as t', 't.id', '=', 'tm.tax_id')
                        ->select('tm.id as id', 't.id as tax_id', 't.tax_name as tax_name', 't.tax_percentage', 't.tax_applicable_amt', 't.start_date', 't.end_date')
                        ->where('product_id', $pdt_id)->where('t.is_active', '=', 1)->where('tm.is_active', '=', 1)
//                        ->where('t.start_date', '<=', $current_date)
//                        ->where('t.end_date', '>=', $current_date);
                        ->whereRaw(DB::raw("(start_date <= '$current_date' or  `start_date` is null) and (end_date >= '$current_date' or end_date is null) "));
                $pdt->taxMapping = $taxMapping->get();
            }
            //Tier Pricing wer
            foreach ($products as $pdt) {
                $pdt_id = $pdt->product_id;
                $tierPricing = DB::table('tbl_product_tier_pricing as tm')
                        ->select('tm.*')
                        ->where('product_id', $pdt_id)->where('tm.is_active', '=', 1);
                $pdt->tierPricing = $tierPricing->get();
            }
            $resVal['list'] = $products;
        }
        return ($resVal);
    }
}
?>