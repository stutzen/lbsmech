<?php
namespace App\Http\Controllers;
use App\Role;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserRoleController
 *
 * @author Deepa
 */
class RoleController extends Controller {
    //put your code here
    public function save(Request $request){
        $resVal = array();
        $resVal['message'] = 'Role Added Successfully';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'save');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();


        $userrole = new Role;
        
        if ($request->input('name') != NULL) {

            $userRoleCollection = Role::where('name', "=", $request->input('name'))->get();

            if (count($userRoleCollection) > 0) {
                $resVal['success'] = FALSE;
                $resVal['message'] = 'Name is already exits';
                return $resVal;
            }
        }
        $userrole->created_by = $currentuser->id;
        $userrole->updated_by = $currentuser->id;
        $userrole->fill($request->all());
        $userrole->save();
        $resVal['id'] = $userrole->id;

        return $resVal;
               
    }
     public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Role Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'update');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
         $currentuser = Auth::user();

        try {
            $userrole = Role::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'User Role Not found';
            return $resVal;
        }
        
        $userrole->updated_by = $currentuser->id;
        $userrole->fill($request->all());
        $userrole->save();

        return $resVal;
    }
    
    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'list');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        
        $is_active = $request->input('is_active', '');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_role')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }

        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');

        }
        
        $builder->orderBy('id', 'desc');

        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }
     public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = ' Role Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code,'delete');                 
        if(!$ret_auth)
        {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        try {
            $userrole = Role::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Role Not found';
            return $resVal;
        }

        $userrole->delete();

        return $resVal;
    }
    

    
}

?>
