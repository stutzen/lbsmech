<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\DealStage;
use Illuminate\Http\Request;
use App\Transformer\UomTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UomController
 *
 * @author Deepa
 */
class DealStageController extends Controller {

    //put your code here

    public function save(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Deal Stage Added Successfully.';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'save');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        $validator = Validator::make($request->all(), [
                    'name' => 'required|unique:tbl_deal_stage,name'
        ]);
        if ($validator->fails()) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Error in request format';
            if (array_key_exists('name', $validator->failed())) {
                $resVal['message'] = 'Deal Stage name is already exist';
            }

            return $resVal;
        }
        $deal = new DealStage;

        $deal->created_by = $currentuser->id;
        $deal->updated_by = $currentuser->id;
        $deal->created_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $deal->fill($request->all());
        $deal->save();

        $resVal['id'] = $deal->id;

        return $resVal;
    }

    public function listAll(Request $request) {
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
       /* if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }*/
        $currentuser = Auth::user();

        $id = $request->input('id');
        $name = $request->input('name', '');
        $is_active = $request->input('is_active', '');
        $stage_flag=$request->input('stage_flag');

        //$status = $request->input('status', '');
        $builder = DB::table('tbl_deal_stage')
                ->select('*');
        $resVal['success'] = TRUE;
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 100);

        if (!empty($id)) {
            $builder->where('id', '=', $id);
        }
        if ($is_active != '') {
            $builder->where('is_active', '=', $is_active);
        }
         if (!empty($stage_flag)) {
            $builder->where('stage_flag', '=', $stage_flag);
        }

        if (!empty($name)) {
            $builder->where('name', 'like', '%' . $name . '%');
//            DB::connection()->enableQueryLog();
//            var_dump(DB::getQueryLog());
        }
        $builder->orderBy('level', 'asc');
        
        $resVal['total'] = $builder->count();
        $resVal['list'] = $builder->skip($start)->take($limit)->get();
        return ($resVal);
    }

    public function update(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Deal Stage Updated Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'update');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $deal = DealStage::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $resVal['success'] = FALSE;
            $resVal['message'] = 'Deal Stage Not found';
            return $resVal;
        }
        $deal->updated_by = $currentuser->id;
        $deal->updated_by_name = $currentuser->f_name . ' ' . $currentuser->l_name;
        $deal->fill($request->all());
        DB::table('tbl_deal')->where('stage_id', '=', $id)
                -> update(['stage_name' =>$request->name]);
                       
                 
         
            
        $deal->save();

        return $resVal;
    }

    public function delete(Request $request, $id) {
        $resVal = array();
        $resVal['success'] = TRUE;
        $resVal['message'] = 'Deal Stage Deleted Successfully';
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'delete');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $currentuser = Auth::user();
        try {
            $deal = DealStage::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $resVal['success'] = FALSE;
            $resVal['message'] = 'Deal Stages Not found';
            return $resVal;
        }
 
        $deal->delete();
        $resVal['id'] = $deal->id;

        return $resVal;
    }

}

?>
