<?php

namespace App\Http\Controllers;

use DateTime;
use DateInterval;
use DatePeriod;
use DB;
use App\EmployeeProductionAttendance;
use App\EmployeeProductionDetail;
use App\ActivityLog;
use App\PurchasePayment;
use Carbon\Carbon;
use App\Attributevalue;
use App\Attribute;
use app\Customer;
use Illuminate\Http\Request;
use App\Transformer\InvoiceTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helper\AuthorizationHelper;

class GrnReportsController {

    public function externalWiseProduct(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $current_date = Carbon::now();
        $customer_id = $request->input('customer_id');
        $product_id = $request->input('product_id');
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $builder = DB::table('tbl_work_order as wo')
                ->leftjoin('tbl_work_order_input as woi', 'wo.work_order_input_id', '=', 'woi.id')
                ->leftjoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                ->leftjoin('tbl_customer as cus', 'wo.customer_id', '=', 'cus.id')
                ->leftjoin('tbl_product as p', 'woi.input_pdt_id', '=', 'p.id')
                ->select('wo.customer_id as customer_id', 'cus.fname as customer_name', 'woi.input_pdt_id as product_id', 'woi.input_pdt_name as product_name', 'p.sku as sku', DB::raw("sum(woi.input_qty) as overAllQty"))
                ->whereRaw(("tgd.type = 'external'"))
                ->where('woi.is_active', '=', 1)
                ->groupby('wo.customer_id', 'woi.input_pdt_id');
        if (!empty($customer_id)) {
            $builder->where('wo.customer_id', '=', $customer_id);
        }
        if (!empty($product_id)) {
            $builder->where('woi.input_pdt_id', '=', $product_id);
        }
        if (!empty($fromDate)) {
            $builder->whereDate('wo.start_date', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->whereDate('wo.start_date', '<=', $toDate);
        }
        if ($start == 0 && $limit == 100) {
            $builder = $builder->get();
        } else {
            $builder = $builder->skip($start)->take($limit)->get();
        }
        $resVal['total'] = $builder->count();
        foreach ($builder as $build) {
            $customer_id = $build->customer_id;
            $product_id = $build->product_id;
//                    $work_order_input_id = $build->('woi.id');
            $products = DB::table('tbl_work_order as wo')
                    ->leftjoin('tbl_work_order_input as woi', 'wo.work_order_input_id', '=', 'woi.id')
                    ->leftjoin('tbl_product as p', 'woi.input_pdt_id', '=', 'p.id')
                    ->select('woi.mo_id as mo_id', 'woi.input_pdt_id as product_id', 'woi.input_pdt_name as product_name', 'woi.input_qty', 'p.sku as sku')
                    ->where('wo.customer_id', '=', $customer_id)
                    ->where('woi.input_pdt_id', '=', $product_id)
                    ->where('woi.is_active', '=', 1);
            if (!empty($fromDate)) {
                $products->whereDate('wo.start_date', '>=', $fromDate);
            }
            if (!empty($toDate)) {
                $products->whereDate('wo.start_date', '<=', $toDate);
            }
            $build->products = $products->get();
        }

        $resVal['list'] = $builder;
        return ($resVal);
    }

    //put your code here
    public function receivedQtyReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $customer_id = $request->input('customer_id');
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $builder = DB::table('tbl_manufacturing_order as mo')
                ->leftJoin('tbl_work_order_input as woi', 'woi.mo_id', '=', 'mo.id')
                ->leftJoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                ->leftJoin('tbl_customer as c', 'mo.customer_id', '=', 'c.id')
                ->leftJoin('tbl_product as p', 'mo.output_pdt_id', '=', 'p.id')
                ->select('mo.id', 'mo.customer_id', 'mo.date', DB::raw("sum(mo.produced_qty) as qty"), 'c.fname', 'c.lname', 'p.name', 'p.sku', 'p.uom', 'mo.output_pdt_id')
                ->groupBy(DB::raw("mo.customer_id , mo.output_pdt_id"))
                ->where('mo.is_active', '=', 1)
                ->where('tgd.type', '=', 'external');


        if (!empty($customer_id)) {
            $builder->where('mo.customer_id', '=', $customer_id);
        }

        if (!empty($fromDate)) {
            $builder->whereDate('mo.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->whereDate('mo.date', '<=', $toDate);
        }
        if ($start == 0 && $limit == 0) {
            $builder = $builder->get();
        } else {
            $builder = $builder->skip($start)->take($limit)->get();
        }


        $resVal['success'] = TRUE;

        foreach ($builder as $bu) {
            $customer_id = $bu->customer_id;
            $pro_id = $bu->output_pdt_id;
            $mo_id = $bu->id;


            $builder_detail = DB::table('tbl_manufacturing_order as mo')
                    ->leftJoin('tbl_work_order_input as woi', 'woi.mo_id', '=', 'mo.id')
                    ->leftJoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                    ->leftJoin('tbl_product as p', 'mo.output_pdt_id', '=', 'p.id')
                    ->select('mo.id', 'mo.customer_id', 'mo.date', 'mo.output_pdt_id', "mo.produced_qty as qty", 'p.name', 'p.sku', 'p.uom', "mo.output_pdt_id as product_id")
                    ->where('mo.is_active', '=', 1)
                    ->where('mo.output_pdt_id', '=', $pro_id)
                    ->where('mo.customer_id', $customer_id)
                    ->where('tgd.type', '=', 'external');

            if (!empty($fromDate)) {
                $builder_detail->whereDate('mo.date', '>=', $fromDate);
            }
            if (!empty($toDate)) {
                $builder_detail->whereDate('mo.date', '<=', $toDate);
            }



            $bu->data = $builder_detail->get();
        }
        $resVal['list'] = $builder;
        return ($resVal);
    }

    public function expectedQtyReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $customer_id = $request->input('customer_id');
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);

        $builder = DB::table('tbl_manufacturing_order as mo')
                ->leftJoin('tbl_work_order_input as woi', 'woi.mo_id', '=', 'mo.id')
                ->leftJoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                ->leftJoin('tbl_customer as c', 'mo.customer_id', '=', 'c.id')
                ->select('mo.id', 'mo.customer_id', 'mo.date', DB::raw("sum(mo.qty) as qty"), 'c.fname', 'c.lname', 'mo.output_pdt_id')
                ->groupBy(DB::raw("mo.customer_id , mo.output_pdt_id"))
                ->where('mo.is_active', '=', 1)
                ->where('tgd.type', '=', 'external');

        if (!empty($customer_id)) {
            $builder->where('mo.customer_id', '=', $customer_id);
        }

        if (!empty($fromDate)) {
            $builder->whereDate('mo.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->whereDate('mo.date', '<=', $toDate);
        }
        if ($start == 0 && $limit == 0) {
            $builder = $builder->get();
        } else {
            $builder = $builder->skip($start)->take($limit)->get();
        }

        $resVal['success'] = TRUE;

        foreach ($builder as $bu) {
            $customer_id = $bu->customer_id;
            $mo_id = $bu->id;
            $pro_id = $bu->output_pdt_id;

            $builder_detail = DB::table('tbl_manufacturing_order as mo')
                    ->leftJoin('tbl_work_order_input as woi', 'woi.mo_id', '=', 'mo.id')
                    ->leftJoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                    ->leftJoin('tbl_product as p', 'mo.output_pdt_id', '=', 'p.id')
                    ->select('mo.id', 'mo.customer_id', 'mo.date', "mo.qty  as qty", 'p.name', 'p.sku', 'p.uom', "mo.output_pdt_id as product_id")
                    ->where('mo.output_pdt_id', '=', $pro_id)
                    ->where('mo.is_active', '=', 1)
                    ->where('mo.customer_id', $customer_id)
                    ->where('tgd.type', '=', 'external');
            if (!empty($fromDate)) {
                $builder_detail->whereDate('mo.date', '>=', $fromDate);
            }
            if (!empty($toDate)) {
                $builder_detail->whereDate('mo.date', '<=', $toDate);
            }



            $bu->data = $builder_detail->get();
        }
        $resVal['list'] = $builder;
        return ($resVal);
    }

    public function expectedReceivedDiffQtyReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $customer_id = $request->input('customer_id');
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);


        $builder = DB::table('tbl_manufacturing_order as mo')
                ->leftJoin('tbl_work_order_input as woi', 'woi.mo_id', '=', 'mo.id')
                ->leftJoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                ->leftJoin('tbl_customer as c', 'mo.customer_id', '=', 'c.id')
                ->select('mo.id', 'mo.customer_id', 'mo.date', DB::raw("SUM(case when produced_qty != qty then  qty end) qty"), DB::raw("SUM(case when produced_qty != qty then  produced_qty end) produced_qty"), 'c.fname', 'c.lname', 'mo.output_pdt_id')
                ->groupBy(DB::raw("mo.customer_id , mo.output_pdt_id"))
                ->where('mo.qty', '!=', 'mo.produced_qty')
                ->where('tgd.type', '=', 'external')
                ->where('mo.is_active', '=', 1);

        if (!empty($customer_id)) {
            $builder->where('mo.customer_id', '=', $customer_id);
        }

        if (!empty($fromDate)) {
            $builder->whereDate('mo.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->whereDate('mo.date', '<=', $toDate);
        }
        if ($start == 0 && $limit == 0) {
            $builder = $builder->get();
        } else {
            $builder = $builder->skip($start)->take($limit)->get();
        }
        $resVal['success'] = TRUE;

        foreach ($builder as $bu) {
            $customer_id = $bu->customer_id;
            $mo_id = $bu->id;
            $pro_id = $bu->output_pdt_id;


            $builder_detail = DB::table('tbl_manufacturing_order as mo')
                    ->leftJoin('tbl_work_order_input as woi', 'woi.mo_id', '=', 'mo.id')
                    ->leftJoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                    ->leftJoin('tbl_product as p', 'mo.output_pdt_id', '=', 'p.id')
                    ->select('mo.id', 'mo.customer_id', 'mo.date', DB::raw("(case when produced_qty != qty then  qty end) qty"), DB::raw("(case when produced_qty != qty then  produced_qty end) produced_qty"), 'p.name', 'p.sku', 'p.uom', "mo.output_pdt_id as product_id")
                    ->where('mo.output_pdt_id', '=', $pro_id)
                    ->where('mo.customer_id', $customer_id)
                    ->where('mo.qty', '!=', 'mo.produced_qty')
                    ->where('tgd.type', '=', 'external')
                    ->where('mo.is_active', '=', 1);
            if (!empty($fromDate)) {
                $builder_detail->whereDate('mo.date', '>=', $fromDate);
            }
            if (!empty($toDate)) {
                $builder_detail->whereDate('mo.date', '<=', $toDate);
            }



            $bu->data = $builder_detail->get();
        }
        $resVal['list'] = $builder;
        return ($resVal);
    }

    public function internalMoReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        //Get Current and Previous Date
        $current_date = Carbon::now();

        $customer_id = $request->input('customer_id');
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);
        $mo_id = $request->input('mo_id');

        $builder = DB::table('tbl_work_order_input as woi')
                ->leftJoin('tbl_manufacturing_order as mo', 'woi.mo_id', '=', 'mo.id')
                ->leftJoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                ->leftJoin('tbl_product as p', 'woi.input_pdt_id', '=', 'p.id')
                ->select('mo.date', DB::raw("SUM(input_qty) qty"), 'p.name', 'p.sku', 'p.uom', 'woi.input_pdt_id')
                ->groupBy(DB::raw("woi.input_pdt_id"))
                ->where('tgd.type', '=', 'internal')
                ->where('woi.is_active', '=', 1);

        if (!empty($customer_id)) {
            $builder->where('mo.customer_id', '=', $customer_id);
        }
        if (!empty($mo_id)) {
            $builder->where('mo.id', '=', $mo_id);
        }
        if (!empty($fromDate)) {
            $builder->whereDate('mo.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->whereDate('mo.date', '<=', $toDate);
        }
        if ($start == 0 && $limit == 0) {
            $builder = $builder->get();
        } else {
            $builder = $builder->skip($start)->take($limit)->get();
        }
        $resVal['success'] = TRUE;

        foreach ($builder as $bu) {
            $pro_id = $bu->input_pdt_id;


            $builder_detail = DB::table('tbl_work_order_input as woi')
                    ->leftJoin('tbl_manufacturing_order as mo', 'woi.mo_id', '=', 'mo.id')
                    ->leftJoin('tbl_task_group_detail as tgd', 'woi.task_group_detail_id', '=', 'tgd.id')
                    ->leftJoin('tbl_product as p', 'woi.input_pdt_id', '=', 'p.id')
                    ->leftJoin('tbl_customer as c', 'mo.customer_id', '=', 'c.id')
                    ->select('woi.mo_id', 'mo.date', 'p.name', 'c.fname', 'c.lname', 'p.sku', 'p.uom', "woi.input_pdt_id as product_id", DB::raw("SUM(input_qty) qty"))
                    ->where('woi.input_pdt_id', $pro_id)
                    ->groupBy(DB::raw("woi.mo_id"))
                    ->where('tgd.type', '=', 'internal')
                    ->where('woi.is_active', '=', 1);
            if (!empty($fromDate)) {
                $builder_detail->whereDate('mo.date', '>=', $fromDate);
            }
            if (!empty($toDate)) {
                $builder_detail->whereDate('mo.date', '<=', $toDate);
            }



            $bu->data = $builder_detail->get();
        }
        $resVal['list'] = $builder;
        return ($resVal);
    }

    public function InternalReceivedBasedMoReport(Request $request) {
        $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $customer_id = $request->input('customer_id');
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        $start = $request->input('start', 0);
        $limit = $request->input('limit', 0);
        $mo_id = $request->input('mo_id');

        $builder = DB::table('tbl_work_order as wo')
                ->leftJoin('tbl_manufacturing_order as mo', 'wo.mo_id', '=', 'mo.id')
                ->leftjoin('tbl_work_order_input as woi', 'woi.id', '=', 'wo.work_order_input_id')
                ->leftJoin('tbl_product as p', 'woi.input_pdt_id', '=', 'p.id')
                ->select('mo.date', DB::raw("SUM(wo.produced_qty) as qty"), 'p.name', 'p.sku', 'p.uom', 'woi.input_pdt_id')
                ->where('wo.type', '=', 'internal')
                ->where('wo.is_active', '=', 1)
                ->where('woi.is_active', '=', 1)
                ->groupBy(DB::raw("woi.input_pdt_id"));

        if (!empty($customer_id)) {
            $builder->where('mo.customer_id', '=', $customer_id);
        }
        if (!empty($mo_id)) {
            $builder->where('mo.id', '=', $mo_id);
        }
        if (!empty($fromDate)) {
            $builder->whereDate('mo.date', '>=', $fromDate);
        }
        if (!empty($toDate)) {
            $builder->whereDate('mo.date', '<=', $toDate);
        }
        if ($start == 0 && $limit == 0) {
            $collection = $builder->get();
        } else {
            $collection = $builder->skip($start)->take($limit)->get();
        }
        foreach ($collection as $collect) {
            $pro_id = $collect->input_pdt_id;

            $builder_detail = DB::table('tbl_work_order as wo')
                    ->leftJoin('tbl_manufacturing_order as mo', 'wo.mo_id', '=', 'mo.id')
                    ->leftjoin('tbl_work_order_input as woi', 'woi.id', '=', 'wo.work_order_input_id')
                    ->leftJoin('tbl_product as p', 'woi.input_pdt_id', '=', 'p.id')
                    ->leftJoin('tbl_customer as c', 'mo.customer_id', '=', 'c.id')
                    ->select('wo.mo_id', 'mo.date', 'p.name as product_name', 'c.fname', 'c.lname', 'p.sku', 'p.uom', "woi.input_pdt_id as product_id", DB::raw("SUM(wo.produced_qty) qty"))
                    ->where('woi.input_pdt_id', $pro_id)
                    ->where('wo.type', '=', 'internal')
                    ->where('wo.is_active', '=', 1)
                    ->where('woi.is_active', '=', 1)
                    ->groupBy(DB::raw("mo.id"));

            if (!empty($fromDate)) {
                $builder_detail->whereDate('mo.date', '>=', $fromDate);
            }
            if (!empty($toDate)) {
                $builder_detail->whereDate('mo.date', '<=', $toDate);
            }
            
            $collect->detail=$builder_detail->get();
        }
        
          $resVal['list'] = $collection;
        return ($resVal);
    }
    
    public function MoItemWiseIssueReport(Request $request){
         $resVal = array();
        $resVal['message'] = 'Success';
        $resVal['success'] = TRUE;
        $screen_code = $request->header('screen-code');
        $ret_auth = AuthorizationHelper::check($screen_code, 'list');
        if (!$ret_auth) {
            $resVal['message'] = 'Access Denied';
            $resVal['success'] = FALSE;
            return $resVal;
        }
        $product_id = $request->input('product_id');
        $fromDate = $request->input('from_date');
        $toDate = $request->input('to_date');
        
        $builder=DB::table('tbl_issue as i')
                ->select('p.name as product_name',DB::raw("sum(id.issued_qty) as issued_qty"),'p.uom','p.sku','i.date')
                ->leftjoin('tbl_issue_detail as id','i.id','=','issue_id')
                ->leftjoin('tbl_product as p','p.id','=','id.product_id')
                ->where('i.is_active','=',1)
                ->groupby('p.id');
        
        if(!empty($fromDate)){
            $builder->whereDate('i.date','>=',$fromDate);
        }
        if(!empty($toDate)){
            $builder->whereDate('i.date','<=',$toDate);
        }
        if(!empty($product_id)){
            $builder->where('p.id','=',$product_id);
        }
        
         $resVal['list'] = $builder->get();
         
         return $resVal;
    }

}
