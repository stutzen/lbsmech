<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmployeeProductionInput
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeProductionInput extends Model {
        protected $table = 'tbl_emp_production_input' ;
    protected $fillable = ['emp_production_id','ref_id','ref_name','name','task_id','task_name','input_pdt_id','input_pdt_name','input_pdt_uom_id'
        ,'input_pdt_uom','input_qty','updated_by','created_by','is_active','comments','config_input_id'];
    protected $dates = ['created_at', 'updated_at'];
}
