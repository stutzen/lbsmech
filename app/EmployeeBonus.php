<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeBonus extends Model {
    
    protected $table = 'tbl_employee_bonus' ;
    protected $fillable = ['emp_id','emp_code','emp_name','emp_team_id','from_date','to_date','present_count','absent_count','bonus_percentage','bonus_amount','remark'
        ,'status','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

