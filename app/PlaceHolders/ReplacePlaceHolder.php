<?php
namespace App\PlaceHolders;
use App\Helper\ReplaceHolderHelper;

class ReplacePlaceHolder {

    public function placeHolderFun($no, $check_print_pdf, $debug, $from_date, $to_date) {
        
        $replace_array = array();
        $thous_sep = ReplaceHolderHelper::getThosandSeperatorFormat();
        $replace_array = $this->getPlaceHolderValue($no, $check_print_pdf, $thous_sep,$from_date,$to_date);
        $design_msg = $this->getDesign($check_print_pdf);
        $replace_msg = $this->replacePlaceHolderValue($replace_array, $design_msg);
        if (!empty($debug)) {
            $replace_msg = $replace_msg . " " . htmlentities($replace_msg);
            echo "<pre>";
            print_r($replace_array);
            echo "</pre>";
        }
        return $replace_msg;
    }
}

?>