<?php

namespace App\PlaceHolders;

use DB;
use App\Helper\ReplaceHolderHelper;

class WorkOrderExternalPrint extends ReplacePlaceHolder {

    function getPlaceHolderValue($mo_id, $con, $check_print_pdf, $thous_sep) {
        $replace_array = array();

        $companyAddress = ReplaceHolderHelper::getAddress();
        $title = ReplaceHolderHelper::getTitle();
        $configureDateFormat = ReplaceHolderHelper::getDateFormat();
        $thous_sep = ReplaceHolderHelper::getThosandSeperatorFormat();

        $builder = DB::table('tbl_manufacturing_order as m')
                ->leftjoin('tbl_work_order as w', 'w.mo_id', '=', 'm.id')
                ->leftjoin('tbl_task_group as t', 't.id', '=', 'm.task_group_id')
                ->leftjoin('tbl_task_group_detail as td', 't.id', '=', 'td.task_group_id')
                ->leftjoin('tbl_work_center as wc', 'wc.id', '=', 'td.work_center_id')
                ->select('m.*', 'p.name as product_name', 'p.sku as product_sku', 'td.amount', 'w.start_date', 'wc.code', 'w.end_date', 'w.start_time', 'w.end_time', 'w.produced_qty', DB::raw("coalesce(w.customer_id,0) as customer_id"))
                ->leftjoin('tbl_product as p', 'p.id', '=', 'm.output_pdt_id')
                ->where('m.id', '=', $mo_id)
                ->where('m.is_active', '=', 1)
                ->get();

        $replace_array['product_detail'] = array();
        $count = 0;
        $customer_id = 0;
        foreach ($builder as $build) {
            $count = $count + 1;
            $product_det['sno'] = $count;
            $product_det['product_name'] = $build->product_name;
            $product_det['product_sku'] = $build->product_sku;
            $product_det['qty'] = $build->produced_qty;
            $product_det['unit_price'] = ReplaceHolderHelper::convertNumberFormat($build->amount, $thous_sep);
            $totalAmount = $build->produced_qty * $build->amount;
            $product_det['row_total'] = ReplaceHolderHelper::convertNumberFormat($totalAmount, $thous_sep);
            $customer_id = $build->customer_id;
            $start_date = $build->start_date . ' ' . $build->start_time;
            $end_date = $build->end_date . ' ' . $build->end_time;
            $sTime = strtotime($start_date);
            $eTime = strtotime($end_date);
            $duration1 = $eTime - $sTime;
            $duration = date('h:i:s ', strtotime($duration1));
            $code = $build->code;
            $date = ReplaceHolderHelper::formatDate($build->date, $configureDateFormat);
            $deadline_date = ReplaceHolderHelper::formatDate($build->deadline_date, $configureDateFormat);

            array_push($replace_array['product_detail'], $product_det);
        }


        $customer = DB::table('tbl_customer')
                ->where('id', '=', $customer_id)
                ->first();
        $cus_name="";
        $address="";
        if ($customer != '') {
            $cus_name = $customer->fname . ' ' . $customer->lname;
            $cus_billing_addr = $customer->billing_address;
            
            $array = explode("\n", $cus_billing_addr);
        $address = "";
        for ($i = 0; $i < count($array); $i++) {
            $address = $address . "<p style=margin-left:100px;>    " . $array[$i] . "</p>";
        }
        }
        

        $replace_array['cus_name'] = $cus_name;
        $replace_array['cus_address'] = $address;

        $issueProduct = DB::table('tbl_manufacturing_order as m')
                ->leftjoin('tbl_work_order_input as w', 'w.mo_id', '=', 'm.id')
                ->select('w.*', 'p.name as product_name', 'p.sku as product_sku', 'p.uom')
                ->leftjoin('tbl_product as p', 'p.id', '=', 'w.input_pdt_id')
                ->where('w.mo_id', '=', $mo_id)
                ->where('w.is_active', '=', 1)
                ->get();

        $replace_array['issued_product_detail'] = array();
        $count = 0;
        foreach ($issueProduct as $issued) {
            $count = $count + 1;
            $issued_product_det['isno'] = $count;
            $issued_product_det['issued_product_name'] = $issued->product_name;
            $issued_product_det['issued_product_sku'] = $issued->product_sku;
            $issued_product_det['issued_product_uom'] = $issued->uom;
            $issued_product_det['issued_qty'] = number_format((float) $issued->reserved_qty, 2, '.', '');
            $issued_product_det['priority'] = '';

            array_push($replace_array['issued_product_detail'], $issued_product_det);
        }

        $replace_array['company_address'] = $companyAddress;
        $replace_array['company_name'] = $title;
        $replace_array['dc_no'] = $mo_id;
        $replace_array['duration'] = $duration;
        $replace_array['value'] = '';
        $replace_array['date'] = $date;
        $replace_array['time'] = '';
        $replace_array['code'] = $code;
        $replace_array['ren_date'] = $deadline_date;
        return $replace_array;
    }

    function getDesign($check_print_pdf) {

        $result = DB::table('tbl_email_templates')
                        ->where('tplname', 'work_order_external_print_template')->first();
        if ($result != null) {
            $msg = $result->message;
        }
        if ($check_print_pdf == 'for_pdf') {
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $msg);
        }
        return $msg;
    }

    function replacePlaceHolderValue($replace_array, $msg) {
        $count = count($replace_array['product_detail']);
        foreach ($replace_array as $key => $value) {
            if (gettype($replace_array[$key]) == 'array') {
                if ($key == 'product_detail') {
                    $product_det = "";
                    $extract_string = ReplaceHolderHelper::extractString($msg, '<!--received-product-row-start-->', '<!--received-product-row-end-->');
                    foreach ($replace_array[$key] as $prod_key => $prod_value) {
                        $copy_string = $extract_string;
                        foreach ($prod_value as $prod1_key => $prod1_value) {
                            $extract_string = str_replace("{{" . $prod1_key . "}}", $prod1_value, $extract_string);
                        }
                        $product_det .= $extract_string;
                        $extract_string = $copy_string;
                    }

                    $msg = str_replace("<!--received-product-row-start-->$extract_string<!--received-product-row-end-->", $product_det, $msg);
                } else if ($key == 'issued_product_detail') {
                    $issued_product_det = "";
                    $extract_string = ReplaceHolderHelper::extractString($msg, '<!--issued-product-row-start-->', '<!--issued-product-row-end-->');
                    foreach ($replace_array[$key] as $prod_key => $prod_value) {
                        $copy_string = $extract_string;
                        foreach ($prod_value as $prod1_key => $prod1_value) {
                            $extract_string = str_replace("{{" . $prod1_key . "}}", $prod1_value, $extract_string);
                        }
                        $issued_product_det .= $extract_string;
                        $extract_string = $copy_string;
                    }

                    $msg = str_replace("<!--issued-product-row-start-->$extract_string<!--issued-product-row-end-->", $issued_product_det, $msg);
                }
            } else {
                $msg = str_replace("{{" . $key . "}}", $value, $msg);
            }
        }
        return $msg;
    }

}
