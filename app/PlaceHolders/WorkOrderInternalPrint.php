<?php
namespace App\PlaceHolders;
use DB;
use App\Helper\ReplaceHolderHelper;

class WorkOrderInternalPrint extends ReplacePlaceHolder {

    function getPlaceHolderValue($wo_id, $con, $check_print_pdf, $thous_sep) {
        $replace_array = array();
        
          $mo=DB::table('tbl_work_order')
                ->where('id','=',$wo_id)
                ->first();
        $mo_id=$mo->mo_id;
           $configureDateFormat = ReplaceHolderHelper::getDateFormat();
        $lastWoId=DB::table('tbl_work_order')
                ->where('mo_id','=',$mo_id)
                ->max('id');
        
        $firsWoId=DB::table('tbl_work_order')
                ->select('id')
                ->where('mo_id','=',$mo_id)
                ->where('id','<',$wo_id)
                ->orderBy('id','desc')->first();
        
        if(!empty($firsWoId)){
            $iss_wo_id=$firsWoId->id;
        }else{
            $iss_wo_id=$wo_id;
        }
                
        if($wo_id == $lastWoId){
        $builder=DB::table('tbl_work_order as w')
                ->leftjoin('tbl_manufacturing_order as m','w.mo_id','=','m.id')
                ->leftjoin('tbl_task_group_detail as td','td.id','=','w.task_group_detail_id')
                ->leftjoin('tbl_work_center as wc','wc.id','=','td.work_center_id')
                ->select('w.*','p.name as product_name','p.sku as product_sku','td.amount','td.operation','wc.name as work_center_name',DB::raw("coalesce(w.customer_id,0) as customer_id"))
                ->leftjoin('tbl_product as p','p.id','=','m.output_pdt_id')
                ->where('w.id','=',$wo_id)
                ->where('w.is_active','=',1)
                ->get();
        }else{
            $builder=DB::table('tbl_work_order_input as wi')
                ->leftjoin('tbl_manufacturing_order as m','wi.mo_id','=','m.id')
                ->leftjoin('tbl_work_order as w','w.mo_id','=','m.id')
                ->leftjoin('tbl_task_group_detail as td','td.id','=','w.task_group_detail_id')
                ->leftjoin('tbl_work_center as wc','wc.id','=','td.work_center_id')
                ->select('w.*','p.name as product_name','p.sku as product_sku','td.amount','td.operation','w.start_date','wc.name as work_center_name','wc.code','w.end_date','w.start_time','w.end_time','w.produced_qty',DB::raw("coalesce(w.customer_id,0) as customer_id"))
                ->leftjoin('tbl_product as p','p.id','=','wi.input_pdt_id')
                ->where('w.id','=',$wo_id)
                ->where('m.is_active','=',1)
                ->get();
        }
        
      
        
        
        $replace_array['product_detail'] = array();
        $count=0;
        $customer_id=0;
        foreach($builder as $build){
            $count=$count+1;
             $product_det['sno'] = $count;
             $product_det['product_name'] = $build->product_name;
             $product_det['product_sku'] =$build->product_sku;
             $product_det['qty'] =$build->produced_qty;
            
             $product_det['operation']=$build->operation;
             $customer_id=$build->customer_id;
             $date = ReplaceHolderHelper::formatDate($build->created_at, $configureDateFormat);
             $replace_array['date']=$date;
             $replace_array['center_name']=$build->work_center_name;
            
            array_push($replace_array['product_detail'], $product_det);
        }
        
        
        
        $issueProduct=DB::table('tbl_work_order_input as wi')
                ->leftjoin('tbl_manufacturing_order as m','wi.mo_id','=','m.id')
                ->leftjoin('tbl_work_order as w','w.mo_id','=','m.id')
                ->leftjoin('tbl_task_group_detail as td','td.id','=','w.task_group_detail_id')
                ->leftjoin('tbl_work_center as wc','wc.id','=','td.work_center_id')
                ->select('wi.*','p.name as product_name','p.sku as product_sku','td.amount','p.uom','td.operation','w.start_date','wc.name as work_center_name','wc.code','w.end_date','w.start_time','w.end_time','w.produced_qty',DB::raw("coalesce(w.customer_id,0) as customer_id"))
                ->leftjoin('tbl_product as p','p.id','=','wi.input_pdt_id')
                ->where('w.id','=',$iss_wo_id)
                ->where('m.is_active','=',1)
                ->get();
        $replace_array['issued_product_detail'] = array();
        $count=0;
        foreach($issueProduct as $issued){
            $count=$count+1;
             $issued_product_det['isno'] = $count;
             $issued_product_det['issued_product_name'] = $issued->product_name;
             $issued_product_det['issued_product_sku'] =$issued->product_sku;
             $issued_product_det['issued_product_uom'] =$issued->uom;
             $issued_product_det['issued_qty'] =number_format((float) $issued->reserved_qty, 2, '.', '');
             if(!empty($firsWoId))
             $issued_product_det['issued_operation']=$issued->operation;
             else
                 $issued_product_det['issued_operation']='';
             $issued_product_det['priority'] ='';
            
            array_push($replace_array['issued_product_detail'], $issued_product_det);
        }
         $replace_array['dc_no']=$wo_id;
        return $replace_array;
    }

    function getDesign($check_print_pdf) {

       $result = DB::table('tbl_email_templates')
                        ->where('tplname', 'work_order_internal_print_template')->first();
        if ($result != null) {
            $msg = $result->message;
        }
        if ($check_print_pdf == 'for_pdf') {
            $msg = preg_replace("/<!--pdf-hidden-start-->.+?<!--pdf-hidden-end-->/i", "", $msg);
        }
        return $msg;
    }

    function replacePlaceHolderValue($replace_array, $msg) {
       $count = count($replace_array['product_detail']);
        foreach ($replace_array as $key => $value) {
            if (gettype($replace_array[$key]) == 'array') {
                if($key== 'product_detail'){
                $product_det = "";
                $extract_string = ReplaceHolderHelper::extractString($msg, '<!--received-product-row-start-->', '<!--received-product-row-end-->');
                foreach ($replace_array[$key] as $prod_key => $prod_value) {
                    $copy_string = $extract_string;
                    foreach ($prod_value as $prod1_key => $prod1_value) {
                        $extract_string = str_replace("{{" . $prod1_key . "}}", $prod1_value, $extract_string);
                    }
                    $product_det .= $extract_string;
                    $extract_string = $copy_string;
                }
              
                $msg = str_replace("<!--received-product-row-start-->$extract_string<!--received-product-row-end-->", $product_det, $msg);
                }else if($key== 'issued_product_detail'){
                    $issued_product_det = "";
                $extract_string = ReplaceHolderHelper::extractString($msg, '<!--issued-product-row-start-->', '<!--issued-product-row-end-->');
                foreach ($replace_array[$key] as $prod_key => $prod_value) {
                    $copy_string = $extract_string;
                    foreach ($prod_value as $prod1_key => $prod1_value) {
                        $extract_string = str_replace("{{" . $prod1_key . "}}", $prod1_value, $extract_string);
                    }
                    $issued_product_det .= $extract_string;
                    $extract_string = $copy_string;
                }
              
                $msg = str_replace("<!--issued-product-row-start-->$extract_string<!--issued-product-row-end-->", $issued_product_det, $msg);
                }
            } else {
                $msg = str_replace("{{" . $key . "}}", $value, $msg);
            }
        }
        return $msg;
    }

}