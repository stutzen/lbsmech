<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Attributes
 *
 * @author Deepa
 */
class Attribute extends Model {
    protected $table = 'tbl_attributes';
    protected $fillable = ['attributetype_id','attribute_code','attribute_label','input_type','is_required','validation_type'
                            ,'has_editable','default_value','option_value','attributetype_code','sno','category','is_active','created_by','updated_by','reg_pattern','is_show_in_list','is_show_in_invoice','is_permission'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
    
}

?>
