<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeSalaryPreparation extends Model {
       protected $table = 'tbl_emp_salary_preparation' ;
    protected $fillable = ['from_date','to_date','salary_type','salary_month','overall_amount','team_id','account_id','team_count','paid_count','not_paid','total_paid','payroll_type'];
    protected $dates = ['created_at', 'updated_at'];
}
