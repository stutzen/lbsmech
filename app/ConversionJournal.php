<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class ConversionJournal extends Model {
    
    protected $table = 'tbl_conversion_journal' ;
    protected $fillable = ['date','overall_from_qty','overall_to_qty','comments','description','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
