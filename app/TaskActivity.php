<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TaskActivity extends Model {
    
    protected $table = 'tbl_task_activity' ;
    protected $fillable = ['title','task_id','task_name','emp_id','emp_name','type_id','type_name','date','status','priority','config_type','estimated_date','from_time','to_time','', 'comments', 'is_active','created_by','updated_by','created_for','ref_id','ref_name'];
    protected $dates = ['created_at', 'updated_at','date'];

}

?>
