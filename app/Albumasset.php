<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Albumasset extends Model {
    
    protected $table = 'tbl_album_asset' ;
    protected $fillable = ['id', 'album_id','album_code','url','comments','seq_no','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
?>
