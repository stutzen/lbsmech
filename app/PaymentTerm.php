<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserRole
 *
 * @author Deepa
 */
class PaymentTerm extends Model {
    //put your code here
    protected  $table ='tbl_payment_term';
    protected  $fillable = ['name','comments','is_active','is_default','created_by','updated_by'];
    protected $dates = ['updated_at','created_at'];
}

?>
