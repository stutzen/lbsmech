<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Attendance
 *
 * @author Stutzen Admin
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
class Attendance extends Model {

    protected $table = 'tbl_attendance';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'start_time', 'end_time', 'start_location', 'end_location', 'start_geo_text', 'end_geo_text','start_img_url','end_img_url',
        'duration',  'created_by', 'updated_by', 'is_active','date','ref_id','ref_type','emp_id'];
    protected $dates = [
        'created_at', 'updated_at' ];


}
