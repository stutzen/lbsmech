<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class DealFollowers extends Model {
    
    protected $table = 'tbl_deal_followers' ;
    protected $fillable = ['deal_id','emp_id' , 'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
