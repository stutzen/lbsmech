<?php
namespace App;
use Illuminate\Database\Eloquent\MOdel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Purchaseinvoice
 *
 * @author Deepa
 */
class Purchaseinvoice extends Model {
    //put your code here
    protected $table ='tbl_purchase_invoice';
    protected $fillable = [ 'customer_id','prefix','purchase_no','purchase_code','date','duedate','datepaid','subtotal','discount_mode','discount_amount','discount_percentage','credit_amount','type'
        ,'custom_attribute_json','tax_amount','tax_id','total_amount','round_off','status','paymentmethod','notes','is_active','purchase_quote_id','customer_address','created_at','created_by','updated_at','updated_at','reference_no','type','ref_id'];
    protected $dates = [
        'created_at',
        'updated_at',
        'date',
        'duedate'
    ];
}

?>
