<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeTaskType extends Model {
    
    protected $table = 'tbl_emp_task_type' ;
    protected $fillable = ['name','description','comments','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
   
}