<?php


namespace App;
use Illuminate\Database\Eloquent\Model;
class Comments extends Model {
    
    protected $table = 'tbl_comments' ;
    protected $fillable = ['emp_id','emp_code','emp_name','emp_task_activity_id','description','comments','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
   
}