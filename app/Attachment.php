<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Attachement
 *
 * @author Deepa
 */
class Attachment extends Model {
    //put your code here
    protected $table = 'tbl_attachment';
    //public $primaryKey  = 'empcode';
    protected $fillable = [
        'type',
        'ref_id',
        'is_active',
        'url',
        'comments',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
    protected $dates = [ 'updated_at', 'created_at'];
}

?>
