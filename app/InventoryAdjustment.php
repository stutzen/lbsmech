<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Item
 *
 * @author Deepa
 */
class InventoryAdjustment extends Model {
     protected $table = 'tbl_inventory_adjustment';
     protected $fillable = ['product_id', 'uom_id','uom_name','sku','quantity_debit','quantity_credit',
         'type','comments','ref_id','total_stock','is_active','created_by','updated_by','created_by_name','updated_by_name'];
     protected $dates = ['created_at','updated_at'];
    //put your code here
}

?>
