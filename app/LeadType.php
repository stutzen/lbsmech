<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class LeadType extends Model {
    
    protected $table = 'tbl_lead_master' ;
    protected $fillable = ['name','comments','type'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>