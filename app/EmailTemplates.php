<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailTemplates
 *
 * @author Deepa
 */
class EmailTemplates extends Model {
    //put your code here
    protected $table = 'tbl_email_templates';

    protected  $fillable = ['tplname','subject','message','is_active','created_at','updated_at','created_by','updated_by','label'];
    protected $dates = ['created_at','updated_at'];
}

?>
