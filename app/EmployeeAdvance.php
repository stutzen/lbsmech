<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeAdvance extends Model {
    
    protected $table = 'tbl_employee_advance' ;
    protected $fillable = ['emp_id','emp_code','date','resource','adv_amount','deduction_amt','total_deduction_amt','balance_amt','remark'
        ,'is_active','is_user','created_by','updated_by','account_id'];
    protected $dates = ['created_at', 'updated_at','date'];
    //put your code here
}

