<?php

namespace App\Listeners;

use App\Events\InvoiceNotificationEvent;
use App\Events\Event;
use App\Events\PurchaseNotificationEvent;
use App\Events\PurchasePaymentNotificationEvent;
use App\Events\InvoicePaymentNotificationEvent;
use App\Events\DealNotificationEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Invoice;
use App\Helper\NotificationHelper;
use Illuminate\Database\Eloquent\Model;
use DB;

class NotificationListener {

    protected $invoice;
    protected $purchaseinvoice;
    protected $purchasepayment;
    protected $payment;
    protected $deal;

    /**
     * Create the event listener.
     *
     * @return void 
     */
    public function __construct() {
        
    }

    /**
     * Handle the event.
     *
     * @param  PodcastWasPurchased  $event
     * @return void
     */
    public function handle(Event $event) {
        if ($event instanceof InvoiceNotificationEvent) {
//            $user = DB::table('tbl_user')->select('id')->whereRaw(DB::raw("rolename = 'superadmin' or rolename = 'admin'"))->get();

            $id = $event->invoice->id;
            $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "invoice_notification_send")
                    ->first();

            $invoice = DB::table('tbl_invoice as i')
                            ->leftjoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                            ->select('i.customer_id', 'i.total_amount', DB::raw("concat(c.fname,'',c.lname) as customer_name"))
                            ->where('i.id', '=', $id)->first();

            $cust_name = $invoice->customer_name;
            $amount = $invoice->total_amount;

            $content = $get_template->message;
            $new_msg = str_replace("{{invoiceid}}", $id, $content);
            $new_msg = str_replace("{{customer_name}}", $cust_name, $new_msg);
            $new_msg = str_replace("{{invoice_amount}}", $amount, $new_msg);
            $user_id = '';
//            foreach ($user as $admin) {
//                if (!empty($admin)) {
//                    $user_id = $admin->id;
//                    $response = $this->sendNotification($user_id, '', $new_msg);
//                }
//            }
//            if (empty($user_id)) {
                $response = $this->sendNotification('', '', $new_msg);
//            }
            return $response;
        } elseif ($event instanceof PurchaseNotificationEvent) {
//            $user = DB::table('tbl_user')->select('id')->whereRaw(DB::raw("rolename = 'superadmin' or rolename = 'admin'"))->get();

            $id = $event->purchaseinvoice->id;

            $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "purchase_notification_send")
                    ->first();

            $invoice = DB::table('tbl_purchase_invoice as i')
                            ->leftjoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                            ->select('i.customer_id', 'i.total_amount', DB::raw("concat(c.fname,'',c.lname) as customer_name"))
                            ->where('i.id', '=', $id)->first();

            $cust_name = $invoice->customer_name;
            $amount = $invoice->total_amount;

            $content = $get_template->message;
            $new_msg = str_replace("{{invoiceid}}", $id, $content);
            $new_msg = str_replace("{{customer_name}}", $cust_name, $new_msg);
            $new_msg = str_replace("{{invoice_amount}}", $amount, $new_msg);
            $user_id = '';
//            foreach ($user as $admin) {
//                if (!empty($admin)) {
//                    $user_id = $admin->id;
//                    $response = $this->sendNotification($user_id, '', $new_msg);
//                }
//            }
//            if (empty($user_id)) {
                $response = $this->sendNotification('', '', $new_msg);
//            }
            return $response;
        } elseif ($event instanceof PurchasePaymentNotificationEvent) {
//            $user = DB::table('tbl_user')->select('id')->whereRaw(DB::raw("rolename = 'superadmin' or rolename = 'admin'"))->get();

            $id = $event->purchasepayment->id;
            $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "purchase_payment_notification_send")
                    ->first();

            $invoice = DB::table('tbl_purchase_payment as p')
                            ->leftjoin('tbl_purchase_invoice as i', 'p.purchase_invoice_id', '=', 'i.id')
                            ->leftjoin('tbl_customer as c', 'i.customer_id', '=', 'c.id')
                            ->select('i.customer_id', 'i.total_amount', DB::raw("concat(c.fname,'',c.lname) as customer_name"), 'i.paid_amount', DB::raw("(i.total_amount-i.paid_amount) as balance_amount"))
                            ->where('p.id', '=', $id)->first();

            $cust_name = $invoice->customer_name;
            $amount = $invoice->total_amount;
            $paid_amount = $invoice->paid_amount;
            $balance_amount = $invoice->balance_amount;

            $content = $get_template->message;
            $new_msg = str_replace("{{invoiceid}}", $id, $content);
            $new_msg = str_replace("{{customer_name}}", $cust_name, $new_msg);
            $new_msg = str_replace("{{paid_amount}}", $paid_amount, $new_msg);
            $new_msg = str_replace("{{balance_amount}}", $balance_amount, $new_msg);
            $user_id = '';
//            foreach ($user as $admin) {
//                if (!empty($admin)) {
//                    $user_id = $admin->id;
//                    $response = $this->sendNotification($user_id, '', $new_msg);
//                }
//            }
//            if (empty($user_id)) {
                $response = $this->sendNotification('', '', $new_msg);
//            }

            return $response;
        } elseif ($event instanceof InvoicePaymentNotificationEvent) {
//            $user = DB::table('tbl_user')->select('id')->whereRaw(DB::raw("rolename = 'superadmin' or rolename = 'admin'"))->get();

            $id = $event->payment->id;
            $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "invoice_payment_notification_send")
                    ->first();

            $invoice = DB::table('tbl_payment as p')
                            ->leftjoin('tbl_invoice as i', 'p.invoice_id', '=', 'i.id')
                            ->leftjoin('tbl_customer as c', 'p.customer_id', '=', 'c.id')
                            ->select('i.customer_id', 'i.total_amount', DB::raw("concat(c.fname,'',c.lname) as customer_name"), 'i.paid_amount', DB::raw("(i.total_amount-i.paid_amount) as balance_amount"))
                            ->where('p.id', '=', $id)->first();


            $cust_name = $invoice->customer_name;
            $amount = $invoice->total_amount;
            $paid_amount = $invoice->paid_amount;
            $balance_amount = $invoice->balance_amount;

            $content = $get_template->message;
            $new_msg = str_replace("{{invoiceid}}", $id, $content);
            $new_msg = str_replace("{{customer_name}}", $cust_name, $new_msg);
            $new_msg = str_replace("{{paid_amount}}", $paid_amount, $new_msg);
            $new_msg = str_replace("{{balance_amount}}", $balance_amount, $new_msg);
            $user_id = '';
//            foreach ($user as $admin) {
//                if (!empty($admin)) {
//                    $user_id = $admin->id;
//                    $response = $this->sendNotification($user_id, '', $new_msg);
//                }
//            }
//            if (empty($user_id)) {
                $response = $this->sendNotification($user_id, '', $new_msg);
//            }

            return $response;
        } elseif ($event instanceof DealNotificationEvent) {
//            $user = DB::table('tbl_user')->select('id')->whereRaw(DB::raw("rolename = 'superadmin' or rolename = 'admin'"))->get();
            $id = $event->deal->id;
            $get_template = DB::table("tbl_email_templates")
                    ->where('is_active', '=', 1)
                    ->where('tplname', '=', "deal_notification_send")
                    ->first();

            $deal = DB::table('tbl_deal as d')
                            ->leftjoin('tbl_contact as c', 'd.contact_id', '=', 'c.id')
                            ->leftjoin('tbl_employee as e', 'd.emp_id', '=', 'e.id')
                            ->select('d.amount', 'e.user_id', DB::raw("concat(c.fname,'',c.lname) as contact_name"), DB::raw("concat(e.f_name,'',e.l_name) as employee_name"))
                            ->where('d.id', '=', $id)->first();

            $con_name = $deal->contact_name;
            $emp_name = $deal->employee_name;
            $deal_amount = $deal->amount;

            $content = $get_template->message;
            $new_msg = str_replace("{{dealid}}", $id, $content);
            $new_msg = str_replace("{{customer_name}}", $con_name, $new_msg);
            $new_msg = str_replace("{{emp_name}}", $emp_name, $new_msg);
            $new_msg = str_replace("{{deal_amount}}", $deal_amount, $new_msg);
            $user_id = '';
//            $value = array($deal->user_id);

//            foreach ($user as $admin) {
//                array_push($value, $admin->id);
//            }
//            foreach ($value as $val) {
//                if (!empty($val)) {
                    $response = $this->sendNotification($deal->user_id, '', $new_msg);
//                }
//            }
            return $response;
        }

        // Access the podcast using $event->podcast...
    }

    public function sendNotification($user_id, $device_id, $content) {
        $content = array("en" => $content
        );
         $user = DB::table('tbl_user')->select('id')->whereRaw(DB::raw("rolename = 'superadmin' or rolename = 'admin'"))->get();
         $user_val = array();
         if(!empty($user_id)){
                array_push($user_val,$user_id);
            }
         
            foreach ($user as $admin) {
                if (!empty($user)) {
                    $use_id = $admin->id;
                    array_push($user_val,$use_id);
                    
                }
            }
            
  if(!empty($user_val)){

        $builder = DB::table('tbl_device_detail')
                ->select('device_id')
                ->where('is_active', '=', 1)
        ;

        if (!empty($device_id)) {
            $builder->where('device_id', '=', $device_id);
        }
        if (!empty($user_id)) {
            $builder->where('user_id', '=', $user_id);
        }


        $collection = $builder->get();
        $player = '';
        foreach ($collection as $collect) {
            if (strlen($player) == 0) {
                $player = $collect->device_id;
            } else {
                $player = $player . ',' . $collect->device_id;
            }
           
        }
         if(strlen($player)>0){
         $dat = explode(',', $player);
        $fields = array(
            'app_id' => "87dfb5a3-1a2c-4511-9edc-67378dbc7099",
            'include_player_ids' => $dat,
            'data' => array("foo" => "bar"),
            'contents' => $content
        );

        $fields = json_encode($fields);
        //print("\nJSON sent:\n");
        // print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic N2MwMTA2YjUtODdjMi00OGM0LWE2ZjktNTJiOWI5YzQxODMx'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
         } else{
            return "";
        }
  }else{
        return "";
  }
    }

}
