<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Item
 *
 * @author Deepa
 */
class Inventory extends Model {
     protected $table = 'tbl_inventory';
     protected $fillable = ['product_id', 'uom_id','uom_name','sku','quantity',
         'is_active','created_by','updated_by','created_by_name','updated_by_name'];
     protected $dates = ['created_at','updated_at'];
    //put your code here
}

?>
