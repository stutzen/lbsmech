<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class BomByProduct extends Model{
    protected $table = 'tbl_bom_by_product' ;
    protected $fillable = ['bom_id','product_id','qty','uom_id','updated_by','created_by',
        'is_active','comments'];
    protected $dates = ['created_at', 'updated_at'];
}
