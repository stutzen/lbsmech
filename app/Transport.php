<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transport
 *
 * @author Stutzen Admin
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
class Transport extends Model {

    protected $table = 'tbl_transport';

   
    protected $fillable = [
        'name', 'address', 'land_mark', 'email', 'mobile_no', 'landline_no', 'city_id','city_name',
        'state_id', 'state_name', 'country_id', 'country_name', 'postcode', 'created_by', 'updated_by', 'is_active', 'comments'
    ];
    protected $dates = ['created_at', 'updated_at'];

}
