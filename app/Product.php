<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Item
 *
 * @author Deepa
 */
class Product extends Model {
     protected $table = 'tbl_product';
     protected $fillable = ['hsn_code','name', 'description','uom','uom_id','sales_price','category_id',
         'item_number','type','has_inventory','min_stock_qty','sku','available','is_active','is_sale','is_purchase','created_by','updated_by','mrp_price'
         ,'packaging_uom_id','packaging_uom_name','packaging_count'];
     protected $dates = ['created_at','updated_at'];
    //put your code here
}

?>
