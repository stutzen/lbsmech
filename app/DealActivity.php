<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActivityLog
 *
 * @author Deepa
 */
class DealActivity extends Model {
    //put your code here
    protected  $table = 'tbl_deal_activity';
    protected $fillable = ['type','description','log_activity_type','deal_id','contact_id','date','time','created_by','created_at','updated_by',
        'updated_at','created_by_name','updated_by_name','is_active','emp_id','emp_name','log_activity_sub_type','comments','ref_id',
        'latitude','longitude','geo_point','ip_address','time','longitude','latitude','user_id','user_name'];
    protected $dates = ['created_at','updated_at','date'];
}

?>
