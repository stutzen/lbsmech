<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Creditnote
 *
 * @author Deepa
 */
class Creditnote extends Model {
protected $table = 'tbl_credit_note';
protected  $fillable = ['invoice_id','is_active','date','amount','comments','created_by','updated_by'];
protected $dates = ['created_at','updated_at','date'];
    //put your code here
}

?>
