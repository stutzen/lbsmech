<?php
namespace app;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActivityLog
 *
 * @author Deepa
 */
class ActivityLog extends Model {
    //put your code here
    protected  $table = 'tbl_activity_log';
    protected $fillable = ['date','type','amount','ref_id','description','userid','ip','is_active','created_by','created_at','updated_by','updated_at'];
    protected $dates = ['created_at','updated_at','date'];
}

?>
