<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class IssueDetail extends Model{
    protected $table='tbl_issue_detail';
    protected $fillable=['product_id','issue_id','product_name','consumed_qty','issued_qty','balance_qty','status','work_order_input_id','is_active','created_by','updated_by','comments'];
    protected $dates=['created_at','updated_at'];
    
}
