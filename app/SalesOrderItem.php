<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Quotesitems
 *
 * @author Deepa
 */
class SalesOrderItem extends Model {
    
    //put your code here
    protected $table = 'tbl_sales_order_item';
    protected $fillable = ['sales_order_id','customer_id','product_id','product_name','product_sku','qty','unit_price',
                           'tax_amount','total_price', 'created_at','gross_price','duedate','paymentmethod','notes',
                            'mrp_price','uom_id','uom_name','is_active','created_by','updated_at','updated_by','custom_opt1'
                            ,'custom_opt2','custom_opt3','custom_opt4','custom_opt5','hsn_code'];
    protected $dates = ['created_at',
        'updated_at'];
                            
}

?>
