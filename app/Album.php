<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model {

    protected $table = 'tbl_album';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'type', 'comments',  
        'created_by', 'updated_by', 'is_active'   
    ];
    protected $dates = [
        'created_at',
        'updated_at'
        ];

//    /**
//     * The attributes excluded from the model's JSON form.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        
//    ];
}
