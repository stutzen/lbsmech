<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tasks
 *
 * @author Deepa
 */
class Templates extends Model {
    //put your code here
    protected  $table = 'tbl_email_templates';
    protected $fillable = ['id','tplname','subject','message','is_active'];
}

?>
