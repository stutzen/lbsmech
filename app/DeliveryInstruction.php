<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeliveryInstruction
 *
 * @author Stutzen Admin
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
class DeliveryInstruction extends Model {

    protected $table = 'tbl_delivery_instruction';

   
    protected $fillable = [
        'transport_id', 'transport_name', 'driver_name', 'driver_mobile_no', 'vehicle_type', 'vehicle_no','status','type','final_destination',
        'shipment_date','delivery_date','freight','driver_beta','net_weight','gross_weight','volume', 'no_of_pkts','qty', 'unloading', 'drop_seq',
        'billing_unit','store', 'created_by', 'updated_by', 'is_active', 'comments'
    ];
    protected $dates = ['created_at', 'updated_at'];

}