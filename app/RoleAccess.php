<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserScreenAccess
 *
 * @author Deepa
 */
class RoleAccess extends Model {
    
    //put your code here
    protected $table = 'tbl_role_access';
    protected  $fillable = ['role_id','screen_id','screen_code','is_create','is_modify','is_view','is_delete',
                            'is_active','created_by','updated_by','screen_name','is_visible_to'];
    protected $dates = ['created_at','updated_at'];
}

?>
