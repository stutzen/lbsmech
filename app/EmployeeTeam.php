<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SysCategory
 *
 * @author Deepa
 */
class EmployeeTeam extends Model {
    //put your code here
    protected $table = 'tbl_emp_team' ;
     protected $fillable = ['name','emp_id','emp_name', 'count', 'current_count','is_active','created_by','updated_by','created_by_name','updated_by_name'];
     protected $dates = ['created_at', 'updated_at'];
}

?>
