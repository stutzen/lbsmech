<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class ManufacturingOrder extends Model {
    
    protected $table = 'tbl_manufacturing_order' ;
    protected $fillable = ['customer_id','deadline_date','output_pdt_id','bom_id','produced_qty','user_id','task_group_id'
        ,'work_type','status','date','qty','is_active','created_by','updated_by','ref_id','is_root','root_id','wo_status'
        ,'failed_qty','passed_qty','qc_status'];
    protected $dates = ['created_at', 'updated_at'];
   
}