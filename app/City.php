<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class City extends Model {
    
    protected $table = 'tbl_city' ;
    protected $fillable = ['name','code','state_id','state_name','country_id','country_name',  'created_by','updated_by','dual_language'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>
