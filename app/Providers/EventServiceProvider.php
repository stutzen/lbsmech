<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{ 
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
         
        'App\Events\InvoiceNotificationEvent' => [
            'App\Listeners\NotificationListener',
        ],
         'App\Events\PurchaseNotificationEvent' => [
            'App\Listeners\NotificationListener',
        ],
        'App\Events\PurchasePaymentNotificationEvent' => [
            'App\Listeners\NotificationListener',
        ],
         'App\Events\InvoicePaymentNotificationEvent' => [
            'App\Listeners\NotificationListener',
        ],
         'App\Events\DealNotificationEvent' => [
            'App\Listeners\NotificationListener',
        ],
       
       
    ];
}
