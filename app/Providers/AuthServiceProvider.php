<?php

namespace App\Providers;

use App\User;
use App\PersistentLogin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
//
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot() {
// Here you may define how you wish users to be authenticated for your Lumen
// application. The callback which receives the incoming request instance
// should return either a User instance or null. You're free to obtain
// the User instance via an API token or any other method necessary.

        Auth::viaRequest('api', function ($request) {
            $api_token = $request->header('api-token');
            $splitapitoken = explode(':',$api_token);
            if ($request->header('api-token')) {
               $persistentLoginCollection = PersistentLogin::where('token', '=',$splitapitoken[0])
                                        ->where('series','=',$splitapitoken[1])->get();
                if (!$persistentLoginCollection->isEmpty()) {
                    $firstUser = $persistentLoginCollection->first();
                    return User::where('username', $firstUser->username)->where('is_active','=',1)->first();
                }
                return null;
            }
        });
    }

}
