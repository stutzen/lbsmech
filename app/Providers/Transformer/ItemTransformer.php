<?php
namespace App\Transformer;

use App\Item;
use League\Fractal\TransformerAbstract;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemTransformer
 *
 * @author Deepa
 */
class ItemTransformer  extends TransformerAbstract {
    //put your code here

    public function transform(Item $item){
       return[
          'id'=>$item->id,
           'name'=>$item->name,
           'createdBy'=>$item->created_by,
           'updatedBy'=>$item->updated_by,
          
           'uomId'=>$item->uom_id,
           'Uno'=>$item->uom,
           'Salesprice'=>$item->sales_price,
           'ItemNumber'=>$item->item_number,
           'Description'=>$item->description,
           'Type'=>$item->type,
           'isactive'=>$item->isactive,
           'Trackinventroy'=>$item->track_inventroy,
           'available'=>$item->Available,
       ];
       
   }
}

?>
