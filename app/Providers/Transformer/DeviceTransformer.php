<?php

namespace App\Transformer;

use App\Device;
use League\Fractal\TransformerAbstract;

class DeviceTransformer extends TransformerAbstract{
    
   public function transform(Device $device) {
        return [
        'id' => $device->id,
        'deviceCode' => $device->device_code,
        'name' => $device->name,
        'shortCode' => $device->short_code,
        'createdBy'=>$device->created_by,
        'updatedBy'=>$device->modified_by,
        //'createdOn'=>$device->created_at->toIso8601String(),
        //'lastmodifiedon'=>$device->updated_at->toIso8601String(),
        'isActive'=>$device->is_active,
        'comments'=>$device->comments,
        ];
    }
}
