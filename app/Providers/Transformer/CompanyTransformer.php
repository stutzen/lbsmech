<?php

namespace App\Transformer;

use App\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract {

    
    public function transform(Company $company) {
        return [
        'id' => $company->id,
        'name' => $company->name,
        'business_type' => $company->business_type,
        'email' => $company->email,
        'ph_no' => $company->ph_no,
        'address' => $company->address,
        'city' => $company->city,
        'country' => $company->country,
        'state' => $company->state,
        'post_code' => $company->post_code,
        'created_by' => $company->created_by,
        'updated_by' => $company->updated_by,
        'created_at' => strtotime($company->created_at->toIso8601String()),
        'updated_at' => strtotime($company->updated_at->toIso8601String()),
        'isactive' => $company->isactive,
        'f_name' => $company->f_name,
            'catId'=>$company->id,
        ];
    }

}
