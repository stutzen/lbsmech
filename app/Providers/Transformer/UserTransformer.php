<?php

namespace App\Transformer;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {

    /**
     * Transform a Book model into an array
     *
     * @param Book $book
     * @return array
     */
    public function transform(User $user) {

        return [

            'userid' => $user->id,
            'username' => $user->username,
            'createdBy' => $user->created_by,
            'updatedBy' => $user->updated_by,
            'createdOn'=>$user->created_at->toIso8601String(),
            'lastmodifiedon'=>$user->updated_at->toIso8601String(),
            'firstName' => $user->f_name, 
            'lastName' => $user->l_name, 
            'gender' => $user->id, 
            'dob' => $user->gender, 
            'email' => $user->email, 
            'phoneNumber' => $user->ph_no, 
            'city' => $user->city, 
            'state' => $user->state, 
            'country' => $user->country, 
            'address' => $user->address, 
            'postcode' => $user->post_code, 
            'userType' => $user->user_type, 
            'isactive' => $user->isactive, 
            'companyId' => $user->comp_id, 
            'imagepath' => $user->imagepath, 
            'device_id' => $user->device_id,
            'rolename' => $user->rolename,
        ];
       
    }

}
