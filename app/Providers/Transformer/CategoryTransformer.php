<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryTransformer
 *
 * @author Stutzen Admin
 */

namespace App\Transformer;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract{
    
 
    
    public function transform(Category $category) {
        return [
        'id' => $category->id,
        'name' => $category->name,
        'createdBy'=>$category->created_by,
        'updatedBy'=>$category->updated_by,
        'createdOn'=>$category->created_at->toIso8601String(),
        'lastmodifiedon'=>$category->updated_at->toIso8601String(),
        'isActive'=>$category->is_active,
        'companyId'=>$category->comp_id,
        'comments'=>$category->comments,
        'description'=>$category->description,
        ];
    }
}
