<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class BomDetail extends Model {
    
    protected $table = 'tbl_bom_detail' ;
    protected $fillable = ['name','bom_id','name','input_pdt_id','input_pdt_name','input_pdt_uom','updated_by','created_by',
        'input_qty','is_active','comments','input_pdt_uom_id','task_group_detail_id'];
    protected $dates = ['created_at', 'updated_at'];

}
