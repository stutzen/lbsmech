<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class AllowanceMapping extends Model {
    
    protected $table = 'tbl_allowance_mapping' ;
    protected $fillable = ['role_id','amount','is_active','created_by','updated_by','comments','type','allowance_id'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
