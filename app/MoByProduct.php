<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class MoByProduct extends Model{
    protected $table = 'tbl_mo_by_product' ;
    protected $fillable = ['mo_id','pro_id','qty','produced_qty','uom_id','updated_by','created_by',
        'is_active','comments'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
