<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Purchaseinvoiceitem
 *
 * @author Deepa
 */
class Purchaseinvoiceitem extends Model  {
    //put your code here
     protected  $table ='tbl_purchase_invoice_item';
    protected  $fillable =['invoice_id','customer_id','product_id','product_name','product_sku','qty','unit_price',
                           'tax_amount','total_price', 'created_at','gross_price','duedate','paymentmethod','notes','internal_product_id',
                            'mrp_price','uom_id','uom_name','is_active','created_by','updated_at','updated_by','custom_opt1'
                            ,'custom_opt2','custom_opt3','custom_opt4','custom_opt5','passed_qty','failed_qty','qc_status'];
    protected $dates = ['updated_at','created_at','duedate'];
}

?>
