<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeSalaryTaskDetail extends Model {
    
    protected $table = 'tbl_emp_salary_task_detail' ;
    protected $fillable = ['emp_salary_id','task_config_id','productName','unit_price','outputQty'
        ,'is_active','created_by','updated_by','comments'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

