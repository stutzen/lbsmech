<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transaction
 *
 * @author Deepa
 */
class Transaction extends Model {
    //put your code here
     protected $table = 'tbl_transaction' ;
     protected $fillable = ['name', 'fiscal_year_id', 'fiscal_year','transaction_date','voucher_type','voucher_number','credit'
         ,'debit','acode','acoderef','account','pmtcode','pmtcoderef','is_cash_flow','particulars','created_by','updated_by','account_category','created_by_name','updated_by_name'
         ,'fiscal_year_code','payment_mode','fiscal_month_code','is_active'];
     protected $dates = ['created_at', 'updated_at'];
     
    public static $SALES = 'sales_invoice';
    public static $SALES_PAYMENT = 'sales_invoice_payment';
    public static $PURCHASE = 'purchase_invoice';
    public static $PURCHASE_PAYMENT = 'purchase_invoice_payment';
    
    public static $DIRECT_INCOME = 'direct_income';
    public static $DIRECT_EXPENSE = 'direct_expense';
  
    public static $SALARY_AMOUNT='salary_amount';
     public static $BONUS_AMOUNT='bonus_amount';
     public static $BONUS_DELETE='bonus_delete';
       public static $ADVANCE_AMOUNT='advance_amount';
     
}


    ?>
