<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeAllowance extends Model {
    
    protected $table = 'tbl_emp_allowance' ;
    protected $fillable = ['emp_id','emp_allow_mas_id','emp_code','date','amount','type'
        ,'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at','date'];
    //put your code here
}

