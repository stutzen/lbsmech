<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeBonusPreparation extends Model {
       protected $table = 'tbl_emp_bonus_preparation' ;
    protected $fillable = ['from_date','to_date','overall_bonus_amount','team_id','account_id'];
    protected $dates = ['created_at', 'updated_at'];
}

