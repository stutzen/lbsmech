<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class ServiceRequest extends Model {
    
    protected $table = 'tbl_service_request' ;
    protected $fillable = ['customer_id','date','machine_status','product_id','model','assigned_emp_id','service_charge','spare_charge'
        ,'total_charge','status','problem','solution','comments','customer_remark','serial_no','invoice_item_id'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>