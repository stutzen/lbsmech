<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class WorkOrderInput extends Model {
    
    protected $table = 'tbl_work_order_input' ;
    protected $fillable = ['mo_id','task_group_detail_id','bom_id','bom_detail_id','name','input_pdt_id','input_pdt_name','input_pdt_uom','updated_by','created_by',
        'input_qty','is_active','comments','input_pdt_uom_id','to_consume','consumed','reserved_qty','status'];
    protected $dates = ['created_at', 'updated_at'];

}
