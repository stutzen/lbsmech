<?php
namespace app;
use Illuminate\Database\Eloquent\Model;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoice
 *
 * @author Deepa
 */
class Invoice extends Model{
    //put your code here
    protected $table ='tbl_invoice';
    protected $fillable = ['invoice_no','invoice_code', 'customer_id','prefix','date','duedate','datepaid','subtotal','discount_mode','discount_amount','discount_percentage','credit_amount','type'
        ,'tax_amount','tax_id','total_amount','status','paymentmethod','notes','is_active','quote_id','sales_order_id','customer_address','round_off','created_at','created_by','updated_at','updated_at','custom_attribute_json','deal_id'
        ,'deal_name','payee','consignee','insurance_charge','packing_charge','insurance_percentage','packing_percentage','company_id','ref_id','ref_name'];
    protected $dates = [
        'created_at',
        'updated_at',
        'date',
        'duedate'
    ];
}

?>
