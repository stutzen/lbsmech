<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Uom extends Model {
    
    protected $table = 'tbl_uom' ;
    protected $fillable = ['name', 'description', 'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>
