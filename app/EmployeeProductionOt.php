<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmployeeProductionDetail
 *
 * @author Stutzen Admin
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeProductionOt extends Model {
       protected $table = 'tbl_emp_production_ot' ;
    protected $fillable = ['allowance_master_id','emp_production_id','emp_id','emp_code','amount'
        ,'updated_by','created_by','is_active','comments'];
    protected $dates = ['created_at', 'updated_at'];
}
