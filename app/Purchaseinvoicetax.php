<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tax
 *
 * @author Deepa
 */
class Purchaseinvoicetax extends Model {

    //put your code here
    protected $table = 'tbl_purchase_invoice_tax';
    protected $fillable = ['invoice_id', 'tax_id', 'tax_name', 'tax_value', 'tax_percentage',
        'product_amt', 'applicable_amt', 'taxable_amount', 'tax_amount', 'is_active'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
