<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Grn extends Model {

    protected $table = 'tbl_grn';
    protected $fillable = ['customer_id', 'over_all_qty', 'over_all_amt', 'is_active', 'created_by', 'updated_by'
        , 'date', 'product_id', 'comments','status'];
    protected $dates = ['created_at', 'updated_at'];

}
