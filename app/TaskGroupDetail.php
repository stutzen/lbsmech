<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class TaskGroupDetail extends Model {
    
    protected $table = 'tbl_task_group_detail' ;
    protected $fillable = ['operation','type','department_id','work_center_id','duration','task_group_id','amount','updated_by','created_by','is_active','comments','sequence_no'];
    protected $dates = ['created_at', 'updated_at'];

}
