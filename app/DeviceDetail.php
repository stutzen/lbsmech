<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class DeviceDetail extends Model {
    
    protected $table = 'tbl_device_detail' ;
    protected $fillable = ['user_id', 'device_id', 'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>
