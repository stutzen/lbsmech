<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class ProductType extends Model{
    protected $table = 'tbl_product_type' ;
    protected $fillable = ['id','name', 'code','is_active','comments','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
}
