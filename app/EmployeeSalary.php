<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeSalary extends Model {
    
    protected $table = 'tbl_emp_salary' ;
    protected $fillable = ['emp_id','emp_team_id','esi_percentage','pf_percentage','week_salary','esi_amount','pf_amount','from_date','to_date'
        ,'deduction_amount','salary_amount','overall_qty','ot_amount','adv_amount','total_amount','is_active','created_by','updated_by','salary_type','salary_month'];
    protected $dates = ['created_at', 'updated_at','date'];
    //put your code here
}

