<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class QcHistory extends Model {
    
    protected $table = 'tbl_qc_history' ;
    protected $fillable = ['title','product_id','description','instruction','ref_id','ref_type','approved_by','date','overall_qty'
        ,'passed_qty','failed_qty','tested_qty','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
