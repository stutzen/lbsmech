<?php
namespace app;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExpensePayment
 *
 * @author Deepa
 */
class ExpensePayment  extends Model{
    //put your code here
    protected $table = 'tbl_expense_payment';
    protected $fillable = ['expense_id','tax_id','amount','expense_name','additional_notes','created_by',
                            'created_at','updated_by','updated_at'];
    protected $dates = ['created_at','updated_at'];
}

?>
