<?php
namespace app;
use Illuminate\Database\Eloquent\Model;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Expense
 *
 * @author Deepa
 */
class Expense extends Model {
    //put your code here
    protected  $table = 'tbl_expense';
    protected $fillable = ['attacement','amount','payment_type','payment_date','customer_id','category_id','tax_id','comments'
                                ,'total_amount','refno','additional_notes','created_by','created_at','updated_by','updated_at','tra_category'];
    protected $dates = ['created_at','updated_at','payment_date'];
}

?>
