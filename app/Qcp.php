<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Qcp extends Model{
    protected $table = 'tbl_qcp' ;
    protected $fillable = ['title','type','responsible_by','product_id','updated_by','created_by',
        'is_active','comments','instructions'];
    protected $dates = ['created_at', 'updated_at'];
}
