<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class AllowanceMaster extends Model {
    
    protected $table = 'tbl_allowance_master' ;
    protected $fillable = ['name','amount','is_active','created_by','updated_by','comments'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
