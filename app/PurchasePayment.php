<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PurchasePayment
 *
 * @author Deepa
 */
class PurchasePayment extends Model {

    //put your code here
    protected $table = 'tbl_purchase_payment';
    protected $fillable = ['created_by', 'updated_by', 'created_at', 'updated_at', 'is_active', 'customer_id', 'comments'
        , 'is_settled', 'date', 'amount', 'purchase_invoice_id', 'purchase_quote_id', 'payment_mode', 'cheque_no', 'status', 'tra_category','adv_payment_id'];
    protected $dates = ['created_at', 'updated_at', 'date'];
    public static $SALES = 'sales_invoice_payment';
    public static $PURCHASE = 'purchase_invoice_payment';
    public static $SALESAccountID = '1';
    public static $SALESAccount = 'Income';
    public static $PURCHASEAccountID = '1';
    public static $PURCHASEAccount = 'Income';
    public static $TYPESAVE = 'invoice save';
    public static $TYPEDELETE = 'invoice delete';
    public static $TYPEPURCHASESAVE = 'Purchase save';
    public static $TYPEPURCHASEDELETE = 'Purchase delete';

}

?>
