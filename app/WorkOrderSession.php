<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class WorkOrderSession extends Model {
    
    protected $table = 'tbl_work_order_session' ;
    protected $fillable = ['work_order_id','updated_by','created_by',
        'is_active','comments','description','type','start_date','end_date','duration','qty'];
    protected $dates = ['created_at', 'updated_at'];

}
