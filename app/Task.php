<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Task extends Model {
    
    protected $table = 'tbl_task' ;
    protected $fillable = ['task_name', 'comments', 'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
