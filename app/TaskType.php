<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TaskType extends Model {
    
    protected $table = 'tbl_task_type' ;
    protected $fillable = ['type_name', 'comments', 'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
