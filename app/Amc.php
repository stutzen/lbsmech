<?php
namespace app;
use Illuminate\Database\Eloquent\Model;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of invoice
 *
 * @author Deepa
 */
class Amc extends Model{
    //put your code here
    protected $table ='tbl_amc';
    protected $fillable = ['customer_id','invoice_id','invoice_item_id','product_id','from_date','to_date','duration','amount','serial_no','status'];
        protected $dates = [
        'created_at',
        'updated_at'
    ];
}

?>
