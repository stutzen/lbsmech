<?php
namespace app;
use Illuminate\Database\Eloquent\Model;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Customer
 *
 * @author Deepa
 */
class CustomerDual extends Model {
    //put your code here
    
    protected  $table ='tbl_customer_dual';
    protected  $fillable = ['account','customer_id','fname','lname','company','jobtitle','phone','email','city','state','zip'
        ,'country','balance','status','notes','tags','password','img','web','facebook','google','linkedin','twitter','skype'
        ,'tax_number','dob','assistant','asst_phone','second_email','second_phone','bankname','banktype','bankcode','bankacct'
        ,'is_active','cdate','climit','mlimt','created_by','updated_by','user_id','is_sale','is_purchase','billing_address','shopping_address','other','fax'
        ,'shopping_pincode','shopping_country','shopping_state','shopping_city','billing_pincode','billing_country','billing_state','billing_city','custom_attribute_json'
        ,'billing_city_id','billing_state_id','billing_country_id','shopping_city_id','shopping_state_id','shopping_country_id'];
    protected $dates = ['created_at','updated_at'];
}

?>
