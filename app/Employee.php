<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Employee extends Model {
    
    protected $table = 'tbl_employee' ;
    protected $fillable = ['email','user_id','team_id','f_name','l_name','emp_code','address','city','state','country','gender'
        ,'salary_type','type_of_emp','salary_amount','is_active','is_user','created_by','updated_by','ph_no'
        ,'dob','doj','city_id','state_id','country_id','esi_percentage','esi_amount','pf_percentage','pf_amount',
        'marital_status','nominee_name','reliving_date','reliving_reason','relationship','basic_salary','img',
        'employer_pf_percentage','employer_esi_percentage','employer_pf_amount','employer_esi_amount',
        'max_leave_count','bonus_percentage','payroll_type'];
    protected $dates = ['created_at', 'updated_at','dob','doj'];
    //put your code here
}

