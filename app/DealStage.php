<?php
namespace app;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActivityLog
 *
 * @author Deepa
 */
class DealStage extends Model {
    //put your code here
    protected  $table = 'tbl_deal_stage';
    protected $fillable = ['name','level','created_by','created_at','updated_by','updated_at','created_by_name','updated_by_name','is_active','stage_flag'];
    protected $dates = ['created_at','updated_at'];
}

?>
