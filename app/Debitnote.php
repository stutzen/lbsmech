<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Debitnote
 *
 * @author Deepa
 */
class Debitnote extends Model {
    //put your code here
    protected  $table = 'tbl_debit_note';
    protected  $fillable = ['invoice_id','date','amount','comments','created_by','updated_by'];
    protected $dates = ['created_at','updated_at'];
}

?>
