<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Source extends Model {
    
    protected $table = 'tbl_source' ;
    protected $fillable = ['name', 'is_active','created_by','updated_by','comments'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>
