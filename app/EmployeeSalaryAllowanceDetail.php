<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeSalaryAllowanceDetail extends Model {
    
    protected $table = 'tbl_emp_salary_allowance_detail' ;
    protected $fillable = ['emp_salary_id','allowance_id','amount', 
        'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

