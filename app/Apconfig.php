<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Appconfig
 *
 * @author Deepa
 */
class Apconfig extends Model{
    //put your code here

    protected $table = 'tbl_app_config';
    protected $fillable = ['setting','value'];
    protected $dates = ['updated_at','created_at'];
}

?>
