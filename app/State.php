<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class State extends Model {
    
    protected $table = 'tbl_state' ;
    protected $fillable = ['code', 'name','country_id','country_name','dual_language'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

?>