<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeAdvDeduction extends Model {
    
    protected $table = 'tbl_emp_adv_deduction' ;
    protected $fillable = ['emp_id','emp_code','date','advance_amt_id','deduction_amt'
        ,'is_active','created_by','updated_by','account_id'];
    protected $dates = ['created_at', 'updated_at','date'];
    //put your code here
}

