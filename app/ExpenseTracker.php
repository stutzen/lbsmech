<?php
namespace app;
use Illuminate\Database\Eloquent\Model;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Expense
 *
 * @author Deepa
 */
class ExpenseTracker extends Model {
    //put your code here
    protected  $table = 'tbl_expense_tracker';
    protected $fillable = ['attachment','amount','comments','allowance_id','user_id','date','role_id','status'
                               ,'created_by','created_at','updated_by','updated_at','is_active'];
    protected $dates = ['created_at','updated_at','date'];
}

?>
