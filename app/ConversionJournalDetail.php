<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class ConversionJournalDetail extends Model {
    
    protected $table = 'tbl_conversion_journal_detail' ;
    protected $fillable = ['conversion_journal_id','from_pdt_id','to_pdt_id','from_qty','to_qty','from_uom_id','to_uom_id','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}
