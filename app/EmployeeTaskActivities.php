<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeTaskActivities extends Model {
    
    protected $table = 'tbl_emp_task_activities' ;
    protected $fillable = ['created_emp_id','created_emp_name','created_emp_code','created_emp_team_id','assigned_emp_id','assigned_emp_name','assigned_emp_code','assigned_emp_team_id',
        'detail','task_id','task_name','status','date','priority','is_active','created_by','updated_by','comments','type','config_type','estimated_date','from_time','to_time','created_for'];
    protected $dates = ['created_at', 'updated_at'];
   
}