<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrnDetail extends Model {

    protected $table = 'tbl_grn_detail';
    protected $fillable = ['mo_id','work_order_input_id','grn_id','amt', 'product_id', 'product_name', 'uom_id', 'uom', 'qty', 'is_active', 'created_by', 'updated_by'
        , 'date', 'product_id', 'comments','failed_qty','passed_qty','qc_status'];
    protected $dates = ['created_at', 'updated_at'];

}
