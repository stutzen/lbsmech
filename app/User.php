<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
 
class User extends Model implements
AuthenticatableContract, AuthorizableContract {

//   use Authenticatable\
//        Authorizable\
//        HasApiTokens;

    protected $table = 'tbl_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'created_by', 'updated_by', 'f_name', 'l_name', 'gender', 'dob', 'email', 'ph_no', 'city', 
        'state', 'country', 'address', 'post_code', 'user_type','status', 'is_active', 'comp_id', 'imagepath',
        'device_id', 'remember_token','role_id','rolename','latitude','longitude','last_geo_point','ip_address'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    
//    
//    public function company()
//    {
//        return $this->belongsTo(\App\Company::class);
//    }

    public function can($ability, $arguments = array()) {
        
    }

    public function getAuthIdentifier() {
        
    }

    public function getAuthIdentifierName() {
        
    }

    public function getAuthPassword() {
        
    }

    public function getRememberToken() {
        
    }

    public function getRememberTokenName() {
        
    }

    public function setRememberToken($value) {
        
    }

}
