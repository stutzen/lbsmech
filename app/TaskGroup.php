<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class TaskGroup extends Model {
    
    protected $table = 'tbl_task_group' ;
    protected $fillable = ['name','code','type','updated_by','created_by','is_active','comments'];
    protected $dates = ['created_at', 'updated_at'];

}
