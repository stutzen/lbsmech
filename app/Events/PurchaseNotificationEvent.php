<?php

namespace App\Events;

use App\PurchaseInvoice;
use DB;
use Illuminate\Queue\SerializesModels;

class PurchaseNotificationEvent extends Event
{
    use SerializesModels;

    public $purchaseinvoice;   

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(PurchaseInvoice $purchaseinvoice)
    { 
        $this->purchaseinvoice = $purchaseinvoice;
    }
    

}