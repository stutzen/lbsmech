<?php

namespace App\Events;

use App\Invoice;
use DB;
use Illuminate\Queue\SerializesModels;

class InvoiceNotificationEvent extends Event
{
    use SerializesModels;

    public $invoice;   

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }
    
    

}