<?php

namespace App\Events;

use App\Payment;
use DB;
use Illuminate\Queue\SerializesModels;

class InvoicePaymentNotificationEvent extends Event
{
    use SerializesModels;

    public $payment;   

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }
    
    

}