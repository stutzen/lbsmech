<?php

namespace App\Events;

use App\PurchasePayment;
use DB;
use Illuminate\Queue\SerializesModels;

class PurchasePaymentNotificationEvent extends Event
{
    use SerializesModels;

    public $purchasepayment;   

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(PurchasePayment $purchasepayment)
    {
        $this->purchasepayment = $purchasepayment;
    }
    
    

}