<?php

namespace App\Events;

use App\Deal;
use DB;
use Illuminate\Queue\SerializesModels;

class DealNotificationEvent extends Event
{
    use SerializesModels;

    public $deal;    

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(Deal $deal)
    { 
        $this->deal = $deal; 
    }
    
    

}