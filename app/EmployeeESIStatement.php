<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class EmployeeESIStatement extends Model {
    
    protected $table = 'tbl_employee_esi_statement' ;
    protected $fillable = ['emp_id','emp_salary_id','days','salary_amount','esi_percentage','esi_amount', 
        'employer_esi_percentage','employer_esi_amount','total_esi_amount','is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];
    //put your code here
}

