<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model {

    protected $table = 'tbl_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'acode', 'account', 'description', 'balance', 'bank_name', 'account_number', 'currency', 'branch', 
        'address', 'contact_person', 'contact_phone', 'website', 'notes', 'sno', 'token', 
        'created_by', 'updated_by', 'is_active', 'status','prefix'
    ];
    protected $dates = [
        'created_at',
        'updated_at'
        ];

//    /**
//     * The attributes excluded from the model's JSON form.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        
//    ];
}
