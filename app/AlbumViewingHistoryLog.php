<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumViewingHistoryLog extends Model {

    protected $table = 'tbl_album_viewing_history_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'album_id', 'album_code', 'email','customer_id','ip','created_by_name','updated_by_name',
        'created_by', 'updated_by', 'is_active'   
    ];
    protected $dates = [
        'created_at',
        'updated_at'
        ];

//    /**
//     * The attributes excluded from the model's JSON form.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        
//    ];
}
