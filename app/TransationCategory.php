<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SysCategory
 *
 * @author Deepa
 */
class TransationCategory extends Model {
    //put your code here
    protected $table = 'tbl_tran_category' ;
     protected $fillable = ['name', 'comment', 'type','is_active','created_by','updated_by','seq_no','created_by_name','updated_by_name'];
     protected $dates = ['created_at', 'updated_at'];
}

?>
