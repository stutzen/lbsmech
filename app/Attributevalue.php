<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Attributevalue
 *
 * @author Deepa
 */
class Attributevalue extends Model {
    //put your code here
    protected  $table ='tbl_attribute_value';
    protected  $filable =['attributes_id','refernce_id','value','attribute_code','sno','attribute_typeid','attributetype_id'
                            ,'created_by','updated_by'];
    protected $dates =['updated_at','created_at'];
}

?>
