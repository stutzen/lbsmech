<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TaskFollowers extends Model {
    
    protected $table = 'tbl_task_followers' ;
    protected $fillable = ['task_id','task_activity_id','emp_id' , 'is_active','created_by','updated_by'];
    protected $dates = ['created_at', 'updated_at'];

}

?>
