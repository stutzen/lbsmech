<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Attributestypes
 *
 * @author Deepa
 */
class Attributestype extends Model {
    //put your code here
    protected $table = 'tbl_attribute_types' ;
     protected $fillable = [ 'code', 'is_active','display_name'];
}

?>
